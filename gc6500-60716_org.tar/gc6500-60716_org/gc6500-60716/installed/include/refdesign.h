/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

/*******************************************************************************
* @(#) $Id: demoapp.h 1663 2004-10-11 04:21:41Z cwein $
*******************************************************************************/

#ifndef __REFDESIGN_H
#define __REFDESIGN_H

#include <endian.h>

#define BE8(a)  (a)
#if __BYTE_ORDER == BIG_ENDIAN
#define BE16(a) (a)
#define BE24(a) (a)
#define BE32(a) (a)
#define BE64(a) (a)
#else
#define BE16(a)                                                             \
        ((((a)>>8)&0xFF) |                                                  \
             (((a)<<8)&0xFF00))
#define BE24(a)                                                             \
        ((((a)>>16)&0xFF) |                                                 \
             ((a)&0xFF00) |                                                 \
             (((a)<<16)&0xFF0000))
#define BE32(a)                                                             \
        ((((a)>>24)&0xFF) |                                                 \
             (((a)>>8)&0xFF00) |                                            \
             (((a)<<8)&0xFF0000) |                                          \
             ((((a)<<24))&0xFF000000))
#define BE64(a)                                                             \
        ((unsigned long long) BE32((unsigned int) (a)) << 32) |             \
             (BE32((unsigned long long) (a) >> 32))
#endif

extern int codecs_partid[];
#define QMM_PARTITION(codec)	(codecs_partid[codec])

#ifdef _WIN32
#else
#define O_BINARY 0
#endif

#endif
