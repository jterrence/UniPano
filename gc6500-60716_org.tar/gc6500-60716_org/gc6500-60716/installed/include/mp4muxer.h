/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef __MP4MUXER_H
#define __MP4MUXER_H


#ifdef __cplusplus
extern "C" {
#endif 

#include <sys/types.h>
#include "mp4mux.h"

    typedef void *MP4MUXER_HANDLE;

    typedef struct
    {
        unsigned char* vidBuff;
        unsigned char* audBuff;
        int vidSize;
        int audSize;
        long long timeStamp;
    }MP4MUXER_PROCESS_PARAMS;


    MP4MUXER_HANDLE mp4MuxerOpen(char *name);
    int  mp4MuxerClose(MP4MUXER_HANDLE  h);
    int  mp4MuxerProcess(MP4MUXER_HANDLE  h, MP4MUXER_PROCESS_PARAMS *p);

#ifdef __cplusplus
}
#endif 

#endif
