/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#define TO_FOURCC_LE(d,c,b,a) (a<<24|b<<16|c<<8|d)
#define TO_FOURCC_BE(a,b,c,d) (a<<24|b<<16|c<<8|d)


typedef enum
{
  MP4_AUDIO_OBJECT_TYPE_AAC = 0x40,
  MP4_AUDIO_OBJECT_TYPE_MP2 = 0x6B
}MP4AudObjType;

// Usefule struct
typedef struct
{
	unsigned long size;
	unsigned long type;	
} mp4_atom_box ;


typedef struct
{
	unsigned long version : 8;
	unsigned long flag : 24;
} mp4_atom_flag;

typedef struct
{
	unsigned long size;
	unsigned long type;
	union {
		unsigned long value;
		mp4_atom_flag field;
	} flags	;
} mp4_atom_full_box ;



// MP4 atoms for Video

typedef struct  
{
	mp4_atom_box atom ;
	unsigned long major_brand;
	unsigned long minor_version;
	unsigned long compatible_brands[4];
} mp4_atom_ftyp ;

typedef struct
{
	mp4_atom_box atom;
} mp4_atom_moov;


typedef struct
{
	mp4_atom_box atom;
} mp4_atom_mdat;

typedef struct
{
	unsigned long size;
	unsigned long type;
	unsigned long long largesize;
} mp4_atom_mdat64;

typedef struct
{
	mp4_atom_box atom;
} mp4_atom_free;

typedef struct
{
	mp4_atom_full_box atom;
	unsigned long creation_time;
	unsigned long modification_time;
	unsigned long timescale;
	unsigned long duration;
	unsigned long rate;
	unsigned long volume;
	unsigned long reserved[2];
	unsigned long matrix[9];
	unsigned long pre_defined[6];
	unsigned long next_track_ID;
} mp4_atom_mvhd;
typedef struct
{
  mp4_atom_full_box atom;
  unsigned long long creation_time;
  unsigned long long modification_time;
  unsigned long timescale;
  unsigned long long duration;
  unsigned long rate;
  unsigned long volume;
  unsigned long reserved[2];
  unsigned long matrix[9];
  unsigned long pre_defined[6];
  unsigned long next_track_ID;
} mp4_atom_mvhd64;

typedef struct 
{
	mp4_atom_box atom;
} mp4_atom_trak;

typedef struct 
{
	mp4_atom_full_box atom;
	unsigned long creation_time;
	unsigned long modification_time;
	unsigned long track_ID;
	unsigned long reserved;
	unsigned long duration;
	unsigned long reserved2[2];
	char layer[2];
	char alternate_group[2];
	unsigned long volume ;
	unsigned long matrix[9];
	unsigned long width;
	unsigned long height;
} mp4_atom_tkhd;

typedef struct 
{
  mp4_atom_full_box atom;
  unsigned long long creation_time;
  unsigned long long modification_time;
  unsigned long track_ID;
  unsigned long reserved;
  unsigned long long duration;
  unsigned long reserved2[2];
  char layer[2];
  char alternate_group[2];
  unsigned long volume ;
  unsigned long matrix[9];
  unsigned long width;
  unsigned long height;
} mp4_atom_tkhd64;

typedef struct 
{
	mp4_atom_box atom;
} mp4_atom_mdia;

typedef struct
{
	mp4_atom_full_box atom;
	unsigned long creation_time;
	unsigned long modification_time;
	unsigned long timescale;
	unsigned long duration;
	unsigned long language;
} mp4_atom_mdhd;
typedef struct
{
  mp4_atom_full_box atom;
  unsigned long long creation_time;
  unsigned long long modification_time;
  unsigned long timescale;
  unsigned long long duration;
  unsigned long language;
} mp4_atom_mdhd64;


typedef struct
{
	mp4_atom_full_box atom;
	unsigned long pre_defined ;
	unsigned long handler_type;
	unsigned long reserved[3];
	char name[18];
} mp4_atom_hdlr;

typedef struct 
{
	mp4_atom_box atom;
} mp4_atom_minf;

typedef struct
{
	mp4_atom_full_box atom;
	unsigned long graphicsmode;
	unsigned long opcolor;
} mp4_atom_vmhd;

typedef struct
{
	mp4_atom_box atom;
} mp4_atom_dinf;

typedef struct
{
	mp4_atom_full_box atom;
	char * location;
} mp4_atom_url;

typedef struct
{
	mp4_atom_full_box atom;
	unsigned long entry_count;
} mp4_atom_dref;

typedef struct
{
	mp4_atom_box atom;
} mp4_atom_stbl;

typedef struct 
{
	mp4_atom_full_box atom;
	unsigned long entry_count;
} mp4_atom_stsd;

typedef struct 
{
	unsigned long sample_count;
	unsigned long sample_delta;
} stts_entry;

typedef struct
{
	mp4_atom_full_box atom;
	unsigned long entry_count;	
	stts_entry * table;
} mp4_atom_stts;

typedef struct
{ 
	mp4_atom_full_box atom;
	unsigned long entry_count;
	unsigned long * sample_number;	
} mp4_atom_stss;

typedef struct 
{
	unsigned long first_chunk;
	unsigned long samples_per_chunk;
	unsigned long sample_description_index;
} stsc_entry;

typedef struct
{
	mp4_atom_full_box atom;
	unsigned long entry_count;
	stsc_entry * table;
} mp4_atom_stsc;

typedef struct
{
	mp4_atom_full_box atom;
	unsigned long sample_size;
	unsigned long sample_count;
	unsigned long * entry_size;

} mp4_atom_stsz;

typedef struct
{
	mp4_atom_full_box atom;
	unsigned long entry_count;
	unsigned long * chunk_offset;
} mp4_atom_stco;

typedef struct
{
	mp4_atom_full_box atom;
	unsigned long entry_count;
	unsigned long long * chunk_offset;
} mp4_atom_co64;


typedef struct
{
	mp4_atom_box atom;
	char configurationVersion;
	char AVCProfileIndication;
	char profile_compatibility;
	char AVCLevelIndication;
	char lengthSizeMinusOne;
	char numOfSequenceParameterSets;
	char sequenceParameterSetLength[2];
	char * sequence_entries;
	char numOfPictureParameterSets;
	char pictureParameterSetLength[2];
	char * picture_entries;
} mp4_atom_avcC;

typedef struct
{
	mp4_atom_box atom;
	unsigned long reserved;
	unsigned long data_reference_index;
	unsigned long pre_defined[4];
	unsigned long width_height;
	unsigned long horizresolution;
	unsigned long vertresolution;
	unsigned long reserved2;
	char frame_count[2];
	char compressorname[32];
	char depth[4];
} mp4_atom_avc1;

typedef struct
{
	unsigned long size;
	unsigned long type;
	unsigned long uuid[4];
	unsigned long data;
} mp4_atom_uuid;

typedef struct
{
  mp4_atom_ftyp  ftyp;
  mp4_atom_moov  moov;
  unsigned int moov_offset;
  mp4_atom_mdat  mdat;
  int mdat_offset;
  mp4_atom_mvhd  mvhd;
  mp4_atom_trak  trak;
  mp4_atom_tkhd  tkhd;
  mp4_atom_tkhd64  tkhd64;
  mp4_atom_mdia  mdia;
  mp4_atom_mdhd  mdhd;
  mp4_atom_mdhd64  mdhd64;
  mp4_atom_hdlr  hdlr;
  mp4_atom_minf  minf;
  mp4_atom_vmhd  vmhd;
  mp4_atom_dinf  dinf;
  mp4_atom_dref  dref;
  mp4_atom_stbl  stbl;
  mp4_atom_stsd  stsd;
  mp4_atom_url  url;
  mp4_atom_avc1  avc1;
  mp4_atom_avcC  avcC;
  mp4_atom_uuid  uuid;
  mp4_atom_stts  stts;
  mp4_atom_stss  stss;
  mp4_atom_stsc  stsc;
  mp4_atom_stsz  stsz;
  mp4_atom_stco  stco;\
  mp4_atom_co64  co64;
	mp4_atom_mdat64 mdat64;
  mp4_atom_mvhd64 mvhd64;

	
} mp4_header_video_handler;


// MP4 atoms for Audio

typedef struct
{
	mp4_atom_full_box atom;
	unsigned short balance;
	unsigned short reserved;
	
} mp4_atom_smhd;


typedef struct
{
  mp4_atom_box atom;
  unsigned char reserved1[6];
  unsigned short data_ref_index;
  unsigned long reserved2[2];
  unsigned short channelcount;
  unsigned short samplesize;
  unsigned short predefined;
  unsigned short reserved3;
  unsigned int   samplerate;

} mp4_atom_mp4a;



typedef struct _BaseDescriptor
{
  unsigned char tag;
  /* the first bit of each byte indicates if the next byte should be used */
  unsigned long size;
} BaseDescriptor;

typedef struct _DecoderSpecificInfoDescriptor
{
  BaseDescriptor base;
  unsigned char* data;
} DecoderSpecificInfoDescriptor;


typedef struct _DecoderConfigDescriptor {
  BaseDescriptor base;

  unsigned char object_type;

  /* following are condensed into streamType:
   * bit(6) streamType;
   * bit(1) upStream;
   * const bit(1) reserved=1;
  */
  unsigned char stream_type;

  unsigned char buffer_size_DB[3];
  unsigned long max_bitrate;
  unsigned long avg_bitrate;

  DecoderSpecificInfoDescriptor dec_specific_info;
} DecoderConfigDescriptor;

typedef struct _SLConfigDescriptor
{

  BaseDescriptor base;
  unsigned char predefined;              /* everything is supposed predefined */
} SLConfigDescriptor;

typedef struct _ESDescriptor
{
  BaseDescriptor base;

  unsigned short id;

  /* flags contains the following:
   * bit(1) streamDependenceFlag;
   * bit(1) URL_Flag;
   * bit(1) OCRstreamFlag;
   * bit(5) streamPriority;
   */
  unsigned char flags;

#if 0
  unsigned short depends_on_es_id;
  unsigned char  url_length;              /* only if URL_flag is set */
  unsigned char* *url_string;             /* size is url_length */
  unsigned short ocr_es_id;              /* only if OCRstreamFlag is set */
#endif

  DecoderConfigDescriptor dec_conf_desc;
  SLConfigDescriptor sl_conf_desc;

  /* optional remainder of ESDescriptor is not used */
} ESDescriptor;

typedef struct
{
  mp4_atom_full_box atom;
  //unsigned char   audiospecificconfig[2];
  ESDescriptor es_desc;

} mp4_atom_esds;

typedef struct
{
  mp4_atom_trak  trak;
  mp4_atom_tkhd  tkhd;
  mp4_atom_tkhd64  tkhd64;
  mp4_atom_mdia  mdia;
  mp4_atom_mdhd  mdhd;
  mp4_atom_mdhd64  mdhd64;
  mp4_atom_hdlr  hdlr;
  mp4_atom_minf  minf;
  mp4_atom_smhd  smhd;
  mp4_atom_dinf  dinf;
  mp4_atom_dref  dref;
  mp4_atom_url   url;
  mp4_atom_stbl  stbl;
  mp4_atom_stsd  stsd;
  mp4_atom_mp4a  mp4a;
  mp4_atom_esds  esds;
  mp4_atom_stts  stts;
  mp4_atom_stsc  stsc;
  mp4_atom_stsz  stsz;
  mp4_atom_stco  stco;
  mp4_atom_co64  co64;
} mp4_header_audio_handler;

// Prototypes :
mp4_header_video_handler* create_empty_video_header(unsigned long track_ID,
		unsigned long width, unsigned long height);
mp4_header_audio_handler* create_empty_audio_header(unsigned long track_ID,unsigned int volume);
int mp4_header_video_swap(mp4_header_video_handler * h);
int mp4_header_video_write(mp4_header_video_handler * h, int fd,int large_file);
int mp4_header_audio_swap(mp4_header_audio_handler * h);
int mp4_header_audio_write(mp4_header_audio_handler * h, int fd, int large_file);
void mp4_header_video_duration_update(mp4_header_video_handler * h,unsigned long duration);
void mp4_header_video_size_update(mp4_header_video_handler *h,int large_file);
void mp4_audio_video_merge(mp4_header_video_handler *vh,mp4_header_audio_handler *ah);
void mp4_header_audio_duration_update(mp4_header_audio_handler * h,unsigned long duration);
void mp4_audio_header_size_update(mp4_header_audio_handler *h,int largefile);
int mp4_header_movie_write(mp4_header_video_handler * h, int fd,int largefile);
void mp4_header_video_extended_duration_update(mp4_header_video_handler * h,unsigned long duration);
void mp4_header_audio_extended_duration_update(mp4_header_audio_handler * h,unsigned long duration);
// Prototype for atoms
void mp4_stts_update(mp4_atom_stts * atom, unsigned long sample_count, unsigned long sample_delta);
void mp4_stss_update(mp4_atom_stss * atom, unsigned long sample_number);
void mp4_stsc_update(mp4_atom_stsc * atom, unsigned long first_chunk, unsigned long samples_per_chunk,unsigned long sample_description_index);
void mp4_stsz_update(mp4_atom_stsz * atom, unsigned long entry_size);
void mp4_stco_update(mp4_atom_stco * atom, unsigned long chunk_offset);
void mp4_co64_update(mp4_atom_co64 * atom, unsigned long long chunk_offset);
void mp4_avcC_update(mp4_atom_avcC * atom, unsigned short spslen, char * sps , unsigned short ppslen, char * pps);
void mp4_esds_update(mp4_atom_esds * esds,int asclength, unsigned char* asc,MP4AudObjType objType);

#define ENCODER_CLOCK 90000

