/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/


#ifndef GLOBAL_H
#define GLOBAL_H

//VPP stats
//Add additional stats to the end of enum before 'FEATURECOUNT'.
enum Features
{
    HVHF,
    SOBEL,
    FEATURECOUNT
};

enum Sensitivity
{
    HIGH_SENSITIVITY,
    MED_SENSITIVITY,
    LOW_SENSITIVITY,
};


#endif  /* GLOBAL_H */
/*******************************************************************************
* vi: set shiftwidth=4 tabstop=8 softtabstop=4 expandtab nosmarttab:
*******************************************************************************/

