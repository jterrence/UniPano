/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/


#ifndef RECTANGLE_H
#define RECTANGLE_H

struct Coordinates
{
    int top_left_mbx, top_left_mby;
    int bot_right_mbx, bot_right_mby;
};

class Rectangle
{
    //Basic parameters
public:
    int id;
    Coordinates location;

    //Adavanced parameters
    int Size;
    int featureCount;
    int *featureScore;
    int trackingCount;
    int overlapid;
    int childCount;

    bool final;
    Coordinates parent_location;
    void Init(int features);
    void Close();

};

#endif  /* RECTANGLE_H */
/*******************************************************************************
* vi: set shiftwidth=4 tabstop=8 softtabstop=4 expandtab nosmarttab:
*******************************************************************************/

