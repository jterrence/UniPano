/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/


#ifndef CONFIG_H
#define CONFIG_H
#include    "global.h"



class Config
{
    public:
        //Image configuration
        int imageWidth, imageHeight;

        //Histogram configuration
        int histogram_threshold;
        int histogram_bins;

        //Histogram classifier configuration
        bool enable_running_avg;

        //BG model configuration
        int short_term_learning_time;
        int learning_time;
        
        //Rectangle configuration
        int rectangle_fg_size_threshold;
        int rectangle_min_fg_count;
        int maxRectangles;
        
        //Rectangle overlap configuration
        int overlap_distance_threshold;
        int overlap_size_threshold0;
        int overlap_size_threshold1;
        int overlap_tracking_count_threshold;

        //overlap pruning configuration
        int *overlap_prune_threshold0;
        int *overlap_prune_threshold1;
        
        //object pruning configuration
        int *prune_threshold;
        int prune_line_threshold;

        //pixel processing config
        int pixel_histogram_bins;
        int pixel_sub_sampling_rate;
        int pixel_SAD_threshold;
        float pixel_histogram_l2norm_threshold;
        
        //persistent tracking config
        int persistent_tracking_count;

        //sensitivity level - 0 high sensitivity, 1 - med sensitivity, 2 - low sensitivity
        int sensitivity;
        int hist_range[3][3]; // a set of 3 for each level of sensitivity.

        //Debug configurations
        int debug;
        int mbx;
        int mby;
        
        //pixel processing time
        int proc_time_per_pixel_ns;
        long int smartmotion_proc_time_limit_us;
        int pixel_group_size;

        //functionalities
        Config();
        ~Config();

        void SetImage(int width, int height);
        void SetFPS(int fps);
        void SetSensitivity(Sensitivity  value); 
};

#endif  /* CONFIG_H */
/*******************************************************************************
* vi: set shiftwidth=4 tabstop=8 softtabstop=4 expandtab nosmarttab:
*******************************************************************************/

