
typedef struct EdgeStructure
{
  int strength;
  int direction;
} Edge;

class VPPStatistics
{
	private:

    	unsigned char *yHHfBuffer, *yVHfBuffer, *yHLfBuffer, *yVLfBuffer ;
    	unsigned char *yHHfMBAvg, *yVHfMBAvg; 
    	unsigned char *yBufferHorzEdge, *yBufferVertEdge, *yBufferDiagRightEdge, *yBufferDiagLeftEdge;
    	int *intBufferHorzEdge, *intBufferVertEdge, *intBufferDiagRightEdge, *intBufferDiagLeftEdge;
    	
    	int  *HorzEdgeMBAvg, *VertEdgeMBAvg, *MBSum, *DiagRightEdgeMBAvg, *DiagLeftEdgeMBAvg;
		int *MBAverages;
    	Edge *EdgePictMBArray;


		void MemoryError(const char *message);
		void makeMBStats(unsigned char *BufferIn, int *BufferOut0, int *BufferOut1, int *total, int *max,
                 			int hsize, int vsize, int mblines, int mbGain, int squareFlag);
		
		void sortEdge(Edge EdgeArray[], int size);
		int makeMBEdgeStats(int *BufferHorzEdge, int *BufferVertEdge, int *BufferDiagRightEdge,	int *BufferDiagLeftEdge,
		     					Edge *EdgePictMb, int hsize, int vsize, int EdgeLowMbThr, int EdgeNeighborMbThr, int EdgeRangeMbThr);
		void HorzVertStatFilter(unsigned char *BufferIn, unsigned char *HorzLowPass, unsigned char *VertLowPass, unsigned char *HorzHighPass, unsigned char *VertHighPass, 
                        			int hsize, int vsize, int hgain, int vgain, int HighFreqByPass);

		void genMBHighFreq(unsigned char *HorzHFMBavg_BufferIn, unsigned char *VertHFMBavg_BufferIn, int *HFMBavg_BufferOut, int widthMB, int heightMB);
		void genHVHFStat(unsigned char *yBufferIn, int *yHVHfMBAvg, int hsize, int vsize);
		void HorzVertEdgeDetect(unsigned char *BufferIn, unsigned char *BufferHorzEdge, unsigned char *BufferVertEdge, unsigned char *BufferDiagRightEdge, unsigned char *BufferDiagLeftEdge,
                        int *intBufferHorzEdge, int *intBufferVertEdge, int *intBufferDiagRightEdge, int *intBufferDiagLeftEdge, int hsize, int vsize, int EdgeDetectBypass);

		void genSobelStat(unsigned char *yBufferIn, int *ySobelMBAvg, int hsize, int vsize);

	public:
		VPPStatistics(int width, int height);
		~VPPStatistics();
		void GenerateVPPStats(unsigned char *yBufferIn, int *yHVHfMBAvg, int *ySobelMBAvg, int hsize, int vsize);
};
