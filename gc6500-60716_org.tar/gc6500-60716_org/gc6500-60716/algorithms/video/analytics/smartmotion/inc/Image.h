/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/


#ifndef IMAGEPROFILE_H
#define IMAGEPROFILE_H
#include    "Config.h"
#include    "PlatformAbstraction.h"
#include    "Rectangle.h"
class BGModel;
class RectangleProcessor;
class PixelProcessor;
class Feature;

class Image
{
    private:
        
        BGModel *currBGModel;
        BGModel *prevBGModel;
        Config *cfg;
        PixelProcessor *pProc;
        Feature **featureList;
        RectangleProcessor *rProc;
        int MBSize;
        int frameNumber;
        int *mbAddress;
        unsigned char *ycurrAddress, *yprevAddress;
        int *cHist, *pHist;

        int ValidateRectangle( Rectangle *Rect );

        long int pixel_proc_time_limit_us;

        struct timeval timerbegin, timerend;

        int ProcessNewRectangles(Rectangle **newRects);
        void StartTimer();
        long int GetElapsedTime();

    public:
        Image(Config *cfg);
        ~Image();
        void InitBGModel();
        int GetNumberofRectangles();    
        Rectangle** GetRectanglePointer();    
        int GetFrameNumber();
        void ProcessData();
        void SetYAddress(unsigned char *ycurrAddress, unsigned char *yprevAddress);
        void SetFeatureAddress(int, int*);
        void AddFeature(int);
        int CalculateMDistance(int feature, Rectangle *Rect);
        int CheckBGStrength(Rectangle *Rect);
};

#endif  /* IMAGEPROFILE_H */
/*******************************************************************************
* vi: set shiftwidth=4 tabstop=8 softtabstop=4 expandtab nosmarttab:
*******************************************************************************/

