/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/


#ifndef PLATFORMABSTRACTION_H_
#define PLATFORMABSTRACTION_H_\

#include <sys/time.h>


void *AlarmMemAlloc(unsigned int  n);
void *AlarmMemSet(void *s, int c, unsigned int  n);
void AlarmMemFree(void* ptr);
int AlarmAbs( int j);
int AlarmPrintf (const char *format, ...);
int AlarmGetTime(struct timeval *tv);
void AlarmAssert( int condition);

#endif /* PLATFORMABSTRACTION_H_ */


