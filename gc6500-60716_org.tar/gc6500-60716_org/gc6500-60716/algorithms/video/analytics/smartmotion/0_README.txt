/********************************************Smart Motion Library*******************************************/
* This contains the source code for smartmotion. Refer to the document "Intelligent Motion Alarms", for     *
* a detailed discussion of the algorithm.  																    *				
************************************************************************************************************/


/********************************************Directory Structure********************************************/

smartmotion|
	   |-test
	   |-inc
           |-src
           |-obj 


The source code for executable 'alarm' is within test directory. 
All smart motion library files are within src directory.



/********************************************Building The Source********************************************/

The Makefile provided can be used to compile same source code on x86 platform and MIPS.
There are code segments which uses SIMD capabilities on MIPS to accelerate various algorithms(some of which are written MIPS assembly), 
those are disabled when the platform is x86.

Run make from top level directory, smartmotion. By default the executable and library is compiled for x86.
To produce MIPS compatible library, run make with the option 'TARGET_PLATFORM=MIPS'.  




/*********************************************Using The Library********************************************/

Run ./alarm to get the usage options

Usage: ./alarm -i <input 4:2:0 yuv> -w <width> -h <height> -n <frames> -g <generate statistics>
eg:- ./alarm -i night_720x1480.yuv -g 1 -w 720 -h 480 -n 120

The input to the library is a yuv 4:2:0 video. If the option 'g' is set to 1 then the library generates the 
statistics required. Set this option to 1 when Raptor statistics are unavailable.


/*************************************************Output***************************************************/

The application detects genuine objects within each frame, the output will be the coordinates of the rectangle enclosing the object.
The coordinates are listed in macroblock units(16 pixels).
The structure of the output is,  
<rectangle id> <bot_right_mbx> <bot_right_mby> <top_left_mbx> <top_leftmby> size <size> hvhf <hvhf> sobel <sobel> tracking count <tracking count> parent id < parent index>  MD <MD> 


The output contains a lot more more information than needed. The id of the rectangle and its coordinates are first 5 fields, which are the most important information. The output also
contains other useful information like tracking count and parent rectangle id.

The algorithm does not produce any output for the first 100 frames, this is the initial learning time for the algorithm.


A sample output is listed below,

.........................
#Picture 100
1 44 18 38 15 size 22 hvhf 3 sobel 46 tracking count 0 parent id -1 MD 79 150 
#Picture 101
3 44 27 37 22 size 31 hvhf 47 sobel 35 tracking count 0 parent id -1 MD 104 20 
1 44 18 37 15 size 24 hvhf 6 sobel 62 tracking count 1 parent id 1 MD 54 170 
#Picture 102
2 44 18 37 15 size 26 hvhf 9 sobel 59 tracking count 2 parent id 1 MD 62 162 
4 44 29 33 22 size 40 hvhf 22 sobel 17 tracking count 1 parent id 3 MD 47 2 
#Picture 103
0 44 18 36 14 size 28 hvhf 8 sobel 48 tracking count 3 parent id 2 MD 50 128 
2 44 26 36 22 size 33 hvhf 44 sobel 31 tracking count 2 parent id 4 MD 89 11 
#Picture 104
0 44 18 36 14 size 31 hvhf 6 sobel 48 tracking count 4 parent id 0 MD 50 1
...............................

/*************************************************Object Reference***************************************************/

The library contains multiple objects which serve different purposes.

There are a number of confifguration parameters that the library uses, and all of those are set
using the Config object.
 
The algorithm works on two sets of image features, Sobel edge and high frequency. 
The features are represented using 'Feature' object. 

Histogram modelling and feature classification is done for each macroblock using Histogram
object.

The background model grouping algorithms which include connected components and rectangle
formation is handled by BGModel object.

Image object is the top level object, that coordinates the activities of various objects
and produces the final result. This object also contains much of the rectangle filtering
operations.
 
VPPStatistics object produces the Raptor statistics, including Sobel edge and high frequency, when the algorithm
does not have access to Raptor.

/************************************************ Visualization of alarm rectangle output *************************************/

Can be done with overlayRect.c in algorithms/video/tools/Utils

Just make sure that the X and Y coordinates for top-left and bottom-right corners are aligned correctly.

overlayRect expects, per rectangle, 

X0 X1 Y0 Y1

alarm outputs 

X1 Y1 X0 Y0

where top-left corner is (X0,Y0) and bottom-right corner is (X1,Y1)
