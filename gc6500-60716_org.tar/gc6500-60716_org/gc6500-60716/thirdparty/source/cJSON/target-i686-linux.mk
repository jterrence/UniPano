#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================

#===============================================================================
# @(#) $Id: target-i686-linux.mk 57936 2016-04-30 11:26:30Z bsmith $
#===============================================================================

BUILD_DIR	= build.i686-linux

AS		= as
LD		= ld
CC		= gcc
CXX		= g++
AR		= ar
STRIP		= strip
OBJCOPY		= objcopy

CFLAGS		=					\
		-g					\
		-ggdb					\
		-O2					\
		-Wall					\
		-Wpointer-arith				\
		-Wstrict-prototypes			\
		-Wundef					\
		-Wcast-align				\
		-Wcast-qual				\
		-Wextra					\
		-Wno-empty-body				\
		-Wno-unused-parameter			\
		-Wshadow				\
		-Wwrite-strings				\
		-Wswitch-default			\
		-Wno-array-bounds			\
		-Wno-ignored-qualifiers			\
		-fno-exceptions				\
		-fsigned-bitfields			\
		-fsigned-char				\
		-fmessage-length=0			\
		$(C_INC_DIR:%=-I%)

#===============================================================================
# vi: set filetype=make shiftwidth=8 tabstop=8 softtabstop=8 noexpandtab nosmarttab:
#===============================================================================
