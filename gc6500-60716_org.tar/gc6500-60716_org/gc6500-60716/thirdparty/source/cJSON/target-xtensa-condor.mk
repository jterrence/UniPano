#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================

#===============================================================================
# @(#) $Id: makefile.skel,v 1.10 2014/01/22 00:02:45 monsen Exp $
#===============================================================================


BUILD_DIR		= build.xtensa-condor
XTDEVTOOLS_DIR		= /design/mm/condora0/release/project_ip_ext/tensilica/xtensa/XtDevTools
export LM_LICENSE_FILE	= 27000@carbon.local.geosemi.com
export XTENSA_CORE	= hardware_eval_tie
export XTENSA_SYSTEM	= /design/mm/condora0/release/project_ip_ext/tensilica/core_asic/tensilica_2013_06_01/RD-2012.4-linux/hardware_eval_tie/config
export XTENSA_SW_TOOLS	= $(XTDEVTOOLS_DIR)/install/tools/RD-2012.4-linux/XtensaTools

AS		= $(XTENSA_SW_TOOLS)/bin/xt-as
LD		= $(XTENSA_SW_TOOLS)/bin/xt-ld
CC		= $(XTENSA_SW_TOOLS)/bin/xt-xcc
CXX		= $(XTENSA_SW_TOOLS)/bin/xt-xc++
AR		= $(XTENSA_SW_TOOLS)/bin/xt-ar
STRIP		= $(XTENSA_SW_TOOLS)/bin/xt-strip
OBJCOPY		= $(XTENSA_SW_TOOLS)/bin/xt-objcopy

CFLAGS		=					\
		-g					\
		-O2					\
		--xtensa-system=$(XTENSA_SYSTEM)	\
		--xtensa-core=$(XTENSA_CORE)		\
		--xtensa-params=			\
		-Wall					\
		-Wpointer-arith				\
		-Wstrict-prototypes			\
		-Wundef					\
		-Wcast-align				\
		-Wcast-qual				\
		-Wextra					\
		-Wno-unused-parameter			\
		-Wshadow				\
		-Wwrite-strings				\
		-Wswitch-default			\
		-fno-exceptions				\
		-mlongcalls				\
		-fsigned-bitfields			\
		-fsigned-char				\
		-fmessage-length=0			\
		$(C_INC_DIR:%=-I%)
C_INC_DIR	=					\
		$(XTENSA_SW_TOOLS)/xtensa-elf/include	

#===============================================================================
# vi: set filetype=make shiftwidth=8 tabstop=8 softtabstop=8 noexpandtab nosmarttab:
#===============================================================================
