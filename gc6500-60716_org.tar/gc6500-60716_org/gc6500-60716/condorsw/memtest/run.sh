#!/bin/sh -x

#Use following image when using DDR boot loader
$GEOSW_ROOT/condorsw/host/apps/mxcam/mxcam boot $GEOSW_ROOT/condorsw/images/ddrboot/gc6500_ddrboot_fw.img memtest-tiled-768-1088.json $GEOSW_ROOT/condorsw/config/isp/sensor_ar0330_dsl216_le.bin
#Use following image when using cache boot loader
#$GEOSW_ROOT/condorsw/host/apps/mxcam/mxcam boot $GEOSW_ROOT/condorsw/images/cacheboot/gc6500_cacheboot_534_fw.img memtest-tiled-768-1088.json $GEOSW_ROOT/condorsw/config/isp/sensor_ar0330_dsl216_le.bin

#Wait till camera is ready
#Change following accordingly for whatever is the correct video device for condorsw
VIDEODEV=0
while [ 1 ]
do
    if [ -c /dev/video$VIDEODEV ]
    then
        break;
    fi
    sleep 1;
done
sleep 2

START_FRAME_BUF=`$GEOSW_ROOT/condorsw/host/apps/mxcam/mxcam qcc 0x1 0x244 4 | awk '{print $1;}'` 
$GEOSW_ROOT/condorsw/host/tools/memrw/i686-condora0-usb.build/memrw 127 $START_FRAME_BUF -m w -f fbr_fbw_multi_pat_cbcr8080.bin

#Read back the downloaded frame for comparison
$GEOSW_ROOT/condorsw/host/tools/memrw/i686-condora0-usb.build//memrw 127 $START_FRAME_BUF -m r -d `expr 768 \* 1088` > check.bin
#The following comparison should pass except possibly for an earlier EOF on check.bin
cmp fbr_fbw_multi_pat_cbcr8080.bin check.bin

$GEOSW_ROOT/condorsw/host/apps/mxuvc/mxuvc stream --offset $VIDEODEV --stats --vout1 /dev/null

# To check the result while it's running (in another window), do:
# ./dump_memtest_regs.pl -run |& tee fbr_fbw_memtest_run.log
