#! /usr/bin/perl

$reg_cmd="$ENV{'GEOSW_ROOT'}/condorsw/host/apps/mxcam/mxcam qcc";
$block_id="0x1";

@reg_list = (
	{ name => "FBW_BUF_1_Luma"    , addr => "0x100" },
	{ name => "FBW_BUF_1_Chroma"  , addr => "0x104" },
	{ name => "FBW_BUF_2_Luma"    , addr => "0x108" },
	{ name => "FBW_BUF_2_Chroma"  , addr => "0x10c" },
	{ name => "Command"           , addr => "0x1f8" },
	{ name => "BufferCount"       , addr => "0x200" },
	{ name => "ExpectedData"      , addr => "0x204" },
	{ name => "ActualData"        , addr => "0x208" },
	{ name => "ErrorAddr"         , addr => "0x20c" },
	{ name => "NumErrors"         , addr => "0x240" },
	{ name => "FBR_BUF_Luma"      , addr => "0x244" },
	{ name => "FBR_BUF_Chroma"    , addr => "0x248" },
);

sub read_reg ($$ ) {
   my $name = shift @_;
   my $addr = shift @_;

   $reg_output = `$reg_cmd $block_id $addr 4`;
   chop $reg_output;
   ($value) = split(m/\s+/, $reg_output);
   #$value = oct($value);
   #print "reg_output = `$reg_output'\n";
   printf "Register %-20s = %10s ( %9d )\n", $name, $value, oct($value);
}

if (($#ARGV >= 0) && ($ARGV[0] eq "-run")) {
  while (1) {
      # kludgy, I know... quick and dirty
      foreach my $reg_struct ($reg_list[5], $reg_list[9]) {
         $name = $reg_struct->{name};
         $addr = $reg_struct->{addr};
         read_reg $name, $addr;
      }
     
      sleep 1;
  }
} else {
  foreach my $reg_struct (@reg_list) {
     $name = $reg_struct->{name};
     $addr = $reg_struct->{addr};
     read_reg $name, $addr;
  }
}
