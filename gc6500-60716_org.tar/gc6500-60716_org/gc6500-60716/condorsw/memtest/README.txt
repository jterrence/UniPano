There are two types of memory tests:

1. Integrated memory test in which we run the memory test along with the
   actual datapath.

2. Standalone test in which we do not run the actual data path and just do 
   memory testing.

The JIRA covering the memory tests is GEOCONDSW-1514.

======================
INTEGRATED MEMORY TEST
======================

In the JSON used for production datapath, add following key in system area:

"MEMTEST": "1"

If this key is not defined or "0", then memory test is not enabled.

How memory test results are reported:
-------------------------------------

Reports error results as follows:

1. Puts in dlog which can be retrieved using the  getlog tool. It reports:
   ErrorCode
   Real Data
   Expected Data
   Error Address

   The error codes are defined as follows:

   #define ERROR_FAILED_INCTEST                0xdead0001
   #define ERROR_FAILED_INC16TEST              0xdead0002
   #define ERROR_FAILED_ALL0_ALL1_TEST         0xdead0003
   #define ERROR_FAILED_ALTERNATING_TEST       0xdead0004
   #define ERROR_FAILED_ALTERNATING8_TEST      0xdead0005
   #define ERROR_FAILED_ALTERNATING16_TEST     0xdead0006
   #define ERROR_FAILED_WALKING0_TEST          0xdead0007
   #define ERROR_FAILED_WALKING1_TEST          0xdead0008
   #define ERROR_FAILED_INC16MODTEST           0xdead0009
   #define ERROR_FAILED_COMB32_TEST            0xdead000a
   #define ERROR_FAILED_WALKING01_TEST         0xdead000b

2. Puts results in registers which can be retrieved using "mxcam qcc" command.
   The details are:
   Buffer count:  mxcam qcc 0x1 0x200 4
   Expected Data: mxcam qcc 0x1 0x204 4
   Real data:     mxcam qcc 0x1 0x208 4
   Error Address: mxcam qcc 0x1 0x20c 4
   Error count:   mxcam qcc 0x1 0x240 4

======================
Standalone Memory Test
======================

This test uses a different video capture object "simcap" which gets data from
a memory location instead of sensor/isp. That mmeory location is filled
with a pattern.
Also "nvpp" object will have the following special configuration:
"Q_NVPP_CMP_MEMTEST": 1

General Description:
--------------------

Note that the way the standalone, FBR-FBW test is written really expects that
the problem is with Data Writes. We won't be able to distinguish between Data
Read and Write issues the way this test is written.

Theoretically, the problem could be in:

    Address
    Original frame download (write)
    FBR data read
    FBR data *write
    Firmware frame buffer data read of both source & dest. frames, for comparison

Let's skip the possible address issue - rarely happens.

The original frame download is checked before even starting the unit test using
an 'memrw write' followed by a 'memrw read' and compare. If a problem is detected
at this point, the frame can be read ('memrw') multiple times to see if you get
consistent results. If they are consistent, then the corruption probably happened
on the write; otherwise, it's a read issue.

With our existing hardware, we can't think of a way to distinguish whether the
FBR had a problem vs. the FBW.

Like the download, the firmware's frame buffer read & check could also be done
multiple times to determine whether it is the read that is a problem or whether
the frame buffer itself was corrupted.

However, rather than putting in this logic, it is easier for the time being just
to re-run the test with Write-only ODT turned on and see how that affects the error rate.

Setting up the tools and scripts according to desired setup:
------------------------------------------------------------

In JSON memtest-tiled-768-1088.json, set the board name correctly in "BOARD"
variable under "system". It should be "mobileyes5" for mobileyes5 board.

By default, the paths of the tools mxcam, mxuvc and memrw are used where
they are built from source as follows:

In GEO internal build:

cd $GEOSW_ROOT/condorsw
make host

In release:

cd $GEOSW_ROOT
make -C thirdparty minimal
cd $GEOSW_ROOT/condorsw
make -C host/lib/mxcam
make -C host/lib/mxuvc
make -C host/lib/qhal
make -C host/apps/mxcam
make -C host/apps/mxuvc
make -C host/tools/memrw

If for some reason, the paths used are different, then modify the paths of the 
tools in run.sh and dump_memtest_regs.pl.

The ISP bin file specified is $GEOSW_ROOT/condorsw/config/isp/sensor_ar0330_dsl216_le.bin.
No need to change this as ISP is not actually used in this test though the 
argument still needs to be provided for initialization.

The image and and ISP bin files are as used in release. Change accordingly if
local build within GEO is used.
 
Runing the test:
----------------


Switch user to root using "su root" and providing root password.

If you don't switch user to root then memrw, mxcam, and muxuvc will have to be
run with 'sudo'
In one window, run the simple script run.sh (or manually copy-and-paste the
commands from it into the shell to see output from individual commands)
    This sets up the test (boots firmware, loads pattern frame) and then simply
    kicks off 'mxuvc stream...'.
    As mentioned in general description, the original frame download is checked
    before even starting the unit test using an 'memrw write' followed by a
    'memrw read' and comparing using cmp command.
In another window (in root access mode), run the script to monitor the # of
frames and # of erors detected:

./dump_memtest_regs.pl -run |& tee fbr_fbw_memtest_run.log

Use sudo for above if not in root mode.

Sample output is:

Register BufferCount          =       0x16 (        22 )
Register NumErrors            =        0x0 (         0 )
Register BufferCount          =       0x20 (        32 )
Register NumErrors            =        0x0 (         0 )
Register BufferCount          =       0x2a (        42 )
Register NumErrors            =        0x0 (         0 )
Register BufferCount          =       0x34 (        52 )
Register NumErrors            =        0x0 (         0 )
Register BufferCount          =       0x3e (        62 )
Register NumErrors            =        0x0 (         0 )
Register BufferCount          =       0x48 (        72 )
Register NumErrors            =        0x0 (         0 )
Register BufferCount          =       0x52 (        82 )
Register NumErrors            =        0x0 (         0 )
Register BufferCount          =       0x5c (        92 )
Register NumErrors            =        0x0 (         0 )
Register BufferCount          =       0x66 (       102 )

Other than the above, results can also be obtained as follows:

1. From dlog which can be retrieved using the  getlog tool. It reports:
   ErrorCode: 0xdead1001
   Real Data
   Expected Data
   Error Address

2. Using "mxcam qcc" command.
   The details are:
   Buffer count:  mxcam qcc 0x1 0x200 4
   Expected Data: mxcam qcc 0x1 0x204 4
   Real data:     mxcam qcc 0x1 0x208 4
   Error Address: mxcam qcc 0x1 0x20c 4
   Error count:   mxcam qcc 0x1 0x240 4

If errors are reported, GEO will provide instructions on how to analyze which
type of errors are occurring.
