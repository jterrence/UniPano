#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================

# Libraries and packages
import subprocess
from subprocess import CalledProcessError
from sys import exit, argv, stderr
import platform
import os

if __name__ == "__main__":

    try:

        if len(argv) < 2:
            print ("Error: argument missing!")
            print ("Usage: readSensorEV.py <target-device> [<connection_params>]")
            exit(1)

        # Get target device
        target_device = argv[1]
        
        linux = platform.system() == 'Linux'

        if target_device == "gw4xx" or target_device == "gw3xx":

            # Get the connection type
            if len(argv) < 3:
                print ("Error: no connection parameters.")
                exit(1)

            connection_params = argv[2]

            print ("Reading gain and integration time...")

            binary = r'./bin/%s/isp_read_reg' % target_device if linux else r'./bin/%s/isp_read_reg.exe' % target_device

            direct_access = False
            if not os.path.isfile(binary):
                direct_access = True
                binary = r'./bin/%s/direct_access' % target_device if linux else r'./bin/%s/direct_access.exe' % target_device
                
            if not os.path.isfile(binary):
                if linux:
                    print >> stderr, "Error: isp_read_reg missing"
                else:
                    print >> stderr, "Error: isp_read_reg.exe missing"
                exit(1)

            connection_params = connection_params.split()        

            if direct_access:
                # Init
                command = [binary] + connection_params + ['40400074']
                p = subprocess.check_output(command)
                accessInit = p.split()[1]
    
                # Access board
                command = [binary] + connection_params + ['40400074', '808080']
                p = subprocess.check_output(command)
        
            # Read gain integer part
            command = [binary] + connection_params + ['40020B08']
            p = subprocess.check_output(command)
            gainInt = p.split()[1]
            
            # Read gain fractional part
            command = [binary] + connection_params + ['40020B2C']
            p = subprocess.check_output(command)
            gainFrac = p.split()[1]
        
            # Read integration time
            command = [binary] + connection_params + ['40020B04']
            p = subprocess.check_output(command)
            integrationTime = p.split()[1]

            if direct_access:
                # Deinit
                command = [binary] + connection_params + ['40400074', accessInit]
                p = subprocess.check_output(command)

        elif target_device == "gc65xx":
            
            print ("Reading gain and integration time...")
            
            if platform.system() == 'Windows':
                mxcam = r'./bin/%s/mxcam.exe'%target_device
            else:
                mxcam = r'./bin/%s/mxcam'%target_device

            # Read gain integer part
            command = [mxcam] + ['isprw'] + ['0B08']
            p = subprocess.check_output(command)
            if p.lower().find("no compatible device") > -1:
                print ("Error: %s" % p.lower())
                exit(1)
            elif p.lower().find("command is not supported") > -1:
                print ("Error: Device may not be booted")
                exit(1)
            gainInt = p.split()[1]
            
            # Read gain fractional part
            command = [mxcam] + ['isprw'] + ['0B2C']
            p = subprocess.check_output(command)
            gainFrac = p.split()[1]

            # Read integration time
            command = [mxcam] + ['isprw'] + ['0B04']
            p = subprocess.check_output(command)
            integrationTime = p.split()[1]

        else:
            print ("Error: unrecognized device.")
            exit(1)

    except CalledProcessError as cpe:
        print ("Error: return code: {0}, output: {1}".format(cpe.returncode, cpe.output))
        exit(1)

    # Print
    n = 2
    gainInt = "".join([gainInt[2:][i+n-1:i+n] for i in range(0, len(gainInt)-2, n)])
    gainProcessInt = int(gainInt, 16)
    gainFrac = "".join([gainFrac[2:][i+n-1:i+n] for i in range(0, len(gainFrac)-2, n)])
    gainProcessFrac = int(gainFrac, 16)/1000.0

    print "Gain (int): 0x{0} ({1})".format(gainInt, gainProcessInt)
    print "Gain (frac): ({0})".format(str(gainProcessInt + gainProcessFrac))

    integrationTimeProcess = "".join([integrationTime[2:][i+n-1:i+n] for i in range(0, len(integrationTime)-2, n)])
    integrationTimeProcessInt = str(int(integrationTimeProcess, 16))

    print "Integration time: 0x" + integrationTimeProcess + " (" + integrationTimeProcessInt + ")"
