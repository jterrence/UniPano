#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================

import sys
import platform
import subprocess

if __name__ == "__main__":
    device_type = sys.argv[1]
    
    shell_exec = platform.system() == 'Linux'
    
    if device_type == 'gw4xx':

        print "Connecting to dWarpcam..."

        if platform.system() == 'Windows':
            command = r'./bin/{0}/dWarpcam_RCV_setup.exe'.format(device_type)
        elif platform.system() == 'Linux':
            command = r'./bin/{0}/dWarpcam_RCV_setup'.format(device_type)
            
        if subprocess.call(command, shell=shell_exec) is not 0:
            sys.exit(1)
            
    elif device_type == 'gw3xx':
        
        print "Error: {0} currently does not support this feature".format(device_type)
        sys.exit(1)
            
    elif device_type == 'gc65xx':
        
        print "Error: {0} does not support this feature".format(device_type)
        sys.exit(1)
        
    else:
        print ("Error: unrecognized device.")
        sys.exit(1)

    sys.exit(0)
