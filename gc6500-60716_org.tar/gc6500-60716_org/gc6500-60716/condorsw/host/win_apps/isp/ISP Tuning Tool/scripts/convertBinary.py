#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================

import sys
import platform
import subprocess
from subprocess import CalledProcessError
import os

if __name__ == "__main__":
	device_type = sys.argv[1]
	json_path = sys.argv[2]
	verbosity_level = sys.argv[3]
	spec_file_path = sys.argv[4]
	bin_path = sys.argv[5]
	
	shell_exec = platform.system() == 'Linux'
	
	if device_type == 'gw4xx':
		
		command = r'python ./scripts/genbin.py "{3}" "{0}" be "{1}" -d {2} -v2'.format(json_path, bin_path, verbosity_level, spec_file_path)
		if subprocess.call(command, shell=shell_exec) is not 0:
			sys.exit(1)
			
	elif device_type == 'gw3xx':
		
		command = r'python ./scripts/genbin.py "{3}" "{0}" be "{1}" -d {2} -v1'.format(json_path, bin_path, verbosity_level, spec_file_path)
		if subprocess.call(command, shell=shell_exec) is not 0:
			sys.exit(1)
			
	elif device_type == 'gc65xx':
		
		command = r'python ./scripts/genbin.py "{3}" "{0}" le "{1}" -d {2}'.format(json_path, bin_path, verbosity_level, spec_file_path)
		if subprocess.call(command, shell=shell_exec) is not 0:
			sys.exit(1)

	else:
		print ("Error: unrecognized device.")
		sys.exit(1)

	sys.exit(0)
