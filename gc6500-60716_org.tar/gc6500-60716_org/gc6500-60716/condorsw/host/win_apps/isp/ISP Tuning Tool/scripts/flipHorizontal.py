#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================

import sys
import platform
import subprocess
from subprocess import CalledProcessError

if __name__ == "__main__":
    device_type = sys.argv[1]
    connection_params = sys.argv[2]
    
    shell_exec = platform.system() == 'Linux'
    
    if device_type in ['gw4xx', 'gw3xx']:

        if platform.system() == 'Windows':
            command = r'./bin/{0}/cfg_param.exe {1} 0x005F'.format(device_type, connection_params)
        elif platform.system() == 'Linux':
            command = r'./bin/{0}/cfg_param {1} 0x005F'.format(device_type, connection_params)
            
        try:
            init = subprocess.check_output(command, shell=shell_exec)
            last_piece = init.split()[-1]

            if last_piece == '0x00':
                last_piece = '0x01'
            elif last_piece == '0x01':
                last_piece = '0x00'
            elif last_piece == '0x02':
                last_piece = '0x03'
            elif last_piece == '0x03':
                last_piece = '0x02'
            else:
                print ("Error: invalid output '{0}'".format(last_piece))
                sys.exit(1)
            
            if platform.system() == 'Windows':
                command = r'./bin/{0}/cfg_param.exe {1} 0x005F {2}'.format(device_type, connection_params, last_piece)
            elif platform.system() == 'Linux':
                command = r'./bin/{0}/cfg_param {1} 0x005F {2}'.format(device_type, connection_params, last_piece)
            
            if subprocess.call(command, shell=shell_exec) is not 0:
                sys.exit(1)
            
        except CalledProcessError as cpe:
            print ("Error: return code: {0}, output: {1}".format(cpe.returncode, cpe.output))
            sys.exit(1)

    elif device_type == 'gc65xx':

        print "Error: {0} currently does not support this feature".format(device_type)
        sys.exit(1)
        
    else:
        print ("Error: unrecognized device.")
        sys.exit(1)

    sys.exit(0)
