#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================

# Libraries and packages
import argparse
import json
from libparam.param_config import ParamConfig
import genbin


def getJSONfromBinary(currentSpec, tblFile, verbosityLevel):
    # Load spec
    spec = genbin.load_spec(currentSpec, verbosityLevel)
    # Open binary table
    tbl = open(tblFile, "rb")
    # Open spec as Paramconfig object
    config = ParamConfig(spec)
    # Get data from binary file
    config.from_bin(tbl.read())
    # Get JSON string from binary
    jsonString = config.to_json()
    # Close binary file
    tbl.close()
    # Return JSON string
    return jsonString


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Get JSON from binary table.")
    parser.add_argument("-t", dest="table", required=True, type=str,
                        help="Path to input table")
    parser.add_argument("-s", dest="spec", required=True, type=str,
                        help="JSON spec file")
    parser.add_argument("-j", dest="jsonOut", required=True, type=str,
                        help="Output JSON file")
    parser.add_argument("-d", dest="verbosityLevel", required=False, type=int,
                        default=0,
                        help=argparse.SUPPRESS)
    args = parser.parse_args()

    # Get JSON string from binary table
    jsonFromTable = getJSONfromBinary(args.spec, args.table, args.verbosityLevel)
    # Add JSON string to JSON file
    f = open(args.jsonOut, "w")
    f.write("%s\n" % jsonFromTable)
    f.close()
