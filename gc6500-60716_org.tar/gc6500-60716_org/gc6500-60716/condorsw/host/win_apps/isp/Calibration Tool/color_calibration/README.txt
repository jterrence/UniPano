/*******************************************************************************
*
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this
* copyright notice.
*
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*******************************************************************************/

===================================
 'raw2rggb_4patches' Python script
===================================


++++++++++++++++
 Pre-requisites
++++++++++++++++

- Python 2.7


++++++++
 Syntax 
++++++++

   python raw2rggb_4patches.py
            -i INPUTFILE.raw -j CONFIG.json -c ISPCONFIG.json [-w]

where:
"-i INPUTFILE.raw": input raw image. Expectations: left and right sides should
have a color chart.

"-j INPUTCONFIG.json": input configuration file with coordinates of patches. Not
all coordinates for all patches need to be defined at the same time. Script
allows to define one, two, three, or four set of coordinates.

"-c ISPCONFIG.json": ISP JSON configuration file. If no
'dual_sensor_calibration' section is defined, section will be added. If
'dual_sensor_calibration' is present, values will be overwritten. (Note: format 
will be changed to standard JSON format. However, values will not be changed.)
If input file is not in JSON format, contents will be deleted and replaced with 
JSON format of results. If input file is empty, contents will be written in JSON
format.

"-w": (Optional) enable to display warnings. Disabled by default.


++++++++++++++++++++
 Configuration file
++++++++++++++++++++

--------------
 Example File
--------------

An example of the configuration file format is:
    {
        "inputRaw": {
            "width": 2688,
            "height": 1344,
            "bayerPattern": "grbg"
        },
        "left20Coordinates": {
            "horizontalStart": 441,
            "horizontalEnd": 499,
            "verticalStart": 494,
            "verticalEnd": 535
        },
        "left23Coordinates": {
            "horizontalStart": 1,
            "horizontalEnd": 200,
            "verticalStart": 1,
            "verticalEnd": 200
        },
        "right20Coordinates": {
            "horizontalStart": 200,
            "horizontalEnd": 599,
            "verticalStart": 200,
            "verticalEnd": 599
        },
        "right23Coordinates": {
            "horizontalStart": 1000,
            "horizontalEnd": 1500,
            "verticalStart": 1000,
            "verticalEnd": 1300
        },
        "displayInConsole": true
    }

'inputRaw' section:
    - width: width of input raw image.
    - height: height of input raw image.
    - bayerPattern: one of four Bayer patterns (rggb, bggr, grbg, gbrg) of raw
      image.

'left20Coordinates' section:
    - horizontalStart: horizontal start point of patch 20 for left sensor.
    - horizontalEnd: horizontal end point of patch 20 for left sensor.
    - verticalStart: vertical start point of patch 20 for left sensor.
    - verticalEnd: vertical end point of patch 20 for left sensor.
    +Note+: (1,1) is on the top left side of the image. Coordinates increase 
    from left to right and from top to bottom.

'left23Coordinates' section:
    Same as 'left20Coordinates' but for patch 23 of left sensor.

'right20Coordinates' section:
    Same as 'left20Coordinates' but for patch 20 of right sensor.

'right23Coordinates' section:
    Same as 'left20Coordinates' but for patch 23 of right sensor.


+++++++++++++++++
 ISP JSON syntax
+++++++++++++++++

On the ISP JSON file, to enable and use dual sensor color calibration, the
section "dual_sensor_calibration" should be added. An example is:

    "dual_sensor_calibration": {
        "enable": 1,
        "reference_rggb_patch20": [2004, 2535, 2572, 1586],
        "reference_rggb_patch23": [450, 534, 540, 397],
        "left_rggb_patch20": [1988, 2505, 2541, 1564], 
        "left_rggb_patch23": [454, 538, 544, 399], 
        "right_rggb_patch20": [2021, 2565, 2603, 1608], 
        "right_rggb_patch23": [447, 530, 537, 395]
    }

where:

"enable": enable or disable dual sensor color calibration and/or pedestal:
    0: do NOT use dual sensor color calibration or pedestal.
    1: use ONLY dual sensor color calibration.
    2: use BOTH dual sensor color calibration AND pedestal.

"reference_rggb_patch20":
    Reference RGGB mean values for patch 20.

"reference_rggb_patch23":
    Reference RGGB mean values for patch 23.

"left_rggb_patch20":
    RGGB mean values for patch 20 of left sensor.

"left_rggb_patch23":
    RGGB mean values for patch 23 of left sensor.

"right_rggb_patch20":
    RGGB mean values for patch 20 of right sensor.

"right_rggb_patch23":
    RGGB mean values for patch 23 of right sensor.