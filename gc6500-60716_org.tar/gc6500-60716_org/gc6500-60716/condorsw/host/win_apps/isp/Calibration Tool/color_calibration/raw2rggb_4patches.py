# /*****************************************************************************
# *
# * The content of this file or document is CONFIDENTIAL and PROPRIETARY
# * to GEO Semiconductor.  It is subject to the terms of a License Agreement
# * between Licensee and GEO Semiconductor, restricting among other things,
# * the use, reproduction, distribution and transfer.  Each of the embodiments,
# * including this information and any derivative work shall retain this
# * copyright notice.
# *
# * Copyright 2013-2016 GEO Semiconductor, Inc.
# * All rights reserved.
# ******************************************************************************

# Libraries and packages
import argparse
import sys
import struct
import json
from collections import OrderedDict


def validateDirection(configDict, patchName, direction, isDisplayWarning):
    # Define direction start/end
    directionStart = direction + 'Start'
    directionEnd = direction + 'End'

    isErrorFound = False
    if patchName not in configDict:
        if isDisplayWarning:
            print 'WARNING: ' + patchName + ' ' + direction + \
                  ' field not found.'
    else:
        if directionStart not in configDict[patchName] \
                or directionEnd not in configDict[patchName]:
            print 'ERROR: ' + patchName + ' ' + direction \
                  + ' start/end not found.'
            isErrorFound = True
        else:
            if configDict[patchName][directionEnd] \
                    < configDict[patchName][directionStart]:
                print 'ERROR: ' + patchName + ' ' + direction + \
                      ' end point incorrectly defined'
                isErrorFound = True
            if configDict[patchName][directionStart] > configDict['inputRaw']['width'] \
                    or configDict[patchName][directionStart] < 0 \
                    or configDict[patchName][directionEnd] > configDict['inputRaw']['width'] \
                    or configDict[patchName][directionEnd] < 0:
                print 'ERROR: ' + patchName + ' ' + direction + \
                      ' coordinates out of bounds'
                isErrorFound = True
            if config[patchName][directionStart] == 0:
                print 'ERROR: ' + patchName + ' ' + direction \
                      + ' starting coordinate is 0.'
                isErrorFound = True
            if config[patchName][directionStart] == config[patchName][directionEnd]:
                print 'ERROR: ' + patchName + ' ' + direction \
                      + ' start/end point are equal.'
                isErrorFound = True


    return isErrorFound


def validatePatchCoordinates(configDict, patchName, isDisplayWarning):
    # Check direction fields of patch
    isErrorFoundHorizontal = validateDirection(config, patchName, 'horizontal',
                                               isDisplayWarning)
    isErrorFoundVertical = validateDirection(config, patchName, 'vertical',
                                             isDisplayWarning)

    if isErrorFoundHorizontal or isErrorFoundVertical:
        isErrorFound = True
    else:
        isErrorFound = False

    return isErrorFound


def validateData(configDict, isDisplayWarning):
    isErrorFound = False

    # Validate input raw data
    if 'inputRaw' not in configDict:
        print 'ERROR: ''inputRaw'' field not found.'
        isErrorFound = True

    # Validate width and height
    if 'width' not in configDict['inputRaw'] \
            or 'height' not in configDict['inputRaw']:
        print 'ERROR: input raw width/height not found.'
        isErrorFound = True
    else:
        # Validate patch coordinates
        isErrorFoundLeft20 = validatePatchCoordinates(configDict,
                                                      'left20Coordinates',
                                                      isDisplayWarning)
        isErrorFoundLeft23 = validatePatchCoordinates(configDict,
                                                      'left23Coordinates',
                                                      isDisplayWarning)
        isErrorFoundRight20 = validatePatchCoordinates(configDict,
                                                       'right20Coordinates',
                                                       isDisplayWarning)
        isErrorFoundRight23 = validatePatchCoordinates(configDict,
                                                       'right23Coordinates',
                                                       isDisplayWarning)
        if isErrorFoundLeft20 or isErrorFoundLeft23 \
                or isErrorFoundRight20 or isErrorFoundRight23:
            print 'ERROR: Coordinates not found or incorrectly set.'
            isErrorFound = True

    # Validate Bayer pattern
    if 'bayerPattern' not in configDict['inputRaw']:
        print 'ERROR: Bayer pattern not found.'
        isErrorFound = True
    else:
        if configDict['inputRaw']['bayerPattern'] != 'rggb' \
                and configDict['inputRaw']['bayerPattern'] != 'gbrg' \
                and configDict['inputRaw']['bayerPattern'] != 'grbg' \
                and configDict['inputRaw']['bayerPattern'] != 'bggr':
            print 'ERROR: unknown Bayer pattern.'
            isErrorFound = True

    return isErrorFound


def fromBin(binaryFilePath, width):
    # Define formats
    uint16 = 'H'
    endianness = '>'

    # Set format value
    formatValue = endianness + uint16

    try:
        with open(binaryFilePath, mode='rb') as fid:
            binaryFile = fid.read()
    except IOError:
        print 'ERROR opening input raw file.'
        sys.exit()

    # Add contents to list
    contentsList = list()
    for x in xrange(0, len(binaryFile), 2):
        dummy = (struct.unpack(formatValue, binaryFile[x:x + 2])[0])
        contentsList.append(dummy)

    # Convert to 2D list
    twoDList = [contentsList[x:(x + width)]
                for x in xrange(0, len(contentsList), width)]

    return twoDList


def getSubList(inputList,
               horizontalStart, horizontalEnd,
               verticalStart, verticalEnd):
    # Use 1 index base (Matlab's)
    subList = [item[(horizontalStart-1):horizontalEnd]
               for item in inputList[(verticalStart-1):verticalEnd]]
    return subList


def getBayer(input2dList):
    # [[1,0],
    #  [0,0]]
    bayer1 = list()
    for y in xrange(0, len(input2dList), 2):
        dummy = input2dList[y][0::2]
        bayer1.extend(dummy)

    # [[0,1],
    #  [0,0]]
    bayer2 = list()
    for y in xrange(0, len(input2dList), 2):
        dummy = input2dList[y][1::2]
        bayer2.extend(dummy)

    # [[0,0],
    #  [1,0]]
    bayer3 = list()
    for y in xrange(1, len(input2dList), 2):
        dummy = input2dList[y][0::2]
        bayer3.extend(dummy)

    # [[0,0],
    #  [0,1]]
    bayer4 = list()
    for y in xrange(1, len(input2dList), 2):
        dummy = input2dList[y][1::2]
        bayer4.extend(dummy)

    return bayer1, bayer2, bayer3, bayer4


def getRGGB(rawList, resolution, patchCoordDict, bayerPattern):
    # Get width and height (adjust coordinates to match Matlab's)
    width = resolution[0]
    height = resolution[1]

    # Get horizontal coordinates
    horizontalCoordStart = patchCoordDict['horizontalStart']
    horizontalCoordEnd = patchCoordDict['horizontalEnd']

    # Get vertical coordinates
    verticalCoordStart = patchCoordDict['verticalStart']
    verticalCoordEnd = patchCoordDict['verticalEnd']

    # Get patch area
    patchValues = getSubList(rawList,
                             horizontalCoordStart, horizontalCoordEnd,
                             verticalCoordStart, verticalCoordEnd)
    # Create lists for single Bayer pattern
    bayer1, bayer2, bayer3, bayer4 = getBayer(patchValues)

    # Get mean values
    bayer1mean = sum(bayer1) / float(len(bayer1))
    bayer2mean = sum(bayer2) / float(len(bayer2))
    bayer3mean = sum(bayer3) / float(len(bayer3))
    bayer4mean = sum(bayer4) / float(len(bayer4))

    # Adjust RGGB pattern
    if verticalCoordStart % 2 == 0:
        if bayerPattern == 'rggb':
            bayerPattern = 'gbrg'
        elif bayerPattern == 'gbrg':
            bayerPattern = 'rggb'
        elif bayerPattern == 'grbg':
            bayerPattern = 'bggr'
        elif bayerPattern == 'bggr':
            bayerPattern = 'grbg'
    if horizontalCoordStart % 2 == 0:
        if bayerPattern == 'rggb':
            bayerPattern = 'grbg'
        elif bayerPattern == 'gbrg':
            bayerPattern = 'bggr'
        elif bayerPattern == 'grbg':
            bayerPattern = 'rggb'
        elif bayerPattern == 'bggr':
            bayerPattern = 'gbrg'

    # Add to output list
    if bayerPattern == 'rggb':
        outRGGBmean = [bayer1mean, bayer2mean, bayer3mean, bayer4mean]
    elif bayerPattern == 'gbrg':
        outRGGBmean = [bayer3mean, bayer4mean, bayer1mean, bayer2mean]
    elif bayerPattern == 'grbg':
        outRGGBmean = [bayer2mean, bayer1mean, bayer4mean, bayer3mean]
    elif bayerPattern == 'bggr':
        outRGGBmean = [bayer4mean, bayer3mean, bayer2mean, bayer1mean]
    else:
        print 'ERROR: unknown Bayer pattern.'
        sys.exit()

    # Round
    outRGGBmean = [int(round(x)) for x in outRGGBmean]

    return outRGGBmean


def addRGGBToDict(ispJSONDict, meanRGGBValuesDict, sidePatch):
    # Check if there are ratios
    if sidePatch not in meanRGGBValuesDict:
        return ispJSONDict
    # Display warning if section will be added
    elif sidePatch not in ispJSONDict['dual_sensor_calibration']:
        print 'WARNING: ''' + sidePatch + ' '' not in' \
              + ' ''dual_sensor_calibration'' section'
        print '         ''' + sidePatch + ' '' will be added'
    ispJSONDict['dual_sensor_calibration'][sidePatch] \
        = meanRGGBValuesDict[sidePatch]

    return ispJSONDict


def addToISPJSON(meanRGGBValuesDict, ispJSON, isDisplayWarning):
    isJSONFile = False
    # Try to open JSON file
    try:
        # Open ISP JSON file as dictionary
        with open(ispJSON, 'r') as fid:
            ispJSONDict = json.load(fid, object_pairs_hook=OrderedDict)
        isJSONFile = True
    except IOError:
        print 'ERROR opening output ISP JSON file.'
        sys.exit()
    except ValueError:
        if isDisplayWarning:
            print 'WARNING: File is not in JSON format. ' \
                  + 'Contents will be deleted.'
        ispJSONDict = dict()

    # Check if 'dual_sensor_calibration' is present
    if 'dual_sensor_calibration' not in ispJSONDict \
            or isJSONFile is False:
        # Display warning
        if isJSONFile is False and isDisplayWarning:
            print 'WARNING: "dual_sensor_calibration" section not found in ' \
                    + 'current JSON file.'
            print '         "dual_sensor_calibration" section will be added'
        # Create section
        ispJSONDict['dual_sensor_calibration'] = dict()
        # Add mean RGGB values
        if 'left_rggb_patch20' in meanRGGBValuesDict:
            ispJSONDict['dual_sensor_calibration']['left_rggb_patch20'] \
                = meanRGGBValuesDict['left_rggb_patch20']
        if 'left_rggb_patch23' in meanRGGBValuesDict:
            ispJSONDict['dual_sensor_calibration']['left_rggb_patch23'] \
                = meanRGGBValuesDict['left_rggb_patch23']
        if 'right_rggb_patch20' in meanRGGBValuesDict:
            ispJSONDict['dual_sensor_calibration']['right_rggb_patch20'] \
                = meanRGGBValuesDict['right_rggb_patch20']
        if 'right_rggb_patch23' in meanRGGBValuesDict:
            ispJSONDict['dual_sensor_calibration']['right_rggb_patch23'] \
                = meanRGGBValuesDict['right_rggb_patch23']
    else:
        # Check if fields are present
        # Left 20
        ispJSONDict = addRGGBToDict(ispJSONDict, meanRGGBValuesDict,
                                    'left_rggb_patch20')
        # Left 23
        ispJSONDict = addRGGBToDict(ispJSONDict, meanRGGBValuesDict,
                                    'left_rggb_patch23')
        # Right 20
        ispJSONDict = addRGGBToDict(ispJSONDict, meanRGGBValuesDict,
                                    'right_rggb_patch20')
        # Right 23
        ispJSONDict = addRGGBToDict(ispJSONDict, meanRGGBValuesDict,
                                    'right_rggb_patch23')

    # Overwrite current file
    with open(ispJSON, 'w') as fout:
        json.dump(ispJSONDict, fout,  indent=4, sort_keys=False)


if __name__ == '__main__':

    parser \
        = argparse.ArgumentParser(description='Get RGGB values from raw image.')
    parser.add_argument('-i', dest='inputRawFile', type=str, required=True,
                        help='Input raw file.')
    parser.add_argument('-j', dest='jsonConfig', type=str, required=True,
                        help='Input JSON parameters file.')
    parser.add_argument('-c', dest='ispJSONConfig', type=str, required=True,
                        help='ISP JSON config file.')
    parser.add_argument('-w', dest='isDisplayWarning', action='store_true',
                        required=False, help='Enable warnings display')
    args = parser.parse_args()

    # Load JSON configuration
    try:
        config = json.load(open(args.jsonConfig))
    except IOError:
        print 'ERROR opening JSON configuration file.'
        sys.exit()
    except ValueError:
        print 'ERROR decoding JSON configuration file. ' \
              'Please check JSON syntax.'
        sys.exit()

    # Validate data
    validation = validateData(config, args.isDisplayWarning)
    if validation:
        print 'ERROR: Errors on one or more fields. ' \
              + 'Please check configuration file.'
        sys.exit()

    # Extract from JSON configuration
    rawWidth = config['inputRaw']['width']
    rawHeight = config['inputRaw']['height']
    rawBayerPattern = config['inputRaw']['bayerPattern']

    # Convert binary file to list
    binaryList = fromBin(args.inputRawFile, rawWidth)

    # Dict to hold mean RGGB values
    meanRGGBValues = dict()
    # Flag to track coordinates
    isCoordinateLeft20Set = False
    isCoordinateLeft23Set = False
    isCoordinateRight20Set = False
    isCoordinateRight23Set = False

    # Left 20
    if 'left20Coordinates' in config:
        meanRGGBValues['left_rggb_patch20'] \
            = getRGGB(binaryList,
                      [rawWidth, rawHeight],
                      config['left20Coordinates'],
                      rawBayerPattern)
        isCoordinateLeft20Set = True
    # Left 23
    if 'left23Coordinates' in config:
        meanRGGBValues['left_rggb_patch23'] \
            = getRGGB(binaryList,
                      [rawWidth, rawHeight],
                      config['left23Coordinates'],
                      rawBayerPattern)
        isCoordinateLeft23Set = True
    # Right 20
    if 'right20Coordinates' in config:
        meanRGGBValues['right_rggb_patch20'] \
            = getRGGB(binaryList,
                      [rawWidth, rawHeight],
                      config['right20Coordinates'],
                      rawBayerPattern)
        isCoordinateRight20Set = True
    # Right 23
    if 'right23Coordinates' in config:
        meanRGGBValues['right_rggb_patch23'] \
            = getRGGB(binaryList,
                      [rawWidth, rawHeight],
                      config['right23Coordinates'],
                      rawBayerPattern)
        isCoordinateRight23Set = True

    # Check if all coordinates have been set
    isCoordinateAllSet = True
    if not isCoordinateLeft20Set and not isCoordinateLeft23Set \
        and not isCoordinateRight20Set and not isCoordinateRight23Set:
        isCoordinateAllSet = False

    # Display warning if no coordinate is set and do not add to ISP JSON config
    if not isCoordinateAllSet:
        print 'ERROR: Input jsonConfig ' + args.jsonConfig \
              + ' does not have coordinates!'
        sys.exit()
    else:
        # Add to ISP JSON config file
        addToISPJSON(meanRGGBValues, args.ispJSONConfig, args.isDisplayWarning)

    # Check if results should be displayed
    if 'displayInConsole' in config and config['displayInConsole'] is True:
        if 'left20Coordinates' in config and isCoordinateLeft20Set:
            print 'Left 20 [R, Gr, Gb, B] = ' \
                  + str(meanRGGBValues['left_rggb_patch20'])
        if 'left23Coordinates' in config and isCoordinateLeft23Set:
            print 'Left 23 [R, Gr, Gb, B] = ' \
                  + str(meanRGGBValues['left_rggb_patch23'])
        if 'right20Coordinates' in config and isCoordinateRight20Set:
            print 'Right 20 [R, Gr, Gb, B] = ' \
                  + str(meanRGGBValues['right_rggb_patch20'])
        if 'right23Coordinates' in config and isCoordinateRight23Set:
            print 'Right 23 [R, Gr, Gb, B] = ' \
                  + str(meanRGGBValues['right_rggb_patch23'])

    print 'Done!'
