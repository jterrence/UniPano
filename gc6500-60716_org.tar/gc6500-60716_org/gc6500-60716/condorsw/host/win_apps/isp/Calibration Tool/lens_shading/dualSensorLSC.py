'''
/*******************************************************************************
*
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this
* copyright notice.
*
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*******************************************************************************/
'''

import argparse
from dualSensorLSCClass import dualSensorLSC


if __name__ == '__main__':

    # Help message for '--inputRaw'
    helpInputRaw = '''Input raw file(s). For a single file with left and right
        combined horizontally, specify only one file. For two files, specify
        first capture with left side and then capture with right side.
        '''

    parser = argparse.ArgumentParser(description='Lens shading correction for '
                                                 'dual sensors.')
    parser.add_argument('-i', dest='inputRaw', nargs='+', type=str,
                        required=True, help=helpInputRaw)
    parser.add_argument('-j', dest='jsonConfig', type=str, required=True,
                        help='Input JSON parameters file.')
    args = parser.parse_args()

    # Initialize class
    lsc = dualSensorLSC(args.inputRaw, args.jsonConfig)

    # Process left side
    meshRGBLeft = lsc.processSide('left')
    # Process right side
    meshRGBRight = lsc.processSide('right')

    # Save to output file
    if lsc.misc['save']:
        lsc.saveToFile(meshRGBLeft, meshRGBRight)
