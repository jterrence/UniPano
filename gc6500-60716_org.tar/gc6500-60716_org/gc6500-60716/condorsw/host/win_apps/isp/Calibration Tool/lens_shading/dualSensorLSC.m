%{
/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
%}

%%
function dualSensorLSC(inputRaw, configJSON)
% Initialization

% Read JSON file with arguments and add it to a Matlab structure
% (Note: to parse a JSON file, the toolbox JSONlab is used. Available at:
% http://www.mathworks.com/matlabcentral/fileexchange/33381-jsonlab--a-toolbox-
% to-encode-decode-json-files )
configStruct = loadjson(configJSON);

% Check endianness
if strcmp(configStruct.info.endianness, 'be')
    configStruct.info.endianness = 'ieee-be';
elseif strcmp(configStruct.info.endianness, 'le')
    configStruct.info.endianness = 'ieee-le';
else
    error('Endianness not recognized, please use ''be'' or ''le''');
end

% Assuming raw capture is two sensor horizontally merged, divide lens shading
% input width by 2
if mod(configStruct.lensShading.meshSize(1),2) == 0
    configStruct.lensShading.meshSize(1) = configStruct.lensShading.meshSize(1)/2;
else
    error('Cannot use current lens shading mesh width. Please use even number');
end

% To Apical format
info.bayer_type = configStruct.info.bayerType;
info.header_size_in_bytes = configStruct.info.headerSize; 
info.res = configStruct.info.resolution;
info.bit_depth = configStruct.info.bitDepth;
info.endianness = configStruct.info.endianness;
info.BLACK_LEVEL = configStruct.info.blackLevels;

% Read raw file
fid = fopen(inputRaw,'r');
header = char(fread(fid, info.header_size_in_bytes)');
raw = fread(fid,[info.res(1) info.res(2)],'uint16=>uint16',info.endianness);
fclose(fid);
raw=(raw(1:info.res(1),:));
raw = single(raw);
% Transpose to keep row/column from original capture
% 'rawCircle' is 32 rows by 64 columns
rawCircle=raw';

% Process left
rawLeft = rawCircle(:,1:info.res(1)/2);
meshRGBLeft = processSide(rawLeft, configStruct);
% Process right
rawRight = rawCircle(:,info.res(1)/2 + 1:end);
meshRGBRight = processSide(rawRight, configStruct);

% Check for normalization
if configStruct.normalization.enable
    [meshRGBLeft, meshRGBRight] = ...
        normalizeLSC(meshRGBLeft, meshRGBRight, configStruct.normalization);
end

% Save to file
if configStruct.misc.save
    if ~isfield(configStruct.misc, 'outFileName') ...
            || isempty(configStruct.misc.outFileName)
        % If no output JSON file name is given, use date time and input raw
        % file name as output JSON file name.
        fileName = ...
            [datestr(now, 'yyyymmdd_HHMMSS') '_' inputRaw(1:end-4) '.json'];
    else
        fileName = configStruct.misc.outFileName;
    end
    saveToFile(fileName, meshRGBLeft, meshRGBRight, configStruct)
end

end


%%
function meshRGB = processSide(raw, configStruct)

% To Apical format
% 'info'
info.bayer_type = configStruct.info.bayerType;
info.header_size_in_bytes = configStruct.info.headerSize; 
info.res = configStruct.info.resolution;
info.bit_depth = configStruct.info.bitDepth;
info.endianness = configStruct.info.endianness;
info.BLACK_LEVEL = configStruct.info.blackLevels;
% 'settings'
settings.mesh_size         = [configStruct.lensShading.meshSize(2) ...
                              configStruct.lensShading.meshSize(1)];
settings.luma_strength     = configStruct.lensShading.lumaStrength;
settings.chroma_strength   = configStruct.lensShading.chromaStrength;
settings.input_mesh_scale  = configStruct.lensShading.scale;

% Add to function struct
preProc = configStruct.pseudoRadialExpansion;

% Check for pseudo radial expansion
if configStruct.pseudoRadialExpansion.enable
    rawExt = pseudoRadialExpansion(raw, preProc.pixelThreshold,...
                                   preProc.distanceThreshold,...
                                   preProc.depth, configStruct.misc.display);
    % Get mesh table
    meshRGB = calibrate_shading_mesh(rawExt, info, settings);
else
    % Get mesh table
    meshRGB = calibrate_shading_mesh(raw, info, settings);
end

% Display, if necessary
if configStruct.misc.display
    % Correct LSC
    rgb = correct_black_level(raw,info,info.BLACK_LEVEL)./(2^info.bit_depth-1);
    rgbCorrected = correct_shading_mesh(rgb, info, meshRGB, settings.input_mesh_scale);
    % Plot
    figure;imagesc(rgb);axis square;colorbar;
    set(gca,'LooseInset',get(gca,'TightInset'));
    cl = caxis;
    figure;imagesc(rgbCorrected,cl);axis square;colorbar;
    set(gca,'LooseInset',get(gca,'TightInset'));
end

end


%%
function outImg = pseudoRadialExpansion(varargin)
% Input:
%   - img: input image.
%   - pixelThreshold: pixel threshold value to use. Default 600.
%   - distanceThreshold: distance from 'pixelThreshold' location to use for
%     selecting pixel value to replace current row/column. Default 50.
%   - depth: distance from 'pixelThreshold' location to use as end location
%     for replacing row/column values. Value should be less or equal to
%     'distanceThreshold'. Default 30.
%   - displayFigures: display figures. Default 'false'.
%
% Output:
%   - outImg: processed image.

if length(varargin) == 1
    img = varargin{1};
    pixelThreshold = 600;
    distanceThreshold = 50;
    depth = 30;
    displayFigures = false;
elseif length(varargin) == 2
    img = varargin{1};
    pixelThreshold = varargin{2};
    distanceThreshold = 50;
    depth = 30;
    displayFigures = false;
elseif length(varargin) == 3
    img = varargin{1};
    pixelThreshold = varargin{2};
    distanceThreshold = varargin{3};
    depth = 30;
    displayFigures = false;
elseif length(varargin) == 4
    img = varargin{1};
    pixelThreshold = varargin{2};
    distanceThreshold = varargin{3};
    depth = varargin{4};
    displayFigures = false;
elseif length(varargin) == 5
    img = varargin{1};
    pixelThreshold = varargin{2};
    distanceThreshold = varargin{3};
    depth = varargin{4};
    displayFigures = varargin{5};
else
    error('Too many input arguments.');
end

% Check if 'depth' is less than or equal to 'pixelThreshold'
if depth > distanceThreshold
    error(['''depth'' (' num2str(depth) ') should be less or equal to'...
           '''distanceThreshold'' (' num2str(distanceThreshold) ')']);
end

% Assuming image is already centered, calculate start/end points
r = size(img,1)/2;
rowStart = round(r - (sqrt(2)/2 * r));
rowEnd = round(r + (sqrt(2)/2 * r));
colStart = round(r - (sqrt(2)/2 * r));
colEnd = round(r + (sqrt(2)/2 * r));
% Process row (left side)
outImg = rowProcessFull(img, pixelThreshold, distanceThreshold,...
                        rowStart:rowEnd, depth, 'left2right');
if displayFigures
    figure;imagesc(img);axis square
    cl = caxis;
    figure;imagesc(outImg, cl);axis square
    % cl = caxis;
end
% Process row (right side)
outImg = rowProcessFull(outImg, pixelThreshold, distanceThreshold,...
                        rowStart:rowEnd, depth, 'right2left');
if displayFigures
    figure;imagesc(outImg, cl);axis square
end
% Process column (upper side)
outImg = colProcessFull(outImg, pixelThreshold, distanceThreshold, ...
                        colStart:colEnd, depth, 'ascending');
if displayFigures
    figure;imagesc(outImg, cl);axis square
end
% Process column (bottom side)
outImg = colProcessFull(outImg, pixelThreshold, distanceThreshold, ...
                        colStart:colEnd, depth, 'descending');
if displayFigures
    figure;imagesc(outImg, cl);axis square
end
% Now start from the beginning
outImg = rowProcessFull(outImg, pixelThreshold, distanceThreshold,...
                        1:size(img,2), depth, 'left2right');
outImg = rowProcessFull(outImg, pixelThreshold, distanceThreshold,...
                        1:size(img,2), depth, 'right2left');
if displayFigures
    figure;imagesc(outImg, cl);axis square
end

end


%%
function outRP = rowProcessFull(imgIn, pixelThreshold, distanceThreshold,...
                                rowRange, depth, direction)
% Check if image should be flipped
if strcmp(direction,'right2left')
    imgIn = fliplr(imgIn);
end
% Create output array
outRP = imgIn;

% Iterate over rows
for row = rowRange
    % Find location where values are less than threshold
    [~, loc] = find(imgIn(row,1:end/2) <= pixelThreshold);
    % Check if last location plus distance threshold goes out of bounds
    if ~isempty(loc) && (loc(end) + distanceThreshold) < size(imgIn,2)
        if mod(loc(end)+distanceThreshold,2) == 0
            outRP(row,2:2:(loc(end)+depth)) = imgIn(row,loc(end)+distanceThreshold);
            outRP(row,1:2:(loc(end)+depth)) = imgIn(row,loc(end)+distanceThreshold+1);
        else
            outRP(row,2:2:(loc(end)+depth)) = imgIn(row,loc(end)+distanceThreshold+1);
            outRP(row,1:2:(loc(end)+depth)) = imgIn(row,loc(end)+distanceThreshold);
        end
    end
end

% Check if image should be flipped back
if strcmp(direction,'right2left')
    outRP = fliplr(outRP);
end

end


%%
function outRP = colProcessFull(imgIn, pixelThreshold, distanceThreshold,...
                                colRange, depth, direction)
% Check if image should be flipped
if strcmp(direction,'descending')
    imgIn = flipud(imgIn);
end
% Create output array
outRP = imgIn;


% Iterate over columns
for col = colRange
    % Find location where values are less than threshold
    [loc, ~] = find(imgIn(1:end/2,col) <= pixelThreshold);
    % Check if last location plus distance threshold goes out of bounds
    if ~isempty(loc) && (loc(end) + distanceThreshold) > 1
        if mod(loc(end)+distanceThreshold,2) == 0
            outRP(2:2:(loc(end)+depth),col) = imgIn(loc(end)+distanceThreshold,col);
            outRP(1:2:(loc(end)+depth),col) = imgIn(loc(end)+distanceThreshold+1,col);
        else
            outRP(2:2:(loc(end)+depth),col) = imgIn(loc(end)+distanceThreshold+1,col);
            outRP(1:2:(loc(end)+depth),col) = imgIn(loc(end)+distanceThreshold,col);
        end
    end
end

% Check if image should be flipped back
if strcmp(direction,'descending')
    outRP = flipud(outRP);
end

end


%%
function [outMeshLeft, outMeshRight] ...
    = normalizeLSC(meshRGBLeft, meshRGBRight, normStruct)
% Input:
%   - meshRGBLeft: input mesh RGB array for left sensor.
%   - meshRGBRight: input mesh RGB array for right sensor.
%   - normStruct: structure with:
%       - corr: correction factor. Default 0.011.
%       - rgRatio: R/G correction ratio. Default 0.5.
%       - bgRatio: B/G correction ratio. Default 0.8.
%       - leftGain: left-side gain. Default 1.125.
%       - rightGain: right-side gain. Default 1.000.
%
% Output:
%   - outMesh: post-processed lens shading correction table.

% Correction
if ~isfield(normStruct, 'corr')
    corr = 0.011;
else
    corr = normStruct.corr;
end

% R/G ratio
if ~isfield(normStruct, 'rgRatio')
    rgRatio = 0.5;
else
    rgRatio = normStruct.rgRatio;
end

% B/G ratio
if ~isfield(normStruct, 'bgRatio')
    bgRatio = 0.5;
else
    bgRatio = normStruct.bgRatio;
end

% Left gain
if ~isfield(normStruct, 'leftGain')
    leftGain = 0.5;
else
    leftGain = normStruct.leftGain;
end

% Right gain
if ~isfield(normStruct, 'rightGain')
    rightGain = 0.5;
else
    rightGain = normStruct.rightGain;
end

% Check sizes of mesh tables
if size(meshRGBLeft) ~= size(meshRGBRight)
    error('Incorrect size for mesh arrays!');
end
% Check size of plane
if size(meshRGBLeft,3) ~= 3 || size(meshRGBRight,3) ~= 3
    error('Mesh array is not of size 3!');
end

% Calculate corrections
rCorr = corr * rgRatio;
gCorr = corr;
bCorr = corr * bgRatio;

% Process each side
outMeshLeft = normalizeSide(meshRGBLeft, rCorr, gCorr, bCorr, leftGain);
outMeshRight = normalizeSide(meshRGBRight, rCorr, gCorr, bCorr, rightGain);

end


%%
function outMeshRGB = normalizeSide(meshRGB, rCorr, gCorr, bCorr, sideGain)
% Input:
%   - meshRGB: input mesh RGB array for any side.
%   - rCorr: calculated R correction.
%   - gCorr: calculated G correction.
%   - bCorr: calculated B correction.
%   - sideGain: current side gain
%
% Output:
%   - outMeshRGB: post-processed side.

% Create output array
outMeshRGB = zeros(size(meshRGB));

% Process each plane
outMeshRGB(:,:,1) = arrayfun(@(x) ...
    max(32, floor(x - (rCorr * power(x-32, 2)))) * sideGain, meshRGB(:,:,1));
outMeshRGB(:,:,2) = arrayfun(@(x) ...
    max(32, floor(x - (gCorr * power(x-32, 2)))) * sideGain, meshRGB(:,:,2));
outMeshRGB(:,:,3) = arrayfun(@(x) ...
    max(32, floor(x - (bCorr * power(x-32, 2)))) * sideGain, meshRGB(:,:,3));

% Round
outMeshRGB = round(outMeshRGB);

end


%%
function saveToFile(fileName, meshRGBLeft, meshRGBRight, configStruct)

% Add to function struct
lensShading = configStruct.lensShading;
pre = configStruct.pseudoRadialExpansion;
normHB = configStruct.normalization;

if ~isfield(configStruct.misc, 'saveMeshOnly') ...
        || isempty(configStruct.misc.saveMeshOnly)
    configStruct.misc.saveMeshOnly = 0;
end

% To output file
fID = fopen(fileName,'w');
fprintf(fID, '{\n');
% Write pseudo-radial expansion data
if ~configStruct.misc.saveMeshOnly && pre.enable
    fprintf(fID, '    "pseudoRadialExpansion": {\n');
    fprintf(fID, '        "pixelThreshold": %d,\n', pre.pixelThreshold);
    fprintf(fID, '        "distanceThreshold": %d,\n', pre.distanceThreshold);
    fprintf(fID, '        "depth": %d\n', pre.depth);
    fprintf(fID, '    },\n');
end
% Write normalization data
if ~configStruct.misc.saveMeshOnly && normHB.enable
    fprintf(fID, '    "normalization": {\n');
    fprintf(fID, '        "corr": %f,\n', normHB.corr);
    fprintf(fID, '        "rgRatio": %f,\n', normHB.rgRatio);
    fprintf(fID, '        "bgRatio": %f,\n', normHB.bgRatio);
    fprintf(fID, '        "leftGain": %f,\n', normHB.leftGain);
    fprintf(fID, '        "rightGain": %f\n', normHB.rightGain);
    fprintf(fID, '    },\n');
end
% Write LSC data
if ~configStruct.misc.saveMeshOnly
    fprintf(fID, '    "lensShadingStrength": {\n');
    fprintf(fID, '        "lumaStrength": %d,\n', lensShading.lumaStrength);
    fprintf(fID, '        "chromaStrength": %d\n', lensShading.chromaStrength);
    fprintf(fID, '    },\n');
end
% Write ISP JSON data
fprintf(fID, '    "lens_shading": {\n');
fprintf(fID, '        "enable": 1,\n');
fprintf(fID, '        "num_lens_shading_tables": 1,\n');
fprintf(fID, '        "mesh_width": %d,\n', lensShading.meshSize(1) * 2);
fprintf(fID, '        "mesh_height": %d,\n', lensShading.meshSize(2));
fprintf(fID, '        "mesh_scale": %d,\n', lensShading.scale);
for plane = 1:size(meshRGBLeft,3)
    if plane == 1
        fprintf(fID, '        "mesh_medium_tbl_r": [\n');
    elseif plane == 2
        fprintf(fID, '        "mesh_medium_tbl_g": [\n');
    elseif plane == 3
        fprintf(fID, '        "mesh_medium_tbl_b": [\n');
    end
    for row = 1:size(meshRGBLeft,1)
        fprintf(fID, '            ');
        fprintf(fID, '%d, ', meshRGBLeft(row,:,plane));
        if row == size(meshRGBLeft,1)
            fprintf(fID, '%d, ', meshRGBRight(row,1:end-1,plane));
            fprintf(fID, '%d', meshRGBRight(row,end,plane));
        else
            fprintf(fID, '%d, ', meshRGBRight(row,:,plane));
        end
        fprintf(fID, '\n');
    end
    if plane == size(meshRGBLeft,3)
        fprintf(fID, '        ]\n');
    else
        fprintf(fID, '        ],\n');
    end
end
fprintf(fID, '    }\n');
fprintf(fID, '}');
fclose(fID);

end