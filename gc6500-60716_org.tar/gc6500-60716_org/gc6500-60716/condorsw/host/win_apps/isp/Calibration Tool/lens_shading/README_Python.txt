/*******************************************************************************
*
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this
* copyright notice.
*
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*******************************************************************************/

==================================
 'dualSensorLSC.py' Python script
==================================


++++++++++++++++
 Pre-requisites
++++++++++++++++

- Python 2.7.x.
- The following Python packages:
    - numpy
    - scipy
    - skimage
    - See https://packaging.python.org/en/latest/installing/ for information on 
      how to install packages in Python.
- Python class for dual sensor lens shading correction: 'dualSensorLSCClass.py'.

- Versions tested:
    - Python 2.7.5 on Fedora 20 with:
        - numpy 1.8.2
        - scipy 0.12.1
        - skimage 0.10.1
    - Python 2.7.10 on Windows 7 with:
        - numpy 1.11.0
        - scipy 0.17.1
        - skimage 0.12.3


++++++++
 Syntax 
++++++++

    python dualSensorLSC.py -i INPUTFILE.raw [INPUTFILE.raw] -j CONFIG.json

where:
"-i INPUTFILE.raw [INPUTFILE.raw]": input raw image(s). For a single file with
left and right combined horizontally, specify only one file. Expectations for a
single file is left and right combined horizontally, and left and right
centered. For two files, specify first capture with left side and then capture
with right side. Only two raw files are supported.

"-j CONFIG.json": configuration file in JSON format.

Example for single file:
    python dualSensorLSC.py -i SINGLE.raw -j CONFIG.json

Example for two files:
    python dualSensorLSC.py -i LEFT.raw RIGHT.raw -j CONFIG.json


++++++++++++++++++++
 Configuration file
++++++++++++++++++++

--------------
 Example File
--------------

An example of the JSON format is:
    {
        "info": {
            "bayerType": "grbg",
            "headerSize": 0,
            "resolution": [
                2688,
                1344
            ],
            "bitDepth": 12,
            "endianness": "be",
            "blackLevels": [
                168,
                168,
                168,
                168
            ]
        },
        "lensShading": {
            "meshSize": [
                64,
                32
            ],
            "lumaStrength": 100,
            "chromaStrength": 0,
            "scale": 2
        },
        "pseudoRadialExpansion": {
            "enable": 1,
            "pixelThreshold": 800,
            "distanceThreshold": 5,
            "depth": 5
        },
        "normalization": {
            "enable": 0,
            "corr": 0.011,
            "rgRatio": 0.58,
            "bgRatio": 0.89,
            "leftGain": 1.000,
            "rightGain": 1.000
        },
        "misc": {
            "display": 0,
            "save": 1,
            "saveMeshOnly": 1,
            "outFileName": ""
        }
    }

'info' section:
    - bayerType: one of four Bayer patterns (rggb, bggr, grbg, gbrg) of raw
      capture.
    - headerSize: if raw capture has header, header size in bytes. For GC65XX
      raw captures header size is usually 0.
    - resolution: width and height of input raw capture.
    - bitDepth: bit depth of input raw capture.
    - endianness: endianness of input raw capture ("be" or "le"). For GC65XX,
      endianness is usually "be".
    - blackLevels: black levels for R, Gr, Gb, and B.

'lensShading' section:
    - meshSize: width and height of whole image. Since raw capture is expected
      to be horizontally merged, width will be divided by two to compute each
      lens shading table side individually. If this is not the case, an error
      will be displayed.
    - lumaStrength: luma strength for lens shading. (Typical value used: 100)
    - chromaStrength: chroma strength for lens shading.
    - scale: scale for lens shading.
        --------------------------------------------
        | scale |          Format       | Max gain |
        --------------------------------------------
        |   0   | Unsigned Fraction 1.7 |   x 2    |
        |   1   | Unsigned Fraction 2.6 |   x 4    |
        |   2   | Unsigned Fraction 3.5 |   x 8    |
        |   3   | Unsigned Fraction 4.4 |   x 16   |

'pseudoRadialExpansion' section:
    - enable: enable or disable Pseudo-radial Expansion. (Typical value used: 1)
    - pixelThreshold: pixel threshold value to use. Default 600. (Typical value
      used: 800)
    - distanceThreshold: distance from 'pixelThreshold' location to use for
      selecting pixel value to replace current row/column. Default 50. (Typical
      value used: 5)
    - depth: distance from 'pixelThreshold' location to use as end location for
      replacing row/column values. Value should be less or equal to
      'distanceThreshold'. Default 30. (Typical value used: 5)

'normalization' section: 
    - NOTE: section currently not supported in the Python version.
    - enable: enable or disable 'normalization' post-processing (merged from
      Excel spreadsheet).
    - corr: correction value. (Typical value used: 0.011)
    - rgRatio: R/G ratio. (Typical value used: 0.58)
    - bGRatio: B/G ratio. (Typical value used: 0.89)
    - leftGain: left shading table gain. (Typical value used: 1.000)
    - rightGain: right shading table gain. (Typical value used: 1.000)

'misc' section:
    - display: enable or disable displaying figures for validation. (Note:
      currently not supported in Python version.)
    - save: save result to JSON file.
    - saveMeshOnly: set to 1 to only save lens shading mesh data. Set to 0 to
      save configuration data as well.
    - outFileName: file name of JSON file. If this field is not specified or if
      it is empty, current date and time plus input file name will be used.


+++++++++++++++++
 ISP JSON syntax
+++++++++++++++++

On the ISP JSON file, to enable and use lens shading, the
section "lens_shading" should be added. An example is:

    "lens_shading": {
        "enable": 1,
        "mesh_width": 64,
        "mesh_height": 32,
        "mesh_scale": 3,
        "num_lens_shading_tables": 1,
        "mesh_medium_tbl_r": [
            31, 31, 31, 31, 32, 32, 32, 32, 32, 31, 30, 29, 28, 27, 25, 25, 25, 
            ...
        ],
        "mesh_medium_tbl_g": [
            31, 31, 31, 31, 32, 32, 32, 32, 32, 31, 30, 29, 28, 27, 25, 25, 25, 
            ...
        ], 
        "mesh_medium_tbl_b": [
            31, 31, 31, 31, 32, 32, 32, 32, 32, 31, 30, 29, 28, 27, 25, 25, 25, 
            ...
        ]
    }

where:

"enable": enable or disable lens shading correction.
    0: do NOT use lens shading
    1: use lens shading

"mesh_width" and "mesh_height": full width and height of lens shading table. For
example, if the lens shading table for each side is 32 width x 32 height, then 
the full lens shading table size is 64 width x 32 height.

"mesh_scale": scale of lens shading table, please refer to the following table
    --------------------------------------------
    | scale |          Format       | Max gain |
    --------------------------------------------
    |   0   | Unsigned Fraction 1.7 |   x 2    |
    |   1   | Unsigned Fraction 2.6 |   x 4    |
    |   2   | Unsigned Fraction 3.5 |   x 8    |
    |   3   | Unsigned Fraction 4.4 |   x 16   |

"num_lens_shading_tables": MUST be set to 1.

"mesh_medium_tbl_r": lens shading table for R channel.

"mesh_medium_tbl_g": lens shading table for G channel.

"mesh_medium_tbl_b": lens shading table for B channel.


NOTE: values for "mesh_width", "mesh_height", and "mesh_scale" MUST match the 
values set for lens shading computation (see section 'Configuration file').