%{
/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
%}

function raw = correct_shading_mesh(raw, info, shading_LUT,mesh_scale_mode)

switch mesh_scale_mode
    case 0
        mesh_scale = 128;
    case 1
        mesh_scale = 64;
    case 2
        mesh_scale = 32;    
    case 3
        mesh_scale = 16;
end

rgb_ = (1/(shading_LUT/mesh_scale));
rgb = imresize(rgb_, [size(raw,1) size(raw,2)], 'bilinear','AntiAliasing',false);
raw_gainmap = raw.*0;
rgbsh = imresize(rgb_, [size(raw,1) size(raw,2)], 'bilinear','AntiAliasing',false);
rgbin=rgb;
rgb_corr=rgbin./rgbsh;

switch info.bayer_type
    case {1,'rggb'}
        raw_gainmap(1:2:end,1:2:end)=(rgb(1:2:end,1:2:end,1));
        raw_gainmap(1:2:end,2:2:end)=(rgb(1:2:end,2:2:end,2));
        raw_gainmap(2:2:end,1:2:end)=(rgb(2:2:end,1:2:end,2));
        raw_gainmap(2:2:end,2:2:end)=(rgb(2:2:end,2:2:end,3));
    case {2,'grbg'}
        raw_gainmap(1:2:end,2:2:end)=(rgb(1:2:end,2:2:end,1));
        raw_gainmap(1:2:end,1:2:end)=(rgb(1:2:end,1:2:end,2));
        raw_gainmap(2:2:end,2:2:end)=(rgb(2:2:end,2:2:end,2));
        raw_gainmap(2:2:end,1:2:end)=(rgb(2:2:end,1:2:end,3));
    case {3,'gbrg'}
        raw_gainmap(2:2:end,1:2:end)=(rgb(2:2:end,1:2:end,1));
        raw_gainmap(1:2:end,1:2:end)=(rgb(1:2:end,1:2:end,2));
        raw_gainmap(2:2:end,2:2:end)=(rgb(2:2:end,2:2:end,2));
        raw_gainmap(1:2:end,2:2:end)=(rgb(1:2:end,2:2:end,3));
    case {4,'bggr'}
        raw_gainmap(2:2:end,2:2:end)=(rgb(2:2:end,2:2:end,1));
        raw_gainmap(1:2:end,2:2:end)=(rgb(1:2:end,2:2:end,2));
        raw_gainmap(2:2:end,1:2:end)=(rgb(2:2:end,1:2:end,2));
        raw_gainmap(1:2:end,1:2:end)=(rgb(1:2:end,1:2:end,3));
end

raw=raw./raw_gainmap;

raw = min(max(raw,0),1);

