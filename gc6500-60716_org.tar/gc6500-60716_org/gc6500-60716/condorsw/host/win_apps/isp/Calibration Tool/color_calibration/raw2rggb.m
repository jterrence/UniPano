%{
/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
%}

function raw2rggb(inputRaw, configJSON)

% Read JSON file with arguments and add it to a Matlab structure
% (Note: to parse a JSON file, the toolbox JSONlab is used. Available at:
% http://www.mathworks.com/matlabcentral/fileexchange/33381-jsonlab--a-toolbox-
% to-encode-decode-json-files )
configStruct = loadjson(configJSON);

% Check endianness
if strcmp(configStruct.inputRaw.endianness, 'be')
    configStruct.inputRaw.endianness = 'ieee-be';
elseif strcmp(configStruct.inputRaw.endianness, 'le')
    configStruct.inputRaw.endianness = 'ieee-le';
else
    error('Endianness not recognized, please use ''be'' or ''le''');
end

% Read raw file
fid = fopen(inputRaw,'r');
raw = fread(fid,[configStruct.inputRaw.width configStruct.inputRaw.height],...
    'uint16=>uint16', configStruct.inputRaw.endianness);
fclose(fid);
raw = single(raw);
% Transpose to keep row/column from original capture
% 'rawCircle' is 32 rows by 64 columns
rawCircle=raw';

if isfield(configStruct,'left20') && configStruct.left20 == 1
    patchStruct.left_rggb_patch20 = ...
        getRGGB(rawCircle, configStruct.inputRaw.bayerPattern, 'Left Patch 20');
end
if isfield(configStruct,'left23') && configStruct.left23 == 1
    patchStruct.left_rggb_patch23 = ...
        getRGGB(rawCircle, configStruct.inputRaw.bayerPattern, 'Left Patch 23');
end
if isfield(configStruct,'right20') && configStruct.right20 == 1
    patchStruct.right_rggb_patch20 = ...
        getRGGB(rawCircle, configStruct.inputRaw.bayerPattern, 'Right Patch 20');
end
if isfield(configStruct,'right23') && configStruct.right23 == 1
    patchStruct.right_rggb_patch23 = ...
        getRGGB(rawCircle, configStruct.inputRaw.bayerPattern, 'Right Patch 23');
end

% Check if values should be displayed
if isfield(configStruct.misc,'displayInConsole')
    % Print cooordinates (if needed)
    if isfield(configStruct.misc.displayInConsole,'coordinates') ...
            && configStruct.misc.displayInConsole.coordinates == 1
        displayCoordinates(patchStruct);
    end
    % Print values (if needed)
    if isfield(configStruct.misc.displayInConsole,'rggbValues') ...
            && configStruct.misc.displayInConsole.rggbValues == 1
        displayValues(patchStruct);
    end
end

% Save to file
if configStruct.misc.save
    if ~isfield(configStruct.misc, 'outFileName') ...
            || isempty(configStruct.misc.outFileName)
        % If no output file name is given, use date time and input raw
        % file name as output JSON file name.
        fileName = ...
            [datestr(now, 'yyyymmdd_HHMMSS') '_' inputRaw(1:end-4) '.json'];
    else
        fileName = configStruct.misc.outFileName;
    end
    saveToFile(fileName, patchStruct)
end

end

function outStruct = getRGGB(varargin)
% Input:
%   - rawIn: input raw array.
%   - bayerPattern: one of four Bayer patterns.
%   - titleStr: (Optional) string to use when displaying image.
%
% Output:
%   - outStruct: structure with
%       - outCoord: coordinates of selected region.
%       - outRGGB: rounded mean RGGB values for selected region.

if length(varargin) == 2
    rawIn = varargin{1};
    bayerPattern = varargin{2};
    titleStr = 'Click top left, then bottom right to select neutral patch';
elseif length(varargin) == 3
    rawIn = varargin{1};
    bayerPattern = varargin{2};
    titleStr = varargin{3};
end

% Manually select patch
figure(100);imagesc(rawIn.^.45)
title(titleStr)
[Y, X] = ginput(2);
X=fix(X);Y=fix(Y);
close(100);
patch = rawIn(X(1):X(2),Y(1):Y(2),:);

% Add coordinates to output
outCoord = [X, Y];

% Check if height start is odd to adjust Bayer type
if mod(X(1),2) == 0
    if strcmp(bayerPattern,'rggb')
        bayerPattern = 'gbrg';
    elseif strcmp(bayerPattern,'gbrg')
        bayerPattern = 'rggb';
    elseif strcmp(bayerPattern,'grbg')
        bayerPattern = 'bggr';
    elseif strcmp(bayerPattern,'bggr')
        bayerPattern = 'grbg';
    end
end
% Now check width
if mod(Y(1),2) == 0
    if strcmp(bayerPattern,'rggb')
        bayerPattern = 'grbg';
    elseif strcmp(bayerPattern,'gbrg')
        bayerPattern = 'bggr';
    elseif strcmp(bayerPattern,'grbg')
        bayerPattern = 'rggb';
    elseif strcmp(bayerPattern,'bggr')
        bayerPattern = 'gbrg';
    end
end

% Set width and height
maskHeight = ceil(size(patch,1)/2);
maskWidth = ceil(size(patch,2)/2);

% Initialize arrays
mask_bayer.r  = repmat(logical([1 0;0 0]),[maskHeight maskWidth]);
mask_bayer.gr = repmat(logical([0 1;0 0]),[maskHeight maskWidth]);
mask_bayer.gb = repmat(logical([0 0;1 0]),[maskHeight maskWidth]);
mask_bayer.b  = repmat(logical([0 0;0 1]),[maskHeight maskWidth]);

% Switch between Bayer type
switch bayerPattern
    case {1,'rggb'}
        mask_bayer.r  = repmat(logical([1 0;0 0]),[maskHeight maskWidth]);
        mask_bayer.gr = repmat(logical([0 1;0 0]),[maskHeight maskWidth]);
        mask_bayer.gb = repmat(logical([0 0;1 0]),[maskHeight maskWidth]);
        mask_bayer.b  = repmat(logical([0 0;0 1]),[maskHeight maskWidth]);
    case {2,'grbg'}
        mask_bayer.gr = repmat(logical([1 0;0 0]),[maskHeight maskWidth]);
        mask_bayer.r  = repmat(logical([0 1;0 0]),[maskHeight maskWidth]);
        mask_bayer.b  = repmat(logical([0 0;1 0]),[maskHeight maskWidth]);
        mask_bayer.gb = repmat(logical([0 0;0 1]),[maskHeight maskWidth]);
    case {3,'gbrg'}
        mask_bayer.gb = repmat(logical([1 0;0 0]),[maskHeight maskWidth]);
        mask_bayer.b  = repmat(logical([0 1;0 0]),[maskHeight maskWidth]);
        mask_bayer.r  = repmat(logical([0 0;1 0]),[maskHeight maskWidth]);
        mask_bayer.gr = repmat(logical([0 0;0 1]),[maskHeight maskWidth]);
    case {4,'bggr'}
        mask_bayer.b  = repmat(logical([1 0;0 0]),[maskHeight maskWidth]);
        mask_bayer.gb = repmat(logical([0 1;0 0]),[maskHeight maskWidth]);
        mask_bayer.gr = repmat(logical([0 0;1 0]),[maskHeight maskWidth]);
        mask_bayer.r  = repmat(logical([0 0;0 1]),[maskHeight maskWidth]);
end

% If size of input patch image is odd, mask needs to be adjusted
if (mod(size(patch,1),2) ~= 0) && (mod(size(patch,2),2) ~= 0)
    mask_bayer.r = mask_bayer.r(1:end-1,1:end-1);
    mask_bayer.gr = mask_bayer.gr(1:end-1,1:end-1);
    mask_bayer.gb = mask_bayer.gb(1:end-1,1:end-1);
    mask_bayer.b = mask_bayer.b(1:end-1,1:end-1);
elseif mod(size(patch,1),2) ~= 0
    mask_bayer.r = mask_bayer.r(1:end-1,:);
    mask_bayer.gr = mask_bayer.gr(1:end-1,:);
    mask_bayer.gb = mask_bayer.gb(1:end-1,:);
    mask_bayer.b = mask_bayer.b(1:end-1,:);
elseif mod(size(patch,2),2) ~= 0
    mask_bayer.r = mask_bayer.r(:,1:end-1);
    mask_bayer.gr = mask_bayer.gr(:,1:end-1);
    mask_bayer.gb = mask_bayer.gb(:,1:end-1);
    mask_bayer.b = mask_bayer.b(:,1:end-1);
end

% Get R Gr Gb B mean values
meanR  = (mean2(patch(mask_bayer.r)));
meanGr = (mean2(patch(mask_bayer.gr)));
meanGb = (mean2(patch(mask_bayer.gb)));
meanB  = (mean2(patch(mask_bayer.b)));

% To output array
outRGGB = round([meanR, meanGr, meanGb, meanB]);

% To output struct
outStruct.outCoord = outCoord;
outStruct.outRGGB = outRGGB;

end


function displayCoordinates(patchStruct)
% Get field names
fields = fieldnames(patchStruct);
% Iterate over field names
for i = 1:numel(fields)
    fprintf('%s:\n', fields{i});
    fprintf('    Horizontal start/end: %d, %d\n', ...
        patchStruct.(fields{i}).outCoord(1,2), ...
        patchStruct.(fields{i}).outCoord(2,2));
    fprintf('    Vertical start/end: %d, %d\n', ...
        patchStruct.(fields{i}).outCoord(1,1), ...
        patchStruct.(fields{i}).outCoord(2,1));
end

end


function displayValues(patchStruct)
% Get field names
fields = fieldnames(patchStruct);
% Iterate over field names
for i = 1:numel(fields)
    fprintf('%s:\n', fields{i});
    fprintf('    Mean RGGB values: %d, %d, %d, %d \n', ...
        patchStruct.(fields{i}).outRGGB(1), ...
        patchStruct.(fields{i}).outRGGB(2), ...
        patchStruct.(fields{i}).outRGGB(3), ...
        patchStruct.(fields{i}).outRGGB(4));
end

end


function saveToFile(fileName, patchStruct)

% To output file
fID = fopen(fileName,'w');
fprintf(fID, '{\n');
fprintf(fID, '    "dual_sensor_calibration": {\n');

% Get field names
fields = fieldnames(patchStruct);
% Iterate over field names
for i = 1:numel(fields)
    fprintf(fID, '        "%s": [\n', fields{i});
    fprintf(fID, '            %d,\n', patchStruct.(fields{i}).outRGGB(1));
    fprintf(fID, '            %d,\n', patchStruct.(fields{i}).outRGGB(2));
    fprintf(fID, '            %d,\n', patchStruct.(fields{i}).outRGGB(3));
    fprintf(fID, '            %d\n', patchStruct.(fields{i}).outRGGB(4));
    if i < numel(fields)
        fprintf(fID, '        ],\n');
    else
        fprintf(fID, '        ]\n');
    end
end
fprintf(fID, '    }\n');
fprintf(fID, '}');

fclose(fID);

end