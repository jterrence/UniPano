%{
/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
%}

function [raw] = correct_black_level(raw,info,black_level)

switch info.bayer_type
    case {1,'rggb'}
        mask_bayer.r  = repmat(logical([1 0;0 0]),[size(raw,1)/2 size(raw,2)/2]);
        mask_bayer.gr = repmat(logical([0 1;0 0]),[size(raw,1)/2 size(raw,2)/2]);
        mask_bayer.gb = repmat(logical([0 0;1 0]),[size(raw,1)/2 size(raw,2)/2]);
        mask_bayer.b  = repmat(logical([0 0;0 1]),[size(raw,1)/2 size(raw,2)/2]);
    case {2,'grbg'}
        mask_bayer.gr = repmat(logical([1 0;0 0]),[size(raw,1)/2 size(raw,2)/2]);
        mask_bayer.r  = repmat(logical([0 1;0 0]),[size(raw,1)/2 size(raw,2)/2]);
        mask_bayer.b  = repmat(logical([0 0;1 0]),[size(raw,1)/2 size(raw,2)/2]);
        mask_bayer.gb = repmat(logical([0 0;0 1]),[size(raw,1)/2 size(raw,2)/2]);
    case {3,'gbrg'}
        mask_bayer.gb = repmat(logical([1 0;0 0]),[size(raw,1)/2 size(raw,2)/2]);
        mask_bayer.b  = repmat(logical([0 1;0 0]),[size(raw,1)/2 size(raw,2)/2]);
        mask_bayer.r  = repmat(logical([0 0;1 0]),[size(raw,1)/2 size(raw,2)/2]);
        mask_bayer.gr = repmat(logical([0 0;0 1]),[size(raw,1)/2 size(raw,2)/2]);
    case {4,'bggr'}
        mask_bayer.b  = repmat(logical([1 0;0 0]),[size(raw,1)/2 size(raw,2)/2]);
        mask_bayer.gb = repmat(logical([0 1;0 0]),[size(raw,1)/2 size(raw,2)/2]);
        mask_bayer.gr = repmat(logical([0 0;1 0]),[size(raw,1)/2 size(raw,2)/2]);
        mask_bayer.r  = repmat(logical([0 0;0 1]),[size(raw,1)/2 size(raw,2)/2]);
end
mask_bayer.g = mask_bayer.gr|mask_bayer.gb;


raw = max(raw.*mask_bayer.r  - black_level(1), 0) +...
      max(raw.*mask_bayer.gr - black_level(2), 0) +...
      max(raw.*mask_bayer.gb - black_level(3), 0) +...
      max(raw.*mask_bayer.b  - black_level(4), 0);