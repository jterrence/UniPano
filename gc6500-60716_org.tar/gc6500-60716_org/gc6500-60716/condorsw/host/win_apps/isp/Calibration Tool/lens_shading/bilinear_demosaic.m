%{
/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
%}

function rgb = bilinear_demosaic(raw,bayer_type)
% modes:
%         'bilinear'

%% Bayer Pattern Masks

switch bayer_type
    case {0,'rggb'}
        mask_bayer.r  = repmat(logical([1 0;0 0]),[size(raw,1)/2 size(raw,2)/2]);
        mask_bayer.gr = repmat(logical([0 1;0 0]),[size(raw,1)/2 size(raw,2)/2]);
        mask_bayer.gb = repmat(logical([0 0;1 0]),[size(raw,1)/2 size(raw,2)/2]);
        mask_bayer.b  = repmat(logical([0 0;0 1]),[size(raw,1)/2 size(raw,2)/2]);
    case {2,'grbg'}
        mask_bayer.gr = repmat(logical([1 0;0 0]),[size(raw,1)/2 size(raw,2)/2]);
        mask_bayer.r  = repmat(logical([0 1;0 0]),[size(raw,1)/2 size(raw,2)/2]);
        mask_bayer.b  = repmat(logical([0 0;1 0]),[size(raw,1)/2 size(raw,2)/2]);
        mask_bayer.gb = repmat(logical([0 0;0 1]),[size(raw,1)/2 size(raw,2)/2]);
    case {3,'gbrg'}
        mask_bayer.gb = repmat(logical([1 0;0 0]),[size(raw,1)/2 size(raw,2)/2]);
        mask_bayer.b  = repmat(logical([0 1;0 0]),[size(raw,1)/2 size(raw,2)/2]);
        mask_bayer.r  = repmat(logical([0 0;1 0]),[size(raw,1)/2 size(raw,2)/2]);
        mask_bayer.gr = repmat(logical([0 0;0 1]),[size(raw,1)/2 size(raw,2)/2]);
    case {1,'bggr'}
        mask_bayer.b  = repmat(logical([1 0;0 0]),[size(raw,1)/2 size(raw,2)/2]);
        mask_bayer.gb = repmat(logical([0 1;0 0]),[size(raw,1)/2 size(raw,2)/2]);
        mask_bayer.gr = repmat(logical([0 0;1 0]),[size(raw,1)/2 size(raw,2)/2]);
        mask_bayer.r  = repmat(logical([0 0;0 1]),[size(raw,1)/2 size(raw,2)/2]);
end
mask_bayer.g = mask_bayer.gr|mask_bayer.gb;

rgb = interp_bilinear(raw,mask_bayer);

rgb(1,:,:) = rgb(2,:,:);
rgb(end,:,:) = rgb(end-1,:,:);
rgb(:,1,:) = rgb(:,2,:);
rgb(:,end,:) = rgb(:,end-1,:);

%% RGB Output
% rgb = min(max( rgb ,0),1);


%% FUNCTIONS  -----------------------------------------------------------------%
%------------------------------------------------------------------------------%
%                                                                              %
%------------------------------------------------------------------------------%

% Linear Interpolation
function RGB = interp_bilinear(raw,mask_bayer)                               
G = imfilter(mask_bayer.g.*raw,[1 2 1;2 4 2;1 2 1]/8,'symmetric');
R = imfilter(mask_bayer.r.*raw,[1 2 1;2 4 2;1 2 1]/4,'symmetric');
B = imfilter(mask_bayer.b.*raw,[1 2 1;2 4 2;1 2 1]/4,'symmetric');
RGB = cat(3,R,G,B);






