%{
/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
%}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   ------------------------------------------------------------------------------  
%       This confidential and proprietary software/information may be used only     
%          as authorized by a licensing agreement from Apical Limited               
%                                                                                   
%                     (C) COPYRIGHT 2016 Apical Limited                             
%                            ALL RIGHTS RESERVED                                    
%                                                                                   
%        The entire notice above must be reproduced on all authorized               
%         copies and copies may only be made to the extent permitted                
%               by a licensing agreement from Apical Limited.                       
%                                                                                   
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function LUT_SHADING_MESH = calibrate_shading_mesh(raw,info,settings)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function returns the the gain map LUT for MESH shading correction

% Usage: lut_shading_mesh = calibrate_shading_mesh(raw,info,settings)
% Inputs:
% Raw -> Bayer raw image with a range of values 0:(2^bit depth-1)
% info-> structure with image information. 
%
%   Bayer pattern info is required and should be set from one of the
%   following options:
%       info.bayer_type = 'rggb' | 'grbg' | 'gbrg' | 'bggr'
% 
%   Black levels are required.
%   info.BLACK_LEVELS -> as estimated from calibrate_black_levels
%
%   info.bit_depth -> bit depth of the image
% 
% settings -> structure with calibration parameters
%   settings.mesh_size         = [32 32]; %Lut size
%   settings.luma_strength     = 100; %correction percentage for luma in the range of [1:100]
%   settings.chroma_strength   = 100; %correction percentage for chroma in the range of [1:100]
%   settings.input_mesh_scale  = 1; %(default)
%         mesh_scale format         Max gain 
%         0	Unsigned Fraction 1.7	x 2
%         1	Unsigned Fraction 2.6	x 4
%         2	Unsigned Fraction 3.5	x 8
%         3	Unsigned Fraction 4.4	x 16

% Outputs:
% LUT_SHADING_MESH -> shading gain map
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Normalise the image and correct for black level
raw = single(correct_black_level(raw,info,info.BLACK_LEVEL))./(2^info.bit_depth-1);

switch settings.input_mesh_scale
    case 0
        settings.mesh_scale = 128;
    case 1
        settings.mesh_scale = 64;
    case 2
        settings.mesh_scale = 32;
    case 3
        settings.mesh_scale = 16;
end

LUT_SHADING_MESH  = calc_mesh_shading(raw,info, settings);

%%Minimum mesh scale required
max_gain = max(LUT_SHADING_MESH(:)./settings.mesh_scale);
if max_gain > 0 && max_gain < 2
    min_mesh_scale = 0;
elseif max_gain >= 2  && max_gain < 4
    min_mesh_scale = 1;
elseif max_gain >= 4  && max_gain < 8
    min_mesh_scale = 2;
elseif max_gain >= 8
    min_mesh_scale = 3;
end

if  settings.input_mesh_scale  < min_mesh_scale
    res_str = sprintf('WARNING: \n Max gain = %f \n Please set Mesh scale to %d \n',max_gain,min_mesh_scale);
    fprintf(res_str);
end
% make sure it only uses a 8 bit variable -> min(256,xx)
LUT_SHADING_MESH  = min(256,round(LUT_SHADING_MESH));


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%% Get gain map %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function rgb_mesh_gainmap = calc_mesh_shading(raw,info,settings)
        meshsize = settings.mesh_size;
        chroma_strength = settings.chroma_strength;
        luma_strength   = settings.luma_strength;
        %%Calculate mesh gainmap for each color channel
        rgb = bilinear_demosaic(raw,info.bayer_type);
        
        rgb_mesh_gainmap(:,:,1) = calc_mesh_gainmap(rgb(:,:,1),meshsize);
        rgb_mesh_gainmap(:,:,2) = calc_mesh_gainmap(rgb(:,:,2),meshsize);
        rgb_mesh_gainmap(:,:,3) = calc_mesh_gainmap(rgb(:,:,3),meshsize);
        
        % Adjust Luma and Chroma shading correction strength
        rgb_mesh_gainmap = double(rgb_mesh_gainmap);
        gain_R = rgb_mesh_gainmap(:,:,1);
        gain_G = rgb_mesh_gainmap(:,:,2);
        gain_B = rgb_mesh_gainmap(:,:,3);
        gain_RG = gain_R./gain_G;
        gain_BG = gain_B./gain_G;
        gain_RG = (gain_RG-1) .* (chroma_strength/100) +1;
        gain_BG = (gain_BG-1) .* (chroma_strength/100) +1;
        gain_G  = (gain_G -1) .* (luma_strength  /100) +1;
        gain_R = gain_G.*gain_RG;
        gain_B = gain_G.*gain_BG;
        
        rgb_mesh_gainmap = cat(3,gain_R,gain_G,gain_B);
        
        
        rgb_mesh_gainmap = settings.mesh_scale.*rgb_mesh_gainmap;
    end

    function gainmap = calc_mesh_gainmap(raw,meshsize)
        shading = imresize(raw,[meshsize(1),meshsize(2)],'bilinear','AntiAliasing',false);
        shading = imfilter(shading,[1 4 6 4 1]'*[1 4 6 4 1]/256,'symmetric');
        gainmap = max(shading(:))./shading;
    end
end

