/*******************************************************************************
*
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this
* copyright notice.
*
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*******************************************************************************/

===================================
 'raw2rggb.m' Matlab function
===================================


++++++++++++++++
 Pre-requisites
++++++++++++++++

- JSONlab: to parse a JSON file in Matlab, the toolbox JSONlab is used.
Available at: http://www.mathworks.com/matlabcentral/fileexchange/33381-jsonlab--a-toolbox-to-encode-decode-json-files


++++++++
 Syntax 
++++++++

   raw2rggb(inputRaw, configJSON)

where:
- inputRaw: input raw image.
- configJSON: configuration file in JSON format.


++++++++++++++++++++
 Configuration file
++++++++++++++++++++

--------------
 Example File
--------------

An example of the configuration file format is:
    {
        "inputRaw": {
            "width": 2688,
            "height": 1344,
            "bayerPattern": "grbg",
            "endianness": "be"
        },
        "left20": 1,
        "left23": 1,
        "right20": 1,
        "right23": 1,
        
        "misc": {
            "displayInConsole": {
                "coordinates": 1,
                "rggbValues": 0
            },
            "save": 1,
            "outFileName": ""
        }
    }

'inputRaw' section:
    - width: width of input raw image.
    - height: height of input raw image.
    - bayerPattern: one of four Bayer patterns (rggb, bggr, grbg, gbrg) of raw
      image.
    - endianness: endianness of input raw image, either 'be' or 'le'.

'left20', 'left23', 'right20', 'right23' sections:
    - Enable or disable patch selection.

'misc' section:
    - displayInConsole sub-section:
        - coordiantes: enable or disable displaying coordiantes from input
          patches.
        - rggbValues: enable or disable displaying mean RGGB values from input
        patches.
    - save: save result to JSON file.
    - outFileName: file name of JSON file. If this field is not specified or if
      it is empty, current date and time plus input file name will be used.