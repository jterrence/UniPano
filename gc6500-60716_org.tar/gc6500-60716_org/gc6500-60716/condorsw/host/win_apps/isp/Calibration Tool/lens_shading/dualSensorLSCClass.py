'''
/*******************************************************************************
*
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this
* copyright notice.
*
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*******************************************************************************/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   ----------------------------------------------------------------------------
%       This confidential and proprietary software/information may be used only
%          as authorized by a licensing agreement from Apical Limited
%
%                     (C) COPYRIGHT 2016 Apical Limited
%                            ALL RIGHTS RESERVED
%
%        The entire notice above must be reproduced on all authorized
%         copies and copies may only be made to the extent permitted
%               by a licensing agreement from Apical Limited.
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'''

import json
import sys
import datetime
import ntpath

try:
    import numpy as np
except ImportError:
    print 'ERROR: ''numpy'' not installed!'
    sys.exit()

try:
    from scipy import ndimage
except ImportError:
    print 'ERROR: ''scipy.ndimage'' not installed!'
    sys.exit()

try:
    from skimage import transform
except ImportError:
    print 'ERROR: ''skimage'' (scikit-image) not installed!'
    sys.exit()


class dualSensorLSC:

    def __init__(self, rawFile=None, jsonFile=None):

        # Initialize raw information
        self.info = dict()
        self.info['bayerType'] = 'grbg'
        self.info['headerSizeInBytes'] = 0
        self.info['resolution'] = [2688, 1344]
        self.info['bitDepth'] = 12
        self.info['endianness'] = 'be'
        self.info['blackLevels'] = [168, 168, 168, 168]
        # Initialize lens shading
        self.lensShading = dict()
        self.lensShading['meshSize'] = [32, 32]
        self.lensShading['lumaStrength'] = 100
        self.lensShading['chromaStrength'] = 0
        self.lensShading['meshScale'] = 2
        # Initialize pseudo-radial expansion
        self.pseudoRadial = dict()
        self.pseudoRadial['enable'] = True
        self.pseudoRadial['pixelThreshold'] = 800
        self.pseudoRadial['distanceThreshold'] = 5
        self.pseudoRadial['depth'] = 5
        # Misc section
        self.misc = dict()
        self.misc['display'] = False
        self.misc['save'] = False
        self.misc['saveMeshOnly'] = False

        # Update from JSON file
        if jsonFile is not None:
            self.getJSONInfo(jsonFile)

        # Read raw file
        if rawFile is None:
            print 'ERROR: raw file is required.'
            sys.exit()
        else:
            # Check if only one or two files are provided
            if len(rawFile) == 1:
                # Read single raw file
                self.raw = self.readRawFile(rawFile[0])
                self.rawLeft = None
                self.rawRight = None
                # Get rawFile file name
                self.misc['outFileName'] \
                    = datetime.datetime.now().strftime('%Y%m%d_%H%M%S') \
                      + '_' \
                      + ntpath.basename(rawFile[0])[:-4] \
                      + '.json'
            elif len(rawFile) == 2:
                # Read two raw files
                self.raw = None
                self.rawLeft = self.readRawFile(rawFile[0])
                self.rawRight = self.readRawFile(rawFile[1])
                # Get rawFile file name (as left one)
                self.misc['outFileName'] \
                    = datetime.datetime.now().strftime('%Y%m%d_%H%M%S') \
                      + '_' \
                      + ntpath.basename(rawFile[0])[:-4] \
                      + '.json'
            else:
                print 'ERROR: only one or two raw files supported.'
                sys.exit()



    def getJSONInfo(self, jsonFile):
        # Read JSON file
        with open(jsonFile, 'r') as fID:
            configDict = json.load(fID)
        # Update info section
        if 'info' in configDict:
            self.info['bayerType'] = configDict['info']['bayerType']
            self.info['headerSizeInBytes'] = configDict['info']['headerSize']
            self.info['resolution'] = configDict['info']['resolution']
            self.info['bitDepth'] = configDict['info']['bitDepth']
            self.info['endianness'] = configDict['info']['endianness']
            self.info['blackLevels'] = configDict['info']['blackLevels']
        else:
            print 'WARNING: using default values for ''info'' section.'
        # Update lens shading section
        if 'lensShading' in configDict:
            self.lensShading['meshSize'] = configDict['lensShading']['meshSize']
            # Assuming raw capture is two sensors horizontally merged, divide
            # lens shading input width by 2
            self.lensShading['meshSize'][0] /= 2
            self.lensShading['lumaStrength'] \
                = configDict['lensShading']['lumaStrength']
            self.lensShading['chromaStrength'] \
                = configDict['lensShading']['chromaStrength']
            self.lensShading['meshScale'] = configDict['lensShading']['scale']
        else:
            print 'WARNING: using default values for ''lensShading'' section.'
        # Update Pseudo-radial Expansion section
        if 'pseudoRadialExpansion' in configDict:
            self.pseudoRadial['enable'] \
                = configDict['pseudoRadialExpansion']['enable']
            self.pseudoRadial['pixelThreshold'] \
                = configDict['pseudoRadialExpansion']['pixelThreshold']
            self.pseudoRadial['distanceThreshold'] \
                = configDict['pseudoRadialExpansion']['distanceThreshold']
            self.pseudoRadial['depth'] \
                = configDict['pseudoRadialExpansion']['depth']
        else:
            print 'WARNING: using default values for' \
                  ' ''pseudoRadialExpansion'' section.'
        # Update misc section
        if 'misc' in configDict:
            self.misc['display'] = configDict['misc']['display']
            self.misc['save'] = configDict['misc']['save']
            self.misc['saveMeshOnly'] = configDict['misc']['saveMeshOnly']
            if configDict['misc']['outFileName']:
                self.misc['outFileName'] = configDict['misc']['outFileName']
        else:
            print 'WARNING: using default values for ''misc'' section.'


    def readRawFile(self, rawFile):
        # Check endianness
        if self.info['endianness'] == 'be':
            formatEndianness = '>'
        elif self.info['endianness'] == 'le':
            formatEndianness = '<'
        else:
            print 'ERROR: unsupported endianness'
            sys.exit()

        # Check bit size
        if self.info['bitDepth'] <= 8:
            formatBitDepth = 'B'
        elif 8 < self.info['bitDepth'] <= 16:
            formatBitDepth = 'H'
        elif 16 < self.info['bitDepth'] <= 32:
            formatBitDepth = 'I'
        else:
            print 'ERROR: unsupported bit depth'
            sys.exit()

        # Set format value
        formatValue = formatEndianness + formatBitDepth

        # Read raw file as numpy array
        rawArray = np.fromfile(rawFile, dtype=formatValue)
        # Check if resolution matches
        if np.size(rawArray) != (self.info['resolution'][0] * self.info['resolution'][1]):
            print 'ERROR: resolution does not match raw file'
            sys.exit()

        # Resize according to resolution
        resolution = self.info['resolution']
        resolution = resolution[::-1]
        rawOut = np.resize(rawArray, resolution)

        return rawOut


    def processSide(self, side):

        # Check if one or two captures have been specified
        if self.rawLeft is None and self.rawRight is None:
            # Check for current side
            if side == 'left':
                rawSide = self.raw[:, :(self.info['resolution'][0]/2)]
            elif side == 'right':
                rawSide = self.raw[:, (self.info['resolution'][0]/2):]
            else:
                print 'ERROR: unrecognized side'
                sys.exit()
        elif self.raw is None:
            # Check for current side
            if side == 'left':
                rawSide = self.rawLeft[:, :(self.info['resolution'][0]/2)]
            elif side == 'right':
                rawSide = self.rawRight[:, (self.info['resolution'][0]/2):]
            else:
                print 'ERROR: unrecognized side'
                sys.exit()
        else:
            # Should not get here
            print 'ERROR: only one or two raw files supported (processing).'
            sys.exit()


        # Check for PRE
        if self.pseudoRadial['enable']:
            rawSidePRE = self.pseudoRadialExpansion(rawSide)
            meshRGB = self.calibrateShadingMesh(rawSidePRE)
        else:
            meshRGB = self.calibrateShadingMesh(rawSide)

        # Correct black levels of raw image
        rawBL = self.correctBlackLevel(rawSide).astype(np.float32)
        # Normalize
        rawNL = rawBL / (2**self.info['bitDepth'] - 1)
        # Correct lens shading of corrected black levels image
        rawBLCorr = self.correctShadingMesh(rawNL, meshRGB)

        return meshRGB


    def pseudoRadialExpansion(self, rawSide):
        # Check if 'depth' is less than or equal to 'pixelThreshold'
        if self.pseudoRadial['depth'] > self.pseudoRadial['distanceThreshold']:
            print 'ERROR: ''depth'' (' + str(self.pseudoRadial['depth']) + ') should'\
                  'be less or equal to ''distanceThreshold'' (' + \
                  str(self.pseudoRadial['distanceThreshold']) + ')'
            sys.exit()

        # Assuming image is already centered, calculate start/end points
        r = rawSide.shape[0]/2
        rowStart = np.rint(r - np.sqrt(2)/2 * r).astype(int) - 1
        rowEnd = np.rint(r + np.sqrt(2)/2 * r).astype(int)
        colStart = np.rint(r - np.sqrt(2)/2 * r).astype(int) - 1
        colEnd = np.rint(r + np.sqrt(2)/2 * r).astype(int)

        # Process row (left side)
        rawOut = np.empty_like(rawSide)
        rawOut[:] = rawSide
        rawOut = self.rowProcessFull(rawOut, rowStart, rowEnd, 'left2right')
        # Process row (right side)
        rawOut = self.rowProcessFull(rawOut, rowStart, rowEnd, 'right2left')
        # Process column (upper side)
        rawOut = self.colProcessFull(rawOut, colStart, colEnd, 'ascending')
        # Process column (bottom side)
        rawOut = self.colProcessFull(rawOut, colStart, colEnd, 'descending')
        # Now start from the beginning
        rawOut = self.rowProcessFull(rawOut, 0, rawSide.shape[1], 'left2right')
        rawOut = self.rowProcessFull(rawOut, 0, rawSide.shape[1], 'right2left')

        return rawOut


    def rowProcessFull(self, rawIn, rowStart, rowEnd, direction):
        # Check if image should be flipped
        if direction == 'right2left':
            rawIn = np.fliplr(rawIn)
        # Create output array
        rawOut = rawIn

        # Iterate over rows
        colHalf = rawIn.shape[1] / 2
        for row in xrange(rowStart, rowEnd):
            currentRow = rawIn[row, :colHalf]
            # Find locations where values are less than threshold
            loc = np.where(currentRow <= self.pseudoRadial['pixelThreshold'])
            # Get last location
            if loc[0].size > 0:
                locEnd = loc[0][-1]
                # Check if last location plus distance threshold goes out of bounds
                if (locEnd + self.pseudoRadial['distanceThreshold']) <= rawIn.shape[1]:
                    if (locEnd + self.pseudoRadial['distanceThreshold']) % 2 == 0:
                        rawOut[row, 1:(locEnd + self.pseudoRadial['depth'] + 1):2] = \
                            rawIn[row, locEnd + self.pseudoRadial['distanceThreshold'] + 1]
                        rawOut[row, 0:(locEnd + self.pseudoRadial['depth'] + 1):2] = \
                            rawIn[row, locEnd + self.pseudoRadial['distanceThreshold']]
                    else:
                        rawOut[row, 1:(locEnd + self.pseudoRadial['depth'] + 1):2] = \
                            rawIn[row, locEnd + self.pseudoRadial['distanceThreshold']]
                        rawOut[row, 0:(locEnd + self.pseudoRadial['depth'] + 1):2] = \
                            rawIn[row, locEnd + self.pseudoRadial['distanceThreshold'] + 1]

        # Check if raw array should be flipped back
        if direction == 'right2left':
            rawOut = np.fliplr(rawOut)

        return rawOut


    def colProcessFull(self, rawIn, colStart, colEnd, direction):
        # Check if image should be flipped
        if direction == 'descending':
            rawIn = np.flipud(rawIn)
        # Create output array
        rawOut = rawIn

        # Iterate over columns
        rowHalf = rawIn.shape[0] / 2
        for col in xrange(colStart, colEnd):
            currentCol = rawIn[:rowHalf, col]
            # Find locations where values are less than threshold
            loc = np.where(currentCol <= self.pseudoRadial['pixelThreshold'])
            # Get last location
            if loc[0].size > 0:
                locEnd = loc[0][-1]
                # Check if last location plus distance threshold goes out of bounds
                if (locEnd + self.pseudoRadial['distanceThreshold']) > 0:
                    if (locEnd + self.pseudoRadial['distanceThreshold']) % 2 == 0:
                        rawOut[1:(locEnd + self.pseudoRadial['depth'] + 1):2, col] = \
                            rawIn[locEnd + self.pseudoRadial['distanceThreshold'] + 1, col]
                        rawOut[0:(locEnd + self.pseudoRadial['depth'] + 1):2, col] = \
                            rawIn[locEnd + self.pseudoRadial['distanceThreshold'], col]
                    else:
                        rawOut[1:(locEnd + self.pseudoRadial['depth'] + 1):2, col] = \
                            rawIn[locEnd + self.pseudoRadial['distanceThreshold'], col]
                        rawOut[0:(locEnd + self.pseudoRadial['depth'] + 1):2, col] = \
                            rawIn[locEnd + self.pseudoRadial['distanceThreshold'] + 1, col]

        # Check if raw array should be flipped back
        if direction == 'descending':
            rawOut = np.flipud(rawOut)

        return rawOut


    def correctBlackLevel(self, rawIn):
        # Define initial 'logical' arrays
        logicalOne = np.array([[1, 0], [0, 0]])
        logicalTwo = np.array([[0, 1], [0, 0]])
        logicalThree = np.array([[0, 0], [1, 0]])
        logicalFour = np.array([[0, 0], [0, 1]])

        # Define size of masks
        maskRows = rawIn.shape[1] / 2
        maskCols = rawIn.shape[0] / 2

        # Select correct mask
        if self.info['bayerType'] == 'rggb':
            maskBayerR = np.tile(logicalOne, [maskRows, maskCols])
            maskBayerGr = np.tile(logicalTwo, [maskRows, maskCols])
            maskBayerGb = np.tile(logicalThree, [maskRows, maskCols])
            maskBayerB = np.tile(logicalFour, [maskRows, maskCols])
        elif self.info['bayerType'] == 'grbg':
            maskBayerGr = np.tile(logicalOne, [maskRows, maskCols])
            maskBayerR = np.tile(logicalTwo, [maskRows, maskCols])
            maskBayerB = np.tile(logicalThree, [maskRows, maskCols])
            maskBayerGb = np.tile(logicalFour, [maskRows, maskCols])
        elif self.info['bayerType'] == 'gbrg':
            maskBayerGb = np.tile(logicalOne, [maskRows, maskCols])
            maskBayerB = np.tile(logicalTwo, [maskRows, maskCols])
            maskBayerR = np.tile(logicalThree, [maskRows, maskCols])
            maskBayerGr = np.tile(logicalFour, [maskRows, maskCols])
        elif self.info['bayerType'] == 'bggr':
            maskBayerB = np.tile(logicalOne, [maskRows, maskCols])
            maskBayerGb = np.tile(logicalTwo, [maskRows, maskCols])
            maskBayerGr = np.tile(logicalThree, [maskRows, maskCols])
            maskBayerR = np.tile(logicalFour, [maskRows, maskCols])
        else:
            print 'ERROR: Unrecognized Bayer pattern'
            sys.exit()

        # Get black level corrected raw image
        rawOut = \
            np.maximum(rawIn * maskBayerR - self.info['blackLevels'][0], 0) + \
            np.maximum(rawIn * maskBayerGr - self.info['blackLevels'][1], 0) + \
            np.maximum(rawIn * maskBayerGb - self.info['blackLevels'][2], 0) + \
            np.maximum(rawIn * maskBayerB - self.info['blackLevels'][3], 0)
        return rawOut


    def calibrateShadingMesh(self, rawIn):
        # Correct black levels
        rawBL = self.correctBlackLevel(rawIn).astype(np.float32)
        # Normalize image
        rawNL = rawBL / (2**self.info['bitDepth'] - 1)

        # Set mesh scale
        if self.lensShading['meshScale'] == 0:
            meshScale = 128
        elif self.lensShading['meshScale'] == 1:
            meshScale = 64
        elif self.lensShading['meshScale'] == 2:
            meshScale = 32
        elif self.lensShading['meshScale'] == 3:
            meshScale = 16
        else:
            print 'ERROR: mesh scale should be a value between 0 and 3'
            sys.exit()

        # Calculate lens shading mesh
        lutShadingMesh = self.calcMeshShading(rawNL, meshScale)

        # Minimum mesh scale required
        maxGain = lutShadingMesh.max() / meshScale
        if 0 < maxGain < 2:
            minMeshScale = 0
        elif 2 <= maxGain < 4:
            minMeshScale = 1
        elif 4 <= maxGain < 8:
            minMeshScale = 2
        elif maxGain >= 8:
            minMeshScale = 3
        else:
            print 'ERROR: max gain cannot be negative'
            sys.exit()

        if self.lensShading['meshScale'] < minMeshScale:
            print 'WARNING: Max gain is ' + str(maxGain)
            print '         Please set mesh scale to ' + str(minMeshScale)

        # Make sure it only uses 8 bits
        lutShadingMesh = np.minimum(256, np.round(lutShadingMesh))

        return lutShadingMesh



    def calcMeshShading(self, rawIn, meshScale):
        # Get RGB image
        rgb = self.bilinearDemosaic(rawIn)

        rMeshGainMap = self.calcMeshGainMap(rgb[0,:,:])
        gMeshGainMap = self.calcMeshGainMap(rgb[1,:,:])
        bMeshGainMap = self.calcMeshGainMap(rgb[2,:,:])

        # Concatenate and change type to double. Size is (plane, row, column)
        rgbMeshGainMap = np.array([rMeshGainMap, gMeshGainMap, bMeshGainMap])\
            .astype(np.float64)

        # Adjust luma and chroma shading correction strength
        gainR = rgbMeshGainMap[0,:,:]
        gainG = rgbMeshGainMap[1,:,:]
        gainB = rgbMeshGainMap[2,:,:]

        gainRG = gainR / gainG
        gainBG = gainB / gainG

        gainRG = (gainRG - 1) * (self.lensShading['chromaStrength']/100.) + 1
        gainBG = (gainBG - 1) * (self.lensShading['chromaStrength']/100.) + 1

        gainG = (gainG - 1) * (self.lensShading['lumaStrength']/100.) + 1

        gainR = gainG * gainRG
        gainB = gainG * gainBG

        # Concatenate again...
        rgbMeshGainMap = np.array([gainR, gainG, gainB]).astype(np.float64)

        # Scale
        rgbMeshGainMap *= meshScale

        return rgbMeshGainMap


    def calcMeshGainMap(self, rgbPlaneIn):
        # Downscale input plane
        shading = transform.resize(rgbPlaneIn, self.lensShading['meshSize'])\
            .astype(np.float32)
        # Generate kernel for smoothing filter
        kernelVector = np.array([1,4,6,4,1], dtype=np.float32)
        kernelSmooth = np.outer(kernelVector, kernelVector) / 256
        # Smooth
        shadingSmooth = ndimage.filters.correlate(shading, kernelSmooth)
        # Calculate gain map
        gainMap = shadingSmooth.max() / shadingSmooth

        return gainMap


    def bilinearDemosaic(self, rawIn):
        # Define initial 'logical' arrays
        logicalOne = np.array([[1, 0], [0, 0]])
        logicalTwo = np.array([[0, 1], [0, 0]])
        logicalThree = np.array([[0, 0], [1, 0]])
        logicalFour = np.array([[0, 0], [0, 1]])

        # Define size of masks
        maskRows = rawIn.shape[1] / 2
        maskCols = rawIn.shape[0] / 2

        # Create mask dictionary
        maskBayer = dict()
        # Select correct mask
        if self.info['bayerType'] == 'rggb':
            maskBayer['R'] = np.tile(logicalOne, [maskRows, maskCols])
            maskBayer['Gr'] = np.tile(logicalTwo, [maskRows, maskCols])
            maskBayer['Gb'] = np.tile(logicalThree, [maskRows, maskCols])
            maskBayer['B'] = np.tile(logicalFour, [maskRows, maskCols])
        elif self.info['bayerType'] == 'grbg':
            maskBayer['Gr'] = np.tile(logicalOne, [maskRows, maskCols])
            maskBayer['R'] = np.tile(logicalTwo, [maskRows, maskCols])
            maskBayer['B'] = np.tile(logicalThree, [maskRows, maskCols])
            maskBayer['Gb'] = np.tile(logicalFour, [maskRows, maskCols])
        elif self.info['bayerType'] == 'gbrg':
            maskBayer['Gb'] = np.tile(logicalOne, [maskRows, maskCols])
            maskBayer['B'] = np.tile(logicalTwo, [maskRows, maskCols])
            maskBayer['R'] = np.tile(logicalThree, [maskRows, maskCols])
            maskBayer['Gr'] = np.tile(logicalFour, [maskRows, maskCols])
        elif self.info['bayerType'] == 'bggr':
            maskBayer['B'] = np.tile(logicalOne, [maskRows, maskCols])
            maskBayer['Gb'] = np.tile(logicalTwo, [maskRows, maskCols])
            maskBayer['Gr'] = np.tile(logicalThree, [maskRows, maskCols])
            maskBayer['R'] = np.tile(logicalFour, [maskRows, maskCols])
        else:
            print 'ERROR: Unrecognized Bayer pattern'
            sys.exit()

        # Create mask for G channel
        maskBayer['G'] = maskBayer['Gr'] | maskBayer['Gb']

        # Bilinear interpolation
        rgb = self.bilinearInterp(rawIn, maskBayer)

        # Replace first/last row/columns
        # First row
        rgb[:,0,:] = rgb[:,1,:]
        # Last row
        rgb[:,rgb.shape[1]-1,:] = rgb[:,rgb.shape[1]-2,:]
        # First column
        rgb[:,:,0] = rgb[:,:,1]
        # Last column
        rgb[:,:,rgb.shape[2]-1] = rgb[:,:,rgb.shape[2]-2]

        return rgb


    def bilinearInterp(self, rawIn, maskBayer):
        # Kernel for R and B channel
        kernelRB = np.array([[1,2,1],[2,4,2],[1,2,1]]).astype(np.float32) / 4
        kernelG = np.array([[1,2,1],[2,4,2],[1,2,1]]).astype(np.float32) / 8

        # Get R, G, and B channels from raw input
        # Note: this is equivalent to 'imfilter' in Matlab
        # Reference:
        # http://stackoverflow.com/questions/22142369/the-equivalent-function-of
        # -matlab-imfilter-in-python
        R = ndimage.filters.correlate(rawIn * maskBayer['R'], kernelRB)
        G = ndimage.filters.correlate(rawIn * maskBayer['G'], kernelG)
        B = ndimage.filters.correlate(rawIn * maskBayer['B'], kernelRB)

        # Concatenate. Size is (plane, row, column)
        rgb = np.array([R, G, B]).astype(np.float32)

        return rgb


    def correctShadingMesh(self, rawIn, meshRGB):
        # Set mesh scale
        if self.lensShading['meshScale'] == 0:
            meshScale = 128
        elif self.lensShading['meshScale'] == 1:
            meshScale = 64
        elif self.lensShading['meshScale'] == 2:
            meshScale = 32
        elif self.lensShading['meshScale'] == 3:
            meshScale = 16
        else:
            print 'ERROR: mesh scale should be a value between 0 and 3'
            sys.exit()

        # Scale mesh table
        meshTableScaled = meshScale / meshRGB

        # Upscale planes of scaled mesh table
        meshTableUpscaledR \
            = transform.resize(meshTableScaled[0,:,:],
                               rawIn.shape,
                               mode='reflect')\
            .astype(np.float32)
        meshTableUpscaledG \
            = transform.resize(meshTableScaled[1,:,:],
                               rawIn.shape,
                               mode='reflect')\
            .astype(np.float32)
        meshTableUpscaledB \
            = transform.resize(meshTableScaled[2,:,:],
                               rawIn.shape,
                               mode='reflect')\
            .astype(np.float32)
        # Concatenate. Size is (plane, row, column)
        meshTableUpscaled = np.array([meshTableUpscaledR,
                                      meshTableUpscaledG,
                                      meshTableUpscaledB]).astype(np.float32)

        # Create array of zeros
        rawGainMap = np.zeros(shape=rawIn.shape)

        # Adjust Bayer type
        if self.info['bayerType'] == 'rggb':
            rawGainMap[0::2,0::2] = meshTableUpscaled[0,0::2,0::2]
            rawGainMap[0::2,1::2] = meshTableUpscaled[1,0::2,1::2]
            rawGainMap[1::2,0::2] = meshTableUpscaled[1,1::2,0::2]
            rawGainMap[1::2,1::2] = meshTableUpscaled[2,1::2,1::2]
        elif self.info['bayerType'] == 'grbg':
            rawGainMap[0::2,1::2] = meshTableUpscaled[0,0::2,0::2]
            rawGainMap[0::2,0::2] = meshTableUpscaled[1,0::2,1::2]
            rawGainMap[1::2,1::2] = meshTableUpscaled[1,1::2,0::2]
            rawGainMap[1::2,0::2] = meshTableUpscaled[2,1::2,1::2]
        elif self.info['bayerType'] == 'gbrg':
            rawGainMap[1::2,0::2] = meshTableUpscaled[0,0::2,0::2]
            rawGainMap[0::2,0::2] = meshTableUpscaled[1,0::2,1::2]
            rawGainMap[1::2,1::2] = meshTableUpscaled[1,1::2,0::2]
            rawGainMap[0::2,1::2] = meshTableUpscaled[2,1::2,1::2]
        elif self.info['bayerType'] == 'bggr':
            rawGainMap[1::2,1::2] = meshTableUpscaled[0,0::2,0::2]
            rawGainMap[0::2,1::2] = meshTableUpscaled[1,0::2,1::2]
            rawGainMap[1::2,0::2] = meshTableUpscaled[1,1::2,0::2]
            rawGainMap[0::2,0::2] = meshTableUpscaled[2,1::2,1::2]
        else:
            print 'ERROR: Unrecognized Bayer pattern'
            sys.exit()

        # Apply gain map on raw image
        rawLSCCorr = rawIn / rawGainMap

        # Scale min and max
        rawLSCCorr = np.minimum(np.maximum(rawLSCCorr, 0), 1)

        return rawLSCCorr


    def saveToFile(self, meshRGBLeft, meshRGBRight):

        # Open file to write
        fID = open(self.misc['outFileName'], 'w')

        # Start writing
        fID.write('{\n')

        # Write pseudo-radial expansion data
        if not self.misc['saveMeshOnly'] and self.pseudoRadial['enable']:
            fID.write('    "pseudoRadialExpansion": {\n')
            fID.write('        "pixelThreshold": %d,\n' % self.pseudoRadial['pixelThreshold'])
            fID.write('        "distanceThreshold": %d,\n' % self.pseudoRadial['distanceThreshold'])
            fID.write('        "depth": %d\n' % self.pseudoRadial['depth'])
            fID.write('    },\n')

        # Write LSC data
        if not self.misc['saveMeshOnly']:
            fID.write('    "lensShadingStrength": {\n')
            fID.write('        "lumaStrength": %d,\n' % self.lensShading['lumaStrength'])
            fID.write('        "chromaStrength": %d\n' % self.lensShading['chromaStrength'])
            fID.write('    },\n')

        # Write ISP JSON data
        fID.write('    "lens_shading": {\n')
        fID.write('        "enable": 1,\n')
        fID.write('        "num_lens_shading_tables": 1,\n')
        fID.write('        "mesh_width": %d,\n' % int(self.lensShading['meshSize'][0] * 2))
        fID.write('        "mesh_height": %d,\n' % self.lensShading['meshSize'][1])
        fID.write('        "mesh_scale": %d,\n' % self.lensShading['meshScale'])

        for plane in xrange(0, meshRGBLeft.shape[0]):
            if plane == 0:
                fID.write('        "mesh_medium_tbl_r": [\n')
            elif plane == 1:
                fID.write('        "mesh_medium_tbl_g": [\n')
            elif plane == 2:
                fID.write('        "mesh_medium_tbl_b": [\n')
            for row in xrange(0, meshRGBLeft.shape[1]):
                fID.write('            ')
                # Write left mesh
                for col in xrange(0, meshRGBLeft.shape[2]):
                    fID.write('%d, ' % meshRGBLeft[plane, row, col])
                # Write right mesh
                for col in xrange(0, meshRGBRight.shape[2]):
                    if row == (meshRGBRight.shape[1] - 1) \
                            and col == (meshRGBRight.shape[2] - 1):
                        fID.write('%d\n' % meshRGBRight[plane, row, col])
                    elif col == (meshRGBRight.shape[2] - 1):
                        fID.write('%d, \n' % meshRGBRight[plane, row, col])
                    else:
                        fID.write('%d, ' % meshRGBRight[plane, row, col])

            if plane == (meshRGBLeft.shape[0] - 1):
                fID.write('        ]\n')
            else:
                fID.write('        ],\n')

        fID.write('    }\n')
        fID.write('}')

        fID.close()
