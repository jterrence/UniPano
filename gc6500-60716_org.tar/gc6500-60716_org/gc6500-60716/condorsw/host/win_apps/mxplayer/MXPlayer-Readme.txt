===============================================================================
README for MXPlayer
===============================================================================
Date: Aug 06, 2012

Version: v3.5

================================================================================
About:
===============================================================================
This program is designed to playback/capture the video and audio streams from
Maxim cameras. This supports Maxim's SkypeCam and RaptorCam designs.

================================================================================
System Requirements:
===============================================================================
# PC with Windows XP or Windows 7


================================================================================
Installing PC Modules:
================================================================================
NOTE: MXPlayer makes use of ffdshow filters for video decode. Hence ffdshow
filters should be installed on the Windows PC used for setup.

# Install ffdshow filters. 
  You can get it from http://ffdshow-tryout.sourceforge.net/

# Install the windows modules from the release.
  Unzip the �WindowsUtils.zip� to a folder on the Windows PC.
  Install the modules using Install.bat.
  For Windows XP, just double click on Install.bat file.
  For Windows 7 you have to install Install.bat as �Run As Administrator� mode.
  You can do this as follows,
   - Search for cmd.exe from Windows start menu.
   - right click on the cmd.exe icon, select �Run As administrator�
   - Now in the command prompt window, go to the folder where Install.bat is 
     located and execute that.
     ex: If you extract the zip file to C:\WindowsUtils, command will look like,
     >  C:\WindowsUtils\Install.bat

Install.bat will install the following modules,
  # MobiPropPage.ax
  # dump.ax
  # MXSplitter.ax
  # MXStatFilter.ax
  # proppage.dll

# Now connect the Camera to the PC.

# Once the system loads the UVC driver for the device, launch MXPlayer.

================================================================================
Description:
================================================================================
MXPlayer provides a simple UI with Play, Stop, Options, "Start Preview" and 
audio mute/unmute buttons on the main UI. Following are the main dialog windows
for the application MXPlayer.

================================================================================
Options dialog:
================================================================================
This dialog window provides the controls to configure different options 
available for the streams. This can be launched by click on the options button 
on the main window or go to menu, File->Options. If the device has different 
VID/PID than the Maxim's default IDs, specify the custom IDs under the Device 
section of the options dialog window.

Options window also allows you to save the streams individually by enabling
the "Save to file" checkbox for corresponding stream. For main stream (also 
called as Video Capture channel) you can select TS or AVC-Elementary format for
stream save option. For secondary (or preview stream) you can select any of the
formats provided under format drop down menu. Audio will be saved as RAW PCM 
samples. 

If the PC has multiple audio input devices, you have to select the Maxim's audio 
device from audio dev dropdown menu. Use the "configure..." button to configure 
the corresponding stream. For example, click on the "configure..." button under 
"Video Capture" group will open the property page where you can change stream 
configurations.

Please refer to "MobiPropPage-Readme.txt" for more information on the property 
page controls for MP2TS stream.

================================================================================
Message Settings dialog:
================================================================================
You can hide/show the debug message console using the message settings dialog
window. Go to menu, File->Message Settings...
You can set the verbose levels form the drop down menu.

================================================================================
Camera PU extension dialog:
================================================================================
This provides additional controls to control encoding paramets. 
Go to menu, File->Camera PU Extensions...

================================================================================
Test Config dialog:
================================================================================
Go to menu, Test->Test Config...
This is applicable for Skypecam reference design Maxim cameras only. You can 
enable the HFStat prints (prints on the MXPlayer debug console). Also you can 
set the threshold and check if the HFStat is in the desired range. If the 
threshold is set, the HFStat prints will have additional information wherher 
the stat value pass/fail with respect to the threshold value.


===============================================================================
Viewing Streams
===============================================================================

-------------------------------------------------------------------------------
Viewing Primary stream:
-------------------------------------------------------------------------------
STEPS
� Launch MXPlayer.exe
� Click on Play button.

-------------------------------------------------------------------------------
Configuring the Primary stream:
-------------------------------------------------------------------------------
STEPS
� Click on Option button or go to File->Options menu.
� Click on "Configure" button under "Video Capture" group.
� Use this property page to configure resolution, bitrate 
  and other video parameters.
  
-------------------------------------------------------------------------------
Saving video from Primary stream to file:
-------------------------------------------------------------------------------
STEPS
� Click on Option button or go to File->Options menu.
� Select the video format to save (TS or Elementary)
� Use "Browse" button to select the file name and location. If this is not 
  specified, video file ("Video.ts") will be stored in the folder where the 
  application resides.
� Enable "Save to file" check box.
� Save the settings by clicking "OK".
� Click on Play button. This will start recoding the stream to file.
� Click on Stop button to stop the recording.

NOTE: One cannot capture and view video simultaneously.

-------------------------------------------------------------------------------
Viewing Secondary stream:
-------------------------------------------------------------------------------
Introduction: The secondary streams support MJPEG, H.264 and RAW YUV.  
Appropriate stream can be selected by using the configuration tab. A variety 
of resolutions is supported from 1080p to QCIF and can be selected using the 
configuration of MXPlayer. There are no restrictions at this point of time on 
the resolutions for simultaneous streaming of video from the camera. 

You *might* hit into some performance issues if try to stream 1080p H.264 and 
1080p MJPEG simultaneously. If you do require two 1080p streams at the same 
time we can boost the clocks of the chips to make it possible.

Note: On RAW YUV streaming, some of the resolutions(ex. 160X120) where width 
is non-aligned to 64 pixels will not be rendered properly due to 
Video Renderer�s limitation.

STEPS
� Launch MXPlayer.exe
� Click on "Start Preview" button.

-------------------------------------------------------------------------------
Configuring the Secondary stream:
-------------------------------------------------------------------------------
STEPS
� Click on Option button or go to File->Options menu.
� Select the preview video format from the drop down menu.
  (Currently MPEG-TS, MJPG and YUY2 are supported)
� Click on "Configure" button under "Video Preview" group.
� Use this property page to configure video parameters for secondary channel.

-------------------------------------------------------------------------------
Saving video from Secondary stream to file:
-------------------------------------------------------------------------------
STEPS

� Click on Option button or go to File->Options menu.
� Use "Browse" button to select the file name and location. If this is not 
  specified,  video file("Preview.ts" or "Preview.mjpg" or �Preview.yuv�) 
  will be stored in the folder where the application resides.
� Enable "Save to file" check box.
� Save the settings by clicking "OK".
� Click on "Start Preview" button. This will start recoding the stream to file.
� Click on "Stop Preview" button to stop the recording.

NOTE: One cannot capture and view video simultaneously.

-------------------------------------------------------------------------------
Audio stream:
-------------------------------------------------------------------------------
Introduction: The Audio stream supports the RAW PCM stream with 24Khz, 16KHz 
and 8KHz sampling frequencies.

STEPS
� Launch MXPlayer.exe
� Click on Mic button. This will start the audio capture.
  
NOTE: By default, when you start the Primary video stream, the application 
starts the audio stream too.

-------------------------------------------------------------------------------
Saving audio to a file
-------------------------------------------------------------------------------
� Click on Option button or go to File->Options menu.
� Use "Browse" button to select the file name and location. 
  If this is not specified,  audio file("Audio.pcm") will be stored in the 
  folder where the application resides.
� Enable "Save to file" check box.
� Save the settings by clicking "OK".
� Click on Mic button. This will start recoding the stream to file.
� Click on Mic button to stop the recording.

===============================================================================
Viewing Video Stats
===============================================================================

STEPS
� Select File->Video Stats menu for video stats configuration dialog.
� Enable Video Stats checkbox.
� Set the desired "Number of regions" and "Macro blocks per regions"
� Enable "Display Video Stats" checkbox.
� Set the video resolution to VGA or 720p 
	� Click on Option button or go to File->Options menu.
	� Click on "Configure" button under "Video Capture" group.
	� Select VGA or 720p video resolution
� Now select the regions of interest on the MXPlayer grid window.
� Click on Play button.

NOTE: For testing user can set the criteria for pass or fail condition. Goto 
"Test->Test Config..." menu. Under "Video Stats" section provide the minimum and 
maximum threshold for the video stats. 
On the MXPlayer main window while you see the stats displayed for a region, the 
background color will change to GREEN if the region stat passed the criteria. 
Otherwise the background color will be set to RED.


