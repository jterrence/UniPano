===============================================================================
README for Windows module installation for MXPlayer
===============================================================================

NOTE: MXPlayer makes use of ffdshow filters for video decode. Hence ffdshow
filters should be installed on the Windows PC used for setup.

# Install ffdshow filters. 
  You can get it from http://ffdshow-tryout.sourceforge.net/

# Install the modules from the release.
  Unzip the release to a folder on the Windows PC.
  Install the modules using Install.bat.
  For Windows XP, just double click on install.bat file.
  For Windows 7/8 you have to run install.bat as Administrator.
  You can do this as follows,
   - Search for cmd.exe from Windows start menu.
   - right click on the cmd.exe icon, select �Run As administrator�
   - Now in the command prompt window, go to the folder where Install.bat is located and execute that.

install.bat will install the following modules,
  # MobiPropPage.ax
  # dump.ax
  # MXSplitter.ax
  # MXStatFilter.ax
  # proppage.dll

NOTE: Please refer to MXPlayer-Readme.txt for detailed instructions on using MXPlayer for video/audio
playback from Geo cameras.