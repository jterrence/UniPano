@echo off
IF "%1"=="-v" GOTO verbose_mode
echo Installing filters for MXPlayer...
regsvr32 /s proppage.dll
regsvr32 /u /s MobiPropPage.ax
regsvr32 /s MobiPropPage.ax
regsvr32 /u /s dump.ax
regsvr32 /s dump.ax
regsvr32 /u /s MXSplitter.ax
regsvr32 /s MXSplitter.ax
regsvr32 /u /s MXStatFilter.ax
regsvr32 /s MXStatFilter.ax
GOTO end

:verbose_mode
echo Installing filters for MXPlayer...
echo Verbose mode : ON
regsvr32 proppage.dll
regsvr32 /u /s MobiPropPage.ax
regsvr32 MobiPropPage.ax
regsvr32 /u /s dump.ax
regsvr32 dump.ax
regsvr32 /u /s MXSplitter.ax
regsvr32 MXSplitter.ax
regsvr32 /u /s MXStatFilter.ax
regsvr32 MXStatFilter.ax

:end
echo Installation complete...