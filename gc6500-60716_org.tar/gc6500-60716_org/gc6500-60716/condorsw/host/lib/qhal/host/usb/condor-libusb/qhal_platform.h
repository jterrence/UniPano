/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef __QHAL_PLATFORM_H
#define __QHAL_PLATFORM_H

#ifdef CHIP_REGS_DEFINED_LOCAL
#define   QCC_BID_PMU0           0x21

#define	Reg_M8_MEMS_MEM0_PMU_x_PartBaseSeg0_ADDRESS	0x200
#define	Reg_M8_MEMS_MEM0_PMU_x_PartBaseSeg1_ADDRESS	0x202
#define	Reg_M8_MEMS_MEM0_PMU_x_PartBaseSeg0_NUM_BYTES	0x2
#define	Reg_M8_MEMS_MEM0_PMU_x_VPATSegSize_ADDRESS	0x08
#define	Reg_M8_MEMS_MEM0_PMU_x_VPATSegSize_NUM_BYTES	0x2
#else
#include "m8_reg.h"
#include "m8_qcc_bid.h"
//#include "m8_regspec.ch"
#endif

#define REG_ADDR(reg)		Reg_M8_##reg##_ADDRESS
#define REG_BYTES(reg)		Reg_M8_##reg##_NUM_BYTES
#define REG_TYPE(reg)		Reg_M8_##reg##_t
#define REG_INDEX(reg)		Reg_M8_##reg##_I
#define REG_FIELD_LSB(reg)	RegF_M8_##reg##_LSB

#define SHIF_ADDR(reg)		REG_ADDR(SERIAL_HOSTIF_x_##reg)
#define SHIF_BYTES(reg)		REG_BYTES(SERIAL_HOSTIF_x_##reg)
#define SHIF_TYPE(reg)		REG_TYPE(SERIAL_HOSTIF_x_##reg)

#define PROD_ID_MERLIN		3
#define PROD_ID_FALCON		4
#define PROD_ID_RAPTOR		5
#define PROD_ID_CONDOR		6
#define QHAL_CONFIG_PROD_ID	PROD_ID_CONDOR

/*#define REG_ADDR(reg)		Reg_M7_##reg##_ADDRESS
#define REG_BYTES(reg)		Reg_M7_##reg##_NUM_BYTES
#define REG_TYPE(reg)		Reg_M7_##reg##_t
#define REG_INDEX(reg)		Reg_M7_##reg##_I
#define REG_FIELD_LSB(reg)	RegF_M7_##reg##_LSB

#define CHIPCTL_ADDR(reg)	REG_ADDR(CHIPCTL_x_##reg)
#define CHIPCTL_BYTES(reg)	REG_BYTES(CHIPCTL_x_##reg)
#define CHIPCTL_TYPE(reg)	REG_TYPE(CHIPCTL_x_##reg)*/

#define CHIPCTL_RESET_MS	1

//#define QMM_PARTITION_ID        125
#define XTENSA_PARTITION_ID	127

#define PARTITION_BASE_ADDR(i)				\
    (REG_ADDR(MEMS_MEM0_PMU_x_PartBaseSeg0) + (i)*	\
     (REG_ADDR(MEMS_MEM0_PMU_x_PartBaseSeg1) -		\
      REG_ADDR(MEMS_MEM0_PMU_x_PartBaseSeg0)))



#endif  // ifndef __QHAL_PLATFORM_H
