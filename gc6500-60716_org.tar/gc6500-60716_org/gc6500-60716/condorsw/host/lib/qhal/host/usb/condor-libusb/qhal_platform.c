/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <pthread.h>
#include "libusb.h"
#include "vendor_req.h"
#include "qhal_usb.h"
#include "qhal_priv.h"
#include "qhal_host.h"
#include "qhal_platform.h"

#define RESET_MS	5

typedef struct host_info_struct {
    int codec;
    libusb_context *usb_context;
    libusb_device_handle *usb_h;
    pthread_mutex_t *lock_p;
    qhalhost_interface_t interface;
    qhalhost_handle_t handle;
    struct host_info_struct *next;
} host_info_t;

static libusb_context *usb_context = NULL;
static host_info_t *host_info_head = NULL;
static unsigned int host_info_ref_count = 0;


/****************************************************************************
 *
 * USB Functions
 *
 ****************************************************************************/

static int usb_configure(host_info_t *host_p)
{
    int config;

    // Set Configuration:
    if (libusb_get_configuration(host_p->usb_h, &config)) {
	return QHAL_ERR_IO;
    }

    if (config != USB_CFG_QHAL) {
	if (libusb_set_configuration(host_p->usb_h, USB_CFG_QHAL)) {
	    return QHAL_ERR_IO;
	}
    }

#if 0
    // Claim Interface:
    if (r = libusb_claim_interface(host_p->usb_h, USB_IF_QHAL)) {
	printf("claim intf error %d\n", r);
	return QHAL_ERR_IO;
    }
#endif

    return QHAL_SUCCESS;
}

static int usb_reset(host_info_t *host_p)
{
    int ret = libusb_reset_device(host_p->usb_h);

    // Re-enumeration is necessary:
    if (ret == LIBUSB_ERROR_NOT_FOUND) {
	libusb_close(host_p->usb_h);
	host_p->usb_h = libusb_open_device_with_vid_pid(usb_context,
							USB_VENDOR_ID_GEO, USB_DEVICE_ID_GEO_CONDOR);
	if (host_p->usb_h == NULL)
	    return QHAL_ERR_NODEV;

	ret = usb_configure(host_p);
    }
    return ret ? QHAL_ERR_IO : QHAL_SUCCESS;
}

inline int usb_reset_ep(host_info_t *host_p, qhal_usb_ep_t ep)
{
    int ret = libusb_clear_halt(host_p->usb_h, ep);

    return ret ? QHAL_ERR_IO : QHAL_SUCCESS;
}

static int usb_rw(host_info_t *host_p,//TODO
		  qhal_usb_vr_hdr_t *vr_p, void *data_p)
{
    vr_p->data = 0;
    vr_p->ret = 0;
    return QHAL_SUCCESS;
}


/****************************************************************************
 *
 * Host Init
 *
 ****************************************************************************/

qhalhost_handle_t qhalhost_open(int codec, QHALHOST_INTERFACE interface)
{
    static qhalhost_handle_t handle_index = 1;
    host_info_t *host_info_tail, *host_p;
    static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
    int ret;
    struct libusb_device **devs;
    struct libusb_device *dev;
    int i;

    if (codec != 0)
	return QHAL_ERR_BADPARAM;

    switch (interface) {
    case QHALHOST_BUS_CSR:
    case QHALHOST_BUS_EM1:
    case QHALHOST_BUS_EM1_STREAM:
    case QHALHOST_INT_MBOX0:
    case QHALHOST_INT_MBOX1:
	break;

    default:
	return QHAL_ERR_NA;
    }

    host_p = (host_info_t *)malloc(sizeof(host_info_t));
    if (host_p == NULL)
	return QHAL_ERR_MEM;

    // First opened handle, initialize device:
    //
    if (host_info_head == NULL) {
	pthread_mutexattr_t attr;

	// Set locking mechanism to be process-shared:
	//
	pthread_mutexattr_init(&attr);
	pthread_mutexattr_setpshared(&attr, PTHREAD_PROCESS_SHARED);
	ret = pthread_mutex_init(&lock, &attr);
	pthread_mutexattr_destroy(&attr);
	if (ret)
        {
	    free(host_p);
	    return QHAL_ERR_INIT;
        }

	// Init the lib and grab our reference:
	ret = libusb_init(&usb_context);
	if (ret) {
	    pthread_mutex_destroy(&lock);
	    EXIT_WITH_ERROR(QHAL_ERR_INIT);
	}

	libusb_get_device_list(NULL, &devs);
	i = 0;
	while ( (dev = devs[i++]) ) { 
	    struct libusb_device_descriptor desc;
	    libusb_get_device_descriptor(dev, &desc);
	    //printf("vid %d pid %d\n", desc.idVendor, desc.idProduct);
	    if (desc.idVendor == USB_VENDOR_ID_GEO && desc.idProduct == USB_DEVICE_ID_GEO_CONDOR) {
		libusb_open(dev, &host_p->usb_h);
		break;
	    }
	}
	libusb_free_device_list(devs, 1);

	if (host_p->usb_h == NULL) {
	    pthread_mutex_destroy(&lock);
	    libusb_exit(usb_context);
	    usb_context = NULL;
	    EXIT_WITH_ERROR(QHAL_ERR_NODEV);
	}

	if (usb_configure(host_p)) {
	    libusb_close(host_p->usb_h);
	    libusb_exit(usb_context);
	    usb_context = NULL;
	    EXIT_WITH_ERROR(QHAL_ERR_INIT);
	}
    }
    else {
	host_p->usb_h = host_info_head->usb_h;
    }

    host_p->usb_context = usb_context;
    host_p->lock_p = &lock;
    host_p->codec = codec;
    host_p->interface = interface;
    host_p->handle = handle_index++;
    host_p->next = NULL;
    APPEND_INFO(host_info_head, host_info_tail, host_p);
    host_info_ref_count++;
    return host_p->handle;

 err_exit:
    free((void *)host_p);
    return ret;
}

int qhalhost_close(qhalhost_handle_t host_h)
{
    host_info_t *host_info_tail, *host_p;

    FIND_INFO(host_info_head, host_p, host_h);

    // Last one cleans up:
    //
    if (host_info_ref_count == 1) {
	pthread_mutex_destroy(host_p->lock_p);
	libusb_release_interface(host_p->usb_h, USB_IF_QHAL);
	libusb_close(host_p->usb_h);
	libusb_exit(usb_context);
	usb_context = NULL;
    }

    REMOVE_INFO(host_info_head, host_info_tail, host_p);
    free((void *)host_p);
    host_info_ref_count--;
    return QHAL_SUCCESS;
}

qhalhost_busmode_t qhalhost_getbusmode(qhalhost_handle_t host_h)
{
    return QHALHOST_BUSMODE_PIO_POLL;
}

int qhalhost_swapmode(qhalhost_handle_t host_h, int swapmode)
{
    return QHAL_ERR_NA;
}


/****************************************************************************
 *
 * OS Utils
 *
 ****************************************************************************/

int qhalhost_wait(qhalhost_handle_t host_h, unsigned long time_ms)
{
    if (time_ms)
	if (usleep(time_ms*1000) < 0)
	    return QHAL_ERR_IO;
    return QHAL_SUCCESS;
}

int qhalhost_wait_ns(qhalhost_handle_t host_h, unsigned long time_ns)
{
    struct timespec req = { 0, time_ns };

    if (time_ns)
	if (nanosleep(&req, NULL) < 0)
	    return QHAL_ERR_IO;
    return QHAL_SUCCESS;
}


int qhalhost_lock(qhalhost_handle_t host_h)
{
    host_info_t *host_p;

    FIND_INFO(host_info_head, host_p, host_h);
    if (pthread_mutex_lock(host_p->lock_p))
	return QHAL_ERR_LOCK;
    return QHAL_SUCCESS;
}

int qhalhost_unlock(qhalhost_handle_t host_h)
{
    host_info_t *host_p;

    FIND_INFO(host_info_head, host_p, host_h);
    if (pthread_mutex_unlock(host_p->lock_p))
	return QHAL_ERR_LOCK;
    return QHAL_SUCCESS;
}


void *qhalhost_malloc(qhalhost_handle_t host_h, int size)
{
    return malloc((size_t)size);
}

int qhalhost_free(qhalhost_handle_t host_h, void *ptr)
{
    if (ptr != NULL)
	free(ptr);
    return QHAL_SUCCESS;
}


/****************************************************************************
 *
 * Bus writes
 *
 ****************************************************************************/

int qhalhost_write(qhalhost_handle_t host_h,
		   unsigned long addr, unsigned long data, int width)
{
    host_info_t *host_p;
    int ret;
    uint16_t addr_hi;
    uint16_t addr_lo;
    unsigned char status[64];
    unsigned char cmd_sta;

    FIND_INFO(host_info_head, host_p, host_h);
    switch (width) {
    case 1:
	data &= 0xFF;
	break;

    case 2:
	data &= 0xFFFF;
	break;

    case 4:
	break;

    default:
	return QHAL_ERR_BADPARAM;
    }

    addr_hi = (uint16_t)(addr >> 16);
    addr_lo = (uint16_t)(addr & 0xFFFF);

    ret = libusb_control_transfer(host_p->usb_h,
				  /* bmRequestType */
				  (LIBUSB_ENDPOINT_OUT | LIBUSB_REQUEST_TYPE_VENDOR |
				   LIBUSB_RECIPIENT_INTERFACE),
				  /* bRequest      */ MEM_WRITE,
				  /* wValue        */ (uint16_t)addr_hi,
				  /* MSB 4 bytes   */
				  /* wIndex        */ (uint16_t)addr_lo,
				  /* Data          */ (unsigned char *)&data,
				  /* wLength       */ (uint16_t)width,
				  /* timeout       */ 0   
				  );
    if (ret < 0) {
	printf("qhalhost_write error: vendor req MEM_WRITE. %d\n", ret);
	return ret;
    }

    ret = libusb_control_transfer(host_p->usb_h,
				  /* bmRequestType */
				  (LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_VENDOR |
				   LIBUSB_RECIPIENT_INTERFACE),
				  /* bRequest      */ MEM_WRITE_STATUS,
				  /* wValue        */ 0,
				  /* wIndex        */ 0,
				  /* Data          */ status,
				  /* wLength       */ sizeof(status),
				  /* timeout       */ 0
				  );
    
    if (ret < 0) {
        printf("qhalhost_write error: vendor req MEM_READ_STATUS. %d\n", ret);
        return ret;
    }

    cmd_sta = *(unsigned char *)status;
    //if (cmd_sta >= MXCAM_ERR_FAIL) {
    if (cmd_sta >= 128) {
        return cmd_sta;
    }   
    
    return QHAL_SUCCESS;
}

int qhalhost_write_block(qhalhost_handle_t host_h,
			 unsigned long addr, unsigned long *data_p, int width, int length)
{
    host_info_t *host_p;
    int ret;
    uint16_t bytes_transfer;
    uint16_t addr_hi;
    uint16_t addr_lo;
    unsigned char status[64];
    unsigned char cmd_sta;

    if (data_p == NULL)
	return QHAL_ERR_BADPARAM;

    FIND_INFO(host_info_head, host_p, host_h);
    switch (width) {
    case 1:
    case 2:
    case 4:
	break;

    default:
	return QHAL_ERR_BADPARAM;
    }

    addr_hi = (uint16_t)(addr >> 16);
    addr_lo = (uint16_t)(addr & 0xFFFF);
    bytes_transfer = (uint16_t)(width * length);

    ret = libusb_control_transfer(host_p->usb_h,
				  /* bmRequestType */
				  (LIBUSB_ENDPOINT_OUT | LIBUSB_REQUEST_TYPE_VENDOR |
				   LIBUSB_RECIPIENT_INTERFACE),
				  /* bRequest      */ MEM_WRITE,
				  /* wValue        */ (uint16_t)addr_hi,
				  /* MSB 4 bytes   */
				  /* wIndex        */ (uint16_t)addr_lo,
				  /* Data          */ (unsigned char *)data_p,
				  /* wLength       */ bytes_transfer,
				  /* timeout       */ 0   
				  );
    if (ret < 0) {
	printf("qhalhost_write_block error: vendor req MEM_WRITE. %d\n", ret);
	return ret;
    }

    ret = libusb_control_transfer(host_p->usb_h,
				  /* bmRequestType */
				  (LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_VENDOR |
				   LIBUSB_RECIPIENT_INTERFACE),
				  /* bRequest      */ MEM_WRITE_STATUS,
				  /* wValue        */ 0,
				  /* wIndex        */ 0,
				  /* Data          */ status,
				  /* wLength       */ sizeof(status),
				  /* timeout       */ 0
				  );
    
    if (ret < 0) {
        printf("qhalhost_write_block error: vendor req MEM_WRITE_STATUS. %d\n", ret);
        return ret;
    }

    cmd_sta = *(unsigned char *)status;
    //if (cmd_sta >= MXCAM_ERR_FAIL) {
    if (cmd_sta >= 128) {
        return cmd_sta;
    }      

    return QHAL_SUCCESS;
}


/****************************************************************************
 *
 * Bus reads
 *
 ****************************************************************************/

int qhalhost_read(qhalhost_handle_t host_h,
		  unsigned long addr, unsigned long *data_p, int width)
{
    host_info_t *host_p;
    int mask, ret;
    unsigned char wid = (unsigned char)width;
    uint16_t addr_hi;
    uint16_t addr_lo;

    if (data_p == NULL)
	return QHAL_ERR_BADPARAM;

    FIND_INFO(host_info_head, host_p, host_h);
    switch (width) {
    case 1:
	mask = 0xFF;
	break;

    case 2:
	mask = 0xFFFF;
	break;

    case 4:
	mask = 0xFFFFFFFF;
	break;

    default:
	return QHAL_ERR_BADPARAM;
    }

    addr_hi = (uint16_t)(addr >> 16);
    addr_lo = (uint16_t)(addr & 0xFFFF);

    ret = libusb_control_transfer(host_p->usb_h,
				  /* bmRequestType */
				  (LIBUSB_ENDPOINT_OUT | LIBUSB_REQUEST_TYPE_VENDOR |
				   LIBUSB_RECIPIENT_INTERFACE),
				  /* bRequest      */ MEM_READ,
				  /* wValue        */ (uint16_t)addr_hi,
				  /* MSB 4 bytes   */
				  /* wIndex        */ (uint16_t)addr_lo,
				  /* Data          */ &wid,
				  /* wLength       */ 1,
				  /* timeout       */ 0   
				  );
    if (ret < 0) {
	printf("qhalhost_read error: vendor req MEM_READ. %d\n", ret);
	return ret;
    }

    ret = libusb_control_transfer(host_p->usb_h,
				  /* bmRequestType */
				  (LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_VENDOR |
				   LIBUSB_RECIPIENT_INTERFACE),
				  /* bRequest      */ MEM_READ_STATUS,
				  /* wValue        */ 0,
				  /* wIndex        */ 0,
				  /* Data          */ (unsigned char*)data_p,
				  /* wLength       */ (uint16_t)width,
				  /* timeout       */ 0
				  );
    
    if (ret < 0) {
        printf("qhalhost_read error: vendor req MEM_READ_STATUS. %d\n", ret);
        return ret;
    }

    *data_p &= mask;    

    return QHAL_SUCCESS;
}

int qhalhost_read_block(qhalhost_handle_t host_h,
			unsigned long addr, unsigned long *data_p, int width, int length)
{
    host_info_t *host_p;
    int ret;
    uint16_t addr_hi;
    uint16_t addr_lo;
    int txlength = 0;
    //unsigned char *p = (unsigned char *)data_p;

    //printf("qhalhost_read_block: addr 0x%x width %d length %d\n", (int)addr, width, length);

    if (data_p == NULL)
	return QHAL_ERR_BADPARAM;

    FIND_INFO(host_info_head, host_p, host_h);

    addr_hi = (uint16_t)(addr >> 16);
    addr_lo = (uint16_t)(addr & 0xFFFF);

    txlength = (width * length);

    ret = libusb_control_transfer(host_p->usb_h,
				  /* bmRequestType */
				  (LIBUSB_ENDPOINT_OUT | LIBUSB_REQUEST_TYPE_VENDOR |
				   LIBUSB_RECIPIENT_INTERFACE),
				  /* bRequest      */ MEM_READ,
				  /* wValue        */ (uint16_t)addr_hi,
				  /* MSB 4 bytes   */
				  /* wIndex        */ (uint16_t)addr_lo,
				  /* Data          */ (unsigned char *)&txlength,
				  /* wLength       */ sizeof(int),
				  /* timeout       */ 0   
				  );
    if (ret < 0) {
	printf("qhalhost_read_block error: vendor req MEM_READ. %d\n", ret);
	return ret;
    }

    ret = libusb_control_transfer(host_p->usb_h,
				  /* bmRequestType */
				  (LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_VENDOR |
				   LIBUSB_RECIPIENT_INTERFACE),
				  /* bRequest      */ MEM_READ_STATUS,
				  /* wValue        */ 0,
				  /* wIndex        */ 0,
				  /* Data          */ (unsigned char*)data_p,
				  /* wLength       */ txlength,
				  /* timeout       */ 0
				  );
    
    if (ret < 0) {
        printf("qhalhost_read_block error: vendor req MEM_READ_STATUS. %d\n", ret);
        return ret;
    }
#if 0
    printf("qhalhost_read_block: data:\n");
    p = data_p;
    for (ret = 0; ret < length; ret++) {
	int j;
	for (j = 0; j < width; j++) {
	    printf("%02x ", *p++);
	}
	printf("\n");
    }
#endif

    return QHAL_SUCCESS;
}


/****************************************************************************
 *
 * Platform-specific QCC access
 *
 ****************************************************************************/

#define QCC_BID_OK(bid)		((bid & 0xFF) == bid)
#define QCC_ADDR_OK(addr)	((addr & 0xFFFF) == addr)

int qhalhost_write_qcc(qhalhost_handle_t host_h, unsigned long bid,
		       unsigned long addr, unsigned long data, int width)
{
    host_info_t *host_p;
    unsigned char status[64];
    unsigned char cmd_sta;
    int ret = QHAL_SUCCESS;

    if (!QCC_BID_OK(bid) || !QCC_ADDR_OK(addr))
	return QHAL_ERR_BADPARAM;

    FIND_INFO(host_info_head, host_p, host_h);
    switch (width) {
    case 1:
	data &= 0xFF;
	break;

    case 2:
	data &= 0xFFFF;
	break;

    case 4:
	break;

    default:
	return QHAL_ERR_BADPARAM;
    }

    ret = libusb_control_transfer(host_p->usb_h,
				  /* bmRequestType */
				  (LIBUSB_ENDPOINT_OUT | LIBUSB_REQUEST_TYPE_VENDOR |
				   LIBUSB_RECIPIENT_INTERFACE),
				  /* bRequest      */ QCC_WRITE,
				  /* wValue        */ (uint16_t)bid,
				  /* MSB 4 bytes   */
				  /* wIndex        */ (uint16_t)addr,
				  /* Data          */ (unsigned char *)&data,
				  /* wLength       */ (uint16_t)width,
				  /* timeout       */ 0   
				);
    if (ret < 0) {
	printf("qhalhost_write_qcc error: vendor req QCC_WRITE. %d\n", ret);
	return ret;
    }

    ret = libusb_control_transfer(host_p->usb_h,
				  /* bmRequestType */
				  (LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_VENDOR |
				   LIBUSB_RECIPIENT_INTERFACE),
				  /* bRequest      */ QCC_WRITE_STATUS,
				  /* wValue        */ 0,
				  /* wIndex        */ 0,
				  /* Data          */ status,
				  /* wLength       */ sizeof(status),
				  /* timeout       */ 0
				  );
    
    if (ret < 0) {
        printf("qhalhost_write_qcc error: vendor req QCC_WRITE_STATUS. %d\n", ret);
        return ret;
    }
    
    cmd_sta = *(unsigned char *)status;
    //if (cmd_sta >= MXCAM_ERR_FAIL) {
    if (cmd_sta >= 128) {
        return cmd_sta;
    }

    return QHAL_SUCCESS;
}

int qhalhost_read_qcc(qhalhost_handle_t host_h, unsigned long bid,
		      unsigned long addr, unsigned long *data_p, int width)
{
    host_info_t *host_p;
    int mask, ret = QHAL_SUCCESS;

    if (data_p == NULL)
	return QHAL_ERR_BADPARAM;

    if (!QCC_BID_OK(bid) || !QCC_ADDR_OK(addr))
	return QHAL_ERR_BADPARAM;

    FIND_INFO(host_info_head, host_p, host_h);
    switch (width) {
    case 1:
	mask = 0xFF;
	break;

    case 2:
	mask = 0xFFFF;
	break;

    case 4:
	mask = 0xFFFFFFFF;
	break;

    default:
	return QHAL_ERR_BADPARAM;
    }

    ret = libusb_control_transfer(host_p->usb_h,
				  /* bmRequestType */
				  (LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_VENDOR |
				   LIBUSB_RECIPIENT_INTERFACE),
				  /* bRequest      */ QCC_READ,
				  /* wValue        */ (uint16_t)bid,
				  /* MSB 4 bytes   */
				  /* wIndex        */ (uint16_t)addr,
				  /* Data          */ (unsigned char*)data_p,
				  /* wLength       */ (uint16_t)width,
				  /* timeout       */ 0   
				  );
    if (ret < 0) {
	printf("qhalhost_read_qcc error: vendor req QCC_READ. %d\n", ret);
	return ret;
    }

    *data_p &= mask;

    return QHAL_SUCCESS;
}


/****************************************************************************
 *
 * Platform Functions
 *
 ****************************************************************************/

int qhalhost_wait_interrupt(qhalhost_handle_t host_h, int timeout)
{
    host_info_t *host_p;

    // Only mailbox interrupts implemented:
    FIND_INFO(host_info_head, host_p, host_h);
    switch (host_p->interface) {
    case QHALHOST_INT_MBOX0:
	break;

    case QHALHOST_INT_MBOX1:
	break;

    default:
	return QHAL_ERR_NA;
    }
    return QHAL_SUCCESS;
}

int qhalhost_ack_interrupt(qhalhost_handle_t host_h)
{
    host_info_t *host_p;

    // Only mailbox interrupts implemented:
    FIND_INFO(host_info_head, host_p, host_h);
    switch (host_p->interface) {
    case QHALHOST_INT_MBOX0:
    case QHALHOST_INT_MBOX1:
	// mailbox interrupts are cleared through qcc mbox
	return QHAL_SUCCESS;

    default:
	return QHAL_ERR_NA;
    }
}

int qhalhost_reset(qhalhost_handle_t host_h)
{
    host_info_t *host_p;
    qhal_usb_vr_hdr_t vr;
    int ret;

    FIND_INFO(host_info_head, host_p, host_h);

    // Request the reset:
    //vr.vr = USB_VR_RESET_CHIP;
    ret = usb_rw(host_p, &vr, NULL);
    EXIT_ON_EITHER_ERROR(ret, vr.ret);

    // Wait for the interface to come back:
    ret = qhalhost_wait(host_h, RESET_MS);
    EXIT_ON_ERROR(ret);

    // Now reset our nexus:
    ret = usb_reset(host_p);

 err_exit:
    return ret;
}

