/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef __QHAL_USB_H
#define __QHAL_USB_H

// Vendor and product identification:
#define USB_VENDOR_ID_GEO		0x29fe
#define USB_DEVICE_ID_GEO_CONDOR	0x4d53

// Configurations and interfaces:
#define USB_CFG_QHAL	1
#define USB_IF_QHAL	0

// Endpoints (per configuration?):
typedef enum {
    USB_EP_CTRL	= 0x00,
    USB_EP_OUT0	= 0x01,
    USB_EP_IN0	= 0x82,
} qhal_usb_ep_t;

#if 0
// Vendor Request codes and structures:
typedef enum {
    USB_VR_RESET_CHIP,
    USB_VR_READ,
    USB_VR_READ_BLOCK,
    USB_VR_READ_QCC,
    USB_VR_WRITE,
    USB_VR_WRITE_BLOCK,
    USB_VR_WRITE_QCC,
} qhal_usb_vr_t;
#endif

// This structure generalizes data so it can be passed
// both in and out of a function locally or prepended
// to a control packet. In reality doing the latter is
// a data copy so for most block transfers the bulk
// endpoints make more sense.
typedef struct {
    //usb_vr_t vr;
    VEND_CMD_LIST vr;
    int ret;	// return code (0 = success)
    int width;	// 1, 2, or 4 byte element width
    int length;	// number of elements to transfer
    unsigned long bid;	// block ID (if required)
    unsigned long addr;	// physical address of request
    unsigned long data;	// addressed value (if length==1)
} qhal_usb_vr_hdr_t;

#endif
