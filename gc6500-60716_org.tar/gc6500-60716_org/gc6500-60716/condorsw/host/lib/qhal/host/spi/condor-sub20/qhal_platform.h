/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef __QHAL_PLATFORM_H
#define __QHAL_PLATFORM_H

#include "m8_reg.h"
#include "m8_qcc_bid.h"
//#include "m8_regspec.ch"

#define REG_ADDR(reg)		Reg_M8_##reg##_ADDRESS
#define REG_BYTES(reg)		Reg_M8_##reg##_NUM_BYTES
#define REG_TYPE(reg)		Reg_M8_##reg##_t
#define REG_INDEX(reg)		Reg_M8_##reg##_I
#define REG_FIELD_LSB(reg)	RegF_M8_##reg##_LSB

#define SHIF_ADDR(reg)		REG_ADDR(SERIAL_HOSTIF_x_##reg)
#define SHIF_BYTES(reg)		REG_BYTES(SERIAL_HOSTIF_x_##reg)
#define SHIF_TYPE(reg)		REG_TYPE(SERIAL_HOSTIF_x_##reg)

#define PROD_ID_MERLIN		3
#define PROD_ID_FALCON		4
#define PROD_ID_RAPTOR		5
#define PROD_ID_CONDOR		6
#define QHAL_CONFIG_PROD_ID	PROD_ID_CONDOR

#endif  // ifndef __QHAL_PLATFORM_H
