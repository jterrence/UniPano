/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <pthread.h>
#include "libsub.h"
#include "qhal_priv.h"
#include "qhal_host.h"
#include "qhal_platform.h"

#define DbgTrace(str...) //printf(str)

// Set this and wire MOSI to MISO for basic r/w verification
#undef TEST_LOOPBACK
//#define TEST_LOOPBACK	1


#define SPI_MODE_0		(SPI_CPOL_RISE | SPI_SMPL_SETUP)
#define SPI_MODE_1		(SPI_CPOL_FALL | SPI_SMPL_SETUP)
#define SPI_MODE_2		(SPI_CPOL_RISE | SPI_SETUP_SMPL)
#define SPI_MODE_3		(SPI_CPOL_FALL | SPI_SETUP_SMPL)
#define SPI_MODE		SPI_MODE_0

//FIXME: SPI_ORDER may need to be switched based on endianness
#define SPI_ORDER		SPI_MSB_FIRST
//#define SPI_ORDER		SPI_LSB_FIRST

#define SPI_ORDER_MASK	(SPI_LSB_FIRST | SPI_MSB_FIRST)

//#define SPI_SPEED		SPI_CLK_125KHZ
//#define SPI_SPEED		SPI_CLK_250KHZ
//#define SPI_SPEED		SPI_CLK_500KHZ
//#define SPI_SPEED		SPI_CLK_1MHZ
//#define SPI_SPEED		SPI_CLK_2MHZ
//#define SPI_SPEED		SPI_CLK_4MHZ
#define SPI_SPEED		SPI_CLK_8MHZ

#define SPI_SPEED_MASK	( SPI_CLK_125KHZ	\
						| SPI_CLK_250KHZ	\
						| SPI_CLK_500KHZ	\
						| SPI_CLK_1MHZ		\
						| SPI_CLK_2MHZ		\
						| SPI_CLK_4MHZ		\
						| SPI_CLK_8MHZ )


#define RAPTOR_SPI_ADDR(x)		(((x)>>2) & 0x1F)
#define RAPTOR_SPI_ADDR_OK(x)	(RAPTOR_SPI_ADDR(x) == ((x)>>2))

#define RAPTOR_SPI_WRITE	0x00
#define RAPTOR_SPI_READ		0x80

#define RAPTOR_SPI_LEN_0	0x00
#define RAPTOR_SPI_LEN_1	0x20
#define RAPTOR_SPI_LEN_2	0x40
#define RAPTOR_SPI_LEN_4	0x60


#define RAPTOR_DATA_ENDIAN_BIG		0x00
#define RAPTOR_DATA_ENDIAN_LITTLE	0x81
#define RAPTOR_BIT_ENDIAN_BIG		0x00
#define RAPTOR_BIT_ENDIAN_LITTLE	0x42

//FIXME: We need to test endianness on FPGA with both i686 and ppc targets!!
#if defined(__BIG_ENDIAN__)
#define RAPTOR_DATA_ENDIAN	RAPTOR_DATA_ENDIAN_BIG
#define RAPTOR_BIT_ENDIAN	RAPTOR_BIT_ENDIAN_BIG
#endif

#if defined(__LITTLE_ENDIAN__)
#define RAPTOR_DATA_ENDIAN	RAPTOR_DATA_ENDIAN_LITTLE
#define RAPTOR_BIT_ENDIAN	RAPTOR_BIT_ENDIAN_BIG
#endif

#define RAPTOR_CONFIG_ADDR	0x04
#define RAPTOR_CONFIG_BYTE	(RAPTOR_DATA_ENDIAN | RAPTOR_BIT_ENDIAN)

// buffer up data to be transferred to the usb-spi interface
#define SPI_BUFFER_SZ 640

/****************************************************************************
 *
 * Host Init
 *
 ****************************************************************************/

typedef struct host_info_struct {
	int codec;
	sub_handle sub_h;
	pthread_mutex_t *lock_p;
    qhalhost_interface_t interface;
	qhalhost_handle_t handle;
	struct host_info_struct *next;

    unsigned char out_buffer[SPI_BUFFER_SZ];
    unsigned char in_buffer[SPI_BUFFER_SZ];
    int buffer_index;

} host_info_t;

static host_info_t *host_info_head = NULL;
static unsigned int host_info_ref_count = 0;

qhalhost_handle_t qhalhost_open(int codec, QHALHOST_INTERFACE interface)
{
	static qhalhost_handle_t handle_index = 1;
	host_info_t *host_info_tail, *host_p;
	static sub_handle sub_h;
	static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
	unsigned char outbuf[2] = {
		(RAPTOR_SPI_WRITE | RAPTOR_SPI_LEN_1 | RAPTOR_CONFIG_ADDR),
		RAPTOR_CONFIG_BYTE
	};
	int ret;

	if (codec != 0)
		return QHAL_ERR_BADPARAM;

	switch (interface) {
	case QHALHOST_BUS_CSR:
    case QHALHOST_BUS_EM1:
    case QHALHOST_BUS_EM1_STREAM:
	case QHALHOST_INT_MBOX0:
	case QHALHOST_INT_MBOX1:
		break;

	default:
		return QHAL_ERR_NA;
	}

	host_p = (host_info_t *)malloc(sizeof(host_info_t));
	if (host_p == NULL)
		return QHAL_ERR_MEM;

	// First opened handle, initialize device:
	//
	if (host_info_head == NULL) {
		pthread_mutexattr_t attr;

		// Set locking mechanism to be process-shared:
		//
		pthread_mutexattr_init(&attr);
		pthread_mutexattr_setpshared(&attr, PTHREAD_PROCESS_SHARED);
		ret = pthread_mutex_init(&lock, &attr);
		pthread_mutexattr_destroy(&attr);
		if (ret){
			free(host_p);
			return QHAL_ERR_INIT;
		}
		// Initialize sub20 library:
		//
		sub_h = sub_open(0);
		if (sub_h == NULL) {
			pthread_mutex_destroy(&lock);
			free(host_p);
			return QHAL_ERR_INIT;
		}
		if (sub_spi_config(sub_h,
				(SPI_ENABLE | SPI_MODE | SPI_ORDER | SPI_SPEED), NULL))
			EXIT_WITH_ERROR(QHAL_ERR_IO);
		if (sub_spi_transfer(sub_h,
							(char *)outbuf, NULL, 2, SS_CONF(0,SS_LO)))
			EXIT_WITH_ERROR(QHAL_ERR_IO);
	}

	host_p->sub_h = sub_h;
	host_p->lock_p = &lock;
	host_p->codec = codec;
    host_p->interface = interface;
	host_p->handle = handle_index++;
	host_p->next = NULL;
	APPEND_INFO(host_info_head, host_info_tail, host_p);
    host_info_ref_count++;
	return host_p->handle;

err_exit:
	if (sub_h)
		sub_close(sub_h);

	free(host_p);
	return ret;
}

int qhalhost_close(qhalhost_handle_t host_h)
{
	host_info_t *host_info_tail, *host_p;

	FIND_INFO(host_info_head, host_p, host_h);

	// Last one cleans up:
	//
	if (host_info_ref_count == 1) {
		pthread_mutex_destroy(host_p->lock_p);
		sub_close(host_p->sub_h);
	}

	REMOVE_INFO(host_info_head, host_info_tail, host_p);
	free((void *)host_p);
    host_info_ref_count--;
	return QHAL_SUCCESS;
}

qhalhost_busmode_t qhalhost_getbusmode(qhalhost_handle_t host_h)
{
	return QHALHOST_BUSMODE_PIO_POLL;
}

int qhalhost_swapmode(qhalhost_handle_t host_h, int swapmode)
{
	return QHAL_ERR_NA;
}


/****************************************************************************
 *
 * OS Utils
 *
 ****************************************************************************/

int qhalhost_wait(qhalhost_handle_t host_h, unsigned long time_ms)
{
	if (time_ms)
		if (usleep(time_ms*1000) < 0)
		    return QHAL_ERR_IO;
	return QHAL_SUCCESS;
}

int qhalhost_wait_ns(qhalhost_handle_t host_h, unsigned long time_ns)
{
	struct timespec req = { 0, time_ns };

	if (time_ns)
		if (nanosleep(&req, NULL) < 0)
			return QHAL_ERR_IO;
	return QHAL_SUCCESS;
}


int qhalhost_lock(qhalhost_handle_t host_h)
{
	host_info_t *host_p;

	FIND_INFO(host_info_head, host_p, host_h);
	if (pthread_mutex_lock(host_p->lock_p))
		return QHAL_ERR_LOCK;
	return QHAL_SUCCESS;
}

int qhalhost_unlock(qhalhost_handle_t host_h)
{
	host_info_t *host_p;

	FIND_INFO(host_info_head, host_p, host_h);
	if (pthread_mutex_unlock(host_p->lock_p))
		return QHAL_ERR_LOCK;
	return QHAL_SUCCESS;
}


void *qhalhost_malloc(qhalhost_handle_t host_h, int size)
{
	return malloc((size_t)size);
}

int qhalhost_free(qhalhost_handle_t host_h, void *ptr)
{
	if (ptr != NULL)
		free(ptr);
	return QHAL_SUCCESS;
}


/****************************************************************************
 *
 * Bus writes
 *
 ****************************************************************************/

#ifdef TEST_LOOPBACK
static unsigned char testbuf[5] = { 0 };
#endif

static int _qhalhost_write_byte(qhalhost_handle_t host_h,
		unsigned long addr, unsigned char data)
{
	host_info_t *host_p;
	unsigned char outbuf[2] = { 0 };
#ifdef TEST_LOOPBACK
	unsigned char *inbuf = testbuf;
#else
	unsigned char *inbuf = NULL;
#endif

	FIND_INFO(host_info_head, host_p, host_h);

	outbuf[0] = (unsigned char)
				(RAPTOR_SPI_WRITE | RAPTOR_SPI_LEN_1 | RAPTOR_SPI_ADDR(addr));
	outbuf[1] = data;
	if (sub_spi_transfer(host_p->sub_h,
						(char *)outbuf, (char *)inbuf, 2, SS_CONF(0,SS_LO))) {
		fprintf(stderr, "qhalhost_write_byte: spi_write failed");
		return QHAL_ERR_IO;
	}
#ifdef TEST_LOOPBACK
	printf("Read back [1]=%02X (%u)\n", inbuf[1], inbuf[1]);
#endif
	return QHAL_SUCCESS;
}

static int _qhalhost_write_byte_block(qhalhost_handle_t host_h,
		unsigned long addr, char *buffer_p, int nBytes)
{
	host_info_t *host_p;
	unsigned char outbuf[2] = { 0 };
	int i;

	if (buffer_p == NULL)
		return QHAL_ERR_BADPARAM;

	FIND_INFO(host_info_head, host_p, host_h);

	for (i = 0; i < nBytes; i++, addr++, buffer_p++) {
		outbuf[0] = (unsigned char)
				(RAPTOR_SPI_WRITE | RAPTOR_SPI_LEN_1 | RAPTOR_SPI_ADDR(addr));
		outbuf[1] = (unsigned char)*buffer_p;
		if (sub_spi_transfer(host_p->sub_h,
							(char *)outbuf, NULL, 2, SS_CONF(0,SS_LO))) {
			fprintf(stderr,
				"qhalhost_write_byte_block(#%d): spi_write failed", ++i);
			return QHAL_ERR_IO;
		}
	}
	return nBytes;
}


static int _qhalhost_write_short(qhalhost_handle_t host_h,
		unsigned long addr, unsigned short data)
{
	host_info_t *host_p;
	unsigned char outbuf[3] = { 0 };
#ifdef TEST_LOOPBACK
	unsigned char *inbuf = testbuf;
#else
	unsigned char *inbuf = NULL;
#endif

	FIND_INFO(host_info_head, host_p, host_h);

	outbuf[0] = (unsigned char)
				(RAPTOR_SPI_WRITE | RAPTOR_SPI_LEN_2 | RAPTOR_SPI_ADDR(addr));
#if defined(__BIG_ENDIAN__)
	outbuf[1] = (unsigned char)(data >> 8);
	outbuf[2] = (unsigned char)(data & 0xFF);
#else
	outbuf[2] = (unsigned char)(data >> 8);
	outbuf[1] = (unsigned char)(data & 0xFF);
#endif	
	if (sub_spi_transfer(host_p->sub_h,
						(char *)outbuf, (char *)inbuf, 3, SS_CONF(0,SS_LO))) {
		fprintf(stderr, "qhalhost_write_short: spi_write failed");
		return QHAL_ERR_IO;
	}
#ifdef TEST_LOOPBACK
	{
		unsigned short ret = ((inbuf[1] << 8) & 0xFF00)
								| ((inbuf[2] << 0) & 0x00FF);
		printf("Read back [1]=%02X [2]=%02X %04X (%u)\n",
			inbuf[1], inbuf[2], ret, ret);
	}
#endif
	return QHAL_SUCCESS;
}

static int _qhalhost_write_short_block(qhalhost_handle_t host_h,
		unsigned long addr, short *buffer_p, int nShorts)
{
	host_info_t *host_p;
	unsigned char outbuf[3] = { 0 };
	int i;

	if (buffer_p == NULL)
		return QHAL_ERR_BADPARAM;

	FIND_INFO(host_info_head, host_p, host_h);

	for (i = 0; i < nShorts; i++, addr += 2, buffer_p += 2) {
		outbuf[0] = (unsigned char)
				(RAPTOR_SPI_WRITE | RAPTOR_SPI_LEN_2 | RAPTOR_SPI_ADDR(addr));
#if defined(__BIG_ENDIAN__)
		outbuf[1] = (unsigned char)(*buffer_p >> 8);
		outbuf[2] = (unsigned char)(*buffer_p & 0xFF);
#else
		outbuf[2] = (unsigned char)(*buffer_p >> 8);
		outbuf[1] = (unsigned char)(*buffer_p & 0xFF);
#endif
		if (sub_spi_transfer(host_p->sub_h,
							(char *)outbuf, NULL, 3, SS_CONF(0,SS_LO))) {
			fprintf(stderr,
				"qhalhost_write_short_block(#%d): spi_write failed", ++i);
			return QHAL_ERR_IO;
		}
	}
	return nShorts;
}


static int _qhalhost_write_long(qhalhost_handle_t host_h,
		unsigned long addr, unsigned long data)
{
	host_info_t *host_p;
	unsigned char outbuf[5] = { 0 };
#ifdef TEST_LOOPBACK
	unsigned char *inbuf = testbuf;
#else
	unsigned char *inbuf = NULL;
#endif

	FIND_INFO(host_info_head, host_p, host_h);

	outbuf[0] = (unsigned char)
				(RAPTOR_SPI_WRITE | RAPTOR_SPI_LEN_4 | RAPTOR_SPI_ADDR(addr));
#if defined(__BIG_ENDIAN__)
	outbuf[1] = (unsigned char)(data >> 24);
	outbuf[2] = (unsigned char)(data >> 16);
	outbuf[3] = (unsigned char)(data >> 8);
	outbuf[4] = (unsigned char)(data & 0xFF);
#else
	outbuf[4] = (unsigned char)(data >> 24);
	outbuf[3] = (unsigned char)(data >> 16);
	outbuf[2] = (unsigned char)(data >> 8);
	outbuf[1] = (unsigned char)(data & 0xFF);
#endif

	if (sub_spi_transfer(host_p->sub_h,
						(char *)outbuf, (char *)inbuf, 5, SS_CONF(0,SS_LO))) {
		fprintf(stderr, "qhalhost_write_long: spi_write failed");
		return QHAL_ERR_IO;
	}

#ifdef TEST_LOOPBACK
	{
		unsigned long ret = ((inbuf[1] << 24) & 0xFF000000)
			| ((inbuf[2] << 16) & 0x00FF0000)
			| ((inbuf[3] << 8) & 0x0000FF00)
			| ((inbuf[4] << 0) & 0x000000FF);
		printf("Read back [1]=%02X [2]=%02X [3]=%02X [4]=%02X %08lX (%lu)\n",
			inbuf[1], inbuf[2], inbuf[3], inbuf[3], ret, ret);
	}
#endif
	return QHAL_SUCCESS;
}

static int _qhalhost_write_long_block(qhalhost_handle_t host_h,
		unsigned long addr, long *buffer_p, int nLongs)
{
	host_info_t *host_p;
	unsigned char outbuf[5] = { 0 };
	int i;

	if (buffer_p == NULL)
		return QHAL_ERR_BADPARAM;

	FIND_INFO(host_info_head, host_p, host_h);

	for (i = 0; i < nLongs; i++, addr += 4, buffer_p += 4) {
		outbuf[0] = (unsigned char)
				(RAPTOR_SPI_WRITE | RAPTOR_SPI_LEN_4 | RAPTOR_SPI_ADDR(addr));
#if defined(__BIG_ENDIAN__)
		outbuf[1] = (unsigned char)(*buffer_p >> 24);
		outbuf[2] = (unsigned char)(*buffer_p >> 16);
		outbuf[3] = (unsigned char)(*buffer_p >> 8);
		outbuf[4] = (unsigned char)(*buffer_p & 0xFF);
#else
		outbuf[4] = (unsigned char)(*buffer_p >> 24);
		outbuf[3] = (unsigned char)(*buffer_p >> 16);
		outbuf[2] = (unsigned char)(*buffer_p >> 8);
		outbuf[1] = (unsigned char)(*buffer_p & 0xFF);
#endif
		if (sub_spi_transfer(host_p->sub_h,
							(char *)outbuf, NULL, 5, SS_CONF(0,SS_LO))) {
			fprintf(stderr,
				"qhalhost_write_long_block(#%d): spi_write failed", ++i);
			return QHAL_ERR_IO;
		}
	}
	return nLongs;
}

int qhalhost_write(qhalhost_handle_t host_h,
					unsigned long addr, unsigned long data, int width)
{
	switch (width) {
	case 1:
		return _qhalhost_write_byte(host_h,
									addr, (unsigned char)(data & 0xFF));

	case 2:
		return _qhalhost_write_short(host_h,
									addr, (unsigned short)(data & 0xFFFF));

	case 4:
		return _qhalhost_write_long(host_h, addr, data);

	}
	return QHAL_ERR_BADPARAM;
}

int qhalhost_write_block(qhalhost_handle_t host_h,
			unsigned long addr, unsigned long *data_p, int width, int length)
{
	switch (width) {
	case 1:
		return _qhalhost_write_byte_block(host_h,
											addr, (char *)data_p, length);

	case 2:
		return _qhalhost_write_short_block(host_h,
											addr, (short *)data_p, length);

	case 4:
		return _qhalhost_write_long_block(host_h,
											addr, (long *)data_p, length);

	}
	return QHAL_ERR_BADPARAM;
}


/****************************************************************************
 *
 * Bus reads
 *
 ****************************************************************************/

static int qhalhost_errno = QHAL_SUCCESS;

static unsigned char _qhalhost_read_byte(qhalhost_handle_t host_h,
		unsigned long addr)
{
	host_info_t *host_p;
	unsigned char outbuf[2] = { 0 };
	unsigned char inbuf[2] = { 0 };
	unsigned short ret = 0;

	FIND_INFO(host_info_head, host_p, host_h);

	outbuf[0] = (unsigned char)
				(RAPTOR_SPI_READ | RAPTOR_SPI_LEN_1 | RAPTOR_SPI_ADDR(addr));
#ifdef TEST_LOOPBACK
	outbuf[1] = 0x1A;
#endif
	if (sub_spi_transfer(host_p->sub_h,
						(char *)outbuf, (char *)inbuf, 2, SS_CONF(0,SS_LO))) {
		fprintf(stderr, "qhalhost_read_byte: spi_read failed");
		qhalhost_errno = QHAL_ERR_IO;
	}
	else {
		ret = inbuf[1];
	}
	return ret;
}

static int _qhalhost_read_byte_block(qhalhost_handle_t host_h,
		unsigned long addr, char *buffer_p, int nBytes)
{
	host_info_t *host_p;
	unsigned char outbuf[2] = { 0 };
	unsigned char inbuf[2] = { 0 };
	int i;

	if (buffer_p == NULL)
		return QHAL_ERR_BADPARAM;

	FIND_INFO(host_info_head, host_p, host_h);

	for (i = 0; i < nBytes; i++, addr++, buffer_p++) {
		outbuf[0] = (unsigned char)
				(RAPTOR_SPI_READ | RAPTOR_SPI_LEN_1 | RAPTOR_SPI_ADDR(addr));
#ifdef TEST_LOOPBACK
		outbuf[1] = (unsigned char)i+1;
#endif
		if (sub_spi_transfer(host_p->sub_h,
						(char *)outbuf, (char *)inbuf, 2, SS_CONF(0,SS_LO))) {
			fprintf(stderr,
				"qhalhost_read_byte_block(#%d): spi_read failed", ++i);
			return QHAL_ERR_IO;
		}
		*buffer_p = inbuf[1];
	}
	return nBytes;
}


static unsigned short _qhalhost_read_short(qhalhost_handle_t host_h,
		unsigned long addr)
{
	host_info_t *host_p;
	unsigned char outbuf[3] = { 0 };
	unsigned char inbuf[3] = { 0 };
	unsigned short ret = 0;

	FIND_INFO(host_info_head, host_p, host_h);

	outbuf[0] = (unsigned char)
				(RAPTOR_SPI_READ | RAPTOR_SPI_LEN_2 | RAPTOR_SPI_ADDR(addr));
#ifdef TEST_LOOPBACK
	outbuf[1] = 0x2B;
	outbuf[2] = 0x1A;
#endif
	if (sub_spi_transfer(host_p->sub_h,
						(char *)outbuf, (char *)inbuf, 3, SS_CONF(0,SS_LO))) {
		fprintf(stderr, "qhalhost_read_short: spi_read failed");
		qhalhost_errno = QHAL_ERR_IO;
	}
	else {
#if defined (__BIG_ENDIAN__)
		ret = ((inbuf[1] << 8) & 0xFF00) | ((inbuf[2] << 0) & 0x00FF);
#else
		ret = ((inbuf[2] << 8) & 0xFF00) | ((inbuf[1] << 0) & 0x00FF);
#endif
	}
	return ret;
}

static int _qhalhost_read_short_block(qhalhost_handle_t host_h,
		unsigned long addr, short *buffer_p, int nShorts)
{
	host_info_t *host_p;
	unsigned char outbuf[3] = { 0 };
	unsigned char inbuf[3] = { 0 };
	int i;

	if (buffer_p == NULL)
		return QHAL_ERR_BADPARAM;

	FIND_INFO(host_info_head, host_p, host_h);

	for (i = 0; i < nShorts; i++, addr += 2, buffer_p++) {
		outbuf[0] = (unsigned char)
				(RAPTOR_SPI_READ | RAPTOR_SPI_LEN_2 | RAPTOR_SPI_ADDR(addr));
#ifdef TEST_LOOPBACK
		outbuf[1] = (unsigned char)i+1;
		outbuf[2] = (unsigned char)i+2;
#endif
		if (sub_spi_transfer(host_p->sub_h,
						(char *)outbuf, (char *)inbuf, 3, SS_CONF(0,SS_LO))) {
			fprintf(stderr,
				"qhalhost_read_short_block(#%d): spi_read failed", ++i);
			return QHAL_ERR_IO;
		}
#if defined(__BIG_ENDIAN__)
		*buffer_p = ((inbuf[1] << 8) & 0xFF00) | ((inbuf[2] << 0) & 0x00FF);
#else
		*buffer_p = ((inbuf[2] << 8) & 0xFF00) | ((inbuf[1] << 0) & 0x00FF);
#endif
	}
	return nShorts;
}


static unsigned long _qhalhost_read_long(qhalhost_handle_t host_h,
		unsigned long addr)
{
	host_info_t *host_p;
	unsigned char outbuf[5] = { 0 };
	unsigned char inbuf[5] = { 0 };
	unsigned long ret = 0;

	FIND_INFO(host_info_head, host_p, host_h);

	outbuf[0] = (unsigned char)
				(RAPTOR_SPI_READ | RAPTOR_SPI_LEN_4 | RAPTOR_SPI_ADDR(addr));
#ifdef TEST_LOOPBACK
	outbuf[1] = 0x4D;
	outbuf[2] = 0x3C;
	outbuf[3] = 0x2B;
	outbuf[4] = 0x1A;
#endif
	if (sub_spi_transfer(host_p->sub_h,
						(char *)outbuf, (char *)inbuf, 5, SS_CONF(0,SS_LO))) {
		fprintf(stderr, "qhalhost_read_long: spi_read failed");
		qhalhost_errno = QHAL_ERR_IO;
	}
	else {
#if defined(__BIG_ENDIAN__)
		ret = ((inbuf[1] << 24) & 0xFF000000)
			| ((inbuf[2] << 16) & 0x00FF0000)
			| ((inbuf[3] << 8) & 0x0000FF00)
			| ((inbuf[4] << 0) & 0x000000FF);
#else
		ret = ((inbuf[4] << 24) & 0xFF000000)
			| ((inbuf[3] << 16) & 0x00FF0000)
			| ((inbuf[2] << 8) & 0x0000FF00)
			| ((inbuf[1] << 0) & 0x000000FF);
#endif
	}
	return ret;
}

static int _qhalhost_read_long_block(qhalhost_handle_t host_h,
		unsigned long addr, long *buffer_p, int nLongs)
{
	host_info_t *host_p;
	unsigned char outbuf[5] = { 0 };
	unsigned char inbuf[5] = { 0 };
	int i;

	if (buffer_p == NULL)
		return QHAL_ERR_BADPARAM;

	FIND_INFO(host_info_head, host_p, host_h);

	for (i = 0; i < nLongs; i++, addr += 4, buffer_p++) {
		outbuf[0] = (unsigned char)
				(RAPTOR_SPI_READ | RAPTOR_SPI_LEN_4 | RAPTOR_SPI_ADDR(addr));
#ifdef TEST_LOOPBACK
		outbuf[1] = (unsigned char)i+1;
		outbuf[2] = (unsigned char)i+2;
		outbuf[3] = (unsigned char)i+3;
		outbuf[4] = (unsigned char)i+4;
#endif
		if (sub_spi_transfer(host_p->sub_h,
						(char *)outbuf, (char *)inbuf, 5, SS_CONF(0,SS_LO))) {
			fprintf(stderr,
				"qhalhost_read_long_block(#%d): spi_read failed", ++i);
			return QHAL_ERR_IO;
		}
#if defined(__BIG_ENDIAN__)
		*buffer_p = ((inbuf[1] << 24) & 0xFF000000)
				| ((inbuf[2] << 16) & 0x00FF0000)
				| ((inbuf[3] << 8) & 0x0000FF00)
				| ((inbuf[4] << 0) & 0x000000FF);
#else
		*buffer_p = ((inbuf[4] << 24) & 0xFF000000)
				| ((inbuf[3] << 16) & 0x00FF0000)
				| ((inbuf[2] << 8) & 0x0000FF00)
				| ((inbuf[1] << 0) & 0x000000FF);
#endif
	}
	return nLongs;
}

int qhalhost_read(qhalhost_handle_t host_h,
					unsigned long addr, unsigned long *data_p, int width)
{
	unsigned long data = 0;
	int ret = QHAL_SUCCESS;

	if (data_p == NULL)
		return QHAL_ERR_BADPARAM;

	switch (width) {
	case 1:
		data = (unsigned long)_qhalhost_read_byte(host_h, addr);
		break;

	case 2:
		data = (unsigned long)_qhalhost_read_short(host_h, addr);
		break;

	case 4:
		data = (unsigned long)_qhalhost_read_long(host_h, addr);
		break;

	default:
		return QHAL_ERR_BADPARAM;
	}
	if (qhalhost_errno != QHAL_SUCCESS) {
		ret = qhalhost_errno;
		qhalhost_errno = QHAL_SUCCESS;
	}
	*data_p = data;
	return ret;
}

int qhalhost_read_block(qhalhost_handle_t host_h,
			unsigned long addr, unsigned long *data_p, int width, int length)
{
	switch (width) {
	case 1:
		return _qhalhost_read_byte_block(host_h,
											addr, (char *)data_p, length);
		break;

	case 2:
		return _qhalhost_read_short_block(host_h,
											addr, (short *)data_p, length);
		break;

	case 4:
		return _qhalhost_read_long_block(host_h,
											addr, (long *)data_p, length);
		break;
	}
	return QHAL_ERR_BADPARAM;
}


/****************************************************************************
 *
 * Platform-specific QCC access
 *
 ****************************************************************************/

#define QCC_BID_OK(bid)		((bid & 0xFF) == bid)
#define QCC_ADDR_OK(addr)	((addr & 0xFFFF) == addr)

#define CSR_READ	1
#define CSR_WRITE	0

int qhalhost_write_qcc(qhalhost_handle_t host_h, unsigned long bid,
					unsigned long addr, unsigned long data, int width)
{
	volatile SHIF_TYPE(Status) Status;
	SHIF_TYPE(CSRWrite) CSRWrite;
	SHIF_TYPE(CSRCmd) CSRCmd;
	uint32_t mask;
	int ret = QHAL_SUCCESS;

	if (!QCC_BID_OK(bid) || !QCC_ADDR_OK(addr))
		return QHAL_ERR_BADPARAM;

	switch (width) {
	case 1:
		mask = 0xFF;
		break;

	case 2:
		mask = 0xFFFF;
		break;

	case 4:
		mask = 0xFFFFFFFF;
		break;

	default:
		return QHAL_ERR_BADPARAM;
	}

	// Buffer the value to be shifted in:
	//
	CSRWrite.b.CSRWrite_Data = data & mask;
	ret = qhalhost_write(host_h, SHIF_ADDR(CSRWrite),
								CSRWrite.r, SHIF_BYTES(CSRWrite));
	EXIT_ON_ERROR(ret);

	// Write the command and addressing:
	//
	CSRCmd.b.CSRCmd_RW = CSR_WRITE;
	CSRCmd.b.CSRCmd_Size = width;
	CSRCmd.b.CSRCmd_BID = bid;
	CSRCmd.b.CSRCmd_Addr = addr;
	ret = qhalhost_write(host_h, SHIF_ADDR(CSRCmd),
								CSRCmd.r, SHIF_BYTES(CSRCmd));
	EXIT_ON_ERROR(ret);

	// Poll the DONE bit:
	//
	while (1) {
	    unsigned long s;
		ret = qhalhost_read(host_h, SHIF_ADDR(Status),
							&s, SHIF_BYTES(Status));
		EXIT_ON_ERROR(ret);
		Status.r = (uint8_t)s;
		if (Status.b.Status_CSRDone || Status.b.Status_CSRErr)
			break;
	}

	// Check for error encountered:
	//
	if (Status.b.Status_CSRErr)
		EXIT_WITH_ERROR(QHAL_ERR_CSRERR);

err_exit:
	return ret;
}

int qhalhost_read_qcc(qhalhost_handle_t host_h, unsigned long bid,
					unsigned long addr, unsigned long *data_p, int width)
{
	volatile SHIF_TYPE(Status) Status;
	SHIF_TYPE(CSRRead) CSRRead;
	SHIF_TYPE(CSRCmd) CSRCmd;
	uint32_t mask;
	int ret = QHAL_SUCCESS;

	if (data_p == NULL)
		return QHAL_ERR_BADPARAM;

	if (!QCC_BID_OK(bid) || !QCC_ADDR_OK(addr))
		return QHAL_ERR_BADPARAM;

	switch (width) {
	case 1:
		mask = 0xFF;
		break;

	case 2:
		mask = 0xFFFF;
		break;

	case 4:
		mask = 0xFFFFFFFF;
		break;

	default:
		return QHAL_ERR_BADPARAM;
	}

	// Write the command and addressing:
	//
	CSRRead.r = 0;
	CSRCmd.b.CSRCmd_RW = CSR_READ;
	CSRCmd.b.CSRCmd_Size = width;
	CSRCmd.b.CSRCmd_BID = bid;
	CSRCmd.b.CSRCmd_Addr = addr;
	ret = qhalhost_write(host_h, SHIF_ADDR(CSRCmd),
								CSRCmd.r, SHIF_BYTES(CSRCmd));
	EXIT_ON_ERROR(ret);

	// Poll the DONE bit:
	//
	while (1) {
        unsigned long s;
        ret = qhalhost_read(host_h, SHIF_ADDR(Status),
                            &s, SHIF_BYTES(Status));
        EXIT_ON_ERROR(ret);
        Status.r = (uint8_t)s;
		if (Status.b.Status_CSRDone || Status.b.Status_CSRErr)
			break;
	}

	// Check for error encountered:
	//
	if (Status.b.Status_CSRErr)
        EXIT_WITH_ERROR(QHAL_ERR_CSRERR);

	// No errors, so read in value:
	//
	ret = qhalhost_read(host_h, SHIF_ADDR(CSRRead),
						(unsigned long *)&CSRRead.r, SHIF_BYTES(CSRRead));

err_exit:
	*data_p = CSRRead.r & mask;
	return ret;
}


/****************************************************************************
 *
 * Platform Functions
 *
 ****************************************************************************/

#define MBOX_INT_MASK_CPU(src)	(1 << src)
#define MBOX_INT_POLL_MS		30

//TODO: add timeout support to qhalhost_wait_interrupt?
int qhalhost_wait_interrupt(qhalhost_handle_t host_h, int timeout)
{
	host_info_t *host_p;
	volatile SHIF_TYPE(Status) Status;
	int ret = QHAL_SUCCESS;
	int mask;

	// Only mailbox interrupts implemented:
	FIND_INFO(host_info_head, host_p, host_h);
	switch (host_p->interface) {
	case QHALHOST_INT_MBOX0:
		mask = MBOX_INT_MASK_CPU(0);
		break;

	case QHALHOST_INT_MBOX1:
		mask = MBOX_INT_MASK_CPU(1);
		break;

	default:
		return QHAL_ERR_NA;
	}

	// 'Interrupts' are actually polled Status bits:
	while (1) {
        unsigned long s;
        ret = qhalhost_read(host_h, SHIF_ADDR(Status),
                            &s, SHIF_BYTES(Status));
        EXIT_ON_ERROR(ret);
        Status.r = (uint8_t)s;

		if (Status.b.Status_Mbox & mask)
			return QHAL_SUCCESS;

		ret = qhalhost_wait(host_h, MBOX_INT_POLL_MS);
		EXIT_ON_ERROR(ret);
	}

err_exit:
	return ret;
}

int qhalhost_ack_interrupt(qhalhost_handle_t host_h)
{
	host_info_t *host_p;

	// Only mailbox interrupts implemented:
	FIND_INFO(host_info_head, host_p, host_h);
	switch (host_p->interface) {
	case QHALHOST_INT_MBOX0:
	case QHALHOST_INT_MBOX1:
		// mailbox interrupts are cleared through qcc mbox
		return QHAL_SUCCESS;

	default:
		return QHAL_ERR_NA;
	}
}

#define CHIPCTL_ADDR(reg)	REG_ADDR(CHIPCTL_x_##reg)
#define CHIPCTL_BYTES(reg)	REG_BYTES(CHIPCTL_x_##reg)
#define CHIPCTL_TYPE(reg)	REG_TYPE(CHIPCTL_x_##reg)

#define CHIPCTL_RESET_MS	1

int qhalhost_reset(qhalhost_handle_t host_h)
{
	CHIPCTL_TYPE(ResetControlSet) Reset;
	CHIPCTL_TYPE(ChipID) ChipID;
	int ret;

	if (qhalhost_lock(host_h) != QHAL_SUCCESS)
		return QHAL_ERR_LOCK;

	// Setting chip reset bit is all you need to do:
	Reset.b.ChipSWReset_set = 1;
	ret = qhalhost_write_qcc(host_h, QCC_BID_CHIPCTL,
								CHIPCTL_ADDR(ResetControlSet), Reset.r,
											CHIPCTL_BYTES(ResetControlSet));
	EXIT_ON_ERROR(ret);

	// Give the chip a little time to come back:
	ret = qhalhost_wait(host_h, CHIPCTL_RESET_MS);
	EXIT_ON_ERROR(ret);

	// Verify the reset was OK by reading a known value:
	ret = qhalhost_read_qcc(host_h, QCC_BID_CHIPCTL, CHIPCTL_ADDR(ChipID),
					(unsigned long *)&ChipID.r, CHIPCTL_BYTES(ChipID));
	EXIT_ON_ERROR(ret);

	if (ChipID.b.ProductID != QHAL_CONFIG_PROD_ID)
		EXIT_WITH_ERROR(QHAL_ERR_WRONGPROD);

err_exit:
	qhalhost_unlock(host_h);
	return ret;
}

// For now, these APIs are only valid for words access
int qhalhost_buffer_write_start(qhalhost_handle_t host_h)
{
    host_info_t *host_p;
    FIND_INFO(host_info_head, host_p, host_h);
    
    host_p->buffer_index = 0;

    return QHAL_SUCCESS;
}

int qhalhost_buffer_write(qhalhost_handle_t host_h,
			  unsigned long addr, unsigned long data)
{
    host_info_t *host_p;
    unsigned char *outbuf;

    FIND_INFO(host_info_head, host_p, host_h);
    outbuf = host_p->out_buffer + host_p->buffer_index;

    outbuf[0] = (unsigned char) (RAPTOR_SPI_WRITE | RAPTOR_SPI_LEN_4 | RAPTOR_SPI_ADDR(addr));
#if defined(__BIG_ENDIAN__)
    outbuf[1] = (unsigned char)(data >> 24);
    outbuf[2] = (unsigned char)(data >> 16);
    outbuf[3] = (unsigned char)(data >> 8);
    outbuf[4] = (unsigned char)(data & 0xFF);
#else
    outbuf[4] = (unsigned char)(data >> 24);
    outbuf[3] = (unsigned char)(data >> 16);
    outbuf[2] = (unsigned char)(data >> 8);
    outbuf[1] = (unsigned char)(data & 0xFF);
#endif

    host_p->buffer_index += 5;
    
    if (host_p->buffer_index >= SPI_BUFFER_SZ) {
	qhalhost_buffer_write_end(host_h);
	qhalhost_buffer_write_start(host_h);
    }

    return QHAL_SUCCESS;
}

int qhalhost_buffer_write_end(qhalhost_handle_t host_h)
{
    host_info_t *host_p;
    unsigned char *outbuf;

    FIND_INFO(host_info_head, host_p, host_h);

    if (host_p->buffer_index < 5)
	return QHAL_SUCCESS;

    outbuf = host_p->out_buffer;

    if (sub_spi_transfer(host_p->sub_h,
			 (char *)outbuf, NULL, host_p->buffer_index, SS_CONF(0,SS_LO))) {
	fprintf(stderr, "qhalhost_buffer_end: spi_write failed");
	return QHAL_ERR_IO;
    }

    return QHAL_SUCCESS;
}

int qhalhost_buffer_read_start(qhalhost_handle_t host_h)
{
    host_info_t *host_p;

    FIND_INFO(host_info_head, host_p, host_h);

    DbgTrace("qhalhost_buffer_read_start\n");
    
    host_p->buffer_index = 0;

    return QHAL_SUCCESS;
}

int qhalhost_buffer_read(qhalhost_handle_t host_h, unsigned long addr)
{
    host_info_t *host_p;
    unsigned char *outbuf;

    FIND_INFO(host_info_head, host_p, host_h);

    DbgTrace("qhalhost_buffer_read addr=0x%x\n", addr);

    outbuf = host_p->out_buffer + host_p->buffer_index;

    outbuf[0] = (unsigned char)(RAPTOR_SPI_READ | RAPTOR_SPI_LEN_4 | RAPTOR_SPI_ADDR(addr));
    
    host_p->buffer_index += 5;
    
#if 0
    if (host_p->buffer_index >= SPI_BUFFER_SZ) {
	qhalhost_buffer_read_end(host_h);
	qhalhost_buffer_read_start(host_h);
    }
#endif

    return QHAL_SUCCESS;
}

int qhalhost_buffer_read_end(qhalhost_handle_t host_h, long *returnWords)
{
    host_info_t *host_p;
    unsigned char *outbuf;
    unsigned char *inbuf;
    unsigned char *pInBuf;
    int i;

    FIND_INFO(host_info_head, host_p, host_h);

    DbgTrace("qhalhost_buffer_read_end returnWords 0x%x\n", (int)returnWords);

    if (host_p->buffer_index < 5)
	return 0;

    outbuf = host_p->out_buffer;
    pInBuf = inbuf = host_p->in_buffer;

    if (sub_spi_transfer(host_p->sub_h,
			 (char *)outbuf, (char *)inbuf, host_p->buffer_index, SS_CONF(0,SS_LO))) {
	fprintf(stderr, "qhalhost_buffer_read_end: spi_read failed");
	qhalhost_errno = QHAL_ERR_IO;
    }

    for (i = 0; i < host_p->buffer_index / 5; i++) {
#if defined(__BIG_ENDIAN__)
	returnWords[i] = ((pInBuf[1] << 24) & 0xFF000000)
	    | ((pInBuf[2] << 16) & 0x00FF0000)
	    | ((pInBuf[3] << 8) & 0x0000FF00)
	    | ((pInBuf[4] << 0) & 0x000000FF);
#else
	returnWords[i] = ((pInBuf[4] << 24) & 0xFF000000)
	    | ((pInBuf[3] << 16) & 0x00FF0000)
	    | ((pInBuf[2] << 8) & 0x0000FF00)
	    | ((pInBuf[1] << 0) & 0x000000FF);
#endif

	pInBuf += 5;
    }

    DbgTrace("qhalhost_buffer_read_end returns: %d words\n", host_p->buffer_index / 5);

    return (host_p->buffer_index / 5);
}
