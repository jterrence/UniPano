/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef __QHAL_MBOX_H
#define __QHAL_MBOX_H

#include <qhal_errno.h>

typedef enum {
	QHAL_MBOX0		= 0,
	QHAL_MBOX1		= 1,
	QHAL_SOC_MBOX0	= 2,
	QHAL_SOC_MBOX1	= 3,
} qhalmbox_dev_t;
typedef qhalmbox_dev_t QHALMBOX_DEV;

// These match the mbox status register bitfields now:
typedef enum {
	QHALMBOX_EVENT_NONE		= 0x0,
	QHALMBOX_EVENT_READ		= 0x1,
	QHALMBOX_EVENT_READY	= 0x2,
	QHALMBOX_EVENT_ALL		= 0x3,
} qhalmbox_event_t;
typedef qhalmbox_event_t QHALMBOX_EVENT;

// No one should modify a handle or what is inside
typedef int qhalmbox_handle_t;

#ifdef    __cplusplus
extern "C" {
#endif /* __cplusplus */

/**qhalmbox_open_codec
 * For multiple codecs system
 * Must be called first
 * Init all ressources and subscribe the callback to the list of functions
 * that will be called if an interrupt occurs
 * \param   codec   The codec you want to access.
 * \param   mbox    Which mailbox needs to be opened
 * \param   cb      Callback to call
 * \param   cbparam Parameter that will be used when calling callback
 * \return  a negative value means an error, and a positive value is a handler
 */
qhalmbox_handle_t qhalmbox_open_codec(int codec, qhalmbox_dev_t mbox);

/**qhalmbox_open
 * For single codec applications
 * Must be called first
 * Init all ressources and subscribe the callback to the list of functions
 * that will be called if an interrupt occurs
 * \param   mbox    Which mailbox needs to be opened
 * \param   cb      Callback to call
 * \param   cbparam Parameter that will be used when calling callback
 * \return  a negative value means an error, and a positive value is a handler
 */
qhalmbox_handle_t qhalmbox_open(qhalmbox_dev_t mbox);

/**qhalqcc_close
 * Must be called last
 * Unregister the callback and close the driver
 * \param mbox_h    qhalmbox handler returned by qhalmbox_open
 * \return  QHAL_SUCCESS, otherwise see qhal_errno.h.
 */
int qhalmbox_close(qhalmbox_handle_t mbox_h);

/**qhalmbox_wait_event
 * Wait for an interrupt from the mailbox and check which event it is.
 * \warning do not use qhalmbox_get_event and qhalmbox_wait_event on the same
 *          mailbox and for the same events in multiple threads
 *          otherwise they may collide.
 * \param mbox_h    qhalmbox handler returned by qhalmbox_open
 * \param event     If it contain a specific event, qhal will wait
 *                  for this specific event.
 *                  When this API return, contain the event mask.
 * \return  QHAL_SUCCESS, otherwise see qhal_errno.h. if not QHAL_SUCCESS
 *          event should be ignored, otherwise event report a valid event
 * \note    This API return only if the requested event(s) happened
 */
int qhalmbox_wait_event(qhalmbox_handle_t mbox_h, qhalmbox_event_t *event_p);

/**qhalmbox_get_event
 * Check if an event arrived, fetch it and clear interrupt if it did.
 * \warning do not use qhalmbox_get_event and qhalmbox_wait_event on the same
 *          mailbox and for the same events in multiple threads
 *          otherwise they may collide.
 * \param mbox_h    qhalmbox handler returned by qhalmbox_open
 * \param event     If event contain a specific event (ie QHALMBOX_EVENT_READY
                    or QHALMBOX_EVENT_READ) then this API will only notify
                    user about this specific event.
                    When API return it contains QHALMBOX_EVENT_NONE if
                    none of the requested events happened, otherwise it
                    contains the event that was detected.
 * \return  QHAL_SUCCESS, otherwise see qhal_errno.h.
 */
int qhalmbox_get_event(qhalmbox_handle_t mbox_h, qhalmbox_event_t *event_p);

/**qhalmbox_read
 * Perform a Mailbox QCC register read
 * \param mbox_h    qhalmbox handler returned by qhalmbox_open
 * \param datap     Pointer to where data will be stored
 * \return  QHAL_SUCCESS, otherwise see qhal_errno.h.
 */
int qhalmbox_read(qhalmbox_handle_t mbox_h, unsigned long *data_p);

/**qhalmbox_write
 * Perform a Mailbox QCC register write
 * \param mbox_h    qhalmbox handler returned by qhalmbox_open
 * \param data      Data to write
 * \return  QHAL_SUCCESS, otherwise see qhal_errno.h.
 */
int qhalmbox_write(qhalmbox_handle_t mbox_h, unsigned long data);

/**qhalmbox_read_silent
 * Perform Mailbox QCC register read without triggering READ interrupt in QMM
 * \param mbox_h    qhalmbox handler returned by qhalmbox_open
 * \param datap     Pointer to where data will be stored
 * \return  QHAL_SUCCESS, otherwise see qhal_errno.h.
 */
int qhalmbox_read_silent(qhalmbox_handle_t mbox_h, unsigned long *data_p);

/**qhalmbox_write_silent
 * Perform Mailbox QCC register write without triggering READY interrupt in QMM
 * \param mbox_h    qhalmbox handler returned by qhalmbox_open
 * \param data      Data to write
 * \return  QHAL_SUCCESS, otherwise see qhal_errno.h.
 */
int qhalmbox_write_silent(qhalmbox_handle_t mbox_h, unsigned long data);

#ifdef    __cplusplus
} /* extern "C" */
#endif /* __cplusplus */

#endif  /* __QHAL_MBOX_H */
