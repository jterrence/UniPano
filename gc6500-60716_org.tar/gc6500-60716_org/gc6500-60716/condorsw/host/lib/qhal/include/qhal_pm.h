/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef __QHAL_PM_H
#define __QHAL_PM_H

#include <qhal_errno.h>

/* No one should modify a handle or what is inside */
typedef int qhalpm_handle_t;

#ifdef    __cplusplus
extern "C" {
#endif /* __cplusplus */

/**qhalpm_open_codec
 * For multiple codecs system
 * Call this API first, use the returned handler to call APIs
 * \param codec     The codec you want to access.
 * \param xin_Hz    oscillator (XIN) frequency.
 * \note    This API will not setup hardware,
 * \note    Only one instance of qhalpm can exist at anytime
 * \return  a negative value means an error, and a positive value is a handler 
 */
qhalpm_handle_t qhalpm_open_codec( int codec, unsigned long xin_Hz);

/**qhalpm_open
 * For single codec applications
 * Call this API first, use the returned handler to call APIs
 * \param xin_Hz    oscillator (XIN) frequency.
 * \note    This API will not setup hardware,
 * \note    Only one instance of qhalpm can exist at anytime
 * \return  a negative value means an error, and a positive value is a handler 
 */
qhalpm_handle_t qhalpm_open( unsigned long xin_Hz);

/**qhalpm_setclock
 * Set the system clock. If the clock cannot be set exactly 
 * but a combination exists for a lower frequency, that lower
 * frequency will be used and returned by the API.
 * \param pm_h      Handle returned by qhalpm_open
 * \param sysclk    System clock speed
 * \return  frequency set if positive, otherwise see qhal_errno.h.
 */
int qhalpm_setclock( qhalpm_handle_t pm_h, unsigned long sysclk_Hz);

/**qhalpm_powerdown
 * Chip enters in PowerDown mode where clocks will be shutdown
 * \param pm_h      Handle returned by qhalpm_open
 * \return  QHAL_SUCCESS, otherwise see qhal_errno.h.
 */
int qhalpm_powerdown( qhalpm_handle_t pm_h);

/**qhalpm_wakeup
 * Restart chip after having been shutdown using qhalpm_powerdown
 * \note    If called without having called qhalpm_powerdown first
 *          this API will return an error. Call setclock to start chip
 * \param pm_h      Handle returned by qhalpm_open
 * \return  QHAL_SUCCESS, otherwise see qhal_errno.h.
 */
int qhalpm_wakeup( qhalpm_handle_t pm_h);

/**qhalpm_reset
 * Reset chip
 * \note    Every qhal handler should be closed and re-opened after that except for qhalpm
 * \param   pm_h      Handle returned by qhalpm_open
 * \return  QHAL_SUCCESS, otherwise see qhal_errno.h.
 */
int qhalpm_reset( qhalpm_handle_t pm_h);

/**qhalpm_close
 * Use this to free handle. You can't use this handle after that
 * \param pm_h      Handle returned by qhalpm_open
 * \return  QHAL_SUCCESS, otherwise see qhal_errno.h.
 */
int qhalpm_close( qhalpm_handle_t pm_h);

#ifdef    __cplusplus
} /* extern "C" */
#endif /* __cplusplus */

#endif  /* ifndef __QHAL_EM_H */
