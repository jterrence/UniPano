/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef __QHAL_EM_H
#define __QHAL_EM_H

#include <qhal_errno.h>

typedef enum {
	// Use this mode if you are planning
	// on short transfers. It will prefer PIO
	// transfers because they are quick to setup
	// and perform even if they use more CPU 
	QHALEM_ACCESSTYPE_CMD=0,

	// Use this mode if you are planning on long
	// transfers. It will try to use DMA, this
	// will use hardware flowcontrol, reducing the CPU
	// usage considerably and should also have better
	// datarate. But qhal will have to setup the DMA 
	// engine which may take few cycles.
	QHALEM_ACCESSTYPE_STREAM=1,

	// Use this mode if you want to use the external 
	// memory instead of the bitstream interface to 
	// transfer bitstreams.
	QHALEM_ACCESSTYPE_BITSTREAM=2
} qhalem_accesstype_t;
typedef qhalem_accesstype_t QHALEM_ACCESSTYPE;

typedef enum {
	QHALEM_MODE_FBFRAME,
	QHALEM_MODE_FBFIELD,
	QHALEM_MODE_LINEAR
} qhalem_mode_t;
typedef qhalem_mode_t QHALEM_MODE;

typedef enum {
	QHALEM_PRIORITY_NORMAL=0,
	QHALEM_PRIORITY_LOWER=1,
	QHALEM_PRIORITY_HIGHER=2,
	QHALEM_PRIORITY_HIGHEST=3
} qhalem_priority_t;
typedef qhalem_priority_t QHALEM_PRIORITY;

typedef enum {
	QHALEM_BURSTSIZE_8WORDS=0,
	QHALEM_BURSTSIZE_16WORDS=1,
	QHALEM_BURSTSIZE_32WORDS=2,
	QHALEM_BURSTSIZE_64WORDS=3
} qhalem_burstsize_t;
typedef qhalem_burstsize_t QHALEM_BURSTSIZE;

// No one should modify a handle or what is inside
typedef int qhalem_handle_t;

#ifdef    __cplusplus
extern "C" {
#endif /* __cplusplus */

/**qhalem_open_codec
 * For multiple codecs system
 * Call this API first, use the returned handler to call APIs
 * \param codec     The codec you want to access.
 * \param type      Primary kind of access you will perform.
 * \return  a negative value means an error, and a positive value is a handler 
 */
qhalem_handle_t qhalem_open_codec(int codec, qhalem_accesstype_t type);

/**qhalem_open
 * For single codec applications
 * Call this API first, use the returned handler to call APIs
 * \param type      Primary kind of access you will perform.
 * \param txmode    Linear or Frame buffer
 * \return  a negative value means an error, and a positive value is a handler 
 */
qhalem_handle_t qhalem_open(
		qhalem_accesstype_t type, qhalem_mode_t deprecated);

/**qhalem_close
 * Use this to free handle. You can't use this handle after that
 * \param em_h      Handle returned by qhalem_open
 * \return  QHAL_SUCCESS, otherwise see qhal_errno.h.
 */
int qhalem_close(qhalem_handle_t em_h);

/**qhalem_setconfig
 * Change channel configuration
 * \param em_h      Handle returned by qhalem_open
 * \param threshold Fifo threshold in words that will trigger Req pin from FifoStatus
 * \param burst     Size of burst, this parameter is device dependent !!!!!
 * \param priority  Tx priority in the chip, only on IPMD1
 * \return  QHAL_SUCCESS, otherwise see qhal_errno.h.
 */
int qhalem_setconfig(qhalem_handle_t em_h, char threshold,
		qhalem_burstsize_t burst, qhalem_priority_t priority);

/**qhalem_read_bytes
 * Read bytes from mobilygen's device memory
 * \param em_h      Handle returned by qhalem_open
 * \param blockID   block you want to read from
 * \param addr      address within the block
 * \param buffer    memory space where data will be stored
 * \param nBytes    number of bytes to transfer
 * \return  negative value if error, positive value giving 
 *          number of bytes transfered, length mean tx ok
 */
int qhalem_read_bytes(qhalem_handle_t em_h, unsigned char blockID,
	unsigned long addr, char *buffer, int nBytes);

/**qhalem_read_words
 * Read 32bits words from mobilygen's device memory
 * \param em_h      Handle returned by qhalem_open
 * \param blockID   block you want to read from
 * \param addr      address within the block
 * \param buffer    memory space where data will be stored
 * \param nWords    number of words to transfer
 * \return  negative value if error, positive value giving 
 *          number of words transfered, length mean tx ok
 */
int qhalem_read_words(qhalem_handle_t em_h, unsigned char blockID,
	unsigned long addr, long *buffer, int nWords);

/**qhalem_read_fbframe
 * Read bytes from mobilygen's device memory using FrameBuffer-frame access
 * \param em_h      Handle returned by qhalem_open
 * \param blockID   block you want to read from
 * \param addrX     Column where to start
 * \param addrY     Line where to start
 * \param buffer    memory space where data will be stored
 * \param sizeX     number of bytes per line
 * \param sizeY     number of lines
 * \return  negative value if error, positive value giving 
 *          number of bytes transfered, length mean tx ok
 */
int qhalem_read_fbframe(qhalem_handle_t em_h, unsigned char blockID,
		unsigned long addrX, unsigned long addrY, char *buffer,
		int sizeX, int sizeY);

/**qhalem_write_bytes
 * Write bytes to mobilygen's device memory
 * \param em_h      Handle returned by qhalem_open
 * \param blockID   block you want to read from
 * \param addr      address within the block
 * \param buffer    data to write
 * \param nBytes    number of bytes to transfer
 * \return  negative value if error, positive value giving 
 *          number of bytes transfered, length mean tx ok
 */
int qhalem_write_bytes(qhalem_handle_t em_h, unsigned char blockID,
		unsigned long addr, char *buffer, int nBytes);

/**qhalem_write_words
 * Write 32bits words to mobilygen's device memory
 * \param em_h      Handle returned by qhalem_open
 * \param blockID   block you want to read from
 * \param addr      address within the block
 * \param buffer    data to write
 * \param nWords    number of words to transfer
 * \return  negative value if error, positive value giving 
 *          number of words transfered, length mean tx ok
 */
int qhalem_write_words(qhalem_handle_t em_h, unsigned char blockID,
		unsigned long addr, long *buffer, int nWords);

/**qhalem_write_fbframe
 * Write bytes from mobilygen's device memory using FrameBuffer-frame access
 * \param em_h      Handle returned by qhalem_open
 * \param blockID   block you want to read from
 * \param addrX     Column where to start
 * \param addrY     Line where to start
 * \param buffer    memory space where data will be stored
 * \param sizeX     number of bytes per line
 * \param sizeY     number of lines
 * \return  negative value if error, positive value giving 
 *          number of bytes transfered, length mean tx ok
 */
int qhalem_write_fbframe(qhalem_handle_t em_h, unsigned char blockID,
		unsigned long addrX, unsigned long addrY, char *buffer,
		int sizeX, int sizeY);

/**qhalem_get_partbase
 * Available only on platforms with unified memory.
 * Gets the base addres of a specified block.
 * \param em_h      Handle returned by qhalem_open
 * \param blockID   block you wish to query
 * \param base      returned base address
 * \return  QHAL_SUCCESS, otherwise see qhal_errno.h.
 */
int qhalem_get_partbase(qhalem_handle_t em_h, unsigned char blockID,
		unsigned long* base);

#ifdef    __cplusplus
} /* extern "C" */
#endif /* __cplusplus */

#endif  /* ifndef __QHAL_EM_H */
