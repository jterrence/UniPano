/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef __QHAL_BS_H
#define __QHAL_BS_H

#include <qhal_errno.h>

// No one should modify a handle or what is inside
typedef int qhalbs_handle_t;

typedef struct {
    unsigned long tx_count;
    unsigned long rx_count;
    unsigned long drop_count;
} qhalbs_udp_stats_t;

#ifdef    __cplusplus
extern "C" {
#endif /* __cplusplus */

/**qhalbs_open_codec
 * For multiple codecs system
 * Must be called first
 * Init bitstream interface, and return handle to this instance
 * \param   codec       The codec you want to access.
 * \return  a negative value means an error,
 *          and a positive value is a handler 
 */
qhalbs_handle_t qhalbs_open_codec(int codec);

/**qhalbs_open
 * For single codec applications
 * Must be called first
 * Init bitstream interface, and return handle to this instance
 * \return  a negative value means an error,
 *          and a positive value is a handler 
 */
qhalbs_handle_t qhalbs_open(void);

/**qhalbs_close
 * Must be called last
 * Close the driver
 * \return  QHAL_SUCCESS, otherwise see qhal_errno.h.
 */
int qhalbs_close(qhalbs_handle_t bs_h);

/**qhalbs_setconfig
 * Change bitstream interface parameters
 * \param   threshold   Treshold in words at which DMA req
 *                      will be asserted by QMM
 * \return  QHAL_SUCCESS, otherwise see qhal_errno.h.
 */
int qhalbs_setconfig(qhalbs_handle_t bs_h, int threshold);

/**qhalbs_read
 * Read data from mobilygen's device memory
 * \param bs_h      Handle returned by qhalbs_open
 * \param buffer    data to write
 * \param length    number of bytes to transfer
 * \return  negative value if error, positive value giving 
 *          number of bytes transfered, length mean tx ok
 */
int qhalbs_read(qhalbs_handle_t bs_h, char *buffer, int length);

/**qhalbs_write
 * Write data to mobilygen's device memory
 * \param bs_h      Handle returned by qhalbs_open
 * \param buffer    data to write
 * \param length    number of bytes to transfer
 * \return  negative value if error, positive value giving 
 *          number of bytes transfered, length mean tx ok
 */
int qhalbs_write(qhalbs_handle_t bs_h, char *buffer, int length);
/**qhalbs_udp_bindfd
 * Bind bitstream interface to socket
 * \param   fd   File descriptor for socket
 * \return  QHAL_SUCCESS, otherwise see qhal_errno.h.
 */
#define QHALBS_UDP_UNBINDFD	-1
int qhalbs_udp_bindfd(qhalbs_handle_t bs_h, int socket_fd);

/**qhalbs_udp_get_stats
 * Get packet transmitted, received, and dropped
 * NOTE: unsigned long counts subject to saturation
 * \param bs_h      Handle returned by qhalbs_open
 * \param stats_p   Packet statistics
 * \return  QHAL_SUCCESS, otherwise see qhal_errno.h.
 */
int qhalbs_udp_get_stats(qhalbs_handle_t bs_h, qhalbs_udp_stats_t *stats_p);

#ifdef    __cplusplus
} /* extern "C" */
#endif /* __cplusplus */

#endif  /* __QHAL_BS_H */
