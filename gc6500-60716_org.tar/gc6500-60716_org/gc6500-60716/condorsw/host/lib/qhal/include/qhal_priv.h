/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef __QHAL_PRIV_H
#define __QHAL_PRIV_H

// Macros to handle info struct lists:

#define FIND_INFO( head_p, info_p, int_handle)	\
	do {										\
		for (info_p = head_p;					\
			info_p != NULL;						\
			info_p = info_p->next) {			\
			if (info_p->handle == int_handle)	\
				break;							\
		}										\
		if (info_p == NULL)						\
			return QHAL_ERR_HANDLE;				\
	} while (0)

#define APPEND_INFO( head_p, tail_p, info_p)	\
	if (head_p == NULL) {						\
		head_p = info_p;						\
	} else {									\
		for (tail_p = head_p;					\
			tail_p->next != NULL;				\
			tail_p = tail_p->next)				\
		{ }										\
		tail_p->next = info_p;					\
	}											\

#define REMOVE_INFO( head_p, tail_p, info_p)	\
	if (info_p == head_p) {						\
		head_p = head_p->next;					\
	} else {									\
		for (tail_p = head_p;					\
			tail_p->next != info_p;				\
			tail_p = tail_p->next)				\
			{ }									\
		tail_p->next = info_p->next;			\
	};


// Inline convenience macros for QHAL error checking:
//   (declare ret as int, return ret, and
//    label err_exit before any cleanup)

#define EXIT_ON_ERROR(ret)		\
{								\
	if (ret != QHAL_SUCCESS)	\
		goto err_exit;			\
}

#define EXIT_WITH_ERROR(err)	\
{								\
	ret = err;					\
	goto err_exit;				\
}

// To check both transfer and proxied returns:
#define EXIT_ON_EITHER_ERROR(ret,r2)	\
{								\
	if (ret != QHAL_SUCCESS)	\
		goto err_exit;			\
	if (r2 != QHAL_SUCCESS) {	\
		ret = r2;				\
		goto err_exit;			\
	}							\
}

#endif  // ifndef __QHAL_PRIV_H
