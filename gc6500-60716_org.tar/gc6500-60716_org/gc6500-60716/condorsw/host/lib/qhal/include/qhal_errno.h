/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef __QHAL_ERRNO_H
#define __QHAL_ERRNO_H

// Useful global define:
#ifndef NULL
#define NULL    ((void *)0)
#endif

#define QHAL_NOTIMEOUT  -1  // API with timeout will never timeout
#define QHAL_NOWAIT     0   // API with timeout will return immediately


// Error codes:
#define QHAL_SUCCESS        0       // Any positive value means success
#define QHAL_ERR_MEM        (-1)    // Memory allocation - fatal corruption
#define QHAL_ERR_ENDTX      (-2)    // Wait for busy timed out
#define QHAL_ERR_DEVBUSY    (-3)    // Busy is set before transfer started
#define QHAL_ERR_BADPARAM   (-4)    // Wrong parameter
#define QHAL_ERR_NA         (-5)    // Feature not available on this device
#define QHAL_ERR_HANDLE     (-6)    // Bad handle
#define QHAL_ERR_IO         (-7)    // I/O error from host
#define QHAL_ERR_CSRERR     (-8)    // CSRStat returned an error
#define QHAL_ERR_NOINT      (-9)    // Interrupt has not been handled
#define QHAL_ERR_LOCK       (-10)   // Unable to lock the host
#define QHAL_ERR_WRONGPROD  (-11)   // QHAL can't find the chip
#define QHAL_ERR_INTR       (-12)   // QHAL call interrupted by a signal
#define QHAL_ERR_OVERFLOW   (-13)   // Read or Write overflowed a buffer or FIFO
#define QHAL_ERR_IOCTL      (-14)   // I/O error from driver proxy, not host
#define QHAL_ERR_INIT		(-15)	// Unable to initialize necessary subsystems
#define QHAL_ERR_NODEV		(-16)	// Unable to find the device

#endif  // ifndef __QHAL_ERRNO_H
