/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef __QHAL_CMDEV_H
#define __QHAL_CMDEV_H

#include <qhal_errno.h>

/* No one should modify a handle or what is inside */
typedef int qhalcmdev_handle_t;

#ifdef    __cplusplus
extern "C" {
#endif /* __cplusplus */

/**qhalcmdev_open
 * For single codec applications
 * Call this API first, use the returned handler to call APIs
 * \param cmdBlock Address of command block in codec memory
 * \param evPtr Address of event block pointer in codec memory
 * \return  a negative value means an error, and a positive value is a handler 
 */
qhalcmdev_handle_t qhalcmdev_open( void);

/**qhalcmdev_send_cmd
 * Send a command and block until returned
 * \param cmdev_h      Handle returned by qhalcmdev_open
 * \return  QHAL_SUCCESS, otherwise see qhal_errno.h.
 */
int qhalcmdev_send_command( qhalcmdev_handle_t cmdev_h, char *cmd);


/**qhalcmdev_get_event
 * Block until an event is returned
 * \return  QHAL_SUCCESS, otherwise see qhal_errno.h.
 */
int qhalcmdev_get_event( qhalcmdev_handle_t cmdev_h, char *event);

/**qhalcmdev_close
 * Use this to free handle. You can't use this handle after that
 * \param cmdev_h      Handle returned by qhalcmdev_open
 * \return  QHAL_SUCCESS, otherwise see qhal_errno.h.
 */
int qhalcmdev_close( qhalcmdev_handle_t cmdev_h);

#ifdef    __cplusplus
} /* extern "C" */
#endif /* __cplusplus */

#endif  /* ifndef __QHAL_CMDEV_H */
