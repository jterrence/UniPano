/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef __QHAL_QCC_H
#define __QHAL_QCC_H

#include <qhal_errno.h>

/* No one should modify a handle or what is inside */
typedef int qhalqcc_handle_t;

#ifdef    __cplusplus
extern "C" {
#endif /* __cplusplus */

/**qhalqcc_open_codec
 * For multiple codecs system
 * Must be called first
 * Open devices used in QCC transactions and perfom necessary init
 * \param   codec       The codec you want to access.
 * \return  a negative value means an error, and a positive value is a handler
 */
qhalqcc_handle_t qhalqcc_open_codec(int codec);

/**qhalqcc_open
 * For single codec applications
 * Must be called first
 * Open devices used in QCC transactions and perfom necessary init
 * \return  a negative value means an error, and a positive value is a handler
 */
qhalqcc_handle_t qhalqcc_open(void);

/**qhalqcc_read
 * Perform a QCC register read
 * \param qcc_h     qhalqcc handler
 * \param bid       Block ID to access
 * \param addr      Address of the QCC register
 * \param datap     Pointer to where data will be stored
 * \param len       Size of the register (4, 2 or 1)
 * \return  QHAL_SUCCESS, otherwise see qhal_errno.h.
 */
int qhalqcc_read(qhalqcc_handle_t qcc_h, int bid,
					int addr, unsigned long *dat_ap, int len);

/**qhalqcc_write
 * Perform a QCC register write
 * \param qcc_h     qhalqcc handler
 * \param bid       Block ID to access
 * \param addr      Address of the QCC register
 * \param data      Data to write
 * \param len       Size of the register (4, 2 or 1)
 * \return  QHAL_SUCCESS, otherwise see qhal_errno.h.
 */
int qhalqcc_write(qhalqcc_handle_t qcc_h, int bid,
						int addr, unsigned long data, int len);

/**qhalqcc_close
 * Must be called last
 * Close the driver and perfom necessary shutdown
 * \param qcc_h     qhalqcc handler
 * \return  QHAL_SUCCESS, otherwise see qhal_errno.h.
 */
int qhalqcc_close(qhalqcc_handle_t qcc_h);

#ifdef    __cplusplus
} /* extern "C" */
#endif /* __cplusplus */

#endif  /* __QHAL_QCC_H */
