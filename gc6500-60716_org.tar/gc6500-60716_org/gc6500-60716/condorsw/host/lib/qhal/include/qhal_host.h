/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef __QHAL_HOST_H
#define __QHAL_HOST_H

#include <qhal_errno.h>

typedef enum {
    QHALHOST_BUS_CSR,
    QHALHOST_BUS_EM1,
    QHALHOST_BUS_EM1_STREAM,
    QHALHOST_BUS_EM2,
    QHALHOST_BUS_EM2_STREAM,
    QHALHOST_BUS_BITSTREAM,
    QHALHOST_INT_MBOX0,
    QHALHOST_INT_MBOX1,
    QHALHOST_INT_VIDEO,
	NINTERFACES
} qhalhost_interface_t;
typedef qhalhost_interface_t QHALHOST_INTERFACE;

typedef enum {
/* This interface can't be used to transfer data.
   thus read/write operation can't be used */
	QHALHOST_BUSMODE_DISABLED,

/* This interface can only transfer single data
   thus block read/write operation can't be used */
	QHALHOST_BUSMODE_WORD,

/* poll ready signal and send threshold data, 
   chip layer (qhal except qhalhost) is 
   responsible for threshold control
   and polling */
	QHALHOST_BUSMODE_PIO_POLL,

/* use wait signal to pause transfer, host is responsible
   for setting up properly DMA engine and make sure
   number of data sent doesn't exceed host DMA engine capabilities.
   chip layer is responsible for making sure chip's DMA engine
   capabilities are not exceeded */
	QHALHOST_BUSMODE_DMA_WAIT,

/* use req signal to continue transfer, host is responsible
   for setting up properly DMA engine and make sure
   number of data sent doesn't exceed host DMA engine capabilities.
   chip layer is responsible for making sure chip's DMA engine
   capabilities are not exceeded. */
	QHALHOST_BUSMODE_DMA_REQ
} qhalhost_busmode_t;
typedef qhalhost_busmode_t QHALHOST_BUSMODE;

#define QHALHOST_GETSWAPMASK                 (0)
#define QHALHOST_DISABLESWAPMODE(swapmask)   (swapmask)
#define QHALHOST_BYTESWAP                    (1<<0)  /* Swap 8bits words */
#define QHALHOST_SHORTSWAP                   (1<<1)  /* Swap 16bits words */
#define QHALHOST_LONGSWAP                    (1<<2)  /* Swap 32bits words */

typedef int qhalhost_handle_t;

typedef int (*qhalhost_callback_t)(void *);

#ifdef    __cplusplus
extern "C" {
#endif /* __cplusplus */

/**qhalhost_open
 * Perform init for hostbus
 * \param   codec       The codec you want to access.
 * \param   interface   Which host interface the user wants to open
 * \return  a negative value means an error, and a positive value is a handler
 */
qhalhost_handle_t qhalhost_open(int codec, qhalhost_interface_t interface);

/**qhalhost_close
 * Last call that will release hostbus ressource
 * \param   host_h  Handle returned by qhalqhalhost_open
 * \return  QHAL_SUCCESS on success, see qhal_errno.h.
 */
int qhalhost_close(qhalhost_handle_t host_h);


/**qhalhost_getbusmode
 * Return the mode of operations for the selected interface
 * \param   host_h  Handle returned by qhalqhalhost_open
 * \return  mode being used.
 */
qhalhost_busmode_t qhalhost_getbusmode(qhalhost_handle_t host_h);

/**qhalhost_swapmode
 * Get and Select byteswap modes for qhalhost_*_block APIs
 * \param   host_h      Handle returned by qhalqhalhost_open
 * \param   swapmode    If GETSWAPMASK, api will return current swapmode
 *                      otherwise will disable byteswap according to mask
 * \return  a negative value means an error, QHAL_SUCCESS otherwise
 */
int qhalhost_swapmode(qhalhost_handle_t host_h, int swapmode);


/****************************************************************************
 *
 * Platform IO interface
 *
 ****************************************************************************/

/**qhalhost_write
 * Write to host bus
 * \param   host_h  Handle returned by qhalhost_open
 * \param   addr    Address on host bus to write (relative to HOST_INTERFACE)
 * \param   data    Data to write
 * \param   width   Width of data write (1, 2, or 4 bytes)
 * \return  QHAL_SUCCESS on success, see qhal_errno.h.
 */
int qhalhost_write(qhalhost_handle_t host_h,
					unsigned long addr, unsigned long data, int width);

/**qhalhost_write_block
 * Write to host bus
 * \param   host_h  Handle returned by qhalhost_open
 * \param   addr    Address on host bus to write (relative to HOST_INTERFACE)
 * \param   data    Data to write
 * \param   width   Width of data write (1, 2, or 4 bytes)
 * \param   length  Number of elements to transfer
 * \return  QHAL_SUCCESS on success, see qhal_errno.h.
 */
int qhalhost_write_block(qhalhost_handle_t host_h, unsigned long addr,
								unsigned long *data_p, int width, int length);

/**qhalhost_write_qcc
 * NON-LOCKED Write to QCC register
 * (qhalqcc_write shouldn't be called from mbox, em, etc, because
 *  it nests locking calls, which isn't supported on all platforms).
 * \param   host_h  Handle returned by qhalhost_open
 * \param   addr    Address of register to write to
 * \param   data    Data to write
 * \param   width   Width of data write (1, 2, or 4 bytes)
 * \return  QHAL_SUCCESS on success, see qhal_errno.h.
 */
int qhalhost_write_qcc(qhalhost_handle_t host_h, unsigned long bid,
					unsigned long addr, unsigned long data, int width);

/**qhalhost_read
 * Read data from host bus
 * \param   host_h  Handle returned by qhalhost_open
 * \param   addr    Address on host bus to read (relative to HOST_INTERFACE)
 * \param   data_p  Pointer to storage for data read
 * \param   width   Width of data read (1, 2, or 4 bytes)
 * \return  QHAL_SUCCESS on success, see qhal_errno.h.
 */
int qhalhost_read(qhalhost_handle_t host_h,
					unsigned long addr, unsigned long *data_p, int width);

/**qhalhost_read_block
 * Read data from host bus
 * \param   host_h  Handle returned by qhalhost_open
 * \param   addr    Address on host bus to read (relative to HOST_INTERFACE)
 * \param   data_p  Pointer to storage for data read
 * \param   width   Width of data read (1, 2, or 4 bytes)
 * \param   length  Number of elements to transfer
 * \return  QHAL_SUCCESS on success, see qhal_errno.h.
 */
int qhalhost_read_block(qhalhost_handle_t host_h, unsigned long addr,
								unsigned long *data_p, int width, int length);

/**qhalhost_read_qcc
 * NON-LOCKED Read from QCC register
 * (qhalqcc_read shouldn't be called from mbox, em, etc, because
 *  it nests locking calls, which isn't supported on all platforms).
 * \param   host_h  Handle returned by qhalhost_open
 * \param   addr    Address of register to read
 * \param   data_p  Pointer to storage for data read
 * \param   width   Width of data read (1, 2, or 4 bytes)
 * \return  QHAL_SUCCESS on success, see qhal_errno.h.
 */
int qhalhost_read_qcc(qhalhost_handle_t host_h, unsigned long bid,
					unsigned long addr, unsigned long *data_p, int width);


/****************************************************************************
 *
 * OS Utils
 *
 ****************************************************************************/

/**qhalhost_wait
 * Wait for a specific time in ms
 * NOTE: This is an approximative wait and should be implemented as a context switch
 * on most host environment, thus allowing the host to do something else for a while
 * All that should be expected by API calling this method is that AT LEAST time_ms ms
 * will elapse.
 * NOTE2: If time_ms is 0 this API should return immediately if qhal is running in a
 * pre-emptible environemnt, or force a context switch otherwise
 * \param   host_h  Handle returned by qhalhost_open
 * \param   time_ms Time to wait in milliseconds
 * \return  QHAL_SUCCESS on success, see qhal_errno.h.
 */
int qhalhost_wait(qhalhost_handle_t host_h, unsigned long time_ms);

/**qhalhost_wait_ns
 * Wait for a specific time in ns
 * NOTE: This is an exact wait, it should be implemented to be as exact as possible
 * No context switch should be allowed in this wait
 * \param   host_h  Handle returned by qhalhost_open
 * \param   time_ns Time to wait in nanoseconds
 * \return  QHAL_SUCCESS on success, see qhal_errno.h.
 */
int qhalhost_wait_ns(qhalhost_handle_t host_h, unsigned long time_ns);

/**qhalhost_malloc
 * Allocate host memory for qhal use
 * \param   host_h  Handle returned by qhalhost_open
 * \param   size    Number of bytes to allocate
 * \return  NULL if error, pointer to the newly allocated memory otherwise
 */
void *qhalhost_malloc(qhalhost_handle_t host_h, int size);

/**qhalhost_free
 * Free meory allocated with qhalhost_malloc
 * \param   host_h  Handle returned by qhalhost_open
 * \param   ptr     Pointer returned by qhalhost_malloc
 * \return  QHAL_SUCCESS on success, see qhal_errno.h.
 */
int qhalhost_free(qhalhost_handle_t host_h, void *ptr);

/**qhalhost_lock
 * After this method succeeded, caller will be the only one to have access
 * to the interface used, call qhalhost_unlock to allow other user to access
 * host interface
 * \param   host_h  Handle returned by qhalhost_open
 * \return  QHAL_SUCCESS on success, see qhal_errno.h.
 */
int qhalhost_lock(qhalhost_handle_t host_h);

/**qhalhost_unlock
 * Will allow other user to access ressources again. YOU MUST NOT
 * FORGET TO CALL THIS OTHERWISE BUS WILL NOT BE DEAD
 * \param   host_h  Handle returned by qhalhost_open
 * \return  QHAL_SUCCESS on success, see qhal_errno.h.
 */
int qhalhost_unlock(qhalhost_handle_t host_h);


/****************************************************************************
 *
 * Platform Functions
 *
 ****************************************************************************/

/**qhalhost_wait_interrupt
 * Wait for an interrupt to come
 * \param   host_h  Handle returned by qhalhost_open
 * \param   timeout Wait will fail if interrupt has not come
 *                  after timeout microseconds
 * \return  QHAL_SUCCESS on success, see qhal_errno.h.
 */
int qhalhost_wait_interrupt(qhalhost_handle_t host_h, int timeout);

/**qhalhost_ack_interrupt
 * Acknowledge to the host that interrupt has been serviced.
 * This API is used by QHAL to notify host that this interface interrupt
 * can be enabled again (interrupt status has been cleared on QMM side)
 * \param   host_h  Handle returned by qhalhost_open
 * \return  QHAL_SUCCESS on success, see qhal_errno.h.
 */
int qhalhost_ack_interrupt(qhalhost_handle_t host_h);

/**qhalhost_reset
 * Assert reset and configure the board
 * \param   host_h  Handle returned by qhalhost_open
 * \return  QHAL_SUCCESS on success, see qhal_errno.h.
 */
int qhalhost_reset(qhalhost_handle_t host_h);

int qhalhost_buffer_write_start(qhalhost_handle_t host_h);
int qhalhost_buffer_write(qhalhost_handle_t host_h,
			  unsigned long addr, unsigned long data);
int qhalhost_buffer_write_end(qhalhost_handle_t host_h);

int qhalhost_buffer_read_start(qhalhost_handle_t host_h);
int qhalhost_buffer_read(qhalhost_handle_t host_h, unsigned long addr);
int qhalhost_buffer_read_end(qhalhost_handle_t host_h, long *retWords);


#ifdef    __cplusplus
} /* extern "C" */
#endif /* __cplusplus */

#endif  /* ifndef __QHAL_HOST_H */
