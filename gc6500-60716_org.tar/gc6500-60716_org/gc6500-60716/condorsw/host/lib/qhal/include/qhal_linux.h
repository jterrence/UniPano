/**qhal_linux.h
 * This file describe the QHAL linux kernel module
 * interface.
 */

/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef __QHAL_LINUX_H
#define __QHAL_LINUX_H

#include <qhal_mbox.h>
#include <qhal_em.h>
#include <qhal_host.h>

/* Device major is either given or allocated 
 * at module loading. To get the major please
 * look at /proc/devices
 * To get the device address please use
 * /sys/devices 
 * Below are the names the devices should have
 */
#define QHAL_LINUX_PREFIX_DEV           "qhal"
#define QHAL_LINUX_HOST_DEV             "host_if"
#define QHAL_LINUX_BS_DEV               "bs"
#define QHAL_LINUX_EM_CMD_DEV           "em_cmd"
#define QHAL_LINUX_EM_STREAM_DEV        "em_stream"
#define QHAL_LINUX_EM_BITSTREAM_DEV     "em_bitstream"
#define QHAL_LINUX_MBOX_0_DEV           "mbox0"
#define QHAL_LINUX_MBOX_1_DEV           "mbox1"
#define QHAL_LINUX_PM_DEV               "pm"
#define QHAL_LINUX_QCC_DEV              "qcc"

/* IOCTLs */
/* NOTE: if ioctl returns 0, check ret parameter. otherwise it may be invalid */
#define QHAL_IOC_MAGIC  'Q'

/*
 * qhal_bs 
 */
/* qhalbs_open: use open */

/* qhalbs_setconfig */
typedef struct {
    int threshold;
    int ret;
} qhalbs_linux_setconfig_t;
#define QHALBS_IOC_SETCONFIG            _IOWR(QHAL_IOC_MAGIC, 33, qhalbs_linux_setconfig_t)

/* qhalbs_udp_bindfd */
typedef struct {
    int socket_fd;
    int ret;
} qhalbs_linux_udp_bindfd_t;
#define QHALBS_IOC_UDP_BINDFD           _IOWR(QHAL_IOC_MAGIC, 34, qhalbs_linux_udp_bindfd_t)

/* qhalbs_udp_get_stats */
typedef struct {
    unsigned long tx_count;
    unsigned long rx_count;
    unsigned long drop_count;
    int ret;
} qhalbs_linux_udp_get_stats_t;
#define QHALBS_IOC_UDP_GET_STATS        _IOR(QHAL_IOC_MAGIC, 35, qhalbs_linux_udp_get_stats_t)

/* qhalbs_write: use write */
/* qhalbs_close: use close */

/*
 * qhal_em 
 */
/* qhalem_open: use open */
/* qhalem_setconfig */
typedef struct {
    char threshold;
    QHALEM_BURSTSIZE burst;
    QHALEM_PRIORITY priority;
    int ret;
} qhalem_linux_setconfig_t;
#define QHALEM_IOC_SETCONFIG            _IOWR(QHAL_IOC_MAGIC, 65, qhalem_linux_setconfig_t)
/* qhalem_read_bytes 
 * NOTE: use llseek and read for partition 64
 */
typedef struct {
    unsigned char blockID;
    unsigned long addr;
    char *buffer;
    int nBytes;
    int ret;
} qhalem_linux_read_bytes_t;
#define QHALEM_IOC_READ_BYTES           _IOWR(QHAL_IOC_MAGIC, 66, qhalem_linux_read_bytes_t)
/* qhalem_read_words */
typedef struct {
    unsigned char blockID;
    unsigned long addr;
    long *buffer;
    int nWords;
    int ret;
} qhalem_linux_read_words_t;
#define QHALEM_IOC_READ_WORDS           _IOWR(QHAL_IOC_MAGIC, 67, qhalem_linux_read_words_t)
/* qhalem_read_fbframe */
typedef struct {
    unsigned char blockID;
    unsigned long addrX;
    unsigned long addrY;
    char *buffer;
    int sizeX;
    int sizeY;
    int ret;
} qhalem_linux_read_fbframe_t;
#define QHALEM_IOC_READ_FBFRAME         _IOWR(QHAL_IOC_MAGIC, 68, qhalem_linux_read_fbframe_t)
/* qhalem_write_bytes 
 * NOTE: use llseek and write for partition 64
 */
typedef struct {
    unsigned char blockID;
    unsigned long addr;
    char *buffer;
    int nBytes;
    int ret;
} qhalem_linux_write_bytes_t;
#define QHALEM_IOC_WRITE_BYTES           _IOWR(QHAL_IOC_MAGIC, 69, qhalem_linux_write_bytes_t)
/* qhalem_write_words */
typedef struct {
    unsigned char blockID;
    unsigned long addr;
    long *buffer;
    int nWords;
    int ret;
} qhalem_linux_write_words_t;
#define QHALEM_IOC_WRITE_WORDS          _IOWR(QHAL_IOC_MAGIC, 70, qhalem_linux_write_words_t)
/* qhalem_write_fbframe */
typedef struct {
    unsigned char blockID;
    unsigned long addrX;
    unsigned long addrY;
    char *buffer;
    int sizeX;
    int sizeY;
    int ret;
} qhalem_linux_write_fbframe_t;
#define QHALEM_IOC_WRITE_FBFRAME        _IOWR(QHAL_IOC_MAGIC, 71, qhalem_linux_write_fbframe_t)
/* qhalem_linux_get_partbase_t */
typedef struct {
    unsigned char blockID;
    unsigned long* base;
    int ret;
} qhalem_linux_get_partbase_t;
#define QHALEM_IOC_GET_PARTBASE         _IOWR(QHAL_IOC_MAGIC, 72, qhalem_linux_get_partbase_t)
/* qhalem_close: use close */

/*
 * qhal_mbox
 */
/* qhalmbox_open: use open */
/* qhalmbox_wait_event */
typedef struct {
    QHALMBOX_EVENT *event;
    int ret;
} qhalmbox_linux_wait_event_t;
#define QHALMBOX_IOC_WAIT_EVENT         _IOWR(QHAL_IOC_MAGIC, 97, qhalmbox_linux_wait_event_t)
/* qhalmbox_get_event */
typedef struct {
    QHALMBOX_EVENT *event;
    int ret;
} qhalmbox_linux_get_event_t;
#define QHALMBOX_IOC_GET_EVENT          _IOWR(QHAL_IOC_MAGIC, 98, qhalmbox_linux_get_event_t)
/* qhalmbox_read */
typedef struct {
    unsigned long *datap;
    int ret;
} qhalmbox_linux_read_t;
#define QHALMBOX_IOC_READ               _IOWR(QHAL_IOC_MAGIC, 99, qhalmbox_linux_read_t)
/* qhalmbox_write */
typedef struct {
    unsigned long data;
    int ret;
} qhalmbox_linux_write_t;
#define QHALMBOX_IOC_WRITE              _IOWR(QHAL_IOC_MAGIC, 100, qhalmbox_linux_write_t)
/* qhalmbox_read_silent */
typedef struct {
    unsigned long *datap;
    int ret;
} qhalmbox_linux_read_silent_t;
#define QHALMBOX_IOC_READ_SILENT        _IOWR(QHAL_IOC_MAGIC, 101, qhalmbox_linux_read_silent_t)
/* qhalmbox_write_silent */
typedef struct {
    unsigned long data;
    int ret;
} qhalmbox_linux_write_silent_t;
#define QHALMBOX_IOC_WRITE_SILENT       _IOWR(QHAL_IOC_MAGIC, 102, qhalmbox_linux_write_silent_t)
/* qhalmbox_close: use close */

/*
 * qhal_pm
 */
/* qhalpm_open */
typedef struct {
    unsigned long xin_Hz;
    int ret;
} qhalmbox_linux_open_t;
#define QHALPM_IOC_OPEN                 _IOWR(QHAL_IOC_MAGIC, 128, qhalmbox_linux_open_t)
/* qhalpm_setclock */
typedef struct {
    unsigned long sysclk_Hz;
    int ret;
} qhalmbox_linux_setclock_t;
#define QHALPM_IOC_SETCLOCK             _IOWR(QHAL_IOC_MAGIC, 129, qhalmbox_linux_setclock_t)
/* qhalpm_powerdown */
typedef struct {
    int ret;
} qhalmbox_linux_powerdown_t;
#define QHALPM_IOC_POWERDOWN            _IOR(QHAL_IOC_MAGIC, 130, qhalmbox_linux_powerdown_t)
/* qhalpm_wakeup */
typedef struct {
    int ret;
} qhalmbox_linux_wakeup_t;
#define QHALPM_IOC_WAKEUP               _IOR(QHAL_IOC_MAGIC, 131, qhalmbox_linux_wakeup_t)
/* qhalpm_reset */
typedef struct {
    int ret;
} qhalmbox_linux_reset_t;
#define QHALPM_IOC_RESET                _IOR(QHAL_IOC_MAGIC, 132, qhalmbox_linux_reset_t)
/* qhalpm_close: use close */

/*
 * qhal_qcc
 */
/* qhalqcc_open: use open */
/* qhalqcc_read */
typedef struct {
    int bid;
    int addr;
    unsigned long *datap;
    int len;
    int ret;
} qhalqcc_linux_read_t;
#define QHALQCC_IOC_READ                _IOWR(QHAL_IOC_MAGIC, 161, qhalqcc_linux_read_t)
/* qhalqcc_write */
typedef struct {
    int bid;
    int addr;
    unsigned long data;
    int len;
    int ret;
} qhalqcc_linux_write_t;
#define QHALQCC_IOC_WRITE               _IOWR(QHAL_IOC_MAGIC, 162, qhalqcc_linux_write_t)
/* qhalqcc_close: use close */

/* This one is used for qhalen read and write system call: help for debugging */
#define QHALEM_QMM_PARTID           QMM_PARTITION_ID

/*
 * qhal_host
 */
/* qhalhost_open: use open */
/* qhalqcc_close: use close */

typedef struct {
	unsigned long bid;
    unsigned long addr;
    unsigned long value;
    void *data_p;
    int width;
    int length;
    int ret;
} qhalhost_linux_ioaccess_t;
#define QHALHOST_IOC_READ			_IOWR(QHAL_IOC_MAGIC, 194, qhalhost_linux_ioaccess_t)
#define QHALHOST_IOC_READ_BLOCK		_IOWR(QHAL_IOC_MAGIC, 195, qhalhost_linux_ioaccess_t)
#define QHALHOST_IOC_READ_QCC		_IOWR(QHAL_IOC_MAGIC, 196, qhalhost_linux_ioaccess_t)
#define QHALHOST_IOC_WRITE			_IOWR(QHAL_IOC_MAGIC, 197, qhalhost_linux_ioaccess_t)
#define QHALHOST_IOC_WRITE_BLOCK	_IOWR(QHAL_IOC_MAGIC, 198, qhalhost_linux_ioaccess_t)
#define QHALHOST_IOC_WRITE_QCC		_IOWR(QHAL_IOC_MAGIC, 199, qhalhost_linux_ioaccess_t)

#endif  /* #ifndef __QHAL_LINUX_H */
