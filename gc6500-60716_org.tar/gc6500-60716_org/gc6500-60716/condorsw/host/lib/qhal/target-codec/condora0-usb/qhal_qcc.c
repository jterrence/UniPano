/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include "qhal_priv.h"
#include "qhal_host.h"
#include "qhal_platform.h"
#include "qhal_qcc.h"

typedef struct qcc_info_struct {
	qhalhost_handle_t host_h;
	qhalqcc_handle_t handle;
	struct qcc_info_struct *next;
} qcc_info_t;

static qcc_info_t *qcc_info_head = NULL;

qhalqcc_handle_t qhalqcc_open_codec(int codec)
{
	static qhalqcc_handle_t handle_index = 1;
	qcc_info_t *qcc_info_tail, *qcc_p;
	qhalhost_handle_t host_h;

	host_h = qhalhost_open(codec, QHALHOST_BUS_CSR);
	if (host_h < 0)
		return host_h;

	qcc_p = (qcc_info_t *)qhalhost_malloc(host_h, sizeof(qcc_info_t));
	if (qcc_p == NULL) {
		qhalhost_close(host_h);
		return QHAL_ERR_MEM;
	}

	qcc_p->host_h = host_h;
	qcc_p->handle = handle_index++;
	qcc_p->next = NULL;
	APPEND_INFO(qcc_info_head, qcc_info_tail, qcc_p);
	return qcc_p->handle;
}

qhalqcc_handle_t qhalqcc_open(void)
{
	return qhalqcc_open_codec(0);
}

int qhalqcc_close(qhalqcc_handle_t qcc_h)
{
	qcc_info_t *qcc_info_tail, *qcc_p;
	qhalhost_handle_t host_h;

	FIND_INFO(qcc_info_head, qcc_p, qcc_h);
	host_h = qcc_p->host_h;

	REMOVE_INFO(qcc_info_head, qcc_info_tail, qcc_p);
	qhalhost_free(host_h, (void *)qcc_p);
	return qhalhost_close(host_h);
}

/****************************************************************************/

int qhalqcc_read(qhalqcc_handle_t qcc_h,
		int bid, int addr, unsigned long *data_p, int len)
{
	qcc_info_t *qcc_p;
	int ret;

	FIND_INFO(qcc_info_head, qcc_p, qcc_h);
	if (qhalhost_lock(qcc_p->host_h) != QHAL_SUCCESS)
		return QHAL_ERR_LOCK;

	ret = qhalhost_read_qcc(qcc_p->host_h, bid, addr, data_p, len);
	qhalhost_unlock(qcc_p->host_h);
	return ret;
}

int qhalqcc_write(qhalqcc_handle_t qcc_h,
		int bid, int addr, unsigned long data, int len)
{
	qcc_info_t *qcc_p;
	int ret;

	FIND_INFO(qcc_info_head, qcc_p, qcc_h);
	if (qhalhost_lock(qcc_p->host_h) != QHAL_SUCCESS)
		return QHAL_ERR_LOCK;

	ret = qhalhost_write_qcc(qcc_p->host_h, bid, addr, data, len);
	qhalhost_unlock(qcc_p->host_h);
	return ret;
}

