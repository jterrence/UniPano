/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include <stdio.h>
#include "qhal_priv.h"
#include "qhal_host.h"
#include "qhal_platform.h"
#include "qhal_em.h"

#define DbgTrace(str...) //printf(str)

/*
 *  See the MEMS spec for VPAT register details,
 *  particularly how to determine the partition base address
 */
#define VPAT_SXSW 9
#define VPAT_SYSW 6

#define QCCFIELD_VPATSEGXSIZE_SHIFT 0
#define QCCFIELD_VPATSEGXSIZE_MASK  0x3
#define QCCFIELD_VPATSEGYSIZE_SHIFT 2
#define QCCFIELD_VPATSEGYSIZE_MASK  0x3

#define EM_BID_OK(bid)				((bid & 0xFF) == bid)
#define EM_ADDR_OK(addr)			((addr & 0xFFFFFFFC) == addr)
#define EM_COUNT_OK(count)			(1)
#define EM_COUNT_ALIGNED(count)		((count & 0x3) == count)

// right now the libusb/uvc drivers set the max transfer size to be just 2048 bytes
#define QHAL_USB_MAX_XFER_SZ 2048

typedef struct em_info_struct {
    qhalem_priority_t priority;
    qhalhost_handle_t host_h;
    qhalem_handle_t handle;
    int curr_part_id;
    int curr_part_base;
    struct em_info_struct *next;
} em_info_t;

static em_info_t *em_info_head = NULL;

static unsigned long GetPartitionBase(em_info_t *em_p, int partition)
{
    unsigned long reg;
    int ret;
    int qba = PARTITION_BASE_ADDR(partition);
    ret = qhalhost_read_qcc(em_p->host_h, QCC_BID_PMU0, qba, &reg, 
			    REG_BYTES(MEMS_MEM0_PMU_x_PartBaseSeg0));
    if (ret != QHAL_SUCCESS)
	return ret;

    return reg;
}

qhalem_handle_t qhalem_open_codec(int codec, qhalem_accesstype_t type)
{
    static qhalem_handle_t handle_index = 1;
    em_info_t *em_info_tail, *em_p;
    qhalhost_interface_t interface;
    qhalhost_handle_t host_h;

    switch (type) {
	case QHALEM_ACCESSTYPE_CMD:
	    interface = QHALHOST_BUS_EM1;
	    break;

	case QHALEM_ACCESSTYPE_STREAM:
	    interface = QHALHOST_BUS_EM1_STREAM;
	    break;

	case QHALEM_ACCESSTYPE_BITSTREAM:
	    interface = QHALHOST_BUS_BITSTREAM;
	    break;

	default:
	    return QHAL_ERR_NA;
    }

    host_h = qhalhost_open(codec, interface);
    if (host_h < 0)
	return host_h;
    
    em_p = (em_info_t *)qhalhost_malloc(host_h, sizeof(em_info_t));
    if (em_p == NULL) {
	qhalhost_close(host_h);
	return QHAL_ERR_MEM;
    }

    em_p->priority = QHALEM_PRIORITY_NORMAL;
    em_p->host_h = host_h;
    em_p->handle = handle_index++;
    em_p->next = NULL;
    em_p->curr_part_id = -1;
    em_p->curr_part_base = -1;
    APPEND_INFO(em_info_head, em_info_tail, em_p);
    return em_p->handle;
}

qhalem_handle_t qhalem_open(qhalem_accesstype_t type, qhalem_mode_t txmode)
{
    return qhalem_open_codec(0, type);
}

int qhalem_close(qhalem_handle_t em_h)
{
    em_info_t *em_info_tail, *em_p;
    qhalhost_handle_t host_h;
    
    FIND_INFO(em_info_head, em_p, em_h);
    host_h = em_p->host_h;
    
    REMOVE_INFO(em_info_head, em_info_tail, em_p);
    qhalhost_free(host_h, (void *)em_p);
    return qhalhost_close(host_h);
}

/****************************************************************************/

static int em_get_partbase(em_info_t *em_p,
			   unsigned char bid, unsigned long* base_p)
{
    unsigned long seg_aw;	// address width for this segment
    unsigned long reg;
    unsigned long reg2;
    int ret = 0;
    
    if ( (em_p->curr_part_id == bid) && (em_p->curr_part_base >= 0) ) {
	*base_p = em_p->curr_part_base;
	return QHAL_SUCCESS;
    }

    ret = qhalhost_read_qcc(em_p->host_h, QCC_BID_PMU0,
			    REG_ADDR(MEMS_MEM0_PMU_x_VPATSegSize), &reg,
			    REG_BYTES(MEMS_MEM0_PMU_x_VPATSegSize));
    //printf("em_get_partbase reg 0x%x ret %d\n", reg, ret);

    EXIT_ON_ERROR(ret);

    seg_aw = VPAT_SXSW - 3
	+ ((reg >> QCCFIELD_VPATSEGXSIZE_SHIFT)
	   & QCCFIELD_VPATSEGXSIZE_MASK)
	+ VPAT_SYSW - 3
	+ ((reg >> QCCFIELD_VPATSEGYSIZE_SHIFT)
	   & QCCFIELD_VPATSEGYSIZE_MASK);

    reg2 = GetPartitionBase(em_p, XTENSA_PARTITION_ID);
    reg  = GetPartitionBase(em_p, bid);

    *base_p = (reg - reg2) << seg_aw;
    em_p->curr_part_id = bid;
    em_p->curr_part_base = *base_p;

 err_exit:
    
    //printf("em_get_partbase DONE. reg 0x%x ret %d\n", reg, ret);

    return ret;
}

int qhalem_get_partbase(qhalem_handle_t em_h,
			unsigned char bid, unsigned long* base_p)
{
    em_info_t *em_p;
    int ret;

    if (base_p == NULL)
	return QHAL_ERR_BADPARAM;

    if (!EM_BID_OK(bid))
	return QHAL_ERR_BADPARAM;

    FIND_INFO(em_info_head, em_p, em_h);

    if (qhalhost_lock(em_p->host_h) != QHAL_SUCCESS)
	return QHAL_ERR_LOCK;

    ret = em_get_partbase(em_p, bid, base_p);

    qhalhost_unlock(em_p->host_h);

    return ret;
}

int qhalem_setconfig(qhalem_handle_t em_h,
		     char threshold, qhalem_burstsize_t burst, qhalem_priority_t priority)
{
    em_info_t *em_p;

    FIND_INFO(em_info_head, em_p, em_h);

    switch (priority) {
    case QHALEM_PRIORITY_NORMAL:
    case QHALEM_PRIORITY_LOWER:
    case QHALEM_PRIORITY_HIGHER:
    case QHALEM_PRIORITY_HIGHEST:
	em_p->priority = priority;
	break;
	
    default:
	return QHAL_ERR_BADPARAM;
    }
    return QHAL_SUCCESS;
}

/****************************************************************************/
#if 0
typedef enum {
    MEM_WRITE	= 0,
    MEM_READ	= 1,
} mem_cmd_t;

static int mem_send_cmd(em_info_t *em_p, mem_cmd_t cmd,
			unsigned char bid, unsigned long addr, int nWords)
{
    SHIF_TYPE(MemCmd) MemCmd;
    SHIF_TYPE(MemAddr) MemAddr;
    SHIF_TYPE(MemCount) MemCount;
    int ret;
    
    // Address the transfer:
    //
    MemAddr.b.MemAddr_Addr = addr;
    ret = qhalhost_write(em_p->host_h, SHIF_ADDR(MemAddr),
			 MemAddr.r, SHIF_BYTES(MemAddr));
    EXIT_ON_ERROR(ret);

    // Write number of words to transfer:
    //
    MemCount.b.MemCount_Count = nWords;
    ret = qhalhost_write(em_p->host_h, SHIF_ADDR(MemCount),
			 MemCount.r, SHIF_BYTES(MemCount));
    EXIT_ON_ERROR(ret);
    
    // Command the transfer:
    //
    MemCmd.b.MemCmd_RW = cmd;
    MemCmd.b.MemCmd_Pri = em_p->priority;
    MemCmd.b.MemCmd_PartId = bid;
    ret = qhalhost_write(em_p->host_h, SHIF_ADDR(MemCmd),
			 MemCmd.r, SHIF_BYTES(MemCmd));
err_exit:
    return ret;
}

static int mem_read_words(em_info_t *em_p, long **buffer_pp, int nWords, int swapEndian)
{
    long *buffer_p = *buffer_pp;
    volatile SHIF_TYPE(Status) Status;
    volatile SHIF_TYPE(MemRead) MemRead;
    int i, ret = QHAL_SUCCESS;
    int retWords;

    DbgTrace("mem_read_words nWords %d swap %d\n", nWords, swapEndian);

    while (nWords) {
	unsigned long s;
	ret = qhalhost_read(em_p->host_h, SHIF_ADDR(Status),
			    &s, SHIF_BYTES(Status));
	EXIT_ON_ERROR(ret);
	Status.r = (uint8_t)s;

	// Done bit is set when everything's finished
	// In actuality we may never hit this condition
	// by counting down nWords. That should be OK because
	// the FIFO should be emptied by the time the app sets up
	// the next r/w cmd, but if not we can add logic below the
	// 'for' loop below to wait for DONE if nWords == 0
	if (Status.b.Status_MemDone)
	    break;

	// If the FREE bit is set, the 8-word FIFO is full
	if (!Status.b.Status_Free)
	    continue;

	qhalhost_buffer_read_start(em_p->host_h);

	// FIFO reads may be successive after FREE bit set:
        if(swapEndian) {
            long ldata;
            for (i = 0; nWords && i < EM_DEFAULT_READ_BURSTSIZE; i++, nWords--) {
#if 0
                ret = qhalhost_read(em_p->host_h, SHIF_ADDR(MemRead),
				    (unsigned long *)&MemRead.r, SHIF_BYTES(MemRead));
                EXIT_ON_ERROR(ret);
                ldata = (long)MemRead.b.MemRead_Data;
                ldata = ((ldata >> 24) & 0xff) | (ldata << 24) | ((ldata >> 8) & 0xff00) | ((ldata & 0xff00) << 8);
		*buffer_p++ = ldata;
#else
		qhalhost_buffer_read(em_p->host_h, SHIF_ADDR(MemRead));
#endif

            }
        }
        else {
            for (i = 0; nWords && i < EM_DEFAULT_READ_BURSTSIZE; i++, nWords--) {
#if 0
                ret = qhalhost_read(em_p->host_h, SHIF_ADDR(MemRead),
				    (unsigned long *)&MemRead.r, SHIF_BYTES(MemRead));
                EXIT_ON_ERROR(ret);
                *buffer_p++ = (long)MemRead.b.MemRead_Data;
#else
		qhalhost_buffer_read(em_p->host_h, SHIF_ADDR(MemRead));
#endif
            }
        }

#if 1
	retWords = qhalhost_buffer_read_end(em_p->host_h, buffer_p);
	if (swapEndian) {
	    int j;
	    for (j = 0; j < retWords; j++) {
		long wd = buffer_p[j];
		buffer_p[j] = ((wd >> 24) & 0xff) | (wd << 24) | ((wd >> 8) & 0xff00) | ((wd & 0xff00) << 8);
	    }
	}
	buffer_p += retWords;
#endif
	
    }

 err_exit:
    *buffer_pp = buffer_p;
    return ret;
}

static int mem_write_words(em_info_t *em_p, long **buffer_pp, int nWords, int swapEndian)
{
    long *buffer_p = *buffer_pp;
    volatile SHIF_TYPE(Status) Status;
    SHIF_TYPE(MemWrite) MemWrite;
    int i, ret = QHAL_SUCCESS;

    while (nWords) {
        unsigned long s;
        ret = qhalhost_read(em_p->host_h, SHIF_ADDR(Status),
                            &s, SHIF_BYTES(Status));
        EXIT_ON_ERROR(ret);
        Status.r = (uint8_t)s;

	// Done bit is set when everything's finished
	// In actuality we may never hit this condition
	// by counting down nWords. That should be OK because
	// the FIFO should be emptied by the time the app sets up
	// the next r/w cmd, but if not we can add logic below the
	// 'for' loop below to wait for DONE if nWords == 0
	if (Status.b.Status_MemDone)
	    break;

	// If the FREE bit is set, the 8-word FIFO is empty
	if (!Status.b.Status_Free)
	    continue;

	// FIFO writes may be successive up to FIFO depth:

	qhalhost_buffer_write_start(em_p->host_h);

        if(swapEndian) {
            long ldata;
            for (i = 0; nWords && i < EM_DEFAULT_WRITE_BURSTSIZE; i++, nWords--) {
                ldata = *buffer_p;
                ldata = ((ldata >> 24) & 0xff) | (ldata << 24) | ((ldata >> 8) & 0xff00) | ((ldata & 0xff00) << 8);
                MemWrite.b.MemWrite_Data = ldata;
#if 0
                ret = qhalhost_write(em_p->host_h, SHIF_ADDR(MemWrite),
				     MemWrite.r, SHIF_BYTES(MemWrite));
                EXIT_ON_ERROR(ret);
#else
		qhalhost_buffer_write(em_p->host_h, SHIF_ADDR(MemWrite),
				      MemWrite.r);
#endif
                buffer_p++;
            }
        }
        else {
            for (i = 0; nWords && i < EM_DEFAULT_WRITE_BURSTSIZE; i++, nWords--) {
                MemWrite.b.MemWrite_Data = *buffer_p;
#if 0
                ret = qhalhost_write(em_p->host_h, SHIF_ADDR(MemWrite),
				     MemWrite.r, SHIF_BYTES(MemWrite));
                EXIT_ON_ERROR(ret);
#else
		qhalhost_buffer_write(em_p->host_h, SHIF_ADDR(MemWrite),
				      MemWrite.r);
#endif
                buffer_p++;
            }
        }
	
#if 1
	qhalhost_buffer_write_end(em_p->host_h);
#endif

    }

err_exit:
    *buffer_pp = buffer_p;
    return ret;
}
#endif
/****************************************************************************/

int qhalem_read_bytes(qhalem_handle_t em_h, unsigned char bid,
		      unsigned long addr, char *buffer_p, int nBytes)
{
    em_info_t *em_p;
    unsigned long base;
    int ret;
    int bytesRemain = nBytes;
    int bytesToRead = 0;
    int offset = 0;

    //printf("qhalem_read_bytes bid %d addr 0x%x bytes %d\n", bid, (int)addr, nBytes);

    if (buffer_p == NULL)
	return QHAL_ERR_BADPARAM;

    if (!EM_BID_OK(bid) || !EM_ADDR_OK(addr))
	return QHAL_ERR_BADPARAM;

    FIND_INFO(em_info_head, em_p, em_h);

    if (qhalhost_lock(em_p->host_h) != QHAL_SUCCESS)
	return QHAL_ERR_LOCK;

    ret = em_get_partbase(em_p, bid, &base);
    EXIT_ON_ERROR(ret);

#if 1
#if 0
    ret = qhalhost_read_block(em_p->host_h, base + addr,
			      (unsigned long *)buffer_p, 1, nBytes);
#else
    bytesRemain = nBytes;
    offset = 0;
    while (bytesRemain > 0) {
	bytesToRead = (bytesRemain > QHAL_USB_MAX_XFER_SZ) ? QHAL_USB_MAX_XFER_SZ : bytesRemain;
	ret = qhalhost_read_block(em_p->host_h, base + addr + offset,
				  (unsigned long *)(buffer_p + offset), 1, bytesToRead);
	offset += bytesToRead;
	bytesRemain -= bytesToRead;
    }
#endif
#else
    // for testing purpose
    ret = qhalhost_read(em_p->host_h, base + addr,
			(unsigned long *)buffer_p, nBytes);
#endif    
    

 err_exit:
    qhalhost_unlock(em_p->host_h);

    //printf("qhalem_read_bytes DONE. bid %d addr 0x%x bytes %d\n", bid, (int)addr, nBytes);

    return ret;
}

int qhalem_read_words(qhalem_handle_t em_h, unsigned char bid,
		      unsigned long addr, long *buffer_p, int nWords)
{
    em_info_t *em_p;
    unsigned long base;
    int ret;
    int bytesRemain = nWords * 4;
    int bytesToRead = 0;
    int offset = 0;
#if __BYTE_ORDER == LITTLE_ENDIAN
    int i;
    unsigned char *pbuf;
#endif

    //printf("qhalem_read_words bid %d addr 0x%x words %d\n", bid, (int)addr, nWords);

    if (buffer_p == NULL)
	return QHAL_ERR_BADPARAM;

    if (!EM_BID_OK(bid) || !EM_ADDR_OK(addr))
	return QHAL_ERR_BADPARAM;

    FIND_INFO(em_info_head, em_p, em_h);
    if (qhalhost_lock(em_p->host_h) != QHAL_SUCCESS)
	return QHAL_ERR_LOCK;

    ret = em_get_partbase(em_p, bid, &base);
    EXIT_ON_ERROR(ret);

#if 0
    ret = qhalhost_read_block(em_p->host_h, base + addr,
			      (unsigned long *)buffer_p, 4, nWords);
#else
    bytesRemain = nWords * 4;
    offset = 0;
    while (bytesRemain > 0) {
	bytesToRead = (bytesRemain > QHAL_USB_MAX_XFER_SZ) ? QHAL_USB_MAX_XFER_SZ : bytesRemain;
	ret = qhalhost_read_block(em_p->host_h, base + addr + offset,
				  (unsigned long *)(buffer_p + offset), 4, bytesToRead/4);
	offset += bytesToRead;
	bytesRemain -= bytesToRead;
    }
#endif

#if __BYTE_ORDER == LITTLE_ENDIAN
    pbuf = (unsigned char *)buffer_p;
    for (i = 0; i < nWords * 4; i += 4) {
	char tmp = pbuf[i];
	pbuf[i] = pbuf[i+3];
	pbuf[i+3] = tmp;
	tmp = pbuf[i+1];
	pbuf[i+1] = pbuf[i+2];
	pbuf[i+2] = tmp;
    }
#endif


 err_exit:
    qhalhost_unlock(em_p->host_h);

    //printf("qhalem_read_words DONE. bid %d addr 0x%x words %d\n", bid, (int)addr, nWords);

    return ret;
}

int qhalem_read_fbframe(qhalem_handle_t em_h, unsigned char bid,
			unsigned long addrX, unsigned long addrY,
			char *buffer_p, int sizeX, int sizeY)
{
    return QHAL_ERR_NA;
}

int qhalem_write_bytes(qhalem_handle_t em_h, unsigned char bid,
		       unsigned long addr, char *buffer_p, int nBytes)
{
    em_info_t *em_p;
    unsigned long base;
    int ret;
    int bytesRemain = nBytes;
    int bytesToWrite = 0;
    int offset = 0;

    //printf("qhalem_write_bytes bid %d addr 0x%x bytes %d\n", bid, (int)addr, nBytes);

    if (buffer_p == NULL)
	return QHAL_ERR_BADPARAM;

    if (!EM_BID_OK(bid) || !EM_ADDR_OK(addr))
	return QHAL_ERR_BADPARAM;

    FIND_INFO(em_info_head, em_p, em_h);
    if (qhalhost_lock(em_p->host_h) != QHAL_SUCCESS)
	return QHAL_ERR_LOCK;

    ret = em_get_partbase(em_p, bid, &base);
    EXIT_ON_ERROR(ret);

#if 0
    ret = qhalhost_write_block(em_p->host_h, base + addr,
			       (unsigned long *)buffer_p, 1, nBytes);
#else
    bytesRemain = nBytes;
    offset = 0;
    while (bytesRemain > 0) {
	bytesToWrite = (bytesRemain > QHAL_USB_MAX_XFER_SZ) ? QHAL_USB_MAX_XFER_SZ : bytesRemain;
	ret = qhalhost_write_block(em_p->host_h, base + addr + offset,
				   (unsigned long *)(buffer_p + offset), 1, bytesToWrite);
	offset += bytesToWrite;
	bytesRemain -= bytesToWrite;
    }
#endif

 err_exit:
    qhalhost_unlock(em_p->host_h);

    //printf("qhalem_write_bytes DONE. bid %d addr 0x%x bytes %d\n", bid, (int)addr, nBytes);
    
    return ret;
}

int qhalem_write_words(qhalem_handle_t em_h, unsigned char bid,
		       unsigned long addr, long *buffer_p, int nWords)
{
    em_info_t *em_p;
    unsigned long base;
    int ret;
    int bytesRemain;
    int bytesToWrite = 0;
    int offset = 0;
#if __BYTE_ORDER == LITTLE_ENDIAN
    int i;
    unsigned char *pbuf;
#endif

    //printf("qhalem_write_words bid %d addr 0x%x words %d\n", bid, (int)addr, nWords);

    if (buffer_p == NULL)
	return QHAL_ERR_BADPARAM;

    if (!EM_BID_OK(bid) || !EM_ADDR_OK(addr))
	return QHAL_ERR_BADPARAM;

    FIND_INFO(em_info_head, em_p, em_h);

    if (qhalhost_lock(em_p->host_h) != QHAL_SUCCESS)
	return QHAL_ERR_LOCK;

    ret = em_get_partbase(em_p, bid, &base);
    EXIT_ON_ERROR(ret);

#if __BYTE_ORDER == LITTLE_ENDIAN
    pbuf = (unsigned char *)buffer_p;
    for (i = 0; i < nWords * 4; i += 4) {
	char tmp = pbuf[i];
	pbuf[i] = pbuf[i+3];
	pbuf[i+3] = tmp;
	tmp = pbuf[i+1];
	pbuf[i+1] = pbuf[i+2];
	pbuf[i+2] = tmp;
    }
#endif

#if 0
    ret = qhalhost_write_block(em_p->host_h, base + addr,
			      (unsigned long *)buffer_p, 4, nWords);
#else
    bytesRemain = nWords * 4;
    offset = 0;
    while (bytesRemain > 0) {
	bytesToWrite = (bytesRemain > QHAL_USB_MAX_XFER_SZ) ? QHAL_USB_MAX_XFER_SZ : bytesRemain;
	ret = qhalhost_write_block(em_p->host_h, base + addr + offset,
				   (unsigned long *)(buffer_p + offset), 4, bytesToWrite/4);
	offset += bytesToWrite;
	bytesRemain -= bytesToWrite;
    }   
#endif

 err_exit:
    qhalhost_unlock(em_p->host_h);

    //printf("qhalem_write_words DONE. bid %d addr 0x%x words %d\n", bid, (int)addr, nWords);

    return ret;
}

int qhalem_write_fbframe(qhalem_handle_t em_h, unsigned char bid,
			 unsigned long addrX, unsigned long addrY,
			 char *buffer_p, int sizeX, int sizeY)
{
    return QHAL_ERR_NA;
}

