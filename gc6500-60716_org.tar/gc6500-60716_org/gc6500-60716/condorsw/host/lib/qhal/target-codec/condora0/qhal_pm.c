/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include "qhal_priv.h"
#include "qhal_host.h"
#include "qhal_platform.h"
#include "qhal_pm.h"

typedef struct pm_info_struct {
	int codec;
	unsigned long xin_Hz;
	qhalhost_handle_t host_h;
	qhalpm_handle_t handle;
	struct pm_info_struct *next;
} pm_info_t;

static pm_info_t *pm_info_head = NULL;

qhalpm_handle_t qhalpm_open_codec(int codec, unsigned long xin_Hz)
{
	static qhalpm_handle_t handle_index = 1;
	pm_info_t *pm_info_tail, *pm_p;
	qhalhost_handle_t host_h;

	host_h = qhalhost_open(codec, QHALHOST_BUS_CSR);
	if (host_h < 0)
		return host_h;

	pm_p = (pm_info_t *)qhalhost_malloc(host_h, sizeof(pm_info_t));
	if (pm_p == NULL)
		return QHAL_ERR_MEM;

	pm_p->xin_Hz = xin_Hz;
	pm_p->codec = codec;
	pm_p->host_h = host_h;
	pm_p->handle = handle_index++;
	pm_p->next = NULL;
	APPEND_INFO(pm_info_head, pm_info_tail, pm_p);
	return pm_p->handle;
}

qhalpm_handle_t qhalpm_open(unsigned long xin_Hz)
{
	return qhalpm_open_codec(0, xin_Hz);
}

int qhalpm_close(qhalpm_handle_t pm_h)
{
	pm_info_t *pm_info_tail, *pm_p;
	qhalhost_handle_t host_h;
	int ret;

	FIND_INFO(pm_info_head, pm_p, pm_h);
	host_h = pm_p->host_h;

	REMOVE_INFO(pm_info_head, pm_info_tail, pm_p);
	ret = qhalhost_close(pm_p->host_h);
	if(ret != QHAL_SUCCESS)
		return ret;

	ret = qhalhost_free(host_h, (void *)pm_p);

	return ret;
}

/****************************************************************************/

int qhalpm_reset(qhalpm_handle_t pm_h)
{
	pm_info_t *pm_p;
	int ret;

	FIND_INFO(pm_info_head, pm_p, pm_h);
	if (qhalhost_lock(pm_p->host_h) != QHAL_SUCCESS)
		return QHAL_ERR_LOCK;

	ret = qhalhost_reset(pm_p->host_h);
	qhalhost_unlock(pm_p->host_h);
	return ret;
}

int qhalpm_setclock(qhalpm_handle_t pm_h, unsigned long sysclk_Hz)
{
	return QHAL_SUCCESS;
}

int qhalpm_powerdown(qhalpm_handle_t pm_h)
{
	return QHAL_ERR_NA;
}

int qhalpm_wakeup(qhalpm_handle_t pm_h)
{
	return QHAL_ERR_NA;
}

