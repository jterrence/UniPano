/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include "qhal_priv.h"
#include "qhal_host.h"
#include "qhal_platform.h"
#include "qhal_bs.h"

typedef struct bs_info_struct {
	qhalhost_handle_t host_h;
	qhalbs_handle_t handle;
	struct bs_info_struct *next;
} bs_info_t;

static bs_info_t *bs_info_head = NULL;

qhalbs_handle_t qhalbs_open_codec(int codec)
{
	static qhalbs_handle_t handle_index = 1;
	bs_info_t *bs_info_tail, *bs_p;
	qhalhost_handle_t host_h;

	host_h = qhalhost_open(codec, QHALHOST_BUS_CSR);
	if (host_h < 0)
		return host_h;

	bs_p = (bs_info_t *)qhalhost_malloc(host_h, sizeof(bs_info_t));
	if (bs_p == NULL) {
		qhalhost_close(host_h);
		return QHAL_ERR_MEM;
	}

	bs_p->host_h = host_h;
	bs_p->handle = handle_index++;
	bs_p->next = NULL;
	APPEND_INFO(bs_info_head, bs_info_tail, bs_p);
	return bs_p->handle;
}

qhalbs_handle_t qhalbs_open(void)
{
    return qhalbs_open_codec(0);
}

int qhalbs_close( qhalbs_handle_t bs_h)
{
	bs_info_t *bs_info_tail, *bs_p;
	qhalhost_handle_t host_h;

	FIND_INFO(bs_info_head, bs_p, bs_h);
	host_h = bs_p->host_h;

	REMOVE_INFO(bs_info_head, bs_info_tail, bs_p);
	qhalhost_free(host_h, (void *)bs_p);
	return qhalhost_close(host_h);
}

/****************************************************************************/

int qhalbs_setconfig(qhalbs_handle_t bs_h, int threshold)
{
	bs_info_t *bs_p;

	FIND_INFO(bs_info_head, bs_p, bs_h);
	return QHAL_ERR_NA;
}

int qhalbs_write(qhalbs_handle_t bs_h, char *buffer_p, int length)
{
	bs_info_t *bs_p;

	if (buffer_p == NULL)
		return QHAL_ERR_BADPARAM;

	FIND_INFO(bs_info_head, bs_p, bs_h);
    return QHAL_ERR_NA;
}

/****************************************************************************/

int qhalbs_udp_bindfd(qhalbs_handle_t bs_h, int socket_fd)
{
    return QHAL_ERR_NA;
}

int qhalbs_udp_get_stats(qhalbs_handle_t bs_h, qhalbs_udp_stats_t *stats_p)
{
    return QHAL_ERR_NA;
}

