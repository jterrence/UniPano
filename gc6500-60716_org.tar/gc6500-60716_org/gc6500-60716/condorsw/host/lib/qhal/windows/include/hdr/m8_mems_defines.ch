/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to Maxim Integrated.  It is subject to the terms of a License Agreement 
* between Licensee and Maxim Integrated, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and * any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2012 Maxim Integrated, Inc.
* All rights reserved.
* 
* QuArc and Mobilygen are registered trademarks of Maxim Integrated.
* 
*******************************************************************************/

/*******************************************************************************
* @(#) $Id: m8_mems_defines.ch 87848 2012-09-10 22:19:55Z henrys $
*******************************************************************************/
#ifndef M8_MEMS_DEFINES_CH
#define M8_MEMS_DEFINES_CH

// Are these correct?
#define M8_MEMS_PMC_CSR_BASE    0
#define M8_MEMS_PMU_CSR_BASE    0
#define M8_MEMS_MMU_CSR_BASE    0


#endif  /* M8_MEMS_DEFINES_CH */
/*******************************************************************************
* vi: set shiftwidth=4 tabstop=8 softtabstop=4 expandtab nosmarttab:
*******************************************************************************/
