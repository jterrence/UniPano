// *****************************************************************************
//  
//  The content of this file or document is CONFIDENTIAL and PROPRIETARY
//  to Mobilygen Corporation.  It is subject to the terms of a
//  License Agreement between Licensee and Mobilygen Corporation.
//  restricting among other things, the use, reproduction, distribution
//  and transfer.  Each of the embodiments, including this information and
//  any derivative work shall retain this copyright notice.
//  
//  Copyright 2006 - 2013 Mobilygen Corporation.
//  All rights reserved.
//  
//  QuArc is a registered trademark of Mobilygen Corporation.
//  
// *****************************************************************************

// *****************************************************************************
// @(#) $Id: m8_qcc_bid.h 95359 2013-08-22 20:53:18Z henrys $
// *****************************************************************************

// *****************************************************************************
// QCC Block ID Assignments
// *****************************************************************************

// -----------------------------------------------------------------------------
// General scheme:  define the Block IDs for QCC clients in each clock domain
// such that that group of blocks can be identified by a common base Id and
// mask.  That way the new "QCC router" module can bypass loops that aren't
// necessary.
// -----------------------------------------------------------------------------

//------------------------------------------------------------------------------
// QMM DOMAIN
//------------------------------------------------------------------------------
`define   QCC_BID_QDBG_SYS      8'h01   // QMM Debug Unit (System QMM)
`define   QCC_BID_UART0         8'h02   // QMM UART0
`define   QCC_BID_SBC_MMU       8'h03   // SBC's MMU
`define   QCC_BID_QMM_MASK      8'hF8   // Mask

//------------------------------------------------------------------------------
// PME:
//------------------------------------------------------------------------------
`define   QCC_BID_PME           8'h08   // PME
`define   QCC_BID_PME_MASK      8'hFF   // Mask

//------------------------------------------------------------------------------
// Audio and ChipCtl share the same router
//------------------------------------------------------------------------------
`define   QCC_BID_AII0          8'h10   // Audio Input Unit #0
`define   QCC_BID_AII1          8'h11   // Audio Input Unit #1
`define   QCC_BID_AOI           8'h12   // Audio Output Unit ( unused )
`define   QCC_BID_CHIPCTL       8'h13   // QCC Chip Control Unit
`define   QCC_BID_AUD_MASK      8'hFC   // Mask

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// MMU0, PMU0, PMC0, GPU, and FBR2 : These are on mem_clk domain router
//------------------------------------------------------------------------------
`define   QCC_BID_MMU0          8'h20   // Memory Management Unit #0
`define   QCC_BID_PMU0          8'h21   // Partition Management Unit #0
`define   QCC_BID_PMC0          8'h22   // Physical Memory Control Unit #0
`define   QCC_BID_MMU0_MASK     8'hFC   // Mask

`define   QCC_BID_FBR2          8'h24   // Frame Buffer Read Unit #2 (for USB)
`define   QCC_BID_FBR2_MASK     8'hFE   // FBR2  Mask (1 FBR only)

`define   QCC_BID_GPU           8'h26   // Graphics Processing Unit
`define   QCC_BID_GPU_MASK      8'hFF   // Mask

`define   QCC_BID_MEM_ALL_MASK  8'hF8   // Mask for all mem_clk BIDs

//------------------------------------------------------------------------------
//DDR PHY
//------------------------------------------------------------------------------
`define   QCC_BID_PHYCTRL       8'h30   // PUB portion of DDR-PHY Control Unit
`define   QCC_BID_PHYCTRL1      8'h31   // Misc portion of DDR-PHY Control Unit
`define   QCC_BID_PHYCTRL_MASK  8'hFE   // Mask

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Video SS BIDs
//------------------------------------------------------------------------------
// The following modules take 2 BID slots and must be located at an EVEN BID address:
//   VPP, VSC, TFCC, FBW, and FBR
//------------------------------------------------------------------------------
// VIDEO LOOP: All the video components: BID addresses 0x40 - 0x5F
//------------------------------------------------------------------------------
`define   QCC_BID_VIDEO         8'h40   // Base address for video blocks
`define   QCC_BID_VIDEO_MASK    8'hE0   // Mask for the entire video subsystem

`define   QCC_BID_VPP0          8'h40   // Video Pre-Processing Unit #0
`define   QCC_BID_CROP1         8'h41
`define   QCC_BID_FBW1          8'h42   // Frame Buffer Write Unit 1
`define   QCC_BID_VPP0_MASK     8'hFC   // Mask for VPP and FBW

//`define   QCC_BID_VPP1          8'h44   // Video Pre-Processing Unit #1
//`define   QCC_BID_CROP1         8'h45
//`define   QCC_BID_FBW1          8'h46   // Frame Buffer Write Unit 3
//`define   QCC_BID_VPP1_MASK     8'hFC   // Mask for VPP and FBW
//
`define   QCC_BID_VSC0          8'h48   // VScale 0
`define   QCC_BID_CROP2         8'h49
`define   QCC_BID_FBW2          8'h4A   // Frame Buffer Write Unit 2
`define   QCC_BID_VSC0_MASK     8'hFC   // Mask for VSC and FBW

`define   QCC_BID_VSC1          8'h4C   // VScale 1
`define   QCC_BID_CROP3         8'h4D
`define   QCC_BID_FBW3          8'h4E   // Frame Buffer Write Unit 3
`define   QCC_BID_VSC1_MASK     8'hFC   // Mask for VSC and FBW
//
//DUMMY_BID_START                       // All BIDs after this until DUMMY_BID_END will be treated by const2h script as dummy for .pl generation
`define   QCC_BID_FBR           8'h50   // Frame Buffer Read Unit :: placeholder to keep a TB happy ?
//DUMMY_BID_END
`define   QCC_BID_FBR0          8'h50   // Frame Buffer Read Unit #0
`define   QCC_BID_FBR1          8'h52   // Frame Buffer Read Unit #1
`define   QCC_BID_FBR_MASK      8'hFE   // FBR   Mask (1 FBR only)
//
//DUMMY_BID_START                       // All BIDs after this until DUMMY_BID_END will be treated by const2h script as dummy for .pl generation
`define   QCC_BID_FBW           8'h54   // Frame Buffer Write Unit  :: placeholder to keep a TB happy?
//DUMMY_BID_END
`define   QCC_BID_FBW0          8'h54   // Frame Buffer Write Unit 0
`define   QCC_BID_FBW_MASK      8'hFE   // FBW Mask   (1 FBW only)
//
`define   QCC_BID_TFCC0         8'h58   // TFCC 0
`define   QCC_BID_CROP0         8'h59
`define   QCC_BID_TFCC_MASK     8'hFE   // TFCC   Mask
`define   QCC_BID_TFCC0_MASK    8'hFE   // TFCC 0 Mask

//`define   QCC_BID_TFCC1         8'h5A   // TFCC 1
//`define   QCC_BID_CROP3         8'h5B
//`define   QCC_BID_TFCC1_MASK    8'hFE   // TFCC 1 Mask
//
`define   QCC_BID_DW            8'h5C   // DeWarp
`define   QCC_BID_DW_MASK       8'hFF   // DeWarp Mask

`define   QCC_BID_VIDMUX        8'h5D   // Video Mux Unit
`define   QCC_BID_VIDMUX_MASK   8'hFF   // Video Mux Mask
//
`define   QCC_BID_MIPI0         8'h5E   // MIPI Unit #0
//`define   QCC_BID_MIPI1         8'h5F   // MIPI Unit #1
`define   QCC_BID_MIPI_MASK     8'hFF   // Mask for both MIPI BIDs
//
//------------------------------------------------------------------------------
// End of Video SS BID-specific items
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// VPU0 - needed only for FPGA
//------------------------------------------------------------------------------
`define   QCC_BID_VPU0          8'h70   // Video Processing Unit #0 (Video Output)
`define   QCC_BID_VPU0_MASK     8'hFE   // Mask (reserve BID 51 - VCK0 placeholder)

// -----------------------------------------------------------------------------
// Bogus QCC BID for components that are not on a ring.
// Just give them unique ID as placeholders.
// -----------------------------------------------------------------------------
`define QCC_BID_IPPIF           8'h80
`define QCC_BID_JCIF            8'h81
`define QCC_BID_QCCWDMA         8'h82
`define QCC_BID_SBC_SC_101_1000 8'h84
`define QCC_BID_SBC_SC_000_1000 8'h85
`define QCC_BID_SBC_SC_100_1000 8'h86
`define QCC_BID_VCIF            8'h89
`define QCC_BID_QCCRTC          8'h8a
`define QCC_BID_SERIAL_HOSTIF   8'h8b
`define QCC_BID_MEMCTRL         8'h8c
`define QCC_BID_PIF2MMU         8'h8d
`define QCC_BID_ISP             8'h8e
`define QCC_BID_AHB_DMA_MMU     8'h8f

//------------------------------------------------------------------------------

// *****************************************************************************
// vi: set shiftwidth=4 tabstop=8 softtabstop=4 expandtab nosmarttab filetype=verilog:
// emacs: (set-buffer-verilog-mode)
// *****************************************************************************
