/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to Mobilygen Corporation.  It is subject to the terms of a
* License Agreement between Licensee and Mobilygen Corporation.
* restricting among other things, the use, reproduction, distribution
* and transfer.  Each of the embodiments, including this information and
* any derivative work shall retain this copyright notice.
* 
* Copyright 2006 - 2013 Mobilygen Corporation.
* All rights reserved.
* 
* QuArc is a registered trademark of Mobilygen Corporation.
* 
*******************************************************************************/

/*******************************************************************************
* @(#) $Id: m8_reg_defines.h 95359 2013-08-22 20:53:18Z henrys $
*******************************************************************************/
#ifndef M8_REG_DEFINES_H
#define M8_REG_DEFINES_H

#include    "hdr/m8_mems_defines.ch"
#include    "hdr/m8_qcc_bid.ch"
#include    "blocks/video/src/m8_video_reg_defines.h"

#define REG_BID_QDBG_SYS    QCC_BID_QDBG_SYS
#define REG_BID_QDBG_VID    QCC_BID_QDBG_VID
#define REG_BID_MBOX        QCC_BID_MBOX
#define REG_BID_UART0       QCC_BID_UART0
#define REG_BID_UART1       QCC_BID_UART1
#define REG_BID_CHIPCTL     QCC_BID_CHIPCTL
#define REG_BID_SBC_MMU     QCC_BID_SBC_MMU

#define REG_BID_PME         QCC_BID_PME

#define REG_BID_MMU0        QCC_BID_MMU0
#define REG_BID_PMC0        QCC_BID_PMC0
#define REG_BID_PMU0        QCC_BID_PMU0

#define REG_BID_AII0        QCC_BID_AII0
#define REG_BID_AII1        QCC_BID_AII1
#define REG_BID_AOI         QCC_BID_AOI

#define REG_BID_VPU0        QCC_BID_VPU0

#define REG_BID_VIN0        QCC_BID_VIN0
#define REG_BID_VPP0        QCC_BID_VPP0
#define REG_BID_FBR0        QCC_BID_FBR0
#define REG_BID_FBW         QCC_BID_FBW

#define REG_BID_VIN1        QCC_BID_VIN1
#define REG_BID_VPP1        QCC_BID_VPP1
#define REG_BID_FBR1        QCC_BID_FBR1

#define REG_BID_VIDMUX      QCC_BID_VIDMUX

#define REG_BID_PHYCTRL     QCC_BID_PHYCTRL
#define REG_BID_PHYCTRL1    QCC_BID_PHYCTRL1

// -----------------------------------------------------------------------------
// Bogus QCC BID for components that are not on a ring.
// Just give them unique ID as placeholders.
// -----------------------------------------------------------------------------
#define REG_BID_IPPIF           QCC_BID_IPPIF           
#define REG_BID_JCIF            QCC_BID_JCIF            
#define REG_BID_QCCWDMA         QCC_BID_QCCWDMA         
#define REG_BID_SBCMMU          QCC_BID_SBCMMU          
#define REG_BID_SBC_SC_101_1000 QCC_BID_SBC_SC_101_1000 
#define REG_BID_SBC_SC_000_1000 QCC_BID_SBC_SC_000_1000 
#define REG_BID_SBC_SC_100_1000 QCC_BID_SBC_SC_100_1000 
#define REG_BID_VCIF            QCC_BID_VCIF            
#define REG_BID_QCCRTC          QCC_BID_QCCRTC       
#define REG_BID_SERIAL_HOSTIF   QCC_BID_SERIAL_HOSTIF
#define REG_BID_PIF2MMU         QCC_BID_PIF2MMU
#define REG_BID_ISP             QCC_BID_ISP    
#define REG_BID_AHB_DMA_MMU     QCC_BID_AHB_DMA_MMU     

#endif  /* M8_REG_DEFINES_H */
/*******************************************************************************
* vi: set shiftwidth=4 tabstop=8 softtabstop=4 expandtab nosmarttab:
*******************************************************************************/
