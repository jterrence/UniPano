/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to Mobilygen Corporation.  It is subject to the terms of a
* License Agreement between Licensee and Mobilygen Corporation.
* restricting among other things, the use, reproduction, distribution
* and transfer.  Each of the embodiments, including this information and
* any derivative work shall retain this copyright notice.
* 
* Copyright 2006 - 2012 Mobilygen Corporation.
* All rights reserved.
* 
* QuArc is a video_registered trademark of Mobilygen Corporation.
* 
*******************************************************************************/

/*******************************************************************************
* @(#) $Id: m8_video_reg_defines.h 88576 2012-10-16 22:49:51Z henrys $
*******************************************************************************/
#ifndef M8_VIDEO_REG_DEFINES_H
#define M8_VIDEO_REG_DEFINES_H

/*******************************************************************************
* The purpose of this chunk of code is to have the RCS/CVS/SVN keyword string
* embedded in an object file.
*
* Requirements:
* . The char array containing the keywords must be used so as to avoid compiler
*   warning.  A bogus function is created to use the said char array.
* . The .cc file must define 'CC_IDENT_NAMESPACE' with a legal string of 
*   characters.
*******************************************************************************/
#ifdef CC_IDENT_NAMESPACE
#define KW_JOIN(x,y) x ## _ ## y
#define KW_XJOIN(x,y) KW_JOIN(x,y)
#define HH_IDENT_FUNC_NAME KW_XJOIN(CC_IDENT_NAMESPACE, M8_VIDEO_REG_DEFINES_H_FUNC)
const char* HH_IDENT_FUNC_NAME()
{
    static char rcs_id[] = "$Id: m8_video_reg_defines.h 88576 2012-10-16 22:49:51Z henrys $" ;
    return rcs_id ;
}
#undef HH_IDENT_FUNC_NAME
#undef KW_XJOIN
#undef KW_JOIN
#endif // CC_IDENT_NAMESPACE


#include    "hdr/m8_qcc_bid.ch"

#define REG_BID_CROP0       QCC_BID_CROP0       
#define REG_BID_CROP1       QCC_BID_CROP1       
#define REG_BID_CROP2       QCC_BID_CROP2       
#define REG_BID_CROP3       QCC_BID_CROP3       

#define REG_BID_VSC0        QCC_BID_VSC0        
#define REG_BID_VSC0        QCC_BID_VSC0        
#define REG_BID_VSC0        QCC_BID_VSC0        
#define REG_BID_VSC0        QCC_BID_VSC0        
#define REG_BID_VSC0        QCC_BID_VSC0        

#define REG_BID_VSC1        QCC_BID_VSC1        
#define REG_BID_VSC1        QCC_BID_VSC1        
#define REG_BID_VSC1        QCC_BID_VSC1        
#define REG_BID_VSC1        QCC_BID_VSC1        
#define REG_BID_VSC1        QCC_BID_VSC1        

#define REG_BID_VPP0        QCC_BID_VPP0        
#define REG_BID_VPP0        QCC_BID_VPP0        
#define REG_BID_VPP0        QCC_BID_VPP0        
#define REG_BID_VPP0        QCC_BID_VPP0        
#define REG_BID_VPP0        QCC_BID_VPP0        

#define REG_BID_TFCC0       QCC_BID_TFCC0       
#define REG_BID_TFCC0       QCC_BID_TFCC0       
#define REG_BID_TFCC0       QCC_BID_TFCC0       
#define REG_BID_TFCC0       QCC_BID_TFCC0       
#define REG_BID_TFCC0       QCC_BID_TFCC0       

#define REG_BID_FBR0        QCC_BID_FBR0        
#define REG_BID_FBR1        QCC_BID_FBR1        

#define REG_BID_FBW0        QCC_BID_FBW0        
#define REG_BID_FBW1        QCC_BID_FBW1        
#define REG_BID_FBW2        QCC_BID_FBW2        
#define REG_BID_FBW3        QCC_BID_FBW3        

#define REG_BID_VIDMUX      QCC_BID_VIDMUX      

#endif  /* M8_VIDEO_REG_DEFINES_H */
/*******************************************************************************
* vi: set shiftwidth=4 tabstop=8 softtabstop=4 expandtab nosmarttab:
*******************************************************************************/
