#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================
#===============================================================================
# @(#) $Id: Makefile 59930 2001-05-07 12:09:34Z bsmith $
#===============================================================================

ifndef	GEOSW_ROOT
$(error Variable GEOSW_ROOT must be defined)
endif

CC = gcc
AR = ar

LIBRARY = libqhal.a

SRCDIR = src
QHAL_HOST_USB_SRC = $(GEOSW_ROOT)/condorsw/host/lib/qhal/host/usb/condor-libusb
QHAL_TARGET_USB_SRC = $(GEOSW_ROOT)/condorsw/host/lib/qhal/target-codec/condora0-usb
BUILDDIR = build
INCLUDEDIR = include

QHAL_HOST_USB_FILES =	\
	$(QHAL_HOST_USB_SRC)/qhal_platform.c
	
QHAL_TARGET_USB_FILES =	\
	$(QHAL_TARGET_USB_SRC)/qhal_bs.c	\
	$(QHAL_TARGET_USB_SRC)/qhal_em.c	\
	$(QHAL_TARGET_USB_SRC)/qhal_pm.c	\
	$(QHAL_TARGET_USB_SRC)/qhal_qcc.c

CFLAGS	+=	\
	-Os	\
	-Wall	\
	-Wno-empty-body	\
	-Wno-unused-parameter	\
	-Wno-unused-variable	\
	-Wno-unused-function	\
	-I/usr/include/libusb-1.0	\
	-I$(GEOSW_ROOT)/condorsw/host/lib/include	\
	-I$(INCLUDEDIR)	\
	-I$(QHAL_HOST_USB_SRC)	\
	-I$(GEOSW_ROOT)/condorsw/host/lib/qhal/include	\
	-D_REENTRANT -D__LITTLE_ENDIAN__ -D__FALCON__ -DQMM_PARTITION_ID=125 -DQMM_BOOT_PARTITION_ID=126

OBJECTS = $(patsubst $(QHAL_HOST_USB_SRC)/%.c, $(BUILDDIR)/%.o, $(QHAL_HOST_USB_FILES)) $(patsubst $(QHAL_TARGET_USB_SRC)/%.c, $(BUILDDIR)/%.o, $(QHAL_TARGET_USB_FILES)) 
	
all: dir $(LIBRARY)

dir:
	@mkdir -p $(BUILDDIR)

copyfiles:
	@cp $(SRC_FILES_TO_COPY) $(SRCDIR)
	@cp $(HEADER_FILES_TO_COPY) $(INCLUDEDIR)

#$(LIBRARY): $(eval OBJECTS = $(patsubst $(SRCDIR)/%.c, $(BUILDDIR)/%.o, $(wildcard $(SRCDIR)/*.c)))
$(LIBRARY): $(OBJECTS)
	$(AR) -rucs $@ $^

$(BUILDDIR)/%.o: $(QHAL_HOST_USB_SRC)/%.c
	$(CC) $(CFLAGS) -c $< -o $@
	
$(BUILDDIR)/%.o: $(QHAL_TARGET_USB_SRC)/%.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	-rm -rf $(LIBRARY) $(BUILDDIR)

.PHONY: clean all