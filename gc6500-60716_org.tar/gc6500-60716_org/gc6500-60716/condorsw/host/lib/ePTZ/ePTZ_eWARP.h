/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

/*******************************************************************************
* @(#) $Id: h.skel,v 1.17 2013/01/03 02:20:21 monsen Exp $
*******************************************************************************/
#ifndef EPTZ_EWARP_H
#define EPTZ_EWARP_H

//////////////////////////////////////////////////////////////////////////////////////////
//
// ePTZ Modes
//
/// \brief The following constants are defined for use with function gridgenerator().  When
///  gridgenerator is called, params[7] is set to one of these constants to set the
/// dewarp mode. Please refer to GEO Semiconductor's "ePTZ Modes" document for more information
//
#define EWARPMODE_EPTZ_OFF                        0
#define EWARPMODE_WM_ZCL                        110
#define EWARPMODE_WM_ZCLCylinder                111
#define EWARPMODE_WM_ZCLStretch                 112
#define EWARPMODE_WM_1PanelEPTZ                 120
#define EWARPMODE_WM_1PanelEPTZ_4pt             121
#define EWARPMODE_WM_4PanelEPTZ                 130
#define EWARPMODE_WM_4DepPanelEPTZ              131
#define EWARPMODE_WM_FullView_3PanelEPTZ        140
#define EWARPMODE_WM_ZCLView_3PanelEPTZ         141
#define EWARPMODE_WM_ZCLCylinderView_3PanelEPTZ 142
#define EWARPMODE_WM_PanoView_2PanelEPTZ        150
#define EWARPMODE_CM_2CircPanoViewPan           160
#define EWARPMODE_CM_360CircPanoView_2PanelEPTZ 170
#define EWARPMODE_CM_4PanelEPTZ                 180
#define EWARPMODE_CM_FullView_3PanelEPTZ        190
#define EWARPMODE_CM_FullViewTL_1PanelEPTZ      200
#define EWARPMODE_TM_2CircPanoViewPan           210
#define EWARPMODE_TM_1CircPanoViewPan           211
#define EWARPMODE_TM_360CircPanoView_1PanelNonPerspPan  212
#define EWARPMODE_TM_360CircPanoView_2PanelNonPerspPan  220
#define EWARPMODE_TM_360CircPanoView_2PanelPan          221
#define EWARPMODE_TM_360CircPanoView_4PanelPan          230
#define EWARPMODE_SWEEP_WM_1PANELEPTZ                   400
#define EWARPMODE_WM_Magnify                            401
#define EWARPMODE_WM_Flush                              402
#define EWARPMODE_WM_ROI_1PanelPTZ                      450
#define EWARPMODE_EPTZ_MAX                              500



#define EWARPMODE_MAX_NUM_OF_PARAMS               32

#define GEMAP_MAX_NUM_OF_COEEFS               2048

#define GEMAP_RETCODE_OK            0
#define GEMAP_RETCODE_ERR_NOMAP     -1
#define GEMAP_RETCODE_ERR_SOMETHING -1



typedef struct __gemap_struct{
    short InputFrameWidth;
    short InputFrameHeight;
    short DewarpedFrameWidth;   // this is the output from the dewarp engine.
    short DewarpedFrameHeight;  // this is the output from the dewarp engine.
    short MapN;
    short MapWidth;
    short MapHeight;
    int   MapNumOfCoeffs;
    int   MapMaxNumOfCoeffs;
    short *pMapCoeffs;
}gemap_struct;


typedef struct __eptz_handle_struct {
    int  state;     // 0 = unused, 1 = used
    int  maxMapWidth;
    int  maxMapHeight;
    void *posXM;
    void *posYM;
    void *posXMR;
    void *posYMR;
    float param[EWARPMODE_MAX_NUM_OF_PARAMS];
}eptz_handle_struct;



#ifdef __cplusplus
extern "C" {
#endif


//////////////////////////////////////////////////////////////////////////////////////////
//
// gridgenerator
//
/// \brief The purpose of function gridgenerator is to generate a grid type map for the
///        dewarp engine.  Please refer to GEO Semiconductor's "ePTZ Modes" document for
///        more information.
/// \param pHandle a private data structure which has been returned by function eptz_init()
/// \param inWidth  Width of input image in pixels
/// \param inHeight Height of input image in pixels
/// \param outWidth Width of dewarped image in pixels
/// \param outHeight Height of dewarped image in pixels
/// \MapN  MapN parameter
/// \param params a generic array of parameters.  Please refer to GEO Semiconductor's "ePTZ Modes"
///        document for more information.
/// \return On success, this function returns GEMAP_RETCODE_OK to indicate there's no error
/// and the variable pMap contains the output map.  Otherwise, an error code is returned
/// and the contents of pMap is undefined.
///
int eptz_generate_gridmap(eptz_handle_struct *pHandle, short inWidth, short inHeight,
        short outWidth, short outHeight,
        short MapN, const float *params, gemap_struct *pMap, char* path);


//////////////////////////////////////////////////////////////////////////////////////////
//
// eptz_init
//
/// \brief The purpose of function epgz_init is to initialize the ePTZ Map Generator module.
///   This function must be called and must be called before all other functions in this module.
/// \param maxMapWidth Maximum Map Width.
/// \param maxMapHeight Maximum Map Height.
/// \return On success, this function returns a pointer to structure eptz_handle_struct.  This
/// pointer is needed for subsequent calls to other functions in this module.
///

eptz_handle_struct *eptz_init(int mode, short maxMapWidth, short maxMapHeight);

//////////////////////////////////////////////////////////////////////////////////////////
//
// eptz_deinit
//
/// \brief The purpose of function epgz_deinit is to deinitialize the ePTZ Map Generator module.
/// It deallocates memory and deallocate resources.
/// \param pHandle handle to be deallocated
/// \return On success, this function returns GEMAP_RETCODE_OK to indicate there's no error.
///

int eptz_deinit(eptz_handle_struct *pHandle);


void mkmap_linear_map (
    int             dewarp_width,   // Width   (Dewarp-frame)
    int             dewarp_height,  // Height  (Dewarp-frame)
    int             src_width,      // Width   (Source-frame)
    int             src_height,     // Height  (Source-frame)
    float           Scale,          // Scaling-factor (1.0 => "Source" is strecthed to fill "Dewarp")
    int             MapN,
    gemap_struct   *pMap);


#ifdef __cplusplus
}
#endif


#endif  /* EPTZ_EWARP_H */
/*******************************************************************************
* vi: set shiftwidth=4 tabstop=8 softtabstop=4 expandtab nosmarttab:
*******************************************************************************/

