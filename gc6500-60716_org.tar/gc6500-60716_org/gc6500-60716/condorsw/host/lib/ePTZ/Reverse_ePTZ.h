/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

/*******************************************************************************
* @(#) $Id: Reverse_ePTZ.h 50214 2014-10-14 16:49:48Z nmasood $
*******************************************************************************/

#ifndef REVERSE_EPTZ_H
#define REVERSE_EPTZ_H

#ifdef __cplusplus
extern "C" {
#endif

int
reverse_eptz (
        const   int          src_width
    ,   const   int          src_height
    ,   const   int          dewarp_width
    ,   const   int          dewarp_height
    ,           int          MapN
    ,           float        target_x           // CENTER target  (in dewarp source units)
    ,           float        target_y           // CENTER target  (in dewarp source units)
    ,           float        target_dist        // Target HEIGHT (Window-height / 2) (in dewarp source units)
    ,           float        lens_fov           // Lens FOV in degrees
    ,           float        lens_radius        // Lens radius in pixels
    ,           float        lens_hshift        // Lens horizontal shift in pixels
    ,           float        lens_vshift        // Lens vertical shift in pixels
    ,           float        *hpan_out          // EPTZ "hpan", in degrees
    ,           float        *vpan_out          // EPTZ "vpan", in degrees
    ,           float        *hfov_out          // EPTZ "Horz Field-of_View", in degrees
);

#ifdef __cplusplus
}
#endif

#endif   /* REVERSE_EPTZ_H */

