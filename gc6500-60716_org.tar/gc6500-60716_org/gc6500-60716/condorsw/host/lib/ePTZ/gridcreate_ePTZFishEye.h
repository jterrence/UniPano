/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef GRIDCREATE_EPTZFISHEYE_H_INCLUDED
#define GRIDCREATE_EPTZFISHEYE_H_INCLUDED

#include "math.h"
#include "ePTZ_eWARP.h"

#define pi 3.1415926535f
#define fac 0.017453292f
#define hpi 1.570796327f



#define REAL float

typedef struct
{
	REAL ** m;
	int r;
	int c;
} MATRIX2D;

int gridcreate_ePTZFishEye(MATRIX2D * pX, MATRIX2D *pY, float * parval, int win, int hin, int wout, int hout, int mapN);


#define zsgn(X) (((X)>=0) ? 1 : -1)
#define zabs(X) (((X)>=0) ? (X) : -(X))
#define zdiv(X, Y) ((X)/(Y))
#define zcos(X) (cos((X)))
#define zsin(X) (sin((X)))
#define zatan2(X,Y) (atan2((X),(Y)))
#define zfloor(X) (floor((X)))
#define zceil(X) (ceil((X)))
#define zsqrt(X) (sqrt((X)))
#define zmin(X,Y) (((X)>(Y)) ? Y : X)
#define zmax(X,Y) (((X)<(Y)) ? Y : X)


#endif // GRIDCREATE_EPTZFISHEYE_H_INCLUDED
