/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include <stdio.h>
#include <libusb-1.0/libusb.h>
#include "hostio.h"

#define HOSTIO_USB_CMD  0x5f

#ifdef DEBUG
#define DPRINT(x...) printf(x)
#else
#define DPRINT(x...)
#endif

static struct libusb_context *dcontext;
static struct libusb_device_handle *devhandle = NULL;
static libusb_device *dev = NULL;

int HostIOInit(int vid, int pid)
{
    int r;

    if (libusb_init(&dcontext) < 0) {
        printf("failed to create usb context\n");
        return -1;
    }

    devhandle = libusb_open_device_with_vid_pid(dcontext, vid, pid);
    if (!devhandle) {
        printf("failed to detect USB device with VID %d, PID %d\n", vid, pid);
        goto devnotfound;
    }

    // get the device with the handle
    dev = libusb_get_device(devhandle);
    if (dev == NULL) {
        printf("failed to access USB device\n");
        goto devnotfound;
    }

    return 0;

devnotfound:
    libusb_exit(dcontext);

    return -1;
}

void HostIODeinit()
{
    libusb_close(devhandle);
    libusb_exit(dcontext);
}

int HostIOBase::HostIOSendToDevice(HostMessage *msg, int len)
{
    int r;
    r = libusb_control_transfer(devhandle,
                LIBUSB_ENDPOINT_OUT | LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_INTERFACE,
                HOSTIO_USB_CMD,
                0,
                0,
                (unsigned char*)msg,
                len,
                500);

    if (r != len) {
        DPRINT("Sending USB message failed \n");
        r  = -1;
    } else
        r = 0;

    return r;
}

int HostIOBase::HostIOReadFromDevice(HostMessage *msg, int len)
{
    int r;
    r = libusb_control_transfer(devhandle,
                LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_INTERFACE,
                HOSTIO_USB_CMD,
                msg->objId,         /* object id in wValue */
                msg->cmd,           /* command in wIndex */
                (unsigned char*)&msg->msg[0],
                len,
                500);
    if (r != len) {
        DPRINT("Reading USB message failed %d\n", r);
//        printf("######################  Reading USB message failed %d\n", r);
        r  = -1;
    } else
        r = 0;

    return r;
}

/*******************************************************************************
 * vi: set shiftwidth=4 tabstop=8 softtabstop=4 expandtab nosmarttab:
 ******************************************************************************/
