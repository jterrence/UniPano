/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef FIN_FOUT_H
#define FIN_FOUT_H

#include <stdlib.h>
#include <unistd.h>
#include "hostio.h"

#ifdef DEBUG
#define DPRINT(x...) printf(x)
#else
#define DPRINT(x...)
#endif

enum FileInCmds {
    FIN_SIZE = 1,
    FIN_DATA,
    FIN_COMPLETE
};

class FileIn : private HostIOBase
{
    public:

        FileIn(int id) : HostIOBase(id, OBJ_TYPE_FILE_IN)
        {
        }

        int SendFile(const char *fPath)
        {
            FILE *fd;
            char *fileBuf;
            int fSize;
            if (fPath == NULL)
                return -1;
            fd = fopen(fPath, "r");
            if (fd == NULL)
                return -1;
            fseek(fd, 0, SEEK_END);
            fSize = ftell(fd);
            rewind(fd);
            SendCmd(FIN_SIZE, &fSize, sizeof(int));
            int status = -1;
            do {
                status = GetLastCmdStatus();
                usleep(10000);
            } while (status != 0);
            fileBuf = (char *)malloc(fSize);
            fread (fileBuf, 1 , fSize, fd);
            SendCmd(FIN_DATA, fileBuf, fSize);
            do {
                status = GetLastCmdStatus();
                usleep(10000);
            } while (status != 0);
            free(fileBuf);
            fclose(fd);
            return 0;
        }
};

enum {
    FOUT_SIZE = 1,
    FOUT_DATA
};

class FileOut : private HostIOBase
{
    public:

        FileOut(int id) : HostIOBase(id, OBJ_TYPE_FILE_OUT)
        {
        }

        int RecvFile(const char *fPath)
        {
            void *fileBuf;
            int fileSize;
            FILE *fd;

            if (fPath == NULL)
                return -1;

            fd = fopen(fPath, "w");
            if (fd == NULL)
                return -1;
            fileSize = 0;
            while (fileSize == 0) {
                GetData(FOUT_SIZE, &fileSize, 4);
                usleep(10000);
            }
            DPRINT("recv file size %d\n", fileSize);
            fileBuf = malloc(fileSize);
            GetData(FOUT_DATA, fileBuf, fileSize);
            fwrite(fileBuf, 1, fileSize, fd);
            fclose(fd);
            free(fileBuf);
            return 0;
        }

    private:
};

#endif  /* FIN_FOUT_H */
/*******************************************************************************
 * vi: set shiftwidth=4 tabstop=8 softtabstop=4 expandtab nosmarttab:
 ******************************************************************************/
