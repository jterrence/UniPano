/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef FIN_FOUT_H
#define FIN_FOUT_H

#include <stdlib.h>
#include <unistd.h>
#include "hostio.h"

#ifdef DEBUG
#define DPRINT(x...) printf(x)
#else
#define DPRINT(x...)
#endif

enum FileInCmds {
    FIN_SIZE = 1,
    FIN_DATA,
    FIN_INFO,
    FIN_COMPLETE
};


struct FileInfo {
    int width;
    int height;
    int format;
    int ysize;
    int uvsize;
    int frmCount; // -1 is the end of the frame
};

class FileIn : private HostIOBase
{
    public:

        FileIn(int id) : HostIOBase(id, OBJ_TYPE_FILE_IN)
        {
        }

        void SetFileInfo(int width, int height, int format, int ysize, int uvsize, int frmCount)
        {
            fileInfo.width = width;
            fileInfo.height = height;
            fileInfo.format = format;
            fileInfo.ysize = ysize;
            fileInfo.uvsize = uvsize;
            fileInfo.frmCount = frmCount;
//			printf("FileInfo - format[%d], ysize[%d], uvsize[%d], width[%d], height[%d], frmCount[%d]\n",
//					fileInfo.format, fileInfo.ysize, fileInfo.uvsize, fileInfo.width, fileInfo.height, fileInfo.frmCount);

        }

        int SendFile(const char *fPath)
        {
            FILE *fd;
            char *fileBuf;
            int fSize;
            if (fPath == NULL)
                return -1;
            fd = fopen(fPath, "r");
            if (fd == NULL)
                return -1;
            fseek(fd, 0, SEEK_END);
            fSize = ftell(fd);
            rewind(fd);
            SendCmd(FIN_SIZE, &fSize, sizeof(int));
            int status = -1;
            do {
                status = GetLastCmdStatus();
                usleep(10000);
            } while (status != 0);
            fileBuf = (char *)malloc(fSize);
            fread (fileBuf, 1 , fSize, fd);
            SendCmd(FIN_DATA, fileBuf, fSize);
            free(fileBuf);
            fclose(fd);
            return 0;
        }

        // Send data in file, fPath, to fileencode.
        // The file size should be <bufeSize>.
        int SendFileEncode(const char *fPath, int bufeSize)
        {
            FILE *fd;
            int fSize;

            DPRINT("In SendFileInParts %s:%d \n", fPath, bufeSize);
            fd = fopen(fPath, "r");
            if (fd == NULL)
            {
            	printf("FIle [%s] doesn't exist!!\n", fPath);
                return -1;
            }
            fseek(fd, 0, SEEK_END);
            fSize = ftell(fd);
            rewind(fd);
            if (fSize != bufeSize)
            {
               	printf("[%s]: file size is not correct!!\n", fPath);
                fclose(fd);
                return -1;
	    }
            else //lets break large files into smaller pieces
			{
            	// Note: it is very important the sending order
            	//       1. FIN_SIZE
            	//       2. FIN_INFO
            	//       3. FIN_DATA
                int status;
                char *fileBuf = (char *)malloc(bufeSize);
                if(!fileBuf){
                    fclose(fd);
                    return -1;
                }
                // Send file size to fileencode
                status = SendCmd(FIN_SIZE, &fSize, sizeof(int));
//               printf("1 CMH -- SendCmd(FIN_SIZE, &fSize, sizeof(int))--- status=[%d]\n", status );
               if (status == -1)
                {
                 	printf("Function [%s]: SendCmd() failed!\n", __FUNCTION__);
                        fclose(fd);
                        free(fileBuf);
                 	return (-1);
                }

               // Send file info to fileencode
                status = SendCmd(FIN_INFO, &fileInfo, sizeof(fileInfo));
//                printf("2 CMH -- SendCmd(FIN_INFO, &fileInfo, sizeof(fileInfo))--- status=[%d]\n", status );
                if (status == -1)
                {
                 	printf("Function [%s]: SendCmd() failed!\n", __FUNCTION__);
                        fclose(fd);
                        free(fileBuf);
                 	return (-1);
                }

                // Send file data to fileencode
                fread (fileBuf, 1 , fSize, fd);
                status = SendCmd(FIN_DATA, fileBuf, fSize);
//                printf("3 CMH -- SendCmd(FIN_DATA, fileBuf, fSize)--- status=[%d]\n", status );
                if (status == -1)
                {
                 	printf("Function [%s]: SendCmd() failed!\n", __FUNCTION__);
                        fclose(fd);
                        free(fileBuf);
                 	return (-1);
                }

                free(fileBuf);
            }
            fclose(fd);
            DPRINT("DONE \n");
            return 0;
        }

    private:
        FileInfo fileInfo;
};

enum {
    FOUT_SIZE = 1,
    FOUT_DATA
};

class FileOut : private HostIOBase
{
    public:

        FileOut(int id) : HostIOBase(id, OBJ_TYPE_FILE_OUT)
        {
        }

        int RecvFile(const char *fPath)
        {
            void *fileBuf;
            int fileSize;
            FILE *fd;

            if (fPath == NULL)
                return -1;

            fd = fopen(fPath, "w");
            if (fd == NULL)
                return -1;
            fileSize = 0;
            while (fileSize == 0) {
                GetData(FOUT_SIZE, &fileSize, 4);
                usleep(10000);
            }
            DPRINT("recv file size %d\n", fileSize);
            fileBuf = malloc(fileSize);
            GetData(FOUT_DATA, fileBuf, fileSize);
            fwrite(fileBuf, 1, fileSize, fd);
            fclose(fd);
            free(fileBuf);
            return 0;
        }

    private:
};

#endif  /* FIN_FOUT_H */
/*******************************************************************************
 * vi: set shiftwidth=4 tabstop=8 softtabstop=4 expandtab nosmarttab:
 ******************************************************************************/
