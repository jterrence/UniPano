/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef HOSTIO_H
#define HOSTIO_H

#include<string.h>
#include <unistd.h>

enum HostIOObjType {
    OBJ_TYPE_NONE = 0,
    OBJ_TYPE_FILE_IN,
    OBJ_TYPE_FILE_OUT
};

#define MSG_BUF_SIZE 1024
struct HostMessage {
    int objId;
    HostIOObjType type;
    int cmd;
    int msgLen;
    char msg[MSG_BUF_SIZE];
};

#define MAX_LOOP	100

class HostIOBase
{
    public:
        HostIOBase(int id, HostIOObjType type)
        {
            objId = id;
            objType = type;
        }

        // return value
        //     -1: failed
        //	    0: successful
        int SendCmd(int cmd, void *msgBuf, int msgLen)
        {
        	int cnt;
            HostMessage msg;
            int txLen = 0;
            msg.objId = objId;
            msg.type = objType;
            msg.cmd = cmd;

            while (txLen < msgLen)
            {
                msg.msgLen = ((msgLen - txLen) > MSG_BUF_SIZE) ? MSG_BUF_SIZE : msgLen - txLen;
                memcpy(&msg.msg[0], (char*)msgBuf + txLen, msg.msgLen);
                int r = HostIOSendToDevice(&msg, sizeof(HostMessage) - MSG_BUF_SIZE + msg.msgLen);
                if (r == 0)
                {
                    txLen += msg.msgLen;
                    r = GetLastCmdStatus();
                    cnt = 0;
                    while (r != 0)
                    {
                        usleep(10000);
                        r = GetLastCmdStatus();
                        cnt++;
                        if ( cnt > MAX_LOOP )
                        {
                        	printf("xxxxxxxxxxxxxxx Function [%s]: loop more than %d times!!\n", __FUNCTION__, cnt);
                        	return(-1);
                        }
                    }
                }
                else
                {
                	printf("Function [%s]: HostIOSendToDevice() failed!\n", __FUNCTION__);
                	return (-1);
                }
            }
            return 0;
        }

        int GetLastCmdStatus()
        {
            HostMessage msg;
            msg.objId = objId;
            msg.type = objType;
            msg.cmd = 0;
            msg.msgLen = 4;
            memset((void*)msg.msg, 0, 4);
            int val = HostIOReadFromDevice(&msg, 4);
//            printf("Function [%s]; val=%d, msg.msg=%d\n", __FUNCTION__, val, *(int*)&msg.msg);
            if (val == -1) // HostIOReadFromDevice() failed!
            	return 1;

            return *(int*)&msg.msg;
         }

        int GetData(int cmd, void *buf, int len)
        {
            if (buf == NULL)
                return -1;
            HostMessage msg;
            int txLen = 0, r;
            msg.objId = objId;
            msg.type = objType;
            msg.cmd = cmd;
            while (txLen < len) {
                msg.msgLen = ((len - txLen) > MSG_BUF_SIZE) ?
                                MSG_BUF_SIZE : len - txLen;
                r = HostIOReadFromDevice(&msg, msg.msgLen);
                if (r == 0) {
                    memcpy((char*)buf+txLen, &msg.msg, msg.msgLen);
                    txLen += msg.msgLen;
                } else
                    break;
            }
            return r;
        }

    private:
        int HostIOSendToDevice(HostMessage *msg, int len);
        int HostIOReadFromDevice(HostMessage *msg, int len);
        int objId;
        HostIOObjType objType;
};

int HostIOInit(int vid, int pid);
void HostIODeinit();

#endif  /* HOSTIO_H */
/*******************************************************************************
 * vi: set shiftwidth=4 tabstop=8 softtabstop=4 expandtab nosmarttab:
 ******************************************************************************/
