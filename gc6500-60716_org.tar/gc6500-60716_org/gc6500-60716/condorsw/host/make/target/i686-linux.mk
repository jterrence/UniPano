#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================

# TARGET_TOOLCHAIN_PREFIX: target GNU tools prefix
TARGET_PREFIX=i686-pc-linux-gnu

# TARGET_ARCH: Architecture of the target
TARGET_ARCH=x86

# TARGET_OS: target Operating system
TARGET_OS=linux

# TARGET_PLATFORM: Platform or board
TARGET_PLATFORM=

# TARGET_LIB_DIR: target specific libraries directories
TARGET_LIB_DIR=

# TARGET_LIBS: Libraries required by the target
TARGET_LIBS=

# TARGET_INC_DIR: target specific include directories
TARGET_INC_DIR=

# TARGET_KERNEL_RELEASE: Kernel version running on the target
TARGET_KERNEL_RELEASE=

# TARGET_KERNEL_SOURCES: path to the target kernel sources, only necessary
# if you want to build linux drivers
TARGET_KERNEL_SOURCES=

# Build against installed kernel
#TARGET_KERNEL_SOURCES=/lib/modules/$(shell uname -r)/build
#TARGET_KERNEL_RELEASE=$(shell uname -r)

# TARGET_DEFINES: target specific defines
TARGET_DEFINES=

# TARGET_CFLAGS: target specific C flags (applied to .c files)
TARGET_CFLAGS=-O2

# TARGET_CPPFLAGS: target specific pre-processor flags (applied to all source files)
TARGET_CPPFLAGS=

# TARGET_CXXFLAGS: target specific C++ flags (applied to .cpp files)
TARGET_CXXFLAGS=-O2

# TARGET_ASFLAGS: target specific ASM flags (applied to .s and .S files)
TARGET_ASFLAGS=

# TARGET_LDFLAGS: target specific linker flags (applied to .o files)
TARGET_LDFLAGS=

# Dist repository
TARGET_DIST_VERSION=
TARGET_DIST_NAME=

# Toolchain to use
TARGET_TOOLCHAIN_VERSION=
TARGET_TOOLCHAIN_NAME=

# Include codec specific files
TARGET_CODEC=
ifdef TARGET_CODEC
include $(MAKE_TARGET_DIR)/codec-$(TARGET_CODEC).mk
endif

# TARGET_QHAL_TYPE: how qhal is going to be built, there are 3 type supported:
# 	lib: static library, all apps will be statically linked against QHAL.
# 	solib: shared library, all apps use a runtime library (that can be the one compiled) on the target.
# 	linuxdrv: linux driver, all apps are linked statically against a user space scheme layer and use a linux driver running on the target (built at the same time)
TARGET_QHAL_TYPE=

# TARGET_QHAL_HOST_DRIVER: qhal_host driver to use. 
# 	If TARGET_QHAL_TYPE=linuxdrv, by default set to linuxdrv
TARGET_QHAL_HOST=

# TARGET_QHAL_PLATFORM: name of the platform specific driver for that specific host.
TARGET_QHAL_PLATFORM=
