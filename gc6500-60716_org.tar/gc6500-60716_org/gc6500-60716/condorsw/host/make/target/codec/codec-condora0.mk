# This is not a target config but a sub-target config
# that contains info relevant to all target connected
# to Raptor coprocessor or Codec.
#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================

# $Id: codec-condora0.mk 57887 2016-04-29 10:50:27Z bsmith $

# HSI release to be used: HSI are the QCC registers description files for the codec/chip used
TARGET_HSI_VERSION=rLatest
TARGET_HSI_NAME=condora0

# Set qmm memory partitions
QMM_PARTITION_ID=125
QMM_BOOT_PARTITION_ID=126

# Codec release files to be used
TARGET_CODEC_VERSION=r27021

# Codec path: auto-detect a local version
TARGET_CODEC_DIR=$(strip \
			$(if $(shell if [ -d $(GEOSW_ROOT)/condorsw/codec/qmm-raptorb0 ]; then echo "found"; fi), \
				$(GEOSW_ROOT)/condorsw/codec/qmm-raptorb0, \
				$(if $(TARGET_CODEC_VERSION), \
					$(PUBLIC_RELEASES_DIR)/falconsw_codec/qmm-raptor-$(TARGET_CODEC_VERSION), \
					$(PUBLIC_RELEASES_DIR)/falconsw_codec/qmm-raptor \
				) \
			))

# Add path to HSI files
TARGET_INC_DIR		+=					\
			$(TARGET_HSI_DIR)/include/src		\
			$(TARGET_HSI_DIR)/include/rtl		\
			$(TARGET_CODEC_DIR)/include

# Codec REG configuration to be used
TARGET_CODEC_REGEXT	=					\
			raptor150MHz-flow_PMCDDR0_JEDEC555_1Gb_200MHz_ODT_OFF-avc_vs_jpeg_clk-usb_eye_pattern-codec

TARGET_DEFINES		+=						\
			__FALCON__					\
			QMM_PARTITION_ID=${QMM_PARTITION_ID}		\
			QMM_BOOT_PARTITION_ID=${QMM_BOOT_PARTITION_ID}
