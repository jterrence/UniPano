#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================

# $Id: i686-sub20-condora0.mk 57887 2016-04-29 10:50:27Z bsmith $

# TARGET_PREFIX: target name
TARGET_PREFIX=i686-pc-linux-gnu

# TARGET_ARCH: Architecture of the target
TARGET_ARCH=

# TARGET_PLATFORM: Platform or board
TARGET_PLATFORM=

# TARGET_OS: target Operating system
TARGET_OS=linux

# TARGET_KERNEL_RELEASE: Kernel version running on the target
TARGET_KERNEL_RELEASE=$(shell ls -1 $(TARGET_DIST_DIR)/$(TARGET_PREFIX)/lib/modules)

# TARGET_LIB_DIR: target specific libraries directories
TARGET_LIB_DIR	=									\
		$(TARGET_DIST_DIR)/$(TARGET_PREFIX)/lib					\
		$(TARGET_DIST_DIR)/$(TARGET_PREFIX)/usr/lib

# TARGET_LIBS: Libraries required by the target
TARGET_LIBS=

# TARGET_INC_DIR: target specific include directories
TARGET_INC_DIR	=									\
		$(TARGET_DIST_DIR)/$(TARGET_PREFIX)/$(TARGET_TEMPLATE)/include		\
		$(TARGET_DIST_DIR)/$(TARGET_PREFIX)/$(TARGET_TEMPLATE)/usr/include	\
		$(TARGET_DIST_DIR)/$(TARGET_PREFIX)/include \
		$(TARGET_DIST_DIR)/$(TARGET_PREFIX)/usr/include

# TARGET_DEFINES: target specific defines
TARGET_DEFINES	=			\
		_REENTRANT		\
		__LITTLE_ENDIAN__

# TARGET_CFLAGS: target specific C flags (applied to .c files)
TARGET_CFLAGS	=			\
		-Os			\
		-pthread

# TARGET_CPPFLAGS: target specific pre-processor flags (applied to all source files)
TARGET_CPPFLAGS=

# TARGET_CXXFLAGS: target specific C++ flags (applied to .cpp files)
TARGET_CXXFLAGS=

# TARGET_ASFLAGS: target specific ASM flags (applied to .s and .S files)
TARGET_ASFLAGS=

# TARGET_LDFLAGS: target specific linker flags (applied to .o files)
TARGET_LDFLAGS=

# Dist repository
TARGET_DIST_VERSION=
TARGET_DIST_NAME=

# Toolchain to use
TARGET_TOOLCHAIN_NAME=$(TARGET_PREFIX)
TARGET_TOOLCHAIN_VERSION=r2

# Include codec specific files
TARGET_CODEC=condora0
include $(MAKE_TARGET_DIR)/codec/codec-$(TARGET_CODEC).mk

# TARGET_QHAL_TYPE: how qhal is going to be built, there are 3 type supported:
# 	lib: static library, all apps will be statically linked against QHAL.
# 	solib: shared library, all apps use a runtime library (that can be the one compiled) on the target.
# 	linuxdrv: linux driver, all apps are linked statically against a user space scheme layer and use a linux driver running on the target (built at the same time)
TARGET_QHAL_TYPE=lib

# TARGET_QHAL_HOST_DRIVER: qhal_host driver to use. 
# 	If TARGET_QHAL_TYPE=linuxdrv, by default set to linuxdrv
TARGET_QHAL_HOST=spi

# TARGET_QHAL_PLATFORM: name of the platform specific driver for that specific host.
TARGET_QHAL_PLATFORM=condor-sub20

