# Config.mk
# Set all the variables of the make system depending on the
# build and running environment
#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================

# $Id: config.mk 57887 2016-04-29 10:50:27Z bsmith $

# WARNING: This is probably not the file where you want to
# define any make environment variable

# Those never change
REPOSITORY	= host
CHIP_SW_NAME	= condorsw
BASE_DIR	= $(GEOSW_ROOT)/$(CHIP_SW_NAME)/$(REPOSITORY)
MAKE_DIR	= $(BASE_DIR)/make

# Where the make system target description files are
MAKE_TARGET_DIR=$(MAKE_DIR)/target
# Where the make system build machine description files are
MAKE_BUILD_DIR=$(MAKE_DIR)/build
# Where the make system rules files are
MAKE_RULES_DIR=$(MAKE_DIR)/rules
# Where the make system scripts are
MAKE_SCRIPTS_DIR=$(MAKE_DIR)/scripts

# Define default rule
.DEFAULT_GOAL=all

# Now define the environment selected by the user
include $(MAKE_DIR)/defaults.mk
include $(MAKE_TARGET_DIR)/$(TARGET).mk
include $(MAKE_BUILD_DIR)/$(BUILD).mk
