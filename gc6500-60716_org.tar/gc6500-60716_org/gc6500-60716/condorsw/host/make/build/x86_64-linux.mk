#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================

#===============================================================================
# @(#) $Id: x86_64-linux.mk 57887 2016-04-29 10:50:27Z bsmith $
#===============================================================================

# BUILD_PREFIX: Build system GNU tools prefix
# We don't have separate rules for x86_64 bit machine and we use i686 rules only
BUILD_PREFIX=i686-pc-linux-gnu

# BUILD_LIB_DIR: Build machine specific libraries directories
BUILD_LIB_DIR=

# BUILD_LIBS: Libraries required by the build machine
BUILD_LIBS=

# BUILD_INC_DIR: Build machine specific include directories
BUILD_INC_DIR=

# BUILD_DEFINES: Build machine specific defines
BUILD_DEFINES=

# BUILD_CFLAGS: Build machine specific C flags (applied to .c files)
BUILD_CFLAGS	=			\
		-Wall			\
		-Wno-unused-function	\
		-Wno-unused-variable

# BUILD_CPPFLAGS: Build machine specific pre-processor flags (applied to all source files)
BUILD_CPPFLAGS	=			\
		-Wall			\
		-Wno-unused-function	\
		-Wno-unused-variable

# BUILD_CXXFLAGS: Build machine specific C++ flags (applied to .cpp files)
BUILD_CXXFLAGS	=			\
		-Wall			\
		-Wno-unused-function	\
		-Wno-unused-variable

# BUILD_ASFLAGS: Build machine specific ASM flags (applied to .s and .S files)
BUILD_ASFLAGS=

# BUILD_LDFLAGS: Build machine specific linker flags (applied to .o files)
BUILD_LDFLAGS=

# Utilities to be used for the build
BUILD_UTILS_VERSION=r15825
BUILD_UTILS_NAME=i686-linux

# TOOLS_DIR: Directory where the tools should be on the build machine
ifeq ($(strip $(TARGET_PREFIX)),$(BUILD_PREFIX))

TOOLS_DIR=/usr

else	# ifeq ($(strip $(TARGET_PREFIX)),$(BUILD_PREFIX))

ifeq ($(strip $(TARGET_PREFIX)),mips_fp_le)

TOOLS_DIR=/opt/hardhat/devkit/mips/fp_le
OLD_COMPILER=1

else	# ifeq ($(strip $(TARGET_PREFIX)),mips_fp_le)

ifeq ($(findstring qmm-,$(TARGET)),qmm-)

TOOLS_DIR=/tools/pub/qmm/$(strip $(TARGET_TOOLCHAIN_NAME))_$(strip $(TARGET_TOOLCHAIN_VERSION))/Linux.i686

else	# ifeq ($(findstring qmm-,$(TARGET)),qmm-)

TOOLS_DIR=$(strip \
		$(if $(shell if [ -d $(GEOSW_ROOT)/condorsw/dist/$(BUILD) ]; then echo "found"; fi), \
			$(GEOSW_ROOT)/condorsw/dist/$(BUILD), \
			$(if $(TARGET_TOOLCHAIN_VERSION), \
				/opt/mobilygen/tools/$(strip $(TARGET_TOOLCHAIN_NAME))-$(TARGET_TOOLCHAIN_VERSION)/i686-linux, \
				/opt/mobilygen/tools/$(strip $(TARGET_TOOLCHAIN_NAME))/i686-linux \
			)\
		))

endif	# ifeq ($(findstring qmm-,$(TARGET)),qmm-)

endif	# ifeq ($(strip $(TARGET_PREFIX)),mips_fp_le)

endif	# ifeq ($(strip $(TARGET_PREFIX)),$(BUILD_PREFIX))

ifndef TOOLS_DIR
$(error Unknown toolchain $(TARGET_TOOLCHAIN_NAME) with version $(TARGET_TOOLCHAIN_VERSION))
endif
