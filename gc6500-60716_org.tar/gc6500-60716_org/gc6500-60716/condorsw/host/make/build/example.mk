# example.mk
# Example of a build machine make configuration
# The build machine is the computer on which this make is run
#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================

# BUILD_PREFIX: Build system GNU tools prefix
BUILD_PREFIX=

# BUILD_LIB_DIR: Build machine specific libraries directories
BUILD_LIB_DIR=

# BUILD_LIBS: Libraries required by the build machine
BUILD_LIBS=

# BUILD_INC_DIR: Build machine specific include directories
BUILD_INC_DIR=

# BUILD_DEFINES: Build machine specific defines
BUILD_DEFINES=

# BUILD_CFLAGS: Build machine specific C flags (applied to .c files)
BUILD_CFLAGS=

# BUILD_CPPFLAGS: Build machine specific pre-processor flags (applied to all source files)
BUILD_CPPFLAGS=

# BUILD_CXXFLAGS: Build machine specific C++ flags (applied to .cpp files)
BUILD_CXXFLAGS=

# BUILD_ASFLAGS: Build machine specific ASM flags (applied to .s and .S files)
BUILD_ASFLAGS=

# BUILD_LDFLAGS: Build machine specific linker flags (applied to .o files)
BUILD_LDFLAGS=

# Utilities to be used for the build
BUILD_UTILS_VERSION=
BUILD_UTILS_NAME=$(BUILD)

# TOOLS_DIR: Directory where the tools should be on the build machine
TOOLS_DIR=

