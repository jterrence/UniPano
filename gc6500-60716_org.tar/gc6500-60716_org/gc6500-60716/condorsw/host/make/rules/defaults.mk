# rules.mk
# Make system rules
#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================

# $Id: defaults.mk 57887 2016-04-29 10:50:27Z bsmith $

# Parallel Make
P	?= 0
ifeq ($(P),0)
PMAKE=
else
PMAKE=-j 8
endif

# Enable Debug mode
# DEBUG=0 (or nothing): build production code (optimized and no debug symbols)
# DEBUG>0 Build non-optimized code with debug symbols
DEBUG	?= 0
ifeq ("$(DEBUG)","0")
DEBUG_CPPFLAGS=-DDEBUG_LEVEL=$(DEBUG)
DEBUG_CFLAGS=
DEBUG_CXXFLAGS=
DEBUG_ASFLAGS=
DEBUG_LDFLAGS=
else	# ifeq ("$(DEBUG)","0")
ifeq ("$(DEBUG)","1")
DEBUG_CPPFLAGS=-DDEBUG_LEVEL=$(DEBUG)
DEBUG_CFLAGS=-ggdb -O0
DEBUG_CXXFLAGS=-ggdb -O0
ifdef OLD_COMPILER
DEBUG_ASFLAGS=
else	# ifdef OLD_COMPILER
DEBUG_ASFLAGS=-ggdb
endif	# ifdef OLD_COMPILER
DEBUG_LDFLAGS=
else	# ifeq ("$(DEBUG)","1")
DEBUG_CPPFLAGS=-DDEBUG -DDEBUG_LEVEL=$(DEBUG)
DEBUG_CFLAGS=-ggdb
DEBUG_CXXFLAGS=-ggdb
ifdef OLD_COMPILER
DEBUG_ASFLAGS=
else	# ifdef OLD_COMPILER
DEBUG_ASFLAGS=-ggdb
endif	# ifdef OLD_COMPILER
DEBUG_LDFLAGS=
endif	# ifeq ("$(DEBUG)","1")
endif	# ifeq ("$(DEBUG)","0")

####################################
#
# Checks to make sure basic stuff
# is set properly
#
####################################
ifndef BASE_DIR
$(error You must define BASE_DIR: where config.mk is)
endif
export BASE_DIR

####################################
#
# Build environment
#
####################################

ifneq ("$(BUILD_PREFIX)", "$(TARGET_PREFIX)")
CROSS_COMPILE=$(TARGET_PREFIX)-
else
CROSS_COMPILE=
endif

ifndef TOOLS_DIR
$(error you must define TOOLS_DIR in your build machine config file)
endif

AR	= $(TOOLS_DIR)/bin/$(CROSS_COMPILE)ar    
AS	= $(TOOLS_DIR)/bin/$(CROSS_COMPILE)as
CC	= $(TOOLS_DIR)/bin/$(CROSS_COMPILE)gcc   
CXX	= $(TOOLS_DIR)/bin/$(CROSS_COMPILE)g++
LD	= $(TOOLS_DIR)/bin/$(CROSS_COMPILE)ld   
NM	= $(TOOLS_DIR)/bin/$(CROSS_COMPILE)nm
OBJCOPY	= $(TOOLS_DIR)/bin/$(CROSS_COMPILE)objcopy
OBJDUMP	= $(TOOLS_DIR)/bin/$(CROSS_COMPILE)objdump
RANLIB	= $(TOOLS_DIR)/bin/$(CROSS_COMPILE)ranlib


########################################
#
# Compilation rules and build variables
# Please do not touch
#
########################################
THIRDPARTY_INSTALLED_DIR	:= $(GEOSW_ROOT)/thirdparty/installed/i686-linux

# Compilation flags
CPPFLAGS	=							\
		$(BUILD_CPPFLAGS)					\
		$(TARGET_CPPFLAGS)					\
		$(MODULE_CPPFLAGS)					\
		$(DEBUG_CPPFLAGS)					\
		$(addprefix -D,$(MODULE_DEFINES))			\
		$(addprefix -D,$(BUILD_DEFINES))			\
		$(addprefix -D,$(TARGET_DEFINES))			\
		$(addprefix -I,$(MODULE_INCLUDE_PATH))			\
		$(addprefix -I,$(BUILD_INC_DIR))			\
		$(addprefix -I,$(TARGET_INC_DIR))			\
		-I$(THIRDPARTY_INSTALLED_DIR)/include/editline		\
		-I$(THIRDPARTY_INSTALLED_DIR)/include/libavcodec	\
		-I$(THIRDPARTY_INSTALLED_DIR)/include/libavfilter	\
		-I$(THIRDPARTY_INSTALLED_DIR)/include/libswresample	\
		-I$(THIRDPARTY_INSTALLED_DIR)/include/libswscale	\
		-I$(THIRDPARTY_INSTALLED_DIR)/include/libusb-1.0	\
		-I$(THIRDPARTY_INSTALLED_DIR)/include/			\
		-I$(INSTALL_INC_DIR)
ifdef TARGET_BUILD_STATIC
CPPFLAGS	+= -DBUILD_STATIC
endif
CFLAGS		=					\
		$(BUILD_CFLAGS)				\
		$(TARGET_CFLAGS)			\
		$(MODULE_CFLAGS)			\
		$(DEBUG_CFLAGS)
CXXFLAGS	=					\
		$(BUILD_CXXFLAGS)			\
		$(TARGET_CXXFLAGS)			\
		$(MODULE_CXXFLAGS)			\
		$(DEBUG_CXXFLAGS)
ASFLAGS		=					\
		$(BUILD_ASFLAGS)			\
		$(TARGET_ASFLAGS)			\
		$(MODULE_ASFLAGS)			\
		$(DEBUG_ASFLAGS)			\
		$(addprefix -I,$(MODULE_INCLUDE_PATH))	\
		$(addprefix -I,$(BUILD_INC_DIR))	\
		$(addprefix -I,$(TARGET_INC_DIR))	\
		-I$(INSTALL_INC_DIR)

LDFLAGS		=					\
		$(BUILD_LDFLAGS)			\
		$(TARGET_LDFLAGS)			\
		$(MODULE_LDFLAGS)			\
		$(DEBUG_LDFLAGS)			\
		-L$(INSTALL_LIB_DIR)			\
		$(addprefix -L,$(MODULE_LIB_DIR))	\
		$(addprefix -l,$(MODULE_LIBS))		\
		$(addprefix -L,$(BUILD_LIB_DIR))	\
		$(addprefix -l,$(BUILD_LIBS))		\
		$(addprefix -L,$(TARGET_LIB_DIR))	\
		$(addprefix -l,$(TARGET_LIBS))		\
		-L$(THIRDPARTY_INSTALLED_DIR)/lib	

# Various variables to reduce rules size
SRC_FILES=$(MODULE_SRC)
C_FILES=$(filter %.c,$(SRC_FILES)) $(BUILD_DIR)/version.c
CXX_FILES=$(filter %.cpp,$(SRC_FILES))
AS_FILES=$(filter %.s %.S,$(SRC_FILES))
COMPILE_FILES=$(C_FILES) $(CXX_FILES) $(AS_FILES)
OBJ_FILES=$(addprefix $(BUILD_DIR)/,$(addsuffix .o,$(basename $(notdir $(COMPILE_FILES)))))
DEP_FILES=$(addprefix $(BUILD_DIR)/,$(addsuffix .d,$(basename $(notdir $(COMPILE_FILES)))))

# We ask both GCC and find to look for the library
# GCC always return the libname if it can't find the library so we eliminate that using filter
LIB_DIRS=$(subst -L,,$(filter -L%, $(LDFLAGS)))
LIB_FILES	=	\
              $(foreach libfile, $(addprefix lib,$(addsuffix .a,$(subst -l,,$(filter -l%, $(LDFLAGS))))), \
                $(filter %/$(libfile),\
                    $(shell find $(LIB_DIRS) -maxdepth 1 \( \( -name .svn -prune \) -o \( -name $(libfile) -print \) \) 2> /dev/null ; \
                            $(CC) -print-file-name=$(libfile) ) \
                ))

# Run target in all subdirectories in MODULE_SUBDIRS
%/all:
	@echo "ENTER $(@D) (ALL $(TARGET))"
	$(MAKE) -C $(@D) $(if $(findstring $(@D), $(PARALLEL_MAKE_SUBDIRS)), $(PMAKE), ) all $(SUBDIRS_MAKEFLAGS)
	@echo "LEAVE $(@D)"

%/clean:
	@echo "ENTER $(@D) (CLEAN $(TARGET))"
	$(MAKE) -C $(@D) clean $(SUBDIRS_MAKEFLAGS)
	@echo "LEAVE $(@D)"

%/install:
	@echo "ENTER $(@D) (INSTALL $(TARGET))"
	$(MAKE) -C $(@D) install $(SUBDIRS_MAKEFLAGS)
	@echo "LEAVE $(@D)"

%/superclean:
	@echo "ENTER $(@D) (SUPERCLEAN)"
	$(MAKE) -C $(@D) superclean $(SUBDIRS_MAKEFLAGS)
	@echo "LEAVE $(@D)"

# If MODULE_TYPE is not defined, we assume nothing should be built
ifneq ($(strip $(MODULE_TYPE)),)

include $(MAKE_RULES_DIR)/$(MODULE_TYPE).mk

ifneq ($(findstring $(TARGET), $(MODULE_EXCLUDE_TARGETS)),)
# Make sure the target is not excluded
MODULE_SUPPORTED_TARGETS=none

else	# ifeq ($(findstring $(TARGET), $(MODULE_EXCLUDE_TARGETS)),)

ifeq ($(strip $(MODULE_SUPPORTED_TARGETS)),)
# If MODULE_SUPPORTED_TARGETS is empty target is supported
MODULE_SUPPORTED_TARGETS=$(TARGET)
endif	# ifeq ($(strip $(MODULE_SUPPORTED_TARGETS)),)

endif	# ifeq ($(findstring $(TARGET), $(MODULE_EXCLUDE_TARGETS)),)

else	# ifneq ($(strip $(MODULE_TYPE)),)

MODULE_SUPPORTED_TARGETS=none

endif	# ifneq ($(strip $(MODULE_TYPE)),)

# Default target:
ifeq ($(findstring $(TARGET),$(MODULE_SUPPORTED_TARGETS)),$(TARGET))

all: $(MODULE_SUBDIRS:%=%/all) $(TARGET_DEPS) $(MAINTARGET_BUILD) $(TARGET_BUILD) $(TARGET_INSTALL) $(MAINTARGET_INSTALL) $(TARGET_POST_INSTALL)

superclean: $(MODULE_SUBDIRS:%=%/superclean) $(MAINTARGET_SUPERCLEAN) $(TARGET_SUPERCLEAN)

clean: $(MODULE_SUBDIRS:%=%/clean) $(TARGET_CLEAN) $(MAINTARGET_CLEAN)

build: $(MODULE_SUBDIRS:%=%/all) $(TARGET_DEPS) $(MAINTARGET_BUILD) $(TARGET_BUILD)

install: $(MODULE_SUBDIRS:%=%/install) $(TARGET_INSTALL) $(MAINTARGET_INSTALL) $(TARGET_POST_INSTALL)

else	# ifeq ($(findstring $(TARGET),$(MODULE_SUPPORTED_TARGETS)),$(TARGET))

all: 

superclean: $(MODULE_SUBDIRS:%=%/superclean) $(MAINTARGET_SUPERCLEAN) $(TARGET_SUPERCLEAN) 

clean: $(MODULE_SUBDIRS:%=%/clean) $(TARGET_CLEAN) $(MAINTARGET_CLEAN)

build: 

install: 

endif	# ifeq ($(findstring $(TARGET),$(MODULE_SUPPORTED_TARGETS)),$(TARGET))

.PHONY: superclean clean build all install

# The more-than-a-one-liner code in version.c prevents the compiler from optimizing out the version string.
$(BUILD_DIR)/version.c::
	if [ ! -d $(BUILD_DIR) ]; then mkdir -p $(BUILD_DIR); fi
	DOLLAR_CHAR='$$' ; echo "const char* rcs_ident_version_c_func() { static  char __version__[] =\"$${DOLLAR_CHAR}Log: $(VERSION) $${DOLLAR_CHAR}\"; return __version__; }" > $@.tmp
	if [ -f $@ ]; then \
		cmp $@ $@.tmp &> /dev/null; \
		if [ $$? -ne 0 ]; then \
			rm $@; \
			mv $@.tmp $@; \
		fi \
	else \
		mv $@.tmp $@; \
	fi
	if [ -f $@.tmp ]; then \
		rm $@.tmp; \
	else \
		echo " VERSION $(VERSION)"; \
	fi

# The following lines (until the $(foreach $(eval $(call ...))))
# generate the rule to build .o from .c
# The dependency is in the rule to make sure the file is recompiled
# if one dependency has changed
define CC_RULE
$(2): $(1)
	@echo " CC $$(notdir $$<)"
	if [ ! -d $$(dir $$@) ]; then mkdir -p $$(dir $$@); fi
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $$< -o $$@

endef

$(eval $(foreach src_file, $(C_FILES), $(call CC_RULE,$(src_file),$(addprefix $(BUILD_DIR)/,$(patsubst %.c,%.o,$(notdir $(src_file)))))))

# The following lines (until the $(foreach $(eval $(call ...))))
# generate the rule to build .o from .cpp
# The dependency is in the rule to make sure the file is recompiled
# if one dependency has changed
define CXX_RULE
$(2): $(1)
	@echo " C++ $$(notdir $$<)"
	if [ ! -d $$(dir $$@) ]; then mkdir -p $$(dir $$@); fi
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $$< -o $$@

endef

$(eval $(foreach src_file, $(CXX_FILES), $(call CXX_RULE,$(src_file),$(addprefix $(BUILD_DIR)/,$(patsubst %.cpp,%.o,$(notdir $(src_file)))))))

# The following lines (until the $(foreach $(eval $(call ...))))
# generate the rule to build .o from .S
# The dependency is in the rule to make sure the file is recompiled
# if one dependency has changed
define CPPAS_RULE
$(2): $(1)
	@echo " CPP+ASM $$(notdir $$<)"
	if [ ! -d $$(dir $$@) ]; then mkdir -p $$(dir $$@); fi
	$(CC) $(CPPFLAGS) $(ASFLAGS) -c $$< -o $$@

endef

$(eval $(foreach src_file, $(filter %.S,$(AS_FILES)), $(call CPPAS_RULE,$(src_file),$(addprefix $(BUILD_DIR)/,$(patsubst %.S,%.o,$(notdir $(src_file)))))))

# The following lines (until the $(foreach $(eval $(call ...))))
# generate the rule to build .o from .s
# The dependency is in the rule to make sure the file is recompiled
# if one dependency has changed
define AS_RULE
$(2): $(1)
	@echo " ASM $$(notdir $$<)"
	if [ ! -d $$(dir $$@) ]; then mkdir -p $$(dir $$@); fi
	$(AS) $(ASFLAGS) $$< -o $$@

endef

$(eval $(foreach src_file, $(filter %.s,$(AS_FILES)), $(call AS_RULE,$(src_file),$(addprefix $(BUILD_DIR)/,$(patsubst %.s,%.o,$(notdir $(src_file)))))))

ifndef OLD_COMPILER
# The following lines (until the $(foreach $(eval $(call ...))))
# generate the dependencies files
define DEPS_RULE

$(2): $(1)
	if [ ! -d $$(dir $$@) ]; then mkdir -p $$(dir $$@); fi
	$(CC) -MT $$@ -MT $$(patsubst %.d,%.o,$$@) -MF $$@ -M $(CPPFLAGS) $(1)

endef

$(eval $(foreach src_file, $(filter-out %.s,$(COMPILE_FILES)), $(call DEPS_RULE,$(src_file),$(addprefix $(BUILD_DIR)/,$(addsuffix .d,$(basename $(notdir $(src_file))))))))

else	# ifndef OLD_COMPILER

# Only for GCC prior to v3
# The following lines (until the $(foreach $(eval $(call ...))))
# generate the dependencies files
define DEPS_RULE

$(2):  $(1)
	if [ ! -d $(dir $(2)) ]; then mkdir -p $(dir $(2)); fi
	rm -f $(2) ;
	echo -ne $(2) $(BUILD_DIR)/ > $(2)
	$(CC) -M -MG $(CPPFLAGS) $(1) | sed 's,\($*\)\.o[ :]*,\1.o: ,g' >> $(2)

endef

$(eval $(foreach src_file, $(filter-out %.s,$(COMPILE_FILES)), $(call DEPS_RULE,$(src_file),$(addprefix $(BUILD_DIR)/,$(addsuffix .d,$(basename $(notdir $(src_file))))))))

endif	# ifndef OLD_COMPILER

# The following lines (until the $(foreach $(eval $(call ...))))
# generate the dependencies files for .s files
define DEPS_RULE_ASM

$(2): $(1)
	if [ ! -d $$(dir $$@) ]; then mkdir -p $$(dir $$@); fi
	rm -f $$@
	echo -ne $(BUILD_DIR)/$$(notdir $$(patsubst %.s, %.o,$(1))): FORCE > $$@

endef

FORCE:

.PHONY: FORCE

$(eval $(foreach src_file, $(filter %.s,$(COMPILE_FILES)), $(call DEPS_RULE_ASM,$(src_file),$(addprefix $(BUILD_DIR)/,$(addsuffix .d,$(basename $(notdir $(src_file))))))))

# We do not have dependencies if the target is not supported
ifeq ($(findstring $(TARGET),$(MODULE_SUPPORTED_TARGETS)),$(TARGET))

# We ignore deps if there is a clean target (not required anyway since everything should be re-compiled
ifeq ($(findstring clean,$(MAKECMDGOALS)),)
ifneq ($(strip $(DEP_FILES)),)

-include $(DEP_FILES)

# Force TARGET_DEPS to be build first during the "Update Makefiles" phase
TARGET_DEPS: $(TARGET_DEPS)
.PHONY: TARGET_DEPS
-include TARGET_DEPS

endif   # ifneq ($(strip $(DEP_FILES)),)
endif   # ifeq ($(findstring clean,$(MAKECMDGOALS)),)

endif	# ifeq ($(findstring $(TARGET),$(MODULE_SUPPORTED_TARGETS)),$(TARGET))
