# solib.mk
# Make system rules for solib modules
#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================

#===============================================================================
# @(#) $Id: solib.mk 57887 2016-04-29 10:50:27Z bsmith $
#===============================================================================

ifndef MODULE_NAME
$(error You must set MODULE_NAME for MODULE_TYPE==solib)
endif	# ifndef MODULE_NAME


LIB_BUILT_LIB_SO_FILE		= $(addprefix $(BUILD_DIR)/lib, $(addsuffix .so,$(MODULE_NAME)))
LIB_BUILT_LIB_A_FILE		= $(addprefix $(BUILD_DIR)/lib, $(addsuffix .a,$(MODULE_NAME)))
LIB_INSTALLED_LIB_SO_ABI_FILE	= $(addprefix $(INSTALL_LIB_DIR)/, $(notdir $(addsuffix .$(MODULE_ABI_VERSION),$(LIB_BUILT_LIB_SO_FILE))))
LIB_INSTALLED_LIB_SO_FILE	= $(addprefix $(INSTALL_LIB_DIR)/, $(notdir                                    $(LIB_BUILT_LIB_SO_FILE)))

LIB_INSTALLED_FILES		= 										\
				$(addprefix $(INSTALL_INC_DIR)/,	$(notdir $(MODULE_PUBLIC_HEADERS)))	\
				$(addprefix $(INSTALL_CONFIG_DIR)/,	$(notdir $(MODULE_PUBLIC_CONFIGFILES)))	\
				$(LIB_INSTALLED_LIB_SO_FILE)	

ifdef MODULE_ABI_VERSION
LIB_INSTALLED_FILES		+=										\
				$(LIB_INSTALLED_LIB_SO_ABI_FILE)
$(LIB_BUILT_LIB_SO_FILE):	LDFLAGS	+= -Wl,-soname,$(notdir $@.$(MODULE_ABI_VERSION))
endif	# MODULE_ABI_VERSION
$(LIB_BUILT_LIB_SO_FILE): $(OBJ_FILES)
	@echo " SHARED LIB $(MODULE_NAME)";
	if [ ! -d $(dir $@) ]; then mkdir -p $(dir $@); fi
	$(CC) -shared -o $@ $(OBJ_FILES) $(LDFLAGS)

solib: $(LIB_BUILT_LIB_SO_FILE)

solib-superclean: clean
	@echo " SUPERCLEAN $(MODULE_NAME)"
	rm -f $(LIB_INSTALLED_FILES)
solib-clean:
	@echo " CLEAN $(MODULE_NAME)"
	rm -rf $(BUILD_DIR)

# $1: DST_DIR
# $2: SRC_FILE_WITH_PATH
# Usage example:
#      $(foreach a_file, $(LIST_OF_FILES_EVEN_WITH_DIR_PATH),     $(eval $(call LIB_INSTALL_A_FILE,    $(A_INSTALL_DIR_NAME),$(a_file))))
define	LIB_INSTALL_A_FILE
$(1)/$$(notdir $(2)): $(2)
	@echo " INSTALL $$(<) to $$(@)"
	mkdir -p $$(@D)
	cp $$(<) $$(@)
endef	# LIB_INSTALL_HEADER

solib-install: $(LIB_INSTALLED_FILES)
ifdef MODULE_ABI_VERSION
$(LIB_INSTALLED_LIB_SO_ABI_FILE):	$(LIB_BUILT_LIB_SO_FILE)
	@echo " INSTALL $(<) to $(@)"
	mkdir -p $(@D)
	cp $(<) $(@)
$(LIB_INSTALLED_LIB_SO_FILE):
	ln -sf $(addsuffix .$(MODULE_ABI_VERSION), $(notdir $(@))) $(@)
else
$(foreach a_file, $(LIB_BUILT_LIB_SO_FILE),      $(eval $(call LIB_INSTALL_A_FILE,    $(INSTALL_LIB_DIR),$(a_file))))
endif
$(foreach a_file, $(MODULE_PUBLIC_HEADERS),     $(eval $(call LIB_INSTALL_A_FILE,    $(INSTALL_INC_DIR),$(a_file))))
$(foreach a_file, $(MODULE_PUBLIC_CONFIGFILES), $(eval $(call LIB_INSTALL_A_FILE, $(INSTALL_CONFIG_DIR),$(a_file))))

# This target is specific to solib which needs to be installed on the target
# If you want to be able to use this target and the board has a shared drive
# with the build machine define TARGET_SOLIB_DIR
ifdef TARGET_SOLIB_DIR
ifdef MODULE_ABI_VERSION
# FIXME: Very strange to have 'install' as a prereq of a target.
$(LIB_INSTALLED_LIB_SO_ABI_FILE): install
release: $(LIB_INSTALLED_LIB_SO_ABI_FILE)
else
# FIXME: Very strange to have 'install' as a prereq of a target.
$(LIB_INSTALLED_LIB_SO_FILE): install
release: $(LIB_INSTALLED_LIB_SO_FILE)
endif	# ifdef MODULE_ABI_VERSION
	@echo " RELEASE $(notdir $<) VERSION $(VERSION)"
	cp $< $(addsuffix .$(VERSION),$(addprefix $(TARGET_SOLIB_DIR)/,$(notdir $<)))
	if [ -h $(addprefix $(TARGET_SOLIB_DIR)/,$(notdir $<)) ]; then rm $(addprefix $(TARGET_SOLIB_DIR)/,$(notdir $<)); fi
	ln -s $(addsuffix .$(VERSION),$(notdir $<)) $(addprefix $(TARGET_SOLIB_DIR)/,$(notdir $<))
endif	# ifdef TARGET_SOLIB_DIR

ifdef TARGET_BUILD_STATIC
# If this is a static build, we use lib.mk rules instead

include $(MAKE_RULES_DIR)/lib.mk

else	# ifdef TARGET_BUILD_STATIC

MAINTARGET_SUPERCLEAN=solib-superclean
MAINTARGET_CLEAN=solib-clean
MAINTARGET_BUILD=solib
MAINTARGET_INSTALL=solib-install

endif	# ifdef TARGET_BUILD_STATIC

.PHONY: solib
.PHONY: solib-clean
.PHONY: solib-install
.PHONY: solib-superclean
