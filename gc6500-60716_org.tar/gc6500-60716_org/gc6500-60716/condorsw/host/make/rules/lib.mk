# lib.mk
# Make system rules for lib modules
#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================

#===============================================================================
# @(#) $Id: lib.mk 57887 2016-04-29 10:50:27Z bsmith $
#===============================================================================

ifndef MODULE_NAME
$(error You must set MODULE_NAME for MODULE_TYPE==lib)
endif	# ifndef MODULE_NAME

LIB_BUILT_LIB_FILE		= $(addprefix $(BUILD_DIR)/lib,$(addsuffix .a,$(MODULE_NAME)))
LIB_INSTALLED_FILES		=										\
				$(addprefix $(INSTALL_INC_DIR)/,   	$(notdir $(MODULE_PUBLIC_HEADERS)))	\
				$(addprefix $(INSTALL_CONFIG_DIR)/,	$(notdir $(MODULE_PUBLIC_CONFIGFILES)))	\
				$(addprefix $(INSTALL_LIB_DIR)/,   	$(notdir $(LIB_BUILT_LIB_FILE)))

$(LIB_BUILT_LIB_FILE): $(OBJ_FILES)
	@echo " LIB $(MODULE_NAME)";
	if [ ! -d $(dir $@) ]; then mkdir -p $(dir $@); fi
	$(AR) -rucs $@ $(OBJ_FILES)

lib: $(LIB_BUILT_LIB_FILE)

lib-superclean: clean
	@echo " SUPERCLEAN $(MODULE_NAME)"
	rm -f $(LIB_INSTALLED_FILES)

lib-clean:
	@echo " CLEAN $(MODULE_NAME)"
	rm -rf $(BUILD_DIR)

# $1: DST_DIR
# $2: SRC_FILE_WITH_PATH
# Usage example:
#      $(foreach a_file, $(LIST_OF_FILES_EVEN_WITH_DIR_PATH),     $(eval $(call LIB_INSTALL_A_FILE,    $(A_INSTALL_DIR_NAME),$(a_file))))
define	LIB_INSTALL_A_FILE
$(1)/$$(notdir $(2)): $(2)
	@echo " INSTALL $$(<) to $$(@)"
	mkdir -p $$(@D)
	cp $$(<) $$(@)
endef	# LIB_INSTALL_HEADER

lib-install:	$(LIB_INSTALLED_FILES)
$(foreach a_file, $(MODULE_PUBLIC_HEADERS),     $(eval $(call LIB_INSTALL_A_FILE,    $(INSTALL_INC_DIR),$(a_file))))
$(foreach a_file, $(MODULE_PUBLIC_CONFIGFILES), $(eval $(call LIB_INSTALL_A_FILE, $(INSTALL_CONFIG_DIR),$(a_file))))
$(foreach a_file, $(LIB_BUILT_LIB_FILE),        $(eval $(call LIB_INSTALL_A_FILE,    $(INSTALL_LIB_DIR),$(a_file))))

MAINTARGET_SUPERCLEAN=lib-superclean
MAINTARGET_CLEAN=lib-clean
MAINTARGET_BUILD=lib
MAINTARGET_INSTALL=lib-install

.PHONY: lib
.PHONY: lib-clean
.PHONY: lib-install
.PHONY: lib-superclean
