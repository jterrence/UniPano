# include.mk
# Make system rules for include directory
#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================

#===============================================================================
# @(#) $Id: include.mk 57887 2016-04-29 10:50:27Z bsmith $
#===============================================================================
INC_INSTALLED_FILES	=									\
			$(addprefix $(INSTALL_INC_DIR)/, 	$(MODULE_PUBLIC_HEADERS))	\
			$(addprefix $(INSTALL_CONFIG_DIR)/,   	$(MODULE_PUBLIC_CONFIGFILES))	

# $1: DST_DIR
include-superclean:
	@echo " SUPERCLEAN $(MODULE_NAME)"
	rm -f $(INC_INSTALLED_FILES)

include-clean:
	@echo " CLEAN $(MODULE_NAME)"
	rm -rf $(BUILD_DIR)

# $1: DST_DIR
# $2: SRC_FILE_WITH_PATH
# Usage example:
#      $(foreach a_file, $(LIST_OF_FILES_EVEN_WITH_DIR_PATH),     $(eval $(call INC_INSTALL_A_FILE,    $(A_INSTALL_DIR_NAME),$(a_file))))
define	INC_INSTALL_A_FILE
$(1)/$(2): $(2)
	@echo " INSTALL $$(<) to $$(@)"
	mkdir -p $$(@D)
	cp $$(<) $$(@)
endef	# EXEC_INSTALL_HEADER

include-install:	$(INC_INSTALLED_FILES)
$(foreach a_file, $(MODULE_PUBLIC_HEADERS),     $(eval $(call INC_INSTALL_A_FILE,    $(INSTALL_INC_DIR),$(a_file))))
$(foreach a_file, $(MODULE_PUBLIC_CONFIGFILES), $(eval $(call INC_INSTALL_A_FILE, $(INSTALL_CONFIG_DIR),$(a_file))))

MAINTARGET_SUPERCLEAN=include-superclean
MAINTARGET_INSTALL=include-install
MAINTARGET_CLEAN=include-clean

.PHONY: include-clean
.PHONY: include-install
.PHONY: include-superclean
