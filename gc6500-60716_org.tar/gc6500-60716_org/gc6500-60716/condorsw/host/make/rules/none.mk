# none.mk
#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================

#===============================================================================
# @(#) $Id: none.mk 57887 2016-04-29 10:50:27Z bsmith $
#===============================================================================

none-clean:
	@echo " CLEAN $(MODULE_NAME)"
	rm -rf $(BUILD_DIR)

MAINTARGET_SUPERCLEAN=none-clean
MAINTARGET_CLEAN=none-clean

.PHONY: none-clean
