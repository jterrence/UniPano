# exec.mk
# Make system rules for exec modules
#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================

#===============================================================================
# @(#) $Id: exec.mk 57887 2016-04-29 10:50:27Z bsmith $
#===============================================================================

ifndef MODULE_NAME
$(error You must set MODULE_NAME for MODULE_TYPE==exec)
endif	# ifndef MODULE_NAME

# Use $(CXX) to link if there are .cpp or .cc source files.
ifeq "$(filter %.cpp %.cc,$(MODULE_SRC))" ""
MODULE_LINK	= $(CC)
else
MODULE_LINK	= $(CXX)
endif

ifdef TARGET_BUILD_STATIC
LDFLAGS	+= -static
endif

$(BUILD_DIR)/$(MODULE_NAME): $(OBJ_FILES) $(LIB_FILES)
	@echo " ELF $(notdir $@)";
	if [ ! -d $(dir $@) ]; then mkdir -p $(dir $@); fi
	$(MODULE_LINK) -o $@ -Wl,--start-group $(OBJ_FILES) $(LDFLAGS) -Wl,--end-group

executable: $(addprefix $(BUILD_DIR)/,$(MODULE_NAME))

EXECUTABLE_INSTALL_FILES	=										\
				$(addprefix $(INSTALL_INC_DIR)/,   	$(notdir $(MODULE_PUBLIC_HEADERS)))	\
				$(addprefix $(INSTALL_CONFIG_DIR)/,	$(notdir $(MODULE_PUBLIC_CONFIGFILES)))	\
				$(addprefix $(INSTALL_BIN_DIR)/,   	$(MODULE_NAME))

executable-superclean: clean
	@echo " SUPERCLEAN $(MODULE_NAME)"
	rm -f $(INSTALL_BIN_DIR)/$(MODULE_NAME)
	rm -f $(EXECUTABLE_INSTALL_FILES)

executable-clean:
	@echo " CLEAN $(MODULE_NAME)"
	rm -rf $(BUILD_DIR)

# $1: DST_DIR
# $2: SRC_FILE_WITH_PATH
# Usage example:
#      $(foreach a_file, $(LIST_OF_FILES_EVEN_WITH_DIR_PATH),     $(eval $(call EXEC_INSTALL_A_FILE,    $(A_INSTALL_DIR_NAME),$(a_file))))
define	EXEC_INSTALL_A_FILE
$(1)/$$(notdir $(2)): $(2)
	@echo " INSTALL $$(<) to $$(@)"
	mkdir -p $$(@D)
	cp $$(<) $$(@)
endef	# EXEC_INSTALL_HEADER

executable-install: $(EXECUTABLE_INSTALL_FILES)
$(foreach a_file, $(MODULE_PUBLIC_HEADERS),     $(eval $(call EXEC_INSTALL_A_FILE,    $(INSTALL_INC_DIR),$(a_file))))
$(foreach a_file, $(MODULE_PUBLIC_CONFIGFILES), $(eval $(call EXEC_INSTALL_A_FILE, $(INSTALL_CONFIG_DIR),$(a_file))))
$(foreach a_file, $(BUILD_DIR)/$(MODULE_NAME),  $(eval $(call EXEC_INSTALL_A_FILE,    $(INSTALL_BIN_DIR),$(a_file))))

MAINTARGET_SUPERCLEAN=executable-superclean
MAINTARGET_CLEAN=executable-clean
MAINTARGET_BUILD=executable
MAINTARGET_INSTALL=executable-install

.PHONY: executable
.PHONY: executable-clean
.PHONY: executable-install
.PHONY: executable-superclean
