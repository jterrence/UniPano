# defaults.mk
# Default settings of the make system
#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================

# $Id: defaults.mk 57887 2016-04-29 10:50:27Z bsmith $

# If not specified we assume the following
BUILD	?= $(shell uname -m)-$(shell echo $(shell uname -s) | awk '{print tolower($$0);}')

ifneq ($(shell if [ -f $(MAKE_BUILD_DIR)/$(BUILD).mk ]; then echo "found"; fi),found)
$(error Unsupported build machine $(BUILD))
endif	# ifneq ($(shell if [ -f $(MAKE_BUILD_DIR)/$(BUILD).mk ]; then echo "found"; fi),found)

TARGET	?= $(BUILD)

ifneq ($(shell if [ -f $(MAKE_TARGET_DIR)/$(TARGET).mk ]; then echo "found"; fi),found)
$(error Unsupported target $(TARGET))
endif	# ifneq ($(shell if [ -f $(MAKE_TARGET_DIR)/$(TARGET).mk ]; then echo "found"; fi),found)

# Make system rules
RULES_MK=$(MAKE_RULES_DIR)/defaults.mk

# Version of the software
ifeq ($(filter clean%,$(MAKECMDGOALS)),)
VERSION	:= $(CHIP_SW_NAME)r$(shell svnversion $(BASE_DIR) | sed -e 's,.*r,, ; s,[^a-zA-Z]*[a-zA-Z][a-zA-Z]*$$,0, ; s,[0-9]*:[0-9]*,0,' )
else
VERSION	:= 0
endif

# Directory were files will be built
BUILD_DIR=./$(TARGET).build
INSTALL_DIR=$(BASE_DIR)/$(TARGET)
CONFIG_DIR=$(BASE_DIR)/config

# Directories where files will be installed
INSTALL_BIN_DIR		= $(INSTALL_DIR)/bin
INSTALL_INC_DIR		= $(INSTALL_DIR)/include
INSTALL_LIB_DIR		= $(INSTALL_DIR)/lib
INSTALL_CONFIG_DIR	= $(INSTALL_DIR)/etc

# Check for someone having checked out utils or qmmkernel
# If not uses the latest release
RELEASES_DIR=/tools/pub/qmm
PUBLIC_RELEASES_DIR=/opt/mobilygen

# Utils repository: assume BUILD_UTILS_NAME and BUILD_UTILS_VERSION (optional) are defined in build.mk
BUILD_UTILS_DIR=$(strip \
		$(if $(shell if [ -d $(GEOSW_ROOT)/$(CHIP_SW_NAME)/utils/$(BUILD_UTILS_NAME) ]; then echo "found"; fi), \
			$(GEOSW_ROOT)/condorsw/utils/$(BUILD_UTILS_NAME), \
			$(if $(BUILD_UTILS_VERSION), \
				$(PUBLIC_RELEASES_DIR)/utils/$(BUILD_UTILS_NAME)-$(BUILD_UTILS_VERSION), \
				$(PUBLIC_RELEASES_DIR)/utils/$(BUILD_UTILS_NAME) \
			) \
		))

# HSI repository: assume TARGET_HSI_NAME and TARGET_HSI_VERSION (optional) are defined in target.mk
TARGET_HSI_DIR=$(strip \
		$(if $(TARGET_HSI_VERSION), \
			$(RELEASES_DIR)/hsi/$(TARGET_HSI_NAME)-$(TARGET_HSI_VERSION), \
			$(RELEASES_DIR)/hsi/$(TARGET_HSI_NAME) \
		))

# Dist repository: assume TARGET_DIST_NAME and TARGET_DIST_VERSION (optional) are defined in target.mk
TARGET_DIST_DIR=$(strip \
		$(if $(shell if [ -d $(GEOSW_ROOT)/condorsw/dist/$(TARGET_DIST_NAME) ]; then echo "found"; fi), \
			$(GEOSW_ROOT)/$(CHIP_SW_NAME)/dist/$(TARGET_DIST_NAME)/$(BUILD), \
			$(if $(TARGET_DIST_VERSION), \
				$(PUBLIC_RELEASES_DIR)/distributions/$(TARGET_DIST_NAME)-$(TARGET_DIST_VERSION)/$(BUILD), \
				$(PUBLIC_RELEASES_DIR)/distributions/$(TARGET_DIST_NAME)/$(BUILD) \
			) \
		))

