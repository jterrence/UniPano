/******************************************************************************* 
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h> /* memset, memcmp */
#include <unistd.h> /* sleep */
#include <assert.h> /* assert */
#include <string.h>
#include <getopt.h>
#include <libusb-1.0/libusb.h>
#include <sys/stat.h>
#include "qmed.h"
#include "qmedext.h"
#include "qbox.h"
#include "usb_iap.h"

static struct libusb_device_handle *video_hdl = NULL;
static struct libusb_context       *video_ctx = NULL;

#define R2_VID 0x29fe
#define R2_PID 0x4d53

#define USB_DIR_OUT         0       /* to device */
#define USB_DIR_IN          0x80        /* to host */

#define IAP2_BULK_IN_EP 0x02
#define IAP2_BULK_OUT_EP 0x01

#define EAP_BULK_IN_EP 0x03
#define EAP_BULK_OUT_EP 0x04

#define EAP_BULK_OUT_BUF_SIZE 1024*4

//iAP2 class specific definations
enum interface_num_t{
    IPCAM_IAP2_INTERFACE_NUM = 0,
    IPCAM_EAP_INTERFACE_NUM = 1,                                                                                                       
    MAX_NUM_INTRFC
};

void close_device()                                                                                                                    
{
    if(video_hdl)libusb_close(video_hdl);
    if(video_ctx)libusb_exit(video_ctx);
}

int cam360_firmware_upgrade(ios_img_upgrade_type_t image_type, FILE *image_fin, uint32_t size)
{
    int max_data_len = EAP_BULK_OUT_BUF_SIZE-sizeof(ios_img_upgrade_hdr_t);
    cmd_rsp_hdr_t *rhdr;
    int  transferred_len;
    int  tx_len; 
    int ret = 0;
    char *img_data = NULL;

    img_data = (char *)malloc(EAP_BULK_OUT_BUF_SIZE);
    if(!img_data){
        printf("malloc failed\n");
        return -1;
    }
    /* GC6500 IOS ACC Mode SNOR Firmware Upgrade state machine:
    *   >All the below mentioned steps will happen over BULK OUT EA EP and ios_img_upgrade_hdr_t structure is used as a message header. 
    *   1. send a FW_UPGRADE_START command to camera with the rom image size.
    *   2. Each and every chunk of data(Max 4KB) has to be send with a command FW_UPGRAD_IMAGE.
    *   3. Last chunk will be send with a command FW_UPGRADE_COMPLETE.
    *   > All the below mentioned steps will happen over BULK IN EA EP and upgrade_rsp_t structure is used as a message header. 
    *   4. GC6500 will send upgrade response using BULK IN EP,
    *   response could be any of the below mentioned status
    *   typedef enum {
    *    UPGRADE_SUCCESS = 0x1, 
    *    UPGRADE_IN_PROGRESS, 
    *    UPGRADE_ERROR, //firmware upgrade failed
    *    UPGRADE_UNDEFINED
    *   }upgrade_status_t;
    *   5. If GC6500 sends UPGRADE_SUCCESS it means firmware upgrade is successful.
    *   6. reset/power-cycle GC6500 such that it boots up with new image from snor.
    */

    /* Send a firmware upgrade command with rom image size
     */
    ios_img_upgrade_hdr_t img_hdr;
    img_hdr.hdr.id = IOS_ACC_UPGRADE_COMMANDS;
    img_hdr.hdr.cmd_len = sizeof(ios_img_upgrade_hdr_t);
	img_hdr.type = image_type;
    img_hdr.cmd = FW_UPGRADE_START;
    img_hdr.image_size = size;

    //send image info to fw
    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)&img_hdr,
                             sizeof(ios_img_upgrade_hdr_t),
                             &transferred_len,
                             2000
                            );    
    if(ret){
        printf("ERR: libusb_bulk_transfer out img_hdr send failed %d\n",ret);
        free(img_data);
        return -1;
    }

    /*
     * Currently Bulk USB OUT EAP EP has 4KB dma buffer allocated.
     * So we have to send the command 
     * */ 
    for(tx_len=0; tx_len<size;){
        ios_img_upgrade_hdr_t hdr;
        hdr.hdr.id = IOS_ACC_UPGRADE_COMMANDS;
        hdr.cmd = FW_UPGRADE_IMAGE;
        hdr.type = image_type;     

        int read_size = ((size-tx_len) > (max_data_len)) 
                        ? (max_data_len) : (size-tx_len);
        //printf("read_size %d\n",read_size);
        hdr.hdr.cmd_len = read_size+sizeof(ios_img_upgrade_hdr_t);

        if((size-tx_len) <= max_data_len){ //last
            //printf("(stfile.st_size-tx_len) %ld\n",stfile.st_size-tx_len);

            hdr.cmd = FW_UPGRADE_COMPLETE;
        }

        hdr.image_size = read_size;
        memcpy(img_data, &hdr, sizeof(ios_img_upgrade_hdr_t));

        fread((void*)(img_data+sizeof(ios_img_upgrade_hdr_t)), 
                     read_size, 1, image_fin);
        //send image info to fw
        ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)img_data,
                             (read_size+sizeof(ios_img_upgrade_hdr_t)),
                             &transferred_len,
                             2000);    
        if(ret){
            printf("ERR: libusb_bulk_transfer out img_data send failed %d\n",ret);
            free(img_data);
            return -1;
        }

        tx_len += read_size;
    }

    printf("Writing image in flash...\n");
    //wait for response
    while(1){
        upgrade_rsp_t *rsp;
        char rsp_data[100];
        //check for status
        ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_IN + EAP_BULK_IN_EP),
                             (unsigned char *)rsp_data,
                             100,
                             &transferred_len,
                             120000);
        if(ret){
            printf("ERR: libusb_bulk_transfer IN rsp receive failed %d\n",ret);
            free(img_data);
            return -1;
            break;
        }

        //check if its a command response
        rhdr = (cmd_rsp_hdr_t *)rsp_data;
        if (rhdr->packet_type != CMDR_TYPE){
            printf("its not a command response\n");
            return 1;
        }
        rsp = (upgrade_rsp_t *)(rsp_data + sizeof(cmd_rsp_hdr_t));
        if(rsp->response == IOS_ACC_UPGRADE_RESPONSE){
            if(rsp->status == UPGRADE_IN_PROGRESS)
                printf("INFO: Upgrade in progress...\n");
            else if(rsp->status == UPGRADE_ERROR){
                printf("ERROR: Upgrade failed\n");
                break;
            }else if(rsp->status == UPGRADE_SUCCESS){
                printf("INFO: Firmware Upgrade is Complete :)\n");
                printf("Please reset the camera\n");
                break;
            }
        }
    }
    free(img_data);
    return ret;
}

void usage(void){
    printf("cam_upgrade usage:\n");
    printf("cam_upgrade --rom <rom-image>               : upgrade camera rom image created by mxpatchrom\n");
    printf("cam_upgrade --fw <fw-image>                 : upgrade camera firmware image\n");
    printf("cam_upgrade --bootloader <bootloader-image> : upgrade camera bootloader\n");
}

static int isfileexist(const char *file)
{
	struct stat flinfo;	/* structure holding file info read by stat() */
	if ((stat(file, &flinfo)) == -1) {
		printf("ERROR: Failed to open %s \n", file);
		return 1;
	}
    if(!flinfo.st_size){
        printf("ERROR: invalid file %s of size %d is provided\n",file,(int)flinfo.st_size);
        return 1;
    }
	return 0;
}

static struct option longopts[] = {
	{"rom", required_argument, NULL, 'r'},
	{"fw", required_argument, NULL, 'f'},
	{"bootloader", required_argument, NULL, 'b'},
	{"json1", required_argument, NULL, 'j'},
	{"json2", required_argument, NULL, 'j'},
	{"isp1", required_argument, NULL, 'i'},
	{"isp2", required_argument, NULL, 'i'},
	{"a1", required_argument,   NULL, 'a'},
	{"a2", required_argument,   NULL, 'a'},
	{"a3", required_argument,   NULL, 'a'},
	{"a4", required_argument,   NULL, 'a'},
	{"help", no_argument, NULL, 'h'},
	{NULL, 0, NULL, 0}
};

int main(int argc, char **argv)
{
    uint16_t vendor_id = R2_VID, product_id = R2_PID;
    int ret;
    FILE *fin = NULL;
    struct stat stfile;
	ios_img_upgrade_type_t image_type;
    char *tx_data;
    int longidx = 0;
    int opt;
    char *rom=NULL;
    int firmware_upgrade_command = 0;
    int gprint = 0;

    while ((opt =
		getopt_long(argc, argv, "r:f:b:j1:j2:i1:i2:a1:a2:a3:a4:iv:ib:gv", longopts, &longidx)) != -1) 
    {
        if (opt == -1){
            usage();
            break;
        }
        if(gprint == 0){
            printf("Upgrade ->");
            gprint = 1;
        }
        switch (opt)
        {
            case 'r': 
                printf("rom\n");
                image_type = FW_UPGRADE_ROM;
                rom = optarg;
                firmware_upgrade_command =1;
            break;
            case 'f':
                printf("firmware\n");
                image_type = FW_UPGRADE_IMG;
                rom = optarg;
                firmware_upgrade_command = 1;
            break;
            case 'b':
                printf("bootloader\n");
                image_type = FW_UPGRADE_BOOTLOADER;
                rom = optarg;
                firmware_upgrade_command = 1;
            break;
            case 'h':
            default:
			    usage();
			    exit(0);
			break;
        }
    }

    if(firmware_upgrade_command==0){
        usage();
        exit(0);
    }
    //check if file is present
    if(firmware_upgrade_command){
        if(isfileexist(rom))
            return 1; 
    }

    ret = libusb_init(&video_ctx);
    if(ret < 0){
        printf("init_libusb failed\n");
        if(fin)fclose(fin);
        return -1;
    }

    video_hdl = libusb_open_device_with_vid_pid(video_ctx, vendor_id,
                            product_id);
    
    if(video_hdl == NULL){
        printf("Could not open USB device %x:%x\n", vendor_id, product_id);
        goto error;
    }
    
    ret = libusb_claim_interface(video_hdl, IPCAM_EAP_INTERFACE_NUM);
    if(ret != 0){
        printf("libusb_claim_interface error %d\n",ret);
        printf("Please connect a GC6500 running in ios acc mode\n");
        goto error;
    }

    tx_data = (char *)malloc(EAP_BULK_OUT_BUF_SIZE);
    if(!tx_data){
        printf("malloc failed\n");
        goto error;
    }

    //set interface to activte the endpoints
    ret = libusb_set_interface_alt_setting( video_hdl, IPCAM_EAP_INTERFACE_NUM, 1);
    if(ret != 0){
        printf("libusb_set_interface_alt_setting error %d\n",ret);
        goto error;
        return -1;
    }
    
    if(firmware_upgrade_command)
    {
        fin = fopen(rom, "rb");
        if(stat(rom, &stfile)) {
            printf("Error: File %s not found\n", rom);
            goto error;        
        }
        ret = cam360_firmware_upgrade(image_type, fin, stfile.st_size);
    }
error:
    if(fin)fclose(fin);
    if(video_hdl)
        ret = libusb_release_interface(video_hdl, IPCAM_EAP_INTERFACE_NUM);
    close_device();
    return 0;
}
