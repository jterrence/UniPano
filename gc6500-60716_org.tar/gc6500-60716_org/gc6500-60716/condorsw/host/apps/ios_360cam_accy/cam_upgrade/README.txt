This cam_upgrade tool is used to upgrade the camera snor from x86 Linux host 
with the provided full rom image created by mxpatchrom.

Compile:
make all

Clean:
make clean

Run:
sudo ./cam_upgrade <rom-image>
