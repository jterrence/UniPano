/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef __USB_IAP_H
#define __USB_IAP_H

#define NUM_REQUESTS_EAP 8

#define CMDR_TYPE   (('c' << 24) | ('m' << 16) | ('d' << 8) | ('r')) //command response

/** @file
 * @brief cam360 API
 */
typedef struct{
    uint32_t packet_type;
    uint32_t packet_size;
}cmd_rsp_hdr_t;

typedef enum {
    IOS_ACC_UPGRADE_COMMANDS = 0x1,
    IOS_ACC_UPGRADE_RESPONSE, //0x02

    IOS_ACC_START_STREAM, //0x03
    IOS_ACC_STOP_STREAM,  //0x04

    IOS_ACC_VIDEO_COMMANDS, //0x05
    IOS_ACC_AUDIO_COMMANDS, //0x06
    IOS_ACC_DEWARP_COMMANDS, //0x07
    IOS_ACC_ISP_COMMANDS,  //0x08
    IOS_ACC_SYSTEM_COMMANDS, //0x09
    IOS_ACC_CHIP_RESET, //0x0A
    IOS_ACC_ALPHA_WRITE, //0x0B
    IOS_ACC_SET_JSON_ID, //0x0C
    IOS_ACC_GET_CAMERA_STATE, //0x0D

    IOS_ACC_QCC_READ, //0x0E
    IOS_ACC_QCC_WRITE, //0x0F

    IOS_ACC_MEM_READ, //0x10
    IOS_ACC_MEM_WRITE, //0x20

    IOS_ACC_GET_DEWARP_STATUS, //0x30
    IOS_ACC_UPDATE_MAP_IDX, //0x31

    IOS_ACC_SENSOR_COMMAND, //0x32
}ios_cmd_id_t;

//command header
typedef struct
{
    ios_cmd_id_t  id; //command id
    uint32_t      cmd_len; //command length
}ios_cmd_hdr_t;

//video commands
typedef enum {
    IOS_ACC_SET_BITRATE = 0x1,
    IOS_ACC_GET_BITRATE, //0x2

    IOS_ACC_SET_FORCE_IFRAME,//0x3

    IOS_ACC_SET_AVC_MAXNAL,//0x4
    IOS_ACC_GET_AVC_MAXNAL, //0x5

}video_cmd_types_t;

//gpio command type
typedef struct{
	unsigned short gpio_pin;
	unsigned short gpio_val;
}__attribute__((__packed__))gpio_type_t;

#define MAX_I2C_PAYLOAD_LEN	128
//i2c structures
typedef struct{
	unsigned short reg_addr;
	char buff_len;
}__attribute__((__packed__))i2c_type_t;

//system commands
typedef enum {
	IOS_ACC_GET_FW_INFO = 0x01,
	IOS_ACC_GPIO_SET, //0x02
	IOS_ACC_GPIO_GET, //0x03
	//i2c related command
		//i2c fpga specific command
	IOS_ACC_FPGA_I2C_WRITE,	//0x04
	IOS_ACC_FPGA_I2C_READ,		//0x05		
	IOS_ACC_FPGA_GET_SYSTEM_STATUS,	//0x06
	IOS_ACC_FPGA_GET_SYSTEM_PARAM,	//0x07
	IOS_ACC_FPGA_GET_VERSION,	//0x08
	IOS_ACC_FPGA_GET_COMMAND,	//0x09
	IOS_ACC_FPGA_GET_SN,	//0x0A
	//i2c bat specific command
	IOS_ACC_BAT_I2C_WRITE,	//0x0B
	IOS_ACC_BAT_I2C_READ,	//0x0C
	IOS_ACC_BAT_GET_LEVEL,	//0x0D
		
	//i2c ACP specific command
	IOS_ACC_ACP_WRITE = 0x20,
	IOS_ACC_ACP_READ,		//0x21

    IOS_ACC_READ_POST_RESULT //0x22
}system_cmd_types_t;

typedef enum {
    VIDEO_CHANNEL = 0x1,
    AUDIO_CHANNEL = 0x2
}channel_type_t;

typedef struct{
    ios_cmd_hdr_t hdr;
    int             mux_channel_id;
    channel_type_t  type;
}ios_stream_state_cmd_t;

//video control commands
typedef struct{
    ios_cmd_hdr_t hdr;
    video_cmd_types_t vcmd;
    int mux_channel_id;
    uint32_t data_length;
    uint32_t cmd_data;
}__attribute__((__packed__))ios_video_cmd_t;

//system commands
typedef struct{
	ios_cmd_hdr_t hdr;
	system_cmd_types_t scmd;
	uint32_t data_length;
}__attribute__((__packed__))ios_system_cmd_t;

/** Json File index in SNOR */
typedef enum{
    /** First Json file */
    JSON_ID_1 =1,
    /** Second Json file */
    JSON_ID_2 =2,
    /** Undefined */
    JSON_ID_UNDEFINED
}json_id_t;

/** Json ID command */
typedef struct{
    ios_cmd_hdr_t hdr;
    json_id_t json_id;
}__attribute__((__packed__))ios_jsonid_cmd_t;

/** response of the camera state */
typedef enum
{
    CAMERA_READY_STATE =1,
    CAMERA_INPROGRESS_STATE =2,
    CAMERA_ERROR_STATE
}camera_state_t;

typedef struct{
    ios_cmd_hdr_t  hdr;
    camera_state_t state;
}__attribute__((__packed__))ios_cam_state_rsp_t;

typedef struct{
    ios_cmd_hdr_t  hdr;
    uint16_t       qcc_addr;
    uint16_t       qcc_bid;
    uint32_t       length;
    uint32_t       value;
}__attribute__((__packed__))ios_cam_qcc_rw_t;

typedef struct{
    ios_cmd_hdr_t  hdr;
    uint32_t       mem_addr;
    uint32_t       data_len;
}__attribute__((__packed__))ios_cam_mem_rw_t;

/** Sensor parameters bitmap */
typedef union {
    unsigned int d32;
#if __BYTE_ORDER == __LITTLE_ENDIAN
    struct {
        unsigned int exposure: 1;
        unsigned int gain : 1;
        unsigned int reserved : 30;
    } b;
#elif __BYTE_ORDER == __BIG_ENDIAN
    struct {
        unsigned int reserved : 30;
        unsigned int gain : 1;
        uint32_t exposure: 1;
    } b;
#endif
}sensor_cmd_type_t;

/** Sensor parameters */
typedef struct{ 
    /** parameter type */
    sensor_cmd_type_t type;
    /** sensor exposure time */
    unsigned int sensor_exposure;
    /** sensor gain */
    unsigned int sensor_gain;
}__attribute__((__packed__))sensor_params_t;

/** Sensor command */
typedef struct{
    /** ios command id */
    ios_cmd_hdr_t   hdr;
    /** sensor parameters */
    sensor_params_t sparams;
}__attribute__((__packed__))ios_sensor_cmd_t;
/** @defgroup video_system Video Subsystem
  * @{
  */
/** @defgroup video_start_stop Start/Stop Video Capture
  * @{
  */
/**
 * @brief This API is used to start the capture of video data from the specified
 * channel in the camera.
 * 
 * @param[in] ch video channel on which the video capture needs to be started
 * 
 * @return 0 on Success, -1 on Failure
 */
int cam360_video_start(video_channel_t ch);

/**
 * @brief This API is used to stop the capture of video data from a channel in
 * the camera.
 * 
 * @param[in] ch video channel on which the video capture needs to be stopped
 * 
 * @return 0 on Success, -1 on Failure
 */
int cam360_video_stop(video_channel_t ch);
/**@}*/

/** @defgroup video_control Video Parameters (Set/Get)
  * @{
  */
/**
 * @brief This API is used to force an I frame in the specified video channel.
 * Applicable only for VID_FORMAT_H264_RAW
 * 
 * @param[in] ch video channel on which I frame needs to be forced
 * 
 * @return 0 on Success, -1 on Failure
 */
int cam360_video_force_iframe(video_channel_t ch);

/**
 * @brief This api is used to set bitrate on the video channel specified.
 * 
 * @param[in] ch     video channel for which bitrate is to set
 * @param[in] bitrate  bitrate to set on the channel
 * 
 * @return 0 on Success, -1 on Failure
 */
int cam360_video_set_bitrate(video_channel_t ch, uint32_t bitrate);

/**
 * @brief This api is used to get current bitrate of the video channel specified.
 * 
 * @param[in]  ch     video channel for which bitrate is to get
 * @param[out] bitrate  pointer to receive the bitrate
 * 
 * @return 0 on Success, -1 on Failure
 */
int cam360_video_get_bitrate(video_channel_t ch, uint32_t *bitrate);

/**
 * @brief This api is used to set sensor parameter.
 * 
 * @param[in] sparam sensor parameters
 * 
 * @return 0 on Success, -1 on Failure
 */
int cam360_video_set_sensor_param(sensor_params_t *sparam);

/**@}*/

/**@}*/

/** @defgroup audio_system Audio Subsystem
  * @{
  */
/** @defgroup audio_start_stop Start/Stop Audio Capture 
  * @{
  */
/**
 * @brief This API is used to start the capture of audio data from the
 * mux channel in the camera.
 * 
 * @param[in] ch channel on which the audio capture needs to be started
 * 
 * @return 0 on Success, -1 on Failure
 */
int cam360_audio_start(video_channel_t ch);

/**
 * @brief This API is used to stop the capture of audio data from a specified
 * channel in the camera.
 * 
 * @param[in] ch channel on which the audio capture needs to be started
 * 
 * @return 0 on Success, -1 on Failure
 */
int cam360_audio_stop(video_channel_t ch);

/**@}*/
/**@}*/

/** @defgroup snor_upgrade Firmware Upgrade
  * @{
  */
/** list of upgrade commands */
typedef enum {
    /** Upgrade start */
    FW_UPGRADE_START = 0x01,
    /** Upgrade image transfer */
    FW_UPGRADE_IMAGE, //0x2
    /** Upgrade complete */
	FW_UPGRADE_COMPLETE,//0x3
    /** Upgrade status query */
    FW_UPGRADE_STATUS,//0x4
    /** unsupported request */
    FW_UPGRADE_REQ_LAST = 0xFF
}ios_upgrade_cmds_t;

/** upgrade types */
typedef enum{
    /** ROM image Upgrade */
	FW_UPGRADE_ROM = 0x01,
    /** Firmware image Upgrade */
	FW_UPGRADE_IMG,	//0x02
    /** Bootloader Upgrade */
    FW_UPGRADE_BOOTLOADER //0x03
}ios_img_upgrade_type_t;

/** upgrade header Structure */
typedef struct{
    ios_cmd_hdr_t        hdr;
	ios_img_upgrade_type_t type;
    ios_upgrade_cmds_t   cmd;
    uint32_t             image_size;
}__attribute__((__packed__))ios_img_upgrade_hdr_t;

/** upgrade Status codes */
typedef enum {
    /** upgrade success */
    UPGRADE_SUCCESS = 0x1,
    /** upgrade in progress */
    UPGRADE_IN_PROGRESS,
    /** upgrade error */
    UPGRADE_ERROR,
    /** undefined */
    UPGRADE_UNDEFINED
}upgrade_status_t;

/** upgrade response Structure */
typedef struct{
    /** upgrade response code */
    uint32_t response;
    /** upgrade Status code */
    upgrade_status_t status;
}__attribute__((__packed__))upgrade_rsp_t;
/**
 * @brief This API is used to upgrade the complete SNOR with .rom image
 * or Just the Firmware image in SNOR
 * 
 * @param[in] image_type image type tp upgrade
 * @param[in] image_fin pointer to image File.
 * @param[in] size image size
 *
 * @return 0 on Success, -1 on Failure
 */
int cam360_firmware_upgrade(ios_img_upgrade_type_t image_type, FILE *image_fin, uint32_t size);
/**@}*/

/** @defgroup system_utils System level API
  * @{
  */
/**
 * @brief This API is used to reset camera
 * 
 * @return 0 on Success, -1 on Failure
 */
int cam360_chip_reset(void);

/**
 * @brief This API is used to select JSON Index from SNOR.
 * 
 * @param[in] id Json Index in SNOR.
 * 
 * @return 0 on Success, -1 on Failure
 */
int cam360_set_jsonid(json_id_t id);

/**
 * @brief This API is used to get the current state of camera to 
 * know if the camera is ready for streaming or not.
 * 
 * @param[in] id Json Index in SNOR.
 * 
 * @return 0 if camera is ready, -1 on error cases
 */
int cam360_get_camera_state(void);

/**
 * @brief This API is used to read any QCC Register value 
 * 
 * @param[in] bid BID of the QCC Register.
 * @param[in] addr QCC Register address
 * @param[in] QCC Register length
 * @param[out] value pointer to receive the QCC register value
 *
 * @return 0 on Success, -1 on Failure
 */
int cam360_qcc_read(uint16_t bid, uint16_t addr, uint16_t length, uint32_t *value);

/**
 * @brief This API is used to write a value in QCC Register
 * 
 * @param[in] bid BID of the QCC Register.
 * @param[in] addr QCC Register address
 * @param[in] QCC Register length
 * @param[in] value value to write in the QCC register
 * 
 * @return 0 on Success, -1 on Failure
 */
int cam360_qcc_write(uint16_t bid, uint16_t addr, uint16_t length, uint32_t value);

/**
 * @brief This API is used to read memory content from camera 
 * 
 * @param[in] address read address.
 * @param[out] buffer pointer to buffer to read data 
 * @param[in] buffersize read length(Max supported size is 4KB)
 * 
 * @return 0 on Success, -1 on Failure
 */
int cam360_mem_read_4k(uint32_t address, uint8_t *buffer, uint32_t buffersize);

/**
 * @brief This API is used to write content to camera memory
 * 
 * @param[in] address write address.
 * @param[out] buffer pointer to data for writing 
 * @param[in] buffersize write length(Max supported size is 4KB)
 * 
 * @return 0 on Success, -1 on Failure
 */
int cam360_mem_write_4k(uint32_t address, uint8_t *buffer, uint32_t buffersize);

/**
 * @brief This API is used to get the running FW svn version
 * 
 * @return 0 on Success, -1 on Failure
 */
int cam360_get_fw_version(void);

/**
 * @brief This API is used to set the state of a gpio line in camera
 * 
 * @param[in] arg_gpio_pin gpio id
 * @param[in] arg_gpio_val gpio value
 * 
 * @return 0 on Success, -1 on Failure
 */
int cam360_gpio_set(char *arg_gpio_pin, char *arg_gpio_val);

/**
 * @brief This API is used to get the current state of a gpio line in camera
 * 
 * @param[in] arg_gpio_pin gpio id
 * 
 * @return current gpio pin value(1/0)
 */
int cam360_gpio_get(char *arg_gpio_pin);
/**@}*/
#endif //__USB_IAP_H
