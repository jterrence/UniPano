/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef _QBOX_H
#define _QBOX_H

/** @file
 * @brief qbox header related informations.
 */

/** enum of video format used in a particular channel */
typedef enum {
    FIRST_VID_FORMAT       = 0,
    /** H264 in elementary stream format */
    VID_FORMAT_H264_RAW    = 0,
    /** H264 in transport stream format */
    VID_FORMAT_H264_TS     = 1,
    /** MJPEG in elmentary stream format */
    VID_FORMAT_MJPEG_RAW   = 2,
    /** YUV stream in YUY2 format */
    VID_FORMAT_YUY2_RAW    = 3,
    /** YUV stream in NV12 format */
    VID_FORMAT_NV12_RAW    = 4,
    /** Luma stream format */
    VID_FORMAT_GREY_RAW    = 5,
    /** H264 and AAC in transport stream format */
    VID_FORMAT_H264_AAC_TS = 6,
    /** mux data */
    VID_FORMAT_MUX         = 7,
    /** datas */
    VID_FORMAT_METADATAS       = 8,
    /** mux data */
    VID_FORMAT_AAC_RAW		   = 9,
    /** total number of video formats supported */
    NUM_VID_FORMAT
} video_format_t;


/** enum to indicate the video channel number */
typedef enum channel {
    FIRST_VID_CHANNEL = 0,

    /** channels for skype mode */
    /** channel for main video */
    CH_MAIN    = 0,
    /** channel for preview video */ 
    CH_PREVIEW = 1,
    /** number of video channels in skype mode */   
    NUM_SKYPE_VID_CHANNELS,
    /** number of video channels in skype mode */   
    NUM_VID_CHANNEL = NUM_SKYPE_VID_CHANNELS,

    /** channels for ip camera */
    /** video channel1 */
    CH1  = 0,
    /** video channel2 */
    CH2  = 1,
    /** video channel3 */
    CH3  = 2,
    /** video channel4 */
    CH4  = 3,
    /** video channel5 */
    CH5  = 4,
    /** video channel6 */
    CH6  = 5,
    /** video channel7 */
    CH7  = 6,
    /** number of mux channels in ip camera */
    NUM_MUX_VID_CHANNELS  = CH7+1,
    /** video channel for RAW stream */
    CH_RAW            = NUM_MUX_VID_CHANNELS,
    /** total number of video channels in ip camera */
    NUM_IPCAM_VID_CHANNELS
} video_channel_t;

/** enum to indicate the H264 profile used for encoding */
typedef enum {
    /** baseline profile */
    PROFILE_BASELINE = 0,
    /** main profile */
    PROFILE_MAIN     = 1,
    /** high profile */
    PROFILE_HIGH     = 2,
    /** number of h264 profiles supported */
    NUM_PROFILE
} video_profile_t;

/** enum to turn on/off video flip */
typedef enum {
    /** turn off flip */
    FLIP_OFF    = 0,
    /** trun on flip */
    FLIP_ON     = 1,
    /** number of flip mode supported */
    NUM_FLIP
} video_flip_t;

/** enum to set the Wide Dynamic Range (WDR) mode */
typedef enum {
    /** disable WDR */
    WDR_DISABLE = 0,
    /** auto WDR */
    WDR_AUTO    = 1,
    /** manual WDR */
    WDR_MANUAL  = 2,
    /** number of WDR modes supported */
    NUM_WDR
} wdr_mode_t;

/** TBD */
typedef enum {
    /** TBD */
    HISTO_EQ_OFF = 0,
    /** TBD */
    HISTO_EQ_ON =  1,
    /** TBD */
    NUM_HISTO_EQ
} histo_eq_t;

typedef enum {
    SATURATION_AUTO = 0,
    SATURATION_MANUAL  = 1,
    NUM_SATURATION
} saturation_mode_t;

typedef enum {
    BRIGHTNESS_AUTO = 0,
    BRIGHTNESS_MANUAL  = 1,
    NUM_BRIGHTNESS
} brightness_mode_t;

typedef enum {
    CONTRAST_AUTO = 0,
    CONTRAST_MANUAL  = 1,
    NUM_CONTRAST
} contrast_mode_t;

/** compressed alpha download error code*/
typedef enum{
    /** success */
    COMPRESSED_ALPHA_SUCCESS = 0,
    /** firmware is busy processing the request from host */
    COMPRESSED_ALPHA_STATUS_BUSY = 1,
    /** firmware not ready to accept the compressed token */
    COMPRESSED_ALPHA_ERROR_TOKEN_NOT_READY = 2,
    /** error while allocating memory in firmware to hold the decompressed alpha map */
    COMPRESSED_ALPHA_ERROR_DECOMPRESSION_MALLOC = 3,
    /** error while decompressing the compressed alpha map in firmware */
    COMPRESSED_ALPHA_ERROR_DECOMPRESSION_FAILURE = 4,
    /** error while converting the raster format alpha map to tile format in firmware */
    COMPRESSED_ALPHA_ERROR_RASTER_TO_TILE = 5,
    /** error while allocating memory in firmware to receive compressed alpha map from host */
    COMPRESSED_ALPHA_ERROR_UVC_MALLOC = 6,
} compressed_alpha_errorcode_t;

/** Structure containing the information regarding an encoding channel */
typedef struct
{
    /** format of the video used in the channel */
    video_format_t  format;

    /** width of the video in the channel */
    uint16_t    width;

    /** height of the video in the channel */
    uint16_t    height;

    /** frame Rate of the video in the channel */
    uint32_t    framerate;

    /** GOP size of video to be used in the channel, applicable only for
    VID_FORMAT_H264_RAW,
    VID_FORMAT_H264_TS,
    VID_FORMAT_H264_AAC_TS */
    uint32_t    goplen;

    /** H264 profile used for encoding in the channel, applicable only for
    VID_FORMAT_H264_RAW,
    VID_FORMAT_H264_TS,
    VID_FORMAT_H264_AAC_TS */
    video_profile_t profile;

    /** bitrate of the video in the channel */
    uint32_t    bitrate;

    /** compression quality in terms of the QP factor set for the video
     on this channel, applicable only for VID_FORMAT_MJPEG_RAW */
    uint32_t    compression_quality;
} video_channel_info_t;

/** structure containing video crop information */
typedef struct {
    /** enable/disable cropping. Default: 0, Min: 0, Max: 1 */
    uint16_t enable;

    /** width to be cropped from the image. Default: 640, Min: 16, Max: 1920 */
    uint16_t width;

    /** height to be cropped from the image. Default: 480, Min: 16, Max: 1080 */
    uint16_t height;

    /** X offset from which the image is cropped. Default: 0, Min: 0, Max: 1920 */
    uint16_t x;

    /** Y Offset from which the image is cropped. Default: 0, Min: 0, Max: 1080 */
    uint16_t y;

} crop_info_t;


/** structure containing video crop information */
typedef struct {
    /** width of AE ROI region*/
    uint16_t width;

    /** height of AE ROI region */
    uint16_t height;

    /** X offset of AE ROI region relative to top left corner */
    uint16_t x;

    /** Y Offset of AE ROI region relative to top left corner */
    uint16_t y;

    /** Options for using the ROI */
    /*  0: Revert to default ISP ROI as set in JSON
        1: Use the passed ROI only (use weight 15 for ROI, 0 outside)
        2: Add the passed ROI (use weight 15 in ROI, keep outside weights same as before)
        3: Remove the passed ROI (use weight 0 for ROI, keep outside weights same as before) */
    uint16_t mode;

} isp_ae_roi_info_t;

/** structure containing the information regarding motion vector statistics */
typedef struct {
    /** buffer containing the motion stats */
    uint8_t *buf;
    /** size of the data in bytes */
    int size;
} motion_stat_t;

/** TBD */
typedef struct
{
    /** TBD */
    uint32_t    PMEVectorSize;
    /** TBD */
    uint32_t    PMEVectorType;
    /** TBD */
    uint32_t    mbWidth;
    /** TBD */
    uint32_t    mbHeight;
    /** TBD */
    uint32_t    PictStr;
    /** TBD */
    uint32_t    ResultsMV;
    /** TBD */
    uint32_t    MCost;
    /** TBD */
    uint32_t    NumRef;
    /** TBD */
    uint32_t    Refid0;
    /** TBD */
    uint32_t    Refid1;
    /** TBD */
    uint32_t    PMEMaxYRange;
} PMEVectorTag_t;

/** TBD */
typedef struct {
    /** TBD */
    PMEVectorTag_t *pmevtag;
    /** TBD */
    int8_t   *PMEVectorAddress;
} pme_info_t;

/** TBD */
typedef struct {
   /** TBD */
    uint8_t *buf;
    /** TBD */
    int size;
} rectangle_info_t;

/** Histogram Data */
typedef struct {
   /** Pointer to histogram data */
    uint8_t *buf;
    /** Size of histogram data */
    int size;
} histogram_info_t;

typedef struct {
/// \brief Reports total sum of all Luma pixels in the entire
    unsigned int GPictYSum;
/// \brief Reports total sum of all Chroma pixels in the entire
    unsigned int GPictCSum;
/// \brief Reports sum of horizontal high frequency Luma information for the entire picture
    unsigned int GPictYHorzHighFreq;
/// \brief Reports sum of vertical high frequency Luma information for the entire picture
    unsigned int GPictYVertHighFreq;
/// \brief Reports sum of vertical high frequency Chroma information for the entire picture
    unsigned int GPictCHighFreq;
/// \brief Counts how many macroblocks had any Luma high frequency information. 
///  The hardware averages horizontal and vertical Luma high frequencies for each macroblock.
///  This value reflects the size of the non-zero two-dimensional high frequency information.
    unsigned int GPictYHighFreqSize;
/// \brief Counts how many macroblocks had any Chroma high frequency information. 
///  The hardware detects chroma high frequency for each macroblock.
///  This value reflects the size of the non-zero high frequency information.
    unsigned int GPictCHighFreqSize;
/// \brief Counts how many macroblocks had any Luma spatial edge information. 
/// The only macroblocks not counted are those with null strength information.
/// This register reflects the size of the spatial Luma edge content of the picture.
    unsigned int GPictYEdgeSize;
/// \brief Counts how many macroblocks had any Luma Motion edge information.
/// The only macroblocks not counted are those without Luma Motion direction and strength information. 
/// \brief Provides the sum of MB averages for all macroblock luma edge strengths in the picture. 
    unsigned int GPictEdgeStrengthMBSum;
/// \brief Total sum of pixels classified as having Color[0-3].
    unsigned int GPictColorSum[4];
/// \brief Counts how many macroblocks had any Color[0-3] information. 
/// The only macroblocks not counted are those without Color[0-3] information. 
/// This register reflects the size of areas of the picture in Color[0-3].
    unsigned int GPictColorSize[4];
} global_stats_t;


/** Global VPP Stats Data */
typedef struct {
   /** Pointer to VPP global stats data */
    global_stats_t *buf;
    /** Size of global stats data */
    int size;
} globalstats_info_t;

/** QMED extension */
typedef struct {
  /** Pointer to QMED extension data */
    char *qmedExt;
   /** Size of QMED extension data */
    unsigned int total_len;
} metadata_t;

/** QMED */
typedef struct {
  /** Pointer to QMED data */
    char *qmedPtr;
   /** Size of QMED data */
    unsigned int total_len;
} qmed_t;


/** Video Information structure used for processing the video data received 
from the camera in the callback function */
typedef struct {
    /** format of the video frame received */
    video_format_t format;

    /** video frame timestamp in terms of ticks of 90kHz clock, where each tick
     corresponds to 1/(90 * 1000) sec or 1/90 ms */
    uint64_t       ts;

    /** motion vector statistics information of the video frame,
     only in case of
     VID_FORMAT_YUY2_RAW,
     VID_FORMAT_NV12_RAW
     VID_FORMAT_GREY_RAW */
    motion_stat_t  stats;

    /** physical buffer index of the video frame dequeued by the V4L.
     This needs to be used by MXUVC application to queue back the video frame
     after processing, in the mxuvc_video_cb_buf_done() function described
     later */
    int            buf_index;

    /** PME data */
    pme_info_t     pme;
    /** rectangle data when smart motion is running on xtensa */
    rectangle_info_t  rect;
    /** Histogram data */
    histogram_info_t histogram;
    /** VPP global stats data */
    globalstats_info_t globalstats;
    /** Metadata */
    metadata_t     metadata;
    /** QMed */
    qmed_t qmed;
}__attribute__((packed))video_info_t;

/** enum to set the noise filter mode for the image processing */
typedef enum {
    /** auto mode */
    NF_MODE_AUTO = 0,
    /** manual mode */
    NF_MODE_MANUAL = 1,
    /** number of noise filter modes supported */
    NUM_NF
} noise_filter_mode_t;

typedef struct {
    uint16_t mode       ; // 0 : Bypass  1: Adpative_with_sensor_defaults  2:Adaptive_with_api_params  3:Manual
    uint16_t minNR      ;
    uint16_t maxNR      ;
    uint16_t minThresh  ;
    uint16_t maxThresh  ;
    uint16_t triggerPt  ;
}sinter_info_t;

/** enum to set the white balance mode in the sensor */
typedef enum {
    /** auto mode */
    WB_MODE_AUTO = 0,
    /** manual mode */
    WB_MODE_MANUAL = 1,
    /** number of white balance modes supported */
    NUM_WB
}white_balance_mode_t;

/** enum to enable/disable sensor zonal white balance */
typedef enum {
    /** disbale zonal white balance */
    ZONE_WB_DISABLE = 0,
    /** enable zonal white balance */
    ZONE_WB_ENABLE  = 1,
    /** number of zonal white balance modes supported */
    NUM_ZONE_WB,
}zone_wb_set_t;

/** enum to set power line frequency mode */
typedef enum {
    /** disable power line frequency */
    PWR_LINE_FREQ_MODE_DISABLE = 0,
    /** 50Hz power line frequency */
    PWR_LINE_FREQ_MODE_50HZ = 1,
    /** 60Hz power line frequency */
    PWR_LINE_FREQ_MODE_60HZ = 2
}pwr_line_freq_mode_t;

typedef enum {
    IPCAM = 0,
    SKYPE = 1,
    UNKNOWN
}camer_mode_t;

typedef struct {
    uint32_t minBitrate;
} vbr_params_t;



/** enum to indicate the format type supported */
typedef enum {
    /** PCM format */
    AUD_FORMAT_PCM_RAW  = 0,
    /** AAC format */
    AUD_FORMAT_AAC_RAW  = 1,
    /** OPUS format */
    AUD_FORMAT_OPUS_RAW = 2,
    /** number of audio formats supported */
    NUM_AUD_FORMAT
} audio_format_t;

/** enum to indicate the MXUVC audio channel type*/
typedef enum {
    /** channel for capturing audio in PCM format */
    AUD_CH1 = 0,
    /** channel for capturing audio in encoded format (AAC or OPUS) */
    AUD_CH2,
    /** number of audio channels available */
    NUM_AUDIO_CHANNELS
} audio_channel_t;

/** structure containing the parameters of audio data received in
 callback associated to audio channel */
typedef struct
{
    /** audio timestamp in terms of ticks of 90khz clock where
    each tick corresponds to 1/(90 * 1000) sec or 1/90 ms */
    long long timestamp;
    /** size of the audio frame received */
    int framesize;
    /** sampling frequency at which the audio frame is captured */
    int samplefreq;
    /** number of audio channels captured by the microphone and/or
    encoded */
    int channelno;
    /** audio object type with which the Audio stream is encoded.
     This is useful to construct the ADTS Header in case of AAC
     encoded stream. Ignore in case of PCM or other format. */
    int audioobjtype;
    /** format of the captured audio */
    audio_format_t format;
    /** pointer to audio frame data */
    unsigned char *dataptr;
} audio_params_t;

int qbox_parse_header(uint8_t *buf, int *channel_id, video_format_t *fmt,
                      uint8_t **data_buf, uint32_t *size, uint64_t *ts,
                      uint32_t *analytics, metadata_t *metadata,
                      qmed_t *qmed);
int audio_param_parser(audio_params_t *h, unsigned char *buf, int len);
int get_qbox_hdr_size(void);
#endif	/* _QBOX_H */
