#ifndef __USB_IAP_LDMAP_H
#define __USB_IAP_LDMAP_H

#define R2_VID 0x29fe
#define R2_PID 0x4d53

#define USB_DIR_OUT         0       /* to device */
#define USB_DIR_IN          0x80        /* to host */

#define IAP2_BULK_IN_EP 0x02
#define IAP2_BULK_OUT_EP 0x01

#define EAP_BULK_IN_EP 0x03
#define EAP_BULK_OUT_EP 0x04

#define EAP_BULK_OUT_BUF_SIZE 1024*4

#define BULK_OUT_TX_TIMEOUT 2000
#define BULK_IN_RX_TIMEOUT 2000

#define NUM_OVERLAY_TEXT_IDX 5
#define NUM_OVERLAY_IMAGE_IDX 1

#endif //__USB_IAP_LDMAP_H
