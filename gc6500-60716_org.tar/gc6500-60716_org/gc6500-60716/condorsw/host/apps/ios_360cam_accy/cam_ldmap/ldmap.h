/*******************************************************************************                                              
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef __LDMAP_H__
#define __LDMAP_H__

/** @file
 * @brief Dewarp related API
 */

/** @defgroup dewarp_control Loading Dewarp Map
  * @{
  */

/** Dewarp Status structure */
typedef struct{
    ios_cmd_hdr_t  hdr;
    uint32_t       status;//0 or 1
}__attribute__((__packed__))ios_cam_dewarp_status_t;

/** Dewarp Queue Pointer Update */
typedef struct{
    /** dewarp map addr */
    unsigned int dwp;
    /** map location */
    unsigned int location;
}__attribute__((__packed__))map_write_pointer_info_t;

/** max Dewarp map count  */
#define MAX_NUM_MAPS 2
/** Dewarp Map index command structure */
typedef struct{
    ios_cmd_hdr_t  hdr;
    uint32_t map_count;
    map_write_pointer_info_t wp[MAX_NUM_MAPS];
}__attribute__((__packed__))ios_cam_map_idx_t;

/** Structure containing dewarp map information */
typedef struct{
    /** string that is used to identify the dewarp object the map is for. This string is same
        as the dewarp name used by JSON script. Normally, it has names like “dewarp0”,
        “dewarp1”.... Etc */
    const char *pObjName;
    /** An array of floats that contains the X components of the map array */
    float *pMapx; 
    /** An array of floats that contains the Y components of the map array. */
    float *pMapy;
    /** Width of the map in number of points. */
    short mapw; 
    /** Height of the map in number of points. */
    short maph;
    /** Width of the input video. You can find this value in the JSON script */
    short inw; 
    /** Height of the input video. Same value as used in JSON script */
    short inh; 
    /** Width of the dewarped image. Get this from JSON script from parameter with the same */
    short dww; 
    /** Height of the dewarped image. Get this from JSON script from parameter with the same */
    short dwh;
    /** dewarp write pointer add */
    unsigned int dwp;
    /** new location */
    unsigned int location;
}map_info_t;

/**
 * @brief This function initializes the map loading module and 
 * it must be called once before other functions in this set of API can be used. 
 * This is a module wise initializer. And only need to be called once regardless 
 * ofhow many dewarp objects there are.
 *  
 * @return 0 on Success, -1 on Failure
 */
int cam360_ldmap_init(void);

/**
 * @brief This function is called once when the map loading API is no longer needed. When called, it deallocates
 * resources that had been reserved for the Map Loading API.
 * 
 * @return Always return 0
 */
int cam360_ldmap_deinit(void);

/**
 * @brief This function uploads a map to dewarp object. This function can only be used once the dewarp has been
 * put in running state. Its job is to copy a map to the dewarp map queue, then increment the write pointer.
 * 
 * @param [in] minfo pointer to structure containing dewarp map information.
 *
 * @param [in] map_count Dewarp map count
 *
 * @return 0 on Success, -1 on Failure
 */
int cam360_ldmap_upload(map_info_t *minfo, unsigned int map_count);

/**
 * @brief This function is called to update Dewarp Map index in the Camera in the queue.
 * 
 * @return 0 on Success, -1 on Failure
 */
int cam360_update_map_index(map_write_pointer_info_t *mapw, int map_count);
/**@}*/

#endif //__LDMAP_H__
