/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <libusb-1.0/libusb.h>
#include <string.h> /* memset, memcmp */
#include <unistd.h> /* sleep */
#include <assert.h> /* assert */

#include "qmed.h"
#include "qmedext.h"
#include "qbox.h"
#include "usb_iap.h"
#include "common.h"
#include "ldmap.h"
#include "yuvutil.h"

#define PRINTF(str...)  //printf(str)

struct libusb_device_handle *video_hdl;
struct libusb_context   *video_ctx;

static unsigned int choidx[NUM_MUX_VID_CHANNELS][NUM_OVERLAY_IMAGE_IDX];
static unsigned int chbidx[NUM_MUX_VID_CHANNELS][NUM_OVERLAY_TEXT_IDX];

#define IDX_HANDLE			(0xFF000000)
#define IDX_TEXT_ID         (0x00AA0000)
#define IDX_IMAGE_ID        (0x00BB0000)

#define MAX_TEXT_LENGTH   	(12)
#define EP0TIMEOUT   		(0)
#define FWPACKETSIZE 		4088
#define QCC_BID_PMU         0x21      /* Partition Management Unit */
#define QCC_BID_PMC         0x22      /* Physical Memory Ctl. (SDRAM) */	

#define BULK_OUT_TX_TIMEOUT 2000
#define MAX_MEMW_SIZE (4*1024-16) //4K-sizeof(ios_cam_mem_rw_t)

int cam360_ldmap_wait_dewarp_done(void);

int cam360_qcc_write(uint16_t bid, uint16_t addr, uint16_t length, uint32_t value)
{
	int r;
    ios_cam_qcc_rw_t *qccw;
    int transferred_len;
    char data[512];

	//PRINTF("%s (IN)\n",__func__);
    switch (length) {
		case 1:
		value &= 0xFF;
		break;
		case 2:
		value &= 0xFFFF;
		break;
		case 4:
		break;
		default:
		return -1;
	}

    qccw = (ios_cam_qcc_rw_t *)data;
    qccw->hdr.id = IOS_ACC_QCC_WRITE;
    qccw->hdr.cmd_len = sizeof(ios_cam_qcc_rw_t);
    qccw->qcc_addr = addr;
    qccw->qcc_bid = bid;
    qccw->length = length;
    qccw->value = value;

	r = libusb_bulk_transfer(video_hdl,
                            (USB_DIR_OUT + EAP_BULK_OUT_EP),
                            (unsigned char *)qccw,
                            qccw->hdr.cmd_len,
                            &transferred_len,
                            BULK_OUT_TX_TIMEOUT);
	if (r) {
		printf("Failed IOS_ACC_QCC_WRITE %d\n", r);
		return r;
	}

    return 0;
}

int cam360_qcc_read(uint16_t bid, uint16_t addr, uint16_t length, uint32_t *value)
{
	int r;
	int mask;
    ios_cam_qcc_rw_t *qccr;
    int transferred_len;
    char data[512];
    cmd_rsp_hdr_t *hdr;

	//PRINTF("%s (IN)\n",__func__);

	switch (length) {
		case 1:
		mask = 0xFF;
		break;
		case 2:
		mask = 0xFFFF;
		break;
		case 4:
		mask = 0xFFFFFFFF;
		break;
		default:
		return -1;
	}

    qccr = (ios_cam_qcc_rw_t *)data;
    qccr->hdr.id = IOS_ACC_QCC_READ;
    qccr->hdr.cmd_len = sizeof(ios_cam_qcc_rw_t);
    qccr->qcc_addr = addr;
    qccr->qcc_bid = bid;
    qccr->length = length;

	r = libusb_bulk_transfer(video_hdl,
                            (USB_DIR_OUT + EAP_BULK_OUT_EP),
                            (unsigned char *)qccr,
                            qccr->hdr.cmd_len,
                            &transferred_len,
                            BULK_OUT_TX_TIMEOUT);
	if (r) {
		printf("Failed IOS_ACC_QCC_READ %d\n", r);
		return r;
	}

    //wait for response
    r = libusb_bulk_transfer(video_hdl,
                                 (USB_DIR_IN + EAP_BULK_IN_EP),
                                 (unsigned char *)data,
                                 512,
                                 &transferred_len,
                                 BULK_IN_RX_TIMEOUT
                                );
    if(r){
        printf("ERR: libusb_bulk_transfer in for ldm_qcc_read failed %d\n",r);
        return -1;
    }
    hdr = (cmd_rsp_hdr_t *)data;
    if (hdr->packet_type != CMDR_TYPE){
        printf("ERR: ldm_qcc_read() its not a command response\n");
        return -1;
    }
    qccr = (ios_cam_qcc_rw_t *)(data+sizeof(cmd_rsp_hdr_t));
    if(qccr->hdr.id != IOS_ACC_QCC_READ){
        printf("ERR: ldm_qcc_read() its not a qcc read command response\n");
        return -1;
    }
    *value = (qccr->value & mask);
	//*value &= mask;
	return 0;
}

int cam360_ldmap_wait_dewarp_done(void){
    ios_cam_dewarp_status_t *dewrp_sts;
    int transferred_len;
    char data[512];
    int retry = 10;
    int ret = 0;
    cmd_rsp_hdr_t *hdr;

    dewrp_sts = (ios_cam_dewarp_status_t *)data;
    dewrp_sts->hdr.id = IOS_ACC_GET_DEWARP_STATUS;
    dewrp_sts->hdr.cmd_len = sizeof(ios_cam_dewarp_status_t);
    dewrp_sts->status = 0xff;

	ret = libusb_bulk_transfer(video_hdl,
                            (USB_DIR_OUT + EAP_BULK_OUT_EP),
                            (unsigned char *)dewrp_sts,
                            dewrp_sts->hdr.cmd_len,
                            &transferred_len,
                            BULK_OUT_TX_TIMEOUT);
	if (ret) {
		printf("Failed IOS_ACC_GET_DEWARP_STATUS %d\n", ret);
		return ret;
	}

    while(retry--){ //wait for dewarp map upload to complete
        //wait for response
        ret = libusb_bulk_transfer(video_hdl,
                                     (USB_DIR_IN + EAP_BULK_IN_EP),
                                     (unsigned char *)data,
                                     sizeof(data),
                                     &transferred_len,
                                     BULK_IN_RX_TIMEOUT
                                    );
        if(ret){
            printf("ERR: libusb_bulk_transfer in for ldmap_wait_dewarp_done failed %d\n",ret);
            return -1;
        }
        hdr = (cmd_rsp_hdr_t *)data;
        if (hdr->packet_type != CMDR_TYPE){
            printf("ERR: ldmap_wait_dewarp_done() its not a command response\n");
            return -1;
        }
        dewrp_sts = (ios_cam_dewarp_status_t *)(data+sizeof(cmd_rsp_hdr_t));
        if(dewrp_sts->hdr.id != IOS_ACC_GET_DEWARP_STATUS){
            printf("ERR: ldmap_wait_dewarp_done() its not the correct command response\n");
            return -1;
        }

        if(dewrp_sts->status == 0) //done
            break;
        else
            usleep(10*1000);
    }

    return ret;
}

int cam360_mem_read_4k(uint32_t address, uint8_t *buffer, uint32_t buffersize)
{
    ios_cam_mem_rw_t memr;
    cmd_rsp_hdr_t *hdr;
    uint8_t *tmp_buf;
    int transferred_len;
    int r;

    memr.hdr.id = IOS_ACC_MEM_READ;
    memr.hdr.cmd_len = sizeof(ios_cam_mem_rw_t);
    memr.mem_addr = address;
    memr.data_len = buffersize;
    //printf("memr.data_len %d\n",memr.data_len);
    r = libusb_bulk_transfer(video_hdl,
                    (USB_DIR_OUT + EAP_BULK_OUT_EP),
                    (unsigned char *)&memr,
                    memr.hdr.cmd_len,
                    &transferred_len,
                    BULK_OUT_TX_TIMEOUT);
    if (r) {
        printf("Failed IOS_ACC_MEM_READ Bulk out %d\n", r);
        return r;
    }

    tmp_buf = (uint8_t *)malloc(buffersize+sizeof(cmd_rsp_hdr_t)
                                +sizeof(ios_cam_mem_rw_t));
    
    //read the memory
    r = libusb_bulk_transfer(video_hdl,
                    (USB_DIR_IN + EAP_BULK_IN_EP),
                    (unsigned char *)tmp_buf,
                    (buffersize+sizeof(cmd_rsp_hdr_t)+sizeof(ios_cam_mem_rw_t)),
                    &transferred_len,
                    BULK_IN_RX_TIMEOUT);
    if (r) {
		printf("Failed IOS_ACC_MEM_READ bulk in %d\n", r);
		return r;
	}

    hdr = (cmd_rsp_hdr_t *)tmp_buf;
    if (hdr->packet_type != CMDR_TYPE){
        printf("ERR: mem_read() its not a command response\n");
        free(tmp_buf);
        return -1;
    }
    ios_cam_mem_rw_t *mem_read;
    mem_read = (ios_cam_mem_rw_t *)(tmp_buf+sizeof(cmd_rsp_hdr_t));
    if(mem_read->hdr.id != IOS_ACC_MEM_READ){
        printf("ERR: mem_read() its not a IOS_ACC_MEM_READ response\n");
        free(tmp_buf);
        return 1;
    }
    memcpy(buffer, 
           (uint8_t *)(tmp_buf+sizeof(cmd_rsp_hdr_t)+sizeof(ios_cam_mem_rw_t)), 
           buffersize);
    free(tmp_buf);
    return buffersize;    
}

int cam360_mem_write_4k(uint32_t address, uint8_t *buffer, uint32_t buffersize)
{
    ios_cam_mem_rw_t *memr;
    uint8_t *tmp_buf=NULL;
    int transferred_len;
    int r;

    if(buffersize > (EAP_BULK_OUT_BUF_SIZE-sizeof(ios_cam_mem_rw_t))){
        printf("ERROR: Can send %dBytes of memory only\n",
                (EAP_BULK_OUT_BUF_SIZE-sizeof(ios_cam_mem_rw_t)));
        return -1;
    }

    tmp_buf = (uint8_t *)malloc(buffersize
                                +sizeof(ios_cam_mem_rw_t));
    assert(tmp_buf);
    memr=(ios_cam_mem_rw_t *)tmp_buf;

    memr->hdr.id = IOS_ACC_MEM_WRITE;
    memr->hdr.cmd_len = sizeof(ios_cam_mem_rw_t)+buffersize;
    memr->mem_addr = address;
    memr->data_len = buffersize;    
    
    memcpy(tmp_buf+sizeof(ios_cam_mem_rw_t), buffer, buffersize);

    r = libusb_bulk_transfer(video_hdl,
                    (USB_DIR_OUT + EAP_BULK_OUT_EP),
                    (unsigned char *)tmp_buf,
                    memr->hdr.cmd_len,
                    &transferred_len,
                    BULK_OUT_TX_TIMEOUT);
    if (r) {
        printf("Failed IOS_ACC_MEM_WRITE %d\n", r);
        free(tmp_buf);
        return r;
    }
    free(tmp_buf);

    return buffersize;
}

int cam360_update_map_index(map_write_pointer_info_t *mapw, int map_count)
{
    ios_cam_map_idx_t mapIdx;
    int count = 0;
    int r = 0;
    int transferred_len;

    mapIdx.hdr.id = IOS_ACC_UPDATE_MAP_IDX;
    mapIdx.hdr.cmd_len = sizeof(ios_cam_map_idx_t);
    mapIdx.map_count = map_count;

    while(count<map_count){
        mapIdx.wp[count].dwp = mapw[count].dwp;
        mapIdx.wp[count].location = mapw[count].location;
        PRINTF("update_map_index: updating write index at codec addr 0x%x with 0x%x\n", mapw[count].dwp,mapw[count].location);
        count++;
    }

    r = libusb_bulk_transfer(video_hdl,
                    (USB_DIR_OUT + EAP_BULK_OUT_EP),
                    (unsigned char *)&mapIdx,
                    mapIdx.hdr.cmd_len,
                    &transferred_len,
                    BULK_OUT_TX_TIMEOUT);
    if (r) {
        printf("Failed IOS_ACC_UPDATE_MAP_IDX %d\n", r);
        return r;
    }

    return r;
}

int ldm_memw(unsigned int codecAddr, unsigned char *buffer, int buffersize)
{
    int i = 0;
    int ret;
    int len;
    
    while(i < buffersize)
    {
        len = (buffersize - i) > MAX_MEMW_SIZE ? MAX_MEMW_SIZE : (buffersize - i);
        ret = cam360_mem_write_4k(codecAddr+i, buffer+i, len);
        if(ret != len) {
            break;
        }
        i += len;
    }

    return(i);
}

int ldm_memr(unsigned int codecAddr, unsigned char *buffer, int buffersize)
{
    int i = 0;
    int ret;
    int len;
    
    while(i < buffersize)
    {
        len = (buffersize - i) > MAX_MEMW_SIZE ? MAX_MEMW_SIZE : (buffersize - i);
        ret = cam360_mem_read_4k(codecAddr+i, buffer+i, len);
        if(ret != len) {
            break;
        }
        i += len;
    }

    return(i);
}

int ldm_init()
{
	int ret=0;

	unsigned int data[4] = {0};
	// read csr registers    
	ret = cam360_qcc_read(QCC_BID_PMU, 0x08, 2, &data[0]);
	ret = cam360_qcc_read(QCC_BID_PMC, 0x14, 1, &data[1]);
	ret = cam360_qcc_read(QCC_BID_PMC, 0x15, 1, &data[2]);
	ret = cam360_qcc_read(QCC_BID_PMC, 0x16, 1, &data[3]);

	YUVUtil_Init2(data[0], data[1], data[2], data[3]);

	int k, l, m;
	for(k=0; k < NUM_MUX_VID_CHANNELS; k++)
	{
		for(l=0; l < NUM_OVERLAY_TEXT_IDX; l++)
		{
			chbidx[k][l] = 0;
		}

		for(m=0; m < NUM_OVERLAY_IMAGE_IDX; m++)
		{
			choidx[k][m] = 0;
		}

	}


	return ret;
}

int ldm_deinit(void){
	int ret = 0;

	return ret;
}


