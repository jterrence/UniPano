/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

/*******************************************************************************
* @(#) $Header: http://bgl-svn.local.geosemi.com/repos/swrepos/users/utkarsh/logo/condorsw/host/tools/f2l/yuvutil.c 52544 2015-02-27 08:34:49Z unimesh $
*******************************************************************************/
//#define DUMP 0
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include "yuvutil.h"
#define LINEAR_FRAME_SIZE (8192*8192)
#define QCC_BID_PMU           0x21      /* Partition Management Unit */
#define QCC_BID_PMC           0x22      /* Physical Memory Ctl. (SDRAM) */

#define clip(x,l,h)     ( ((x)<(l))?(l):(((x)>(h))?(h):(x)) )

#define FULL_TILE      //Always write full tile if defined
                       //If not defined, write half/quarter tile for corner tiles if width/height of image not multiple of tile size
#define PRINTF(str...)      //printf(str)

static int seg_hsize;
static int seg_vsize;
static int tile_hsize;
static int tile_vsize;
static int num_tileh;
static int num_tilev;

static char bInit = 0;

void YUVUtil_Init2(unsigned int reg1, unsigned int reg2, unsigned int reg3, unsigned int reg4)
{
    // constants for seve1
    static const int  VPAT_SXSW =  9;         // Segment     Horizontal Size    <=  512 bytes
    static const int  VPAT_SYSW =  6;         // Segment     Vertical   Size    <=   64 lines

    unsigned int data = 0;
    
    int VPATSegXSize, VPATSegYSize;
    int WordSize;
    int TileXSize, TileYSize;
    int BankXSize, BankYSize; //PageSize;
    int SegXSize, SegYSize;
    
    if (bInit == 0)
    {
        data = reg1;
        VPATSegXSize = data & 0x3;
        VPATSegYSize = (data & 0xc) >> 2;

	    data = reg2;
        WordSize = data & 0x7;

	    data = reg3;
        TileXSize = (data & 0x70) >> 4;
        TileYSize = (data & 0x7);

	    data = reg4;

        BankXSize = (data & 0xc) >> 2;
        BankYSize = (data & 0x3);
        //PageSize = (data & 0x70) >> 4;

        SegXSize = VPAT_SXSW - 3 + VPATSegXSize;
        SegYSize = VPAT_SYSW - 3 + VPATSegYSize;


        SegXSize = VPAT_SXSW - 3 + VPATSegXSize;
        SegYSize = VPAT_SYSW - 3 + VPATSegYSize;
    
        // calculations for SwizzleFrameAddr
        seg_hsize = 1 << SegXSize;
        seg_vsize = 1 << SegYSize;
    
        tile_hsize = 1 << (TileXSize + WordSize);
        tile_vsize = 1 <<  TileYSize;
    
        num_tileh  = 1 <<  BankXSize;
        num_tilev  = 1 <<  BankYSize;
        
        PRINTF( "VPATSegXSize %d VPATSegYSize %d\n", VPATSegXSize, VPATSegYSize );
        PRINTF( "WordSize %d\n", WordSize );
        PRINTF( "TileXSize %d TileYSize %d\n", TileXSize, TileYSize );
        //PRINTF( "BankXSize %d BankYSize %d PageSize %d\n", BankXSize, BankYSize, PageSize );
        PRINTF( "seg_hsize %d seg_vsize %d\n", seg_hsize, seg_vsize );
        PRINTF( "tile_hsize %d tile_vsize %d\n", tile_hsize, tile_vsize );
        PRINTF( "num_tileh %d num_tilev %d\n", num_tileh, num_tilev );

        bInit = 1;
        
    }
}
