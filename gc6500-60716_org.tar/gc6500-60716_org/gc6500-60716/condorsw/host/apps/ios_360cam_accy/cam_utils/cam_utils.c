/******************************************************************************* 
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h> /* memset, memcmp */
#include <unistd.h> /* sleep */
#include <assert.h> /* assert */
#include <string.h>
#include <libusb-1.0/libusb.h>
#include <sys/stat.h>
#include <ctype.h>
#include "qmed.h"
#include "qmedext.h"
#include "qbox.h"
#include "usb_iap.h"

static struct libusb_device_handle *video_hdl = NULL;
static struct libusb_context       *video_ctx = NULL;

#define R2_VID 0x29fe
#define R2_PID 0x4d53

#define USB_DIR_OUT         0       /* to device */
#define USB_DIR_IN          0x80        /* to host */

#define IAP2_BULK_IN_EP 0x02
#define IAP2_BULK_OUT_EP 0x01

#define EAP_BULK_IN_EP 0x03
#define EAP_BULK_OUT_EP 0x04

#define EAP_BULK_OUT_BUF_SIZE 1024*4

#define BULK_OUT_TX_TIMEOUT 2000
#define BULK_IN_RX_TIMEOUT 2000

//iAP2 class specific definations
enum interface_num_t{
    IPCAM_IAP2_INTERFACE_NUM = 0,
    IPCAM_EAP_INTERFACE_NUM = 1,                                                                                                       
    MAX_NUM_INTRFC
};

#define HEXDUMP_COLS 8
void hexdump(void *mem, unsigned int len)
{
        unsigned int i, j;
        
        for(i = 0; i < len + ((len % HEXDUMP_COLS) ? (HEXDUMP_COLS - len % HEXDUMP_COLS) : 0); i++)
        {
                /* print offset */
                if(i % HEXDUMP_COLS == 0)
                {
                        printf("0x%06x: ", i);
                }
 
                /* print hex data */
                if(i < len)
                {
                        printf("%02x ", 0xFF & ((char*)mem)[i]);
                }
                else /* end of block, just aligning for ASCII dump */
                {
                        printf("   ");
                }
                
                /* print ASCII dump */
                if(i % HEXDUMP_COLS == (HEXDUMP_COLS - 1))
                {
                        for(j = i - (HEXDUMP_COLS - 1); j <= i; j++)
                        {
                                if(j >= len) /* end of block, not really printing */
                                {
                                        putchar(' ');
                                }
                                else if(isprint(((char*)mem)[j])) /* printable char */
                                {
                                        putchar(0xFF & ((char*)mem)[j]);        
                                }
                                else /* other char */
                                {
                                        putchar('.');
                                }
                        }
                        putchar('\n');
                }
        }
}

void close_device()                                                                                                                    
{
    if(video_hdl)libusb_close(video_hdl);
    if(video_ctx)libusb_exit(video_ctx);
}

int cam360_chip_reset(void)
{
   ios_video_cmd_t cmd;
   int ret;
   int transferred_len;

   cmd.hdr.id = IOS_ACC_CHIP_RESET;
   cmd.hdr.cmd_len = sizeof(ios_video_cmd_t);
   cmd.data_length = 0;
   cmd.cmd_data = 0;

    printf("cam360_chip_reset\n");
    //send the command
    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)&cmd,
                             sizeof(ios_video_cmd_t),
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for chip reset failed %d\n",ret);
        return -1;
    }

    return 0;
}


int cam360_set_jsonid(json_id_t id)
{
    ios_jsonid_cmd_t cmd;
    int ret;
    int transferred_len;

    cmd.hdr.id = IOS_ACC_SET_JSON_ID;
    cmd.hdr.cmd_len = sizeof(ios_jsonid_cmd_t);
    cmd.json_id = id;

    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)&cmd,
                             sizeof(ios_jsonid_cmd_t),
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for set_jsonid failed %d\n",ret);
        return -1;
    }

    return 0;
}

int cam360_get_camera_state(void)
{
    ios_cmd_hdr_t cmd;
    int ret;
    int transferred_len;
    char data[512];
    ios_cam_state_rsp_t *state_rsp;
    cmd_rsp_hdr_t *hdr;

    cmd.id = IOS_ACC_GET_CAMERA_STATE;
    cmd.cmd_len = sizeof(ios_cmd_hdr_t);
    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)&cmd,
                             sizeof(ios_cmd_hdr_t),
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for get_caminit_status failed %d\n",ret);
        return -1;
    }    
    //wait for camera to become ready
    usleep(1000*300);
    do{
        ret = libusb_bulk_transfer(video_hdl,
                                 (USB_DIR_IN + EAP_BULK_IN_EP),
                                 (unsigned char *)data,
                                 512,
                                 &transferred_len,
                                 2000
                                );
        if(ret){
            printf("ERR: libusb_bulk_transfer in for cam360_get_camera_state failed %d\n",ret);
            return -1;
        }
        hdr = (cmd_rsp_hdr_t *)data;
        if (hdr->packet_type != CMDR_TYPE){
            printf("its not a command response\n");
            return 1;
        }
        state_rsp = (ios_cam_state_rsp_t *)(data + sizeof(cmd_rsp_hdr_t));
        //wait for a while
        //printf("cam_state %d\n",*cam_state);
        if(state_rsp->hdr.id == IOS_ACC_GET_CAMERA_STATE){
            if(state_rsp->state == CAMERA_INPROGRESS_STATE){
                usleep(1000*100);

                cmd.id = IOS_ACC_GET_CAMERA_STATE;
                cmd.cmd_len = sizeof(ios_cmd_hdr_t);
                ret = libusb_bulk_transfer(video_hdl,
                                 (USB_DIR_OUT + EAP_BULK_OUT_EP),
                                 (unsigned char *)&cmd,
                                 sizeof(ios_cmd_hdr_t),
                                 &transferred_len,
                                 2000
                                );
                if(ret){
                    printf("ERR: libusb_bulk_transfer out for get_caminit_status failed %d\n",ret);
                    return -1;
                }
            }
        }else{
            printf("ERROR: its not a COMMAND STATE RESPONSE\n");
        }
    }while(state_rsp->state == CAMERA_INPROGRESS_STATE);

    if(state_rsp->state == CAMERA_ERROR_STATE){
        printf("cam init failed\n");
        return -1;
    }
    if(state_rsp->state == CAMERA_READY_STATE){
        printf("ready\n");
        return 0;
    }

    return 1;
}

int cam360_qcc_read(uint16_t bid, uint16_t addr, uint16_t length, uint32_t *value)
{
	int r;
	int mask;
    ios_cam_qcc_rw_t *qccr;
    int transferred_len;
    char data[512];
    cmd_rsp_hdr_t *hdr;

	//PRINTF("%s (IN)\n",__func__);

	switch (length) {
		case 1:
		mask = 0xFF;
		break;
		case 2:
		mask = 0xFFFF;
		break;
		case 4:
		mask = 0xFFFFFFFF;
		break;
		default:
		return -1;
	}

    qccr = (ios_cam_qcc_rw_t *)data;
    qccr->hdr.id = IOS_ACC_QCC_READ;
    qccr->hdr.cmd_len = sizeof(ios_cam_qcc_rw_t);
    qccr->qcc_addr = addr;
    qccr->qcc_bid = bid;
    qccr->length = length;

	r = libusb_bulk_transfer(video_hdl,
                            (USB_DIR_OUT + EAP_BULK_OUT_EP),
                            (unsigned char *)qccr,
                            qccr->hdr.cmd_len,
                            &transferred_len,
                            BULK_OUT_TX_TIMEOUT);
	if (r) {
		printf("Failed IOS_ACC_QCC_READ %d\n", r);
		return r;
	}

    //wait for response
    r = libusb_bulk_transfer(video_hdl,
                                 (USB_DIR_IN + EAP_BULK_IN_EP),
                                 (unsigned char *)data,
                                 512,
                                 &transferred_len,
                                 BULK_IN_RX_TIMEOUT
                                );
    if(r){
        printf("ERR: libusb_bulk_transfer in for ldm_qcc_read failed %d\n",r);
        return -1;
    }
    hdr = (cmd_rsp_hdr_t *)data;
    if (hdr->packet_type != CMDR_TYPE){
        printf("ERR: ldm_qcc_read() its not a command response\n");
        return -1;
    }
    qccr = (ios_cam_qcc_rw_t *)(data+sizeof(cmd_rsp_hdr_t));
    if(qccr->hdr.id != IOS_ACC_QCC_READ){
        printf("ERR: ldm_qcc_read() its not a qcc read command response\n");
        return -1;
    }
    *value = (qccr->value & mask);
	//*value &= mask;
	return 0;
}

int cam360_qcc_write(uint16_t bid, uint16_t addr, uint16_t length, uint32_t value)
{
	int r;
    ios_cam_qcc_rw_t *qccw;
    int transferred_len;
    char data[512];

	//PRINTF("%s (IN)\n",__func__);
    switch (length) {
		case 1:
		value &= 0xFF;
		break;
		case 2:
		value &= 0xFFFF;
		break;
		case 4:
		break;
		default:
		return -1;
	}

    qccw = (ios_cam_qcc_rw_t *)data;
    qccw->hdr.id = IOS_ACC_QCC_WRITE;
    qccw->hdr.cmd_len = sizeof(ios_cam_qcc_rw_t);
    qccw->qcc_addr = addr;
    qccw->qcc_bid = bid;
    qccw->length = length;
    qccw->value = value;

	r = libusb_bulk_transfer(video_hdl,
                            (USB_DIR_OUT + EAP_BULK_OUT_EP),
                            (unsigned char *)qccw,
                            qccw->hdr.cmd_len,
                            &transferred_len,
                            BULK_OUT_TX_TIMEOUT);
	if (r) {
		printf("Failed IOS_ACC_QCC_WRITE %d\n", r);
		return r;
	}

    return 0;
}

int cam360_mem_read_4k(uint32_t address, uint8_t *buffer, uint32_t buffersize)
{
    ios_cam_mem_rw_t memr;
    cmd_rsp_hdr_t *hdr;
    uint8_t *tmp_buf;
    int transferred_len;
    int r;

    memr.hdr.id = IOS_ACC_MEM_READ;
    memr.hdr.cmd_len = sizeof(ios_cam_mem_rw_t);
    memr.mem_addr = address;
    memr.data_len = buffersize;
    //printf("memr.data_len %d\n",memr.data_len);
    r = libusb_bulk_transfer(video_hdl,
                    (USB_DIR_OUT + EAP_BULK_OUT_EP),
                    (unsigned char *)&memr,
                    memr.hdr.cmd_len,
                    &transferred_len,
                    BULK_OUT_TX_TIMEOUT);
    if (r) {
        printf("Failed IOS_ACC_MEM_READ Bulk out %d\n", r);
        return r;
    }

    tmp_buf = (uint8_t *)malloc(buffersize+sizeof(cmd_rsp_hdr_t)
                                +sizeof(ios_cam_mem_rw_t));
    
    //read the memory
    r = libusb_bulk_transfer(video_hdl,
                    (USB_DIR_IN + EAP_BULK_IN_EP),
                    (unsigned char *)tmp_buf,
                    (buffersize+sizeof(cmd_rsp_hdr_t)+sizeof(ios_cam_mem_rw_t)),
                    &transferred_len,
                    BULK_IN_RX_TIMEOUT);
    if (r) {
		printf("Failed IOS_ACC_MEM_READ bulk in %d\n", r);
		return r;
	}

    hdr = (cmd_rsp_hdr_t *)tmp_buf;
    if (hdr->packet_type != CMDR_TYPE){
        printf("ERR: mem_read() its not a command response\n");
        free(tmp_buf);
        return -1;
    }
    ios_cam_mem_rw_t *mem_read;
    mem_read = (ios_cam_mem_rw_t *)(tmp_buf+sizeof(cmd_rsp_hdr_t));
    if(mem_read->hdr.id != IOS_ACC_MEM_READ){
        printf("ERR: mem_read() its not a IOS_ACC_MEM_READ response\n");
        free(tmp_buf);
        return 1;
    }
    memcpy(buffer, 
           (uint8_t *)(tmp_buf+sizeof(cmd_rsp_hdr_t)+sizeof(ios_cam_mem_rw_t)), 
           buffersize);
    free(tmp_buf);
    return 0;    
}

int cam360_mem_write_4k(uint32_t address, uint8_t *buffer, uint32_t buffersize)
{
    ios_cam_mem_rw_t *memr;
    uint8_t *tmp_buf=NULL;
    int transferred_len;
    int r;

    if(buffersize > (EAP_BULK_OUT_BUF_SIZE-sizeof(ios_cam_mem_rw_t))){
        printf("ERROR: Can send %dBytes of memory only\n",
                (EAP_BULK_OUT_BUF_SIZE-sizeof(ios_cam_mem_rw_t)));
        return -1;
    }

    tmp_buf = (uint8_t *)malloc(buffersize
                                +sizeof(ios_cam_mem_rw_t));
    assert(tmp_buf);

    memr=(ios_cam_mem_rw_t *)tmp_buf;

    memr->hdr.id = IOS_ACC_MEM_WRITE;
    memr->hdr.cmd_len = sizeof(ios_cam_mem_rw_t)+buffersize;
    memr->mem_addr = address;
    memr->data_len = buffersize;    
    
    memcpy(tmp_buf+sizeof(ios_cam_mem_rw_t), buffer, buffersize);

    r = libusb_bulk_transfer(video_hdl,
                    (USB_DIR_OUT + EAP_BULK_OUT_EP),
                    (unsigned char *)tmp_buf,
                    memr->hdr.cmd_len,
                    &transferred_len,
                    BULK_OUT_TX_TIMEOUT);
    if (r) {
        printf("Failed IOS_ACC_MEM_WRITE %d\n", r);
        free(tmp_buf);
        return r;
    }
    free(tmp_buf);

    return 0;
}

void help_print(void)
{
    printf("Supported Commands:\n");
    printf("reset   - reset camera\n");
    printf("setjson <json-id> - send a json-id(1/2) for camera to boot up\n");
    printf("qccr <bid> <addr> <length> - read qcc register, bid & addr should be in hex\n");
    printf("\t length should be in integer\n");
    printf("qccw <bid> <addr> <len> <value> - write qcc register, bid, addr & value should be in hex\n");
    printf("\t length should be in integer\n");
    printf("memw <mem-address> <file> - memory write to <mem-address> from file, mem-address should be in hex\n");
    printf("memr <mem-address> <size> - memory read and dump in console, mem-address should be in hex\n");

}

int main(int argc, char **argv)
{
    uint16_t vendor_id = R2_VID, product_id = R2_PID;
    int ret;

    if(argc < 2 || (strcmp(argv[1], "help")==0) ||
        (strcmp(argv[1], "reset") && strcmp(argv[1], "setjson")
        && strcmp(argv[1], "qccr") && strcmp(argv[1], "qccw") &&
        strcmp(argv[1], "memw") && strcmp(argv[1], "memr"))){
        help_print();
        return 1;
    }

    ret = libusb_init(&video_ctx);
    if(ret < 0){
        printf("init_libusb failed\n");
        return -1;
    }

    video_hdl = libusb_open_device_with_vid_pid(video_ctx, vendor_id,
                            product_id);
    
    if(video_hdl == NULL){
        printf("Could not open USB device %x:%x\n", vendor_id, product_id);
        goto error;
    }
    
    ret = libusb_claim_interface(video_hdl, IPCAM_EAP_INTERFACE_NUM);
    if(ret != 0){
        printf("libusb_claim_interface error %d\n",ret);
        printf("Please connect a GC6500 running in ios acc mode\n");
        goto error;
    }

    //set interface to activte the endpoints
    ret = libusb_set_interface_alt_setting( video_hdl, IPCAM_EAP_INTERFACE_NUM, 1);
    if(ret != 0){
        printf("libusb_set_interface_alt_setting error %d\n",ret);
        goto error;
        return -1;
    }
   
    if(strcmp(argv[1], "reset")==0){
        cam360_chip_reset();
        close_device();
        return 0;
    }else if(strcmp(argv[1], "setjson")==0){
        if((argv[2] != NULL) &&
            (atoi(argv[2]) != 1 && atoi(argv[2]) != 2)){
            printf("ERROR: supported json-id 1/2\n");
            goto error;
        }
        cam360_set_jsonid(atoi(argv[2]));
        ret = cam360_get_camera_state();
        if(ret == 0){
            printf("camera is ready for streaming\n");
        }
    }else if(strcmp(argv[1], "qccr")== 0){
        uint32_t qcc_val;
        uint16_t bid = (uint16_t)strtol(argv[2], NULL, 16);
        uint16_t addr = (uint16_t)strtol(argv[3], NULL, 16);
        uint16_t length = (uint16_t)strtol(argv[4], NULL, 16);
        if((length != 1) && (length != 2) && (length != 4)){
            printf("ERR: supported lengths are 1, 2 or 4\n");
            goto error;
        }
        printf("bid 0x%x addr 0x%x length %d\n",bid, addr, length);
        cam360_qcc_read(bid, addr, length, &qcc_val);
        printf("qcc value 0x%x\n",qcc_val);
    }else if(strcmp(argv[1], "qccw")== 0){
        uint16_t bid = (uint16_t)strtol(argv[2], NULL, 16);
        uint16_t addr = (uint16_t)strtol(argv[3], NULL, 16);
        uint16_t length = (uint16_t)strtol(argv[4], NULL, 16);
        uint32_t qcc_val = (uint16_t)strtol(argv[5], NULL, 16);

        cam360_qcc_write(bid, addr, length, qcc_val);
    }else if(strcmp(argv[1], "memw")== 0){
        struct stat stfile;
        char *data;
        int read_size;
        FILE *fin;
        if(argc < 4){
            help_print();
            goto error;
        }
        if(stat(argv[3], &stfile)) {
            printf("Error: File %s not found\n", argv[3]);
            goto error;        
        }
#define MAX_MEMW_SIZE (4*1024-16) //4K-sizeof(ios_cam_mem_rw_t)
        fin = fopen(argv[3], "rb");

        read_size = (stfile.st_size>MAX_MEMW_SIZE) ? 
                    MAX_MEMW_SIZE : stfile.st_size;
        data = malloc(read_size);
        fread((void*)data, read_size, 1, fin);

        cam360_mem_write_4k((uint32_t)strtol(argv[2], NULL, 16), (uint8_t *)data, read_size);

        fclose(fin);
        free(data);
    }else if(strcmp(argv[1], "memr")== 0){
        char *datam;
        int read_size = 0;

        if(argc < 4){
            help_print();
            goto error;
        }
        
        read_size = ((atoi(argv[3]))>MAX_MEMW_SIZE) ? MAX_MEMW_SIZE : (atoi(argv[3]));
        datam = malloc(read_size);
        cam360_mem_read_4k((uint32_t)strtol(argv[2], NULL, 16), (uint8_t *)datam, read_size);
        //while(printf("");
        //printf("read_size %d\n",read_size);
        hexdump((void *)datam, read_size);
        free(datam);
    }

error:
    if(video_hdl)
        ret = libusb_release_interface(video_hdl, IPCAM_EAP_INTERFACE_NUM);
    close_device();

    return 0;
}
