/******************************************************************************* 
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h> /* memset, memcmp */
#include <unistd.h> /* sleep */
#include <assert.h> /* assert */
#include <string.h>
#include <libusb-1.0/libusb.h>
#include <sys/stat.h>
#include "qmed.h"
#include "qmedext.h"
#include "qbox.h"
#include "usb_iap.h"

static struct libusb_device_handle *video_hdl = NULL;
static struct libusb_context       *video_ctx = NULL;

#define R2_VID 0x29fe
#define R2_PID 0x4d53

#define USB_DIR_OUT         0       /* to device */
#define USB_DIR_IN          0x80        /* to host */

#define IAP2_BULK_IN_EP 0x02
#define IAP2_BULK_OUT_EP 0x01

#define EAP_BULK_IN_EP 0x03
#define EAP_BULK_OUT_EP 0x04

#define EAP_BULK_OUT_BUF_SIZE 1024*4

//iAP2 class specific definations
enum interface_num_t{
    IPCAM_IAP2_INTERFACE_NUM = 0,
    IPCAM_EAP_INTERFACE_NUM = 1,                                                                                                       
    MAX_NUM_INTRFC
};

typedef enum{
    TEST_STATE_UNDEFINED=0x2,
    TEST_STATE_IN_PROGRESS,
    TEST_STATE_COMPLETE,
    TEST_STATE_NOT_RUN
}test_state_t;

typedef enum{
    TEST_RESULT_UNDEFINED = 0x2,
    TEST_RESULT_SUCCESS,
    TEST_RESULT_FAIL,
}test_result_t;

//test result of all the individual components will be stored in this structure.
typedef struct{
    test_state_t     status;
    test_result_t    apple_cp;
    test_result_t    bat_fuel_gauge;
    test_result_t    fpga;
    /*test_result_t    ddr;
    test_result_t    snor_flash */
}__attribute__((__packed__))self_test_result_t;


void close_device()                                                                                                                    
{
    if(video_hdl)libusb_close(video_hdl);
    if(video_ctx)libusb_exit(video_ctx);
}

int cam360_get_fw_version(void)
{
   ios_system_cmd_t cmd;
   int ret;
   int transferred_len;
	char data[512];
	char sys_data[32];
	cmd_rsp_hdr_t *hdr;
	//uint8_t reg_value;

	cmd.hdr.id = IOS_ACC_SYSTEM_COMMANDS;
	cmd.hdr.cmd_len = sizeof(ios_system_cmd_t);
	printf("cmd_len = %d\n", sizeof(ios_system_cmd_t));
	cmd.scmd = IOS_ACC_GET_FW_INFO;
	cmd.data_length = 0;

    printf("cam360_get_fw_version\n");


    //send the command
    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)&cmd,
                             (sizeof(ios_system_cmd_t)),
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for chip reset failed %d\n",ret);
        return -1;
    }

	//get the value
	ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_IN + EAP_BULK_IN_EP),
                             (unsigned char *)data,
                             512,
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer in for get_bitrate failed %d\n",ret);
        return -1;
    }

	hdr = (cmd_rsp_hdr_t *)data;
	if(hdr->packet_type != CMDR_TYPE){
		printf("it's not a command response\n");
		return 1;
	}
	ios_cmd_id_t *cmd_type = (ios_cmd_id_t *)(data + sizeof(cmd_rsp_hdr_t));
	if(*cmd_type == IOS_ACC_SYSTEM_COMMANDS){
		ios_system_cmd_t *cmdp = (ios_system_cmd_t *)(data + sizeof(cmd_rsp_hdr_t));
		if(cmdp->scmd == IOS_ACC_GET_FW_INFO){
			printf("sys_data len = %d\n", cmdp->data_length);
			memcpy(sys_data, data+(sizeof(cmd_rsp_hdr_t)+sizeof(ios_system_cmd_t)), cmdp->data_length);
			sys_data[cmdp->data_length] = '\0';
			printf("SYS READ VALUE = %s\n", sys_data);
		}else{
			printf("Invalid response value\n");
		}
	}

    return 0;
}

//set a specific gpio to the desired value
int cam360_gpio_set(char *arg_gpio_pin, char *arg_gpio_val){

	ios_system_cmd_t cmd;
   int ret;
   int transferred_len;
	unsigned char cmd_buffer[128];	//easy way

	gpio_type_t gpio_cmd;
	
	//convert arg first
	unsigned short gpio_pin = (unsigned short)atoi(arg_gpio_pin);
	unsigned short gpio_val = (unsigned short)atoi(arg_gpio_val);

	printf("cam360_gpio_set : \n");
	printf("\tgpio_pin = %d\n", gpio_pin);
	printf("\tgpio_val = %d\n", gpio_val);

	gpio_cmd.gpio_pin = gpio_pin;
	gpio_cmd.gpio_val = gpio_val;

	cmd.hdr.id = IOS_ACC_SYSTEM_COMMANDS;
	cmd.hdr.cmd_len = sizeof(ios_system_cmd_t)+sizeof(gpio_type_t);
	printf("cmd_len = %d\n", sizeof(ios_system_cmd_t));
	cmd.scmd = IOS_ACC_GPIO_SET;
	cmd.data_length = sizeof(gpio_type_t);
	
	memcpy(cmd_buffer, (unsigned char *)&cmd, sizeof(ios_system_cmd_t));
	memcpy(cmd_buffer+sizeof(ios_system_cmd_t), (unsigned char *)&gpio_cmd, sizeof(gpio_cmd));

	//send the command
    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             cmd_buffer,
                             cmd.hdr.cmd_len,
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for chip reset failed %d\n",ret);
        return -1;
    }

	return 0;
}

//get the state of a specific gpio pin
int cam360_gpio_get(char *arg_gpio_pin){
	char data[512];
	cmd_rsp_hdr_t *hdr;

	ios_system_cmd_t cmd;
   int ret;
   int transferred_len;
	unsigned char cmd_buffer[128];	//easy way

	gpio_type_t gpio_cmd;
	
	//convert arg first
	unsigned short gpio_pin = (unsigned short)atoi(arg_gpio_pin);

	printf("cam360_gpio_get : \n");
	printf("\tgpio_pin = %d\n", gpio_pin);

	gpio_cmd.gpio_pin = gpio_pin;
	gpio_cmd.gpio_val = 0;

	cmd.hdr.id = IOS_ACC_SYSTEM_COMMANDS;
	cmd.hdr.cmd_len = sizeof(ios_system_cmd_t)+sizeof(gpio_type_t);
	printf("cmd_len = %d\n", sizeof(ios_system_cmd_t));
	cmd.scmd = IOS_ACC_GPIO_GET;
	cmd.data_length = sizeof(gpio_type_t);
	
	memcpy(cmd_buffer, (unsigned char *)&cmd, sizeof(ios_system_cmd_t));
	memcpy(cmd_buffer+sizeof(ios_system_cmd_t), (unsigned char *)&gpio_cmd, sizeof(gpio_cmd));

	//send the command
    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             cmd_buffer,
                             cmd.hdr.cmd_len,
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for chip reset failed %d\n",ret);
        return -1;
    }


	//get the value
	ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_IN + EAP_BULK_IN_EP),
                             (unsigned char *)data,
                             512,
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer in for get_bitrate failed %d\n",ret);
        return -1;
    }

	hdr = (cmd_rsp_hdr_t *)data;
	if(hdr->packet_type != CMDR_TYPE){
		printf("it's not a command response\n");
		return 1;
	}
	ios_cmd_id_t *cmd_type = (ios_cmd_id_t *)(data + sizeof(cmd_rsp_hdr_t));
	if(*cmd_type == IOS_ACC_SYSTEM_COMMANDS){
		ios_system_cmd_t *cmdp = (ios_system_cmd_t *)(data + sizeof(cmd_rsp_hdr_t));
		if(cmdp->scmd == IOS_ACC_GPIO_GET){
			gpio_type_t *gpio_cmd = (gpio_type_t *)(data+sizeof(cmd_rsp_hdr_t)+sizeof(ios_system_cmd_t));
			printf("gpio pin = %u --> %u\n", gpio_cmd->gpio_pin, gpio_cmd->gpio_val);
		}else{
			printf("Invalid response value\n");
		}
	}



	return 0;
}

//cam 360 FPGA i2c generic write
int cam360_FPGA_i2c_write(void){
	ios_system_cmd_t cmd;
   int ret;
   int transferred_len;
	uint8_t cmd_buff[128];
	uint8_t i2c_buff[1] = {0x00};
	uint8_t data[512];
	//char i2c_read_len = 0;
	cmd_rsp_hdr_t *hdr;

	i2c_type_t i2c_cmd;
	
	//fill i2c_type
	i2c_cmd.reg_addr = 0x00;
	i2c_cmd.buff_len = 1;

	//fill command header
	cmd.hdr.id = IOS_ACC_SYSTEM_COMMANDS;
	cmd.hdr.cmd_len = sizeof(ios_system_cmd_t)+sizeof(i2c_type_t)+1;

	cmd.scmd = IOS_ACC_FPGA_I2C_WRITE;
	cmd.data_length = sizeof(i2c_type_t);
	printf("cmd lenght = %d\n", cmd.data_length);
	
	memcpy(cmd_buff, &cmd, sizeof(ios_system_cmd_t));
	memcpy(cmd_buff+sizeof(ios_system_cmd_t), &i2c_cmd, sizeof(i2c_type_t));
	memcpy(cmd_buff+sizeof(ios_system_cmd_t)+sizeof(i2c_type_t), i2c_buff, 1);

	//send the command
    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)cmd_buff,
                             cmd.hdr.cmd_len,
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for chip reset failed %d\n",ret);
        return -1;
    }

	//get the value
	ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_IN + EAP_BULK_IN_EP),
                             (unsigned char *)data,
                             512,
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer in for get_bitrate failed %d\n",ret);
        return -1;
    }

	hdr = (cmd_rsp_hdr_t *)data;
	if(hdr->packet_type != CMDR_TYPE){
		printf("it's not a command response\n");
		return 1;
	}
	ios_cmd_id_t *cmd_type = (ios_cmd_id_t *)(data + sizeof(cmd_rsp_hdr_t));
	if(*cmd_type == IOS_ACC_SYSTEM_COMMANDS){
		ios_system_cmd_t *cmdp = (ios_system_cmd_t *)(data + sizeof(cmd_rsp_hdr_t));
		if(cmdp->scmd == IOS_ACC_FPGA_I2C_WRITE){
			i2c_type_t *i2c_response = (i2c_type_t *)(data+sizeof(cmd_rsp_hdr_t)+sizeof(ios_system_cmd_t));
			printf("i2c response len = %d\n", i2c_response->buff_len);
			
		}else{
			printf("Invalid response value\n");
		}
	}
	return 0;
}

//cam360 FPGA i2c read
int cam360_FPGA_i2c_read(void){

	ios_system_cmd_t cmd;
   int ret;
   int transferred_len;
	uint8_t cmd_buff[128];
	uint8_t *i2c_buff;
	uint8_t data[512];
	//char i2c_read_len = 0;
	cmd_rsp_hdr_t *hdr;
	uint8_t i;

	i2c_type_t i2c_cmd;
	
	//fill i2c_type
	i2c_cmd.reg_addr = 0x13;
	i2c_cmd.buff_len = 1;

	//fill command header
	cmd.hdr.id = IOS_ACC_SYSTEM_COMMANDS;
	cmd.hdr.cmd_len = sizeof(ios_system_cmd_t)+sizeof(i2c_type_t);

	cmd.scmd = IOS_ACC_FPGA_I2C_READ;
	cmd.data_length = sizeof(i2c_type_t);
	printf("cmd lenght = %d\n", cmd.data_length);
	
	memcpy(cmd_buff, &cmd, sizeof(ios_system_cmd_t));
	memcpy(cmd_buff+sizeof(ios_system_cmd_t), &i2c_cmd, sizeof(i2c_type_t));

	//send the command
    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)cmd_buff,
                             cmd.hdr.cmd_len,
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for chip reset failed %d\n",ret);
        return -1;
    }

	//get the value
	ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_IN + EAP_BULK_IN_EP),
                             (unsigned char *)data,
                             512,
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer in for get_bitrate failed %d\n",ret);
        return -1;
    }

	hdr = (cmd_rsp_hdr_t *)data;
	if(hdr->packet_type != CMDR_TYPE){
		printf("it's not a command response\n");
		return 1;
	}
	ios_cmd_id_t *cmd_type = (ios_cmd_id_t *)(data + sizeof(cmd_rsp_hdr_t));
	if(*cmd_type == IOS_ACC_SYSTEM_COMMANDS){
		ios_system_cmd_t *cmdp = (ios_system_cmd_t *)(data + sizeof(cmd_rsp_hdr_t));
		if(cmdp->scmd == IOS_ACC_FPGA_I2C_READ){
			i2c_type_t *i2c_response = (i2c_type_t *)(data+sizeof(cmd_rsp_hdr_t)+sizeof(ios_system_cmd_t));
			printf("i2c response len = %d\n", i2c_response->buff_len);
			if(i2c_response->buff_len){
				i2c_buff = (uint8_t *)(data+sizeof(cmd_rsp_hdr_t)+sizeof(ios_system_cmd_t)+sizeof(i2c_type_t));
				for(i = 0; i < i2c_response->buff_len; i++){
					printf("0x%02X\n", *(i2c_buff + i));
				}
			}
		}else{
			printf("Invalid response value\n");
		}
	}
	return 0;
}

//cam360 get SN from sensors
int cam360_get_SN(void){
	ios_system_cmd_t cmd;
   int ret;
   int transferred_len;
	char data[512];
	char *SN;
	cmd_rsp_hdr_t *hdr;
	//uint8_t reg_value;

	cmd.hdr.id = IOS_ACC_SYSTEM_COMMANDS;
	cmd.hdr.cmd_len = sizeof(ios_system_cmd_t);
	printf("cmd_len = %d\n", sizeof(ios_system_cmd_t));
	cmd.scmd = IOS_ACC_FPGA_GET_SN;
	cmd.data_length = 0;

    printf("cam360_get_SN\n");


    //send the command
    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)&cmd,
                             (sizeof(ios_system_cmd_t)),
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for chip reset failed %d\n",ret);
        return -1;
    }

	//get the value
	ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_IN + EAP_BULK_IN_EP),
                             (unsigned char *)data,
                             512,
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer in for get_bitrate failed %d\n",ret);
        return -1;
    }

	hdr = (cmd_rsp_hdr_t *)data;
	if(hdr->packet_type != CMDR_TYPE){
		printf("it's not a command response\n");
		return 1;
	}
	ios_cmd_id_t *cmd_type = (ios_cmd_id_t *)(data + sizeof(cmd_rsp_hdr_t));
	if(*cmd_type == IOS_ACC_SYSTEM_COMMANDS){
		ios_system_cmd_t *cmdp = (ios_system_cmd_t *)(data + sizeof(cmd_rsp_hdr_t));
		if(cmdp->scmd == IOS_ACC_FPGA_GET_SN){
			SN = (char *)(data + sizeof(cmd_rsp_hdr_t) + sizeof(ios_system_cmd_t));
			printf("SN = %s\n", SN);
			printf("SN len = %d\n", strlen(SN));
		}else{
			printf("Invalid response value\n");
		}
	}

    return 0;
}

//cam360 FPGA get system status
int cam360_FPGA_get_system_status(void){
	ios_system_cmd_t cmd;
   int ret;
   int transferred_len;
	char data[512];
	cmd_rsp_hdr_t *hdr;
	uint8_t *i2c_buff;
	uint8_t i;

	cmd.hdr.id = IOS_ACC_SYSTEM_COMMANDS;
	cmd.hdr.cmd_len = sizeof(ios_system_cmd_t);
	printf("cmd_len = %d\n", sizeof(ios_system_cmd_t));
	cmd.scmd = IOS_ACC_FPGA_GET_SYSTEM_STATUS;
	cmd.data_length = 0;

    printf("cam360 FPGA get system status\n");


    //send the command
    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)&cmd,
                             (sizeof(ios_system_cmd_t)),
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for chip reset failed %d\n",ret);
        return -1;
    }

	//get the value
	ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_IN + EAP_BULK_IN_EP),
                             (unsigned char *)data,
                             512,
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer in for get_bitrate failed %d\n",ret);
        return -1;
    }

	hdr = (cmd_rsp_hdr_t *)data;
	if(hdr->packet_type != CMDR_TYPE){
		printf("it's not a command response\n");
		return 1;
	}
	ios_cmd_id_t *cmd_type = (ios_cmd_id_t *)(data + sizeof(cmd_rsp_hdr_t));
	if(*cmd_type == IOS_ACC_SYSTEM_COMMANDS){
		ios_system_cmd_t *cmdp = (ios_system_cmd_t *)(data + sizeof(cmd_rsp_hdr_t));
		if(cmdp->scmd == IOS_ACC_FPGA_GET_SYSTEM_STATUS){
				i2c_buff = (uint8_t *)(data + sizeof(cmd_rsp_hdr_t) + sizeof(ios_system_cmd_t));
				if(cmdp->data_length == 3){
					printf("response length = 3\n");
					for(i = 0; i < 3; i++){
						printf("0x%02X\n", *(i2c_buff + i));
					}
				}else{
					printf("incorrect response length for get system status\n");
				}
		}else{
			printf("Invalid response value\n");
		}
	}
    return 0;
}

//cam360 FPGA get system param
int cam360_FPGA_get_system_param(void){
	ios_system_cmd_t cmd;
   int ret;
   int transferred_len;
	char data[512];
	cmd_rsp_hdr_t *hdr;
	uint8_t *i2c_buff;
	uint8_t i;

	cmd.hdr.id = IOS_ACC_SYSTEM_COMMANDS;
	cmd.hdr.cmd_len = sizeof(ios_system_cmd_t);
	printf("cmd_len = %d\n", sizeof(ios_system_cmd_t));
	cmd.scmd = IOS_ACC_FPGA_GET_SYSTEM_PARAM;
	cmd.data_length = 0;

    printf("cam360 FPGA get system param\n");


    //send the command
    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)&cmd,
                             (sizeof(ios_system_cmd_t)),
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for chip reset failed %d\n",ret);
        return -1;
    }

	//get the value
	ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_IN + EAP_BULK_IN_EP),
                             (unsigned char *)data,
                             512,
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer in for get_bitrate failed %d\n",ret);
        return -1;
    }

	hdr = (cmd_rsp_hdr_t *)data;
	if(hdr->packet_type != CMDR_TYPE){
		printf("it's not a command response\n");
		return 1;
	}
	ios_cmd_id_t *cmd_type = (ios_cmd_id_t *)(data + sizeof(cmd_rsp_hdr_t));
	if(*cmd_type == IOS_ACC_SYSTEM_COMMANDS){
		ios_system_cmd_t *cmdp = (ios_system_cmd_t *)(data + sizeof(cmd_rsp_hdr_t));
		if(cmdp->scmd == IOS_ACC_FPGA_GET_SYSTEM_PARAM){
				i2c_buff = (uint8_t *)(data + sizeof(cmd_rsp_hdr_t) + sizeof(ios_system_cmd_t));
				if(cmdp->data_length == 58){
					printf("response length = 58\n");
					for(i = 0; i < 58; i++){
						printf("0x%02X\n", *(i2c_buff + i));
					}
				}else{
					printf("incorrect response length for get system param\n");
				}
		}else{
			printf("Invalid response value\n");
		}
	}
    return 0;
}

//cam360 FPGA get command
int cam360_FPGA_get_command(void){
	ios_system_cmd_t cmd;
   int ret;
   int transferred_len;
	char data[512];
	cmd_rsp_hdr_t *hdr;
	uint8_t *i2c_buff;
	uint8_t i;

	cmd.hdr.id = IOS_ACC_SYSTEM_COMMANDS;
	cmd.hdr.cmd_len = sizeof(ios_system_cmd_t);
	printf("cmd_len = %d\n", sizeof(ios_system_cmd_t));
	cmd.scmd = IOS_ACC_FPGA_GET_COMMAND;
	cmd.data_length = 0;

    printf("cam360 FPGA get command\n");


    //send the command
    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)&cmd,
                             (sizeof(ios_system_cmd_t)),
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for chip reset failed %d\n",ret);
        return -1;
    }

	//get the value
	ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_IN + EAP_BULK_IN_EP),
                             (unsigned char *)data,
                             512,
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer in for get_bitrate failed %d\n",ret);
        return -1;
    }

	hdr = (cmd_rsp_hdr_t *)data;
	if(hdr->packet_type != CMDR_TYPE){
		printf("it's not a command response\n");
		return 1;
	}
	ios_cmd_id_t *cmd_type = (ios_cmd_id_t *)(data + sizeof(cmd_rsp_hdr_t));
	if(*cmd_type == IOS_ACC_SYSTEM_COMMANDS){
		ios_system_cmd_t *cmdp = (ios_system_cmd_t *)(data + sizeof(cmd_rsp_hdr_t));
		if(cmdp->scmd == IOS_ACC_FPGA_GET_COMMAND){
				i2c_buff = (uint8_t *)(data + sizeof(cmd_rsp_hdr_t) + sizeof(ios_system_cmd_t));
				if(cmdp->data_length == 1){
					printf("response length = 1\n");
					for(i = 0; i < 1; i++){
						printf("0x%02X\n", *(i2c_buff + i));
					}
				}else{
					printf("incorrect response length for get FPGA command\n");
				}
		}else{
			printf("Invalid response value\n");
		}
	}
    return 0;
}

//cam360 FPGA get system version
int cam360_FPGA_get_version(void){
	ios_system_cmd_t cmd;
   int ret;
   int transferred_len;
	char data[512];
	cmd_rsp_hdr_t *hdr;
	uint8_t *i2c_buff;
	uint8_t i;

	cmd.hdr.id = IOS_ACC_SYSTEM_COMMANDS;
	cmd.hdr.cmd_len = sizeof(ios_system_cmd_t);
	printf("cmd_len = %d\n", sizeof(ios_system_cmd_t));
	cmd.scmd = IOS_ACC_FPGA_GET_VERSION;
	cmd.data_length = 0;

    printf("cam360 FPGA get version\n");


    //send the command
    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)&cmd,
                             (sizeof(ios_system_cmd_t)),
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for chip reset failed %d\n",ret);
        return -1;
    }

	//get the value
	ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_IN + EAP_BULK_IN_EP),
                             (unsigned char *)data,
                             512,
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer in for get_bitrate failed %d\n",ret);
        return -1;
    }

	hdr = (cmd_rsp_hdr_t *)data;
	if(hdr->packet_type != CMDR_TYPE){
		printf("it's not a command response\n");
		return 1;
	}
	ios_cmd_id_t *cmd_type = (ios_cmd_id_t *)(data + sizeof(cmd_rsp_hdr_t));
	if(*cmd_type == IOS_ACC_SYSTEM_COMMANDS){
		ios_system_cmd_t *cmdp = (ios_system_cmd_t *)(data + sizeof(cmd_rsp_hdr_t));
		if(cmdp->scmd == IOS_ACC_FPGA_GET_VERSION){
				i2c_buff = (uint8_t *)(data + sizeof(cmd_rsp_hdr_t) + sizeof(ios_system_cmd_t));
				if(cmdp->data_length == 1){
					printf("response length = 1\n");
					for(i = 0; i < 1; i++){
						printf("0x%02X\n", *(i2c_buff + i));
					}
				}else{
					printf("incorrect response length for get fpga version\n");
				}
		}else{
			printf("Invalid response value\n");
		}
	}
    return 0;
}


//cam360 write to MAX17050 fuel gauge
int cam360_BAT_i2c_write(void){
	ios_system_cmd_t cmd;
   int ret;
   int transferred_len;
	uint8_t cmd_buff[128];
	uint8_t i2c_buff[2] = {0x08, 0x07};
	//unsigned short i2c_buff = 0x0708;
	uint8_t data[512];
	//char i2c_read_len = 0;
	cmd_rsp_hdr_t *hdr;

	i2c_type_t i2c_cmd;
	
	//fill i2c_type
	i2c_cmd.reg_addr = 0x23;
	i2c_cmd.buff_len = 2;

	//fill command header
	cmd.hdr.id = IOS_ACC_SYSTEM_COMMANDS;
	cmd.hdr.cmd_len = sizeof(ios_system_cmd_t)+sizeof(i2c_type_t)+i2c_cmd.buff_len;

	cmd.scmd = IOS_ACC_BAT_I2C_WRITE;
	cmd.data_length = sizeof(i2c_type_t);
	printf("cmd lenght = %d\n", cmd.data_length);
	
	memcpy(cmd_buff, &cmd, sizeof(ios_system_cmd_t));
	memcpy(cmd_buff+sizeof(ios_system_cmd_t), &i2c_cmd, sizeof(i2c_type_t));
	memcpy(cmd_buff+sizeof(ios_system_cmd_t)+sizeof(i2c_type_t), i2c_buff, i2c_cmd.buff_len);

	//send the command
    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)cmd_buff,
                             cmd.hdr.cmd_len,
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for chip reset failed %d\n",ret);
        return -1;
    }

	//get the value
	ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_IN + EAP_BULK_IN_EP),
                             (unsigned char *)data,
                             512,
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer in for get_bitrate failed %d\n",ret);
        return -1;
    }

	hdr = (cmd_rsp_hdr_t *)data;
	if(hdr->packet_type != CMDR_TYPE){
		printf("it's not a command response\n");
		return 1;
	}
	ios_cmd_id_t *cmd_type = (ios_cmd_id_t *)(data + sizeof(cmd_rsp_hdr_t));
	if(*cmd_type == IOS_ACC_SYSTEM_COMMANDS){
		ios_system_cmd_t *cmdp = (ios_system_cmd_t *)(data + sizeof(cmd_rsp_hdr_t));
		if(cmdp->scmd == IOS_ACC_BAT_I2C_WRITE){
			i2c_type_t *i2c_response = (i2c_type_t *)(data+sizeof(cmd_rsp_hdr_t)+sizeof(ios_system_cmd_t));
			printf("i2c response len = %d\n", i2c_response->buff_len);
			
		}else{
			printf("Invalid response value\n");
		}
	}
	return 0;
}

//cam360 read from MAX17050 fuel gauge
int cam360_BAT_i2c_read(void){
	ios_system_cmd_t cmd;
   int ret;
   int transferred_len;
	uint8_t cmd_buff[128];
	uint8_t *i2c_buff;
	uint8_t data[512];
	//char i2c_read_len = 0;
	cmd_rsp_hdr_t *hdr;
	uint8_t i;

	i2c_type_t i2c_cmd;
	
	//fill i2c_type
	i2c_cmd.reg_addr = 0x06;
	i2c_cmd.buff_len = 2;

	//fill command header
	cmd.hdr.id = IOS_ACC_SYSTEM_COMMANDS;
	cmd.hdr.cmd_len = sizeof(ios_system_cmd_t)+sizeof(i2c_type_t);

	cmd.scmd = IOS_ACC_BAT_I2C_READ;
	cmd.data_length = sizeof(i2c_type_t);
	printf("cmd lenght = %d\n", cmd.data_length);
	
	memcpy(cmd_buff, &cmd, sizeof(ios_system_cmd_t));
	memcpy(cmd_buff+sizeof(ios_system_cmd_t), &i2c_cmd, sizeof(i2c_type_t));

	//send the command
    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)cmd_buff,
                             cmd.hdr.cmd_len,
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for chip reset failed %d\n",ret);
        return -1;
    }

	//get the value
	ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_IN + EAP_BULK_IN_EP),
                             (unsigned char *)data,
                             512,
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer in for get_bitrate failed %d\n",ret);
        return -1;
    }

	hdr = (cmd_rsp_hdr_t *)data;
	if(hdr->packet_type != CMDR_TYPE){
		printf("it's not a command response\n");
		return 1;
	}
	ios_cmd_id_t *cmd_type = (ios_cmd_id_t *)(data + sizeof(cmd_rsp_hdr_t));
	if(*cmd_type == IOS_ACC_SYSTEM_COMMANDS){
		ios_system_cmd_t *cmdp = (ios_system_cmd_t *)(data + sizeof(cmd_rsp_hdr_t));
		if(cmdp->scmd == IOS_ACC_BAT_I2C_READ){
			i2c_type_t *i2c_response = (i2c_type_t *)(data+sizeof(cmd_rsp_hdr_t)+sizeof(ios_system_cmd_t));
			printf("i2c response len = %d\n", i2c_response->buff_len);
			if(i2c_response->buff_len){
				i2c_buff = (uint8_t *)(data+sizeof(cmd_rsp_hdr_t)+sizeof(ios_system_cmd_t)+sizeof(i2c_type_t));
				for(i = 0; i < i2c_response->buff_len; i++){
					printf("0x%02X\n", *(i2c_buff + i));
				}
			}
		}else{
			printf("Invalid response value\n");
		}
	}
	return 0;
}

//cam360 FPGA get command
int cam360_bat_get_level(void){
	ios_system_cmd_t cmd;
   int ret;
   int transferred_len;
	char data[512];
	cmd_rsp_hdr_t *hdr;
	uint8_t *i2c_buff;
	uint8_t i;

	cmd.hdr.id = IOS_ACC_SYSTEM_COMMANDS;
	cmd.hdr.cmd_len = sizeof(ios_system_cmd_t);
	printf("cmd_len = %d\n", sizeof(ios_system_cmd_t));
	cmd.scmd = IOS_ACC_BAT_GET_LEVEL;
	cmd.data_length = 0;

    printf("cam360 bat get level\n");


    //send the command
    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)&cmd,
                             (sizeof(ios_system_cmd_t)),
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for chip reset failed %d\n",ret);
        return -1;
    }

	//get the value
	ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_IN + EAP_BULK_IN_EP),
                             (unsigned char *)data,
                             512,
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer in for get_bitrate failed %d\n",ret);
        return -1;
    }

	hdr = (cmd_rsp_hdr_t *)data;
	if(hdr->packet_type != CMDR_TYPE){
		printf("it's not a command response\n");
		return 1;
	}
	ios_cmd_id_t *cmd_type = (ios_cmd_id_t *)(data + sizeof(cmd_rsp_hdr_t));
	if(*cmd_type == IOS_ACC_SYSTEM_COMMANDS){
		ios_system_cmd_t *cmdp = (ios_system_cmd_t *)(data + sizeof(cmd_rsp_hdr_t));
		if(cmdp->scmd == IOS_ACC_BAT_GET_LEVEL){
				i2c_buff = (uint8_t *)(data + sizeof(cmd_rsp_hdr_t) + sizeof(ios_system_cmd_t));
				if(cmdp->data_length == 2){
					printf("response length = 2\n");
					for(i = 0; i < 2; i++){
						printf("0x%02X\n", *(i2c_buff + i));
					}
				}else{
					printf("incorrect response length for get bat level\n");
				}
		}else{
			printf("Invalid response value\n");
		}
	}
    return 0;
}

//cam360 read from Apple coProcessor
int cam360_ACP_i2c_read(void){
    ios_system_cmd_t cmd;
    int ret;
    int transferred_len;
	uint8_t cmd_buff[128];
	uint8_t *i2c_buff;
	uint8_t data[512];
	//char i2c_read_len = 0;
	cmd_rsp_hdr_t *hdr;
	uint8_t i;

	i2c_type_t i2c_cmd;
	
	//fill i2c_type
	i2c_cmd.reg_addr = 0x05;
	i2c_cmd.buff_len = 1;

	//fill command header
	cmd.hdr.id = IOS_ACC_SYSTEM_COMMANDS;
	cmd.hdr.cmd_len = sizeof(ios_system_cmd_t)+sizeof(i2c_type_t);

	cmd.scmd = IOS_ACC_ACP_READ;
	cmd.data_length = sizeof(i2c_type_t);
	printf("cmd lenght = %d\n", cmd.data_length);
	
	memcpy(cmd_buff, &cmd, sizeof(ios_system_cmd_t));
	memcpy(cmd_buff+sizeof(ios_system_cmd_t), &i2c_cmd, sizeof(i2c_type_t));

	//send the command
    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)cmd_buff,
                             cmd.hdr.cmd_len,
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for chip reset failed %d\n",ret);
        return -1;
    }

	//get the value
	ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_IN + EAP_BULK_IN_EP),
                             (unsigned char *)data,
                             512,
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer in for get_bitrate failed %d\n",ret);
        return -1;
    }

	hdr = (cmd_rsp_hdr_t *)data;
	if(hdr->packet_type != CMDR_TYPE){
		printf("it's not a command response\n");
		return 1;
	}
	ios_cmd_id_t *cmd_type = (ios_cmd_id_t *)(data + sizeof(cmd_rsp_hdr_t));
	if(*cmd_type == IOS_ACC_SYSTEM_COMMANDS){
		ios_system_cmd_t *cmdp = (ios_system_cmd_t *)(data + sizeof(cmd_rsp_hdr_t));
		if(cmdp->scmd == IOS_ACC_ACP_READ){
			i2c_type_t *i2c_response = (i2c_type_t *)(data+sizeof(cmd_rsp_hdr_t)+sizeof(ios_system_cmd_t));
			printf("i2c response len = %d\n", i2c_response->buff_len);
			if(i2c_response->buff_len){
				i2c_buff = (uint8_t *)(data+sizeof(cmd_rsp_hdr_t)+sizeof(ios_system_cmd_t)+sizeof(i2c_type_t));
				for(i = 0; i < i2c_response->buff_len; i++){
					printf("0x%02X\n", *(i2c_buff + i));
				}
			}
		}else{
			printf("Invalid response value\n");
		}
	}
	return 0;
}

//cam360 write to Apple coProcessor
int cam360_ACP_i2c_write(void){
	ios_system_cmd_t cmd;
   int ret;
   int transferred_len;
	uint8_t cmd_buff[128];
	uint8_t i2c_buff[2] = {0xAA, 0xBB};
	uint8_t data[512];
	//char i2c_read_len = 0;
	cmd_rsp_hdr_t *hdr;

	i2c_type_t i2c_cmd;
		
	printf("not too sure why you would want to do this...:/\n");

	//fill i2c_type
	i2c_cmd.reg_addr = 0x11;
	i2c_cmd.buff_len = 2;
	
	//fill command header
	cmd.hdr.id = IOS_ACC_SYSTEM_COMMANDS;
	cmd.hdr.cmd_len = sizeof(ios_system_cmd_t)+sizeof(i2c_type_t)+1;

	cmd.scmd = IOS_ACC_ACP_WRITE;
	cmd.data_length = sizeof(i2c_type_t);
	printf("cmd lenght = %d\n", cmd.data_length);
	
	memcpy(cmd_buff, &cmd, sizeof(ios_system_cmd_t));
	memcpy(cmd_buff+sizeof(ios_system_cmd_t), &i2c_cmd, sizeof(i2c_type_t));
	memcpy(cmd_buff+sizeof(ios_system_cmd_t)+sizeof(i2c_type_t), i2c_buff, 1);

	//send the command
    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)cmd_buff,
                             cmd.hdr.cmd_len,
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for chip reset failed %d\n",ret);
        return -1;
    }

	//get the value
	ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_IN + EAP_BULK_IN_EP),
                             (unsigned char *)data,
                             512,
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer in for get_bitrate failed %d\n",ret);
        return -1;
    }

	hdr = (cmd_rsp_hdr_t *)data;
	if(hdr->packet_type != CMDR_TYPE){
		printf("it's not a command response\n");
		return 1;
	}
	ios_cmd_id_t *cmd_type = (ios_cmd_id_t *)(data + sizeof(cmd_rsp_hdr_t));
	if(*cmd_type == IOS_ACC_SYSTEM_COMMANDS){
		ios_system_cmd_t *cmdp = (ios_system_cmd_t *)(data + sizeof(cmd_rsp_hdr_t));
		if(cmdp->scmd == IOS_ACC_ACP_WRITE){
			i2c_type_t *i2c_response = (i2c_type_t *)(data+sizeof(cmd_rsp_hdr_t)+sizeof(ios_system_cmd_t));
			printf("i2c response len = %d\n", i2c_response->buff_len);
			
		}else{
			printf("Invalid response value\n");
		}
	}
	return 0;
}

//get power on self test result
int cam360_get_post_result(void)
{
    int ret;
    int transferred_len;
    ios_system_cmd_t cmd;
	uint8_t data[512];
	cmd_rsp_hdr_t *hdr;
    self_test_result_t *post_result;

    do{
        //fill command header
        cmd.hdr.id = IOS_ACC_SYSTEM_COMMANDS;
        cmd.hdr.cmd_len = sizeof(ios_system_cmd_t);

        cmd.scmd = 	IOS_ACC_READ_POST_RESULT;
        cmd.data_length = 0;
        
        //send the command
        ret = libusb_bulk_transfer(video_hdl,
                                 (USB_DIR_OUT + EAP_BULK_OUT_EP),
                                 (unsigned char *)&cmd,
                                 cmd.hdr.cmd_len,
                                 &transferred_len,
                                 2000
                                );
        if(ret){
            printf("ERR: libusb_bulk_transfer out for post command is failed %d\n",ret);
            return -1;
        }

        //get the value
        ret = libusb_bulk_transfer(video_hdl,
                                 (USB_DIR_IN + EAP_BULK_IN_EP),
                                 (unsigned char *)data,
                                 sizeof(data),
                                 &transferred_len,
                                 2000
                                );
        if(ret){
            printf("ERR: libusb_bulk_transfer in for get_bitrate failed %d\n",ret);
            return -1;
        }

        hdr = (cmd_rsp_hdr_t *)data;
        if(hdr->packet_type != CMDR_TYPE){
            printf("it's not a command response\n");
            return 1;
        }

        ios_cmd_id_t *cmd_type = (ios_cmd_id_t *)(data + sizeof(cmd_rsp_hdr_t));
        if(*cmd_type == IOS_ACC_SYSTEM_COMMANDS){
            ios_system_cmd_t *cmdp = (ios_system_cmd_t *)(data + sizeof(cmd_rsp_hdr_t));
            if(cmdp->scmd == IOS_ACC_READ_POST_RESULT){
                post_result = (self_test_result_t *)(data+sizeof(cmd_rsp_hdr_t)+sizeof(ios_system_cmd_t));
            }else{
                printf("Invalid response value\n");
                return 1;
            }
        }

        if(post_result->status == TEST_STATE_COMPLETE){
            //print the test result
            printf("Apple CP test: %s\n",post_result->apple_cp==TEST_RESULT_SUCCESS ? "PASS":"FAIL");
            printf("BAT read test: %s\n",post_result->bat_fuel_gauge==TEST_RESULT_SUCCESS ? "PASS":"FAIL");
            printf("FPGA read test: %s\n",post_result->fpga==TEST_RESULT_SUCCESS ? "PASS":"FAIL");
            if(post_result->apple_cp == TEST_RESULT_FAIL || 
               post_result->bat_fuel_gauge == TEST_RESULT_FAIL ||
               post_result->fpga == TEST_RESULT_FAIL)
               return 1;
        }
        
        if(post_result->status == TEST_STATE_NOT_RUN){
            printf("POST is not enabled\n");
            printf("Please enable POWER_ON_SELF_TEST key in json\n");
            break;
        }

        if(post_result->status == TEST_STATE_IN_PROGRESS)
            printf("POST is in Progress, retry again\n");

    }while(post_result->status == TEST_STATE_IN_PROGRESS);

    return 0;
}

void help_print(void){
	printf("Supported commands\n");
	printf(" - fw_version\n");
	printf(" - gpio_get gpio_pin\n");
	printf(" - gpio_set gpio_pin gpio_val\n");
	printf(" - i2c_FPGA_write\n");
	printf(" - i2c_FPGA_read\n");
	printf(" - i2c_BAT_write\n");
	printf(" - i2c_BAT_read\n");
	printf(" - i2c_ACP_write\n");
	printf(" - i2c_ACP_read\n");
	printf(" - get_SN\n");
	printf(" - FPGA_get_system_status\n");
	printf(" - FPGA_get_system_param\n");
	printf(" - FPGA_get_version\n");
	printf(" - FPGA_get_command\n");
    printf(" - get_post_result (power-on self test result)\n");
}

int main(int argc, char **argv)
{
    uint16_t vendor_id = R2_VID, product_id = R2_PID;
    int ret;

    if(argc < 2 || (strcmp(argv[1], "help")==0) ||
        (strcmp(argv[1], "fw_version") && strcmp(argv[1], "gpio_get")
				&& strcmp(argv[1], "gpio_set") && strcmp(argv[1], "i2c_FPGA_write")
				&& strcmp(argv[1], "i2c_FPGA_read") && strcmp(argv[1], "get_SN")
				&& strcmp(argv[1], "FPGA_get_system_status") && strcmp(argv[1], "FPGA_get_system_param")
				&& strcmp(argv[1], "FPGA_get_version") && strcmp(argv[1], "FPGA_get_command")
				&& strcmp(argv[1], "i2c_BAT_read") && strcmp(argv[1], "i2c_BAT_write")
				&& strcmp(argv[1], "i2c_ACP_read") && strcmp(argv[1], "i2c_ACP_write") 
                && strcmp(argv[1], "get_level")
                && strcmp(argv[1], "get_post_result"))){
        help_print();
        return 1;
    }

    ret = libusb_init(&video_ctx);
    if(ret < 0){
        printf("init_libusb failed\n");
        return -1;
    }

    video_hdl = libusb_open_device_with_vid_pid(video_ctx, vendor_id,
                            product_id);
    
    if(video_hdl == NULL){
        printf("Could not open USB device %x:%x\n", vendor_id, product_id);
        goto error;
    }
    
    ret = libusb_claim_interface(video_hdl, IPCAM_EAP_INTERFACE_NUM);
    if(ret != 0){
        printf("libusb_claim_interface error %d\n",ret);
        printf("Please connect a GC6500 running in ios acc mode\n");
        goto error;
    }

    //set interface to activte the endpoints
    ret = libusb_set_interface_alt_setting( video_hdl, IPCAM_EAP_INTERFACE_NUM, 1);
    if(ret != 0){
        printf("libusb_set_interface_alt_setting error %d\n",ret);
        goto error;
        return -1;
    }
   
    if(strcmp(argv[1], "fw_version")==0){
        cam360_get_fw_version();
    }else if(strcmp(argv[1], "gpio_set")==0){
			cam360_gpio_set(argv[2], argv[3]);
	}else if(strcmp(argv[1], "gpio_get")==0){
			cam360_gpio_get(argv[2]);
	}else if(strcmp(argv[1], "i2c_FPAG_write")==0){
			cam360_FPGA_i2c_write();
	}else if(strcmp(argv[1], "i2c_FPGA_read")==0){
			cam360_FPGA_i2c_read();
	}else if(strcmp(argv[1], "get_SN")==0){
			cam360_get_SN();
	}else if(strcmp(argv[1], "i2c_BAT_read")==0){
			cam360_BAT_i2c_read();
	}else if(strcmp(argv[1], "i2c_BAT_write")==0){
			cam360_BAT_i2c_write();
	}else if(strcmp(argv[1], "i2c_ACP_read")==0){
			cam360_ACP_i2c_read();
	}else if(strcmp(argv[1], "i2c_ACP_write")==0){
			cam360_ACP_i2c_write();
	}else if(strcmp(argv[1], "FPGA_get_system_status")==0){
			cam360_FPGA_get_system_status();
	}else if(strcmp(argv[1], "FPGA_get_system_param")==0){
			cam360_FPGA_get_system_param();
	}else if(strcmp(argv[1], "FPGA_get_version")==0){
			cam360_FPGA_get_version();
	}else if(strcmp(argv[1], "FPGA_get_command")==0){
			cam360_FPGA_get_command();
	}else if(strcmp(argv[1], "get_level")==0){
			cam360_bat_get_level();
	}else if(strcmp(argv[1], "get_post_result")==0){
            if(cam360_get_post_result() == 1)//error
            {
                if(video_hdl)
                    ret = libusb_release_interface(video_hdl, IPCAM_EAP_INTERFACE_NUM);
                close_device();
                return 1;
            }
    }
error:
    if(video_hdl)
        ret = libusb_release_interface(video_hdl, IPCAM_EAP_INTERFACE_NUM);
    close_device();

    return 0;
}
