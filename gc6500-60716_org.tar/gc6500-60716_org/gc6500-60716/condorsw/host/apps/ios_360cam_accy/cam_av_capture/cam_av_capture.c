/* Unit test in host side to test iOS iAP endpoints
 * comile: gcc -o ios_usb_acc_host -lusb-1.0 -Wall ios_usb_acc_host.c
 */
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h> /* memset, memcmp */
#include <unistd.h> /* sleep */
#include <assert.h> /* assert */
#include <string.h>
#include <libusb-1.0/libusb.h>
#include <inttypes.h>
#include <signal.h>
#include <sys/time.h>
#include <stdbool.h>
#include <sys/stat.h>

#include "qmed.h"
#include "qmedext.h"
#include "qbox.h"
#include "usb_iap.h"

#define ERROR(ret_val, msg, ...)                                      \
do {                                                          \
	fprintf(stderr, "ERROR: " msg "\n", ##__VA_ARGS__);   \
	return ret_val;                                       \
} while(0)

static struct libusb_device_handle *video_hdl;
static struct libusb_context       *video_ctx;

#define MAX_NUM_CHANNELS 3
FILE *fd[MAX_NUM_CHANNELS];

#define R2_VID 0x29fe
#define R2_PID 0x4d53

#define USB_DIR_OUT			0		/* to device */
#define USB_DIR_IN			0x80		/* to host */

#define IAP2_BULK_IN_EP 0x02
#define IAP2_BULK_OUT_EP 0x01

#define EAP_BULK_IN_EP 0x03
#define EAP_BULK_OUT_EP 0x04

#define EAP_BULK_OUT_BUF_SIZE 1024*4

//iAP2 class specific definations
enum interface_num_t{
    IPCAM_IAP2_INTERFACE_NUM = 0,
    IPCAM_EAP_INTERFACE_NUM = 1,
    MAX_NUM_INTRFC
};

void close_device()
{
    if(video_hdl)libusb_close(video_hdl);
    if(video_ctx)libusb_exit(video_ctx);
}

void close_file(void)
{
    int i = 0;   
    for(i=0;i<MAX_NUM_CHANNELS;i++)
    {
        if ( fd[i] != NULL)
            fclose(fd[i]);
    }
}

#define OBJECTTYPE_AACLC (2)
#define ADTS_HEADER_LEN (7)

static const int SampleFreq[12] = {
    96000, 88200, 64000, 48000, 44100, 32000,
    24000, 22050, 16000, 12000, 11025, 8000
};

void CopyADTSHeader(int size, unsigned char *adtsHeader)
{
    int i;
    int samplingFreqIndex;

    // clear the adts header
    for (i = 0; i < ADTS_HEADER_LEN; i++)
    {
        adtsHeader[i] = 0;
    }

    // bits 0-11 are sync
    adtsHeader[0] = 0xff;
    adtsHeader[1] = 0xf0;

    // bit 12 is mpeg4 which means clear the bit

    // bits 13-14 is layer, always 00

    // bit 15 is protection absent (no crc)
    adtsHeader[1] |= 0x1;

    // bits 16-17 is profile which is 01 for AAC-LC
    adtsHeader[2] = 0x40;

    // bits 18-21 is sampling frequency index
    for (i=0; i<12; i++)
    {
        if ( SampleFreq[i] == 16000 )
        {
            samplingFreqIndex =  i;
            break;
        }
    }

    adtsHeader[2] |= (samplingFreqIndex << 2);

    // bit 22 is private

    // bit 23-25 is channel config.  However since we are mono or stereo
    // bit 23 is always zero
    adtsHeader[3] = 2 << 6;

    // bits 26-27 are original/home and zeroed

    // bits 28-29 are copyright bits and zeroed

    // bits 30-42 is sample length including header len.  First we get qbox length,
    // then sample length and then add header length


    // adjust for headers
    int frameSize = size + ADTS_HEADER_LEN;

    // get upper 2 bits of 13 bit length and move them to lower 2 bits
    adtsHeader[3] |= (frameSize & 0x1800) >> 11;

    // get middle 8 bits of length
    adtsHeader[4] = (frameSize & 0x7f8) >> 3;

    // get lower 3 bits of length and put as 3 msb
    adtsHeader[5] = (frameSize & 0x7) << 5;

    // bits 43-53 is buffer fulless but we use vbr so 0x7f
    adtsHeader[5] |= 0x1f;
    adtsHeader[6] = 0xfc;

    // bits 54-55 are # of rdb in frame which is always 1 so we write 1 - 1 = 0
    // which means do not write
}

#if __BYTE_ORDER == LITTLE_ENDIAN
#define be32_to_cpu(x)  ((((x) >> 24) & 0xff) | \
             (((x) >> 8) & 0xff00) | \
             (((x) << 8) & 0xff0000) | \
             (((x) << 24) & 0xff000000))
#define be24_to_cpu(a)   \
    ((((a)>>16)&0xFF) |                                                     \
    ((a)&0xFF00) |                                                          \
    (((a)<<16)&0xFF0000))
#define be16_to_cpu(x)  ((((x) >> 8) & 0xff) | \
             (((x) << 8) & 0xff00))
#else
#define be32_to_cpu(x) (x)
#define be24_to_cpu(x) (x)
#define be16_to_cpu(x) (x)
#endif

// default sampling frequency 
int samp_freq = 16000;
#define BOARD_SAMPLE_RATE 16000
uint64_t sam_interval; 

uint64_t prev_ts = 0;

uint64_t permissible_range = 10; //in percentage

uint64_t upper_sam_interval;
uint64_t lower_sam_interval;

static void audio_cb(unsigned char *buffer, unsigned int size,
		video_format_t format, uint64_t ts, void *user_data, 
        unsigned char *adts)
{
	//unsigned char adtsHeader[7];
	audio_channel_t ch = 2;

	switch(format) {
	case VID_FORMAT_AAC_RAW: //TBD
		if(fd[ch] == NULL){
			fd[ch] = fopen("out.audio.aac", "w");
			//calculate required sampling interval
			//(1024/sam_freq)*90  //90 is resampler freq
			sam_interval = (uint64_t)(((float)(1024*1000/samp_freq))*90);
			uint64_t percent = (uint64_t)(sam_interval*permissible_range)/100;
			upper_sam_interval = (uint64_t)(sam_interval + percent);
			lower_sam_interval = (uint64_t)(sam_interval - percent);
			printf("sam_interval %" PRIu64 "upper_limit %" PRIu64 " low_limit %" PRIu64 "\n",
					sam_interval,upper_sam_interval,lower_sam_interval);
		}

		if((((ts-prev_ts) > upper_sam_interval) || ((ts-prev_ts) < lower_sam_interval)) && (prev_ts))
			printf("out of range: %" PRIu64 ", last ts %" PRIu64 " preset ts %" PRIu64 "\n",(ts-prev_ts),prev_ts, ts);

		prev_ts = ts;
			
		if(fd[ch] != NULL)
			fwrite(adts, ADTS_HEADER_LEN, 1, fd[ch]);
		break;
	default:
		printf("Audio format not supported\n");
		return;
	}

	if(fd[ch] != NULL)
		fwrite(buffer, size, 1, fd[ch]);
}


static void ch_cb(unsigned char *buffer, unsigned int size,
        video_info_t info, int ch)
{
    static const char *basename = "out.chX";
    static const char *ext[NUM_VID_FORMAT] = {
            [VID_FORMAT_H264_RAW]    = ".h264",
            [VID_FORMAT_MJPEG_RAW]   = ".mjpeg",
            [AUD_FORMAT_AAC_RAW]     = ".aac"
    };

    video_format_t fmt = (video_format_t) info.format;
    if(fmt < FIRST_VID_FORMAT || fmt >= NUM_VID_FORMAT) {
        printf("Unknown Video format\n");
        return;
    }

    if(fd[ch] == NULL) {
        char *fname = malloc(strlen(basename) + strlen(ext[fmt]) + 1);
        strcpy(fname, basename);
        strcpy(fname + strlen(fname), ext[fmt]);
        fname[6] = ((char) ch + 1) % 10 + 48;
        fd[ch] = fopen(fname, "w");
        free(fname);
    }
   
    fwrite(buffer, size, 1, fd[ch]);
}

#define MAX_VIDEO_BUF_LENGTH 8000000 //
uint8_t data_buf[MAX_VIDEO_BUF_LENGTH];

int cam360_video_start(video_channel_t ch)
{
    ios_stream_state_cmd_t cmd;
    int ret;
    int transferred_len;

    cmd.hdr.id = IOS_ACC_START_STREAM;
    cmd.hdr.cmd_len = sizeof(ios_stream_state_cmd_t);
    cmd.mux_channel_id = (int)ch;
    cmd.type = VIDEO_CHANNEL;

    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)&cmd,
                             sizeof(ios_stream_state_cmd_t),
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for video_start failed %d\n",ret);
        return -1;
    }
    return 0;
}

int cam360_video_stop(video_channel_t ch)
{
    ios_stream_state_cmd_t cmd;
    int ret;
    int     transferred_len;

    cmd.hdr.id = IOS_ACC_STOP_STREAM;
    cmd.hdr.cmd_len = sizeof(ios_stream_state_cmd_t);
    cmd.mux_channel_id = (int)ch;
    cmd.type = VIDEO_CHANNEL;

    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)&cmd,
                             sizeof(ios_stream_state_cmd_t),
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for video_start failed %d\n",ret);
        return -1;
    }    

    return 0;
}

int cam360_audio_start(video_channel_t ch)
{
    ios_stream_state_cmd_t cmd;
    int ret;
    int transferred_len;

    cmd.hdr.id = IOS_ACC_START_STREAM;
    cmd.hdr.cmd_len = sizeof(ios_stream_state_cmd_t);
    cmd.mux_channel_id = (int)ch;
    cmd.type = AUDIO_CHANNEL;

    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)&cmd,
                             sizeof(ios_stream_state_cmd_t),
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for video_start failed %d\n",ret);
        return -1;
    }
    return 0;
}

int cam360_audio_stop(video_channel_t ch)
{
    ios_stream_state_cmd_t cmd;
    int ret;
    int     transferred_len;

    cmd.hdr.id = IOS_ACC_STOP_STREAM;
    cmd.hdr.cmd_len = sizeof(ios_stream_state_cmd_t);
    cmd.mux_channel_id = (int)ch;
    cmd.type = AUDIO_CHANNEL;

    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)&cmd,
                             sizeof(ios_stream_state_cmd_t),
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for video_start failed %d\n",ret);
        return -1;
    }    

    return 0;
}

int cam360_video_force_iframe(video_channel_t ch)
{
   ios_video_cmd_t cmd;
   int ret;
   int transferred_len;

   cmd.hdr.id = IOS_ACC_VIDEO_COMMANDS;
   cmd.hdr.cmd_len = sizeof(ios_video_cmd_t);
   cmd.vcmd = IOS_ACC_SET_FORCE_IFRAME;
   cmd.mux_channel_id = ch;
   cmd.data_length = 0;
   cmd.cmd_data = 0;

   ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)&cmd,
                             sizeof(ios_video_cmd_t),
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for force iframe failed %d\n",ret);
        return -1;
    } 

    return 0;
}

int cam360_video_set_bitrate(video_channel_t ch, uint32_t bitrate)
{
   ios_video_cmd_t cmd;
   int ret;
   int transferred_len;

   cmd.hdr.id = IOS_ACC_VIDEO_COMMANDS;
   cmd.hdr.cmd_len = sizeof(ios_video_cmd_t);
   cmd.vcmd = IOS_ACC_SET_BITRATE;
   cmd.mux_channel_id = ch;
   cmd.data_length = sizeof(uint32_t);
   cmd.cmd_data = bitrate;
   
   ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)&cmd,
                             sizeof(ios_video_cmd_t),
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for set_bitrate failed %d\n",ret);
        return -1;
    } 

    return 0;

}

int cam360_video_get_bitrate(video_channel_t ch, uint32_t *bitrate)
{
   ios_video_cmd_t cmd;
   int ret;
   int transferred_len;
   char data[512];
   cmd_rsp_hdr_t *hdr;

   cmd.hdr.id = IOS_ACC_VIDEO_COMMANDS;
   cmd.hdr.cmd_len = sizeof(ios_video_cmd_t);
   cmd.vcmd = IOS_ACC_GET_BITRATE;
   cmd.mux_channel_id = ch;
   cmd.data_length = 0;
   cmd.cmd_data = 0;

    //send the command
    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)&cmd,
                             sizeof(ios_video_cmd_t),
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for get_bitrate failed %d\n",ret);
        return -1;
    }

    //get the value
    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_IN + EAP_BULK_IN_EP),
                             (unsigned char *)data,
                             512,
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer in for get_bitrate failed %d\n",ret);
        return -1;
    }

    //check if its a command response
    hdr = (cmd_rsp_hdr_t *)data;
    if (hdr->packet_type != CMDR_TYPE){
        printf("its not a command response\n");
        return 1;
    }
    ios_cmd_id_t *cmd_type = (ios_cmd_id_t *)(data + sizeof(cmd_rsp_hdr_t));
    if(*cmd_type == IOS_ACC_VIDEO_COMMANDS){
        ios_video_cmd_t *cmdp = (ios_video_cmd_t *)(data + sizeof(cmd_rsp_hdr_t));
 
        if(cmdp->vcmd == IOS_ACC_GET_BITRATE && cmdp->mux_channel_id == ch)
            *bitrate = cmdp->cmd_data;
        else{
            printf("Invalid response received\n");
            return 0;
        }
    }
    return 0;
}

#if 0  //TBD
int cam360_video_set_maxnal(video_channel_t ch, uint32_t maxnal)
{
   ios_video_cmd_t cmd;
   int ret;
   int transferred_len;

   cmd.hdr = IOS_ACC_VIDEO_COMMANDS;
   cmd.vcmd = IOS_ACC_SET_AVC_MAXNAL;
   cmd.mux_channel_id = ch;
   cmd.data_length = sizeof(uint32_t);
   cmd.cmd_data = maxnal;
    
   ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)&cmd,
                             sizeof(ios_video_cmd_t),
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for set_maxnal failed %d\n",ret);
        return -1;
    } 

    return 0;
}

int cam360_video_get_maxnal(video_channel_t ch, uint32_t *maxnal)
{
   ios_video_cmd_t cmd;
   int ret;
   int transferred_len;

   cmd.hdr = IOS_ACC_VIDEO_COMMANDS;
   cmd.vcmd = IOS_ACC_GET_AVC_MAXNAL;
   cmd.mux_channel_id = ch;
   cmd.data_length = 0;
   cmd.cmd_data = 0;
    
   ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)&cmd,
                             sizeof(ios_video_cmd_t),
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for get_maxnal failed %d\n",ret);
        return -1;
    }    

    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_IN + EAP_BULK_IN_EP),
                             (unsigned char *)&cmd,
                             sizeof(ios_video_cmd_t),
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer in for get_maxnal failed %d\n",ret);
        
        return -1;
    }
    
    if(cmd.vcmd == IOS_ACC_GET_AVC_MAXNAL && cmd.mux_channel_id ==ch)
        *maxnal = cmd.cmd_data;
    else{
        printf("Invalid response received\n");
        return -1;
    }
    return 0;
}

int cam360_load_alpha(char *y_data, uint32_t y_len, char *uv_data, uint32_t uv_len)
{
    ios_alpha_write_cmd_t cmd;
    uint32_t wlen = 0;
    uint32_t i = 0;
    int ret, transferred_len;
    char *pdata = (char *)malloc(EAP_BULK_OUT_BUF_SIZE);

    //write y
    for(i=0 ; i<y_len; )
    {
        if((y_len-i) > (EAP_BULK_OUT_BUF_SIZE-sizeof(ios_alpha_write_cmd_t)))
            wlen = EAP_BULK_OUT_BUF_SIZE-sizeof(ios_alpha_write_cmd_t);
        else
            wlen = y_len-i;

        cmd.hdr.id = IOS_ACC_ALPHA_WRITE;
        cmd.hdr.cmd_len = wlen+sizeof(ios_alpha_write_cmd_t);
        cmd.type = ALPHA_Y;
        cmd.len = wlen;
        cmd.offset = i;

        memcpy(pdata, (char *)&cmd,  sizeof(ios_alpha_write_cmd_t));
        memcpy(pdata+sizeof(ios_alpha_write_cmd_t), y_data+i, wlen);

        ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)pdata,
                             wlen+sizeof(ios_alpha_write_cmd_t),
                             &transferred_len,
                             2000
                            );
        if(ret){
            printf("ERR: libusb_bulk_transfer out for get_maxnal failed %d\n",ret);
            free(pdata);
            return -1;
        }  

        i+=wlen;

    }

    //write uv
    for(i=0 ; i<uv_len; )
    {
        if((uv_len-i) > (EAP_BULK_OUT_BUF_SIZE-sizeof(ios_alpha_write_cmd_t)))
            wlen = EAP_BULK_OUT_BUF_SIZE-sizeof(ios_alpha_write_cmd_t);
        else
            wlen = uv_len-i;

        cmd.hdr.id = IOS_ACC_ALPHA_WRITE;
        cmd.hdr.cmd_len = wlen+sizeof(ios_alpha_write_cmd_t);
        cmd.type = ALPHA_UV;
        cmd.len = wlen;
        cmd.offset = i;

        memcpy(pdata, (char *)&cmd,  sizeof(ios_alpha_write_cmd_t));
        memcpy(pdata+sizeof(ios_alpha_write_cmd_t), y_data+i, wlen);

        ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)pdata,
                             wlen+sizeof(ios_alpha_write_cmd_t),
                             &transferred_len,
                             2000
                            );
        if(ret){
            printf("ERR: libusb_bulk_transfer out for get_maxnal failed %d\n",ret);
            free(pdata);
            return -1;
        }  

        i+=wlen;

    }

    free(pdata);

    return 0;
}
#endif
int send_commands_together(void)
{
    ios_video_cmd_t *cmdv;
    int ret;
    int transferred_len;
    char cdata[512];
    ios_stream_state_cmd_t *cmd;

    //create set bitrate
    cmdv = (ios_video_cmd_t *)cdata;
    cmdv->hdr.id = IOS_ACC_VIDEO_COMMANDS;
    cmdv->hdr.cmd_len = sizeof(ios_video_cmd_t);
    cmdv->vcmd = IOS_ACC_SET_BITRATE;
    cmdv->mux_channel_id = CH1;
    cmdv->data_length = sizeof(uint32_t);
    cmdv->cmd_data = 10000000;

    //create force iframe
    cmdv = (ios_video_cmd_t *)(cdata+sizeof(ios_video_cmd_t));
    cmdv->hdr.id = IOS_ACC_VIDEO_COMMANDS;
    cmdv->hdr.cmd_len = sizeof(ios_video_cmd_t);
    cmdv->vcmd = IOS_ACC_SET_FORCE_IFRAME;
    cmdv->mux_channel_id = CH1;
    cmdv->data_length = 0;

    //create start command on CH1
    cmd = (ios_stream_state_cmd_t *)(cdata+2*sizeof(ios_video_cmd_t));
    cmd->hdr.id = IOS_ACC_START_STREAM;
    cmd->hdr.cmd_len = sizeof(ios_stream_state_cmd_t);
    cmd->mux_channel_id = (int)CH1;
    cmd->type = VIDEO_CHANNEL;

    //send commands together
    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)cdata,
                             2*sizeof(ios_video_cmd_t)+sizeof(ios_stream_state_cmd_t), //total size of commands
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for set_bitrate failed %d\n",ret);
        return -1;
    }

    return 0;
}

static int quit_capture_app = 0;

void my_handler (int sig)
{
    quit_capture_app = 1;
    printf("Exiting app\n");   
}

int main(int argc, char **argv)
{
    uint16_t vendor_id = 0xdead, product_id = 0xbeef;
    int     transferred_len;
    int ret;
    int channel_id;
    video_info_t info;
    uint8_t *video_buf;
    uint32_t analytics;
    int frame_count = 0;
    int rx_aframe_count = 0;
    int rx_vframe_count = 0;
    uint32_t qbox_frame_size = 0;
    uint32_t count=0;
    uint32_t bitrate;
    qmed_t qmed;

    ret = libusb_init(&video_ctx);
	if(ret < 0){
        ERROR(-1, "init_libusb failed");
		return -1;
    }

    vendor_id = 0x29fe;
    product_id = 0x4d53;
    video_hdl = libusb_open_device_with_vid_pid(video_ctx, vendor_id,
							product_id);
    
    if(video_hdl == NULL){
        printf("Could not open USB device\n"
			"%x:%x", vendor_id, product_id);
        close_device();
        return -1;
     }

    /* Claim the interface */
    ret = libusb_claim_interface(video_hdl, IPCAM_IAP2_INTERFACE_NUM);
    if(ret != 0){
        printf("libusb_claim_interface error %d\n",ret);
        close_device(); 
        return -1;
    }
    ret = libusb_claim_interface(video_hdl, IPCAM_EAP_INTERFACE_NUM);
    if(ret != 0){
        printf("libusb_claim_interface error %d\n",ret);
        close_device(); 
        return -1;
    }

    //start channel
    ret = libusb_set_interface_alt_setting( video_hdl, IPCAM_EAP_INTERFACE_NUM, 1);
    if(ret != 0){
        printf("libusb_set_interface_alt_setting error %d\n",ret);
        close_device(); 
        return -1;
    }
#ifdef TEAST_IAP2
    //test iAP2
    for(i =0; i<MAX_VIDEO_BUF_LENGTH-1; i++)
            data_buf[i] = 0;
    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_IN + IAP2_BULK_IN_EP),
                             (unsigned char *)&data_buf[0],
                             6,
                             &transferred_len,
                             2000
                            );
    if(ret){
            printf("ERR: libusb_bulk_transfer in failed %d\n",ret);
            close_device(); 
            return -1;
        }
    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + IAP2_BULK_OUT_EP),
                             (unsigned char *)&data_buf[0],
                             23,
                             &transferred_len,
                             2000
                            );
    if(ret){
            printf("ERR: libusb_bulk_transfer out failed %d\n",ret);
            close_device(); 
            return -1;
        }
    
#endif
    if (argc > 1){
        frame_count = atoi(argv[1]);
    } else
        frame_count = 150;

	//add CTRL-C handler
    signal (SIGQUIT, my_handler);
    signal (SIGINT, my_handler);

    struct timeval tv1, tv2;
#if 0
    //request a I-Frame on CH1 channel
    ret = cam360_video_force_iframe(CH1);
    if(ret)
       goto error;
    //setting a bitrate of 10Mbps on CH1
    ret = cam360_video_set_bitrate(CH1, 10000000);
    if(ret)
      goto error;
#endif 

    //check the current bitrate
    ret = cam360_video_get_bitrate(CH1, &bitrate);
    if(ret)
       goto error;
    printf("bitrate %d\n",bitrate);
    
    gettimeofday(&tv1, NULL);
    //start video on CH1 channel
    usleep(1000*100);

    //send aggregated commands to camera
    ret = send_commands_together();
    
    //ret = cam360_video_start(CH1);
    //if(ret)
     //  goto error;
    //start audio on CH2. NOTE: it depends on json which channel 
    //is configured as Audio on MUX.
    ret = cam360_audio_start(CH2);
    if(ret)
       goto error;
    //wait for channels to start
    //capture audio/video stream from EA Bulk IN EP
    while(frame_count > rx_vframe_count && (!quit_capture_app)){
        ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_IN + EAP_BULK_IN_EP),
                             (unsigned char *)&data_buf[0],
                             MAX_VIDEO_BUF_LENGTH,
                             &transferred_len,
                             5000
                            );

        if(ret){
            printf("ERR: libusb_bulk_transfer in audio/video capture failed %d\n",ret);
            break;
        }
       
        ret = qbox_parse_header(&data_buf[0], &channel_id,
                                &info.format, &video_buf, &qbox_frame_size, &info.ts,
                                &analytics, &(info.metadata), &qmed);
        /*if (ret) {
            printf("Wrong mux video format\n");
            break;
        }*/

        if(ret == 0){ // qbox audio/video data
            if(info.format == VID_FORMAT_H264_RAW || 
                info.format == VID_FORMAT_MJPEG_RAW){
                //video data
                ch_cb(video_buf, qbox_frame_size, info, channel_id);
                rx_vframe_count++;
            }else if(info.format == VID_FORMAT_AAC_RAW){
                //audio data
                unsigned char adts[ADTS_HEADER_LEN];
                CopyADTSHeader(qbox_frame_size, adts);

                audio_cb(video_buf, 
                                (unsigned)qbox_frame_size,
                                VID_FORMAT_AAC_RAW, 
                                info.ts,
                                NULL,
                                adts);
                rx_aframe_count++;
            }

            if(!(count%50) && count){
                printf("Info: received total %d frames\n",count);
            }
            count++;
        }
    }

    
    ret = cam360_video_stop(CH1);
    if(ret)
       goto error;
    ret = cam360_audio_stop(CH2);
    if(ret)
        goto error;

    gettimeofday(&tv2, NULL);
    //print the results
    if(rx_vframe_count)
        printf("Received:\n\
                %d audio frames\n\
                %d video frames\n",rx_aframe_count,rx_vframe_count);
    printf("time %lds\n",(tv2.tv_sec-tv1.tv_sec));
    //wait for channels to close properly.
    sleep(1);
    close_file();
    //printf("reset\n");

    ret = libusb_release_interface(video_hdl, IPCAM_EAP_INTERFACE_NUM);
    ret = libusb_release_interface(video_hdl, IPCAM_IAP2_INTERFACE_NUM);
    close_device(); 

    return 0;

error:
    cam360_audio_stop(CH2);
    cam360_video_stop(CH1);

    close_file();
    ret = libusb_release_interface(video_hdl, IPCAM_EAP_INTERFACE_NUM);
    ret = libusb_release_interface(video_hdl, IPCAM_IAP2_INTERFACE_NUM);
    close_device(); 


    return -1;
}
