This application runs on x86 Linux host and tests
1. iOS EA class BULK IN/OUT EP's.
2. Audio & Video capture and streaming API's.

Compile:
make all 

Clean:
make clean

Run:
sudo ./cam_av_capture <frames-to-capture> <alpha-y-file> <alpha-uv-file>

e.g.
sudo ./cam_av_capture 100 /* This will capture 100 video frames coming from camera. */

