#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h> /* memset, memcmp */
#include <unistd.h> /* sleep */
#include <assert.h> /* assert */
#include <string.h>
#include <libusb-1.0/libusb.h>
#include <inttypes.h>
#include <signal.h>
#include <sys/time.h>
#include <stdbool.h>
#include <sys/stat.h>

#include "qmed.h"
#include "qmedext.h"
#include "qbox.h"
#include "usb_iap.h"

#define ERROR(ret_val, msg, ...)                                      \
do {                                                          \
	fprintf(stderr, "ERROR: " msg "\n", ##__VA_ARGS__);   \
	return ret_val;                                       \
} while(0)

static struct libusb_device_handle *video_hdl;
static struct libusb_context       *video_ctx;

#define MAX_NUM_CHANNELS 3
FILE *fd[MAX_NUM_CHANNELS];

#define R2_VID 0x29fe
#define R2_PID 0x4d53

#define USB_DIR_OUT			0		/* to device */
#define USB_DIR_IN			0x80		/* to host */

#define IAP2_BULK_IN_EP 0x02
#define IAP2_BULK_OUT_EP 0x01

#define EAP_BULK_IN_EP 0x03
#define EAP_BULK_OUT_EP 0x04

#define EAP_BULK_OUT_BUF_SIZE 1024*4

#define MAX_VIDEO_BUF_LENGTH 8000000 //
uint8_t data_buf[MAX_VIDEO_BUF_LENGTH];

//iAP2 class specific definations
enum interface_num_t{
    IPCAM_IAP2_INTERFACE_NUM = 0,
    IPCAM_EAP_INTERFACE_NUM = 1,
    MAX_NUM_INTRFC
};

void close_device()
{
    if(video_hdl)libusb_close(video_hdl);
    if(video_ctx)libusb_exit(video_ctx);
}

void close_file(void)
{
    int i = 0;   
    for(i=0;i<MAX_NUM_CHANNELS;i++)
    {
        if ( fd[i] != NULL)
            fclose(fd[i]);
    }
}


#define MAX_VIDEO_BUF_LENGTH 8000000 //
uint8_t data_buf[MAX_VIDEO_BUF_LENGTH];

int cam360_video_start(video_channel_t ch)
{
    ios_stream_state_cmd_t cmd;
    int ret;
    int transferred_len;

    cmd.hdr.id = IOS_ACC_START_STREAM;
    cmd.hdr.cmd_len = sizeof(ios_stream_state_cmd_t);
    cmd.mux_channel_id = (int)ch;
    cmd.type = VIDEO_CHANNEL;

    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)&cmd,
                             sizeof(ios_stream_state_cmd_t),
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for video_start failed %d\n",ret);
        return -1;
    }
    return 0;
}

int cam360_video_stop(video_channel_t ch)
{
    ios_stream_state_cmd_t cmd;
    int ret;
    int     transferred_len;

    cmd.hdr.id = IOS_ACC_STOP_STREAM;
    cmd.hdr.cmd_len = sizeof(ios_stream_state_cmd_t);
    cmd.mux_channel_id = (int)ch;
    cmd.type = VIDEO_CHANNEL;

    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)&cmd,
                             sizeof(ios_stream_state_cmd_t),
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for video_start failed %d\n",ret);
        return -1;
    }    

    return 0;
}

int cam360_video_set_sensor_param(sensor_params_t *sparam)
{
    ios_sensor_cmd_t cmd;
    int transferred_len;
    int ret;

    cmd.hdr.id = IOS_ACC_SENSOR_COMMAND;
    cmd.hdr.cmd_len = sizeof(ios_sensor_cmd_t);
    cmd.sparams = *sparam;

    printf("cam360_video_set_sensor_param:");
    if(sparam->type.b.exposure)
        printf(" exposure=%d",cmd.sparams.sensor_exposure);
    if(sparam->type.b.gain)
        printf(" gain=%d",cmd.sparams.sensor_gain);
    printf("\n");
    ret = libusb_bulk_transfer(video_hdl,
                         (USB_DIR_OUT + EAP_BULK_OUT_EP),
                         (unsigned char *)&cmd,
                         sizeof(ios_sensor_cmd_t),
                         &transferred_len,
                         2000
                        );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for set_sensor_param failed %d\n",ret);
        return -1;
    }
    return 0;
}

int cam360_chip_reset(void)
{
   ios_video_cmd_t cmd;
   int ret;
   int transferred_len;

   cmd.hdr.id = IOS_ACC_CHIP_RESET;
   cmd.hdr.cmd_len = sizeof(ios_video_cmd_t);
   cmd.data_length = 0;
   cmd.cmd_data = 0;

    printf("cam360_chip_reset\n");
    //send the command
    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)&cmd,
                             sizeof(ios_video_cmd_t),
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for chip reset failed %d\n",ret);
        return -1;
    }

    return 0;
}

int cam360_set_jsonid(json_id_t id)
{
    ios_jsonid_cmd_t cmd;
    int ret;
    int transferred_len;

    cmd.hdr.id = IOS_ACC_SET_JSON_ID;
    cmd.hdr.cmd_len = sizeof(ios_jsonid_cmd_t);
    cmd.json_id = id;

    ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_OUT + EAP_BULK_OUT_EP),
                             (unsigned char *)&cmd,
                             sizeof(ios_jsonid_cmd_t),
                             &transferred_len,
                             2000
                            );
    if(ret){
        printf("ERR: libusb_bulk_transfer out for set_jsonid failed %d\n",ret);
        return -1;
    }

    return 0;
}

sensor_params_t sparam;

static void ch_cb(unsigned char *buffer, unsigned int size,
        video_info_t info, int ch)
{
    static const char *basename = "out.chX";
    static const char *ext[NUM_VID_FORMAT] = {
            [VID_FORMAT_H264_RAW]    = ".h264",
            [VID_FORMAT_MJPEG_RAW]   = ".mjpeg",
            [AUD_FORMAT_AAC_RAW]     = ".aac"
    };
    static unsigned int unclippedEV_prev[NUM_IPCAM_VID_CHANNELS];         //Should be initialized to 0 by compiler

    video_format_t fmt = (video_format_t) info.format;
    if(fmt < FIRST_VID_FORMAT || fmt >= NUM_VID_FORMAT) {
        printf("Unknown Video format\n");
        return;
    }
    metadata_t *metadata = &(info.metadata);
    QMedExtSensorStruct *sensor_info;
    //Don't enable following two AWB related metadata if not needed

    METADATA_GET(metadata, sensor_info, QMedExtSensorStruct, QMED_EXT_TYPE_SENSOR);

    if (sensor_info) {
        if(sensor_info->unclippedEV_q24_8 != unclippedEV_prev[ch]) {
            unclippedEV_prev[ch] = sensor_info->unclippedEV_q24_8;
            printf("Sensor Info on channel CH%i\n", ch+1);
            printf("Unclipped EV = %.2f\n",
                   ((double)sensor_info->unclippedEV_q24_8) / 256);
            printf("Analog gain  = %.2f\n",
                   ((double)sensor_info->analogGain_q24_8) / 256);
            sparam.sensor_gain = (uint32_t)sensor_info->analogGain_q24_8;
            printf("Exposure     = %u\n"  , sensor_info->exposure);
            sparam.sensor_exposure = (uint32_t)sensor_info->exposure;
            printf("VTS          = %u\n"  , sensor_info->vts);
            printf("HTS          = %u\n\n", sensor_info->hts);
        }
    }

    if(fd[ch] == NULL) {
        char *fname = malloc(strlen(basename) + strlen(ext[fmt]) + 1);
        strcpy(fname, basename);
        strcpy(fname + strlen(fname), ext[fmt]);
        fname[6] = ((char) ch + 1) % 10 + 48;
        fd[ch] = fopen(fname, "w");
        free(fname);
    }
   
    fwrite(buffer, size, 1, fd[ch]);
}

int open_geo_ios_device(uint16_t vendor_id, uint16_t product_id)
{
    int ret;

    video_hdl = libusb_open_device_with_vid_pid(video_ctx, vendor_id,
							product_id);
    
    if(video_hdl == NULL){
        printf("Could not open USB device\n"
			"%x:%x", vendor_id, product_id);
        close_device();
        return -1;
     }

    /* Claim the interface */
    ret = libusb_claim_interface(video_hdl, IPCAM_IAP2_INTERFACE_NUM);
    if(ret != 0){
        printf("libusb_claim_interface error %d\n",ret);
        close_device(); 
        return -1;
    }
    ret = libusb_claim_interface(video_hdl, IPCAM_EAP_INTERFACE_NUM);
    if(ret != 0){
        printf("libusb_claim_interface error %d\n",ret);
        close_device(); 
        return -1;
    }

    //start channel
    ret = libusb_set_interface_alt_setting( video_hdl, IPCAM_EAP_INTERFACE_NUM, 1);
    if(ret != 0){
        printf("libusb_set_interface_alt_setting error %d\n",ret);
        close_device(); 
        return -1;
    }

    return 0;
}

static int quit_capture_app = 0;

void my_handler (int sig)
{
    quit_capture_app = 1;
    printf("Exiting app\n");   
}

int main(int argc, char **argv)
{
    uint16_t vendor_id = 0xdead, product_id = 0xbeef;
    int     transferred_len;
    int ret;
    int channel_id;
    video_info_t info;
    uint8_t *video_buf;
    uint32_t analytics;
    int frame_count = 100;
    int rx_vframe_count = 0;
    uint32_t qbox_frame_size = 0;
    uint32_t count=0;
    qmed_t qmed;

    ret = libusb_init(&video_ctx);
	if(ret < 0){
        ERROR(-1, "init_libusb failed");
		return -1;
    }

    vendor_id = 0x29fe;
    product_id = 0x4d53;

    	//add CTRL-C handler
    signal (SIGQUIT, my_handler);
    signal (SIGINT, my_handler);

    ret = open_geo_ios_device(vendor_id, product_id);

    if(ret){
        close_device();
        printf("Unable to find a geo device\n");
    }

    //start video 
    ret = cam360_video_start(CH1);

    while(frame_count > rx_vframe_count && (!quit_capture_app)){
        ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_IN + EAP_BULK_IN_EP),
                             (unsigned char *)&data_buf[0],
                             MAX_VIDEO_BUF_LENGTH,
                             &transferred_len,
                             5000
                            );

        if(ret){
            printf("ERR: libusb_bulk_transfer in audio/video capture failed %d\n",ret);
            break;
        }
       
        ret = qbox_parse_header(&data_buf[0], &channel_id,
                                &info.format, &video_buf, &qbox_frame_size, &info.ts,
                                &analytics, &(info.metadata), &qmed);
        /*if (ret) {
            printf("Wrong mux video format\n");
            break;
        }*/

        if(ret == 0){ // qbox audio/video data
            if(info.format == VID_FORMAT_H264_RAW || 
                info.format == VID_FORMAT_MJPEG_RAW){
                //video data
                ch_cb(video_buf, qbox_frame_size, info, channel_id);
                rx_vframe_count++;
            }

            if(!(count%50) && count){
                printf("Info: received %d frames\n",count);
            }
            count++;
        }
    }
    ret = cam360_video_stop(CH1);
    if(rx_vframe_count)
        printf("Received: %d video frames\n",rx_vframe_count);
    //reset camera 
    cam360_chip_reset();

	//wait for camera to come up.
    sleep(3);

    //detect camera again
    ret = open_geo_ios_device(vendor_id, product_id);

    if(ret){
        close_device();
        printf("Unable to find a geo device\n");
    }

    //set the mode
    //cam360_set_jsonid();

    //set the last saved exposure & gain
    sparam.type.d32 = 0;
    sparam.type.b.exposure = 1;
    sparam.type.b.gain = 1;
    ret=cam360_video_set_sensor_param(&sparam);
    
    usleep(100000); 
    //
    //start video 
    ret = cam360_video_start(CH1);

    //set mode
    rx_vframe_count = 0;
    count = 0;

    while(frame_count > rx_vframe_count && (!quit_capture_app)){
        ret = libusb_bulk_transfer(video_hdl,
                             (USB_DIR_IN + EAP_BULK_IN_EP),
                             (unsigned char *)&data_buf[0],
                             MAX_VIDEO_BUF_LENGTH,
                             &transferred_len,
                             5000
                            );

        if(ret){
            printf("ERR: libusb_bulk_transfer in audio/video capture failed %d\n",ret);
            break;
        }
       
        ret = qbox_parse_header(&data_buf[0], &channel_id,
                                &info.format, &video_buf, &qbox_frame_size, &info.ts,
                                &analytics, &(info.metadata), &qmed);
        /*if (ret) {
            printf("Wrong mux video format\n");
            break;
        }*/

        if(ret == 0){ // qbox audio/video data
            if(info.format == VID_FORMAT_H264_RAW || 
                info.format == VID_FORMAT_MJPEG_RAW){
                //video data
                ch_cb(video_buf, qbox_frame_size, info, channel_id);
                rx_vframe_count++;
            }

            if(!(count%50) && count){
                printf("Info: received %d frames\n",count);
            }
            count++;
        }
    }

    //stop video 
    ret = cam360_video_stop(CH1);
    if(rx_vframe_count)
        printf("Received: %d video frames\n",rx_vframe_count);
    close_file();
    ret = libusb_release_interface(video_hdl, IPCAM_EAP_INTERFACE_NUM);
    ret = libusb_release_interface(video_hdl, IPCAM_IAP2_INTERFACE_NUM);
    close_device(); 

    return 0;
}
