/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef ENGINEINTERFACE_H
#define ENGINEINTERFACE_H

#include <pthread.h>

#include "mxuvc.h"
#include "../engine/misc/queue.h"
#include <stdio.h>
#include <sys/time.h>

typedef struct
{
    int xOff;
    int yOff;
    int width;
    int height;
}Rect;

typedef struct
{
    int      nr;
    uint64_t ts;
    int      motion;
    Rect*   rectangles;
}Motion;

typedef struct
{
    int      bps;
    uint64_t fps;
    int      continuty;
    int      drops;
    int      iFrame;
    int      usbFrameCaptured;
    int      numFramesDelay;
    double      duration;
    int bps_min;
    int bps_max;
    int bufferFullness[2048];
    int framesForBufferFullness;
}Stats;

typedef void (*callback) (void*);
typedef void (*ui_stats_cb) (void*, Stats*);
typedef void (*ui_motionstats_cb) (void*, Motion*);
typedef struct video_session video_session_t;
typedef struct audio_session audio_session_t;

typedef void (*display_func_t)(video_session_t *ses, const void *buf, int len);
typedef void (*speaker_func_t)(audio_session_t *ses, const void *buf, int len);

struct video_session {
	video_channel_t      ch;
	void*                context;
	struct decode       *dec;
	uint16_t             width;
	uint16_t             height;
	video_format_t       fmt;
	int                  framedrops;
        video_info_t         info;
        ui_motionstats_cb    motion_func;
        ui_stats_cb          stats_func;
	display_func_t       display_func;
        callback             ui_cb;
        queue_t              *q; 

	//Continuity counter for TS streams
	int                  lastcc;

        //stats
        Stats                stats;

        //dumps
	int                  indump;
	int                  outdump;

	//Processing thread
	pthread_t            thread;
	pthread_mutex_t      mutex;
	pthread_cond_t       cond;
        int                  active;

        //encoded frame buffer
	unsigned char        *frame;
	signed long          len;
    int            continuitycnt;
    long long timestamp;
	int	       isKeyFrame;

        //decoded frame buffer
	char *decframe;
	int  declen;
	int *decoded_width;
	int *decoded_height;

    //mp4dump
	int file_dump;
	int flush;
	struct timeval file_dump_start_time;

	// if set to 1, perform yuv to rgb conversion in the UI
	int accelerate;

	// Timer thread
	pthread_t timer;
	int bytes;

	pthread_mutex_t     stats_mutex;
	pthread_cond_t      stats_cond;

	int bufferFullnessEnabled;
	int bufferFullnessDuration;
	int bufferFullnessParamChanged;
};


struct audio_session {
	void*               context;
	struct decode       *dec;
	audio_format_t      fmt;
	int                 framedrops;

	speaker_func_t      speaker_func;
        callback            ui_cb;
	
	//Processing thread
	pthread_t           thread;
	pthread_mutex_t     mutex;
	pthread_cond_t      cond;
        int                 active;

        //encoded frame buffer
	unsigned char       *frame;
	signed long         len;

        int                 samp_freq;
        audio_channel_t     ch;
        uint64_t            sam_interval; 
        uint64_t            prev_ts;
        uint64_t            permissible_range; //in percentage
        uint64_t            upper_sam_interval;
        uint64_t            lower_sam_interval;

        //decoded frame buffer
	char *decframe;
	int  declen;
    //dumps
	int                  outdump;
	char dumppath[128];
};


#ifdef __cplusplus 
extern "C" {
#endif
int registeredCallback(callback fptr,void* classPtr,int chID,char *databuffer, int *decodedWidth, int *decodedHeight, int dataBufferSize, ui_stats_cb stats_fptr, ui_motionstats_cb motion_fptr);
int unRegister(int chID);

int registeredAudioCallback(callback fptr,void* classPtr,char *databuffer, int dataBufferSize);
int deRegisterAudio();

int engine_init(int id);
int engine_deinit(void);
int engine_getvideo_channels(void);
int engine_get_camera_mode(camer_mode_t *mode);
int engine_startvideo(int ch);
int engine_stopvideo(int ch);
int engine_startaudio(int audch,int sr,char*);
int engine_stopaudio(void);
int engine_getvideo_channel_count(void);
int engine_getvideo_channel_info(video_channel_t channel, video_channel_info_t* info);
int engine_getvideo_resolution(int ch, int *w, int*h);
int engine_setvideo_resolution(int ch, int w, int h);
int engine_getvideo_fps(int ch, int* fps);
int engine_setvideo_fps(int ch, int fps);
int engine_getvideo_bps(int ch, int* bitrate);
int engine_setvideo_bps(int ch, int bitrate);
int engine_getvideo_gop(int ch, int* gop);
int engine_setvideo_gop(int ch, int gop);
int engine_getvideo_profile(int ch, int* profile);
int engine_setvideo_profile(int ch, int profile);
int engine_getvideo_compression_quality(int ch, int* quality);
int engine_setvideo_compression_quality(int ch, int quality);
int engine_getvideo_avc_level(int ch, int* level);
int engine_setvideo_avc_level(int ch, int level);
int engine_setvideo_force_iframe(int ch);
int engine_getvideo_format(int ch, int* format);
int engine_setvideo_format(int ch, int format);
int engine_setdewarp_mode(int ch, int mode, char* path);

// Audio APIs
int engine_get_audio_samplingrate(int audch);
int engine_start_capture_audio();
int engine_stop_capture_audio();
int engine_audio_get_volume(); // Get the current mic gain/volume
int engine_audio_set_volume(int volume); // set the mic gain/volume. 0-100
int engine_audio_mute(int mute); // mute/unmute mic.
int engine_audio_mute_left(int mute);
int engine_audio_mute_right(int mute);
int engine_agc(int enable);
int engine_is_audio_initialized(); // whether the audio init was successful

int engine_set_wb(white_balance_mode_t mode, uint32_t val);
int engine_get_wb(white_balance_mode_t *mode, uint32_t *val);
int engine_set_wdr(wdr_mode_t mode, uint32_t val);
int engine_get_wdr(wdr_mode_t *mode, uint32_t *val);
int engine_set_zone_wb(zone_wb_set_t sel, uint32_t value);
int engine_get_zone_wb(zone_wb_set_t *sel, uint32_t *val);
int engine_set_nf(noise_filter_mode_t sel, uint32_t value);
int engine_get_nf(noise_filter_mode_t *sel, uint32_t *val);
#if 0 //Disbaled as there is no MXUVC support for this fetaure
int engine_set_zone_exp(zone_exp_set_t sel, uint32_t value);
int engine_get_zone_exp(zone_exp_set_t *sel, uint32_t *val);
int engine_set_exposure(exp_set_t sel, uint32_t value);
int engine_get_exposure(exp_set_t *sel, uint32_t *val);
#endif
int engine_set_gain_multiplier(uint32_t value);
int engine_get_gain_multiplier(uint32_t *val);
int engine_set_sharpen_filter(uint32_t value);
int engine_get_sharpen_filter(uint32_t *val);
int engine_set_histogram_eq(int value);
int engine_get_histogram_eq(int *val);
int engine_set_max_analog_gain(uint32_t value);
int engine_get_max_analog_gain(uint32_t *val);
int engine_set_flip_vertical(int value);
int engine_get_flip_vertical(int *val);
int engine_set_flip_horizontal(int value);
int engine_get_flip_horizontal(int *val);

int engine_start_capture_video(int ch);
int engine_stop_capture_video(int ch);

int engine_start_smartmotion(int ch, int width, int height);
int engine_stop_smartmotion();

//ISP setting calls
int engine_set_brightness(int32_t);
int engine_get_brightness(int32_t*);
int engine_set_contrast(uint32_t);
int engine_get_contrast(uint32_t*);
int engine_set_hue(int32_t);
int engine_get_hue(int32_t*);
int engine_set_saturation(uint32_t);
int engine_get_saturation(uint32_t*);
int engine_set_gama(uint32_t);
int engine_get_gama(uint32_t*);
int engine_set_sharpness(uint32_t);
int engine_get_sharpness(uint32_t*);

// dewarp calls
int engine_get_dewarp_params(video_channel_t ch, int panel, dewarp_mode_t* mode, dewarp_params_t* params);
int engine_set_dewarp_params(video_channel_t ch, int panel, dewarp_mode_t mode, dewarp_params_t* params);
int engine_get_config_params(video_channel_t ch, config_params_t* params);

// compositior apis
/**
 * @brief Get information (mode, size) about a specific panel in a composited channel
 *
 * @param ch        The video channel
 * @param panel     The panel to get information about
 * @param mode      The panel mode (@panel_mode_t)
 * @param params    The panel params (@panel_params_t)
 * @return int      0 if success, -1 if failure.
 */
int engine_get_compositor_params(video_channel_t ch, int panel, panel_mode_t* mode, panel_params_t* params);

/**
 * @brief Set the mode and/or size of a specific panel in a composited channel
 *
 * @param ch        The video channel
 * @param panel     The panel to be updated
 * @param mode      The panel mode (@panel_mode_t)
 * @param params    The panel params (@panel_params_t)
 * @return int      0 if success, -1 if failure.
 */
int engine_set_compositor_params(video_channel_t ch, int panel, panel_mode_t mode, panel_params_t* params);

// Image overlay APIs

/**
 * @brief Init overlay
 */
int engine_overlay_init();

/**
 * @brief Deinit overlay
 */
int engine_overlay_deinit();

/**
 * @brief Add an overlay image to a channel
 *
 * @param ch        The channel to add the overlay image to
 * @param width     Width of the image
 * @param height    Height of the image
 * @param xoff X    offset/position of the image
 * @param yoff Y    offset/position of the image
 * @param filename  Image filename
 * @param id        Reference to the added image
 * @param alpha     Image alpha value
 *
 * @return          0 if success, -1 otherwise
 */
int engine_overlay_add_image(int ch, int width, int height, int xoff, int yoff, char* filename, int* id, int alpha, char *alphafile);

/**
 * @brief Remove an overlay image from a channel
 *
 * @param ch        The channel to remove the overlay image from
 * @param id        Reference to the added image (from engine_overlay_add_image)
 *
 * @return          0 if success, -1 otherwise
 */
int engine_overlay_remove_image(int ch, int id);

/**
 * @brief Sets the alpha value for the overlay image from a channel
 *
 * @param ch        The channel on which alpha needs to be set
 * @param index     The logo index where alpha needs to be set
 * @param alpha     The alpha value for the overlay image
 *
 * @return          0 if success, -1 otherwise
 */
int engine_overlay_image_alpha(int ch, int index, int alpha);

// Text Burn-in APIs

/**
 * @brief Load a font for a channel
 *
 * @param channel       The video channel
 * @param font_file     The font file path
 * @param font_size     The font size
 *
 * @return              0 if success, -1 otherwise
 */
int engine_overlay_load_font(int channel, char* font_file, int font_size);

/**
 * @brief Add burn-in text to a channel
 *
 * @param ch        The channel to add the text to
 * @param text      The text to add
 * @param xOff      The x offset at which to render the text
 * @param yOff      The y offset at which to render the text
 * @param id        Reference to the added text
 *
 * @return          0 if success, -1 otherwise
 */
int engine_overlay_add_text(int ch, char* text, int xOff, int yOff, int* id);

/**
 * @brief Remove burn-in text that was added to a channel
 *
 * @param ch        The channel to remove the text from
 * @param id        Reference to the added text
 *
 * @return          0 if success, -1 otherwise
 */
int engine_overlay_remove_text(int ch, int id);

/**
 * @brief Set the burn-in timer
 *
 * @param ch                The channel to set the time to
 * @param xOff              The x offset at which to render the time
 * @param yOff              The y offset at which to render the time
 * @param hours             The hours component of time
 * @param minutes           The minutes component of time
 * @param seconds           The seconds component of time
 * @param frameNumEnabled   Whether to render number of frames as well
 *
 * @return          0 if success, -1 otherwise
 */
int engine_overlay_set_time(int ch, int xOff, int yOff, int hours, int minutes, int seconds, bool frameNumEnabled);

/**
 * @brief Start rendering the timer
 *        Note: engine_overlay_set_time must be called before this
 *
 * @param ch    The channel to start the timer on
 *
 * @return      0 if success, -1 otherwise
 */
int engine_overlay_show_time(int ch);

/**
 * @brief Stop rendering the timer
 *
 * @param ch    The channel to stop the timer on
 *
 * @return      0 if success, -1 otherwise
 */
int engine_overlay_hide_time(int ch);

int engine_get_max_privacy_masks();
int engine_overlay_privacy_add_mask_rect(int ch, uint32_t xoff, uint32_t yoff, uint32_t width, uint32_t height, uint32_t color, int idx);
//int engine_overlay_privacy_add_mask_polygon(int ch, int points[][2], int numPoints, int idx);
int engine_overlay_privacy_update_mask_color(int ch, unsigned int color, int idx);
int engine_overlay_privacy_remove_mask(int ch, int idx);



// Video dump

/**
 * @brief Start the elementry stream to mp4 conversion
 *
 * @param ch        The video channel
 * @param filePath  The path to the dumpfile
 * @return int      0 if success, -1 if failure.
 */
int engine_start_mp4_dump_video(int ch, const char* filePath);

/**
 * @brief Stop the elementry stream to mp4 conversion
 *
 * @param ch        The video channel
 * @return int      0 if success, -1 if failure.
 */
int engine_stop_mp4_dump_video(int ch);

/**
 * @brief Update the UI render callback
 *  
 * @param ch        The video channel
 * @param fptr      The callback function pointer
 **/
void engine_update_render_callback(int channel, callback fptr);

void engine_flush_queue(int channel);

/**
 * @brief Set whether to optimize the frame decode.
 *        Currently the only optimization is that the decoded frame is sent
 *        to the UI directly without yuv to rgb conversion
 *  
 * @param channel        The video channel
 * @param optimize       Whether to optimize or not
 **/
void engine_set_optimize(int channel, int optimize);

void engine_set_interval_bitrate(int interval);
void engine_set_interval_bitrate_min_max(int interval);

void engine_enable_buffer_fullness(int channel, int enable, int interval);

#ifdef __cplusplus 
}
#endif
#endif
