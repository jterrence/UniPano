/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef __CAMERA_H_
#define __CAMERA_H_

#ifdef  __cplusplus
extern "C" {
#endif

int findCameraDevices(const char* name);

#ifdef  __cplusplus
}
#endif

#ifndef  __cplusplus
extern int camId[];

#endif

#endif
