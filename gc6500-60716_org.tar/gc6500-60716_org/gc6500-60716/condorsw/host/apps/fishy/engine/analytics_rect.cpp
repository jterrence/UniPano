/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <assert.h>
#include "analytics.h"
#include "smrectangle.h"

static Motion MotionInfo;
static Rect   RectInfo[50];
static DataBuffer buffer[MAX_BUFFERS];
static pthread_mutex_t mutex;
static pthread_cond_t waitForData;
static int writeIdx = 0;
static int readIdx = 0;
static int verbose = 0;

static int frameCount;              //Count for frames (to be reset after every 30 frames)
static int objectDetected[30];       //Array containing object detected info for the last 30 frames
static int objectDetectedIndex;      //Index for writing new info in above array
static int motionDetected;            //Flag indicating valid motion detected based on object detected info above
static int transition;               //Last alert transition
static Region region[MAX_NUM_REGIONS];

static pthread_t threadId;
static int       smActive = false;

static int nr=0;
static smrectangle_t *curRectData;

//local functions decls.
static void* processRawFrame(void* userData);
static int smOpen(void);
static int smClose(void);
static int smProcess(DataBuffer *prevBuffer, DataBuffer *currentBuffer);

int smInit(video_session_t* ses)
{
    int ret = 0;
    for(int i=0; i<MAX_BUFFERS;i++)
    {
        buffer[i].empty = true;
        memset(&buffer[i].frame,0,sizeof(RawFrameInfo));
    }
    MotionInfo.rectangles = &RectInfo[0];
    smOpen();
    smAddMotionROI(0, 0, 0, (ses->width/16), (ses->height/16));
    smActive = 1;
    //Spawn a thread to process captured RAW frames and detects smart motion using smart motion library
    ret = pthread_create (&threadId, NULL, &processRawFrame, ses);
    if ( ret < 0 )
        return ret;

    return 0;
}

int smDeinit(void)
{
    smActive = 0;
    pthread_join(threadId, NULL);
    smRemoveMotionROI(0);
    smClose();
    return 0;
}



//rawCapture_cb: This is the callback function registed with mxuvc to receive YUV frames captured
//data    :pointer to the buffer containing YUV frame; must be freed once processed
//length  :length of the buffer
//info    :other metadata containing motion statistics

void rawCapture_cb(unsigned char *data, unsigned int length, video_info_t info, void *userData)
{
    video_session_t * ses = ((video_session_t*)userData);


    if(!ses->active || !smActive)
    {
    	pthread_cond_signal(&waitForData);
        free(data);
    	return;
    }

    pthread_mutex_lock(&mutex);
    if(buffer[writeIdx].empty)
    {
        buffer[writeIdx].empty = false;
        buffer[writeIdx].frame.datalength = length;
        buffer[writeIdx].frame.pdata = data;
        buffer[writeIdx].frame.info.buf_index = info.buf_index;
        buffer[writeIdx].frame.info.format = VID_FORMAT_GREY_RAW;
        buffer[writeIdx].frame.info.ts = info.ts;
        buffer[writeIdx].frame.info.stats.buf = info.stats.buf;
        buffer[writeIdx].frame.info.stats.size = info.stats.size;
        buffer[writeIdx].frame.info.rect.buf = info.rect.buf;
        buffer[writeIdx].frame.info.rect.size = info.rect.size;
        writeIdx++;
        if(writeIdx >= MAX_BUFFERS)
        {
            writeIdx = 0;
        }
        pthread_cond_signal(&waitForData);
    }
    else
    {
        LOG_ERR("Buffer %d not empty and so drop the frame\n",writeIdx);
        free(data);
    }

    pthread_mutex_unlock(&mutex);
}



void* processRawFrame(void* userData)
{
    video_session_t *ses = ((video_session_t *)userData);

    while(smActive)
    {
        DataBuffer *buf1=NULL, *buf2=NULL;

        pthread_mutex_lock(&mutex);

        if(!ses->active || !smActive) break;

        buf1 = &buffer[readIdx];
        if(!buf1->empty)
        {
        	int nextRead = ((readIdx+1)>=MAX_BUFFERS) ? 0:(readIdx+1);
        	buf2 = &buffer[nextRead];
        	if(buf2->empty)
        		pthread_cond_wait(&waitForData, &mutex);
        }
        else
        {
        	pthread_cond_wait(&waitForData, &mutex);
        	pthread_mutex_unlock(&mutex);
        	continue;
        }

        pthread_mutex_unlock(&mutex);

        smProcess(buf1, buf2);
        free(buf1->frame.pdata);

        pthread_mutex_lock(&mutex);
        buf1->empty = true;
        readIdx++;
        if(readIdx >= MAX_BUFFERS)
        {
            readIdx = 0;
        }
        pthread_mutex_unlock(&mutex);

        //UI update support
        #if 1
        if(nr != 0)
        {
            smrectangle_t *ptr;
            int num;        //No of rectangles with 6th bit indicating start of motion
            num = nr;
            if(motionDetected)
            {
                num |= 0x40;         //Set bit 6 indicating valid motion detected
            }
            
            //FIXME: make sure nr does not exceed available static memory in array. 
            //So for now we have cap of 50 rectangles per frame.
            //nr = (nr < sizeof(RectInfo) ) ? nr : sizeof(RectInfo);

            MotionInfo.nr = nr;
            MotionInfo.motion  = motionDetected;
            for(int i=0;i<nr;i++)
            {
                ptr = &curRectData[i];
                MotionInfo.rectangles[i].xOff= ptr->location.top_left_mbx;
                MotionInfo.rectangles[i].yOff= ptr->location.top_left_mby;
                MotionInfo.rectangles[i].width = ptr->location.bot_right_mbx - ptr->location.top_left_mbx + 1;
                MotionInfo.rectangles[i].height = ptr->location.bot_right_mby - ptr->location.top_left_mby + 1;
                   
            }

            //update UI
            if(ses!=NULL && ses->motion_func != NULL && ses->context != NULL)
            ses->motion_func(ses->context, &MotionInfo);
           
        }
        #endif
    
    }

    return NULL;
}



//smOpen: This function will create and initialize the interanal resources

int smOpen(void)
{
    int i;

    motionDetected = 0;
    transition = 0;
    objectDetectedIndex = 0;
    frameCount = 0;
    for(i=0; i < MAX_NUM_REGIONS; i++)
    {
        region[i].xOff = 0;
        region[i].yOff = 0;
        region[i].width = 0;
        region[i].height = 0;
    }


    return 0;
}

//smClose: This function will free up interanal resources

int smClose(void)
{

    return 0;
}


//smProcess: This function process the raw frames and detects the smart motion

int smProcess(DataBuffer *prevBuffer, DataBuffer *currentBuffer)
{
    int overlap=0;
    int i,j=0;

    curRectData = (smrectangle_t *)currentBuffer->frame.info.rect.buf;

    if( curRectData == NULL)
    {
        LOG_ERR("ERR: No rectangles received\n");
        return -1;
    }



    //Get rectangles
    
    nr = 0;
    for(i=0;i<MAX_SMRECTANGLES;i++)
    {
        smrectangle_t *ptr;
        ptr  = &curRectData[i];
        if(ptr->location.top_left_mbx == 0 && ptr->location.top_left_mby ==0 && 
            ptr->location.bot_right_mbx == 0 && ptr->location.bot_right_mby == 0)
            break;
        nr++;
    }
        
    if(nr != 0)
    {
        smrectangle_t *ptr;

        //LOG_TRACE("\n*** %d rectangles (x y width height) for smart motion:\n", nr);
        printf("\n*** %d rectangles (x y width height) for smart motion:\n", nr);

        overlap=0;
        for(i=0;i<nr;i++)
        {
            ptr = &curRectData[i];

            //LOG_TRACE("Rect %d: %d %d %d %d\n", i+1,
            printf("Rect %d: %d %d %d %d\n", i+1,
                            ptr->location.top_left_mbx, ptr->location.top_left_mby,
                            ptr->location.bot_right_mbx - ptr->location.top_left_mbx + 1,    //width
                            ptr->location.bot_right_mby - ptr->location.top_left_mby + 1     //height
            );

            //Compare rectangle with the regions rectangles for overlap
            if(overlap == 0)     //Check only if no previous overlap
            {
                for(j=0;j<MAX_NUM_REGIONS;j++)
                {
                    if(ptr->location.top_left_mbx < region[j].xOff+region[j].width
                        && ptr->location.bot_right_mbx >= region[j].xOff
                        && ptr->location.top_left_mby < region[j].yOff+region[j].height
                        && ptr->location.bot_right_mby >= region[j].yOff)
                    {
                        //Overlap - no need to check with remaining rectangles
                        overlap=1;

                        LOG_TRACE("*** Rectangle %d %d %d %d overlapping with region %d %d %d %d\n",
                                ptr->location.top_left_mbx, ptr->location.top_left_mby,
                                ptr->location.bot_right_mbx - ptr->location.top_left_mbx + 1,    //width
                                ptr->location.bot_right_mby - ptr->location.top_left_mby + 1,     //height
                                region[j].xOff, region[j].yOff, region[j].width, region[j].height
                        );
                        break;
                    }
                }
            }
        }
    }


    //If at least SMARTMOTION_START_THRESHOLD frames with objects detected in last 30 frames, then signal
    //a motion start event.

     if(frameCount < 30)
         frameCount++;
     assert(objectDetectedIndex < 30);
     objectDetected[objectDetectedIndex] = overlap;

     objectDetectedIndex = (objectDetectedIndex + 1) % 30;
     if(frameCount == 30)
     {
         //Calculate number of frames with objects in last window
         int count=0;
         for(i=0; i<30; i++)
         {
             if(objectDetected[i])
                 count++;
         }
         if(count >= SMARTMOTION_START_THRESHOLD)
         {
             if(!motionDetected)
             {
                 motionDetected=1;
                 LOG_TRACE("*** %d or more frames with motion detected - %d\n", SMARTMOTION_START_THRESHOLD, count);
             }
         }
         else if(count <= SMARTMOTION_END_THRESHOLD)
         {
             if(motionDetected)
             {
                 motionDetected=0;
                 LOG_TRACE("*** Less than or equal to %d frames with motion detected - %d\n", SMARTMOTION_END_THRESHOLD, count);
             }
         }
    }

    if(transition != motionDetected)
    {
        transition = motionDetected;
        LOG_INFO("\n[Motion Dectected] state - %s\n",(transition ? "motion started":"motion stopped"));
    }

    return 0;
}



//smAddMotionROI: This function is to set motion region

int smAddMotionROI(int regId, int xOff, int yOff, int width, int height)
{
    if(regId < MAX_NUM_REGIONS-1)
    {
        region[regId].xOff = xOff;
        region[regId].yOff = yOff;
        region[regId].width = width;
        region[regId].height = height;
    }

    return 0;
}



//smRemoveMotionROI: This function is to remove motion region

int smRemoveMotionROI(int regId)
{
    if(regId < MAX_NUM_REGIONS-1)
    {
        region[regId].xOff = 0;
        region[regId].yOff = 0;
        region[regId].width = 0;
        region[regId].height = 0;
    }
    return 0;
}
