/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef __HASH_H__
#define __HASH_H__

typedef struct item* hash_table_t;

hash_table_t* hash_create();
void hash_destroy(hash_table_t *h);
int hash_set(hash_table_t *hash_table, const char *key, void *val);
int hash_get(hash_table_t *hash_table, const char *key, void **val);

#endif
