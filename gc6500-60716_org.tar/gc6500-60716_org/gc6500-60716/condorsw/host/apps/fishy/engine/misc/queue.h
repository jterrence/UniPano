/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef __QUEUE_H__
#define __QUEUE_H__

typedef struct queue queue_t;

queue_t* queue_create();
void queue_destroy(queue_t *q);
int queue_enqueue(queue_t *q, void *val);
int queue_dequeue(queue_t *q, void **val);
int check_queue_empty(queue_t *q);
int get_queue_length(queue_t *q);

#endif
