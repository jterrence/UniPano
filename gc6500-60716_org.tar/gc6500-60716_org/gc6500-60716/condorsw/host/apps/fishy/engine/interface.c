/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include "../include/interface.h"
#include <stdio.h>

extern video_session_t *sId[10];
extern audio_session_t *aud_ses;

int registeredCallback(callback fptr, void* classPtr, int chid, char *databuffer, int *decodedWidth, int *decodedHeight, int dataBufferSize, ui_stats_cb stats_cb, ui_motionstats_cb motion_fptr) {
    printf("registeredCallback: for channel %d\n", chid);
    if (sId[chid] != NULL) {
        //Call the engine API to register for particular channel the callback
        sId[chid]->context = classPtr;
        sId[chid]->ui_cb = fptr;
        sId[chid]->decframe = databuffer;
        sId[chid]->declen = dataBufferSize;
        sId[chid]->decoded_width = decodedWidth;
        sId[chid]->decoded_height = decodedHeight;
        sId[chid]->stats_func = stats_cb;
        sId[chid]->motion_func = motion_fptr;
        return 0;
    }
    printf("registeredCallback: session is already registered for channel %d\n", chid);
    return 1;
}

int unRegister(int chid) {
    //Call the engine API to deregister for the given channel id
    if (sId[chid] != NULL) {
        sId[chid]->context = NULL;
        sId[chid]->ui_cb = NULL;
        sId[chid]->decframe = NULL;
        sId[chid]->declen = 0;
        sId[chid]->stats_func = NULL;
        sId[chid]->motion_func = NULL;
        return 0;
    }
    printf("unRegister: session is already deleted for channel %d\n", chid);
    return 1;
}

int registeredAudioCallback(callback fptr, void* classPtr, char *databuffer, int dataBufferSize) {
    if (aud_ses->ui_cb == NULL && aud_ses->context == NULL) {
        aud_ses->ui_cb = fptr;
        aud_ses->context = classPtr;
        aud_ses->decframe = databuffer;
        aud_ses->declen = dataBufferSize;
        return 0;
    }
    return 1;
}

int deRegisterAudio() {
    if (aud_ses != NULL) {
        aud_ses->ui_cb = NULL;
        aud_ses->context = NULL;
        aud_ses->decframe = NULL;
        aud_ses->declen = 0;
        return 0;
    }
    return 1;
}
