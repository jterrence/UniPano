/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include "opus_encoder.h"
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#define PACKAGE_NAME "opus-tools"
#define PACKAGE_VERSION "0.1.9"

#define readint(buf, base) (((buf[base+3]<<24)&0xff000000)| \
                           ((buf[base+2]<<16)&0xff0000)| \
                           ((buf[base+1]<<8)&0xff00)| \
                           (buf[base]&0xff))
#define writeint(buf, base, val) do{ buf[base+3]=((val)>>24)&0xff; \
                                     buf[base+2]=((val)>>16)&0xff; \
                                     buf[base+1]=((val)>>8)&0xff; \
                                     buf[base]=(val)&0xff; \
                                 }while(0)

static unsigned int char_to_int(unsigned char ch[4]) {
    return ((unsigned int) ch[0] << 24) | ((unsigned int) ch[1] << 16) | ((unsigned int) ch[2] << 8) | (unsigned int) ch[3];
}

typedef struct {
    unsigned char *data;
    int maxlen;
    int pos;
} Packet;

typedef struct {
    const unsigned char *data;
    int maxlen;
    int pos;
} ROPacket;

static int write_uint32(Packet *p, ogg_uint32_t val) {
    if (p->pos > p->maxlen - 4) {
        return 0;
    }
    p->data[p->pos] = (val) & 0xFF;
    p->data[p->pos + 1] = (val >> 8) & 0xFF;
    p->data[p->pos + 2] = (val >> 16) & 0xFF;
    p->data[p->pos + 3] = (val >> 24) & 0xFF;
    p->pos += 4;
    return 1;
}

static int write_uint16(Packet *p, ogg_uint16_t val) {
    if (p->pos > p->maxlen - 2) {
        return 0;
    }
    p->data[p->pos] = (val) & 0xFF;
    p->data[p->pos + 1] = (val >> 8) & 0xFF;
    p->pos += 2;
    return 1;
}

static int write_chars(Packet *p, const unsigned char *str, int nb_chars) {
    int i;
    if (p->pos > p->maxlen - nb_chars) {
        return 0;
    }
    for (i = 0; i < nb_chars; i++) {
        p->data[p->pos++] = str[i];
    }
    return 1;
}

static int read_uint32(ROPacket *p, ogg_uint32_t *val) {
    if (p->pos > p->maxlen - 4) {
        return 0;
    }
    *val = (ogg_uint32_t) p->data[p->pos];
    *val |= (ogg_uint32_t) p->data[p->pos + 1] << 8;
    *val |= (ogg_uint32_t) p->data[p->pos + 2] << 16;
    *val |= (ogg_uint32_t) p->data[p->pos + 3] << 24;
    p->pos += 4;
    return 1;
}

static int read_uint16(ROPacket *p, ogg_uint16_t *val) {
    if (p->pos > p->maxlen - 2)
        return 0;
    *val = (ogg_uint16_t) p->data[p->pos];
    *val |= (ogg_uint16_t) p->data[p->pos + 1] << 8;
    p->pos += 2;
    return 1;
}

static int read_chars(ROPacket *p, unsigned char *str, int nb_chars) {
    int i;
    if (p->pos > p->maxlen - nb_chars) {
        return 0;
    }
    for (i = 0; i < nb_chars; i++) {
        str[i] = p->data[p->pos++];
    }
    return 1;
}

OPUSEncoder::OPUSEncoder() {
    rate = 16000;
    coding_rate = 48000;
    frame_size = 960;
    max_ogg_delay = 48000; /*48kHz samples*/
    packet = NULL;
    id = -1;
    comment_padding = 0;
}

OPUSEncoder::~OPUSEncoder() {
    ogg_stream_clear(&os);
    free(inopt.comments);
    free(packet);
}

int OPUSEncoder::mstInit(int channels, int r) {
    const char *opus_version = "1.1";
    int max_frame_bytes, serialno;
    char ENCODER_string[1024];

    id = -1;
    max_ogg_delay = 48000;

    this->rate = r;
    coding_rate = rate;

    inopt.channels = channels;
    inopt.rate = coding_rate;
    /* 0 dB gain is recommended unless you know what you're doing */
    inopt.gain = 0;
    inopt.samplesize = 16;
    inopt.endianness = 0;
    inopt.rawmode = 0;
    inopt.ignorelength = 0;
    inopt.copy_comments = 1;
    inopt.copy_pictures = 0;
    comment_padding = 512;

    /*Vendor string should just be the encoder library,
     the ENCODER comment specifies the tool used.*/
    comment_init(&inopt.comments, &inopt.comments_length, opus_version);
    snprintf(ENCODER_string, sizeof(ENCODER_string), "opusenc from %s %s", PACKAGE_NAME, PACKAGE_VERSION);
    const char* tag = "ENCODER";
    comment_add(&inopt.comments, &inopt.comments_length, tag, ENCODER_string);

    inopt.skip = 0;

    if (inopt.rate > 24000)
        coding_rate = 48000;
    else if (inopt.rate > 16000)
        coding_rate = 24000;
    else if (inopt.rate > 12000)
        coding_rate = 16000;
    else if (inopt.rate > 8000)
        coding_rate = 12000;
    else
        coding_rate = 8000;

    frame_size = frame_size / (48000 / coding_rate);

    /*OggOpus headers*//*FIXME: broke forcemono*/
    header.channels = channels;
    header.channel_mapping = header.channels > 8 ? 255 : channels > 2;
    header.input_sample_rate = rate;
    header.gain = inopt.gain;

    header.nb_streams = 1;
    header.nb_coupled = 1;

    max_frame_bytes = (1275 * 3 + 7) * header.nb_streams;
    packet = (unsigned char*) malloc(sizeof(unsigned char) * max_frame_bytes);

    if (packet == NULL) {
        fprintf(stderr, "Error allocating packet buffer. %d\n", max_frame_bytes);
        return -1;
    }

    /*Regardless of the rate we're coding at the ogg timestamping/skip is
     always timed at 48000.*/
    header.preskip = inopt.skip * (48000. / coding_rate);

    /* Extra samples that need to be read to compensate for the pre-skip */
    inopt.extraout = (int) header.preskip * (rate / 48000.);

    serialno = rand();

    /*Initialize Ogg stream struct*/
    if (ogg_stream_init(&os, serialno) == -1) {
        fprintf(stderr, "Error: stream init failed\n");
        return -1;
    }

    return 0;
}

int OPUSEncoder::oe_write_page(ogg_page *page, unsigned char *buffer, int start) {
    memcpy(&buffer[start], page->header, page->header_len);
    memcpy(&buffer[start + page->header_len], page->body, page->body_len);
    return page->header_len + page->body_len;
}

void OPUSEncoder::comment_init(char **comments, int* length, const char *vendor_string) {
    /*The 'vendor' field should be the actual encoding library used.*/
    int vendor_length = strlen(vendor_string);
    int user_comment_list_length = 0;
    int len = 8 + 4 + vendor_length + 4;
    char *p = (char*) malloc(len);
    if (p == NULL) {
        fprintf(stderr, "malloc failed in comment_init()\n");
        exit(1);
    }
    memcpy(p, "OpusTags", 8);
    writeint(p, 8, vendor_length);
    memcpy(p + 12, vendor_string, vendor_length);
    writeint(p, 12 + vendor_length, user_comment_list_length);
    *length = len;
    *comments = p;
}

void OPUSEncoder::comment_add(char **comments, int* length, const char *tag, char *val) {
    char* p = *comments;
    int vendor_length = readint(p, 8);
    int user_comment_list_length = readint(p, 8+4+vendor_length);
    int tag_len = (tag ? strlen(tag) + 1 : 0);
    int val_len = strlen(val);
    int len = (*length) + 4 + tag_len + val_len;

    p = (char*) realloc(p, len);
    if (p == NULL) {
        fprintf(stderr, "realloc failed in comment_add()\n");
        exit(1);
    }

    writeint(p, *length, tag_len + val_len); /* length of comment */
    if (tag) {
        memcpy(p + *length + 4, tag, tag_len); /* comment tag */
        (p + *length + 4)[tag_len - 1] = '='; /* separator */
    }
    memcpy(p + *length + 4 + tag_len, val, val_len); /* comment */
    writeint(p, 8 + 4 + vendor_length, user_comment_list_length + 1);
    *comments = p;
    *length = len;
}

void OPUSEncoder::comment_pad(char **comments, int* length, int amount) {
    if (amount > 0) {
        int i;
        int newlen;
        char* p = *comments;
        /*Make sure there is at least amount worth of padding free, and
         round up to the maximum that fits in the current ogg segments.*/
        newlen = (*length + amount + 255) / 255 * 255 - 1;
        p = (char*) realloc(p, newlen);
        if (p == NULL) {
            fprintf(stderr, "realloc failed in comment_pad()\n");
            exit(1);
        }
        for (i = *length; i < newlen; i++)
            p[i] = 0;
        *comments = p;
        *length = newlen;
    }
}

int OPUSEncoder::opus_header_parse(const unsigned char *header1, int len, OpusHeader *h) {
    int i;
    char str[9];
    ROPacket p;
    unsigned char ch;
    ogg_uint16_t shortval;

    p.data = packet;
    p.maxlen = len;
    p.pos = 0;
    str[8] = 0;
    if (len < 19)
        return 0;
    read_chars(&p, (unsigned char*) str, 8);
    if (memcmp(str, "OpusHead", 8) != 0)
        return 0;

    if (!read_chars(&p, &ch, 1))
        return 0;
    h->version = ch;
    if ((h->version & 240) != 0) /* Only major version 0 supported. */
        return 0;

    if (!read_chars(&p, &ch, 1))
        return 0;
    h->channels = ch;
    if (h->channels == 0)
        return 0;

    if (!read_uint16(&p, &shortval))
        return 0;
    h->preskip = shortval;

    if (!read_uint32(&p, &h->input_sample_rate))
        return 0;

    if (!read_uint16(&p, &shortval))
        return 0;
    h->gain = (short) shortval;

    if (!read_chars(&p, &ch, 1))
        return 0;
    h->channel_mapping = ch;

    if (h->channel_mapping != 0) {
        if (!read_chars(&p, &ch, 1))
            return 0;

        if (ch < 1)
            return 0;
        h->nb_streams = ch;

        if (!read_chars(&p, &ch, 1))
            return 0;

        if (ch > h->nb_streams || (ch + h->nb_streams) > 255)
            return 0;
        h->nb_coupled = ch;

        /* Multi-stream support */
        for (i = 0; i < h->channels; i++) {
            if (!read_chars(&p, &h->stream_map[i], 1))
                return 0;
            if (h->stream_map[i] > (h->nb_streams + h->nb_coupled) && h->stream_map[i] != 255)
                return 0;
        }
    } else {
        if (h->channels > 2)
            return 0;
        h->nb_streams = 1;
        h->nb_coupled = h->channels > 1;
        h->stream_map[0] = 0;
        h->stream_map[1] = 1;
    }
    /*For version 0/1 we know there won't be any more data
     so reject any that have data past the end.*/
    if ((h->version == 0 || h->version == 1) && p.pos != len)
        return 0;
    return 1;
}

int OPUSEncoder::opus_header_to_packet(const OpusHeader *h, unsigned char *header_data, int len) {
    int i;
    Packet p;
    unsigned char ch;

    p.data = header_data;
    p.maxlen = len;
    p.pos = 0;
    if (len < 19)
        return 0;
    if (!write_chars(&p, (const unsigned char*) "OpusHead", 8))
        return 0;
    /* Version is 1 */
    ch = 1;
    if (!write_chars(&p, &ch, 1))
        return 0;

    ch = h->channels;
    if (!write_chars(&p, &ch, 1))
        return 0;

    if (!write_uint16(&p, h->preskip))
        return 0;

    if (!write_uint32(&p, h->input_sample_rate))
        return 0;

    if (!write_uint16(&p, h->gain))
        return 0;

    ch = h->channel_mapping;
    if (!write_chars(&p, &ch, 1))
        return 0;

    if (h->channel_mapping != 0) {
        ch = h->nb_streams;
        if (!write_chars(&p, &ch, 1))
            return 0;

        ch = h->nb_coupled;
        if (!write_chars(&p, &ch, 1))
            return 0;

        /* Multi-stream support */
        for (i = 0; i < h->channels; i++) {
            if (!write_chars(&p, &h->stream_map[i], 1))
                return 0;
        }
    }

    return p.pos;
}

int OPUSEncoder::add_ogg_header(unsigned char* dst) {
    int ret;
    unsigned char header_data[100];
    int packet_size = opus_header_to_packet(&header, header_data, 100);
    op.packet = header_data;
    op.bytes = packet_size;
    op.b_o_s = 1;
    op.e_o_s = 0;
    op.granulepos = 0;
    op.packetno = 0;
    ret = ogg_stream_packetin(&os, &op);

    int bytes = 0;
    while ((ret = ogg_stream_flush(&os, &og))) {
        if (!ret)
            break;
        ret = oe_write_page(&og, dst, bytes);
        if (ret != og.header_len + og.body_len) {
            fprintf(stderr, "Error: failed writing header to output stream %lu\n", og.header_len + og.body_len);
            return -1;
        }
        bytes += ret;
    }

    comment_pad(&inopt.comments, &inopt.comments_length, comment_padding);
    op.packet = (unsigned char *) inopt.comments;
    op.bytes = inopt.comments_length;
    op.b_o_s = 0;
    op.e_o_s = 0;
    op.granulepos = 0;
    op.packetno = 1;
    ogg_stream_packetin(&os, &op);

    /* writing the rest of the opus header packets */
    while ((ret = ogg_stream_flush(&os, &og))) {
        if (!ret) {
            break;
        }
        ret = oe_write_page(&og, dst, bytes);
        if (ret != og.header_len + og.body_len) {
            fprintf(stderr, "Error: failed writing header to output stream\n");
            return -1;
        }
        bytes += ret;
    }

    return bytes;
}

int last_segments = 0;
ogg_int64_t enc_granulepos = 0;
ogg_int64_t last_granulepos = 0;
int OPUSEncoder::transcode(unsigned char* src, unsigned char* dst, unsigned int size, int add_header) {
    int ret;
    ogg_int64_t original_samples = 0;
    int bytesWritten = 0;

    if (add_header) {
        bytesWritten = this->add_ogg_header(dst);
        //fprintf(stdout, "Added ogg header of size: %d\n", bytesWritten);
    }

    unsigned int index = 0;

    /*Main encoding loop (one frame per iteration)*/
    while (!op.e_o_s) {
        int size_segments, cur_frame_size, numBytes;
        int max_payload_bytes = 1500;
        unsigned char ch[4];
        id++;

        if (index >= size) {
            op.e_o_s = 1;
            break;
        }

        memcpy(ch, &src[index], 4);
        index += 4;
        numBytes = char_to_int(ch);
        if (numBytes > max_payload_bytes || numBytes < 0) {
            fprintf(stderr, "Invalid payload length: %d\n", numBytes);
            break;
        }

        index += 4;

        if (index >= size) {
            fprintf(stderr, "Ran out of input\n");
            op.e_o_s = 1;
            break;
        }

        memcpy(packet, &src[index], numBytes);
        index += numBytes;
        cur_frame_size = frame_size;
        enc_granulepos += cur_frame_size * 48000 / coding_rate;
        size_segments = (numBytes + 255) / 255;

        /*Flush early if adding this packet would make us end up with a
         continued page which we wouldn't have otherwise.*/
        while ((((size_segments <= 255) && (last_segments + size_segments > 255)) || (enc_granulepos - last_granulepos > max_ogg_delay))
                && ogg_stream_flush_fill(&os, &og, 255 * 255)) {

            if (ogg_page_packets(&og) != 0) {
                last_granulepos = ogg_page_granulepos(&og);
            }
            last_segments -= og.header[26];
            ret = oe_write_page(&og, dst, bytesWritten);
            if (ret != og.header_len + og.body_len) {
                fprintf(stderr, "Error: failed writing data to output stream\n");
                return -1;
            }
            bytesWritten += ret;
        }

        op.packet = (unsigned char *) packet;
        op.bytes = numBytes;
        op.b_o_s = 0;
        op.granulepos = enc_granulepos;
        if (op.e_o_s) {
            /*We compute the final GP as ceil(len*48k/input_rate). When a resampling
             decoder does the matching floor(len*input/48k) conversion the length will
             be exactly the same as the input.*/
            op.granulepos = ((original_samples * 48000 + rate - 1) / rate) + header.preskip;
        }
        op.packetno = 2 + id;
        ret = ogg_stream_packetin(&os, &op);
        last_segments += size_segments;

        /*If the stream is over or we're sure that the delayed flush will fire,
         go ahead and flush now to avoid adding delay.*/
        //while ((op.e_o_s|| (enc_granulepos + (frame_size * 48000 / coding_rate) - last_granulepos > max_ogg_delay) || (last_segments >= 255)) ?
        //        ogg_stream_flush_fill(&os, &og, 255 * 255) : ogg_stream_pageout_fill(&os, &og, 255 * 255)) {
        while(ogg_stream_flush_fill(&os, &og, 255 * 255)) {
            if (ogg_page_packets(&og) != 0) {
                last_granulepos = ogg_page_granulepos(&og);
            }
            last_segments -= og.header[26];
            ret = oe_write_page(&og, dst, bytesWritten);
            if (ret != og.header_len + og.body_len) {
                fprintf(stderr, "Error: failed writing data to output stream\n");
                return -1;
            }
            bytesWritten += ret;
        }
    }

    //fprintf(stdout, "Total bytes written: %d\n", bytesWritten);
    op.e_o_s = 0;
    return bytesWritten;
}

// This will be used when this class is called from C code
OPUSEncoder* object;
void cpluscplus_init_ogg_encoder(int channels, int sampling_rate) {
    if (object == NULL) {
        object = new OPUSEncoder();
        object->mstInit(channels, sampling_rate);
    }
}
void cpluscplus_deinit_ogg_encoder() {
    if (object != NULL) {
        delete object;
        object = NULL;
    }
}
int cplusplus_ogg_encode(unsigned char* src, unsigned char* dst, unsigned int size, unsigned int add_header) {
    if (object == NULL) {
        fprintf(stderr, "Ogg encoder not initialized\n");
        return -1;
    }

    return object->transcode(src, dst, size, add_header);
}
