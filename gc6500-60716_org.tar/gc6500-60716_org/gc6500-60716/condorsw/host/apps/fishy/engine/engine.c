/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
//#define LOAD_BALANCING 
//#define WITH_OPENGL
//#define PCM_AUDIO_STREAMING //Define this if we want audio play
//#define CHECK_H264_ANALYSIS 
#ifdef LOAD_BALANCING
#define _GNU_SOURCE
#endif

#define CHECK_H264_DISCONTINUITY 
#ifndef NOX
#ifdef __i386__
#define ANALYTICS
#include "analytics.h"
#endif
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <sched.h>
#include <unistd.h>
#include <linux/unistd.h>
#include <sys/syscall.h>
#include <sys/time.h>
#include <math.h>
#include "camera.h"
#include "decode.h"
#include "colorspace.h"
#include "../include/interface.h"
#include "pcm.h"
#include "queue.h"
#include "list.h"
#include "encoders/opus_encoder.h"

#include "engine_mp4_dump.h"

#ifdef WITH_OPENGL
#include <GL/gl.h>
#include <GL/glext.h>
#include <SDL/SDL.h>
#endif

#define UNUSED __attribute__((unused))

typedef struct video_frame video_frame_t;

struct video_frame {
    unsigned char *buf;
    int size;
    uint64_t timestamp;
};
pthread_mutex_t gMutex;
audio_session_t *aud_ses = NULL;
video_session_t *sId[10];
audio_format_t currentAudioFormat = NUM_AUD_FORMAT;
int ogg_header_added = 0;
int ogg_initialized = 0;
unsigned char encoded_data[1024];
static int smEnabled = 0;
int core_id = 0;
static int max_channels = 0;
static int devtype = -1;
void* audio_handle = NULL;
#define OBJECTTYPE_AACLC (2)
#define ADTS_HEADER_LEN (7)
#define BOARD_SAMPLE_RATE (16000)

#define TIME_INTERVAL_AVG   1        //Time interval in seconds after which average bitrate and fps is calculated
#define TIME_INTERVAL_BITRATE_MIN_MAX  10 // Time period to calculate bitrate min/max
#define TIME_INTERVAL_BUFFER_FULLNESS  4 // Time period to update buffer fullness to UI

#define DEFAULT_VENDOR_ID 0x29fe
#define DEFAULT_PRODUCT_ID 0x4d53

static unsigned char adtsHeader[ADTS_HEADER_LEN];
static const int SampleFreq[12] = {
        96000,
        88200,
        64000,
        48000,
        44100,
        32000,
        24000,
        22050,
        16000,
        12000,
        11025,
        8000 };
static int init_display(video_session_t *ses);

FILE* audio_fd = NULL;

// Flag to indicate whether audio is initialized successfully
int audio_initialized = -1;

int time_interval_bitrate_min_max = TIME_INTERVAL_BITRATE_MIN_MAX;
int time_interval_bitrate = TIME_INTERVAL_AVG;
int time_interval_buffer_fullness = TIME_INTERVAL_BUFFER_FULLNESS;

#ifdef WITH_OPENGL
static int glYUVtoRGB(char* buf, int width, int height)
{
    SDL_Surface *Win=NULL;
    GLubyte *Ytex,*Utex,*Vtex;
    SDL_Event evt;
    int i;
    GLhandleARB FSHandle,PHandle;
    char *s;
    char *FProgram=
    "uniform sampler2DRect Ytex;\n"
    "uniform sampler2DRect Utex,Vtex;\n"
    "void main(void) {\n"
    "  float nx,ny,r,g,b,y,u,v;\n"
    "  vec4 txl,ux,vx;"
    "  nx=gl_TexCoord[0].x;\n"
    "  ny=1280.0-gl_TexCoord[0].y;\n"
    "  y=texture2DRect(Ytex,vec2(nx,ny)).r;\n"
    "  u=texture2DRect(Utex,vec2(nx/2.0,ny/2.0)).r;\n"
    "  v=texture2DRect(Vtex,vec2(nx/2.0,ny/2.0)).r;\n"

    "  y=1.1643*(y-0.0625);\n"
    "  u=u-0.5;\n"
    "  v=v-0.5;\n"

    "  r=y+1.5958*v;\n"
    "  g=y-0.39173*u-0.81290*v;\n"
    "  b=y+2.017*u;\n"

    "  gl_FragColor=vec4(r,g,b,1.0);\n"
    "}\n";

    if(!SDL_Init(SDL_INIT_VIDEO)) {

        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,1);

        Win=SDL_SetVideoMode(width,height,32,SDL_HWSURFACE|SDL_ANYFORMAT|SDL_OPENGL);

        if(Win) {
            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            glOrtho(0,width,0,height,-1,1);
            glViewport(0,0,width,height);
            glClearColor(0,0,0,0);
            glColor3f(1.0,0.84,0.0);
            glHint(GL_POLYGON_SMOOTH_HINT,GL_NICEST);

            /* Set up program objects. */
            PHandle=glCreateProgramObjectARB();
            FSHandle=glCreateShaderObjectARB(GL_FRAGMENT_SHADER_ARB);

            /* Compile the shader. */
            glShaderSourceARB(FSHandle,1,&FProgram,NULL);
            glCompileShaderARB(FSHandle);

            /* Print the compilation log. */
            glGetObjectParameterivARB(FSHandle,GL_OBJECT_COMPILE_STATUS_ARB,&i);
            s=malloc(32768);
            glGetInfoLogARB(FSHandle,32768,NULL,s);
            printf("Compile Log: %s\n", s);
            free(s);

            /* Create a complete program object. */
            glAttachObjectARB(PHandle,FSHandle);
            glLinkProgramARB(PHandle);

            /* And print the link log. */
            s=malloc(32768);
            glGetInfoLogARB(PHandle,32768,NULL,s);
            printf("Link Log: %s\n", s);
            free(s);

            /* Finally, use the program. */
            glUseProgramObjectARB(PHandle);

            /* Load the textures. */
            Ytex=malloc(width*height);
            Utex=malloc((width*height)/4);
            Vtex=malloc((width*height)/4);

            memcpy(Ytex, buf, width*height);
            memcpy(Utex,buf, ((width*height)/4));
            memcpy(Vtex,buf, ((width*height)/4));

            /* This might not be required, but should not hurt. */
            glEnable(GL_TEXTURE_2D);

            /* Select texture unit 1 as the active unit and bind the U texture. */
            glActiveTexture(GL_TEXTURE1);
            i=glGetUniformLocationARB(PHandle,"Utex");
            glUniform1iARB(i,1); /* Bind Utex to texture unit 1 */
            glBindTexture(GL_TEXTURE_RECTANGLE_NV,1);

            glTexParameteri(GL_TEXTURE_RECTANGLE_NV,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
            glTexParameteri(GL_TEXTURE_RECTANGLE_NV,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
            glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_DECAL);
            glTexImage2D(GL_TEXTURE_RECTANGLE_NV,0,GL_LUMINANCE,width/2,height/2,0,GL_LUMINANCE,GL_UNSIGNED_BYTE,Utex);

            /* Select texture unit 2 as the active unit and bind the V texture. */
            glActiveTexture(GL_TEXTURE2);
            i=glGetUniformLocationARB(PHandle,"Vtex");
            glBindTexture(GL_TEXTURE_RECTANGLE_NV,2);
            glUniform1iARB(i,2); /* Bind Vtext to texture unit 2 */

            glTexParameteri(GL_TEXTURE_RECTANGLE_NV,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
            glTexParameteri(GL_TEXTURE_RECTANGLE_NV,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
            glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_DECAL);
            glTexImage2D(GL_TEXTURE_RECTANGLE_NV,0,GL_LUMINANCE,width/2,height/2,0,GL_LUMINANCE,GL_UNSIGNED_BYTE,Vtex);

            /* Select texture unit 0 as the active unit and bind the Y texture. */
            glActiveTexture(GL_TEXTURE0);
            i=glGetUniformLocationARB(PHandle,"Ytex");
            glUniform1iARB(i,0); /* Bind Ytex to texture unit 0 */
            glBindTexture(GL_TEXTURE_RECTANGLE_NV,3);

            glTexParameteri(GL_TEXTURE_RECTANGLE_NV,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
            glTexParameteri(GL_TEXTURE_RECTANGLE_NV,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
            glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_DECAL);
            glTexImage2D(GL_TEXTURE_RECTANGLE_NV,0,GL_LUMINANCE,width,height,0,GL_LUMINANCE,GL_UNSIGNED_BYTE,Ytex);

            /* Clean up before exit. */

            glUseProgramObjectARB(0);
            glDeleteObjectARB(PHandle);

            free(Ytex);
            free(Utex);
            free(Vtex);

        } else {
            fprintf(stderr,"Unable to create primary surface. \"%s\".\n",SDL_GetError());
        }
        SDL_Quit();
    } else {
        fprintf(stderr,"Initialisation failed. \"%s\".\n",SDL_GetError());
    }

    return(0);
}

#endif

void get_adts_header(audio_params_t *h, unsigned char *adtsheader) {
    int i = 0;
    int samplingFreqIndex = 0;

    // clear the adts header
    for (i = 0; i < ADTS_HEADER_LEN; i++) {
        adtsHeader[i] = 0;
    }

    // bits 0-11 are sync
    adtsHeader[0] = 0xff;
    adtsHeader[1] = 0xf0;

    // bit 12 is mpeg4 which means clear the bit

    // bits 13-14 is layer, always 00

    // bit 15 is protection absent (no crc)
    adtsHeader[1] |= 0x1;

    // bits 16-17 is profile which is 01 for AAC-LC
    adtsHeader[2] = 0x40;

    // bits 18-21 is sampling frequency index
    for (i = 0; i < 12; i++) {
        if (SampleFreq[i] == h->samplefreq) {
            samplingFreqIndex = i;
            break;
        }
    }

    adtsHeader[2] |= (samplingFreqIndex << 2);

    // bit 22 is private

    // bit 23-25 is channel config.  However since we are mono or stereo
    // bit 23 is always zero
    adtsHeader[3] = h->channelno << 6;

    // bits 26-27 are original/home and zeroed

    // bits 28-29 are copyright bits and zeroed

    // bits 30-42 is sample length including header len.  First we get qbox length,
    // then sample length and then add header length

    // adjust for headers
    int frameSize = h->framesize + ADTS_HEADER_LEN;

    // get upper 2 bits of 13 bit length and move them to lower 2 bits
    adtsHeader[3] |= (frameSize & 0x1800) >> 11;

    // get middle 8 bits of length
    adtsHeader[4] = (frameSize & 0x7f8) >> 3;

    // get lower 3 bits of length and put as 3 msb
    adtsHeader[5] = (frameSize & 0x7) << 5;

    // bits 43-53 is buffer fulless but we use vbr so 0x7f
    adtsHeader[5] |= 0x1f;
    adtsHeader[6] = 0xfc;

    // bits 54-55 are # of rdb in frame which is always 1 so we write 1 - 1 = 0
    // which means do not write

}

static void audio_cb(unsigned char *buffer, unsigned int size, audio_format_t format, uint64_t ts, void *user_data, audio_params_t *param) {
    audio_session_t *ses = (audio_session_t *) user_data;
    assert(ses != NULL);
    if (ses->fmt == AUD_FORMAT_PCM_RAW) {
        assert(format == AUD_FORMAT_PCM_RAW);
    } else {
        assert(format == AUD_FORMAT_AAC_RAW || format == AUD_FORMAT_OPUS_RAW);
    }

    currentAudioFormat = format;

    if (0 == pthread_mutex_trylock(&ses->mutex)) {

        switch (format) {
        case AUD_FORMAT_AAC_RAW:
            ses->sam_interval = (uint64_t) (((float) (1024 * 1000 / ses->samp_freq)) * 90);
            uint64_t percent = (uint64_t) (ses->sam_interval * ses->permissible_range) / 100;
            ses->upper_sam_interval = (uint64_t) (ses->sam_interval + percent);
            ses->lower_sam_interval = (uint64_t) (ses->sam_interval - percent);

            ses->prev_ts = ts;
            get_adts_header(param, adtsHeader);
            ses->len = ADTS_HEADER_LEN + size;
            ses->frame = malloc(ses->len);

            if (ses->frame) {
                memcpy(ses->frame, adtsHeader, sizeof(adtsHeader));
                memcpy(&ses->frame[ADTS_HEADER_LEN], buffer, size);
            }
            break;

        case AUD_FORMAT_PCM_RAW:
            ses->frame = malloc(size);
            ses->len = size;
            memcpy(ses->frame, buffer, ses->len);
            break;

        case AUD_FORMAT_OPUS_RAW: {
            if (ogg_initialized == 0) {
                cpluscplus_init_ogg_encoder(param->channelno, param->samplefreq);
                ogg_initialized = 1;
            }

            if (ogg_header_added) {
                size = cplusplus_ogg_encode(buffer, encoded_data, size, 0);
            } else {
                size = cplusplus_ogg_encode(buffer, encoded_data, size, 1);
                ogg_header_added = 1;
            }

            ses->frame = malloc(size);
            ses->len = size;
            memcpy(ses->frame, &encoded_data[0], ses->len);
        }
            break;

        default:
            printf("Audio format not supported\n");
            break;
        }

        if (ses->frame != NULL ) {
#if 1
            if (ses->outdump) {
                if (audio_fd == NULL ) {
                    audio_fd = fopen(ses->dumppath, "w");
                }
                if (audio_fd != NULL) {
                    fwrite(ses->frame, ses->len, 1, audio_fd);
                    fflush(audio_fd);
                    if (ses->ui_cb != NULL ) {
                        ses->ui_cb(ses->context);
                    }
                } else {
                    printf("\nERROR: cannot open audio file to dump\n");
                }
            }
#endif
            // Wake up the processing thread only if buffer is populated
//       		  pthread_cond_signal(&ses->cond);

        } else {
            printf("WARNING: buffer alloc failed for channel %d, size %d\n", (int) ses->ch, (int) ses->len);
        }

        pthread_mutex_unlock(&ses->mutex);

    } else {
        ses->framedrops++;
        printf("WARNING: Dropping frame on audio channel, total: %d\n", ses->framedrops);
    }

    return;
}

#ifdef CHECK_H264_DISCONTINUITY
static void chkH264Discontinuity(video_session_t *ses) {
    int currContinuityCnt;
    unsigned char* buf = ses->frame;

    ses->isKeyFrame = 0;
    /* check if the incoming frame has NAL Type Slice*/
    //Add +6 for skipping delimiters
    if (0x01 == (buf[4 + 6] & 0x1F)) {
        currContinuityCnt = ((buf[5 + 6] >> 1) & 0xF);
        //printf("currContinuityCnt = 0x%x\n",currContinuityCnt);
        if (currContinuityCnt != (ses->continuitycnt + 1)) {
            printf(" H264 Discontinuity Found on channel %d"
                    "currContinuityCnt = 0x%x   prevContinuityCnt "
                    "= 0x%x \n", ses->ch, currContinuityCnt, ses->continuitycnt);

        }
        if (0XF == currContinuityCnt) {
            ses->continuitycnt = -1;
        } else {
            ses->continuitycnt = currContinuityCnt;
        }
    }
    /* check if the incoming frame has NAL Type SPS*/
    else if (0x07 == (buf[4 + 6] & 0x1F)) {
        ses->isKeyFrame = 1;
        ses->continuitycnt = 0;
        //printf("\nGot I Frame %d\n",ses->ch);
    } else {

    }
}
#endif

static void video_cb(unsigned char *buffer, unsigned int size, video_info_t info, void *user_data) {
    video_format_t fmt = info.format;
    video_session_t *ses = (video_session_t *) user_data;
    assert(ses != NULL);

#if 0
    struct timeval now;
    //if(ses->ch == 1) {
    gettimeofday(&now, NULL);
    uint32_t time = now.tv_sec * 1000000 + now.tv_usec;
    printf("CH %X, DATALEN %X, TIME:%X TS: %X %X\n", (uint32_t)ses->ch, size, time, (uint32_t)((info.ts) >> 32) & 0xFFFFFFF, (uint32_t)(info.ts & 0xFFFFFFFF));
    //}
#endif

    /* The video format was changed */
    if (fmt != ses->fmt)
        init_display(ses);

    int mismatch = 0;
    unsigned int expected_size = 0;

    if (size != (expected_size = ses->width * ses->height) && (ses->fmt == VID_FORMAT_GREY_RAW))
        mismatch = 1;
    else if (size != (expected_size = ses->width * ses->height * 2) && (ses->fmt == VID_FORMAT_YUY2_RAW))
        mismatch = 1;
    else if (size != (expected_size = ses->width * ses->height * 1.5) && (ses->fmt == VID_FORMAT_NV12_RAW))
        mismatch = 1;
    else if (size == 0)
        mismatch = 1;

	//printf("ses->width: %d, ses->height: %d, size: %d\n", ses->width, ses->height, size);

    if (mismatch) {
        printf("\nFISHY: Dropping Frame --> Incorrect frame size %d, expected %d\n", size, expected_size);
        ses->framedrops++;
        size = expected_size;
        mxuvc_video_cb_buf_done(ses->ch, info.buf_index);
        return;
    }

    if (0 == pthread_mutex_trylock(&ses->mutex)) {
        /* Copy the data */
        ses->info = info;
        video_frame_t* frame = (video_frame_t*) malloc(sizeof(video_frame_t));

        if (frame != NULL ) {
            frame->buf = (unsigned char*) malloc(size);
            frame->size = size;
            frame->timestamp = info.ts;

            if (frame->buf != NULL ) {
                memcpy(frame->buf, buffer, size);
                queue_enqueue(ses->q, (void*) frame);
            } else {
                free(frame);
            }

            mxuvc_video_cb_buf_done(ses->ch, info.buf_index);
            // Wake up the processing thread only if buffer is populated
            ses->stats.usbFrameCaptured++;
            pthread_cond_signal(&ses->cond);

        } else {
            printf("WARNING: buffer alloc failed for channel %d, size %d\n", (int) ses->ch, (int) ses->len);
        }

        pthread_mutex_unlock(&ses->mutex);

    } else {
        ses->framedrops++;
        mxuvc_video_cb_buf_done(ses->ch, info.buf_index);
        //printf("WARNING: Dropping frame on channel %d, total: %d\n", ses->ch, ses->framedrops);
    }
    return;
}

static void close_fds() {
    if (audio_fd != NULL ) {
        fclose(audio_fd);
        audio_fd = NULL;
    }
}

static void decode(video_session_t *ses, const void *buf, int len) {
    struct framebuffer *frm;

    frm = decode_frame(ses->dec, buf, len, ses->accelerate ? (void*) ses->decframe : NULL, ses->accelerate ?  ses->decoded_width:NULL, ses->accelerate ?  ses->decoded_height:NULL);

    if (frm != NULL) {

        if (ses->ui_cb != NULL ) {

            if (ses->decframe != NULL && ses->declen > 0) {
                if (ses->active) {
                    if (ses->accelerate) {
                        //ses->declen = frm->size;
                        //memcpy((unsigned*)ses->decframe, frm->data, frm->size);
                    } else {
                        colorspace_to_rgb((unsigned*) ses->decframe, frm, FOURCC_I420);
                    }
                } else {
                    memset(ses->decframe, 0, ses->declen);
                }
            }
        }

        free(frm->data);
        free(frm);
        frm = NULL;
    }
        
    if (ses->ui_cb != NULL) {
        ses->ui_cb(ses->context);
    }
}

static void demux_ts(video_session_t *ses, const void *buf, int len) {
    static long long lastbytepos = 0;
    static long long bytepos = 0;
    int i, ns = 0;
    void *nal = malloc(len);

    /* demux ts stream */
    for (i = 0; i < len; i += 188) {
        const unsigned char *p = buf + i;
        int start = p[1] & 0x40;
        int pid = (((int) p[1] & 0x1f) << 8) | p[2];

        // add up bits in the transport stream ignoring the trailer in the uvc packet
        // but including all PIDs:
        //videoTransportStreamBitsThisWindow += 188*8;

        // FIXME - hardcoded PID
        if (pid != 0x1011)
            continue;
        int af = p[3] & 0x20;
        int pl = p[3] & 0x10;
        int cc = p[3] & 0x0f;
        // printf("lastcc %d cc %d pl %d\n", lastcc, cc, pl);
        if ((ses->lastcc != -1) && (pl == 0x10) && (((ses->lastcc + 1) & 0xf) != cc)) {
            printf("** MPEG2-TS continuity error ");
            printf("(expected %d vs %d) at byte %lld\n", (ses->lastcc + 1) & 0xf, cc, bytepos + i);
            printf("   (%lld bytes/%lld packets from last error)\n", bytepos + i - lastbytepos, (bytepos + i - lastbytepos) / 188);
        }
        ses->lastcc = cc;
        lastbytepos = bytepos;
        int ps = 184;
        if (!pl)
            continue;

        p += 4;
        if (af) {
            ps -= p[0] + 1;
            p += p[0] + 1;
        }
        // PES is here
        if (start) {
            if (ns) {
                decode(ses, nal, ns);
                ns = 0;
            }
            // FIXME is this always 20?
            ps -= 20;
            p += 20;
        }
        memcpy(nal + ns, p, ps);
        ns += ps;
    }
    decode(ses, nal, ns);
    free(nal);
    bytepos += len;
}

static void process_raw(video_session_t *ses, const void *buf, int len) {
    if (ses->accelerate) {
        memcpy((void*) ses->decframe, buf, len);
    } else {
        struct framebuffer *frm = malloc(sizeof(struct framebuffer));
        if (!frm) {
            printf("Failed to allocate memory for frame buffer: %s()\n", __FUNCTION__);
            return;
        }

        switch (ses->fmt) {
        case VID_FORMAT_NV12_RAW:
            frm->pixelformat = FOURCC_NV12;
            break;
        case VID_FORMAT_YUY2_RAW:
            frm->pixelformat = FOURCC_YUY2;
            break;
        case VID_FORMAT_GREY_RAW:
            frm->pixelformat = FOURCC_Y800;
            break;

        default:
            printf("Non raw format detected in %s(): %i.\n", __FUNCTION__, (int) ses->fmt);
            abort();
            break;
        }

        if (frm) {
            frm->width = ses->width;
            frm->height = ses->height;
            frm->size = len;
            frm->data = malloc(len);
            memcpy(frm->data, buf, len);

            if (ses->ui_cb != NULL ) {
                if (ses->decframe != NULL && ses->declen > 0) {
                    colorspace_to_rgb((unsigned*) ses->decframe, frm, frm->pixelformat);
                }
            }

            free(frm->data);
            free(frm);
            frm = NULL;
        }
    }

    //produce copy of raw frame for analytics.
    //analytics thread will delete the buffer.
#ifdef ANALYTICS
    if (smEnabled) {
        int luma = ses->width * ses->height;
        // assert(luma > len);
        char* sm = malloc(luma);
        memcpy(sm, buf, luma);
        rawCapture_cb((unsigned char *) sm, (unsigned int) luma, ses->info, (void*) ses);
    }
#endif

    // Send frame to UI to render
    if (ses->ui_cb != NULL) {
        ses->ui_cb(ses->context);
    }
}

static void process_encoded(video_session_t *ses, const void *buf, int len) {
    if (!len) {
        printf("process_encoded: zero size encoded data ch: %d\n", ses->ch);
        return;
    }

    if (*(const char*) buf == 0x47 && len % 188 == 0)
        demux_ts(ses, buf, len);
    else
        decode(ses, buf, len);
}

UNUSED
static void process_aac(audio_session_t *ses, const void *buf, int len) {
    struct framebuffer *frm	= NULL ;
    if (!len)
        return;
#ifdef PCM_AUDIO_STREAMING
    frm = decode_frame(ses->dec, buf, len);

    if (frm) {

        if(ses->ui_cb != NULL) {

            if(ses->decframe!=NULL && ((ses->declen > 0) && (frm->size <= ses->declen)))
            memcpy(ses->decframe, frm->data, frm->size);
            ses->ui_cb(ses->context);
        }

#ifdef PCM_AUDIO_STREAMING
        if(audio_handle)
        pcmwrite(audio_handle, frm->data, frm->size);
#endif
        free(frm->data);
        free(frm);
        frm= NULL;
    }
#endif
    if (ses->ui_cb != NULL ) {
        if (ses->decframe != NULL && ((ses->declen > 0) && (frm->size <= ses->declen)))
            memcpy(ses->decframe, frm->data, frm->size);
        if (ses->ui_cb != NULL ) {
            ses->ui_cb(ses->context);
        }
    }

}

UNUSED
static void process_pcm(audio_session_t *ses, const void *buf, int len) {
    if (!len)
        return;

    if (buf) {

        if (ses->ui_cb != NULL ) {
            if (ses->decframe != NULL && ((ses->declen > 0) && (len <= ses->declen)))
                memcpy(ses->decframe, buf, len);
            if (ses->ui_cb != NULL ) {
                ses->ui_cb(ses->context);
            }
        }
#ifdef PCM_AUDIO_STREAMING
        if(audio_handle)
        pcmwrite(audio_handle, buf, len);
#endif
    }
}

static int init_display(video_session_t *ses) {
    int ret;

    ret = mxuvc_video_get_format(ses->ch, &(ses->fmt));
    if (ret < 0) {
        printf("ERROR: Unable to get the current video format\n");
        return -1;
    }

    ret = mxuvc_video_get_resolution(ses->ch, &(ses->width), &(ses->height));
    if (ret < 0) {
        printf("ERROR: Unable to get the current video resolution\n");
        return -1;
    }
    printf("video resolution %d :%d :%d\n", ses->ch, ses->width, ses->height);
    assert(ses->width != 0 && ses->height != 0);

    if (ses->dec) {
        decode_destroy(ses->dec);
        ses->dec = NULL;
    }
    /* Initialize the decoder and create the window for the display */
    const char *type_str;

    switch (ses->fmt) {
    case VID_FORMAT_MJPEG_RAW:
        ses->dec = decode_create(DEC_MJPEG);
        type_str = "MJPEG";
        ses->display_func = process_encoded;
        break;
    case VID_FORMAT_H264_TS:
    case VID_FORMAT_H264_AAC_TS:
        ses->dec = decode_create(DEC_AVC);
        type_str = "H264/H264+AAC TS";
        ses->display_func = process_encoded;
        break;
    case VID_FORMAT_H264_RAW:
        ses->dec = decode_create(DEC_AVC);
        type_str = "H264 ES";
        ses->display_func = process_encoded;
        break;
    case VID_FORMAT_YUY2_RAW:
        type_str = "Raw YUY2";
        ses->display_func = process_raw;
        break;
    case VID_FORMAT_NV12_RAW:
        type_str = "Raw NV12";
        ses->display_func = process_raw;
        break;
    case VID_FORMAT_GREY_RAW:
        type_str = "Raw YUV800";
        ses->display_func = process_raw;
        break;

    default:
        printf("Unsupported video format: %i\n", (int) ses->fmt);
        return -1;
        break;
    }
    printf("init_display: %s: %d\n", type_str, (int) ses->fmt);
    return 0;
}

UNUSED
static int init_speaker(audio_session_t *ses) {
    int ret;

    if (ses->dec) {
        decode_destroy(ses->dec);
        ses->dec = NULL;
    }

    if (ses->samp_freq == 8000 || ses->samp_freq == 16000 || ses->samp_freq == 24000) {
        ret = mxuvc_audio_set_samplerate(ses->ch, ses->samp_freq);
        if (ret < 0) {
            printf("\nmxuvc_audio_set_samplerate failed\n");
            return ret;
        }
    }

    mxuvc_audio_set_volume(100);

    /* Initialize the decoder and create the window for the display */
    const char *type_str;

    switch (ses->fmt) {
    case AUD_FORMAT_PCM_RAW:
        type_str = "PCM";
        ses->speaker_func = process_pcm;
        printf("PCM: %d\n", (int) ses->fmt);
        break;
    case AUD_FORMAT_AAC_RAW:
        ses->dec = decode_create(DEC_AAC);
        type_str = "AAC";
        ses->speaker_func = process_aac;
        printf("AAC: %d\n", (int) ses->fmt);
        break;
    default:
        printf("Unsupported audio format: %d\n", (int) ses->fmt);
        return -1;
        break;
    }
    printf("init_speaker: %s: %d\n", type_str, (int) ses->fmt);
    return 0;
}

#ifdef LOAD_BALANCING
static int stick_this_thread_to_core(int c_id) {

    int num_cores = 0;

    num_cores = sysconf(_SC_NPROCESSORS_ONLN);

    if (c_id >= num_cores) // core_id = 0, 1, ... n-1 if system has n cores
    return 1;

    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(c_id, &cpuset);

    pthread_t current_thread = pthread_self();
    return pthread_setaffinity_np(current_thread, sizeof(cpu_set_t), &cpuset);
}
#endif

static int isKeyFrame(unsigned char* buffer, int length) {
    long data, type;
    long status = 0;
    int offset = 0;

    while (length > 0) {
        data = buffer[offset];
        offset++;
        length--;

        switch (status) {
        case 0:
        case 1:
        case 2:
            if (data == 0x00) { // NAL Header first 3 byte = "0x00 00 00"
                status++;
            } else {
                status = 0;
            }
            break;

        case 3:
            if (data == 0x01) { // NAL Header 4th byte = '0x01'
                status++;
            } else {
                status = 0;
            }
            break;

        case 4: // 1st byte of NAL Header
            type = data & 0x1F;

            if (type == 1) { //check if this is non-IDR frame
                return 0;
            } else if (type == 5) { //Check if this is IDR frame
                return 1;
            } else {
                if (type == 20 || type == 14) {
                    status++; // To Next byte
                } else {
                    status = 0;
                }
            }
            break;

        default:
            return 0;
        } // end of switch
    } //end of while

    return 0;
}

static void* processing_thread(void *arg) {
    struct video_session *ses = (video_session_t*) arg;
    int bytes_in_1sec = 0, bytes = 0; // Total bytes received. used to calculate the bitrate
    int framesCaptured = 0; // frames captured in TIME_INTERVAL_AVG. Used to calculate average frame size
    struct timeval timestart, timeend, timeBitrateStart, timeBpsMinMax, timeBufferFullness;
    int deltatime = 0;
    char indump[128] = { 0 };
    char outdump[128] = { 0 };
    int stat = 1;
    int numFramesForBufferFullness = 0;

#ifdef LOAD_BALANCING
    int cpu_num = 0;

    stick_this_thread_to_core(core_id);
    cpu_num = sysconf(_SC_NPROCESSORS_ONLN);

    if(core_id < cpu_num)
    core_id++;
    else
    core_id = 0;

#endif

    sprintf(indump, "in.ch%d.h264", ses->ch);
    sprintf(outdump, "out.ch%d.yuv", ses->ch);

    init_display(ses);
    ses->stats.bps_min = 0;
    ses->stats.bps_max = 0;
    ses->stats.bps = 0;
    ses->stats.iFrame = 0;
    ses->stats.usbFrameCaptured = 0;
    ses->stats.numFramesDelay = 0;

    int bps = 0;
    int bps_min = 9999999;
    int bps_max = 0;

    ses->stats.framesForBufferFullness = 0;
    int previousBufferFullness = 0; // Intial buffer fullness is 0
    int bufferFullnessDuration = ses->bufferFullnessDuration;
    ses->bufferFullnessParamChanged = 1;

    uint32_t channelBitrate = 0;
    uint32_t channelFps = 0;

    gettimeofday(&timestart, NULL);
    gettimeofday(&timeBitrateStart, NULL);
    gettimeofday(&timeBpsMinMax, NULL);
    gettimeofday(&timeBufferFullness, NULL);

    while (ses->active) {
        video_frame_t* frame;

        pthread_mutex_lock(&ses->mutex);
        stat = check_queue_empty(ses->q);

        while (stat != 0 && ses->active) {
            pthread_cond_wait(&ses->cond, &ses->mutex);
            stat = check_queue_empty(ses->q);
        }

        gettimeofday(&timeend, NULL);
        deltatime = (timeend.tv_sec - timestart.tv_sec) * 1000000 + (timeend.tv_usec - timestart.tv_usec);

        if (!ses->active) {
            break;
        }

        stat = queue_dequeue(ses->q, (void**) &frame);

        ses->frame = frame->buf;
        ses->len = frame->size;

        if (ses->flush == 1) {
            // empty the queue
            video_frame_t* tframe;
            while (queue_dequeue(ses->q, (void**) &tframe) == 0) {
                free(tframe->buf);
                free(tframe);
            }

            ses->flush = 0;
            fprintf(stderr, "Queue flushed\n");
        }

        pthread_mutex_unlock(&ses->mutex);

        if (stat != 0) {
            printf("Q is MT...\n");
            continue;
        }

        if (ses->file_dump) {
            unsigned char* buf = (unsigned char*) malloc(frame->size);
            memcpy(buf, frame->buf, frame->size);
            if (mp4_dump(ses->ch, buf, frame->size, frame->timestamp, DATA_VIDEO) != 0) {
                free (buf);
            }
        }

        bytes += frame->size;
        bytes_in_1sec += frame->size;
        framesCaptured ++;

        /*
         * Calculate buffer fullness
         * Current Buffer fullness = ( Current Frame Size + Previous frame Buffer fullness ) - ( bitrate / fps )
         * if ( Current Buffer fullness  < 0 )
         *    Current Buffer fullness =  ( Current Frame Size + Previous frame Buffer fullness )
         *
         * For first frame the previous buffer fullness = 0.
         */
        if (ses->bufferFullnessEnabled == 1 && ses->fmt == VID_FORMAT_H264_RAW) {
            if (ses->bufferFullnessParamChanged == 1) {
                previousBufferFullness = 0;
                numFramesForBufferFullness = 0;
                ses->stats.framesForBufferFullness = 0;
                bufferFullnessDuration = ses->bufferFullnessDuration;

                video_channel_info_t info;
                if (engine_getvideo_channel_info(ses->ch, &info) == 0) {
                    channelFps = info.framerate;
                    channelBitrate = info.bitrate;
                    //printf("FPS: %d, Bitrate: %d\n", channelFps, channelBitrate);
                }

                ses->bufferFullnessParamChanged = 0;
                gettimeofday(&timeBufferFullness, NULL);
            }
            int currentBufferFullness = (frame->size*8 + previousBufferFullness) - (int)((float)(channelBitrate)*1.0/(float)channelFps);
            if (currentBufferFullness < 0) {
                currentBufferFullness = (frame->size*8 + previousBufferFullness);
            }
            ses->stats.bufferFullness[numFramesForBufferFullness++] = currentBufferFullness;
            previousBufferFullness = currentBufferFullness;
        }

        if (isKeyFrame(frame->buf, frame->size)) {

            ses->stats.numFramesDelay = get_queue_length(ses->q);

            ses->stats.iFrame++;
        }

#ifdef CHECK_H264_DISCONTINUITY
        if (ses->fmt == VID_FORMAT_H264_RAW) {
            chkH264Discontinuity(ses);
        }
#endif

#ifdef CHECK_H264_ANALYSIS
        if(ses->fmt == VID_FORMAT_H264_RAW) {
            h264analyse(frame->buf, frame->size);
        }
#endif

        if (deltatime >= 1000000) {

            if (!mxuvc_video_alive()) {
                mxuvc_video_stop(ses->ch);
                ses->active = 0;
                printf("ERROR: video channel is not alive, quitting...\n");
                break;
            }

            ses->stats.duration = (double)deltatime/(double)1000000;

            ses->stats.fps = (int)ceil(((double)framesCaptured*1.0/ses->stats.duration));
            ses->stats.drops = ses->framedrops;
            ses->framedrops = 0;
            ses->stats.continuty = ses->continuitycnt;

            //double fps = ses->stats.usbFrameCaptured*1.0/ses->stats.duration;
            //ses->stats.fps = (int)round(fps);

            if (ses->fmt == VID_FORMAT_MJPEG_RAW) {
                bps = (int)floor(((double)(bytes_in_1sec*8)/framesCaptured));
            } else {
                bps = 8 * (int)((bytes_in_1sec)/((double)deltatime/(double)1000000));
            }
            bytes_in_1sec = 0;
            if (bps < bps_min) {
                bps_min = bps;
            }
            if (bps > bps_max) {
                bps_max = bps;
            }

            if (time_interval_bitrate == 1) {
                ses->stats.bps = bps;
                bytes = 0;
            } else {
                //gettimeofday(&timeend, NULL);
                deltatime = (timeend.tv_sec - timeBitrateStart.tv_sec) * 1000000 + (timeend.tv_usec - timeBitrateStart.tv_usec);

                if (deltatime >= time_interval_bitrate * 1000000) {
                    if (ses->fmt == VID_FORMAT_MJPEG_RAW) {
                        bps = (int)floor(((double)(bytes*8)/framesCaptured));
                    } else {
                        bps = 8 * (int)((bytes)/((double)deltatime/(double)1000000));
                    }
                    ses->stats.bps = bps;
                    bytes = 0;
                    gettimeofday(&timeBitrateStart, NULL);
                }
            }

            if (ses->bufferFullnessEnabled == 1 && ses->bufferFullnessParamChanged == 0) {
                //gettimeofday(&timeend, NULL);
                deltatime = (timeend.tv_sec - timeBufferFullness.tv_sec) * 1000000 + (timeend.tv_usec - timeBufferFullness.tv_usec);
                if (numFramesForBufferFullness > 0 && deltatime >= (bufferFullnessDuration * 1000000)) {
                    ses->stats.framesForBufferFullness = numFramesForBufferFullness;
                    gettimeofday(&timeBufferFullness, NULL);
                    numFramesForBufferFullness = 0;
                }
            }

            //gettimeofday(&timeend, NULL);
            deltatime = (timeend.tv_sec - timeBpsMinMax.tv_sec) * 1000000 + (timeend.tv_usec - timeBpsMinMax.tv_usec);
            if (deltatime >= time_interval_bitrate_min_max * 1000000) {
                // Send min max bitrate stats
                ses->stats.bps_min = bps_min;
                ses->stats.bps_max = bps_max;

                bps_min = 9999999;
                bps_max = 0;
                gettimeofday(&timeBpsMinMax, NULL);
            }

            if (ses->stats_func != NULL && ses->context != NULL ) {
                ses->stats_func(ses->context, &ses->stats);
            }
            ses->stats.usbFrameCaptured = 0;
            ses->stats.framesForBufferFullness = 0;
            framesCaptured = 0;
            gettimeofday(&timestart, NULL);
        }

        if (ses->ui_cb != NULL) {
            ses->display_func(ses, frame->buf, frame->size);
        }

        free(frame->buf);
        free(frame);
        frame = NULL;
    }

    if (ses->fmt == VID_FORMAT_GREY_RAW)
        smEnabled = 0;

    if (ses->dec) {
        decode_destroy(ses->dec);
        ses->dec = NULL;
    }

    return NULL ;
}

UNUSED
static void* audio_thread(void *arg) {
    struct audio_session *ses = (audio_session_t*) arg;

    int ret;
    FILE* fp = NULL;

    printf("audio processing thread started \n");
    while (ses->active) {
        ret = pthread_mutex_lock(&ses->mutex);
        assert(ret == 0);

        while (ses->frame == NULL ) {
            ret = pthread_cond_wait(&ses->cond, &ses->mutex);
            assert(ret == 0);
        }

        if (!ses->active) {
            pthread_mutex_unlock(&ses->mutex);
            break;
        }
#if 0
        if (!mxuvc_audio_alive())
        {
            ret = mxuvc_audio_stop(ses->ch);
            ret = pthread_mutex_unlock(&ses->mutex);
            assert(ret == 0);
            return NULL;
        }
#endif
        if (ses->outdump) {
            if (!fp) {
                fp = fopen(ses->dumppath, "w+");
            }
            if (fp != NULL ) {
                fwrite(ses->frame, ses->len, 1, fp);
                fflush(fp);
            } else
                printf("\nERROR: cannot open audio file to dump\n");
        }

        if (ses->ui_cb != NULL ) {
            ses->ui_cb(ses->context);
        }

        free(ses->frame);
        ses->frame = NULL;
        ret = pthread_mutex_unlock(&ses->mutex);

    }

    if (fp != NULL )
        fclose(fp);

    return NULL ;
}

int engine_init(int id) {
    int ret;
    video_channel_t ch;
    char video_backend[32] = "v4l2";
    char tmp[128] = {
            0 };
    camId[0] = -1;
    devtype = -1;

#ifdef VIDEO_BACKEND
    if(strncmp(VIDEO_BACKEND, "libusb-uvc", 10) == 0) {
        int vid=DEFAULT_VENDOR_ID;
        int pid=DEFAULT_PRODUCT_ID;

        sprintf(tmp, "vid=0x%x;pid=0x%x;", vid, pid);
        strncpy(video_backend, "libusb-uvc",10);
        //Find a way to figure out device type (i.e. Condor/MAX64380)  in libusb-uvc mode.
        //For now set the device type to Condor, otherwise caller of this function would fail
        devtype = 1; //setting to Condor device. 
    }
    else
#endif
    {
        int num = 0;

        findCameraDevices("MAX64380");
        if (camId[0] == id) {
            printf("Detected Raptor\n");
            camId[0] = -1;
            devtype = 0;
        }

        num = findCameraDevices("Condor");
        printf ("%d Condor devices found\n", num);

        for (int i = 0; i < num; i++) {
            if (camId[i] == id) {
                printf("Found matching device id\n");
                camId[0] = -1;
                devtype = 1;
            }
        }

        if (devtype == -1) {
            printf("ERROR: Did not detect a Geo Device!\n");
            return -1;
        }

        printf("opening device nodes /dev/video%d, /dev/video%d\n", id, id + 1);

        sprintf(tmp, "dev_offset=%d;dev_offset_secondary=%d;v4l_buffers=%d;", id, id + 1, 10);
    }

    pthread_mutex_init(&gMutex, NULL );

    ret = mxuvc_video_init(video_backend, tmp);
    #ifdef OVERLAY 
    mxuvc_overlay_init();
    overlay_image_params_t params;
    params.width = 320;
    params.height = 120;
    params.xoff = 0;
    params.yoff = 0;

    //ret = mxuvc_overlay_add_image(0, 0, 176, 144, 0, 0, "/home/unimesh/Download/tulips_yuv420_prog_planar_qcif.yuv");
    char text[256];
    strcpy(text, "/export/home/SVN/condor_trunk/condorsw/host/lib/mxuvc/examples/ipcam/overlay/geo_yuv420p_320x120.yuv");
    ret = mxuvc_overlay_add_image(CH1, &params, &params.idx, text);
//    ret = mxuvc_overlay_add_image(CH2, &params, &params.idx, text);
    strcpy(text, "/export/home/SVN/condor_trunk/condorsw/host/lib/mxuvc/examples/ipcam/overlay/MGSysFont16.bin");
    ret = mxuvc_overlay_load_font(16, text);

    //mxuvc_overlay_add_text(0, 0, "test", sizeof("test"), 400, 400);
    //ret = mxuvc_overlay_remove_image(0, 0);
    #endif
    if (ret < 0) {
        printf("Unable to open the camera\n");
        printf("\nYou didnt follow HOWTO properly\n");
        return ret;
    }

    if (devtype == 0) {
        printf("Init audio for Raptor/MAX64380\n");
        audio_initialized = mxuvc_audio_init("alsa", "device = MAX64380");
    }

    if (devtype == 1) {
        printf("Init audio for Condor/MAX64580\n");
        audio_initialized = mxuvc_audio_init("alsa", "device = Condor");
    }

    if (audio_initialized < 0) {
        fprintf(stderr, "Warning: Audio init failed!\n");
    } else {
        audio_initialized = 1;
        aud_ses = calloc(1, sizeof(audio_session_t));
        if (aud_ses != NULL ) {
            aud_ses->ch = AUD_CH2;
            aud_ses->active = 0;
        }
    }

    mxuvc_custom_control_init();

    /*aud_ses->fmt = AUD_FORMAT_AAC_RAW;
    aud_ses->samp_freq = 16000;
	aud_ses->permissible_range = 10;
	aud_ses->active = 0;
	if(aud_ses->fmt == AUD_FORMAT_PCM_RAW)
		aud_ses->ch = AUD_CH1;
	else
		aud_ses->ch = AUD_CH2;
	ret = mxuvc_audio_register_cb(aud_ses->ch, audio_cb, aud_ses);
	if(ret < 0)
		return ret;
	ret = mxuvc_audio_set_format(aud_ses->ch, aud_ses->fmt);
	if(ret < 0)
		return ret;

	init_speaker(aud_ses);*/

    /* init the video */
    uint32_t channel_count;

    if (mxuvc_video_get_channel_count(&channel_count) < 0) {
        printf("ERROR: Unable to get the number of video channel "
                "supported by the camera\n");
        return -1;
    }
    max_channels = channel_count;
    if (channel_count < 1) {
        printf("ERROR: No video channel is supported by the camera\n");
        return ret;
    }
    for (ch = CH1; ch <= (video_channel_t) (channel_count - 1); ++ch) {

        /* Allocate memory for the main channel video session */
        video_session_t *ses = calloc(1, sizeof(video_session_t));

        //save the session in global table.
        sId[ch] = ses;
        ses->frame = NULL;
        ses->ch = ch;
        ses->lastcc = -1; /* start value */
        ses->continuitycnt = 0;
        ses->context = NULL;
        ses->ui_cb = NULL;
        ses->decframe = NULL;
        ses->decoded_width = NULL;
        ses->decoded_height = NULL;
        ses->declen = 0;
        ses->stats_func = NULL;
        ses->active = 0;
        ses->indump = 0;
        ses->outdump = 0;
        ses->file_dump = 0;
        ses->flush = 0;
        ses->accelerate = 0;
        ses->bufferFullnessEnabled = 0;
        ses->bufferFullnessDuration = TIME_INTERVAL_BUFFER_FULLNESS;
        ses->bufferFullnessParamChanged = 0;
        /* Register callback functions */
        ret = mxuvc_video_register_cb(ch, video_cb, ses);
        if (ret < 0) {
            printf("Unable to register the video callback\n");
            continue;
        }
        ret = mxuvc_video_get_format(ch, &ses->fmt);
        if (ret < 0) {
            fprintf(stderr, "get on ch %d for format failed\n", ses->ch);
            continue;
        }

    }

    if(strncmp(video_backend, "libusb-uvc", 10) == 0)
        mxuvc_video_set_format(CH2, VID_FORMAT_MJPEG_RAW);

if (audio_initialized == 1) {
//        engine_startaudio();
}

    return devtype;
}

int engine_deinit(void) {

    /* deinit the video */
    uint32_t channel_count;
    video_channel_t ch;
    int ret = -1;

    if (mxuvc_video_get_channel_count(&channel_count) < 0) {
        printf("ERROR: Unable to get the number of video channel "
                "supported by the camera\n");
        return ret;
    }
    if ((int)channel_count != max_channels) {
        printf("ERROR: No video channel is supported by the camera\n");
        return ret;
    }

    //free video resources
    for (ch = CH1; ch <= (video_channel_t) (channel_count - 1); ++ch) {
        free(sId[ch]);
        sId[ch] = NULL;
    }
    #ifdef OVERLAY
    mxuvc_overlay_deinit();
    #endif
    mxuvc_video_deinit();

    //free audio resources
if (audio_initialized == 1) {
    if (aud_ses != NULL ) {
        free(aud_ses);
    }
    mxuvc_audio_deinit();
    audio_initialized = -1;
}
    //free other resources.
    mxuvc_custom_control_deinit();
    close_fds();
    pthread_mutex_destroy(&gMutex);
    return 0;
}

/*static void* timer_thread(void *arg) {
    struct video_session *ses = (video_session_t*) arg;
    ses->bytes = 0;
    while (ses->active) {
        sleep(1);

        pthread_mutex_lock(&ses->stats_mutex);

        ses->stats.drops = ses->framedrops;
        ses->stats.continuty = ses->continuitycnt;

        ses->stats.bps = ses->bytes * 8;
        ses->stats.fps = ses->stats.usbFrameCaptured;
        ses->stats.duration = 1;
        if (ses->stats_func != NULL && ses->context != NULL ) {
            //ses->stats_func(ses->context, &ses->stats);
        }
        ses->bytes = 0;
        //ses->stats.usbFrameCaptured = 0;
        pthread_mutex_unlock(&ses->stats_mutex);

        if (!ses->active) {
            break;
        }
    }

    return NULL;
}*/

int engine_startvideo(int ch) {
    int ret = 1;
    video_session_t *ses = NULL;

    ses = sId[ch];

    if (ses->active == 0) {
        /* Initialize the processing thread */
        ret = pthread_mutex_init(&ses->mutex, NULL );
        if (ret < 0) {
            fprintf(stderr, "pthread_mutex_init failed\n");
            return ret;
        }

        ret = pthread_cond_init(&ses->cond, NULL );
        if (ret < 0) {
            fprintf(stderr, "pthread_cond_init failed\n");
            return ret;
        }

        // Create queue only if there is a UI callback
        if (ses->q == NULL) {
            ses->q = queue_create();
        }

        ses->active = 1;
        ret = pthread_create(&ses->thread, NULL, processing_thread, ses);
        if (ret < 0) {
            fprintf(stderr, "pthread_create for video ch %d failed\n", ses->ch);
            return ret;
        }

        ret = pthread_mutex_init(&ses->stats_mutex, NULL );
        if (ret < 0) {
            fprintf(stderr, "pthread_mutex_init failed\n");
            return ret;
        }

        ret = pthread_cond_init(&ses->stats_cond, NULL );
        if (ret < 0) {
            fprintf(stderr, "pthread_cond_init failed\n");
            return ret;
        }

        /*ret = pthread_create(&ses->timer, NULL, timer_thread, ses);
        if (ret < 0) {
            fprintf(stderr, "pthread_create for video ch %d failed\n", ses->ch);
            return ret;
        }*/

        mxuvc_video_start(ch);
    } else {
        fprintf(stderr, "video ch %d is already started!!\n", ch);
        return 0;
    }
    return ret;
}

int engine_stopvideo(int ch) {
    int ret = 1;
    video_session_t *ses = NULL;
    video_frame_t* frame;

    ses = sId[ch];
    if (ses != NULL && ses->active == 1) {
#ifdef ANALYTICS
        if (ses->fmt == VID_FORMAT_GREY_RAW) {
            if (smEnabled) {
                smDeinit();
                smEnabled = 0;
            }
        }
#endif
        ses->active = 0;
        pthread_cond_signal(&ses->cond);

        pthread_join(ses->thread, NULL);

        ret = mxuvc_video_stop(ch);

        //make sure q is empty.
        for (; queue_dequeue(ses->q, (void**) &frame) == 0;) {
            free(frame->buf);
            free(frame);
        }

        queue_destroy(ses->q);
        ses->q = NULL;
        pthread_cond_destroy(&ses->cond);
        pthread_mutex_destroy(&ses->mutex);
    } else {
        //fprintf(stderr, "video ch %dis already stopped!!\n", ch);
        return 0;
    }
    return ret;
}

int engine_startaudio(int ch, int sr, char* path) {
    if (audio_initialized != 1) {
        fprintf(stderr, "Audio not initialized\n");
        return -1;
    }

    audio_channel_t audch = (audio_channel_t) ch;
    int ret = 1;
    if (aud_ses->active == 0) {
        ret = pthread_mutex_init(&aud_ses->mutex, NULL );
        if (ret < 0) {
            fprintf(stderr, "pthread_mutex_init failed\n");
            return ret;
        }

        ret = pthread_cond_init(&aud_ses->cond, NULL );
        if (ret < 0) {
            fprintf(stderr, "pthread_cond_init failed\n");
            return ret;
        }

        aud_ses->active = 1;

        aud_ses->ch = audch;
        if (aud_ses->ch == AUD_CH1) {
            aud_ses->fmt = AUD_FORMAT_PCM_RAW;
        } else {
            aud_ses->fmt = AUD_FORMAT_AAC_RAW;
        }
        aud_ses->samp_freq = sr;
        aud_ses->permissible_range = 10;

        ret = mxuvc_audio_register_cb(aud_ses->ch, audio_cb, aud_ses);
        if (ret < 0) {
            aud_ses->active = 0;
            fprintf(stderr, "FAILED mxuvc_audio_register_cb enabled!!%d\n", ret);
            return ret;
        }

        ret = mxuvc_audio_set_samplerate(aud_ses->ch, aud_ses->samp_freq);
        if (ret < 0) {
            printf("Failed set samplerate\n");
            return ret;
        }

        //init_speaker(aud_ses);

        usleep(5000);

        strcpy(aud_ses->dumppath, path);

        printf("Starting audio for channel: %d\n", aud_ses->ch);
        ret = mxuvc_audio_start(aud_ses->ch);
        if (ret < 0) {
            aud_ses->active = 0;
            fprintf(stderr, "mxuvc_audio_start failed %d\n", ret);
            return ret;
        }
    } else {
        fprintf(stderr, "audio channel is already enabled!!\n");
    }

    return ret;
}

int engine_stopaudio(void) {
    if (audio_initialized != 1) {
        fprintf(stderr, "Audio not initialized\n");
        return 0;
    }

    int ret	= 1 ;
#if 1
    if (aud_ses->active == 1) {
        aud_ses->active = 0;

        ret = mxuvc_audio_stop(aud_ses->ch);
        if (ret < 0) {
            fprintf(stderr, "mxuvc_audio_stop failed %d\n", ret);
        } else {
            printf("Audio stop success\n");
        }

#ifdef PCM_AUDIO_STREAMING
        if(audio_handle)
        pcmdeinit(audio_handle);
#endif

        pthread_cond_destroy(&aud_ses->cond);
        pthread_mutex_destroy(&aud_ses->mutex);

       close_fds();

       currentAudioFormat = NUM_AUD_FORMAT;

       if (ogg_initialized == 1) {
           cpluscplus_deinit_ogg_encoder();
           ogg_initialized = 0;
           ogg_header_added = 0;
       }
    } else
        fprintf(stderr, "audio channel is already disabled!!\n");
#endif

    return ret;
}

int engine_enablemic_mute(int flag) {
    int ret;

    if (flag)
        ret = mxuvc_audio_set_mic_mute(1);
    else
        ret = mxuvc_audio_set_mic_mute(0);

    return ret;
}

int engine_startaudiocapture(void) {
    return 0;
}

int engine_stopaudiocapture(void) {
    return 0;
}

int engine_enable_aec(int enable) {
    int ret = 0;

    if (enable) {
        mxuvc_custom_control_enable_aec();
        ret = mxuvc_custom_control_set_audio_codec_samplerate(BOARD_SAMPLE_RATE);
        if (ret < 0)
            return ret;
    } else {
        mxuvc_custom_control_disable_aec();
    }

    return 0;
}

int engine_getaudio_bitrate(int* bitrate) {
    return mxuvc_audio_get_bitrate((uint32_t*) bitrate);

}

int engine_getvideo_channel_count(void) {
    uint32_t channel_count;
    mxuvc_video_get_channel_count(&channel_count);
    return (int) channel_count;
}

// Channel info
int engine_getvideo_channel_info(video_channel_t channel, video_channel_info_t* info) {
    return mxuvc_video_get_channel_info(channel, info);
}

//resolution
int engine_getvideo_resolution(int ch, int *w, int*h) {
    return mxuvc_video_get_resolution(ch, (uint16_t *) w, (uint16_t *) h);
}

int engine_setvideo_resolution(int ch, int w, int h) {
    return mxuvc_video_set_resolution(ch, (uint32_t) w, (uint32_t) h);
}

//fps
int engine_getvideo_fps(int ch, int* fps) {
    return mxuvc_video_get_framerate(ch, (uint32_t*) fps);
}

int engine_setvideo_fps(int ch, int fps) {
    video_session_t *ses = sId[ch];
    ses->bufferFullnessParamChanged = 1;
    return mxuvc_video_set_framerate(ch, (uint32_t) fps);
}

//bitrate
int engine_getvideo_bps(int ch, int* bitrate) {
    return mxuvc_video_get_bitrate(ch, (uint32_t*) bitrate);
}

int engine_setvideo_bps(int ch, int bitrate) {
    video_session_t *ses = sId[ch];
    ses->bufferFullnessParamChanged = 1;
    return mxuvc_video_set_bitrate(ch, (uint32_t) bitrate);
}

//gop
int engine_getvideo_gop(int ch, int* gop) {
    return mxuvc_video_get_goplen(ch, (uint32_t*) gop);
}

int engine_setvideo_gop(int ch, int gop) {
    return mxuvc_video_set_goplen(ch, (uint32_t) gop);
}

//profile
int engine_getvideo_profile(int ch, int* profile) {
    return mxuvc_video_get_profile(ch, (video_profile_t*) profile);
}

int engine_setvideo_profile(int ch, int profile) {
    return mxuvc_video_set_profile(ch, (video_profile_t) profile);
}

//video compression quality
int engine_getvideo_compression_quality(int ch, int* quality) {
    return mxuvc_video_get_compression_quality(ch, (uint32_t*) quality);
}

int engine_setvideo_compression_quality(int ch, int quality) {
    return mxuvc_video_set_compression_quality(ch, quality);
}

//avc level
int engine_getvideo_avc_level(int ch, int* level) {
    return mxuvc_video_get_avc_level(ch, (uint32_t*) level);
}

int engine_setvideo_avc_level(int ch, int level) {
    return mxuvc_video_set_avc_level(ch, (uint32_t) level);
}

//force i frame
int engine_setvideo_force_iframe(int ch) {
    return mxuvc_video_force_iframe((video_channel_t) ch);
}

//channel format
int engine_getvideo_format(int ch, int* format) {
    return mxuvc_video_get_format(ch, (video_format_t*) format);
}

int engine_setvideo_format(int ch, int format) {
    return mxuvc_video_set_format(ch, (video_format_t) format);
}

int engine_setdewarp_mode(int ch, int mode, char* path) {

    printf("setdewarp: %d", mode);
    dewarp_params_t p;

    switch (mode) {
    case EMODE_WM_ZCL: {
        p.eptz_mode_wm_zcl.Phi0 = 55;
        p.eptz_mode_wm_zcl.Rx = 2;
        p.eptz_mode_wm_zcl.Ry = 1;
        p.eptz_mode_wm_zcl.Rz = 1;
        p.eptz_mode_wm_zcl.Gshift = 0;
        break;
    }
    case EMODE_WM_ZCLCylinder: {
        p.eptz_mode_wm_zclcylinder.CylinderHeightMicro = (int) (0.95 * 1000000);
        break;
    }
    case EMODE_WM_ZCLStretch: {
        p.eptz_mode_wm_zclstretch.HPan = 0;
        p.eptz_mode_wm_zclstretch.VPan = 0;
        p.eptz_mode_wm_zclstretch.Zoom = 1;
        break;
    }
    case EMODE_WM_1PanelEPTZ: {
        p.eptz_mode_wm_1paneleptz.HPan = -50;
        p.eptz_mode_wm_1paneleptz.VPan = 0;
        p.eptz_mode_wm_1paneleptz.Zoom = 40;
        p.eptz_mode_wm_1paneleptz.Tilt = 0;
        break;
    }
    case EMODE_WM_Sweep_1PanelEPTZ: {
        p.eptz_mode_sweep_wm_1panelptz.HPanStart = -10;
        p.eptz_mode_sweep_wm_1panelptz.VPanStart = 0;
        p.eptz_mode_sweep_wm_1panelptz.ZoomStart = 40;
        p.eptz_mode_sweep_wm_1panelptz.TiltStart = 0;
        p.eptz_mode_sweep_wm_1panelptz.HPanInc = 1;
        p.eptz_mode_sweep_wm_1panelptz.TiltInc = 1;
        p.eptz_mode_sweep_wm_1panelptz.VPanInc = 1;
        p.eptz_mode_sweep_wm_1panelptz.ZoomInc = 1;
        p.eptz_mode_sweep_wm_1panelptz.Period = 40;
        break;
    }
    case EMODE_OFF: {
        break;
    }
    case EMODE_WM_Magnify: {
        p.eptz_mode_magnify.zoom = 1;
        p.eptz_mode_magnify.radius = 100;
        p.eptz_mode_magnify.xCenter = 100;
        p.eptz_mode_magnify.yCenter = 100;
        break;
    }
    default: {
        printf("Dewarp: Invalid EPTZ mode %d\n", mode);
        break;
    }
    }
    return mxuvc_video_set_dewarp_params((video_channel_t) ch, 0, mode, &p);
}

int engine_start_capture_video(int ch) {
    video_session_t *ses = NULL;
    ses = sId[ch];
    ses->indump = 1;
    return 0;
}

int engine_stop_capture_video(int ch) {
    video_session_t *ses = NULL;
    ses = sId[ch];
    ses->indump = 0;
    return 0;
}

int engine_is_audio_initialized() {
    return audio_initialized == 1;
}

int engine_start_capture_audio() {
    if (audio_initialized == 1) {
        aud_ses->outdump = 1;
    }
    return 0;
}

int engine_stop_capture_audio() {
    if (audio_initialized == 1) {
        aud_ses->outdump = 0;
    }
    return 0;
}

int engine_get_audio_samplingrate(int audch) {
    return mxuvc_audio_get_samplerate((audio_channel_t) audch);
}

int engine_set_audio_samplingrate(int audch, int val) {
    return mxuvc_audio_set_samplerate((audio_channel_t) audch, val);
}

int engine_start_smartmotion(int ch, int width, int height) {
#ifdef ANALYTICS
    video_session_t *ses = NULL;
    ses = sId[ch];
    smInit(ses);
    smEnabled = 1;
#endif
    return 0;
}
int engine_stop_smartmotion() {
#ifdef ANALYTICS
    if (smEnabled) {
        smDeinit();
        smEnabled = 0;
    }
#endif
    return 0;
}

int engine_set_brightness(int32_t val) {
    return mxuvc_video_set_brightness(CH1, (int16_t)val);
}
int engine_get_brightness(int32_t *val) {
    return mxuvc_video_get_brightness(CH1, (int16_t*)val);
}
int engine_set_contrast(uint32_t val) {
    return mxuvc_video_set_contrast(CH1, val);
}
int engine_get_contrast(uint32_t *val) {
    return mxuvc_video_get_contrast(CH1, (uint16_t*)val);
}
int engine_set_hue(int32_t val) {
    return mxuvc_video_set_hue(CH1, val);
}
int engine_get_hue(int32_t *val) {
    return mxuvc_video_get_hue(CH1, (int16_t*) val);
}
int engine_set_saturation(uint32_t val) {
    return mxuvc_video_set_saturation(CH1, val);
}
int engine_get_saturation(uint32_t *val) {
    return mxuvc_video_get_saturation(CH1, (uint16_t*)val);
}
int engine_set_gama(uint32_t val) {
    return mxuvc_video_set_gamma(CH1, val);
}
int engine_get_gama(uint32_t *val) {
    return mxuvc_video_get_gamma(CH1, (uint16_t*)val);
}
int engine_set_sharpness(uint32_t val) {
    return mxuvc_video_set_sharpness(CH1, val);
}
int engine_get_sharpness(uint32_t *val) {
    return mxuvc_video_get_sharpness(CH1, (uint16_t*)val);
}
int engine_set_wb(white_balance_mode_t mode, uint32_t val) {
    return mxuvc_video_set_wb(CH1, mode, val);
}
int engine_get_wb(white_balance_mode_t *mode, uint32_t *val) {
    return mxuvc_video_get_wb(CH1, mode, (uint16_t*)val);
}
int engine_set_wdr(wdr_mode_t mode, uint32_t val) {
    return mxuvc_video_set_wdr(CH1, mode, (uint8_t) val);
}
int engine_get_wdr(wdr_mode_t *mode, uint32_t *val) {
    return mxuvc_video_get_wdr(CH1, mode, (uint8_t*)val);
}
int engine_set_zone_wb(zone_wb_set_t sel, uint32_t value) {
    return mxuvc_video_set_zone_wb(CH1, sel, value);
}
int engine_get_zone_wb(zone_wb_set_t *sel, uint32_t *val) {
    return mxuvc_video_get_zone_wb(CH1, sel, (uint16_t*)val);
}
int engine_set_nf(noise_filter_mode_t sel, uint32_t value) {
    return mxuvc_video_set_nf(CH1, sel, value);
}
int engine_get_nf(noise_filter_mode_t *sel, uint32_t *val) {
    return mxuvc_video_get_nf(CH1, sel, (uint16_t*)val);
}
#if 0 //Disbaled as there is no MXUVC support for this fetaure
int engine_set_zone_exp(zone_exp_set_t sel, uint32_t value) {
    return mxuvc_video_set_zone_exp(CH1, sel, value);
}
int engine_get_zone_exp(zone_exp_set_t *sel, uint32_t *val) {
    return mxuvc_video_get_zone_exp(CH1, sel, (uint16_t*)val);
}
int engine_set_exposure(exp_set_t sel, uint32_t value) {
    return mxuvc_video_set_exp(CH1, sel, value);
}
int engine_get_exposure(exp_set_t *sel, uint32_t *val) {
    return mxuvc_video_get_exp(CH1, sel, (uint16_t*)val);
}
#endif

int engine_set_gain_multiplier(uint32_t value) {
    return mxuvc_video_set_gain_multiplier(CH1, value);
}
int engine_get_gain_multiplier(uint32_t *val) {
    return mxuvc_video_get_gain_multiplier(CH1, val);
}
int engine_set_sharpen_filter(uint32_t value) {
    return mxuvc_video_set_sharpen_filter(CH1, value);
}
int engine_get_sharpen_filter(uint32_t *val) {
    return mxuvc_video_get_sharpen_filter(CH1, val);
}
int engine_set_histogram_eq(int value) {
    return mxuvc_video_set_histogram_eq(CH1, (histo_eq_t) value);
}
int engine_get_histogram_eq(int *val) {
    return mxuvc_video_get_histogram_eq(CH1, (histo_eq_t*) val);
}
int engine_set_max_analog_gain(uint32_t value) {
    return mxuvc_video_set_max_analog_gain(CH1, value);
}
int engine_get_max_analog_gain(uint32_t *val) {
    return mxuvc_video_get_max_analog_gain(CH1, val);
}
int engine_set_flip_vertical(int value) {
    return mxuvc_video_set_flip_vertical(CH1, (video_flip_t) value);
}
int engine_get_flip_vertical(int *val) {
    return mxuvc_video_get_flip_vertical(CH1, (video_flip_t*) val);
}
int engine_set_flip_horizontal(int value) {
    return mxuvc_video_set_flip_horizontal(CH1, (video_flip_t) value);
}
int engine_get_flip_horizontal(int *val) {
    return mxuvc_video_get_flip_horizontal(CH1, (video_flip_t*) val);
}
int engine_get_compositor_params(video_channel_t ch, int panel, panel_mode_t* mode, panel_params_t* params) {
    return mxuvc_video_get_compositor_params(ch, panel, mode, params);
}
int engine_set_compositor_params(video_channel_t ch, int panel, panel_mode_t mode, panel_params_t* params) {
    return mxuvc_video_set_compositor_params(ch, panel, mode, params);
}
int engine_get_dewarp_params(video_channel_t ch, int panel, dewarp_mode_t* mode, dewarp_params_t* params) {
    return mxuvc_video_get_dewarp_params(ch, panel, mode, params);
}
int engine_set_dewarp_params(video_channel_t ch, int panel, dewarp_mode_t mode, dewarp_params_t* params) {
    return mxuvc_video_set_dewarp_params(ch, panel, mode, params);
}
int engine_get_config_params(video_channel_t ch, config_params_t* params) {
    return mxuvc_video_get_config_params(ch, params);
}
int engine_get_camera_mode(camer_mode_t *mode) {
    *mode = IPCAM;
    return mxuvc_get_camera_mode(mode);
}
int engine_audio_get_volume() {
    return mxuvc_audio_get_volume();
}
int engine_audio_set_volume(int volume) {
    return mxuvc_audio_set_volume(volume);
}
int engine_audio_mute(int mute) {
    return mxuvc_audio_set_mic_mute(mute);
}
int engine_audio_mute_left(int mute) {
    return mxuvc_audio_set_left_mic_mute(mute);
}
int engine_audio_mute_right(int mute) {
    return mxuvc_audio_set_right_mic_mute(mute);
}
int engine_agc(int enable) {
    if (enable) {
        return mxuvc_custom_control_enable_agc();
    } else {
        return mxuvc_custom_control_disable_agc();
    }
}

// Overlay
int engine_overlay_init() {
	return mxuvc_overlay_init();
}
int engine_overlay_deinit() {
	return mxuvc_overlay_deinit();
}
int engine_overlay_add_image(int ch, int width, int height, int xoff, int yoff, char* filename, int* id, int alpha, char* alphafile) {
    overlay_image_params_t params;
    params.width = (uint32_t) width;
    params.height = (uint32_t) height;
    params.xoff = (uint32_t) xoff;
    params.yoff = (uint32_t) yoff;
    params.idx = 0;
    params.alpha = (uint8_t)alpha;
    int ret = mxuvc_overlay_add_image((video_channel_t) ch, &params, filename, alphafile);
    if (ret == 0) {
        *id = (int)params.idx;
    }
    return ret;
}
int engine_overlay_remove_image(int ch, int id) {
    return mxuvc_overlay_remove_image((video_channel_t)ch, (uint32_t) id);
}

int engine_overlay_image_alpha(int ch, int id, int alpha) {
    return mxuvc_overlay_set_transparency((video_channel_t)ch,(uint32_t) id,(uint32_t) alpha);
}

int engine_overlay_load_font(int channel, char* font_file, int font_size) {
    return mxuvc_overlay_load_font((video_channel_t)channel, font_size, font_file);
}
int engine_overlay_add_text(int ch, char* text, int xOff, int yOff, int* id) {
    overlay_text_params_t params;
    params.xoff = xOff;
    params.yoff = yOff;
    params.idx = *id;
    int ret = mxuvc_overlay_add_text((video_channel_t)ch, &params, text, strlen(text));
    if (ret == 0) {
        *id = (int)params.idx;
    }
    return ret;
}
int engine_overlay_remove_text(int ch, int id) {
    return mxuvc_overlay_remove_text((video_channel_t)ch, (uint32_t) id);
}
int engine_overlay_set_time(int ch, int xOff, int yOff, int hours, int minutes, int seconds, bool frameNumEnabled) {
    overlay_text_params_t tparams;
    tparams.xoff = xOff;
    tparams.yoff = yOff;

    overlay_time_t params;
    params.hh = (uint8_t) hours;
    params.mm = (uint8_t) minutes;
    params.ss = (uint8_t) seconds;
    params.frame_num_enable = frameNumEnabled;
    return mxuvc_overlay_set_time((video_channel_t)ch, &tparams, &params);
}
int engine_overlay_show_time(int ch) {
    return mxuvc_overlay_show_time((video_channel_t)ch);
}
int engine_overlay_hide_time(int ch) {
    return mxuvc_overlay_hide_time((video_channel_t)ch);
}

// Overlay Privacy

int engine_get_max_privacy_masks() {
    return PRIVACY_IDX_MAX;
}

int engine_overlay_privacy_add_mask_rect(int ch, uint32_t xoff, uint32_t yoff, uint32_t width, uint32_t height, uint32_t c, int idx) {
    privacy_params_t params;

    privacy_mask_shape_rect_t shape;
    shape.xoff = xoff;
    shape.yoff = yoff;
    shape.width = width;
    shape.height = height;

    privacy_color_t color;
    color.yuva = c;// 0x00808055;

    params.type = PRIVACY_SHAPE_RECT;
    params.shape = (privacy_shape_t*) &shape;
    params.color = &color;

    return mxuvc_overlay_privacy_add_mask(CH1, &params, (privacy_index_t) idx);
}

int engine_overlay_privacy_remove_mask(int ch, int idx) {
    return mxuvc_overlay_privacy_remove_mask(CH1, (privacy_index_t)idx);
}

int engine_overlay_privacy_update_mask_color(int ch, uint32_t color, int idx) {
    privacy_color_t pcolor;
    pcolor.yuva = color;
    return mxuvc_overlay_privacy_update_color(CH1, &pcolor, (privacy_index_t)idx);
}

int engine_start_mp4_dump_video(int ch, const char* filePath) {
	video_session_t *ses = sId[ch];
    char file_dump_path[128];
	strcpy(file_dump_path, filePath);
	//gettimeofday(&ses->file_dump_start_time, NULL);
	engine_setvideo_force_iframe((video_channel_t)ch);
	ses->file_dump = 1;
	mp4_dump_init(ch, file_dump_path, ses->width, ses->height);
	return 0;
}

int engine_stop_mp4_dump_video(int ch) {
	video_session_t *ses = NULL;
	ses = sId[ch];
	ses->file_dump = 0;
	mp4_dump_deinit(ch);
	return 0;
}

void engine_update_render_callback(int channel, callback fptr) {
    video_session_t *ses = NULL;
    ses = sId[channel];
    if (fptr != NULL) {
        engine_setvideo_force_iframe(channel);
    } else {
        ses->flush = 1;
    }
    ses->ui_cb = fptr;
}

void engine_flush_queue(int channel) {
    video_session_t *ses = sId[channel];
    ses->flush = 1;
}

void engine_set_optimize(int channel, int optimize) {
    video_session_t *ses = sId[channel];
    //ses->flush = optimize;
    ses->accelerate = optimize;
}

void engine_set_interval_bitrate(int interval) {
    time_interval_bitrate = interval;
}

void engine_set_interval_bitrate_min_max(int interval) {
    time_interval_bitrate_min_max = interval;
}

void engine_enable_buffer_fullness(int channel, int enable, int interval) {
    video_session_t *ses = sId[channel];
    ses->bufferFullnessEnabled = enable;
    ses->bufferFullnessDuration = interval;
    ses->bufferFullnessParamChanged = 1;
}
