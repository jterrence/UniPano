/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef _DECODE_H_
#define _DECODE_H_

#include "framebuffer.h"

#define DEC_AVC   0
#define DEC_MJPEG 1
#define DEC_AAC   2

struct decode;

struct decode* decode_create(char type);
void decode_destroy(struct decode* dec);
struct framebuffer* decode_frame(struct decode *dec, const unsigned char *frame, signed long length, void *out, int *width, int *height);

#endif
