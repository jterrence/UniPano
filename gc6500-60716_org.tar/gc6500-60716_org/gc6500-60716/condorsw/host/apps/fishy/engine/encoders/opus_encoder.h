/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef OPUSENCODER_H_
#define OPUSENCODER_H_

#include <ogg/ogg.h>

#ifdef ENABLE_NLS
#include <libintl.h>
#define _(X) gettext(X)
#else
#define _(X) (X)
#define textdomain(X)
#define bindtextdomain(X, Y)
#endif
#ifdef gettext_noop
#define N_(X) gettext_noop(X)
#else
#define N_(X) (X)
#endif

typedef long (*audio_read_func)(void *src, float *buffer, int samples);
extern const int wav_permute_matrix[8][8];

#ifdef __cplusplus
class OPUSEncoder {
public:
    typedef struct {
        int version;
        int channels; /* Number of channels: 1..255 */
        int preskip;
        ogg_uint32_t input_sample_rate;
        int gain; /* in dB S7.8 should be zero whenever possible */
        int channel_mapping;
        /* The rest is only used if channel_mapping != 0 */
        int nb_streams;
        int nb_coupled;
        unsigned char stream_map[255];
    } OpusHeader;

    typedef struct {
        audio_read_func read_samples;
        void *readdata;
        int rawmode;
        int channels;
        long rate;
        int gain;
        int samplesize;
        int endianness;
        char *infilename;
        int ignorelength;
        int skip;
        int extraout;
        char *comments;
        int comments_length;
        int copy_comments;
        int copy_pictures;
    } oe_enc_opt;

    OPUSEncoder();

    virtual ~OPUSEncoder();

    int mstInit(int channels, int rate);

    int shutdown();

    int transcode(unsigned char* src, unsigned char* dst, unsigned int size, int add_header);

private:

    int add_ogg_header(unsigned char* dst);

    int opus_header_parse(const unsigned char *header, int len, OpusHeader *h);
    int opus_header_to_packet(const OpusHeader *h, unsigned char *packet, int len);

    void comment_add(char **comments, int* length, const char *tag, char *val);
    void comment_init(char **comments, int* length, const char *vendor_string);
    void comment_pad(char **comments, int* length, int amount);

    /**
     * Write an Ogg page to a buffer
     * @param page
     * @param fp
     * @return
     */
    int oe_write_page(ogg_page *page, unsigned char *buffer, int start);

    OpusHeader header;
    ogg_stream_state os;
    ogg_page og;
    ogg_packet op;
    oe_enc_opt inopt;
    ogg_int32_t rate;
    ogg_int32_t id;
    ogg_int32_t coding_rate;
    ogg_int32_t frame_size;
    int max_ogg_delay;
    int comment_padding;
    unsigned char *packet;
};
/*#else
  typedef
    struct OPUSTranscoder
    OPUSTranscoder;*/
#endif

#ifdef __cplusplus
extern "C" {
#endif

//extern void c_ogg_transcode();   /* ANSI C prototypes */
extern void cpluscplus_init_ogg_encoder(int channels, int sampling_rate);
extern void cpluscplus_deinit_ogg_encoder();
extern int cplusplus_ogg_encode(unsigned char* src, unsigned char* dst, unsigned int size, unsigned int add_header);

#ifdef __cplusplus
}
#endif

#endif /* OPUSENCODER_H_ */
