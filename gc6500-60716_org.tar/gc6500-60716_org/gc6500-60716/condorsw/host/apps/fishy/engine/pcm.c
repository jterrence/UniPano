/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <assert.h>
#include <endian.h>
#include <sys/poll.h>
#include <errno.h>
#include <stdarg.h>

#include <alsa/asoundef.h>
#include <alsa/version.h>
#include <alsa/global.h>
#include <alsa/input.h>
#include <alsa/output.h>
#include <alsa/error.h>
#include <alsa/conf.h>
#include <alsa/pcm.h>
#include <alsa/rawmidi.h>
#include <alsa/timer.h>
#include <alsa/hwdep.h>
#include <alsa/control.h>
#include <alsa/mixer.h>
#include <alsa/seq_event.h>
#include <alsa/seq.h>
#include <alsa/seqmid.h>
#include <alsa/seq_midi_event.h>

#include <sched.h>
#include <errno.h>
#include <getopt.h>
#include <sys/time.h>
#include <math.h>

static snd_pcm_t *handle = NULL;
static int method = 0;
static signed short *samples = NULL;
static const char *device = "default"; /* playback device */
static snd_pcm_format_t format = SND_PCM_FORMAT_S16; /* sample format */
static unsigned int rate = 16000; /* stream rate */
static unsigned int channels = 1; /* count of channels */
static unsigned int buffer_time = 200000; /* ring buffer length in us */
static unsigned int period_time = 100000; /* period time in us */
static int verbose = 0; /* verbose flag */
static int resample = 1; /* enable alsa-lib resampling */
static int period_event = 0; /* produce poll event after each period */
static int active = 0;
static snd_pcm_channel_area_t *areas;
static snd_pcm_sframes_t buffer_size;
static snd_pcm_sframes_t period_size;
static snd_output_t *output = NULL;

static int set_hwparams(snd_pcm_t *phandle, snd_pcm_hw_params_t *params, snd_pcm_access_t paccess) {
    unsigned int rrate;
    snd_pcm_uframes_t size;
    int err, dir;

    /* choose all parameters */
    err = snd_pcm_hw_params_any(phandle, params);
    if (err < 0) {
        printf("Broken configuration for playback: no configurations available: %s\n", snd_strerror(err));
        return err;
    }
    /* set hardware resampling */
    err = snd_pcm_hw_params_set_rate_resample(phandle, params, resample);
    if (err < 0) {
        printf("Resampling setup failed for playback: %s\n", snd_strerror(err));
        return err;
    }
    /* set the interleaved read/write format */
    err = snd_pcm_hw_params_set_access(phandle, params, paccess);
    if (err < 0) {
        printf("Access type not available for playback: %s\n", snd_strerror(err));
        return err;
    }
    /* set the sample format */
    err = snd_pcm_hw_params_set_format(phandle, params, format);
    if (err < 0) {
        printf("Sample format not available for playback: %s\n", snd_strerror(err));
        return err;
    }
    /* set the count of channels */
    err = snd_pcm_hw_params_set_channels(phandle, params, channels);
    if (err < 0) {
        printf("Channels count (%i) not available for playbacks: %s\n", channels, snd_strerror(err));
        return err;
    }
    /* set the stream rate */
    rrate = rate;
    err = snd_pcm_hw_params_set_rate_near(phandle, params, &rrate, 0);
    if (err < 0) {
        printf("Rate %iHz not available for playback: %s\n", rate, snd_strerror(err));
        return err;
    }
    if (rrate != rate) {
        printf("Rate doesn't match (requested %iHz, get %iHz)\n", rate, err);
        return -EINVAL;
    }
    /* set the buffer time */
    err = snd_pcm_hw_params_set_buffer_time_near(phandle, params, &buffer_time, &dir);
    if (err < 0) {
        printf("Unable to set buffer time %i for playback: %s\n", buffer_time, snd_strerror(err));
        return err;
    }
    err = snd_pcm_hw_params_get_buffer_size(params, &size);
    if (err < 0) {
        printf("Unable to get buffer size for playback: %s\n", snd_strerror(err));
        return err;
    }
    buffer_size = size;
    /* set the period time */
    err = snd_pcm_hw_params_set_period_time_near(phandle, params, &period_time, &dir);
    if (err < 0) {
        printf("Unable to set period time %i for playback: %s\n", period_time, snd_strerror(err));
        return err;
    }
    err = snd_pcm_hw_params_get_period_size(params, &size, &dir);
    if (err < 0) {
        printf("Unable to get period size for playback: %s\n", snd_strerror(err));
        return err;
    }
    period_size = size;
    /* write the parameters to device */
    err = snd_pcm_hw_params(phandle, params);
    if (err < 0) {
        printf("Unable to set hw params for playback: %s\n", snd_strerror(err));
        return err;
    }
    return 0;
}

static int set_swparams(snd_pcm_t *phandle, snd_pcm_sw_params_t *swparams) {
    int err;

    /* get the current swparams */
    err = snd_pcm_sw_params_current(phandle, swparams);
    if (err < 0) {
        printf("Unable to determine current swparams for playback: %s\n", snd_strerror(err));
        return err;
    }
    /* start the transfer when the buffer is almost full: */
    /* (buffer_size / avail_min) * avail_min */
    err = snd_pcm_sw_params_set_start_threshold(phandle, swparams, (buffer_size / period_size) * period_size);
    if (err < 0) {
        printf("Unable to set start threshold mode for playback: %s\n", snd_strerror(err));
        return err;
    }
    /* allow the transfer when at least period_size samples can be processed */
    /* or disable this mechanism when period event is enabled (aka interrupt like style processing) */
    err = snd_pcm_sw_params_set_avail_min(phandle, swparams, period_event ? buffer_size : period_size);
    if (err < 0) {
        printf("Unable to set avail min for playback: %s\n", snd_strerror(err));
        return err;
    }
    /* enable period events when requested */
    if (period_event) {
        err = snd_pcm_sw_params_set_period_event(phandle, swparams, 1);
        if (err < 0) {
            printf("Unable to set period event: %s\n", snd_strerror(err));
            return err;
        }
    }
    /* write the parameters to the playback device */
    err = snd_pcm_sw_params(phandle, swparams);
    if (err < 0) {
        printf("Unable to set sw params for playback: %s\n", snd_strerror(err));
        return err;
    }
    return 0;
}

/*
 *   Underrun and suspend recovery
 */

static int xrun_recovery(snd_pcm_t *phandle, int err) {
    if (verbose)
        printf("stream recovery\n");
    if (err == -EPIPE) { /* under-run */
        err = snd_pcm_prepare(phandle);
        if (err < 0)
            printf("Can't recovery from underrun, prepare failed: %s\n", snd_strerror(err));
        return 0;
    } else if (err == -ESTRPIPE) {
        while ((err = snd_pcm_resume(phandle)) == -EAGAIN)
            sleep(1); /* wait until the suspend flag is released */
        if (err < 0) {
            err = snd_pcm_prepare(phandle);
            if (err < 0)
                printf("Can't recovery from suspend, prepare failed: %s\n", snd_strerror(err));
        }
        return 0;
    }
    return err;
}

/*
 *   Transfer method - write only
 */

static int write_loop(snd_pcm_t *phandle, signed short *nsamples, snd_pcm_channel_area_t *pareas) {
    signed short *ptr;
    int err = 0, cptr;

    ptr = nsamples;
    //cptr = period_size;
    cptr = 960;
    //while (cptr > 0) {
    err = snd_pcm_writei(phandle, ptr, cptr);

    //if (err < 0) {
    //	if (xrun_recovery(handle, err) < 0) {
    //	printf("Write error: %s\n", snd_strerror(err));
    //	return err;
    //}
    //	break;	/* skip one period */
    //}
    //ptr += err * channels;
    //cptr -= err;
    //}

    if (err < 0)
        printf("Write error: %s\n", snd_strerror(err));

    return err;
}

/*
 *   Transfer method - write and wait for room in buffer using poll
 */

static int wait_for_poll(snd_pcm_t *phandle, struct pollfd *ufds, unsigned int count) {
    unsigned short revents;

    poll(ufds, count, -1);
    snd_pcm_poll_descriptors_revents(phandle, ufds, count, &revents);
    if (revents & POLLERR)
        return -EIO;
    if (revents & POLLOUT)
        return 0;

    return 0;
}

static int write_and_poll_loop(snd_pcm_t *phandle, signed short *nsamples, snd_pcm_channel_area_t *pareas) {
    struct pollfd *ufds;
    signed short *ptr;
    int err, count, cptr, init;

    count = snd_pcm_poll_descriptors_count(phandle);
    if (count <= 0) {
        printf("Invalid poll descriptors count\n");
        return count;
    }

    ufds = malloc(sizeof(struct pollfd) * count);
    if (ufds == NULL ) {
        printf("No enough memory\n");
        return -ENOMEM;
    }
    if ((err = snd_pcm_poll_descriptors(phandle, ufds, count)) < 0) {
        printf("Unable to obtain poll descriptors for playback: %s\n", snd_strerror(err));
        return err;
    }

    init = 1;
    while (active) {
        if (!init) {
            err = wait_for_poll(phandle, ufds, count);
            if (err < 0) {
                if (snd_pcm_state(phandle) == SND_PCM_STATE_XRUN || snd_pcm_state(phandle) == SND_PCM_STATE_SUSPENDED) {
                    err = snd_pcm_state(phandle) == SND_PCM_STATE_XRUN ? -EPIPE : -ESTRPIPE;
                    if (xrun_recovery(phandle, err) < 0) {
                        printf("Write error: %s\n", snd_strerror(err));
                        exit(EXIT_FAILURE);
                    }
                    init = 1;
                } else {
                    printf("Wait for poll failed\n");
                    return err;
                }
            }
        }

        ptr = nsamples;
        cptr = period_size;
        while (cptr > 0) {
            err = snd_pcm_writei(phandle, ptr, cptr);
            if (err < 0) {
                if (xrun_recovery(phandle, err) < 0) {
                    printf("Write error: %s\n", snd_strerror(err));
                    exit(EXIT_FAILURE);
                }
                init = 1;
                break; /* skip one period */
            }
            if (snd_pcm_state(phandle) == SND_PCM_STATE_RUNNING)
                init = 0;
            ptr += err * channels;
            cptr -= err;
            if (cptr == 0)
                break;
            /* it is possible, that the initial buffer cannot store */
            /* all data from the last period, so wait awhile */
            err = wait_for_poll(phandle, ufds, count);
            if (err < 0) {
                if (snd_pcm_state(phandle) == SND_PCM_STATE_XRUN || snd_pcm_state(phandle) == SND_PCM_STATE_SUSPENDED) {
                    err = snd_pcm_state(phandle) == SND_PCM_STATE_XRUN ? -EPIPE : -ESTRPIPE;
                    if (xrun_recovery(phandle, err) < 0) {
                        printf("Write error: %s\n", snd_strerror(err));
                        exit(EXIT_FAILURE);
                    }
                    init = 1;
                } else {
                    printf("Wait for poll failed\n");
                    return err;
                }
            }
        }
    }
    return err;

}

/*
 *   Transfer method - asynchronous notification
 */

struct async_private_data {
    signed short *samples;
    snd_pcm_channel_area_t *areas;
    double phase;
};

static void async_callback(snd_async_handler_t *ahandler) {
    snd_pcm_t *phandle = snd_async_handler_get_pcm(ahandler);
    struct async_private_data *data = snd_async_handler_get_callback_private(ahandler);
    signed short *nsamples = data->samples;
    //snd_pcm_channel_area_t *pareas = data->areas;
    snd_pcm_sframes_t avail;
    int err;

    avail = snd_pcm_avail_update(phandle);
    while (avail >= period_size) {

        err = snd_pcm_writei(phandle, nsamples, period_size);
        if (err < 0) {
            printf("Write error: %s\n", snd_strerror(err));
            exit(EXIT_FAILURE);
        }
        if (err != period_size) {
            printf("Write error: written %i expected %li\n", err, period_size);
            exit(EXIT_FAILURE);
        }
        avail = snd_pcm_avail_update(phandle);
    }
}

static int async_loop(snd_pcm_t *phandle, signed short *nsamples, snd_pcm_channel_area_t *pareas) {
    struct async_private_data data;
    snd_async_handler_t *ahandler;
    int err, count;

    data.samples = nsamples;
    data.areas = pareas;
    data.phase = 0;
    err = snd_async_add_pcm_handler(&ahandler, phandle, async_callback, &data);
    if (err < 0) {
        printf("Unable to register async handler\n");
        exit(EXIT_FAILURE);
    }
    for (count = 0; count < 2; count++) {

        err = snd_pcm_writei(phandle, nsamples, period_size);
        if (err < 0) {
            printf("Initial write error: %s\n", snd_strerror(err));
            exit(EXIT_FAILURE);
        }
        if (err != period_size) {
            printf("Initial write error: written %i expected %li\n", err, period_size);
            exit(EXIT_FAILURE);
        }
    }
    if (snd_pcm_state(phandle) == SND_PCM_STATE_PREPARED) {
        err = snd_pcm_start(phandle);
        if (err < 0) {
            printf("Start error: %s\n", snd_strerror(err));
            exit(EXIT_FAILURE);
        }
    }

    /* because all other work is done in the signal handler,
     suspend the process */
    while (active) {
        sleep(1);
    }

    return err;
}

/*
 *   Transfer method - asynchronous notification + direct write
 */

static void async_direct_callback(snd_async_handler_t *ahandler) {
    snd_pcm_t *phandle = snd_async_handler_get_pcm(ahandler);
    const snd_pcm_channel_area_t *my_areas;
    snd_pcm_uframes_t offset, frames, size;
    snd_pcm_sframes_t avail, commitres;
    snd_pcm_state_t state;
    int first = 0, err;

    while (active) {
        state = snd_pcm_state(phandle);
        if (state == SND_PCM_STATE_XRUN) {
            err = xrun_recovery(phandle, -EPIPE);
            if (err < 0) {
                printf("XRUN recovery failed: %s\n", snd_strerror(err));
                exit(EXIT_FAILURE);
            }
            first = 1;
        } else if (state == SND_PCM_STATE_SUSPENDED) {
            err = xrun_recovery(phandle, -ESTRPIPE);
            if (err < 0) {
                printf("SUSPEND recovery failed: %s\n", snd_strerror(err));
                exit(EXIT_FAILURE);
            }
        }
        avail = snd_pcm_avail_update(phandle);
        if (avail < 0) {
            err = xrun_recovery(phandle, avail);
            if (err < 0) {
                printf("avail update failed: %s\n", snd_strerror(err));
                exit(EXIT_FAILURE);
            }
            first = 1;
            continue;
        }
        if (avail < period_size) {
            if (first) {
                first = 0;
                err = snd_pcm_start(phandle);
                if (err < 0) {
                    printf("Start error: %s\n", snd_strerror(err));
                    exit(EXIT_FAILURE);
                }
            } else {
                break;
            }
            continue;
        }
        size = period_size;
        while (size > 0) {
            frames = size;
            err = snd_pcm_mmap_begin(phandle, &my_areas, &offset, &frames);
            if (err < 0) {
                if ((err = xrun_recovery(phandle, err)) < 0) {
                    printf("MMAP begin avail error: %s\n", snd_strerror(err));
                    exit(EXIT_FAILURE);
                }
                first = 1;
            }

            commitres = snd_pcm_mmap_commit(phandle, offset, frames);
            if (commitres < 0 || (snd_pcm_uframes_t) commitres != frames) {
                if ((err = xrun_recovery(phandle, commitres >= 0 ? -EPIPE : commitres)) < 0) {
                    printf("MMAP commit error: %s\n", snd_strerror(err));
                    exit(EXIT_FAILURE);
                }
                first = 1;
            }
            size -= frames;
        }
    }
}

static int async_direct_loop(snd_pcm_t *phandle, signed short *nsamples ATTRIBUTE_UNUSED, snd_pcm_channel_area_t *pareas ATTRIBUTE_UNUSED) {
    struct async_private_data data;
    snd_async_handler_t *ahandler;
    const snd_pcm_channel_area_t *my_areas;
    snd_pcm_uframes_t offset, frames, size;
    snd_pcm_sframes_t commitres;
    int err, count;

    data.samples = NULL; /* we do not require the global sample area for direct write */
    data.areas = NULL; /* we do not require the global areas for direct write */
    data.phase = 0;
    err = snd_async_add_pcm_handler(&ahandler, phandle, async_direct_callback, &data);
    if (err < 0) {
        printf("Unable to register async handler\n");
        exit(EXIT_FAILURE);
    }
    for (count = 0; count < 2; count++) {
        size = period_size;
        while (size > 0) {
            frames = size;
            err = snd_pcm_mmap_begin(phandle, &my_areas, &offset, &frames);
            if (err < 0) {
                if ((err = xrun_recovery(phandle, err)) < 0) {
                    printf("MMAP begin avail error: %s\n", snd_strerror(err));
                    exit(EXIT_FAILURE);
                }
            }

            commitres = snd_pcm_mmap_commit(phandle, offset, frames);
            if (commitres < 0 || (snd_pcm_uframes_t) commitres != frames) {
                if ((err = xrun_recovery(phandle, commitres >= 0 ? -EPIPE : commitres)) < 0) {
                    printf("MMAP commit error: %s\n", snd_strerror(err));
                    exit(EXIT_FAILURE);
                }
            }
            size -= frames;
        }
    }
    err = snd_pcm_start(phandle);
    if (err < 0) {
        printf("Start error: %s\n", snd_strerror(err));
        exit(EXIT_FAILURE);
    }

    /* because all other work is done in the signal handler,
     suspend the process */
    while (active) {
        sleep(1);
    }

    return err;
}

/*
 *   Transfer method - direct write only
 */

static int direct_loop(snd_pcm_t *phandle, signed short *nsamples ATTRIBUTE_UNUSED, snd_pcm_channel_area_t *pareas ATTRIBUTE_UNUSED) {
    const snd_pcm_channel_area_t *my_areas;
    snd_pcm_uframes_t offset, frames, size;
    snd_pcm_sframes_t avail, commitres;
    snd_pcm_state_t state;
    int err = 0, first = 1;

    while (active) {
        state = snd_pcm_state(phandle);
        if (state == SND_PCM_STATE_XRUN) {
            err = xrun_recovery(phandle, -EPIPE);
            if (err < 0) {
                printf("XRUN recovery failed: %s\n", snd_strerror(err));
                return err;
            }
            first = 1;
        } else if (state == SND_PCM_STATE_SUSPENDED) {
            err = xrun_recovery(phandle, -ESTRPIPE);
            if (err < 0) {
                printf("SUSPEND recovery failed: %s\n", snd_strerror(err));
                return err;
            }
        }
        avail = snd_pcm_avail_update(phandle);
        if (avail < 0) {
            err = xrun_recovery(phandle, avail);
            if (err < 0) {
                printf("avail update failed: %s\n", snd_strerror(err));
                return err;
            }
            first = 1;
            continue;
        }
        if (avail < period_size) {
            if (first) {
                first = 0;
                err = snd_pcm_start(phandle);
                if (err < 0) {
                    printf("Start error: %s\n", snd_strerror(err));
                    exit(EXIT_FAILURE);
                }
            } else {
                err = snd_pcm_wait(phandle, -1);
                if (err < 0) {
                    if ((err = xrun_recovery(phandle, err)) < 0) {
                        printf("snd_pcm_wait error: %s\n", snd_strerror(err));
                        exit(EXIT_FAILURE);
                    }
                    first = 1;
                }
            }
            continue;
        }
        size = period_size;
        while (size > 0) {
            frames = size;
            err = snd_pcm_mmap_begin(phandle, &my_areas, &offset, &frames);
            if (err < 0) {
                if ((err = xrun_recovery(phandle, err)) < 0) {
                    printf("MMAP begin avail error: %s\n", snd_strerror(err));
                    exit(EXIT_FAILURE);
                }
                first = 1;
            }

            commitres = snd_pcm_mmap_commit(phandle, offset, frames);
            if (commitres < 0 || (snd_pcm_uframes_t) commitres != frames) {
                if ((err = xrun_recovery(phandle, commitres >= 0 ? -EPIPE : commitres)) < 0) {
                    printf("MMAP commit error: %s\n", snd_strerror(err));
                    exit(EXIT_FAILURE);
                }
                first = 1;
            }
            size -= frames;
        }
    }

    return err;
}

/*
 *   Transfer method - direct write only using mmap_write functions
 */

static int direct_write_loop(snd_pcm_t *phandle, signed short *nsamples, snd_pcm_channel_area_t *pareas) {
    signed short *ptr;
    int err = 0, cptr = 0;

    while (active) {

        ptr = nsamples;
        cptr = period_size;
        while (cptr > 0) {
            err = snd_pcm_mmap_writei(phandle, ptr, cptr);
            if (err == -EAGAIN)
                continue;
            if (err < 0) {
                if (xrun_recovery(phandle, err) < 0) {
                    printf("Write error: %s\n", snd_strerror(err));
                    exit(EXIT_FAILURE);
                }
                break; /* skip one period */
            }
            ptr += err * channels;
            cptr -= err;
        }
    }

    return err;
}

/*
 *
 */

struct transfer_method {
    const char *name;
    snd_pcm_access_t access;
    int (*transfer_loop)(snd_pcm_t *phandle, signed short *nsamples, snd_pcm_channel_area_t *pareas);
};

static struct transfer_method transfer_methods[] = {
        {
                "write",
                SND_PCM_ACCESS_RW_INTERLEAVED,
                write_loop },
        {
                "write_and_poll",
                SND_PCM_ACCESS_RW_INTERLEAVED,
                write_and_poll_loop },
        {
                "async",
                SND_PCM_ACCESS_RW_INTERLEAVED,
                async_loop },
        {
                "async_direct",
                SND_PCM_ACCESS_MMAP_INTERLEAVED,
                async_direct_loop },
        {
                "direct_interleaved",
                SND_PCM_ACCESS_MMAP_INTERLEAVED,
                direct_loop },
        {
                "direct_noninterleaved",
                SND_PCM_ACCESS_MMAP_NONINTERLEAVED,
                direct_loop },
        {
                "direct_write",
                SND_PCM_ACCESS_MMAP_INTERLEAVED,
                direct_write_loop },
        {
                NULL,
                SND_PCM_ACCESS_RW_INTERLEAVED,
                NULL } };

static void info(void) {
    int k;

    printf("Recognized sample formats are:");
    for (k = 0; k < SND_PCM_FORMAT_LAST; ++k) {
        const char *s = snd_pcm_format_name(k);
        if (s)
            printf(" %s\n", s);
    }
    printf("\n");
    printf("Recognized transfer methods are:");
    for (k = 0; transfer_methods[k].name; k++)
        printf(" %s\n", transfer_methods[k].name);
    printf("\n");
}

void* pcm_init(void) {

    int err;
    snd_pcm_hw_params_t *hwparams;
    snd_pcm_sw_params_t *swparams;

    unsigned int chn;

    snd_pcm_hw_params_alloca(&hwparams);
    snd_pcm_sw_params_alloca(&swparams);

    err = snd_output_stdio_attach(&output, stdout, 0);
    if (err < 0) {
        printf("Output failed: %s\n", snd_strerror(err));
        return NULL ;
    }

    printf("Playback device is %s\n", device);
    printf("Stream parameters are %iHz, %s, %i channels\n", rate, snd_pcm_format_name(format), channels);
    printf("Using transfer method: %s\n", transfer_methods[method].name);
    info();

    if ((err = snd_pcm_open(&handle, device, SND_PCM_STREAM_PLAYBACK, 0)) < 0) {
        printf("Playback open error: %s\n", snd_strerror(err));
        return NULL ;
    }

    if ((err = set_hwparams(handle, hwparams, transfer_methods[method].access)) < 0) {
        printf("Setting of hwparams failed: %s\n", snd_strerror(err));
        snd_pcm_close(handle);
        return NULL ;
    }
    if ((err = set_swparams(handle, swparams)) < 0) {
        printf("Setting of swparams failed: %s\n", snd_strerror(err));
        snd_pcm_close(handle);
        return NULL ;
    }

    if (verbose > 0)
        snd_pcm_dump(handle, output);

    int ts = (period_size * channels * snd_pcm_format_physical_width(format)) / 8;
    samples = malloc(ts);

    printf("Buffer size %d\n", ts);
    if (samples == NULL ) {
        printf("No enough memory\n");
        snd_pcm_close(handle);
        return NULL ;
    }

    areas = calloc(channels, sizeof(snd_pcm_channel_area_t));
    if (areas == NULL ) {
        printf("No enough memory\n");
        free(samples);
        snd_pcm_close(handle);
        return NULL ;
    }
    for (chn = 0; chn < channels; chn++) {
        areas[chn].addr = samples;
        areas[chn].first = chn * snd_pcm_format_physical_width(format);
        areas[chn].step = channels * snd_pcm_format_physical_width(format);
    }

    return (void*) handle;
}

int pcm_deinit(void * phandle) {
    free(areas);
    free(samples);
    snd_pcm_close((snd_pcm_t *) phandle);
    return 0;
}

int pcm_write(void* phandle, signed short *nsamples) {
    int err = transfer_methods[method].transfer_loop((snd_pcm_t *) phandle, nsamples, areas);
    if (err < 0)
        printf("Transfer failed: %s\n", snd_strerror(err));

    return err;
}

void* pcminit(void) {
    int err;

    if ((err = snd_pcm_open(&handle, device, SND_PCM_STREAM_PLAYBACK, 0)) < 0) {
        printf("Playback open error: %s\n", snd_strerror(err));
        return NULL ;
    }
    if ((err = snd_pcm_set_params(handle, SND_PCM_FORMAT_S16_LE, SND_PCM_ACCESS_RW_INTERLEAVED, 1, 16000, 1, 200000)) < 0) { /* 0.2sec */
        printf("Playback open error: %s\n", snd_strerror(err));
        return NULL ;
    }

    return (void*) handle;
}

int pcmdeinit(void * phandle) {
    snd_pcm_close((snd_pcm_t *) phandle);
    return 0;
}
int pcmwrite(void* phandle, signed short *buffer, int size) {
    snd_pcm_sframes_t frames = 0;
    int err;
    err = snd_pcm_writei(phandle, buffer, size);
    if (err < 0) {
        frames = (snd_pcm_sframes_t) snd_pcm_recover(phandle, frames, 0);
    }

    if (frames < 0) {
        printf("snd_pcm_writei failed: %s\n", snd_strerror(err));
        return 1;
    }

    if (frames > 0 && frames < (long) sizeof(buffer))
        printf("Short write (expected %li, wrote %li)\n", (long) size, frames);

    return 0;
}
