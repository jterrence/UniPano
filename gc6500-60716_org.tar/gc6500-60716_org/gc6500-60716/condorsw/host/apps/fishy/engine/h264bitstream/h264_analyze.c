/* 
 * h264bitstream - a library for reading and writing H.264 video
 * Copyright (C) 2005-2007 Auroras Entertainment, LLC
 * 
 * Written by Alex Izvorski <aizvorski@gmail.com>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "h264_stream.h"

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

int h264analyse(char *frame, int size)
{
  
    h264_stream_t* h = h264_new();

    int opt_verbose = 1;
    int opt_probe = 1;

    if (h264_dbgfile == NULL) { h264_dbgfile = stdout; }
    
    size_t sz = size;
    uint8_t* p = (uint8_t*) frame;

    int nal_start, nal_end;

    if(find_nal_unit(p, sz, &nal_start, &nal_end) > 0)
    {
        
        read_nal_unit(h, &p[nal_start], nal_end - nal_start);

        if ( opt_probe && h->nal->nal_unit_type == NAL_UNIT_TYPE_SPS )
        {
            // print codec parameter, per RFC 6381.
            int constraint_byte = h->sps->constraint_set0_flag << 7;
            constraint_byte = h->sps->constraint_set1_flag << 6;
            constraint_byte = h->sps->constraint_set2_flag << 5;
            constraint_byte = h->sps->constraint_set3_flag << 4;
            constraint_byte = h->sps->constraint_set4_flag << 3;
            constraint_byte = h->sps->constraint_set4_flag << 3;

            fprintf( h264_dbgfile, "codec: avc1.%02X%02X%02X\n",h->sps->profile_idc, constraint_byte, h->sps->level_idc );

            // TODO: add more, move to h264_stream (?)

        }

        if ( opt_verbose > 0 )
        {
            fprintf( h264_dbgfile, "XX ");
            debug_bytes(&p[nal_start]-4, nal_end - nal_start + 4 >= 16 ? 16: nal_end - nal_start + 4);
            debug_nal(h, h->nal);
        }

    }
        
    h264_free(h);
    return 0;
}
