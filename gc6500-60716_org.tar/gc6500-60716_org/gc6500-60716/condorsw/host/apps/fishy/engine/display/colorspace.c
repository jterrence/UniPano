/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include "colorspace.h"
#include "framebuffer.h"
#ifndef NOX
#include <libavcodec/avcodec.h>
#endif

#define CLIP(x) if (x > 255) {x = 255;} if (x < 0) {x = 0;}

int fourcc_to_pixfmt(int fourcc)
{
#ifndef NOX
	switch(fourcc) {
	case FOURCC_I420:
		return PIX_FMT_YUV420P;
	case FOURCC_YUY2:
	case FOURCC_YUYV:
		return PIX_FMT_YUYV422;
	case FOURCC_UYVY:
		return PIX_FMT_UYVY422;
	case FOURCC_NV12:
		return PIX_FMT_NV12;
	case FOURCC_Y800:
		return PIX_FMT_YUV420P;
	default:
		fprintf(stderr,"SW RGB conversion for format 0x%x not supported\n", 
				fourcc);
		return PIX_FMT_NONE;
	}
#endif
}

static inline int yuvtorgb(int y, int u, int v)
{
    int r, g, b;

    const int shl16_1point404 = (int) (1.404 * (1 << 16));
    const int shl16_0point344 = (int) (0.344 * (1 << 16));
    const int shl16_0point714 = (int) (0.714 * (1 << 16));
    const int shl16_1point772 = (int) (1.772 * (1 << 16));

    u -= 128;
    v -= 128;

    r = (y + ((shl16_1point404 * v) >> 16));
    g = (y - (((shl16_0point344 * u) + (shl16_0point714 * v)) >> 16));
    b = (y + ((shl16_1point772 * u) >> 16));

    CLIP(r);
    CLIP(g);
    CLIP(b);

    return ((r << 16) + (g << 8) + b);
}

static void y800_to_rgb(unsigned *dst, struct framebuffer *frm)
{
    int i,j;
    unsigned char * y = frm->data;
    
    
    for(j = 0; j < frm->height; j++) {
        for(i = 0; i < frm->width/2; i++)
        {
            int yvalue = y[2*i];
            int uvalue = 128;
            int vvalue = 128;

            int pix = yuvtorgb(yvalue, uvalue, vvalue);
            *dst++ = pix;

            yvalue = y[2*i+1];
            pix = yuvtorgb(yvalue, uvalue, vvalue);
            *dst++ = pix;
        }

        y += frm->width;
        
    }

}
static void i420_to_rgb(unsigned *dst, struct framebuffer *frm)
{
    int i,j;
    unsigned char * y = frm->data;
    unsigned char * u = frm->data + frm->width*frm->height;
    unsigned char * v = u + frm->width*frm->height/4;
    for(j = 0; j < frm->height; j++) {
        for(i = 0; i < frm->width/2; i++)
        {
            int yvalue = y[2*i];
            int uvalue = u[i];
            int vvalue = v[i];

            int pix = yuvtorgb(yvalue, uvalue, vvalue);
            *dst++ = pix;

            yvalue = y[2*i+1];
            pix = yuvtorgb(yvalue, uvalue, vvalue);
            *dst++ = pix;
        }

        y += frm->width;
        if(j&1) {
            u += frm->width/2;
            v += frm->width/2;
        }
    }
}
static void yv12_to_rgb(unsigned *dst, struct framebuffer *frm)
{
    int i,j;
    unsigned char * y = frm->data;
    unsigned char * v = frm->data + frm->width*frm->height;
    unsigned char * u = v + frm->width*frm->height/4;
    for(j = 0; j < frm->height; j++) {
        for(i = 0; i < frm->width/2; i++)
        {
            int yvalue = y[2*i];
            int uvalue = u[i];
            int vvalue = v[i];

            int pix = yuvtorgb(yvalue, uvalue, vvalue);
            *dst++ = pix;

            yvalue = y[2*i+1];
            pix = yuvtorgb(yvalue, uvalue, vvalue);
            *dst++ = pix;
        }

        y += frm->width;
        if(j&1) {
            u += frm->width/2;
            v += frm->width/2;
        }
    }
}
static void nv12_to_rgb(unsigned *dst, struct framebuffer *frm)
{
    int i,j;
    unsigned char * y = frm->data;
    unsigned char * uv = frm->data + frm->width*frm->height;
    for(j = 0; j < frm->height; j++) {
        for(i = 0; i < frm->width/2; i++)
        {
            int yvalue = y[2*i];
            int uvalue = uv[2*i];
            int vvalue = uv[2*i+1];

            int pix = yuvtorgb(yvalue, uvalue, vvalue);
            *dst++ = pix;

            yvalue = y[2*i+1];
            pix = yuvtorgb(yvalue, uvalue, vvalue);
            *dst++ = pix;
        }

        y += frm->width;
        if(j&1) {
            uv += frm->width;
        }
    }
}
static void yuy2_to_rgb(unsigned *dst, struct framebuffer *frm)
{
    int i;
    unsigned char *buf = frm->data;
    for(i = 0; i < frm->size/4; i++)
    {
        int yvalue = buf[4*i];
        int uvalue = buf[4*i+1];
        int vvalue = buf[4*i+3];

        int pix = yuvtorgb(yvalue, uvalue, vvalue);
        *dst++ = pix;

        yvalue = buf[4*i+2];
        pix = yuvtorgb(yvalue, uvalue, vvalue);
        *dst++ = pix;
    }
}
static void uyvy_to_rgb(unsigned *dst, struct framebuffer *frm)
{
    int i;
    unsigned char *buf = frm->data;
    for(i = 0; i < frm->size/4; i++)
    {
        int uvalue = buf[4*i];
        int yvalue = buf[4*i+1];
        int vvalue = buf[4*i+2];

        int pix = yuvtorgb(yvalue, uvalue, vvalue);
        *dst++ = pix;

        yvalue = buf[4*i+3];
        pix = yuvtorgb(yvalue, uvalue, vvalue);
        *dst++ = pix;
    }
}

void colorspace_to_rgb(unsigned *dst, struct framebuffer *frm, int cp_fourcc)
{
    switch (cp_fourcc) {
    case FOURCC_I420:
        i420_to_rgb(dst, frm);
        break;
    case FOURCC_YUY2:
        yuy2_to_rgb(dst, frm);
        break;
    case FOURCC_YUYV:
        yuy2_to_rgb(dst, frm);
        break;
    case FOURCC_UYVY:
        uyvy_to_rgb(dst, frm);
        break;
    case FOURCC_YV12:
        yv12_to_rgb(dst, frm);
        break;
    case FOURCC_NV12:
        nv12_to_rgb(dst, frm);
        break;
    case FOURCC_Y800:
        y800_to_rgb(dst, frm);
        break;
    default:
        fprintf(stderr,"SW RGB conversion for format 0x%x not supported\n", cp_fourcc);
    }
}

