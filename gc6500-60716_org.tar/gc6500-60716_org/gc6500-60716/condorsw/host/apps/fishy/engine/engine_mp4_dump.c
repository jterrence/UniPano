/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
/*
 * engine_mp4_dump.c
 *
 *  Created on: May 27, 2015
 *      Author: bsmith
 */
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include "engine_mp4_dump.h"
#include "muxers/mp4_muxer.h"
#include "queue.h"

typedef struct {
    int channel;
    pthread_t thread;
    queue_t *queue;
    uint64_t prev_timestamp;
    int dump;
} MP4Data;

typedef struct {
    unsigned char* buffer;
    int size;
    uint64_t duration;
    EnumDataType type;
} BufferDescriptor;

MP4Data* MAP_THREADS[5];

static void* mp4_dump_thread(void *arg) {
    MP4Data *data = (MP4Data*) arg;
    int status;

    while (data->dump) {
        // Dequeue to get data
        BufferDescriptor *desc = NULL;
        status = queue_dequeue(data->queue, (void**) &desc);
        if (status != 0) {
            usleep(50000); // 50 ms
            continue;
        }

        // call mp4 lib API
        mp4_muxer_mux(data->channel, desc->buffer, desc->size, desc->duration);
        free(desc->buffer);
        free(desc);
    }

    // dump remaining items in queue
    BufferDescriptor *desc = NULL;
    while(queue_dequeue(data->queue, (void**) &desc) == 0) {
        fprintf(stderr, "Dumping remaining queue data\n");
        mp4_muxer_mux(data->channel, desc->buffer, desc->size, desc->duration);
        free(desc->buffer);
        free(desc);
        desc = NULL;
    }
    return NULL;
}

int mp4_dump_init(int channel, char* mp4_file_path, int width, int height) {
    // start thread for this channel if not already started
    MP4Data *data = MAP_THREADS[channel];
    if (data == NULL) {
        data = (MP4Data*) malloc (sizeof(MP4Data));
        data->channel = channel;
        data->dump = 1;
        data->prev_timestamp = 0;
        int ret = pthread_create(&data->thread, NULL, mp4_dump_thread, data);

        if (ret < 0) {
            fprintf(stderr, "Failed to create mp4 dump thread for channel %d\n", channel);
            free(data);
            return ret;
        }
        data->queue = queue_create();
        MAP_THREADS[channel] = data;

        // Initialize the mp4 muxer for this channel
        mp4_muxer_init(channel, mp4_file_path, width, height);
    }
    return 0;
}

int mp4_dump(int channel, unsigned char* buffer, int size, uint64_t timestamp, EnumDataType type) {
    MP4Data *data = MAP_THREADS[channel];
    if (data == NULL) {
        //fprintf(stderr, "MP4 data is NULL. call start_mp4_dump\n");
        return 1;
    }

    // Enqueue buffer, and signal dump thread
    BufferDescriptor *desc = (BufferDescriptor*) malloc(sizeof(BufferDescriptor));
    desc->buffer = buffer;
    desc->size = size;
    if (data->prev_timestamp == 0) {
        desc->duration = 0;
    } else {
        desc->duration = timestamp - data->prev_timestamp;
    }
    data->prev_timestamp = timestamp;
    desc->type = type;

    /*if (0 == pthread_mutex_trylock(&data->mutex)) {
        pthread_mutex_unlock(&data->mutex);
    } else {
        free (desc);
    }*/

    if (queue_enqueue(data->queue, desc) != 0) {
        free (desc);
    }

    return 0;
}

int mp4_dump_deinit(int channel) {
    // stop thread for this channel

    MP4Data *data = MAP_THREADS[channel];
    if (data != NULL) {
        data->dump = 0;
        fprintf(stderr, "Waiting for mp4 dump thread to complete\n");
        pthread_join(data->thread, NULL);
        fprintf(stderr, "mp4 dump thread completed\n");
        BufferDescriptor *desc;
        while(queue_dequeue(data->queue, (void**) &desc) == 0) {
            free(desc->buffer);
            free(desc);
        }
        queue_destroy(data->queue);
        free(data);
        MAP_THREADS[channel] = NULL;
    }

    mp4_muxer_deinit(channel);

    return 0;
}


