/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
/*
 * Mp4Muxer.h
 *
 *  Created on: May 26, 2015
 *      Author: bsmith
 */

#ifndef MP4MUXER_H_
#define MP4MUXER_H_

#include <stdio.h>

#ifdef __cplusplus

#include "mp4v2/mp4v2.h"

class Mp4Muxer {
public:
    Mp4Muxer():filepath(NULL), mp4(NULL), video_track(0), sps_pps_found(0) {};
    virtual ~Mp4Muxer();

    int init(char* fpath, int width, int height);
    int mux(unsigned char* buffer, int length, uint64_t duration);
    void deinit();

private:
    char *filepath;

    MP4FileHandle mp4;
    MP4TrackId  video_track;
    int sps_pps_found;
};
#endif

#ifdef __cplusplus
extern "C" {
#endif

extern int mp4_muxer_init(int channel, char *filepath, int width, int height);
extern int mp4_muxer_mux(int channel, unsigned char* buffer, int length, uint64_t duration);
extern void mp4_muxer_deinit(int channel);

#ifdef __cplusplus
}
#endif

#endif /* MP4MUXER_H_ */
