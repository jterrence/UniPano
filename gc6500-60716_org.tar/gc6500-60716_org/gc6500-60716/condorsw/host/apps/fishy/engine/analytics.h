/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef __ANALYTICS_H_
#define __ANALYTICS_H_

#include "../include/interface.h"

#ifdef  __cplusplus
extern "C" {
#endif

#include "mxuvc.h"

void rawCapture_cb(unsigned char *data, unsigned int length, video_info_t info, void *userData);

int smInit(video_session_t *ses);
int smDeinit(void);

int smAddMotionROI(int regId, int xOff, int yOff, int width, int height);
int smRemoveMotionROI(int regId);

#ifdef  __cplusplus
}
#endif




#define FRAME_WIDTH                 640
#define FRAME_HEIGHT                360

// This Parameter is used for setting sensitivity
// For high sensitivity, set HIGH_SENSITIVITY
// For medium sensitivity, set MED_SENSITIVITY
// For low sensitivity, set LOW_SENSITIVITY
#define MOTION_SENSITIVITY        MED_SENSITIVITY 

//Number of frames with objects detected in the last 30 frames to trigger motion started alert
#define SMARTMOTION_START_THRESHOLD 1
//Number of frames with objects detected in the last 30 frames to trigger motion ended alert
#define SMARTMOTION_END_THRESHOLD   0

#define MAX_BUFFERS                 2
#define MAX_NUM_REGIONS             12

#define LOG_TRACE(...)  { \
    if(verbose) \
        printf(__VA_ARGS__); \
    }

#define LOG_INFO(...) { \
    if(verbose) { \
        printf("\033[22;31m"); \
        printf(__VA_ARGS__); \
        printf("\033[0m"); \
        printf("\n"); \
    } else \
        printf(__VA_ARGS__); \
    }

#define LOG_ERR  LOG_INFO


typedef struct {
    unsigned char *pdata;
    unsigned int datalength;
    video_info_t info;
}RawFrameInfo;

typedef struct {
    bool empty;
    RawFrameInfo frame;
}DataBuffer;

typedef struct
{
    int xOff;
    int yOff;
    int width;
    int height;
}Region;

#endif

