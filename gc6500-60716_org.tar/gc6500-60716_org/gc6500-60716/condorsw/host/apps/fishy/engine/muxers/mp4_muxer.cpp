/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
/*
 * Mp4Muxer.cpp
 *
 *  Created on: May 26, 2015
 *      Author: bsmith
 */

#include "mp4_muxer.h"
#include <map>

static const int NALU_SPS               = 7;
static const int NALU_PPS               = 8;
static const int NALU_DELIMITER         = 9;
static const int NALU_DELIMITER_SIZE    = 6;
static int sps_size                     = 0;
static int pps_size                     = 0;
const int mp4_time_scale                = 90000;

static int get_nal_header(unsigned char *buf, int parseLength, int spsPpsFound, int *offset, int *nalType);

// Mp4Muxer class members
int Mp4Muxer::init(char* fpath, int width, int height) {
    this->filepath = fpath;
    mp4 = MP4Create(filepath, 0 /*MP4_CREATE_64BIT_DATA*/);
    if(mp4 == MP4_INVALID_FILE_HANDLE) {
        fprintf(stderr, "MP4Create error\n");
        return 1;
    }

    MP4SetTimeScale(mp4, mp4_time_scale);
    MP4SetVideoProfileLevel(mp4, 0x7f);
    video_track = MP4AddH264VideoTrack(mp4, mp4_time_scale, mp4_time_scale / 30, width, height,
    0x42 /*sps[5]*/,
    0xa0 /*sps[6]*/,
    0x1f /*sps[7]*/,
    3);

    return 0;
}

int Mp4Muxer::mux(unsigned char* buffer, int length, uint64_t duration) {
    unsigned char* ptr = buffer;
    int offset = 0;
    int nal_type = 0;

    while (length > 0) {
        int nal_size = get_nal_header(ptr, length, sps_pps_found, &offset, &nal_type);

        if (nal_type == NALU_DELIMITER) {
            ptr += NALU_DELIMITER_SIZE;
            length -= NALU_DELIMITER_SIZE;
            continue;
        } else if ((nal_size == 0) && (nal_type != NALU_SPS) && (nal_type != NALU_PPS) && !sps_pps_found) {
            break;
        }

        if (!sps_pps_found) {
            if (nal_type == NALU_SPS) {
                int i = 0;
                fprintf(stderr, "[%s]SPS data:\n", __FUNCTION__);
                for (i = 0; i < nal_size; i++)
                    fprintf(stderr, "%x ", (unsigned char) (ptr[i]));
                fprintf(stderr, "\n");

                MP4AddH264SequenceParameterSet(mp4, video_track, ptr, nal_size);
                length -= nal_size;
                ptr += nal_size;
                sps_size = nal_size;
                continue;
            } else if (nal_type == NALU_PPS) {
                int i = 0;
                fprintf(stderr, "[%s]PPS data:\n", __FUNCTION__);
                for (i = 0; i < nal_size; i++)
                    fprintf(stderr, "%x ", (unsigned char) (ptr[i]));
                fprintf(stderr, "\n\n");

                MP4AddH264PictureParameterSet(mp4, video_track, ptr, nal_size);
                sps_pps_found = 1;
                length -= nal_size;
                ptr += nal_size;
                pps_size = nal_size;
                continue;
            } else {
                fprintf(stderr, "[%s] ERR: Should not reach here\n", __FUNCTION__);
            }
        } else {
            int iframe_offset = 0;

            if ((nal_type == NALU_PPS) || (nal_type == NALU_SPS) || (nal_type == 6)) {
                iframe_offset = sps_size + pps_size;
            }

            ptr[iframe_offset + 0] = ((nal_size - iframe_offset - 4) >> 24) & 0xff;
            ptr[iframe_offset + 1] = ((nal_size - iframe_offset - 4) >> 16) & 0xff;
            ptr[iframe_offset + 2] = ((nal_size - iframe_offset - 4) >> 8) & 0xff;
            ptr[iframe_offset + 3] = ((nal_size - iframe_offset - 4) >> 0) & 0xff;

            MP4Duration dur;
            if (duration <= 0 ) {
                dur = mp4_time_scale / 30;
                //dur = 0;
            } else {
                dur = (MP4Duration) duration;// mp4_time_scale / 30;
            }
            //fprintf(stderr, "dur:%llu && duration:%llu\n",dur,duration);
            if (!MP4WriteSample(mp4, video_track, (ptr + iframe_offset), (length - iframe_offset), dur, 0, 0)) {
                fprintf(stderr, "Encode mp4 error.\n");
                return 1;
            }
        }

        length -= nal_size;
        ptr += nal_size;
    }

    return 0;
}

Mp4Muxer::~Mp4Muxer() {
    this->filepath = NULL;
    this->deinit();
}

void Mp4Muxer::deinit() {
    if (mp4) {
        MP4Close(mp4,0);

        mp4 = NULL;
    }
}
// End Mp4Muxer class members

// Map of channel=>Mp4Muxer object
std::map <int, Mp4Muxer*> map_muxer;

int mp4_muxer_init(int channel, char *filepath, int width, int height) {
    Mp4Muxer *muxer = map_muxer[channel];
    if (!muxer) {
        muxer = new Mp4Muxer();
	if(!muxer)
	    return 1;	
        if (muxer->init(filepath, width, height) != 0) {
	    delete muxer;
            return 1;
        }
        map_muxer[channel] = muxer;
    }

    return 0;
}

int mp4_muxer_mux(int channel, unsigned char* buffer, int length, uint64_t duration) {
    Mp4Muxer *muxer = map_muxer[channel];
    if (muxer) {
        return muxer->mux(buffer, length, duration);
    }
    return 1;
}

void mp4_muxer_deinit(int channel) {
    Mp4Muxer *muxer = map_muxer[channel];
    if (muxer) {
        delete muxer;
        map_muxer[channel] = NULL;
    }
}

// static function definition
int get_nal_header(unsigned char *buf, int parseLength, int spsPpsFound, int *offset, int *nalType)
{
    unsigned char vidData;
    int offtemp = 0;
    long status = 0;
    int parseForSize = 0;
    int nalSize = 0;
    int nalTypeTemp ;

    *nalType = 0;
    *offset = 0 ;

    if(spsPpsFound == 1)
    {
        if((buf[0] == 0x00) &&  (buf[1] == 0x00) && (buf[2] == 0x00) && (buf[3] == 0x01))
            *nalType = buf[4] & 0x1F;
    return parseLength;
    }
    else if (spsPpsFound == 0)
    {
        while((parseLength > 0) && (parseForSize == 0))
        {
            vidData = buf[(*offset)];
            (*offset)++;
            parseLength--;

            switch(status)
            {
            case 0:
                {
                    if (vidData == 0x00) // NAL Header first 3 byte = "0x00 00 00"
                    {
                        status++;
                    }
                    else
                    {
                        status = 0;
                    }
                }
                break;
            case 1:
                {
                    if (vidData == 0x00) // NAL Header first 3 byte = "0x00 00 00"
                    {
                        status++;
                    }
                    else
                    {
                        status = 0;
                    }
                }
                break;
            case 2:
                {
                    if (vidData == 0x00) // NAL Header first 3 byte = "0x00 00 00"
                    {
                        status++;
                    }
                    else
                    {
                        status = 0;
                    }
                }
                break;

            case 3:
                {
                    if (vidData == 0x01) // NAL Header 4th byte = '0x01'
                    {
                        status++;
                    }
                    else
                    {
                        status = 0;
                    }
                }
                break;

            case 4: // 1st byte of NAL Header
                {
                    *nalType = vidData & 0x1F;
                    if ((*nalType) == NALU_SPS || (*nalType) == NALU_PPS)
                    {
                        parseForSize = 1;
                    }
                    else if ((*nalType) == NALU_DELIMITER)
                    {
                        return (*offset+1);
                    }
                   else
                    {
                        status = 0 ;
                    }

                }
                break;
	   default:
		break;
            } // end of switch
        } //end of while

        buf += (*offset);
        while((parseLength > 0) && (parseForSize == 1) && ((*nalType) == NALU_PPS || (*nalType) == NALU_SPS))
        {
            vidData = buf[offtemp];
            offtemp++;
            parseLength--;
            switch(status)
            {
            case 0:
                {
                    if (vidData == 0x00) // NAL Header first 3 byte = "0x00 00 00"
                    {
                        status++;
                    }
                    else
                    {
                        status = 0;
                    }
                }
                break;
            case 1:
                {
                    if (vidData == 0x00) // NAL Header first 3 byte = "0x00 00 00"
                    {
                        status++;
                    }
                    else
                    {
                        status = 0;
                    }
                }
                break;
            case 2:
                {
                    if (vidData == 0x00) // NAL Header first 3 byte = "0x00 00 00"
                    {
                        status++;
                    }
                    else
                    {
                        status = 0;
                    }
                }
                break;

            case 3:
                {
                    if (vidData == 0x01) // NAL Header 4th byte = '0x01'
                    {
                        status++;
                    }
                    else
                    {
                        status = 0;
                    }
                }
                break;

            case 4: // 1st byte of NAL Header
                {
                    nalTypeTemp = vidData & 0x1F;
                    if (((*nalType) == NALU_SPS && nalTypeTemp == NALU_PPS) || ((*nalType) == NALU_PPS && nalTypeTemp == 5))
                    {
                        parseLength = 0;
                        nalSize = offtemp;
                    }
                    else
                    {
                        status = 0 ;
                    }

                }
                break;
	    default:
		break;
            } // end of switch
        } //end of while
    }
    return nalSize;
}
