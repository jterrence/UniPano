/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <assert.h>
#include "analytics.h"
#include "smartmotion/inc/Image.h"
#include "Reverse_ePTZ.h"

#define  TESTDATA         1              //0:Actual rectangles; 1:Test data

static Motion MotionInfo;
static Rect   RectInfo[50];
static DataBuffer buffer[MAX_BUFFERS];
static pthread_mutex_t mutex;
static pthread_cond_t waitForData;
static int writeIdx = 0;
static int readIdx = 0;
static int verbose = 0;

static Config *cfg=NULL;
static Image *img=NULL;
static int *HVHFMBData=NULL;
static int *SobelMBData=NULL;
static int frameCount;              //Count for frames (to be reset after every 30 frames)
static int objectDetectedFrameCount;  //Count for frame in which soee object is detected
static int objectDetected[30];       //Array containing object detected info for the last 30 frames
static int objectDetectedIndex;      //Index for writing new info in above array
static int motionDetected;            //Flag indicating valid motion detected based on object detected info above
static int transition;               //Last alert transition
static Region region[MAX_NUM_REGIONS];

static pthread_t threadId;
static int       smActive = false;
static int frameWidth = FRAME_WIDTH;    //Will be overwritten later
static int frameHeight = FRAME_HEIGHT;  //Will be overwritten later
static int mbWidth;       //Width of stream in number of macro blocks
static int mbHeight;      //Height of stream in number of macro blocks

static int mbStride;      //Stride for macroblock stats
static int mbStatSize;    //Total size of stats area including padding after each stride
static int numOfMB;       //Total number of macroblocks

static int startx=48;
static int starty=176;
static int count=0;
//local functions decls.
static void* processRawFrame(void* userData);
static int smOpen(void);
static int smClose(void);
static int smProcess(DataBuffer *prevBuffer, DataBuffer *currentBuffer);

int smInit(video_session_t* ses)
{
    int ret = 0;
    for(int i=0; i<MAX_BUFFERS;i++)
    {
        buffer[i].empty = true;
        memset(&buffer[i].frame,0,sizeof(RawFrameInfo));
    }
	frameWidth  = ses->width;
	frameHeight = ses->height;
	//printf("\nFrame width = %d Frame Height = %d\n",frameWidth,frameHeight);
    MotionInfo.rectangles = &RectInfo[0];
    smOpen();
    smAddMotionROI(0, 0, 0, (ses->width/16), (ses->height/16));
    smActive = 1;
    //Spawn a thread to process captured RAW frames and detects smart motion using smart motion library
    ret = pthread_create (&threadId, NULL, &processRawFrame, ses);
    if ( ret < 0 )
        return ret;

    return 0;
}

int smDeinit(void)
{
    smActive = 0;
    pthread_join(threadId, NULL);
    smRemoveMotionROI(0);
    smClose();
    return 0;
}



//rawCapture_cb: This is the callback function registed with mxuvc to receive YUV frames captured
//data    :pointer to the buffer containing YUV frame; must be freed once processed
//length  :length of the buffer
//info    :other metadata containing motion statistics

void rawCapture_cb(unsigned char *data, unsigned int length, video_info_t info, void *userData)
{
    video_format_t fmt = (video_format_t) info.format;
    video_session_t * ses = ((video_session_t*)userData);


    if(!ses->active || !smActive)
    {
    	pthread_cond_signal(&waitForData);
        free(data);
    	return;
    }

    pthread_mutex_lock(&mutex);
    if(buffer[writeIdx].empty)
    {
        buffer[writeIdx].empty = false;
        buffer[writeIdx].frame.datalength = length;
        buffer[writeIdx].frame.pdata = data;
        buffer[writeIdx].frame.info.buf_index = info.buf_index;
        buffer[writeIdx].frame.info.format = VID_FORMAT_GREY_RAW;
        buffer[writeIdx].frame.info.ts = info.ts;
        buffer[writeIdx].frame.info.stats.buf = info.stats.buf;
        buffer[writeIdx].frame.info.stats.size = info.stats.size;
        writeIdx++;
        if(writeIdx >= MAX_BUFFERS)
        {
            writeIdx = 0;
        }
        pthread_cond_signal(&waitForData);
    }
    else
    {
        LOG_ERR("Buffer %d not empty and so drop the frame\n",writeIdx);
        free(data);
    }

    pthread_mutex_unlock(&mutex);
}

void* processRawFrame(void* userData)
{
    video_session_t *ses = ((video_session_t *)userData);

    while(smActive)
    {
        DataBuffer *buf1=NULL, *buf2=NULL;

        pthread_mutex_lock(&mutex);

        if(!ses->active || !smActive) break;

        buf1 = &buffer[readIdx];
        if(!buf1->empty)
        {
        	int nextRead = ((readIdx+1)>=MAX_BUFFERS) ? 0:(readIdx+1);
        	buf2 = &buffer[nextRead];
        	if(buf2->empty)
        		pthread_cond_wait(&waitForData, &mutex);
        }
        else
        {
        	pthread_cond_wait(&waitForData, &mutex);
        	pthread_mutex_unlock(&mutex);
        	continue;
        }

        pthread_mutex_unlock(&mutex);

#if !TESTDATA        
        smProcess(buf1, buf2);
#endif        
        free(buf1->frame.pdata);

        pthread_mutex_lock(&mutex);
        buf1->empty = true;
        readIdx++;
        if(readIdx >= MAX_BUFFERS)
        {
            readIdx = 0;
        }
        pthread_mutex_unlock(&mutex);

        //UI update support
        #if 1
        uint64_t ts = 0;
        //CES2015 stuff
            int src_width=1920;
            int src_height=1080;
            int dewarp_width=2*1920;
            int dewarp_height=2*1080;
            int MapN = 128;
            float hpan_out, vpan_out, hfov_out;
#define DIVISOR 100
#if TESTDATA   //Just for testing
            //float target_x = 320*1920.0/640;
            //float target_y = 90*1080.0/360;
            //float target_dist = 40*1080.0/360;
            float target_x = startx*1920.0/640;
            float target_y = starty*1080.0/360;
            float target_dist = 48*1080.0/360;
            
            MotionInfo.rectangles[0].xOff = (startx-48)/16;
            MotionInfo.rectangles[0].yOff = (starty-48)/16;
            MotionInfo.rectangles[0].width = 2*48/16;
            MotionInfo.rectangles[0].height = 2*48/16;
            MotionInfo.nr = 1;
            count++;
            if((count % 4) == 0)
            {
                startx+=16;
                if(startx > (640-48))
                    startx = 48;
            }
            if (count == 1 || (count % 4) != 0)
            {
                printf("***** Calling reverse_eptz with src_width=%d src_height=%d dewarp_width=%d dewarp_height=%d MapN=%d"
                    "      target_x=%f target_y=%f target_dist=%f\n",
                    src_width, src_height, dewarp_width, dewarp_height, MapN, target_x, target_y, target_dist);
                reverse_eptz(src_width, src_height, dewarp_width, dewarp_height, MapN, target_x, target_y, target_dist,
                    180.0, 833.0, 0, 0,        //Lens parameters
                    &hpan_out, &vpan_out, &hfov_out
                );
                printf("***** reverse_eptz returns hpan_out=%f vpan_out=%f hfov_out=%f\n",
                    hpan_out, vpan_out, hfov_out);
                dewarp_params_t p;
                p.eptz_mode_wm_1paneleptz.HPan = (int)(DIVISOR*hpan_out);
                p.eptz_mode_wm_1paneleptz.VPan = (int)(DIVISOR*vpan_out);
                p.eptz_mode_wm_1paneleptz.Zoom = (int)(DIVISOR*hfov_out);
                p.eptz_mode_wm_1paneleptz.Tilt = 0;
                p.eptz_mode_wm_1paneleptz.Divisor = DIVISOR;
                mxuvc_video_set_dewarp_params((video_channel_t) CH2, 0, EMODE_WM_1PanelEPTZ, &p);
            }
            //update UI
            if(ses!=NULL && ses->motion_func != NULL && ses->context != NULL)
            ses->motion_func(ses->context, &MotionInfo);
#else
        int nr=img->GetNumberofRectangles();
        if(nr != 0)
        {
            Rectangle *ptr;
            int rval;
            int num;        //No of rectangles with 6th bit indicating start of motion
            num = nr;
            if(motionDetected)
            {
                num |= 0x40;         //Set bit 6 indicating valid motion detected
            }
            
            Rectangle** recPtr = img->GetRectanglePointer();

            //FIXME: make sure nr does not exceed available static memory in array. 
            //So for now we have cap of 50 rectangles per frame.
            //nr = (nr < sizeof(RectInfo) ) ? nr : sizeof(RectInfo);

            MotionInfo.nr = nr;
            MotionInfo.motion  = motionDetected;
            for(int i=0;i<nr;i++)
            {
                ptr = recPtr[i];
                MotionInfo.rectangles[i].xOff= ptr->location.top_left_mbx;
                MotionInfo.rectangles[i].yOff= ptr->location.top_left_mby;
                MotionInfo.rectangles[i].width = ptr->location.bot_right_mbx - ptr->location.top_left_mbx + 1;
                MotionInfo.rectangles[i].height = ptr->location.bot_right_mby - ptr->location.top_left_mby + 1;
                   
            }

            //CES2015 stuff start
            //Find biggest rectangle - one with biggest area width*height
            int biggestRect = 0;
            int prevBiggestArea = 0;
            for(int i=0;i<nr;i++)
            {
                int area;
                area = MotionInfo.rectangles[i].width * MotionInfo.rectangles[i].height;
                if( area > prevBiggestArea)
                {
                    biggestRect=i;
                    prevBiggestArea = area;
                }
            }
            //Do ePTZ on CH2 using this rectangle
            //Assuming 640x360 resolution for raw capture
            float target_x = ((MotionInfo.rectangles[biggestRect].xOff + MotionInfo.rectangles[biggestRect].width/2)*16)*1920.0/640;
            float target_y = ((MotionInfo.rectangles[biggestRect].yOff + MotionInfo.rectangles[biggestRect].height/2)*16)*1080.0/360;
            float target_dist = (MotionInfo.rectangles[biggestRect].height * 16 / 2)*1080.0/360;
  
            printf("***** Calling reverse_eptz with src_width=%d src_height=%d dewarp_width=%d dewarp_height=%d MapN=%d"
                   "      target_x=%f target_y=%f target_dist=%f\n",
                   src_width, src_height, dewarp_width, dewarp_height, MapN, target_x, target_y, target_dist);
            reverse_eptz(src_width, src_height, dewarp_width, dewarp_height, MapN, target_x, target_y, target_dist,
                &hpan_out, &vpan_out, &hfov_out
            );
            printf("***** reverse_eptz returns hpan_out=%f vpan_out=%f hfov_out=%f\n",
                   hpan_out, vpan_out, hfov_out);
            dewarp_params_t p;
            p.eptz_mode_wm_1paneleptz.HPan = (int)(DIVISOR*hpan_out);
            p.eptz_mode_wm_1paneleptz.VPan = (int)(DIVISOR*vpan_out);
            p.eptz_mode_wm_1paneleptz.Zoom = (int)(DIVISOR*hfov_out);
            p.eptz_mode_wm_1paneleptz.Tilt = 0;
            p.eptz_mode_wm_1paneleptz.Divisor = DIVISOR;
            mxuvc_video_set_dewarp_params((video_channel_t) CH2, 0, EMODE_WM_1PanelEPTZ, &p);
            //CES2015 stuff end
            
            //update UI
            if(ses!=NULL && ses->motion_func != NULL && ses->context != NULL)
            ses->motion_func(ses->context, &MotionInfo);
           
        }
#endif
        #endif
    }

    return NULL;
}



//smOpen: This function will create and initialize the interanal resources

int smOpen(void)
{
    int i;

    motionDetected = 0;
    transition = 0;
    objectDetectedIndex = 0;
    frameCount = 0;
    for(i=0; i < MAX_NUM_REGIONS; i++)
    {
        region[i].xOff = 0;
        region[i].yOff = 0;
        region[i].width = 0;
        region[i].height = 0;
    }

    if(cfg != NULL)
    {
        printf("Error: smInit() called without calling smDeinit() before in file %s line %d\n", __FILE__, __LINE__);
        exit(1);
    }
    cfg  = new Config();
    cfg->SetImage(frameWidth, frameHeight);
    cfg->SetSensitivity(MOTION_SENSITIVITY);  //Can be changed anytime
    img = new Image(cfg);


    mbWidth = (frameWidth+15)>>4; //MB width
    mbHeight = (frameHeight+15)>>4; //MB height
    mbStride = ((mbWidth + 7) & ~0x7);
    mbStatSize = mbStride * mbHeight;

    numOfMB = mbWidth * mbHeight;
    HVHFMBData = new int[numOfMB];
    SobelMBData = new int[numOfMB];
    img->SetFeatureAddress(HVHF, HVHFMBData );
    img->SetFeatureAddress(SOBEL, SobelMBData );

    return 0;
}

//smClose: This function will free up interanal resources

int smClose(void)
{
    delete [] SobelMBData;
    delete [] HVHFMBData;
    delete img;
    delete cfg;

    SobelMBData = NULL;
    HVHFMBData = NULL;
    img = NULL;
    cfg = NULL;

    return 0;
}



//smProcess: This function process the raw frames and detects the smart motion

int smProcess(DataBuffer *prevBuffer, DataBuffer *currentBuffer)
{
    int nr;
    uint64_t ts;
    uint64_t tsNext;
    Rectangle** recPtr;
    int overlap=0;
    int i,j=0;
    int row,col,addr;
    int statOffsetHVHFMBData, statOffsetSobelMBData;
    dewarp_params_t p;

    if((prevBuffer == NULL) || (currentBuffer==NULL))
    {
        LOG_ERR("ERR: NULL buffer received\n");
        return -1;
    }

    unsigned char* prevFrame = prevBuffer->frame.pdata;
    unsigned int prevFrameSize = prevBuffer->frame.datalength;
    unsigned char* prevMotionStatsData = prevBuffer->frame.info.stats.buf;
    unsigned int prevMotionStatsSize = prevBuffer->frame.info.stats.size;

    unsigned char* curFrame = currentBuffer->frame.pdata;
    unsigned int curFrameSize = currentBuffer->frame.datalength;
    unsigned char* curMotionStatsData = currentBuffer->frame.info.stats.buf;
    unsigned int curMotionStatsSize = currentBuffer->frame.info.stats.size;

    if( (prevFrame==NULL) || (prevFrameSize <= 0) || (curFrame==NULL) || (curFrameSize <= 0))
    {
        LOG_ERR("ERR: NULL data received\n");
        return -1;
    }

    if( (prevMotionStatsData==NULL) || (prevMotionStatsSize <= 0) || (curMotionStatsData==NULL) || (curMotionStatsSize <= 0))
    {
        LOG_ERR("ERR: NULL motion stats received\n");
        return -1;
    }

    statOffsetHVHFMBData  = mbStatSize*2;
    statOffsetSobelMBData = mbStatSize*4;
    
    for(row=0, i=0; row<mbHeight; row++)
    {
	    addr = row * mbStride;
	    for(col=0; col<mbWidth; col++)
	    {
		    //See Table 19.1: Macroblock Statistics Storage Format in codec reference manual
		    //We have stats of each type for each macro block followed by
		    //next stat for each macro block
		    //Luma High Freq MBAvg
		    HVHFMBData[i] = curMotionStatsData[statOffsetHVHFMBData + addr];
		    //Luma Edge Str MBAvg (2 bytes per stat - we need the 2nd byte)
		    SobelMBData[i] = curMotionStatsData[statOffsetSobelMBData + 2*addr + 1];

		    i++;
		    addr++;
	    }
    }

    img->SetYAddress(curFrame, prevFrame);
    img->ProcessData();
    ts = prevBuffer->frame.info.ts;
    tsNext = currentBuffer->frame.info.ts;

    //Get rectangles
    nr=img->GetNumberofRectangles();
    if(nr != 0)
    {
        Rectangle *ptr;

        LOG_TRACE("\n*** %d rectangles (x y width height) for smart motion:\n", nr);

        recPtr = img->GetRectanglePointer();
        overlap=0;
        for(i=0;i<nr;i++)
        {
            ptr = recPtr[i];

            LOG_TRACE("Rect %d: %d %d %d %d\n", i+1,
                            ptr->location.top_left_mbx, ptr->location.top_left_mby,
                            ptr->location.bot_right_mbx - ptr->location.top_left_mbx + 1,    //width
                            ptr->location.bot_right_mby - ptr->location.top_left_mby + 1     //height
            );

            //Compare rectangle with the regions rectangles for overlap
            if(overlap == 0)     //Check only if no previous overlap
            {
                for(j=0;j<MAX_NUM_REGIONS;j++)
                {
                    if(ptr->location.top_left_mbx < region[j].xOff+region[j].width
                        && ptr->location.bot_right_mbx >= region[j].xOff
                        && ptr->location.top_left_mby < region[j].yOff+region[j].height
                        && ptr->location.bot_right_mby >= region[j].yOff)
                    {
                        //Overlap - no need to check with remaining rectangles
                        overlap=1;

                        LOG_TRACE("*** Rectangle %d %d %d %d overlapping with region %d %d %d %d\n",
                                ptr->location.top_left_mbx, ptr->location.top_left_mby,
                                ptr->location.bot_right_mbx - ptr->location.top_left_mbx + 1,    //width
                                ptr->location.bot_right_mby - ptr->location.top_left_mby + 1,     //height
                                region[j].xOff, region[j].yOff, region[j].width, region[j].height
                        );
                        break;
                    }
                }
            }
        }
    }


    //If at least SMARTMOTION_START_THRESHOLD frames with objects detected in last 30 frames, then signal
    //a motion start event.

     if(frameCount < 30)
         frameCount++;
     assert(objectDetectedIndex < 30);
     objectDetected[objectDetectedIndex] = overlap;

     objectDetectedIndex = (objectDetectedIndex + 1) % 30;
     if(frameCount == 30)
     {
         //Calculate number of frames with objects in last window
         int i;
         int count=0;
         for(i=0; i<30; i++)
         {
             if(objectDetected[i])
                 count++;
         }
         if(count >= SMARTMOTION_START_THRESHOLD)
         {
             if(!motionDetected)
             {
                 motionDetected=1;
                 LOG_TRACE("*** %d or more frames with motion detected - %d\n", SMARTMOTION_START_THRESHOLD, count);
             }
         }
         else if(count <= SMARTMOTION_END_THRESHOLD)
         {
             if(motionDetected)
             {
                 motionDetected=0;
                 LOG_TRACE("*** Less than or equal to %d frames with motion detected - %d\n", SMARTMOTION_END_THRESHOLD, count);
             }
         }
    }

    if(transition != motionDetected)
    {
        transition = motionDetected;
        if(transition == 0)
        {
            p.eptz_mode_wm_zclstretch.HPan = 0;
            p.eptz_mode_wm_zclstretch.VPan = 0;
            p.eptz_mode_wm_zclstretch.Zoom = 1;        
            p.eptz_mode_wm_zclstretch.Divisor = 1;        
            mxuvc_video_set_dewarp_params((video_channel_t)CH2, 0, EMODE_WM_ZCLStretch, &p);
        }
#if TESTDATA //CES2015 stuff
        {
            startx=48;
            starty=176;
            count=0;
        }
#endif        
        LOG_INFO("\n[Motion Dectected] state - %s\n",(transition ? "#motion started":"#motion stopped"));
    }

    return 0;
}



//smAddMotionROI: This function is to set motion region

int smAddMotionROI(int regId, int xOff, int yOff, int width, int height)
{
    if(regId < MAX_NUM_REGIONS-1)
    {
        region[regId].xOff = xOff;
        region[regId].yOff = yOff;
        region[regId].width = width;
        region[regId].height = height;
    }

    return 0;
}



//smRemoveMotionROI: This function is to remove motion region

int smRemoveMotionROI(int regId)
{
    if(regId < MAX_NUM_REGIONS-1)
    {
        region[regId].xOff = 0;
        region[regId].yOff = 0;
        region[regId].width = 0;
        region[regId].height = 0;
    }
    return 0;
}
