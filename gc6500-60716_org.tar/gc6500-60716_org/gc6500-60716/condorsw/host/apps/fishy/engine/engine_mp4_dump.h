/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
/*
 * engine_mp4_dump.h
 *
 *  Created on: May 27, 2015
 *      Author: bsmith
 */

#ifndef ENGINE_MP4_DUMP_H_
#define ENGINE_MP4_DUMP_H_

#include <stdint.h>

typedef enum {
    DATA_VIDEO = 0,
    DATA_AUDIO,
    NUM
} EnumDataType;

int mp4_dump_init(int channel, char* mp4_file_path, int width, int height);
int mp4_dump(int channel, unsigned char* buffer, int size, uint64_t timestamp, EnumDataType type);
int mp4_dump_deinit(int channel);

#endif /* ENGINE_MP4_DUMP_H_ */
