/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include <unistd.h>
#include <pthread.h>
#include <sys/time.h>
#include <string.h>
#include <syslog.h>
#include <sys/types.h>
#include <dirent.h>
#include <vector>
#include <stdio.h>
#include <string>
#include <algorithm>
#include "camera.h"

int camId[2] = {-1, -1};

int findCameraDevices(const char* name)
{
	int id = 0;
	std::string v4lDir;
	std::string productStringFile;
	v4lDir.assign("/sys/class/video4linux/");
	struct dirent* pDirEntryResult;
	char tmp[128];
	std::vector <std::string> videoDevList;

	/* Open up the directory */
	DIR *list = opendir( v4lDir.c_str() );

	if( list == NULL )
	{
		/* No directory found */
                printf("Path %s not found\n",v4lDir.c_str());
		return -1;
	}

	
	while(1)
	{
		/* OK directory is open - so scan and get the next item */
		pDirEntryResult = readdir(list);

		if( pDirEntryResult == NULL )
		{
			break;
		}
		else
		{	
			/* OK we have a file */
			/* ignore file names starting with . */
			if (pDirEntryResult->d_name[0] == '.')
			{
				continue;
			}

			std::string videoDev(pDirEntryResult->d_name);

			if(videoDev.find("video") == std::string::npos)
			{
				continue;
			}

			videoDevList.push_back(videoDev);
		}
	}
	closedir(list);

	std::sort( videoDevList.begin(), videoDevList.end() );
	std::vector <std::string>::iterator it;
	for(it=videoDevList.begin(); it < videoDevList.end(); it++)
	{
		std::string videoDev(*it);
		productStringFile = v4lDir + videoDev + "/" + "name";

		FILE* fptr = fopen(productStringFile.c_str(),"r");
		if(fptr != NULL)
		{
			size_t result = fread(tmp,1, 128, fptr);
			if (result == 0) {
				fprintf(stderr, "fread failed\n");
			}

			if(!strncmp(tmp,name,strlen(name)))
			{

				videoDev = videoDev.replace(0,5,"");
                                camId[id] = atoi(videoDev.c_str());
                                printf("\ndevice id %d\n", camId[id]);
                                id++;
			}
			fclose(fptr);
		}
	}
	return id;
}
