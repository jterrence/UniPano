/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "list.h"

struct list_item {
	void *data;
	struct list_item *next;
};

struct mx_list {
	struct list_item *first;
	struct list_item *last;
	struct list_item *cur;
};


mx_list_t* list_create()
{
	mx_list_t *l = calloc(1, sizeof(mx_list_t));
	//l->first = malloc(sizeof(struct list_item));
	//l->last = l->first;
	//l->cur = l->first;

	return l;
}

void list_destroy(mx_list_t *l)
{
	if(l == NULL)
		return;

	struct list_item *it = l->first;
	while(it) {
		struct list_item *tmp = it;
		it = it->next;
		free(tmp);
	}
}

int list_add(mx_list_t *l, void *data)
{
	if(l == NULL)
		return -1;

	struct list_item *it = calloc(1, sizeof(struct list_item));
	it->data = data;
	it->next = NULL;

	if(l->first == NULL) {
		l->first = it;
		l->cur   = it;
	} else {
		l->last->next = it;
	}

	l->last = it;

	return 0;
}

int list_getnext(mx_list_t *l, void **data)
{
	if(l == NULL || l->cur == NULL)
		return -1;

	//if(l->cur->next == NULL)
	//	return -1;

	*data = l->cur->data;
	l->cur = l->cur->next;

	//l->cur = l->cur->next;
	//*data = l->cur->data;

	return 0;

}

int list_reset(mx_list_t *l)
{
	if(l == NULL)
		return -1;

	l->cur = l->first;

	return 0;
}

#if 0
int main()
{
	mx_list_t *l = list_create();
	list_add(l, 10);
	list_add(l, 30);
	list_add(l, 50);

	int val;
	while(list_getnext(l, (void**)&val) >= 0) {
		printf("val = %i\n", val);
	}

	list_reset(l);

	while(list_getnext(l, (void**)&val) >= 0) {
		printf("val = %i\n", val);
	}
}
#endif
