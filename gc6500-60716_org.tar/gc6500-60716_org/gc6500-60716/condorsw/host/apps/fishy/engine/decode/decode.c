/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#ifdef NOX
struct decode {
	char		type;
};

struct decode* decode_create(char type)
{
	return NULL;
}
void decode_destroy(struct decode* dec)
{
}
struct framebuffer* decode_frame(struct decode *dec,
		const unsigned char *frame, signed long length)
{
	return NULL;
}
#else
#include <libavcodec/avcodec.h>
#include <jpegutils.h>
#include <pthread.h>
#include <mjpeg_logging.h>

#include "decode.h"
extern pthread_mutex_t     gMutex;
struct decode {
	char		type;
	/* AVC */
	AVCodecContext 	*codec_ctx;
	AVFrame 	*frame;
	/* JPEG */
};

/* AVC */
static struct decode* decode_create_avc()
{
	struct decode *dec;
	AVCodec *av_codec;

	dec = malloc(sizeof(struct decode));
	if (dec == NULL) {
		perror("malloc");
		goto err_decode;
	}
	dec->type = DEC_AVC;

	avcodec_register_all();

	av_codec = avcodec_find_decoder(CODEC_ID_H264);

	if(av_codec == NULL) {
		printf("avcodec_find_decoder failed\n");
		goto err_decode;
	}

	dec->codec_ctx = avcodec_alloc_context3(NULL);
	if (dec->codec_ctx == NULL) {
		printf("avcodec_alloc_context failed\n");
		goto err_decode;
	}
	//dec->codec_ctx->debug = 0xffff;
	if(avcodec_open2(dec->codec_ctx, av_codec, NULL)<0) {
		printf("avcodec_open failed\n");
		goto err_decode;
	}

	dec->frame = avcodec_alloc_frame();
	if (dec->frame == NULL) {
		printf("avcodec_alloc_frame failed\n");
		goto err_decode;
	}

	return dec;

err_decode:
	if (dec != NULL)
		free(dec);
	return NULL;

}

static void decode_destroy_avc(struct decode* dec)
{
	pthread_mutex_lock(&gMutex);
	av_free(dec->frame);
	av_free(dec->codec_ctx);
	free(dec);
	pthread_mutex_unlock(&gMutex);
}

static void avcodec_copy_out(AVCodecContext* ctx, AVFrame* frame, struct framebuffer* frm, void* out)
{
	int z;
	int plane_y_idx = 0;
	int plane_u_idx = 1;
	int plane_v_idx = 2;
	/* Defaults for YUV 420 */
	int plane_u_inc = 1;
	int plane_v_inc = 1;
	int plane_u_fac = 2;
	int plane_v_fac = 2;
	void *data;
	uint8_t *dest;

	/* setup framebuffer */
	if (frm != NULL) {
		frm->pixelformat = FOURCC_I420;
		frm->width = ctx->width;
		frm->height = ctx->height;
		frm->size = 3*frm->width*frm->height/2;
		frm->data = malloc(frm->size);
		dest = frm->data;
	} else {
		dest = out;
	}

	if(ctx->pix_fmt == PIX_FMT_YUV422P || ctx->pix_fmt == PIX_FMT_YUVJ422P) {
		plane_u_fac = plane_v_fac = 1;
		plane_u_inc = plane_v_inc = 2;
	}

	for (z = 0; frame->data[plane_y_idx] && z < ctx->height; z++)
	{
		data = (void*)&(frame->data[plane_y_idx][frame->linesize[plane_y_idx]*z]);
		memcpy(dest, data, ctx->width);
		dest += ctx->width;
	}
	for (z = 0; frame->data[plane_u_idx] && z < ctx->height/plane_u_fac; z+=plane_u_inc)
	{
		data = (void*)&(frame->data[plane_u_idx][frame->linesize[plane_u_idx]*z]);
		memcpy(dest, data, ctx->width / 2);
		dest += ctx->width / 2;
	}
	for (z = 0; frame->data[plane_v_idx] && z < ctx->height/plane_v_fac; z+=plane_v_inc)
	{
		data = (void*)&(frame->data[plane_v_idx][frame->linesize[plane_v_idx]*z]);
		memcpy(dest, data, ctx->width / 2);
		dest += ctx->width / 2;
	}
}

static struct framebuffer* decode_frame_avc(struct decode *dec, const unsigned char *frame, signed long length, void *out)
{
	AVPacket avpkt;
	signed long decoded_len;
	int got_pic = 0;
	avpkt.pts = AV_NOPTS_VALUE;
	avpkt.dts = AV_NOPTS_VALUE;

#pragma GCC diagnostic push  // require GCC 4.6
#pragma GCC diagnostic ignored "-Wcast-qual"
	// AVPacket->data is of uint8_t* data type. type-casting "const" pointer frame to non-const throws a
	// "cast discards ‘__attribute__((const))’ qualifier" warning. However, since this is being done on purpose,
	// we are ignoring the warning for this line.
	avpkt.data = (uint8_t*)frame;
#pragma GCC diagnostic pop
	avpkt.size = length;
	avpkt.duration = 0;

	decoded_len = avcodec_decode_video2(dec->codec_ctx, dec->frame, &got_pic, &avpkt);

	if (decoded_len < 0) {
		printf("avcodec_decode_video failed: res: %ld, gotPicture: %d\n",
				decoded_len, got_pic);
		return NULL;
	}
	if (!got_pic && decoded_len != length) {
		printf("decoded less data: given %ld, decoded %ld\n",
				length, decoded_len);
	}
	if (got_pic) {
		if (out != NULL) {
			avcodec_copy_out(dec->codec_ctx, dec->frame, NULL, out);
		} else {
			struct framebuffer* frm = malloc(sizeof(struct framebuffer));
			avcodec_copy_out(dec->codec_ctx, dec->frame, frm, NULL);
			return frm;
		}
	}
	return NULL;
}


/* JPEG */
static struct decode* decode_create_jpeg()
{
	struct decode *dec;
	AVCodec *av_codec;

	dec = malloc(sizeof(struct decode));
	if (dec == NULL) {
		perror("malloc");
		goto err_decode;
	}
	dec->type = DEC_MJPEG;

	avcodec_register_all();

	av_codec = avcodec_find_decoder(CODEC_ID_MJPEG);

	if(av_codec == NULL) {
		printf("avcodec_find_decoder failed\n");
		goto err_decode;
	}

	dec->codec_ctx = avcodec_alloc_context3(NULL);
	if (dec->codec_ctx == NULL) {
		printf("avcodec_alloc_context failed\n");
		goto err_decode;
	}
	//dec->codec_ctx->debug = 0xffff;
	if(avcodec_open2(dec->codec_ctx, av_codec, NULL)<0) {
		printf("avcodec_open failed\n");
		goto err_decode;
	}

	dec->frame = avcodec_alloc_frame();
	if (dec->frame == NULL) {
		printf("avcodec_alloc_frame failed\n");
		goto err_decode;
	}

	return dec;

err_decode:
	if (dec != NULL)
		free(dec);
	return NULL;

}

//AAC
static struct decode* decode_create_aac()
{
	struct decode *dec;
	AVCodec *av_codec;

	dec = malloc(sizeof(struct decode));
	if (dec == NULL) {
		perror("malloc");
		goto err_decode;
	}
	dec->type = DEC_AAC;

	avcodec_register_all();

	av_codec = avcodec_find_decoder(CODEC_ID_AAC);

	if(av_codec == NULL) {
		printf("avcodec_find_decoder failed\n");
		goto err_decode;
	}

	dec->codec_ctx = avcodec_alloc_context3(NULL);
	if (dec->codec_ctx == NULL) {
		printf("avcodec_alloc_context failed\n");
		goto err_decode;
	}
	//dec->codec_ctx->debug = 0xffff;
	if(avcodec_open2(dec->codec_ctx, av_codec, NULL)<0) {
		printf("avcodec_open failed\n");
		goto err_decode;
	}

	dec->frame = avcodec_alloc_frame();
	if (dec->frame == NULL) {
		printf("avcodec_alloc_frame failed\n");
		goto err_decode;
	}

	return dec;

err_decode:
	if (dec != NULL)
		free(dec);
	return NULL;

}
static void decode_destroy_jpeg(struct decode* dec)
{
	av_free(dec->frame);
	av_free(dec->codec_ctx);
	free(dec);
}

static void decode_destroy_aac(struct decode* dec)
{

	av_free(dec->frame);
	av_free(dec->codec_ctx);
	free(dec);
}

static struct framebuffer* decode_frame_jpeg(struct decode* dec, const unsigned char *frame, signed long length, void *out)
{
	AVPacket avpkt;
	signed long decoded_len;
	int got_pic = 0;
	avpkt.pts = AV_NOPTS_VALUE;
	avpkt.dts = AV_NOPTS_VALUE;
#pragma GCC diagnostic push  // require GCC 4.6
#pragma GCC diagnostic ignored "-Wcast-qual"
	// AVPacket->data is of uint8_t* data type. type-casting "const" pointer frame to non-const throws a
	// "cast discards ‘__attribute__((const))’ qualifier" warning. However, since this is being done on purpose,
	// we are ignoring the warning for this line.
    avpkt.data = (uint8_t*)frame;
#pragma GCC diagnostic pop
	avpkt.size = length;
	avpkt.duration = 0;

	decoded_len = avcodec_decode_video2(dec->codec_ctx, dec->frame, &got_pic, &avpkt);

	if (decoded_len < 0) {
		printf("avcodec_decode_video failed: res: %ld, gotPicture: %d\n",
				decoded_len, got_pic);
		return NULL;
	}
	if (!got_pic && decoded_len != length) {
		printf("decoded less data: given %ld, decoded %ld\n",
				length, decoded_len);
	}
	if (got_pic) {
		if (out) {
			avcodec_copy_out(dec->codec_ctx, dec->frame, NULL, out);
		} else {
			struct framebuffer* frm = malloc(sizeof(struct framebuffer));
			avcodec_copy_out(dec->codec_ctx, dec->frame, frm, NULL);
			return frm;
		}
	}

	return NULL;

}

static struct framebuffer* decode_frame_aac(struct decode* dec, const unsigned char *frame, signed long length)
{
        AVPacket avpkt;
	signed long decoded_len;
	int got_frame = 0;
	avpkt.pts = AV_NOPTS_VALUE;
	avpkt.dts = AV_NOPTS_VALUE;
#pragma GCC diagnostic push  // require GCC 4.6
#pragma GCC diagnostic ignored "-Wcast-qual"
	// AVPacket->data is of uint8_t* data type. type-casting "const" pointer frame to non-const throws a
	// "cast discards ‘__attribute__((const))’ qualifier" warning. However, since this is being done on purpose,
	// we are ignoring the warning for this line.
    avpkt.data = (uint8_t*)frame;
#pragma GCC diagnostic pop
	avpkt.size = length;
	avpkt.duration = 0;

	decoded_len = avcodec_decode_audio4(dec->codec_ctx, dec->frame, &got_frame, &avpkt);

	if (decoded_len < 0) {
		printf("avcodec_decode_video failed: res: %ld, gotPicture: %d\n",
				decoded_len, got_frame);
		return NULL;
	}
	if (!got_frame && decoded_len != length) {
		printf("decoded less data: given %ld, decoded %ld\n",
				length, decoded_len);
	}
	if (got_frame) {
		struct framebuffer* frm = malloc(sizeof(struct framebuffer));
                
                int data_size = av_samples_get_buffer_size(NULL, dec->codec_ctx->channels,
                                                       dec->frame->nb_samples,
                                                       dec->codec_ctx->sample_fmt, 1);
                frm->size = data_size;
                frm->data = malloc(frm->size);
                memcpy(frm->data, &dec->frame->data[0], data_size);
                //printf("aac decoded: %d\n",data_size);
		return frm;
	}
	return NULL;

}
/* Generic */
struct decode* decode_create(char type)
{
	struct decode* dec = NULL;
	pthread_mutex_lock(&gMutex);
	switch (type) {
	case DEC_AVC:
		dec = decode_create_avc();
		break;
	case DEC_MJPEG:
		dec = decode_create_jpeg();
		break;
	case DEC_AAC:
		dec = decode_create_aac();
		break;
	default:
		printf("decode_create error: bad decode type\n");
		break;
	}
	pthread_mutex_unlock(&gMutex);
	return dec;
}

void decode_destroy(struct decode* dec)
{
	switch (dec->type) {
	case DEC_AVC:
		decode_destroy_avc(dec);
		break;
	case DEC_MJPEG:
		decode_destroy_jpeg(dec);
		break;
        case DEC_AAC:
		decode_destroy_aac(dec);
		break;
	default:
		printf("decode_destroy error: bad decode type\n");
		break;
	}
}

struct framebuffer* decode_frame(struct decode* dec, const unsigned char *frame,
		signed long length, void *out, int *width, int* height)
{
    struct framebuffer* ret = NULL;
	switch (dec->type) {
	case DEC_AVC:
	    ret = decode_frame_avc(dec, frame, length, out);
	    if (width != NULL)
	    {
	        *width = dec->frame->width;
	        *height = dec->frame->height;
	    }
	    return ret;
	case DEC_MJPEG:
	    ret = decode_frame_jpeg(dec, frame, length, out);
	    if (width != NULL)
        {
            *width = dec->frame->width;
            *height = dec->frame->height;
        }
	    return ret;
        case DEC_AAC:
		return decode_frame_aac(dec, frame, length);
	default:
		printf("decode_frame error: bad decode type\n");
		return NULL;
	}
}
#endif//NOX define ends
