/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef __LIST_H__
#define __LIST_H__

typedef struct mx_list mx_list_t;

mx_list_t* list_create();
void list_destroy(mx_list_t *l);
int list_add(mx_list_t *l, void *data);
int list_getnext(mx_list_t *l, void **data);
int list_reset(mx_list_t *l);

#endif
