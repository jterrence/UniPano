/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#include "hash.h"

struct item {
	char *key;
	void *val;
	struct item *next;
};

uint8_t pearson(const char *key)
{
	static const uint8_t table[256] = 
		{49, 118,  63, 252,  13, 155, 114, 130, 137,  40, 210,  62, 219, 246, 136, 221,
		174, 106,  37, 227, 166,  25, 139,  19, 204, 212,  64, 176,  70,  11, 170,  58,
		146,  24, 123,  77, 184, 248, 108, 251,  43, 171,  12, 141, 126,  41,  95, 142,
		167,  46, 178, 235,  30,  75,  45, 208, 110, 230, 226,  50,  32, 112, 156, 180,
		205,  68, 202, 203,  82,   7, 247, 217, 223,  71, 116,  76,   6,  31, 194, 183,
		15, 102,  97, 215, 234, 240,  53, 119,  52,  47, 179,  99, 199,   8, 101,  35,
		65, 132, 154, 239, 148,  51, 216,  74,  93, 192,  42,  86, 165, 113,  89,  48,
		100, 195,  29, 211, 169,  38,  57, 214, 127, 117,  59,  39, 209,  88,   1, 134,
		92, 163,   0,  66, 237,  22, 164, 200,  85,   9, 190, 129, 111, 172, 231,  14,
		181, 206, 128,  23, 187,  73, 149, 193, 241, 236, 197, 159,  55, 125, 196,  60,
		161, 238, 245,  94,  87, 157, 122, 158, 115, 207,  17,  20, 145, 232, 107,  16,
		21, 185,  33, 225, 175, 253,  81, 182,  67, 243,  69, 220, 153,   5, 143,   3,
		26, 213, 147, 222, 105, 188, 229, 191,  72, 177, 250, 135, 152, 121, 218,  44,
		120, 140, 138,  28,  84, 186, 198, 131,  54,   2,  56,  78, 173, 151,  83,  27,
		255, 144, 249, 189, 104,   4, 168,  98, 162, 150, 254, 242, 109,  34, 133, 224,
		228,  79, 103, 201, 160,  90,  18,  61,  10, 233,  91,  80, 124,  96, 244,  36};
	uint8_t hash=0;

	while(*key != '\0') {
		hash = table[hash^(*key)];
		++key;
	}

	return hash;
}

hash_table_t* hash_create()
{
	return (hash_table_t*) calloc(256, sizeof(struct item));
}

void hash_destroy(hash_table_t *h)
{
	if(h != NULL)
		free(h);
}

int hash_set(hash_table_t *hash_table, const char *key, void *val)
{
	if(hash_table == NULL) {
		printf("The hash table is NULL\n");
		return -1;
	}

	uint8_t idx = pearson(key);
	printf("SET: [%-3i] %-25s -> %p\n", idx, key, val);
	
	if(hash_table[idx] == NULL) {
		hash_table[idx] = malloc(sizeof(struct item));
		char *keycp = malloc(strlen(key)+1);

		if(hash_table[idx] == NULL || keycp == NULL) {
			printf("Out of memory\n");
			return -1;
		}
		strncpy(keycp, key, strlen(key)+1);
		hash_table[idx]->key = keycp;
		hash_table[idx]->val = val;
		hash_table[idx]->next = NULL;

		return 0;
	}
	
	struct item *it = hash_table[idx];

	while(it) {
		if(strcmp(it->key, key) == 0) {
			it->val = val;
			return 1;
		}
		if(it->next == NULL)
			break;
		else
			it = it->next;
	}

	printf("Collision!\n");
	it->next = malloc(sizeof(struct item));
	char *keycp = malloc(strlen(key)+1);

	if(it->next == NULL || keycp == NULL) {
		printf("Out of memory\n");
		return -1;
	}
	strncpy(keycp, key, strlen(key)+1);
	it->next->key = keycp;
	it->next->val = val;
	it->next->next = NULL;

	return 0;
}

int hash_get(hash_table_t *hash_table, const char *key, void **val)
{
	if(hash_table == NULL) {
		printf("The hash table is NULL\n");
		return -1;
	}

	uint8_t idx = pearson(key);

	//printf("idx get = %i (%s)\n", idx, key);
	if(hash_table[idx] == NULL)
		return -1;

	struct item *it = hash_table[idx];

	while(it) {
		if(strcmp(it->key, key) == 0) {
			*val = it->val;
			//printf("idx get: idx %i, key %s, value %p\n",
			//		idx, key, *val);
			return 1;
		}
		it = it->next;
	}

	return -1;
}

#if 0
int main()
{
	hash_table_t *h = hash_create();

	hash_set(h,"BenoitFontaine", 32);
	hash_set(h,"Benoit Fontaine", 29);
	hash_set(h,"Benoit", 36);
	hash_set(h,"bfontaine", 52);
	hash_set(h,"benoute", 7);
	hash_set(h,"eoitFontaine", 32);
	hash_set(h,"eoit Fontaine", 29);
	hash_set(h,"eoit", 36);
	hash_set(h,"fntaine", 52);
	hash_set(h,"eoute", 7);
	hash_set(h,"eXotFontaine", 32);
	hash_set(h,"eXot Fontaine", 29);
	hash_set(h,"eXot", 36);
	hash_set(h,"fXnaine", 52);
	hash_set(h,"eXote", 7);
	hash_set(h,"Burnoit", 36);
	hash_set(h,"burontaine", 52);
	hash_set(h,"burnoute", 7);
	hash_set(h,"euritFontaine", 32);
	hash_set(h,"eurit Fontaine", 29);
	hash_set(h,"eurit", 36);
	hash_set(h,"furtaine", 52);

	int val;

	val = -1;
	hash_get(h, "BenoitFontaine", &val);
	printf("BenoitFontaine, val = %p\n", val);
	
	val = -1;
	hash_get(h, "Benoit", &val);
	printf("Benoit, val = %p\n", val);
	
	val = -1;
	hash_get(h, "bfontaine", &val);
	printf("bfontaine, val = %p\n", val);

	return 0;
}
#endif
