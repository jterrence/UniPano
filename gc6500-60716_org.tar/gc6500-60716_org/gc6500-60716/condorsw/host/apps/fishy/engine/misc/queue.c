/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/


#include <stdio.h>
#include <stdlib.h>

#include "queue.h"

struct node {
	void *data;
	struct node *next;
};

struct queue {
	struct node *first;
	struct node *last;
};

queue_t* queue_create()
{
	queue_t *q;
	q = (queue_t *) calloc(1, sizeof(struct queue));
	if(q) {
	q->first = NULL;
	q->last = NULL;
	return q;
	}

	return NULL;
}

void queue_destroy(queue_t *q)
{
	if(q != NULL)
		free(q);
}

int queue_enqueue(queue_t *q, void *val)
{
	if(q == NULL)
		return 1;

	struct node *n = (struct node *)malloc(sizeof(struct node));
	if(n == NULL)
		return 2;


	n->data = val;
	n->next = NULL;

	if(q->first == NULL) {
		q->first = n;
		q->last = q->first;
	} else {
		q->last->next = n;
		q->last = n;
	}
	//printf("ENQUEUE: node : %X\n", (unsigned int *)n->data);
	return 0;
}

int queue_dequeue(queue_t *q, void **val)
{
	if(q == NULL)
		return 1;

	if(q->first == NULL) {
		//printf("Queue empty\n");
		return 2;
	}

	struct node *n = q->first;
        //printf("DEQUEUE: node : %X\n", (unsigned int *) n->data);
	*val = n->data;
	q->first = n->next;
	free(n);

	return 0;
}

int check_queue_empty(queue_t *q)
{
	if(q == NULL)
		return 1;
	
	if(q->first == NULL) {
		return 2;
	}
	return 0;
}

int get_queue_length(queue_t *q) {
    if (check_queue_empty(q) != 0) {
        return 0;
    }

    int length = 1;
    struct node *n = q->first;
    while (n->next != NULL) {
        length ++;
        n = n->next;
    }
    return length;
}

#if 0
int main() {
	queue_t *q1 = queue_create();

	queue_enqueue(q1, 5);
	queue_enqueue(q1, 30);

	int val;
	queue_dequeue(q1, &val);
	printf("val = %p\n", val);
	queue_dequeue(q1, &val);
	printf("val = %p\n", val);

	return 0;
}
#endif
