/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QLayout>
#include <QProcess>
#include "abstractlayout.h"
#include "generalsettings.h"
#include "displaychannel.h"
#include "audiorenderer.h"
#include "dewarpsettings.h"
#include "camerasettings.h"
#include "audiorecord.h"
#include "toastmessage.h"
#include "engineapimanager.h"
#include "channelbar.h"
#include "scrollablewindow.h"
#include "channellayout.h"
#include "videorecord.h"

const int MAX_CAMERA_CHANNELS = 8;

namespace Ui {
class MainWindow;
}

class DisplayChannel;
typedef enum{
    RAPTOR = 0,
    CONDOR
}CAMERA_TYPE;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void init(int,bootProperties);

    void setF1KeyPressed();

    inline bool isToolbarVisible() {
        return generalSettings->isVisible();
    }
    inline int getToolbarHeight() {
        return generalSettings->height();
    }

protected:
    void resizeEvent(QResizeEvent *);
    void closeEvent(QCloseEvent *);

private:
    void keyPressEvent(QKeyEvent *k);
    void wheelEvent(QWheelEvent *event);
    void mousePressEvent(QMouseEvent * event);
    void setupBottomBar();
    void layoutSetup(int numChannels,int startingChannel);
    void setChannelLayout();
    void setChannelResolution();
    void showGeneralSettings(bool show);
    bool getRenderStats(){return statsEnabled;}
    void popoutToastMessage(QString);
    void cleanupLayout();
    void showPagingBar(bool show);
    void calculateNumPages();
    int numChannelsOnPage(int page);
    void layoutSetupEptz();
    void layoutSetupPrivacyMask();

private:
    Ui::MainWindow *ui;
    GeneralSettings *generalSettings;
    CameraSettings *cameraSettings;
    AudioRecord *audioRecord;
    DewarpSettings *dewarpSettings;
    VideoRecord *videoRecord;

    AudioRenderer *renderAudio;

    bool statsEnabled;

    bootProperties bootProp;

    int currentChLayout;

    ToastMessage *toastMessage;

    EngineApiManager *apiManager;

    // paging
    ChannelBar *pagingBar;
    bool singleChannelLayoutSelected; // Whether 1 channel or multiple channels per page
    int numPages;
    int numChannelsPerPage;
    int numChannelsOnLastPage;

    // Raw channel
    DisplayChannel *rawChannel;

    QLabel *labelMode;

    ChannelLayout *channelLayout;

private slots:

    // ui events
    void onExitClicked();
    void onDewarpClicked();
    void onEptzClicked();
    void onCameraSettingsClicked();
    void onMicClicked();
    void onRawClicked();
    void onRawWindowClosed();
    void onScalingEnabled(bool);
    void onPageChanged(int page);
    void onVideoSettingsShown(int channel, bool shown);
    void onSingleChannelLayout();
    void onMultiChannelLayout();
    void onCameraSettingsDialogDestroyed();
    void onAudioRecordHidden();
    void onDewarpDialogDestroyed();
    void onPagingBarResized();
    void onPrivacyClicked();

    // EngineAPIManager events
    void onEngineInitSuccess(int camType);
    void onEngineInitFailed();
    void onCameraConfigGetSuccess();
    void onCameraConfigGetFailed(int);
    void onVideoRecordClicked();

    void onChildDeleted();
};

#endif // MAINWINDOW_H
