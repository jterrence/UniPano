/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef DISPLAYCHANNEL_H
#define DISPLAYCHANNEL_H
#include <QThread>
#include <QWidget>
#include <QMouseEvent>
#include <QTimer>
#ifdef QT5
#include <QGraphicsItem>
#else
#include <QtGui/QGraphicsItem>
#endif
#include <QGraphicsScene>
#include <QLabel>
#include <QCheckBox>
#include "videorendersurface.h"
#include "videoitem.h"
#include "videosettings.h"
#include "engineapimanager.h"
#include <QTimer>
#include <QMutex>
#include "overlay.h"
#include "qcustomplot.h"

namespace Ui {
    class DisplayChannel;
}

//Mapped same as defined in mxuvc.h
const QString ChannelType[] = {
    "H264",
    "H264 TS",
    "MJpeg",
    "YUY2",
    "NV12",
    "Grey Raw",
    "AAC TS",
    "MUX"
};

typedef enum { MAGNIFY=110, STRETCH, SWEEP}DEWARP_MODE;
const QString DEWARP_MAP_RELATIVE_FILE_PATH = "/tmp/";

class OverlayTask;

class EngineReceiver : public QObject
{
    Q_OBJECT

public slots:
    inline void emitFrameReceived() {
        emit signalFrameReceived();
    }

    inline void emitStatsReceived(Stats *val) {
        emit signalStatsReceived(val);
    }

signals:
    void signalFrameReceived();
    void signalStatsReceived(Stats *val);
};

class DisplayChannel : public QWidget
{
    Q_OBJECT

public:
    explicit DisplayChannel(QWidget *parent = 0);
    ~DisplayChannel();

    void cleanup();

    inline int getChannel() { return channel; }
    void setChannel(int);

    inline int getChannelWidth() { return channelWidth; }
    inline void setChannelWidth(int w) { channelWidth = w; }
    inline int getChannelHeight() { return channelHeight; }
    inline void setChannelHeight(int h) { channelHeight= h; }
    inline void setFps(int val){ fps = val;}
    inline void setBps(int val){ bps = val;}
    void setAutoStartVideo(bool autoStartVideo);
    void setStopVideoOnDestroy(bool stop);

    inline void setTimeBitrateMinMax(int time) {
        this->timeBitrateMinMax = time;
    }

    bool isVideoEnabled() {
        return this->videoEnabled;
    }

    void setStatsEnabled(bool val);
    void setDrawTitle(bool drawTitle);

    void setHandleMouseEvents(bool val){handleMouseEvents = val;}

    void startVideo();
    void stopVideo();

    void setDewarpMapEnable(bool val);

    void setMapPoints(QList<QPoint>);

    static void renderCallback(void *);
    static void updateStats(void *,Stats*);
#ifdef WITH_MOTION_RECTS
    static void updateMotion(void *, Motion*);
#endif

    void setToolbarVisible(bool, bool emitSignal=true);
    void highlightPanel(int panel);

    // Handle Key press f1
    void setF1KeyPressed();

    bool isVideoRunning() {
        return (videoItem != NULL && videoItem->isVisible());
    }

    void setOverlayEnabled(bool enabled);
    void setOverlayFontSize(int fontSize);

    void setRender(bool render);
    void setOptimize(bool optimize);

    void keyPressEvent(QKeyEvent *);

    void initMasks(int num);
    void drawMask(QRect rect, int index);
    void removeMask(int index);

    QSize getActualVideoSize() {
        return QSize(videoItem->getviewWidth(), videoItem->getviewHeight());
    }

    VideoItem *videoItem;

protected:
    bool event(QEvent *event);
    void wheelEvent(QWheelEvent *event);
    void resizeEvent(QResizeEvent *event);

private:
    void handleDoubleClick();

    void stopRendering(bool destroying=false);

    void resizeVideoItem();

    void setBasicChannelDetails();

    // Channel settings
    void getChannelInfo();
    bool getCompositorParams(int channel, int panel, panel_mode_t *mode, panel_params_t *params);
    bool setCompositorParams(int channel, int panel, panel_mode_t mode, panel_params_t params);

    OverlayTask *task;

    bool stopVideoOnDestroy;

    QTimer setTransparentFrameTimer;
    QTimer popupTimer;
    int numFramesReceived;
    int timeBitrateMinMax;

public slots:
    void resizeScrollableParentWindow();
    void positionToolbar();
    void presentImage();
    void renderStats(Stats *val);
    void plotGraph();
    inline void emitSignalPlotGraph() {
        emit signalPlotGraph();
    }

private slots:
    // Resolution changed
    void onResolutionChanged(int width, int height);

    // Popup window has been destroyed
    void onPopoutDestroyed();
    void onPopoutShown();
    void openPopup();

    // Handle a single mouse click event
    void handleSingleClick();

    //Video Settings functions
    void updateFrameRate(int);
    void updateBitRate(int);
    void updateGop(int);
    void updateProfile(int);

    void updateLevel(int);
    void updateQuality(int);
    void onVideoEnabled(bool enabled);
    void onRenderToggled(bool);
    void onBufferFullnessToggled(bool);
    void onBufferDurationChanged(int);
    void requestIFrame();
    void enableSmoothScaling(bool);
    void updateFormat(int);

    void setEngineOptimization();

    void onScrollWindowResized(int, int);

    void onPanelModeChanged(int panel, int mode);
    void onPanelChanged(int panel);
    void highlightCurrentPanel(bool);

    // engine api slots
    void onEngineStartVideoSuccess(video_channel_t channel);
    void onEngineStartVideoFailed(video_channel_t channel);
    void onEngineGetChannelInfoSuccess(video_channel_t channel, video_channel_info_t info);
    void onEngineGetChannelInfoFailed(video_channel_t channel, int error);
    void onEngineSetResolutionSuccess(int width, int height);
    void onEngineResolutionNotSupported();
    void onEngineSetResolutionFailed();
    void onEngineSetVideoFormatSuccess();
    void onEngineSetVideoFormatFailed();

    // Overlay
    void onOverlayImageChanged(QString, int, int, int, int, int, QString);
    void onOverlayImageToggled(bool);
    void onOverlayTextChanged(int,QString, int, int);
    void onOverlayTextToggled(bool,int);
    void onOverlayTimeToggled(bool);
    void onOverlayFontChanged(QString, int);
    void onOverlayImageAlphaChanged(int,int);
    void onAddOverlayImageFailed(int);
    void onAddOverlayTextFailed(int, int);
    void onAddOverlayTimeFailed(int);

    // Overlay Privacy
    void onPrivacyMaskChanged(int index);
    //void onPrivacyMaskMoved(int index);

    void startTransparentVideoItem();

    void deleteBufferGraph();

private:
    Ui::DisplayChannel *ui;

    QCheckBox *cbSmoothScaling;
    bool handleMouseEvents;
    VideoSettings *videoSettings;
    int channel;
    int channelWidth;
    int channelHeight;
    int fps;
    int bps;
    bool isPopOut;
    QTimer *clickTimer;
    QLabel *popoutLabel;

    VideoRenderSurface *renderSurface;

    unsigned char *buf;
    int *decodedWidth;
    int *decodedHeight;
    qreal m_size;
#ifdef WITH_MOTION_RECTS
    Motion *motionRegion;
#endif

    bool mResized; /**< indicates whether this widget was resized */

    int currentPanel;
    panel_mode_t currentPanelMode;
    panel_params_t currentPanelParams;

    bool autoStartVideo;
    bool capture360Dump;

    video_format_t videoFormat;

    bool bStatsEnabled; /**< Boolean indicating whether stats are enabled */

    QScrollArea *scrollableParentWindow; /**< Reference to a displaychannels' parent scrollable window (if it has one) */
    DisplayChannel *popoutChannel; /**< Reference to the displaychannel that has been popped out */

    EngineApiManager *apiManager;

    bool bStartVideoAfterGettingChannelInfo; /**< Boolean indicating whether video needs to be started after reading channel info */

    bool videoEnabled;

    // Overlay
    bool overlayEnabled;

    bool render;

    bool reduceRender;

    bool videoSettingsPositioned;

    QImage *transparentImage;

    bool test;

    EngineReceiver *m_receiver;

    bool optimize;

    QCustomPlot *plot;
    bool bufferFullnessEnabled;
    uint numFramesForBufferFullness;
    int bufferFullnessList[2048];

signals:

    /**
     * @brief Signal emitted to indicate that the user toggled the scaling/smoothing
     *
     * @param bool True if scaling enabled. False otherwise
     */
    void signalScalingEnabled(bool);

    /**
     * @brief Signal emitted to indicate whether the video settings toolbar was shown/hidden
     *
     * @param int The channel id
     * @param bool True if the toolbar was shown. False otherwise
     */
    void signalOnVideoSettingsShown(int, bool);

    void signalOnVideoStarted(video_channel_t channel);

    void signalCleanupDone();

    void signalPlotGraph();
};

#endif // DISPLAYCHANNEL_H
