/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include <QColorDialog>
#include <QEvent>
#include <QFont>

#include "privacymaskcontrols.h"
#include "ui_privacymaskcontrols.h"

#include "overlaymanager.h"
#include "overlay.h"
#include "log.h"
#include "alerthandler.h"
#include <QKeyEvent>

PrivacyMaskControls::PrivacyMaskControls(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PrivacyMaskControls)
{
    ui->setupUi(this);

    QStringList lines;
    for (int i = 0; i < OverlayManager::shared()->getNumPrivMasks(); i++) {
        lines << QString("Privacy Mask %1 (not set)").arg(i+1);
    }

    model = new QStringListModel(lines);
    ui->listView->setModel(model);

    ui->sbXOffset->setMinimum(0);
    ui->sbXOffset->setMaximum(9999);

    ui->sbYOffset->setMinimum(0);
    ui->sbYOffset->setMaximum(9999);

    ui->sbWidth->setMinimum(0);
    ui->sbWidth->setMaximum(9999);

    ui->sbHeight->setMinimum(0);
    ui->sbHeight->setMaximum(9999);

    //setControlValues(0);

    ui->leColorPicker->installEventFilter(this);
    ui->frameColor->installEventFilter(this);
    ui->frameColor->setAutoFillBackground(true);

    ui->lblStatus->setFont(QFont("Arial", 12, QFont::Normal));

    ui->cbHighlightAll->setText(tr("highlight all visible masks"));
    updateHightlightAllCheckBox();

    ui->listView->setSelectionMode(QAbstractItemView::SingleSelection);
    //ui->listView->setCurrentIndex(model->index(0, 0));
    ui->listView->setSelectionRectVisible(true);

    //connect(ui->sbXOffset, SIGNAL(valueChanged(int)), this, SIGNAL(signalXChanged(int)));
    connect(ui->sbXOffset, SIGNAL(editingFinished()), this, SLOT(onXSpinBoxEditingFinished()));
    //connect(ui->sbYOffset, SIGNAL(valueChanged(int)), this, SIGNAL(signalYChanged(int)));
    connect(ui->sbYOffset, SIGNAL(editingFinished()), this, SLOT(onYSpinBoxEditingFinished()));
    connect(ui->sbWidth, SIGNAL(editingFinished()), this, SLOT(onWSpinBoxEditingFinished()));
    connect(ui->sbHeight, SIGNAL(editingFinished()), this, SLOT(onHSpinBoxEditingFinished()));
    connect(ui->buttonApply, SIGNAL(clicked()), this, SLOT(onButtonApplyClicked()));
    connect(ui->buttonRemove, SIGNAL(clicked()), this, SLOT(onButtonRemoveClicked()));
    connect(ui->cbHighlight, SIGNAL(toggled(bool)), this, SLOT(onHighlightChanged(bool)));
    connect(ui->buttonRemoveAll, SIGNAL(clicked()), this, SLOT(onRemoveAllMasks()));
    connect(ui->leColorPicker, SIGNAL(returnPressed()), this, SLOT(showColorPicker()));
    connect(ui->cbHighlightAll, SIGNAL(toggled(bool)), this, SLOT(onHightlightAllToggled(bool)));

    connect(ui->listView, SIGNAL(clicked(QModelIndex)), this, SLOT(onMaskSelected(QModelIndex)));

    this->setFocus();

    ui->cbHighlightAll->hide();
    ui->cbHighlight->hide();
    ui->lblStatus->hide();

    ui->sbXOffset->setEnabled(false);
    ui->sbYOffset->setEnabled(false);
    ui->sbWidth->setEnabled(false);
    ui->sbHeight->setEnabled(false);
    ui->buttonApply->setEnabled(false);
    ui->buttonRemove->setEnabled(false);
    ui->leColorPicker->setEnabled(false);
    ui->frameColor->setEnabled(false);
}

PrivacyMaskControls::~PrivacyMaskControls()
{
    qDebug() << "Destroy";
    delete ui;
    if (model) {
        model->deleteLater();
    }
}

void PrivacyMaskControls::keyPressEvent(QKeyEvent *event) {
    switch(event->key()) {
        case Qt::Key_Left:
            qDebug() << "left in privacy mask controls";
            break;
    }
}

QColor argb_hex_to_qcolor(QString hex) {
    // Hex 0xAARRGGBB to QColor
    QString color_str = hex.remove("0x");
    QString hex_alpha_str = color_str.mid(0, 2);
    QString hex_rrggbb_str = color_str.remove(0, 2);

    bool ok;
    QColor color_to_set = QColor(hex_rrggbb_str.toInt(&ok, 16));
    color_to_set.setAlpha(hex_alpha_str.toInt(&ok, 16));

    return color_to_set;
}

bool PrivacyMaskControls::validateControls() {
    //int xOffset = ui->sbXOffset->text().toInt();
    //int yOffset = ui->sbYOffset->text().toInt();
    int width = ui->sbWidth->text().toInt();
    int height = ui->sbHeight->text().toInt();

    if (width <= 0) {
        AlertHandler::displayError(tr("Invalid width"));
        return false;
    }

    if (height <= 0) {
        AlertHandler::displayError(tr("Invalid height"));
        return false;
    }

    return true;
}

void PrivacyMaskControls::enableHightlight(int index, bool enable) {
    if (ui->listView->currentIndex().row() == index) {
        disconnect(ui->cbHighlight, SIGNAL(toggled(bool)), this, SLOT(onHighlightChanged(bool)));
        ui->cbHighlight->setEnabled(enable);
        connect(ui->cbHighlight, SIGNAL(toggled(bool)), this, SLOT(onHighlightChanged(bool)));
    }
}

void PrivacyMaskControls::updatePosition(int index, int x, int y) {
    if (ui->listView->currentIndex().row() == index) {
        ui->sbXOffset->blockSignals(true);
        ui->sbYOffset->blockSignals(true);
        ui->sbXOffset->setValue(x);
        ui->sbYOffset->setValue(y);
        ui->sbXOffset->blockSignals(false);
        ui->sbYOffset->blockSignals(false);
    }
}

void PrivacyMaskControls::selectIndex(int index) {
    if (ui->listView->currentIndex().row() == index) {
        return;
    }

    ui->listView->blockSignals(true);
    ui->listView->setCurrentIndex(model->index(index, 0));
    setControlValues(index);
    ui->listView->blockSignals(false);
}

void PrivacyMaskControls::update() {
    for (int index = 0; index < OverlayManager::shared()->getNumPrivMasks(); index++) {
        PrivacyMask *mask = OverlayManager::shared()->getPrivacyMask(0, index);
        QString value = model->data(model->index(index,0), Qt::DisplayRole).toString();
        if (mask->enabled) {
            value = value.replace(QString("(not set)"), QString("(set)"));
        } else {
            value = value.replace(QString("(set)"), QString("(not set)"));
        }
        model->setData(model->index(index, 0), value);
    }
}

void PrivacyMaskControls::updateHightlightAllCheckBox() {
    int numHighlighted = 0;
    int numEnabled = 0;
    for (int index = 0; index < OverlayManager::shared()->getNumPrivMasks(); index++) {
        PrivacyMask *mask = OverlayManager::shared()->getPrivacyMask(0, index);
        if (mask->enabled) {
            numEnabled ++;

            QString value = model->data(model->index(index,0), Qt::DisplayRole).toString();
            value = value.replace(QString("(not set)"), QString("(set)"));
            model->setData(model->index(index, 0), value);
        }
        if (mask->enabled && mask->highlight) {
            numHighlighted ++;
        }
    }

    disconnect(ui->cbHighlightAll, SIGNAL(toggled(bool)), this, SLOT(onHightlightAllToggled(bool)));
    ui->cbHighlightAll->setChecked(numEnabled > 0 && (numEnabled == numHighlighted));
    connect(ui->cbHighlightAll, SIGNAL(toggled(bool)), this, SLOT(onHightlightAllToggled(bool)));
}

bool PrivacyMaskControls::eventFilter(QObject *obj, QEvent *event) {
    if ((obj == ui->leColorPicker || obj == ui->frameColor) && event->type() == QEvent::MouseButtonRelease) {
        if (ui->leColorPicker->isEnabled() && ui->frameColor->isEnabled()) {
            showColorPicker();
            event->accept();
            return true;
        }
    }

    return QWidget::eventFilter(obj, event);
}

void PrivacyMaskControls::setControlValues(int index) {
    PrivacyMask *mask = OverlayManager::shared()->getPrivacyMask(0, index);

    ui->sbXOffset->blockSignals(true);
    ui->sbYOffset->blockSignals(true);

    //ui->cbHighlight->setChecked(mask->highlight);
    ui->sbXOffset->setValue(mask->xOffset);
    ui->sbYOffset->setValue(mask->yOffset);
    ui->sbWidth->setValue(mask->width);
    ui->sbHeight->setValue(mask->height);
    ui->leColorPicker->setText(QString::number(mask->color.rgba(), 16).prepend("0x"));

    QPalette pal(palette());
    pal.setColor(QPalette::Background, mask->color);
    ui->frameColor->setPalette(pal);

    //ui->cbHighlight->setEnabled(mask->enabled);

    //ui->lblStatus->setText(mask->enabled ? tr("Mask %1 applied").arg(index+1):tr("Mask %1 not applied").arg(index+1));

    ui->sbXOffset->blockSignals(false);
    ui->sbYOffset->blockSignals(false);
}

// Slots

void PrivacyMaskControls::showColorPicker() {
    QColor qcolor = QColorDialog::getColor(argb_hex_to_qcolor(ui->leColorPicker->text()), 0, tr("Select mask color"), QColorDialog::ShowAlphaChannel);
    if(qcolor.isValid()) {
        ui->leColorPicker->setText(QString::number(qcolor.rgba(), 16).prepend("0x"));

        QPalette pal(palette());
        pal.setColor(QPalette::Background, qcolor);
        ui->frameColor->setPalette(pal);

        PrivacyMask *mask = OverlayManager::shared()->getPrivacyMask(0, ui->listView->currentIndex().row());
        if (mask->enabled && validateControls()) {
            //onButtonApplyClicked();
            //emit signalColorUpdated(0, ui->listView->currentIndex().row(), qcolor);
        }
    }
}

void PrivacyMaskControls::onXSpinBoxEditingFinished() {
    if (ui->sbXOffset->hasFocus()) {
        onButtonApplyClicked();
    }
}
void PrivacyMaskControls::onYSpinBoxEditingFinished() {
    if (ui->sbYOffset->hasFocus()) {
        onButtonApplyClicked();
    }
}
void PrivacyMaskControls::onWSpinBoxEditingFinished() {
    if (ui->sbWidth->hasFocus()) {
        onButtonApplyClicked();
    }
}
void PrivacyMaskControls::onHSpinBoxEditingFinished() {
    if (ui->sbHeight->hasFocus()) {
        onButtonApplyClicked();
    }
}

void PrivacyMaskControls::onButtonApplyClicked() {
    if (!validateControls()) {
        return;
    }

    int xOffset = ui->sbXOffset->text().toInt();
    int yOffset = ui->sbYOffset->text().toInt();
    int width = ui->sbWidth->text().toInt();
    int height = ui->sbHeight->text().toInt();
    QColor color = argb_hex_to_qcolor(ui->leColorPicker->text());

    emit signalAddPrivacyMask(0, ui->listView->currentIndex().row(), xOffset, yOffset, width, height, color);
    ui->cbHighlight->setEnabled(true);
    updateHightlightAllCheckBox();
    //ui->lblStatus->setText(tr("Mask %1 applied").arg(ui->cbMasks->currentIndex()+1));
}

void PrivacyMaskControls::onButtonRemoveClicked() {
    emit signalRemovePrivacyMask(0, ui->listView->currentIndex().row());
    ui->cbHighlight->setEnabled(false);
    updateHightlightAllCheckBox();
    //ui->lblStatus->setText(tr("Mask %1 removed").arg(ui->cbMasks->currentIndex()+1));
}

void PrivacyMaskControls::onHighlightChanged(bool checked) {
    emit signalHighlightChanged(0, ui->listView->currentIndex().row(), checked);
    updateHightlightAllCheckBox();
}

void PrivacyMaskControls::onRemoveAllMasks() {
    emit signalRemoveAllMasks(0);
    if (ui->listView->currentIndex().row() >= 0) {
        setControlValues(ui->listView->currentIndex().row());
        //updateHightlightAllCheckBox();
    }
}

void PrivacyMaskControls::onHightlightAllToggled(bool checked) {
    for (int index = 0; index < OverlayManager::shared()->getNumPrivMasks(); index++) {
        PrivacyMask *mask = OverlayManager::shared()->getPrivacyMask(0, index);
        if (mask->enabled && mask->highlight != checked) {
            emit signalHighlightChanged(0, index, checked);
        }
    }

    setControlValues(ui->listView->currentIndex().row());
}

void PrivacyMaskControls::onMaskSelected(QModelIndex index) {
    setControlValues(index.row());
    emit signalMaskSelected(0, index.row());

    if (ui->sbXOffset->isEnabled() == false) {
        ui->sbXOffset->setEnabled(true);
        ui->sbYOffset->setEnabled(true);
        ui->sbWidth->setEnabled(true);
        ui->sbHeight->setEnabled(true);
        ui->buttonApply->setEnabled(true);
        ui->buttonRemove->setEnabled(true);
        ui->leColorPicker->setEnabled(true);
        ui->frameColor->setEnabled(true);
    }
}
