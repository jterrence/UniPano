/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include <QPainter>
#include "audiorecord.h"
#include "ui_audiorecord.h"
#ifdef WITH_ENGINE
#include "interface.h"
#endif
#include "alerthandler.h"
#include "log.h"
#include <QPushButton>

AudioRecord::AudioRecord(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AudioRecord)
{
    ui->setupUi(this);

    audioDataReceived = false;
    totalAudioFramesReceived = 0;

    QPalette pal = palette();
    QColor color = pal.light().color();
    color.setAlpha(240);
    pal.setBrush(QPalette::Window, color);
    setPalette(pal);

    setAutoFillBackground(true);
    setSizeGripEnabled(false);

    ui->buttonStartStop->setText("Start");
    ui->lineEditPath->setText("/dev/null");

    ui->volumeSlider->setRange(0, 100);
    ui->volumeSlider->setLabelWidth(23);
    ui->volumeSlider->setPageStep(10);
    ui->volumeSlider->setStep(1);
    connect(ui->volumeSlider, SIGNAL(signalValueChanged(int)), this, SLOT(onVolumeChanged(int)));

    connect(ui->buttonStartStop,SIGNAL(clicked()),this,SLOT(onButtonClicked()));
    connect(ui->cbCaptureType,SIGNAL(currentIndexChanged(int)),this,SLOT(getSamplingRate(int)));

    audioStarted = false;

    //setWindowFlags(Qt::Window|Qt::WindowCloseButtonHint|Qt::CustomizeWindowHint);
    setWindowTitle(tr("Audio Record"));
    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(close()));
    ui->buttonBox->button(QDialogButtonBox::Ok)->setText(tr("Done"));

    apiManager = new EngineApiManager(0, "arecord");
    connect(apiManager, SIGNAL(signalEngineGetMicVolumeSuccess(int)), this, SLOT(onEngineGetMicVolumeSuccess(int)));
    connect(apiManager, SIGNAL(signalEngineGetMicVolumeFailed()), this, SLOT(onEngineGetMicVolumeFailed()));
    connect(apiManager, SIGNAL(signalEngineSetMicVolumeSuccess()), this, SLOT(onEngineSetMicVolumeSuccess()));
    connect(apiManager, SIGNAL(signalEngineSetMicVolumeFailed()), this, SLOT(onEngineSetMicVolumeFailed()));
    connect(apiManager, SIGNAL(signalEngineMuteMicSuccess()), this, SLOT(onEngineMuteMicSuccess()));
    connect(apiManager, SIGNAL(signalEngineMuteMicFailed()), this, SLOT(onEngineMuteMicFailed()));
    connect(apiManager, SIGNAL(signalEngineAGCSuccess()), this, SLOT(onEngineAGCSuccess()));
    connect(apiManager, SIGNAL(signalEngineAGCFailed()), this, SLOT(onEngineAGCFailed()));

    //apiManager->engineGetMicVolume();
}

AudioRecord::~AudioRecord()
{
    qDebug()<<"Destructor";
    if (audioStarted) {
        stopAudioRecord();
    }
    delete ui;
}

void AudioRecord::mousePressEvent(QMouseEvent * event)
{
    event->accept();
}

void AudioRecord::keyPressEvent(QKeyEvent *event) {
    if (event->key() == Qt::Key_Enter) {
        event->accept();
    } else if (event->key() == Qt::Key_Escape) {
        this->close();
    }
}

void AudioRecord::hideEvent(QHideEvent *) {
    emit(signalHidden());
}

void AudioRecord::getSamplingRate(int index)
{
#ifdef WITH_ENGINE
    if (ui->cbCaptureType->itemText(ui->cbCaptureType->currentIndex()).contains("Raw")) {
        index = AUD_CH1;
    } else if (ui->cbCaptureType->itemText(ui->cbCaptureType->currentIndex()).contains("Encoded")) {
        index = AUD_CH2;
    }
    int val =  engine_get_audio_samplingrate(index);
    qDebug()<<"Sampling rate is "<<val<<" for channel "<<index;
    switch(val)
    {
        case 8000:
            ui->cbSamplingRate->setCurrentIndex(0);
            break;
        case 16000:
            ui->cbSamplingRate->setCurrentIndex(1);
            break;
        case 24000:
            ui->cbSamplingRate->setCurrentIndex(2);
            break;
        default:
            break;
    }
#endif
}

void AudioRecord::setCaptureOptions(QList<AUDIO_FORMAT> formats, int defaultOption) {
    disconnect(ui->cbCaptureType,SIGNAL(currentIndexChanged(int)),this,SLOT(getSamplingRate(int)));
    ui->cbCaptureType->clear();
    for (int format = 0; format < formats.size(); format++) {
        if (formats.at(format) == PCM) {
            ui->cbCaptureType->addItem("Raw (pcm)");
        } else if (formats.at(format) == AAC) {
            ui->cbCaptureType->addItem("Encoded (aac/opus)");
        }
    }

    ui->cbCaptureType->setCurrentIndex(defaultOption);
    getSamplingRate(defaultOption);
    connect(ui->cbCaptureType,SIGNAL(currentIndexChanged(int)),this,SLOT(getSamplingRate(int)));
}

void AudioRecord::onButtonClicked() {
    if (ui->buttonStartStop->text().compare("Start") == 0) {
        startAudioRecord();
    } else {
        stopAudioRecord();
    }
}

void AudioRecord::startAudioRecord()
{
    if(this->isVisible() && ui->lineEditPath->text().isEmpty()) {
        AlertHandler::displayError("Please enter file path");
    }
    else
    {
#ifdef WITH_ENGINE
        int sr=0;
        switch(ui->cbSamplingRate->currentIndex())
        {
            case 0:
                sr = 8000;
                break;
            case 1:
                sr = 16000;
                break;
            case 2:
                sr = 24000;
                break;
            default:
                break;
        }

        fileName = ui->lineEditPath->text();

        audioDataReceived = false;

        int ret = 0;
        if (ui->cbCaptureType->itemText(ui->cbCaptureType->currentIndex()).contains("Raw")) {
            captureType = "pcm";
            ret = engine_startaudio(AUD_CH1, sr, (char*)(fileName.toStdString().c_str()));
        } else if (ui->cbCaptureType->itemText(ui->cbCaptureType->currentIndex()).contains("Encoded")) {
            captureType = "aac/opus";
            ret = engine_startaudio(AUD_CH2, sr, (char*)(fileName.toStdString().c_str()));
        }
        if (ret == 0) {
            engine_start_capture_audio();
            deRegisterAudio();
            if (registeredAudioCallback(AudioRecord::audioCB,this,NULL,0) != 0) {
                qDebug() << "Failed to register audio callback";
            }

            onAudioStarted();
        } else {
            if (this->isVisible()) {
                AlertHandler::displayError("Start failed");
            } else {
                ui->labelInfo->setText("Record failed");
            }
        }
#endif
    }
}

void AudioRecord::stopAudioRecord()
{
#ifdef WITH_ENGINE
    int ret = engine_stopaudio();
    if (ret < 0) {
        qDebug() << "Audio stop failed";

        if (this->isVisible()) {
            AlertHandler::displayError("Stop failed");
        }
    } else {
        engine_stop_capture_audio();
        onAudioStopped();
    }
#endif
}

void AudioRecord::audioCB(void *ptr)
{
    AudioRecord *self = (AudioRecord*)ptr;
    self->audioDataReceived = true;
    self->totalAudioFramesReceived++;
}

void AudioRecord::updateDisplay()
{
    if(audioDataReceived)
    {
        int secs = elapsedTime.elapsed() / 1000;
        int mins = (secs / 60) % 60;
        int hours = (secs / 3600);
        secs = secs % 60;
        QString local(QString("%1:%2:%3")
                      .arg(hours, 2, 10, QLatin1Char('0'))
                      .arg(mins, 2, 10, QLatin1Char('0'))
                      .arg(secs, 2, 10, QLatin1Char('0')));
        ui->labelInfo->setText(QString("Recording ")+captureType+": "+local);
        audioDataReceived = false;
        recordTime = local;
    }
    else
    {
        ui->labelInfo->setText("No audio data received");
    }
}

void AudioRecord::onAudioStarted() {
    audioStarted = true;
    totalAudioFramesReceived = 0;

    ui->buttonStartStop->setText("Stop");
    ui->cbCaptureType->setEnabled(false);
    ui->cbSamplingRate->setEnabled(false);
    ui->lineEditPath->setEnabled(false);

    apiManager->engineGetMicVolume();

    elapsedTime.restart();
    connect(&timer, SIGNAL(timeout()), this, SLOT(updateDisplay()));
    timer.start(500); // twice per second
}

void AudioRecord::onAudioStopped() {
    audioStarted = false;
    if (totalAudioFramesReceived > 0) {
        ui->labelInfo->setText(captureType+ " data dumped to " + fileName + "\nRecord duration: " + recordTime);
    } else {
        ui->labelInfo->setText("No "+captureType+" data captured");
    }

    ui->buttonStartStop->setText("Start");
    ui->cbCaptureType->setEnabled(true);
    ui->cbSamplingRate->setEnabled(true);
    ui->lineEditPath->setEnabled(true);

    disconnect(&timer, SIGNAL(timeout()), this, SLOT(updateDisplay()));
    timer.stop();

    apiManager->engineGetMicVolume();
}

void AudioRecord::onVolumeChanged(int volume) {
    apiManager->engineSetMicVolume(volume);
}

// EngineApiManager slots
void AudioRecord::onEngineGetMicVolumeSuccess(int volume) {
    qDebug()<<"Mic volume"<<volume;
    ui->volumeSlider->setValue(volume);
}
void AudioRecord::onEngineGetMicVolumeFailed() {
    qWarning()<<"Failed to get mic volume";
}
void AudioRecord::onEngineSetMicVolumeSuccess() {
    qDebug()<<"Mic volume set successfully";
}
void AudioRecord::onEngineSetMicVolumeFailed() {
    qWarning()<<"Failed to set mic volume";
}
void AudioRecord::onEngineMuteMicSuccess() {
    //qDebug()<<"Mute state toggled";
}
void AudioRecord::onEngineMuteMicFailed() {
    qWarning()<<"Failed to toggle mute state";
}
void AudioRecord::onEngineAGCSuccess() {
    qDebug()<<"AGC toggled";
}
void AudioRecord::onEngineAGCFailed() {
    qWarning()<<"Failed to toggle AGC";
}

void AudioRecord::on_cbMuteLeft_clicked(bool checked) {
    if (checked) {
        if (ui->cbMuteRight->isChecked()) {
            apiManager->engineMuteMic(true);
            ui->cbMuteBoth->setChecked(true);
        }
    } else {
        if (ui->cbMuteBoth->isChecked()) {
            apiManager->engineMuteMic(false);
            ui->cbMuteBoth->setChecked(false);
        }
    }
    qDebug() << "Toggle left mic mute";
    apiManager->engineMuteMicLeft(checked);
}

void AudioRecord::on_cbMuteRight_clicked(bool checked) {
    if (checked) {
        if (ui->cbMuteLeft->isChecked()) {
            apiManager->engineMuteMic(true);
            ui->cbMuteBoth->setChecked(true);
        }
    } else {
        if (ui->cbMuteBoth->isChecked()) {
            apiManager->engineMuteMic(false);
            ui->cbMuteBoth->setChecked(false);
        }
    }
    qDebug() << "Toggle right mic mute";
    apiManager->engineMuteMicRight(checked);
}

void AudioRecord::on_cbMuteBoth_clicked(bool checked) {
    apiManager->engineMuteMicLeft(checked);
    apiManager->engineMuteMicRight(checked);
    ui->cbMuteLeft->setChecked(checked);
    ui->cbMuteRight->setChecked(checked);
    qDebug() << "Toggle mic mute";
    apiManager->engineMuteMic(checked);
}

void AudioRecord::on_cbAGC_toggled(bool checked) {
    apiManager->engineAGC(checked);
}
