/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include <QDebug>
#include <QFile>
#include "audiorenderer.h"
#include "interface.h"
#include "QVariant"
#include "QTime"

AudioRenderer::AudioRenderer()
{
    m_size = 960;//2048;//Putting  buffer size as 10k for now
    m_charBuffer = (char*)malloc(m_size);
    memset(m_charBuffer,0,m_size);
    m_audioOutput = NULL;
    setup();

#ifdef WITH_ENGINE
//    registeredAudioCallback(AudioRenderer::renderAudio,this, m_charBuffer, m_size);
//call start audio
#endif

//    connect(this,SIGNAL(emitRender()),this,SLOT(renderAudioOut()));
//    connect(&m_buffer,SIGNAL(readyRead()),this,SLOT(playOnDevice()));
}

AudioRenderer::~AudioRenderer()
{
    cleanup();
}

void AudioRenderer::startPlayback()
{
//     m_audioOutput->start(&m_buffer);
//Call start audio
#ifdef WITH_ENGINE
//    engine_startaudio();
#endif
}

void AudioRenderer::stopPlayback()
{
//    m_audioOutput->stop();
#ifdef WITH_ENGINE
    engine_stopaudio();
#endif
}

void AudioRenderer::renderAudio(void* context)
{
#if 0
    AudioRenderer *temp = (AudioRenderer*)context;
    qDebug()<<"set";
//    temp->cleanup();
//    temp->setup();
    static int i = 0;
    if(!i)
    {
        temp->m_buffer.open(QIODevice::ReadWrite);
        temp->m_audioOutput->start(&temp->m_buffer);
        i++;
    }
    if(temp->m_buffer.isOpen())
    {
        temp->m_buffer.close();
    }

    temp->m_buffer.write(temp->m_charBuffer, 2048);
//    temp->m_buffer.setData(temp->m_charBuffer,2048);
    temp->m_buffer.seek(0);
//    temp->m_buffer.write(temp->m_charBuffer,temp->m_size);
    temp->m_buffer.open(QIODevice::ReadWrite);
    temp->m_audioOutput->start(&temp->m_buffer);
//    temp->m_audioOutput->stop();
#endif

    AudioRenderer *temp = (AudioRenderer*)context;

    int off = temp->m_buffer.write(temp->m_charBuffer,temp->m_size);

    qDebug()<<"Audio State" << temp->m_audioOutput->state() << off;

    if(temp->m_audioOutput->error() == QAudio::IOError || temp->m_audioOutput->error() == QAudio::UnderrunError)
    {
        qDebug()<<"Hit the error" << temp->m_audioOutput->error();
//        temp->m_audioOutput->stop();
        temp->m_buffer.seek((temp->m_buffer.pos() - temp->m_size) /*+ temp->m_audioOutput->periodSize()*/);
//        temp->m_buffer.buffer().clear();
//        temp->m_buffer.reset();
//        memset(temp->m_charBuffer,0,temp->m_size);
//        temp->m_buffer.write(temp->m_charBuffer,temp->m_size);
//        temp->m_buffer.seek(0);
        temp->m_audioOutput->start(&temp->m_buffer);
    }
#if 0
    qDebug()<<"no Of bytes written"<< off;
    int pos = temp->m_buffer.pos() - /*temp->m_size */ off * 1.3;
    if(pos == 0)
    {
        pos = 0;
        temp->m_buffer.seek(pos);
        qDebug()<<temp->m_buffer.size()<< temp->m_buffer.data().size();
//        temp->m_audioOutput->start(&temp->m_buffer);
    }
    else
        temp->m_buffer.seek(pos);
#endif

}

void AudioRenderer::playOnDevice()
{
    static int re;
    qDebug()<<"ready Read"<<re++;
}

void AudioRenderer::finishedPlaying(QAudio::State state)
{
    qDebug()<<"State"<<state <<"ERROR"<<m_audioOutput->error();
static int toggle;
  if(state == QAudio::IdleState || m_audioOutput->error() == QAudio::UnderrunError) {
//      cleanup();

      if(!toggle)
      {
        qDebug()<<"Suspending0";
        m_audioOutput->suspend();
        qDebug()<<"Suspended0";
        m_buffer.seek(0);
        m_audioOutput->start(&m_backup);

        qDebug()<<"PLaying from m_backup";
        toggle = 1;
      }
      else
      {
          qDebug()<<"Suspending1";
          m_audioOutput->suspend();
          qDebug()<<"Suspended1";
          m_backup.seek(0);
          m_audioOutput->start(&m_buffer);
          qDebug()<<"PLaying from m_buffer";
          toggle = 0;
      }
//    m_buffer.buffer().clear();
//    m_buffer.close();
//    delete m_audioOutput;
  }
  if(m_audioOutput->error() == QAudio::IOError || m_audioOutput->error() == QAudio::UnderrunError)
  {
  //    m_audioOutput->stop();
//      m_buffer.buffer().clear();
  }
  if(m_audioOutput->error() == QAudio::IOError)
      qDebug()<<"Error1" <<m_audioOutput->error();

  if(m_audioOutput->error() == QAudio::OpenError)
      qDebug()<<"Error2" << m_audioOutput->error();

  if(m_audioOutput->error() == QAudio::UnderrunError)
      qDebug()<<"Error3" << m_audioOutput->error();

  if(m_audioOutput->error() == QAudio::FatalError)
      qDebug()<<"Error4" << m_audioOutput->error();
}

void AudioRenderer::renderAudioOut()
{
//    setup();
}
void AudioRenderer::cleanup()
{
    qDebug()<<" Cleaned";
    if(m_audioOutput != NULL)
    {
        if(m_audioOutput->state() == QAudio::ActiveState)
        {
            qDebug()<<" stopping";
            m_audioOutput->stop();
        }
        qDebug()<<" deleting";
        delete m_audioOutput;
        qDebug()<<" NULL";
        m_audioOutput = NULL;
    }
    if(m_buffer.isOpen())
    {
        qDebug()<<"Closing";
        m_buffer.close();
        qDebug()<<"closed";
    }
//    free(m_charBuffer);
}
void AudioRenderer::setup()
{
    qDebug()<<" Created";
    QAudioFormat format;
    // Set up the format, eg.
    format.setFrequency(16000);
    format.setChannels(2);
    format.setSampleSize(16);
    format.setCodec("audio/pcm");
    format.setByteOrder(QAudioFormat::LittleEndian);
    format.setSampleType(QAudioFormat::SignedInt);

    QAudioDeviceInfo info(QAudioDeviceInfo::defaultOutputDevice());
    if (!info.isFormatSupported(format)) {
        qWarning()<<"raw audio format not supported by backend, cannot play audio.";
        return;
    }

    m_audioOutput = new QAudioOutput(format, this);
    connect(m_audioOutput,SIGNAL(stateChanged(QAudio::State)),SLOT(finishedPlaying(QAudio::State)));

    m_buffer.open(QIODevice::ReadWrite);
    m_backup.open(QIODevice::ReadWrite);
//    m_buffer.write(m_charBuffer,m_size);



    inputFile.setFileName("/tmp/baby_16000.wav");
    inputFile.open(QIODevice::ReadOnly);
    qDebug()<<inputFile.size();
//    m_buffer.write(inputFile.read(477400));
//    m_buffer.seek(0);
//    inputFile.seek(477400);
    m_backup.write(inputFile.readAll());
    m_backup.seek(0);
    inputFile.seek(0);
    m_buffer.write(inputFile.readAll());
    m_buffer.seek(0);

    m_audioOutput->start(&m_buffer);
        qDebug()<<m_buffer.size()<< m_buffer.data().size() << m_audioOutput->periodSize();
}
