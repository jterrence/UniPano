/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include <QTime>
#include <QColor>
#include <log.h>

#include "overlaymanager.h"
#include "overlay.h"

OverlayManager* OverlayManager::s_instance = 0;

void OverlayManager::initOverlayImage(int channel) {
    OverlayImage* overlayImage = new OverlayImage();
    overlayImage->image = QString(DEFAULT_OVERLAY_IMAGE).trimmed().length() > 0 ? QCoreApplication::applicationDirPath() + "/" + DEFAULT_OVERLAY_IMAGE : "";
    overlayImage->width = DEFAULT_OVERLAY_IMAGE_WIDTH;
    overlayImage->height = DEFAULT_OVERLAY_IMAGE_HEIGHT;
    overlayImage->xOffset = overlayImage->yOffset = -1;
    overlayImage->alpha = DEFAULT_IMAGE_ALPHA;
    overlayImage->alphaFile = QString(DEFAULT_OVERLAY_IMAGE_ALPHA).trimmed().length() > 0 ? QCoreApplication::applicationDirPath() + "/" + DEFAULT_OVERLAY_IMAGE_ALPHA : "";
    overlayImage->id = -1;

    mapOverlayImage[channel] = overlayImage;
    setOverlayImageEnabled(channel, true);
}

void OverlayManager::initOverlayTime(int channel) {
    QTime time = QTime::currentTime();

    OverlayTime* overlayTime = new OverlayTime();
    overlayTime->hours = time.hour();
    overlayTime->minutes = time.minute();
    overlayTime->seconds = time.second();
    overlayTime->xOffset = overlayTime->yOffset = -1;

    mapOverlayTime[channel] = overlayTime;
    setOverlayTimeEnabled(channel, true);
}

void OverlayManager::init(int numOverlayText, int numPrivMasks) {
    this->numOverlayText = numOverlayText;
    this->numPrivMasks = numPrivMasks;
}

void OverlayManager::initOverlayTexts(int channel) {
    if (mapOverlayText.contains(channel)) {
        return;
    }

    tlist overlayTexts;
    for (int index = 0; index < numOverlayText; index++) {
        OverlayText* object = new OverlayText();
        object->text = DEFAULT_OVERLAY_TEXTS[index];
        object->xOffset = -1;
        object->yOffset = -1;
        object->index = index;
        object->enabled = (index == 0);
        overlayTexts.append(object);
    }

    mapOverlayText[channel] = overlayTexts;
}

void OverlayManager::initPrivacyMasks(int channel) {
    if (mapPrivacyMask.contains(channel)) {
        return;
    }

    plist privacyMasks;
    for (int index = 0; index < numPrivMasks; index++) {
        PrivacyMask* object = new PrivacyMask();
        object->index = index;
        object->color = QColor::fromRgba(DEFAULT_MASK_COLOR);
        object->xOffset = 150 * index + 20 * (index+1);
        object->yOffset = 50;
        object->width = 150;
        object->height = 150 + 50 * (index+1);
        object->ratio = 1;
        object->points = NULL;
        object->enabled = 0;
        object->highlight = 1;
        privacyMasks.append(object);
    }

    mapPrivacyMask[channel] = privacyMasks;
}

int OverlayManager::getNumPrivMasks() {
    return numPrivMasks;
}

void OverlayManager::initOverlayFont(int channel) {
    OverlayFont *font = new OverlayFont();
    font->fontPath = QString(DEFAULT_OVERLAY_FONT_FILE).trimmed().length() > 0 ? QCoreApplication::applicationDirPath() + "/" + DEFAULT_OVERLAY_FONT_FILE : "";
    font->fontSize = DEFAULT_OVERLAY_FONT_SIZE;
    font->loaded = false;
    mapOverlayFont[channel] = font;
}

bool OverlayManager::isOverlayTextEnabled(int channel) {
    if (!mapOverlayTextEnabled.contains(channel)) {
        return false;
    }

    return mapOverlayTextEnabled[channel];
}

void OverlayManager::setOverlayTextEnabled(int channel, bool enabled, int index) {
    if (index == -1) {
        mapOverlayTextEnabled[channel] = enabled;
    } else {
        mapOverlayText[channel][index]->enabled = enabled;
    }
}

bool OverlayManager::isPrivacyMaskEnabled(int channel) {
    if (!mapPrivacyMaskEnabled.contains(channel)) {
        return false;
    }

    return mapPrivacyMaskEnabled[channel];
}

void OverlayManager::setPrivacyMaskEnabled(int channel, bool enabled, int index) {
    if (index == -1) {
        mapPrivacyMaskEnabled[channel] = enabled;
    } else {
        mapPrivacyMask[channel][index]->enabled = enabled;
    }
}

PrivacyMask* OverlayManager::getPrivacyMask(int channel, int index) {
    if (!mapPrivacyMask.contains(channel)) {
        return NULL;
    }
    return mapPrivacyMask[channel][index];
}

void OverlayManager::setOverlayTimeEnabled(int channel, bool enabled) {
    mapOverlayTimeEnabled[channel] = enabled;
}

bool OverlayManager::isOverlayTimeEnabled(int channel) {
    if (!mapOverlayTimeEnabled.contains(channel)) {
        return false;
    }

    return mapOverlayTimeEnabled[channel];
}

bool OverlayManager::isOverlayImageEnabled(int channel) {
    if (!mapOverlayImageEnabled.contains(channel)) {
        return false;
    }

    return mapOverlayImageEnabled[channel];
}

void OverlayManager::setOverlayImageEnabled(int channel, bool enabled) {
    mapOverlayImageEnabled[channel] = enabled;
}

void OverlayManager::setFontLoaded(int channel, bool loaded) {
    if (!mapOverlayFont.contains(channel)) {
        qWarning() << "OverlayFont not initialized";
        return;
    }
    mapOverlayFont[channel]->loaded = loaded;
}

bool OverlayManager::isFontLoaded(int channel) {
    if (!mapOverlayFont.contains(channel)) {
        qWarning() << "OverlayFont not initialized";
        return false;
    }
    return mapOverlayFont[channel]->loaded;
}

void OverlayManager::setOverlayTextSetupDone(int channel, bool done) {
    mapOverlayTextSetupDone[channel] = done;
}

bool OverlayManager::isOverlayTextSetupDone(int channel) {
    return mapOverlayTextSetupDone[channel];
}

int OverlayManager::getNumOverlayText() {
    return numOverlayText;
}

void OverlayManager::calculateDefaultTextOffsets(int channel, int channelWidth, int channelHeight) {
    if (!mapOverlayText.contains(channel)) {
        return;
    }

    for (int index = 0; index < numOverlayText; index++) {
        this->calculateTextOffset(channel, index, channelWidth, channelHeight);
    }
}

void OverlayManager::calculateTextOffset(int channel, int index, int channelWidth, int channelHeight) {
    int fontSize = getOverlayFont(channel)->fontSize;

    mapOverlayText[channel][index]->xOffset = channelWidth-(fontSize*mapOverlayText[channel][index]->text.length())-1;
    mapOverlayText[channel][index]->yOffset = channelHeight-(fontSize*(index+1))-2-fontSize;
    //mapOverlayText[channel][index]->yOffset = channelHeight-(fontSize*(numOverlayText-index))-2-fontSize;
}

OverlayFont* OverlayManager::getOverlayFont(int channel) {
    if (!mapOverlayFont.contains(channel)) {
        qWarning() << "OverlayFont is not initialized!";
        return NULL;
    }
    return mapOverlayFont[channel];
}

OverlayText* OverlayManager::getOverlayText(int channel, int index) {
    if (!mapOverlayText.contains(channel)) {
        return NULL;
    }
    return mapOverlayText[channel][index];
}

void OverlayManager::calculateDefaultImageOffsets(int channel, int channelWidth, int channelHeight) {
    if (!mapOverlayImage.contains(channel)) {
        return;
    }
    OverlayImage* object = mapOverlayImage[channel];
    if (object == NULL) {
        return;
    }

    object->xOffset = channelWidth-object->width-5;
    object->yOffset = 5 + (channelHeight-channelHeight);
}

void OverlayManager::calculateDefaultTimeOffsets(int channel, int channelWidth, int channelHeight) {
    int fontSize = getOverlayFont(channel)->fontSize;
    mapOverlayTime[channel]->xOffset = channelWidth-(fontSize*10);
    mapOverlayTime[channel]->yOffset = channelHeight-fontSize-2;
    //mapOverlayTime[channel]->yOffset = channelHeight-(fontSize*(numOverlayText+1))-5;
}

OverlayTime* OverlayManager::getOverlayTime(int channel, bool updateTime) {
    if (!mapOverlayTime.contains(channel)) {
        return NULL;
    }
    QTime time = QTime::currentTime();
    OverlayTime* overlayTime = mapOverlayTime[channel];
    if (updateTime) {
        overlayTime->hours = time.hour();
        overlayTime->minutes = time.minute();
        overlayTime->seconds = time.second();
    }
    return overlayTime;
}

OverlayImage* OverlayManager::getOverlayImage(int channel) {
    if (!mapOverlayImage.contains(channel)) {
        return NULL;
    }
    return mapOverlayImage[channel];
}

void OverlayManager::cleanup() {
    // cleanup text
    QMap<int, tlist>::iterator i;
    for (i = mapOverlayText.begin(); i != mapOverlayText.end(); i++) {
        tlist texts = (tlist) i.value();
        for (int j = 0; j < texts.count(); j++) {
            delete (OverlayText*) texts.at(j);
        }
    }

    // cleanup image
    QMap<int, OverlayImage*>::iterator itImage;
    for (itImage = mapOverlayImage.begin(); itImage != mapOverlayImage.end(); itImage++) {
        delete (OverlayImage*) itImage.value();
    }

    // cleanup time
    QMap<int, OverlayTime*>::iterator itTime;
    for (itTime = mapOverlayTime.begin(); itTime != mapOverlayTime.end(); itTime++) {
        delete (OverlayTime*) itTime.value();
    }

    // cleanup privacy objects
    QMap<int, plist>::iterator p;
    for (p = mapPrivacyMask.begin(); p != mapPrivacyMask.end(); p++) {
        plist objs = (plist) p.value();
        for (int j = 0; j < objs.count(); j++) {
            delete (PrivacyMask*) objs.at(j);
        }
    }

    mapOverlayText.clear();
    mapOverlayImage.clear();
    mapOverlayTime.clear();
    mapPrivacyMask.clear();
}
