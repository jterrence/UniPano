/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef CUSTOMSLIDER_H
#define CUSTOMSLIDER_H

#include <QWidget>

namespace Ui {
    class CustomSlider;
}

class CustomSlider : public QWidget
{
    Q_OBJECT

public:
    explicit CustomSlider(QWidget *parent = 0);
    ~CustomSlider();

    int value();

    void setRange(int min, int max);
    void setPageStep(int step);
    void setValue(int value, bool emitSignal=false);
    void setLabelValue(QString value);
    void setLabelWidth(int width);
    void removeLabel();
    void setStep(int);

private:
    Ui::CustomSlider *ui;
    int step;

private slots:
    void onValueChanged(int value);
    void onSliderMoved(int);
    void onLineEditReturnPressed();

signals:
    void signalValueChanged(int value);
};

#endif // CUSTOMSLIDER_H
