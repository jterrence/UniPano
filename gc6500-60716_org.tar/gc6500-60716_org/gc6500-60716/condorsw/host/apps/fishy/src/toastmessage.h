/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef TOASTMESSAGE_H
#define TOASTMESSAGE_H

#include <QWidget>
#include <QTimer>

namespace Ui {
    class ToastMessage;
}

class ToastMessage : public QWidget
{
    Q_OBJECT

public:
    explicit ToastMessage(QWidget *parent = 0);
    ~ToastMessage();
    void setText(QString text);
    void getSize(int *w, int*h);

protected:
    void paintEvent(QPaintEvent*);

private:
    Ui::ToastMessage *ui;
    QTimer timer;

private slots:
    void handleTimeout();
};

#endif // TOASTMESSAGE_H
