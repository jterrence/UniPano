/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef EPTZ_H
#define EPTZ_H

#include <QWidget>
#include <QPushButton>
#include "displaychannel.h"

class EPTZ : public QWidget
{
    Q_OBJECT
public:
    explicit EPTZ(QWidget *parent = 0);
    ~EPTZ();

signals:

public slots:

private slots:
    // EngineApiHandler slots
    void onDewarpGetSuccess(video_channel_t channel, int panel, dewarp_mode_t mode, dewarp_params_t params);
    void onDewarpGetFailed(video_channel_t channel, int panel);

    // button slots

    void onVideoStarted(video_channel_t channel);

    void onEPTZ(int mode, int zoom, int hpan, int vpan, int tilt=-1);

protected:
    void showEvent(QShowEvent *);

private:
    DisplayChannel *eptzChannel;
    DisplayChannel *refChannel;
    EngineApiManager *apiManager;
    video_channel_t oldChannel;
    int oldPanel;
    dewarp_mode_t oldMode;
    dewarp_params_t oldParams;

    int zoom, mode, hPan, vPan, tilt;

    void keyPressEvent(QKeyEvent *);

    void performEPTZ();
    //void updateButtonStates();
    void drawDewarpMap();
    void cleanup();
};

#endif // EPTZ_H
