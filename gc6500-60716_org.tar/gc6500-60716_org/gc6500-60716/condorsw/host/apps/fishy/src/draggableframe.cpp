/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include "draggableframe.h"
#include "log.h"
#include <QStyle>

DraggableFrame::DraggableFrame(QWidget *parent, int index) :
    QFrame(parent),
    index(index),
    labelNumber(NULL),
    moved(false)
{
    /*QPalette Pal(palette());

    // set black background
    Pal.setColor(QPalette::Background, Qt::red);
    this->setAutoFillBackground(true);
    this->setPalette(Pal);*/

    this->setFrameShape(QFrame::Box);
    //setStyleSheet("border: 2px solid red;");

    setStyleSheet("QFrame {border: 2px solid red; background: rgba(255, 0, 0, 80);}"
                  "[selected=\"true\"]{border: 2px solid red; background: rgba(255, 0, 0, 0);}"
                  "[selected=\"false\"]{border: 0px; background: rgba(255, 0, 0, 0);}"
                  "[moved=\"true\"]{border: 2px solid red; background: rgba(255, 0, 0, 80);}"
                  "[dirty=\"true\"]{border: 2px solid red; background: rgba(255, 0, 0, 80);}");

    setProperty("selected", true);
    setProperty("moved", false);
    setProperty("dirty", false);
}

DraggableFrame::~DraggableFrame() {
    delete labelNumber;
}

void DraggableFrame::keyPressEvent(QKeyEvent *event) {
    QPoint toPoint = mapToParent( QPoint(this->geometry().x(), this->geometry().y()) );

    switch(event->key()) {
        case Qt::Key_Left:
            toPoint.setX(toPoint.x() - 5);
            break;
    }

    this->mouseMoveEvent(new QMouseEvent(QEvent::MouseMove, toPoint, Qt::LeftButton, Qt::LeftButton, Qt::NoModifier));
}

void DraggableFrame::resizeEvent(QResizeEvent *) {
    if (labelNumber == NULL) {
        labelNumber = new QLabel(this);
        labelNumber->setFont(QFont("Arial", 14, QFont::Bold));
        labelNumber->setAlignment(Qt::AlignCenter);
        labelNumber->setStyleSheet("color: #ffffff; border: 0px; background: rgba(0,0,0,0);");
        labelNumber->setText(tr("%1").arg(index+1));
    }
    labelNumber->setGeometry(this->width()/2 - 15, this->height()/2 - 15, 30, 30);
}

void DraggableFrame::select() {
    if (this->property("moved") == false) {
        labelNumber->setText(tr("%1").arg(index+1));
    }

    setProperty("selected", true);
    this->style()->unpolish(this);
    this->style()->polish(this);
    this->update();
}

void DraggableFrame::deselect() {

    labelNumber->setText(tr(""));

    setProperty("selected", false);
    setProperty("moved", false);
    setProperty("dirty", false);
    this->style()->unpolish(this);
    this->style()->polish(this);
    this->update();
}

void DraggableFrame::dirty() {
    labelNumber->setText(tr("%1*").arg(index+1));
    setProperty("dirty", true);
    this->style()->unpolish(this);
    this->style()->polish(this);
    this->update();
}

void DraggableFrame::moveX(int xDiff) {
    move(QPoint(originalGeometry.x()+xDiff, this->geometry().y()));
}

void DraggableFrame::moveY(int yDiff) {
    move(QPoint(originalGeometry.x(), originalGeometry.y()+yDiff));
}

bool mousePrsd = false;
QPoint mousePos;
void DraggableFrame::mousePressEvent(QMouseEvent *event) {
    if ( event->button() == Qt::LeftButton ) {
        mousePrsd = true;
        mousePos = event->pos();
        select();
        emit signalMoved(index,-1,-1);
    }
}

void DraggableFrame::mouseMoveEvent(QMouseEvent *event) {
    if (mousePrsd) {
        QPoint toPoint = mapToParent( event->pos() - mousePos );
        if (toPoint.x() < rectLimit.x()) {
            toPoint.setX(rectLimit.x());
        }
        if (toPoint.y() < rectLimit.y()) {
            toPoint.setY(rectLimit.y());
        }
        if ((toPoint.x()+this->width()) > (rectLimit.x()+rectLimit.width())) {
            toPoint.setX(rectLimit.x()+rectLimit.width()-this->width());
        }
        if ((toPoint.y()+this->height()) > (rectLimit.y()+rectLimit.height())) {
            toPoint.setY(rectLimit.y()+rectLimit.height()-this->height());
        }
        move( toPoint );
        if (!moved) {
            moved = true;
        }

        //setStyleSheet("border: 2px solid red; background: rgba(255, 0, 0, 80);");
        setProperty("moved", true);
        this->style()->unpolish(this);
        this->style()->polish(this);
        this->update();
    }
}

void DraggableFrame::mouseReleaseEvent(QMouseEvent *event) {
    if ( event->button() == Qt::LeftButton ) {

        if (moved) {
            //qDebug() << "Point " << toPoint.x()-rectLimit.x() << " - " << toPoint.y()-rectLimit.y();
            QPoint toPoint = mapToParent( QPoint(0,0) );
            emit signalMoved(index,toPoint.x()-rectLimit.x(),toPoint.y()-rectLimit.y());
            labelNumber->setText(tr("%1*").arg(index+1));
        }

        mousePrsd = false;
        mousePos = QPoint();
        moved = false;
    }
}
