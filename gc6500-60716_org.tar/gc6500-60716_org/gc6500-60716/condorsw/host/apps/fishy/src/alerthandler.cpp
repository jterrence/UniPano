/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include "alerthandler.h"
#include <QMessageBox>

AlertHandler::AlertHandler(QObject *parent) :
    QObject(parent)
{
}

void AlertHandler::displayError(QString message, QWidget *parent) {
    QMessageBox box(QMessageBox::Warning, tr("Error"),
                    message,
                    QMessageBox::NoButton,
                    parent);
    box.exec();
}

void AlertHandler::displayMessage(QString message, QString title) {
    QMessageBox box(QMessageBox::NoIcon, title,
                    message,
                    QMessageBox::NoButton);
    box.exec();
}

void AlertHandler::displayInfo(QString message) {
    QMessageBox box(QMessageBox::Information, "",
                    message,
                    QMessageBox::NoButton,
                    0, Qt::FramelessWindowHint);
    box.exec();
}
