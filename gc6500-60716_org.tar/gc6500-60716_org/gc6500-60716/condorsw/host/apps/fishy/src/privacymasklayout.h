/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef PRIVACYMASKLAYOUT_H
#define PRIVACYMASKLAYOUT_H

#include <QWidget>
#include <QColor>

#include "displaychannel.h"

class OverlayTask;
class PrivacyMaskControls;
class ProgressBar;
class DraggableFrame;

class PrivacyMaskLayout : public QWidget
{
    Q_OBJECT
public:
    explicit PrivacyMaskLayout(QWidget *parent = 0, int channelWithZclStretch = 0);
    ~PrivacyMaskLayout();

    void keyPressEvent(QKeyEvent *);

protected:
    //bool event(QEvent *event);

signals:

public slots:

private:
    DisplayChannel *displayChannel;
    OverlayTask *task;
    PrivacyMaskControls *maskControls;
    ProgressBar *bar;
    QList<DraggableFrame*> maskList;

    void selectDraggableMask(int channel, int index);
    void createDraggableMask(int channel, int index);

private slots:
    void onVideoStarted(video_channel_t);
    void onAddPrivacyMask(int ch, int index, int x, int y, int w, int h, QColor c);
    void onRemovePrivacyMask(int ch, int index);
    void onMaskMoved(int index, int x, int y);
    void onHighlightChanged(int ch, int index, bool checked);
    void onRemoveAllMasks(int ch);
    void onColorUpdated(int ch, int index, QColor color);
    void onMaskSelected(int ch, int index);
    void onXChanged(int);
    void onYChanged(int);
    void onTimeOut();

    // OverlayTask slots
    void onAddMaskSuccess(int ch, int index);
    void onAddMaskFailed(int ch, int index);
};

#endif // PRIVACYMASKLAYOUT_H
