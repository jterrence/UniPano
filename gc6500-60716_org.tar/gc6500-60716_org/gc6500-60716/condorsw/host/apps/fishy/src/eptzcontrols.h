/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef EPTZCONTROLS_H
#define EPTZCONTROLS_H

#include <QWidget>

namespace Ui {
    class EptzControls;
}

class EptzControls : public QWidget
{
    Q_OBJECT

public:
    explicit EptzControls(QWidget *parent = 0);
    ~EptzControls();

    void keyPressEvent(QKeyEvent *);

private slots:
    void onModeChanged(QString);
    void onZoomChanged(int);
    void onPanChanged(int);

signals:
    void signalZoom(int zoom);
    void signalHPan(int pan);
    void signalVPan(int pan);
    void signalOnEPTZ(int mode, int zoom, int hpan, int vpan, int tilt);

private:
    Ui::EptzControls *ui;

    void disconnectSliders();
    void connectSliders();
};

#endif // EPTZCONTROLS_H
