/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include "channelbar.h"
#include "ui_channelbar.h"
#include <QPainter>
#include "log.h"

ChannelBar::ChannelBar(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ChannelBar)
{
    ui->setupUi(this);
    ui->buttonGroupChannels->setId(ui->buttonChannel1,0);
    ui->buttonGroupChannels->setId(ui->buttonChannel2,1);
    ui->buttonGroupChannels->setId(ui->buttonChannel3,2);
    ui->buttonGroupChannels->setId(ui->buttonChannel4,3);
    ui->buttonGroupChannels->setId(ui->buttonChannel5,4);
    ui->buttonGroupChannels->setId(ui->buttonChannel6,5);
    ui->buttonGroupChannels->setId(ui->buttonChannel7,6);
    ui->buttonGroupChannels->setId(ui->buttonChannel8,7);
    ui->buttonGroupChannels->setId(ui->buttonChannel9,8);
    ui->buttonGroupChannels->setId(ui->buttonChannel10,9);
    ui->buttonGroupChannels->setId(ui->buttonChannel11,10);
    ui->buttonGroupChannels->setId(ui->buttonChannel12,11);
    ui->buttonGroupChannels->setId(ui->buttonChannel13,12);
    ui->buttonGroupChannels->setId(ui->buttonChannel14,13);
    ui->buttonGroupChannels->setId(ui->buttonChannel15,14);
    ui->buttonGroupChannels->setId(ui->buttonChannel16,15);
    for(int i=0;i<16;i++) {
        ui->buttonGroupChannels->button(i)->setVisible(false);
    }

    buttonIndex = 0;
    numButtons = 0;
    updateButtonStates();
    connect(ui->buttonGroupChannels,SIGNAL(buttonClicked(int)),this,SLOT(onPageClicked(int)));
    connect(ui->buttonPrev,SIGNAL(clicked()),this,SLOT(onPagePreviousClicked()));
    connect(ui->buttonNext,SIGNAL(clicked()),this,SLOT(onPageNextClicked()));
}

ChannelBar::~ChannelBar()
{
    delete ui;
}

void ChannelBar::resizeEvent(QResizeEvent *) {
    emit(signalResized());
}

void ChannelBar::setNumButtons(int val)
{
    numButtons = val;
    for(int i=0;i<ui->buttonGroupChannels->buttons().size();i++) {
        ui->buttonGroupChannels->button(i)->setVisible(i < numButtons);
    }

    adjustSize();
    // reset button index
    this->buttonIndex = 0;
    updateButtonStates();
}

// Private functions
void ChannelBar::updateButtonStates() {
    ui->buttonPrev->setEnabled(this->buttonIndex > 0);
    ui->buttonNext->setEnabled(this->buttonIndex < (this->numButtons-1));

    for (int index = 0; index < numButtons; index++) {
        ui->buttonGroupChannels->button(this->buttonIndex)->setChecked(index == buttonIndex);
    }
}

// End private functions

// Private Slots
void ChannelBar::onPageClicked(int index) {
    this->buttonIndex = index;
    emit(signalPageChanged(buttonIndex));
    updateButtonStates();
}

void ChannelBar::onPagePreviousClicked() {
    this->buttonIndex -= 1;
    emit(signalPageChanged(buttonIndex));

    updateButtonStates();
}

void ChannelBar::onPageNextClicked() {
    this->buttonIndex += 1;
    emit(signalPageChanged(buttonIndex));
    updateButtonStates();
}
// End Private Slots
