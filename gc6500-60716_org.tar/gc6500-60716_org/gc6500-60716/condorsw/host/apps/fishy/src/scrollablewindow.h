/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef SCROLLABLEWIDGET_H
#define SCROLLABLEWIDGET_H

#include <QScrollArea>
#include <QCheckBox>
#include <QKeyEvent>
#include "displaychannel.h"

namespace Ui {
    class ScrollableWindow;
}

class ScrollableWindow : public QScrollArea
{
    Q_OBJECT
public:
    explicit ScrollableWindow(QWidget *parent = 0);
    ~ScrollableWindow();

    void setMainWindow(QWidget *mainWindow) {
        this->mainWindow = mainWindow;
    }

protected:
    void keyPressEvent(QKeyEvent *event );
    void resizeEvent(QResizeEvent *);

signals:
    void signalResized();

public slots:

private slots:
    void onCheckBoxToggled(bool checked);

private:
    QCheckBox *cbKeepOnTop;
    QWidget *mainWindow;
};

#endif // SCROLLABLEWIDGET_H
