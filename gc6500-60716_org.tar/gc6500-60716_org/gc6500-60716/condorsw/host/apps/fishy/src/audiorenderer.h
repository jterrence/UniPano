/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef AUDIORENDERER_H
#define AUDIORENDERER_H
#include <QAudioOutput>
#include <QIODevice>
#include <QBuffer>
#include <QFile>

class AudioRenderer: public /*QAudioOutput ,*/ QObject
{
     Q_OBJECT
public:
    AudioRenderer();
     ~AudioRenderer();
    void startPlayback();
    void stopPlayback();
    static void renderAudio(void *context);
    void setup();
    void cleanup();
//    qint64 readData(char*, qint64){}
//    qint64 writeData(const char*, qint64){}
//    qint64 bytesAvailable();
signals:
 void emitRender(void);

private:
    QAudioOutput *m_audioOutput;
    QBuffer m_buffer;
    QBuffer m_backup;
    char *m_charBuffer;
    qreal m_size;
    QFile inputFile;
public slots:
     void finishedPlaying(QAudio::State);
//     void statedWhat(QAudio::State state);
     void renderAudioOut();
     void playOnDevice();
};

#endif // AUDIORENDERER_H
