/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef CAMERASETTINGS_H
#define CAMERASETTINGS_H

#include <QDialog>
#include <QKeyEvent>

namespace Ui {
    class CameraSettings;
}

class CameraSettings : public QDialog
{
    Q_OBJECT

public:
    explicit CameraSettings(QWidget *parent = 0);
    ~CameraSettings();

protected:
    void showEvent(QShowEvent *);
    void keyPressEvent(QKeyEvent *);

private:
    Ui::CameraSettings *ui;
    void getValues();
    void settingFailedResponse(QString requestType,QString  val);

    bool readExposureZone;

private slots:

    void brightnessChanged(int);
    void contrastChanged(int);
    void gamaChanged(int);
    void hueChanged(int);
    void saturationChanged(int);
    void sharpnessChanged(int);
    void verticalFlipChanged();
    void horizontalFlipChanged();
    void histogramEqualizationChanged();
    void maxAnalogGainChanged(int);
    void sharpenFilterChanged(int);

    void exposureGainMultiplierChanged(int);

    void onNoiseFilterModeChanged(bool);
    void onNoiseFilterStrengthChanged(int);

    void onWdrModeChanged(bool);
    void onWdrStrengthChanged(int);

    void onWhiteBalanceModeChanged(bool);
    void onWhiteBalanceTemperatureChanged(int);
    void onWhiteBalanceZoneEnabledOrDisabled(bool enabled);
    void onWhiteBalanceZoneValueChanged(int);
#if 0 //Disbaled as there is no MXUVC support for this fetaure
    void onZonalExposureEnabledOrDisabled(bool enabled);
    void onExposureModeChanged(bool);
    void onExposureTimeChanged(int);
    void onExposureZoneValueChanged(int);
#endif

};

#endif // CAMERASETTINGS_H
