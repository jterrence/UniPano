/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef AUDIOSETTINGS_H
#define AUDIOSETTINGS_H

#include <QMouseEvent>
#include <QKeyEvent>
#include <QDialog>
#include <QTime>
#include <QTimer>
#include "engineapimanager.h"

namespace Ui {
    class AudioRecord;
}

typedef enum {
    PCM = 0,
    AAC,
    OPUS
} AUDIO_FORMAT;

class AudioRecord : public QDialog
{
    Q_OBJECT

public:
    explicit AudioRecord(QWidget *parent = 0);
    ~AudioRecord();
    static void audioCB(void *);

    void startAudioRecord();
    void stopAudioRecord();

    void setCaptureOptions(QList<AUDIO_FORMAT> formats, int defaultOption=0);

protected:
    void mousePressEvent(QMouseEvent * event);
    void keyPressEvent(QKeyEvent * event);
    void hideEvent(QHideEvent *);

private:
    Ui::AudioRecord *ui;
    QTime elapsedTime;
    QTimer timer;
    bool audioDataReceived;
    int totalAudioFramesReceived;
    QString recordTime;
    QString fileName;

    QString captureType;

    void onAudioStarted();
    void onAudioStopped();

    bool audioStarted;

    EngineApiManager *apiManager;

signals:
    void signalHidden();

private slots:
    void onButtonClicked();
    void updateDisplay();
    void getSamplingRate(int);
    void onVolumeChanged(int volume);
    void onEngineGetMicVolumeSuccess(int volume);
    void onEngineGetMicVolumeFailed();
    void onEngineSetMicVolumeSuccess();
    void onEngineSetMicVolumeFailed();
    void onEngineMuteMicSuccess();
    void onEngineMuteMicFailed();
    void onEngineAGCSuccess();
    void onEngineAGCFailed();

    // Mic Mute
    void on_cbMuteLeft_clicked(bool);
    void on_cbMuteRight_clicked(bool);
    void on_cbMuteBoth_clicked(bool);

    // AGC
    void on_cbAGC_toggled(bool);
};

#endif // AUDIOSETTINGS_H
