/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef ALERTHANDLER_H
#define ALERTHANDLER_H

#include <QObject>

/**
 * @brief AlertHandler class
 *
 *        Provides functions to display messages alerts (error, warning, etc)
 */
class AlertHandler : public QObject
{
    Q_OBJECT
public:
    explicit AlertHandler(QObject *parent = 0);

    static void displayError(QString message, QWidget *parent=0);
    static void displayMessage(QString message, QString title);
    static void displayInfo(QString message);

signals:

public slots:

};

#endif // ALERTHANDLER_H
