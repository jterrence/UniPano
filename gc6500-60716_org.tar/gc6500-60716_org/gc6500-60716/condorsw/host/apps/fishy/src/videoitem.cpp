/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/


#include "videoitem.h"
#include <QVideoSurfaceFormat>
#include <QPainter>
#include <QThread>
#include <QGraphicsScene>
#include <QWidget>
#include <QStyleOptionGraphicsItem>
#include "log.h"

#ifndef FFMPEG
const int shl16_1point404 = (int) (1.404 * (1 << 16));
const int shl16_0point344 = (int) (0.344 * (1 << 16));
const int shl16_0point714 = (int) (0.714 * (1 << 16));
const int shl16_1point772 = (int) (1.772 * (1 << 16));
#define CLIP(x) if (x > 255) {x = 255;} if (x < 0) {x = 0;}

int yuvtorgb(int y, int u, int v)
{
    int r, g, b;

    u -= 128;
    v -= 128;

    r = (y + ((shl16_1point404 * v) >> 16));
    g = (y - (((shl16_0point344 * u) + (shl16_0point714 * v)) >> 16));
    b = (y + ((shl16_1point772 * u) >> 16));

    CLIP(r);
    CLIP(g);
    CLIP(b);

    return ((r << 16) + (g << 8) + b);
}

/*void yuy2_to_rgb(unsigned *dst, void *data, int width, int height)
{
    int i;
    unsigned char *buf = (unsigned char*)data;
    for(i = 0; i < (width*height*4)/4; i++)
    {
        int yvalue = buf[4*i];
        int uvalue = buf[4*i+1];
        int vvalue = buf[4*i+3];

        int pix = yuvtorgb(yvalue, uvalue, vvalue);
        *dst++ = pix;

        yvalue = buf[4*i+2];
        pix = yuvtorgb(yvalue, uvalue, vvalue);
        *dst++ = pix;
    }
}*/

void yv12_to_rgb(unsigned *dst, void* data, int width, int height)
{
    int i,j;
    unsigned char * y = (unsigned char*)data;
    unsigned char * u = (unsigned char*)data + width*height;
    unsigned char * v = u + width*height/4;
    for(j = 0; j < height; j++) {
        for(i = 0; i < width/2; i++)
        {
            int yvalue = y[2*i];
            int uvalue = u[i];
            int vvalue = v[i];

            int pix = yuvtorgb(yvalue, uvalue, vvalue);
            *dst++ = pix;

            //yvalue = y[2*i+1];
            //pix = yuvtorgb(yvalue, uvalue, vvalue);
            *dst++ = pix;
        }

        y += width;
        if(j&1) {
            u += width/2;
            v += width/2;
        }
    }
}

/*void nv12_to_rgb(unsigned *dst, void* data, int width, int height)
{
    int i,j;
    unsigned char * y = (unsigned char*)data;
    unsigned char * uv = (unsigned char*)data + width*height;
    for(j = 0; j < height; j++) {
        for(i = 0; i < width/2; i++)
        {
            int yvalue = y[2*i];
            int uvalue = uv[2*i];
            int vvalue = uv[2*i+1];

            int pix = yuvtorgb(yvalue, uvalue, vvalue);
            *dst++ = pix;

            yvalue = y[2*i+1];
            pix = yuvtorgb(yvalue, uvalue, vvalue);
            *dst++ = pix;
        }

        y += width;
        if(j&1) {
            uv += width;
        }
    }
}*/
#endif

void ImageDrawer::drawMask(QRect rect, int index) {
    if (imageSize.width() <= 0 || imageSize.height() <= 0) {
        return;
    }

    if (rect.x() == 0 && rect.y() == 0 && rect.width() == 0 && rect.height() == 0) {
        return;
    }

    //QImage image(imageSize.width(), imageSize.height(), QImage::Format_ARGB32_Premultiplied);
    QImage image(rect.width(), rect.height(), QImage::Format_ARGB32_Premultiplied);
    image.setOffset(QPoint(rect.x(), rect.y()));
    image.fill(Qt::transparent);

    QPainter painter(&image);

    // Draw green border
    /*painter.setPen(QPen(Qt::green, 1));
    painter.setBackgroundMode(Qt::TransparentMode);
    painter.setBackground(Qt::transparent);
    painter.drawRect(0, 0, rect.width()-1, rect.height()-1);*/

    // Draw the mask number
    painter.setPen(Qt::white);
    painter.setFont(QFont("Arial", 14, QFont::Bold));
    //painter.drawText(rect, Qt::AlignCenter, QString::number(index+1));
    painter.drawText(QRect(0, 0, rect.width()-1, rect.height()-1), Qt::AlignCenter, QString::number(index+1));

    emit signalRectangleDrawn(image, index);
}

void ImageDrawer::drawGreenBorderImage() {
    if (imageSize.width() <= 0 || imageSize.height() <= 0) {
        return;
    }

    if (borderRect.x() == 0 && borderRect.y() == 0 && borderRect.width() == 0 && borderRect.height() == 0) {
        return;
    }

    QImage image(imageSize.width(),imageSize.height(), QImage::Format_ARGB32_Premultiplied);
    image.fill(Qt::transparent);
    QPainter painter(&image);
    painter.setPen(QPen(Qt::green, 3));
    painter.setBackgroundMode(Qt::TransparentMode);
    painter.setBackground(Qt::transparent);
    painter.drawRect(borderRect);

    emit(signalGreenBorderDrawn(image));
}

void ImageDrawer::drawMap() {
    if (imageSize.width() <= 0 || imageSize.height() <= 0) {
        return;
    }

    if (this->points.length() == 0) {
        return;
    }

    QImage image(imageSize.width(),imageSize.height(), QImage::Format_ARGB32_Premultiplied);
    image.fill(Qt::transparent);

    QPainter painter(&image);
    painter.setPen(QPen(Qt::green, 8));
    painter.setBackgroundMode(Qt::TransparentMode);
    painter.setBackground(Qt::transparent);
    foreach(QPoint p, points) {
        painter.drawPoint(p.x(),p.y());
    }

    emit(signalMapDrawn(image));
}

VideoItem::VideoItem(QGraphicsItem *parent)
    : QGraphicsItem(parent)
    , imageFormat(QImage::Format_Invalid)
    , bStatsEnabled(false)
    , noOfRect(0)
    , render(true)
{
    lockFrame = new QMutex(QMutex::Recursive);
    lockStats = new QMutex(QMutex::Recursive);
    viewWidth = 0;
    viewHeight = 0;
    iFrameCount = 0;
    channel = -1;
    bps = -1;
    fps = -1;
    gop = -1;
    res = "";
    profile = "";
    quality = 0;
    continuity = -1;
    iFrameCount = 0;
    tempNumFramesRendered = 0;
    isJpegChannel = false;
    isRawChannel = false;
    numFramesRendered = 0;
    smoothScaling = false;
    bDrawTitle = true;
    numFramesDropped = 0;
    numFramesCaptured = 0;
    numFramesDelay = 0;
    duration = 1.0;
    pixelFormat = PIX_FMT_RGB32; //PIX_FMT_YUV420P;
    decodedWidth = 0;
    decodedHeight = 0;

    buffer = NULL;

    normal = QFont("Arial", 10, QFont::Normal);
    bold = QFont("Arial", 10, QFont::Normal);

    data = NULL;

    drawThread = new QThread;
    drawObject = new ImageDrawer();
    connect(drawObject, SIGNAL(signalGreenBorderDrawn(QImage)), this, SLOT(onGreenBorderDrawn(QImage)));
    connect(drawObject, SIGNAL(signalMapDrawn(QImage)), this, SLOT(onMapDrawn(QImage)));
    connect(drawObject, SIGNAL(signalRectangleDrawn(QImage,int)), this, SLOT(onRectangleDrawn(QImage,int)));
    connect(drawObject, SIGNAL(signalRgbImageReady(QImage)), this, SLOT(onRgbImageReady(QImage)));
    drawObject->moveToThread(drawThread);
    drawThread->start();
    //drawThread->setPriority(QThread::HighestPriority);

    qRegisterMetaType<void*>("void*");

    swsContext = NULL;

    dest_planes[0] = NULL;
    dest_planes[1] = NULL;
    dest_planes[2] = NULL;

    src_planes[0] = NULL;
    src_planes[1] = NULL;
    src_planes[2] = NULL;

    dest_stride[0] = 0;
    dest_stride[1] = 0;
    dest_stride[2] = 0;

    src_stride[0] = 0;
    src_stride[1] = 0;
    src_stride[2] = 0;

    yStatsPos = 0;
    timeBitrateMinMax = 10;
    hStatsPos = 5;

    userSentUpdate = false;
    bufferFullnessEnabled = false;

    avSrcFrame = NULL;
}

VideoItem::~VideoItem()
{
    delete lockFrame;
    if (drawThread != NULL) {
        drawThread->exit(0);
        delete drawThread;
        delete drawObject;
    }

    if (masks.count() > 0) {
        masks.clear();
    }

    if (buffer != NULL) {
        free (buffer);
        buffer = NULL;
    }

    if (swsContext != NULL) {
        sws_freeContext(swsContext);

        for (int i = 0; i < 3; i++) {
            free (src_planes[i]);
        }

        free (dest_planes[0]);
    }

    if (avSrcFrame != NULL) {
        av_free(avSrcFrame);
    }
}

QRectF VideoItem::boundingRect() const
{
//    return QRectF(QPointF(0,0), surfaceFormat().sizeHint());
    return QRectF(QPointF(0,0), QSize(viewWidth, viewHeight));
}

void VideoItem::setStats(Stats *stats) {
    lockStats->lock();
    bps = stats->bps;
    bps_min = stats->bps_min;
    bps_max = stats->bps_max;
    fps = stats->fps;
    iFrameCount = stats->iFrame;
    numFramesDropped = stats->drops;
    continuity = stats->continuty;
    numFramesCaptured = stats->usbFrameCaptured;
    numFramesRendered = tempNumFramesRendered;
    if (numFramesCaptured >= numFramesRendered) {
        numFramesDropped += (numFramesCaptured-numFramesRendered);
    }
    numFramesDelay = stats->numFramesDelay;
    tempNumFramesRendered = 0;
    duration = stats->duration;
    lockStats->unlock();
}

int done = 0;
void VideoItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    if((isVisible() && isActive()) || isSelected())
    {
#ifdef PROFILING
        int profiling = QTime::currentTime().msec();
#endif

        //painter->setRenderHint(QPainter::Antialiasing, true);
        //painter->setRenderHint(QPainter::SmoothPixmapTransform, true);

        if(smoothScaling) {
            if(!currentImage.isNull()) {
                currentImage = currentImage.scaled(viewWidth,viewHeight,Qt::KeepAspectRatio,Qt::SmoothTransformation);
            }
        }

#ifdef FFMPEG
        if (data != NULL) {

            /*if (buffer == NULL) {
                buffer = (uint8_t*) malloc(viewWidth * viewHeight * 4);
            }
            yv12_to_rgb((unsigned*)buffer, data, viewWidth, viewHeight);
            painter->drawImage(option->exposedRect,
                                       QImage((uchar*)buffer,
                                              viewWidth,
                                              viewHeight,
                                              QImage::Format_RGB32));

            if (userSentUpdate && render) {
                if (lockStats->tryLock()) {
                    tempNumFramesRendered++;
                    lockStats->unlock();
                }
            }*/

            if (avSrcFrame == NULL) {
                avSrcFrame = avcodec_alloc_frame();
            }

            avpicture_fill((AVPicture *)avSrcFrame, (uint8_t*)data, pixelFormat, decodedWidth , decodedHeight);

            int dst_width = option->exposedRect.width();
            int dst_height = option->exposedRect.height();

            if ((dst_width*1.0/decodedWidth) > 0.85) {
                dst_width = decodedWidth;
                dst_height = decodedHeight;
            }

            swsContext = sws_getCachedContext(swsContext,
                                              decodedWidth, decodedHeight,
                                              pixelFormat, dst_width, dst_height,
                                              PIX_FMT_RGB32, SWS_FAST_BILINEAR, NULL, NULL, NULL);

            if (buffer == NULL || src_stride[0] != decodedWidth) {
                if (buffer != NULL) {
                    av_free(buffer);
                }

                int nbytes = avpicture_get_size(PIX_FMT_RGB32, decodedWidth, decodedHeight);
                buffer = (uint8_t*)av_malloc( nbytes * sizeof(uint8_t) );

                src_stride[0] = decodedWidth;
            }

            dest_planes[0] = (uint8_t*)buffer;
            dest_stride[0] = dst_width*4;

            sws_scale(swsContext,
                      (const uint8_t * const*)avSrcFrame->data, avSrcFrame->linesize, 0, decodedHeight,
                      dest_planes, dest_stride);

            painter->drawImage(option->exposedRect, QImage((uchar*)buffer,
                                                           dst_width,
                                                           dst_height,
                                                           QImage::Format_RGB32));

            if (userSentUpdate && render) {
                if (lockStats->tryLock()) {
                    tempNumFramesRendered++;
                    lockStats->unlock();
                }
            }
        }

#else
        if (!currentImage.isNull()) {
            lockFrame->lock();
        }
        painter->drawImage(option->exposedRect, currentImage);
        qDebug() << "DRAW";
        if (!currentImage.isNull()) {
            lockFrame->unlock();
        }

        if (userSentUpdate && render) {
            if (lockStats->tryLock()) {
                tempNumFramesRendered++;
                lockStats->unlock();
            }
        }
#endif

        if(channel == 0 && drawMap) {
            if (!dewarpMap.isNull()) {
                painter->drawImage(QRect(0,0,viewWidth,viewHeight), dewarpMap);
            }
        }

        if (!greenBorder.isNull()) {
            painter->drawImage(QRect(0,0,viewWidth,viewHeight), greenBorder);
        }

        if (masks.count() > 0) {
            for (int index = 0; index < masks.count(); index++) {
                painter->drawImage(masks[index].offset(), masks[index]);
            }
        }

        hStatsPos = 0;
        int yStart = yStatsPos + 15;

        if (bDrawTitle && channel != -1) {
            painter->setPen(Qt::white);
            painter->setFont(normal);
            painter->drawText(hStatsPos, yStart ,QString(channelStr + " ("+format+", "+res+")"));

            yStart += 20;
        }

        if(bStatsEnabled || !render)
        {
            if (lockStats->tryLock()) {
                //painter->setPen(Qt::white);
                painter->setFont(bold);
                painter->drawText(hStatsPos, yStart, tr("Captured (%4 sec): %1, Rendered: %2, Dropped: %3").arg(numFramesCaptured).arg(numFramesRendered).arg(numFramesDropped).arg(duration, 0, 'f', 3));
                painter->drawText(hStatsPos,yStart+15,QString("framerate: ") + QString::number(fps));

                if(!isRawChannel) {
                    if(!isJpegChannel) {
                        painter->drawText(hStatsPos,yStart+30,tr("bitrate: %1 kbps %2").arg(QString::number(bps/1024)).arg(bps_min > 0 ? tr("(min: %2 kbps, max: %3 kbps for %4 sec)").arg(bps_min>0?QString::number(bps_min/1024):"-").arg(bps_max>0?QString::number(bps_max/1024):"-").arg(timeBitrateMinMax):""));
                        painter->drawText(hStatsPos,yStart+45,QString("gop: ") + (gop>=0?QString::number(gop):"-"));
                        painter->drawText(hStatsPos,yStart+60,QString("profile: ") + profile);
                        painter->drawText(hStatsPos,yStart+75,QString("continuity: ") + QString::number(continuity));
                        painter->drawText(hStatsPos,yStart+90,QString("iframe count: ") + QString::number(iFrameCount));
                        painter->drawText(hStatsPos,yStart+105,QString("delay: ") + QString::number(numFramesDelay).append(numFramesDelay>1?" frames":" frame"));
                    }
                    else
                    {
                        painter->drawText(hStatsPos,yStart+30,QString("avg frame size: " + (bps>=0?(QString::number(bps/1000)+" kb"):"-")));
                        painter->drawText(hStatsPos,yStart+45,QString("quality: ") + (quality>=0?QString::number(quality):"-"));
                    }
                }

                if (userSentUpdate && bufferFullnessEnabled) {
                    emit signalPlotGraph();
                }

                lockStats->unlock();
            }
        }

        for(int i = 0 ; i < noOfRect; i++)
        {
            painter->setPen(rectColor[i]);
            painter->drawRect(rect[i]);
            //Resetting Rect once updated
            rect[i].setWidth(0);
            rect[i].setHeight(0);
        }

        //currentFrame.unmap();

        if (userSentUpdate) {
            userSentUpdate = false;
        }
    }

#ifdef PROFILING
    qDebug()<< this->channel <<"Total Painter Profiling time in ms"<< QTime::currentTime().msec()- profiling;
#endif
}

void VideoItem::initMasks(int num) {
    for (int index = 0; index < num; index++) {
        masks.append(QImage(0, 0, QImage::Format_Invalid));
    }
}

void VideoItem::drawMask(QRect rect, int index) {
    if (rect.x() != 0 || rect.y() != 0 || rect.width() != 0 || rect.height() != 0) {
        QMetaObject::invokeMethod(drawObject, "drawMask", Qt::QueuedConnection, Q_ARG(QRect, rect), Q_ARG(int, index));
    }
}

void VideoItem::removeMask(int index) {
    if (index < masks.count()) {
        masks[index] = QImage(0, 0, QImage::Format_Invalid);
    }
}

void VideoItem::setRectToFocus(QRect rect) {
    greenBorder = QImage(0, 0, QImage::Format_Invalid);
    drawObject->setBorderRect(rect);
    if (rect.x() != 0 || rect.y() != 0 || rect.width() != 0 || rect.height() != 0) {
        QMetaObject::invokeMethod(drawObject, "drawGreenBorderImage", Qt::QueuedConnection);
    }
}

void VideoItem::setPoints(QList<QPoint> points) {
    this->points = points;
    dewarpMap = QImage(0, 0, QImage::Format_Invalid);
    drawObject->setPoints(points);
    QMetaObject::invokeMethod(drawObject, "drawMap", Qt::QueuedConnection);
}

// SLOTS

void VideoItem::onGreenBorderDrawn(QImage image) {
    greenBorder = image;
}

void VideoItem::onRectangleDrawn(QImage image, int index) {
    if (index < masks.count()) {
        masks[index] = image;
    } else {
        masks.append(image);
    }
}

void VideoItem::onMapDrawn(QImage image) {
    dewarpMap = image;
}

void VideoItem::onRgbImageReady(QImage image) {
    rgbImage = image;
    scene()->update();
}

void VideoItem::setRectCount(int rect)
{
    noOfRect = rect;
    Qt::GlobalColor j = Qt::color0;
    for(int i = 0; i< noOfRect;i++)
    {
        rectColor[i] = (Qt::GlobalColor)(j + (Qt::GlobalColor)i+5);
        if(j >= Qt::transparent)
            j = Qt::black;
    }
}


QList<QVideoFrame::PixelFormat> VideoItem::supportedPixelFormats(
        QAbstractVideoBuffer::HandleType handleType) const
{
    if (handleType == QAbstractVideoBuffer::NoHandle) {
        return QList<QVideoFrame::PixelFormat>()
                << QVideoFrame::Format_RGB32
                << QVideoFrame::Format_ARGB32
                << QVideoFrame::Format_ARGB32_Premultiplied
                << QVideoFrame::Format_RGB565
                << QVideoFrame::Format_RGB555
                << QVideoFrame::Format_YUV420P;
    } else {
        return QList<QVideoFrame::PixelFormat>();
    }
}

bool VideoItem::start(const QVideoSurfaceFormat &format)
{
    if (isFormatSupported(format)) {
        imageFormat = QVideoFrame::imageFormatFromPixelFormat(format.pixelFormat());
        imageSize = format.frameSize();

        if (!QAbstractVideoSurface::start(format)) {
            qCritical()<<"Start failed!";
            return false;
        }

#ifndef QT5
        prepareGeometryChange();
#endif

        drawObject->setImageSize(imageSize);
        QMetaObject::invokeMethod(drawObject, "drawGreenBorderImage", Qt::QueuedConnection);
        QMetaObject::invokeMethod(drawObject, "drawMap", Qt::QueuedConnection);

        return true;
    } else {
        return false;
    }
}

void VideoItem::stop()
{
    //currentFrame = QVideoFrame();
    currentImage = QImage();

    QAbstractVideoSurface::stop();
}

bool VideoItem::present(const QVideoFrame &)
{
    return true;
}

void VideoItem::renderStats() {
    userSentUpdate = true;
    scene()->update();
}

void VideoItem::renderYuv() {
    if (!currentImage.isNull()) {
        currentImage = QImage();
    }

    userSentUpdate = true;
    scene()->update();
}

void VideoItem::setImage(QImage image) {
    if(lockFrame->tryLock() && ((isVisible() && isActive()))) {
        currentImage = image;
        lockFrame->unlock();
        userSentUpdate = true;
        scene()->update();
    }
}
