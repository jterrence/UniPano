/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include <QCoreApplication>
#include <QDebug>
#include <QMouseEvent>
#include "videorendersurface.h"

VideoRenderSurface::VideoRenderSurface(QWidget *parent) :
    QGraphicsView(parent)
{
    scene = new QGraphicsScene();
    this->setScene(scene);

    this->setAttribute(Qt::WA_OpaquePaintEvent);
    this->setViewportUpdateMode(QGraphicsView::NoViewportUpdate);

    //scene->setItemIndexMethod(QGraphicsScene::NoIndex);
    setOptimizationFlag(QGraphicsView::DontSavePainterState);

    setAlignment(Qt::AlignHCenter);
}

void VideoRenderSurface::mousePressEvent(QMouseEvent * event)
{
    event->ignore();
}

void VideoRenderSurface::mouseReleaseEvent(QMouseEvent * event)
{
    event->ignore();
}

void VideoRenderSurface::mouseDoubleClickEvent(QMouseEvent * event)
{
    event->ignore();
}


void VideoRenderSurface::wheelEvent(QWheelEvent *event)
{
    event->ignore();
}

void VideoRenderSurface::showEvent(QShowEvent *event)
{
    qDebug()<< "Render surface" << this->rect();
    Q_UNUSED(event);
    scene->setSceneRect(this->rect());
    setAlignment ( Qt::AlignCenter);
    QColor brushColor(45,45,45);
    this->setBackgroundBrush(QBrush(brushColor, Qt::Dense2Pattern));
}

void VideoRenderSurface::keyPressEvent(QKeyEvent *event)
{
    QWidget::keyPressEvent(event);
}

void VideoRenderSurface::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event);
    scene->setSceneRect(this->rect());
    setAlignment(Qt::AlignCenter);
}
