/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include <QGridLayout>
#include <QtGui/QApplication>
#include <QDesktopWidget>
#include <QTimer>

#include "privacymasklayout.h"
#include "privacymaskcontrols.h"
#include "overlaymanager.h"
#include "overlaytask.h"
#include "log.h"
#include "alerthandler.h"
#include "progressbar.h"
#include <draggableframe.h>

QTimer timer;


PrivacyMaskLayout::PrivacyMaskLayout(QWidget *parent, int channelWithZclStretch) :
    QWidget(parent),
    displayChannel(NULL),
    task(NULL),
    maskControls(NULL),
    bar(NULL)
{
    // setup layout
    int screenWidth = qApp->desktop()->screenGeometry().width();
    int screenHeight = qApp->desktop()->screenGeometry().height();

    QGridLayout* gridLayout = new QGridLayout;
    gridLayout->setSizeConstraint (QLayout::SetMaximumSize);
    gridLayout->setSpacing(5);
    gridLayout->setContentsMargins(2, 2, 2, 2);

    displayChannel = new DisplayChannel(this);
    displayChannel->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    displayChannel->setMinimumSize(screenWidth-320-10, screenHeight-50);
    displayChannel->setMaximumSize(screenWidth-320-10, screenHeight-50);
    gridLayout->addWidget(displayChannel, 0, 0, 1, 1, Qt::AlignCenter);

    maskControls = new PrivacyMaskControls(this);
    maskControls->setMaximumWidth(400);
    //maskControls->setMinimumWidth(200);
    //maskControls->setMinimumHeight(400);
    maskControls->setMaximumHeight(400);
    maskControls->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);

    gridLayout->addWidget(maskControls, 0, 1, 1, 1, Qt::AlignCenter);

    displayChannel->setChannel(channelWithZclStretch);
    displayChannel->setHandleMouseEvents(false);

    this->setLayout(gridLayout);

    maskControls->setFocusPolicy(Qt::StrongFocus);
    maskControls->setFocus();

    connect(maskControls, SIGNAL(signalAddPrivacyMask(int,int,int,int,int,int,QColor)), this, SLOT(onAddPrivacyMask(int,int,int,int,int,int,QColor)));
    connect(maskControls, SIGNAL(signalRemovePrivacyMask(int,int)), this, SLOT(onRemovePrivacyMask(int,int)));
    connect(maskControls, SIGNAL(signalHighlightChanged(int,int,bool)), this, SLOT(onHighlightChanged(int,int,bool)));
    connect(maskControls, SIGNAL(signalRemoveAllMasks(int)), this, SLOT(onRemoveAllMasks(int)));
    connect(maskControls, SIGNAL(signalColorUpdated(int,int,QColor)), this, SLOT(onColorUpdated(int,int,QColor)));
    connect(maskControls, SIGNAL(signalMaskSelected(int,int)), this, SLOT(onMaskSelected(int,int)));

    connect(maskControls, SIGNAL(signalXChanged(int)), this, SLOT(onXChanged(int)));
    connect(maskControls, SIGNAL(signalYChanged(int)), this, SLOT(onYChanged(int)));

    task = new OverlayTask();
    //connect(task, SIGNAL(signalTaskDestroyed()), this, SIGNAL(signalCleanupDone()));
    connect(task, SIGNAL(signalAddMaskFailed(int,int)), this, SLOT(onAddMaskFailed(int,int)));
    connect(task, SIGNAL(signalAddMaskSuccess(int,int)), this, SLOT(onAddMaskSuccess(int,int)));
    task->startThread(this);

    connect(displayChannel, SIGNAL(signalOnVideoStarted(video_channel_t)),this,SLOT(onVideoStarted(video_channel_t)));

    connect(&timer, SIGNAL(timeout()), this, SLOT(onTimeOut()));
    timer.setSingleShot(true);

    this->setObjectName("PrivacyMaskLayout");
}

PrivacyMaskLayout::~PrivacyMaskLayout() {
    qDebug() << "Destroy";
    task->cleanup(-1);

    foreach (DraggableFrame *frame, maskList) {
        if (frame != NULL) {
            delete frame;
        }
    }

    delete displayChannel;
    delete maskControls;

    maskList.clear();

    bar->close();
    delete bar;
}

void PrivacyMaskLayout::keyPressEvent(QKeyEvent *event) {
    DraggableFrame *frame = maskList.at(0);
    QRect rect = frame->geometry();

    switch(event->key()) {
        case Qt::Key_Left:
            event->accept();
            rect.setX(rect.x()-1);
            rect.setWidth(rect.width()-1);
            frame->setGeometry(rect);
            maskControls->updatePosition(0, rect.x(), rect.y());
            break;
        case Qt::Key_Right:
            event->accept();
            rect.setX(rect.x()+1);
            rect.setWidth(rect.width()+1);
            frame->setGeometry(rect);
            maskControls->updatePosition(0, rect.x(), rect.y());
            break;
    }
}

// SLOTS
void PrivacyMaskLayout::onVideoStarted(video_channel_t) {
    displayChannel->initMasks(OverlayManager::shared()->getNumPrivMasks());
    for (int index = 0; index < OverlayManager::shared()->getNumPrivMasks(); index++) {
        maskList.append(NULL);
    }
    timer.start(800);

    bar = new ProgressBar(this);
    int w = 150;
    int h = bar->geometry().height();
    int x = (displayChannel->geometry().width() - w)/2;
    int y = (displayChannel->geometry().height() - h)/2;
    bar->setGeometry(x, y, w, h);
}

void PrivacyMaskLayout::onAddPrivacyMask(int channel, int index, int x, int y, int w, int h, QColor c) {
    PrivacyMask *mask = OverlayManager::shared()->getPrivacyMask(channel, index);
    mask->xOffset = x;
    mask->yOffset = y;
    mask->width = w;
    mask->height = h;
    mask->color = c;

    displayChannel->removeMask(index);
    task->resetPrivacyMask(channel, index);

    bar->show();
}

void PrivacyMaskLayout::onMaskMoved(int index, int x, int y) {
    PrivacyMask *mask = OverlayManager::shared()->getPrivacyMask(0, index);
    displayChannel->removeMask(index);
    if (x == -1 && y == -1) {
        /*DraggableFrame *draggable = maskList.at(index);
        if (draggable) {
            draggable->select();
        }*/
        maskControls->selectIndex(index);
    } else {
        x = x * mask->ratio;
        y = y * mask->ratio;

        maskControls->updatePosition(index, x, y);
    }

    DraggableFrame *draggable = NULL;
    for (int i = 0; i < maskList.count(); i++) {
        if (i == index) {
            continue;
        }
        draggable = maskList.at(i);
        if (draggable != NULL) {
            //if (draggable->isSelected()) {
            draggable->reset();
            draggable->deselect();

            PrivacyMask *mask = OverlayManager::shared()->getPrivacyMask(0, i);
            if (mask->enabled /*&& mask->highlight*/) {
                QRect numberRect;
                numberRect.setX(mask->xOffset/mask->ratio);
                numberRect.setY(mask->yOffset/mask->ratio);
                numberRect.setWidth(mask->width/mask->ratio);
                numberRect.setHeight(numberRect.width() * mask->height/mask->width);

                displayChannel->drawMask(numberRect, i);
            }
            //}
        }
    }
}

void PrivacyMaskLayout::onRemovePrivacyMask(int channel, int index) {
    PrivacyMask *mask = OverlayManager::shared()->getPrivacyMask(channel, index);
    mask->enabled = 0;

    task->removePrivacyMask(channel, index);
    displayChannel->removeMask(index);

    DraggableFrame *draggable = maskList.at(index);
    delete draggable;
    maskList[index] = NULL;

    maskControls->update();
}

void PrivacyMaskLayout::onXChanged(int x) {
    PrivacyMask *mask = OverlayManager::shared()->getPrivacyMask(0, 0);
    int xDiff = x - mask->xOffset;

    DraggableFrame *frame = maskList.at(0);
    frame->moveX(xDiff);
}

void PrivacyMaskLayout::onYChanged(int y) {
    PrivacyMask *mask = OverlayManager::shared()->getPrivacyMask(0, 0);
    int yDiff = y - mask->yOffset;

    DraggableFrame *frame = maskList.at(0);
    frame->moveY(yDiff);
}

// User selected mask from the right control panel
void PrivacyMaskLayout::onMaskSelected(int channel, int index) {
    // Remove green highlight for the selected mask, and show the red draggable frame instead
    PrivacyMask *mask = OverlayManager::shared()->getPrivacyMask(channel, index);

    DraggableFrame *draggable = NULL;

    createDraggableMask(channel, index);
    draggable = maskList.at(index);
    if (mask->enabled) {
        displayChannel->removeMask(index);
        draggable->select();
    } else {
        draggable->dirty();
    }

    // Remove the draggable

    for (int i = 0; i < maskList.count(); i++) {
        if (i != index) {
            draggable = maskList.at(i);
            if (draggable != NULL) {
                draggable->reset();
                draggable->deselect();

                PrivacyMask *mask = OverlayManager::shared()->getPrivacyMask(channel, i);
                if (mask->enabled /*&& mask->highlight*/) {

                    QRect numberRect;
                    numberRect.setX(mask->xOffset/mask->ratio);
                    numberRect.setY(mask->yOffset/mask->ratio);
                    numberRect.setWidth(mask->width/mask->ratio);
                    numberRect.setHeight(numberRect.width() * mask->height/mask->width);

                    displayChannel->drawMask(numberRect, i);
                }
            }
        }
    }
}

void PrivacyMaskLayout::onColorUpdated(int ch, int index, QColor color) {
    PrivacyMask *mask = OverlayManager::shared()->getPrivacyMask(ch, index);
    mask->color = color;
    task->updatePrivacyMaskColor(ch, index);
}

void PrivacyMaskLayout::onHighlightChanged(int channel, int index, bool checked) {
    PrivacyMask *mask = OverlayManager::shared()->getPrivacyMask(channel, index);
    mask->highlight = checked;
    if (checked) {
        displayChannel->drawMask(QRect(mask->xOffset, mask->yOffset, mask->width, mask->height), index);
    } else {
        displayChannel->removeMask(index);
    }
}

void PrivacyMaskLayout::onRemoveAllMasks(int channel) {
    for (int index = 0; index < OverlayManager::shared()->getNumPrivMasks(); index++) {
        PrivacyMask *mask = OverlayManager::shared()->getPrivacyMask(channel, index);
        mask->enabled = 0;

        task->removePrivacyMask(channel, index);
        displayChannel->removeMask(index);

        DraggableFrame *draggable = maskList.at(index);
        if (draggable != NULL) {
            delete draggable;
        }
        maskList[index] = NULL;
    }
}

void PrivacyMaskLayout::onTimeOut() {
    PrivacyMask *mask = NULL;
    int indexToSelect = -1;
    for (int index = 0; index < OverlayManager::shared()->getNumPrivMasks(); index++) {
        mask = OverlayManager::shared()->getPrivacyMask(0, index);
        if (mask->enabled) {
            createDraggableMask(0, index);

            if (indexToSelect == -1) {
                indexToSelect = index;
            }
        }
        if (mask->enabled && mask->highlight) {

            QRect numberRect;
            numberRect.setX(mask->xOffset/mask->ratio);
            numberRect.setY(mask->yOffset/mask->ratio);
            numberRect.setWidth(mask->width/mask->ratio);
            numberRect.setHeight(numberRect.width() * mask->height/mask->width);

            displayChannel->drawMask(numberRect, index);
        } else {
            displayChannel->removeMask(index);
        }
    }

    if (indexToSelect > -1) {
        maskControls->selectIndex(indexToSelect);
        DraggableFrame *frame = maskList.at(indexToSelect);
        frame->select();
    }

    maskControls->update();
}

void PrivacyMaskLayout::createDraggableMask(int channel, int index) {
    DraggableFrame *draggable = maskList.at(index);

    if (draggable != NULL) {
        return;
    }

    draggable = new DraggableFrame(displayChannel, index);
    maskList[index] = draggable;
    connect(draggable, SIGNAL(signalMoved(int,int,int)), this, SLOT(onMaskMoved(int,int,int)));

    PrivacyMask *mask = OverlayManager::shared()->getPrivacyMask(channel, index);
    QRect maskRect = QRect(mask->xOffset, mask->yOffset, mask->width, mask->height);
    QRect boundaryRect = QRect(0, 0, 0, 0);

    if (displayChannel->getChannelWidth() >= displayChannel->rect().width()) {
        float ratio = displayChannel->getChannelWidth()*1.0f/displayChannel->rect().width();
        mask->ratio = ratio;

        maskRect.setX(mask->xOffset/ratio + 2);
        maskRect.setY(mask->yOffset/ratio + 1);
        maskRect.setWidth(mask->width/ratio);
        maskRect.setHeight(maskRect.width()*mask->height/mask->width);

        QSize videoSize = displayChannel->getActualVideoSize();

        boundaryRect.setX(abs(displayChannel->rect().width()-videoSize.width())/2);
        boundaryRect.setY(abs(videoSize.height()-displayChannel->rect().height())/2);
        boundaryRect.setWidth(videoSize.width());
        boundaryRect.setHeight(videoSize.height());

    } else {

        QSize videoSize = displayChannel->getActualVideoSize();

        boundaryRect.setX(abs(displayChannel->rect().width()-videoSize.width())/2);
        boundaryRect.setY(abs(videoSize.height()-displayChannel->rect().height())/2);
        boundaryRect.setWidth(videoSize.width());
        boundaryRect.setHeight(videoSize.height());

        /*boundaryRect.setX(abs(displayChannel->getChannelWidth()-displayChannel->rect().width())/2);
        boundaryRect.setY(abs(displayChannel->getChannelHeight()-displayChannel->rect().height())/2);
        boundaryRect.setWidth(displayChannel->getChannelWidth());
        boundaryRect.setHeight(displayChannel->getChannelHeight());*/
    }

    draggable->setDragLimit(boundaryRect);
    QRect originalGeometry = QRect(boundaryRect.x()+maskRect.x(), boundaryRect.y()+maskRect.y(), maskRect.width(), maskRect.height());
    draggable->setOriginalGeometry(originalGeometry);
    draggable->setGeometry(originalGeometry);
    draggable->show();
    //draggable->dirty();
    draggable->deselect();
    //draggable->reset();
}

void PrivacyMaskLayout::selectDraggableMask(int channel, int index) {
    DraggableFrame *draggable = NULL;
    if (index < maskList.count()) {
        draggable = maskList.at(index);
    }

    if (draggable == NULL) {
        createDraggableMask(channel, index);
        draggable = maskList.at(index);
    }
    draggable->select();
}

// OverlayTask Slots
void PrivacyMaskLayout::onAddMaskSuccess(int channel, int index) {
    PrivacyMask *mask = OverlayManager::shared()->getPrivacyMask(channel, index);
    mask->enabled = 1;

    if (mask->highlight) {
        displayChannel->removeMask(index);
    } else {
        if (mask->enabled && mask->highlight) {
            //displayChannel->drawMask(QRect(mask->xOffset, mask->yOffset, mask->width, mask->height), index);
        }
    }

    DraggableFrame *draggable = maskList.at(index);
    delete draggable;
    maskList[index] = NULL;
    //createDraggableMask(channel, index);
    selectDraggableMask(channel, index);

    //maskControls->updateHightlightAllCheckBox();
    //maskControls->enableHightlight(index, 1);
    maskControls->update();

    for (int i = 0; i < OverlayManager::shared()->getNumPrivMasks(); i++) {
        if (i != index) {
            PrivacyMask *mask = OverlayManager::shared()->getPrivacyMask(channel, i);
            if (mask->enabled /*&& mask->highlight*/) {

                QRect numberRect;
                numberRect.setX(mask->xOffset/mask->ratio);
                numberRect.setY(mask->yOffset/mask->ratio);
                numberRect.setWidth(mask->width/mask->ratio);
                numberRect.setHeight(numberRect.width() * mask->height/mask->width);

                displayChannel->drawMask(numberRect, i);
            }
        }
    }

    bar->hide();

    this->setFocus();
}

void PrivacyMaskLayout::onAddMaskFailed(int channel, int index) {
    PrivacyMask *mask = OverlayManager::shared()->getPrivacyMask(channel, index);
    mask->enabled = 0;

    displayChannel->removeMask(index);
    DraggableFrame *draggable = maskList.at(index);
    delete draggable;
    maskList[index] = NULL;

    //maskControls->updateHightlightAllCheckBox();
    //maskControls->enableHightlight(index, 0);
    maskControls->update();

    bar->hide();
    AlertHandler::displayError(tr("Failed to add mask"));
}
