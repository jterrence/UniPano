/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include "channellayout.h"
#include "ui_channellayout.h"
#include "displaychannel.h"
#include "cameraconfig.h"
#include "log.h"
#include "eptzcontrols.h"
#include <privacymasklayout.h>

ChannelLayout::ChannelLayout(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ChannelLayout)
{
    ui->setupUi(this);
}

ChannelLayout::~ChannelLayout()
{
    delete ui;
}

void ChannelLayout::restartAllVideo() {
    QList<DisplayChannel*> channelList = getChannelList();
    for (int child = 0; child < channelList.count(); child++) {
        if (((DisplayChannel*)channelList.at(child))->isVideoEnabled()) {
            ((DisplayChannel*)channelList.at(child))->startVideo();
        }
    }
}

void ChannelLayout::stopAllVideo() {
    QList<DisplayChannel*> channelList = getChannelList();
    for (int child = 0; child < channelList.count(); child++) {
        ((DisplayChannel*)channelList.at(child))->stopVideo();
    }
}

void ChannelLayout::setF1KeyPressed() {
    QList<DisplayChannel*> channelList = getChannelList();
    for (int child = 0; child < channelList.count(); child++) {
        ((DisplayChannel*)channelList.at(child))->setF1KeyPressed();
    }
}

void ChannelLayout::propagateKeyPressEvent(QKeyEvent *event) {
    for (int child = 0; child < this->children().size(); child++) {
        if (this->children().at(child)->objectName().compare("EptzControls") == 0) {
            ((EptzControls*)this->children().at(child))->keyPressEvent(event);
        } /*else if (this->children().at(child)->objectName().compare("PrivacyMaskLayout") == 0) {
            ((PrivacyMaskLayout*)this->children().at(child))->keyPressEvent(event);
        }*/ else if (this->children().at(child)->objectName().compare("DisplayChannel") == 0) {
            ((DisplayChannel*)this->children().at(child))->keyPressEvent(event);
        }
    }
}

QList<DisplayChannel*> ChannelLayout::getChannelList() {
    QList<DisplayChannel*> channelList;
    for (int child = 0; child < this->children().size(); child++) {
        if (this->children().at(child)->objectName().compare("DisplayChannel") == 0) {
            channelList.append((DisplayChannel*)this->children().at(child));
        }
    }
    return channelList;
}
