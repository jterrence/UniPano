/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include "overlaytask.h"
#include <log.h>
#include <overlaymanager.h>
#include <overlay.h>
#include "engineapimanager.h"
#include "stringutils.h"
#include <unistd.h>
#include <QApplication>
#include <QFileInfo>

OverlayTask::OverlayTask(QObject *parent) :
    QObject(parent), emitdone(false)
{
    thread = new QThread();
}

void OverlayTask::startThread(const QObject*) {
    connect(this,SIGNAL(signalCleanupDone()),thread,SLOT(quit()));
    connect(thread,SIGNAL(finished()),this,SLOT(deleteLater()));

    moveToThread(thread);
    thread->start();
}

OverlayTask::~OverlayTask() {
    emit signalTaskDestroyed();
}

void OverlayTask::cleanup(int channel) {
    if (channel!=-1) {
        emitdone = true;
        this->removeAllOverlayText(channel);
    } else {
        thread->quit();
    }
}

void OverlayTask::setOverlayText(int channel, int index) {
    QMetaObject::invokeMethod(this, "queueSetOverlayText", Qt::QueuedConnection, Q_ARG(int, channel), Q_ARG(int, index));
}

void OverlayTask::removeOverlayText(int channel, int index) {
    QMetaObject::invokeMethod(this, "queueRemoveOverlayText", Qt::QueuedConnection, Q_ARG(int, channel), Q_ARG(int, index));
}

void OverlayTask::resetOverlayText(int channel, int index) {
    QMetaObject::invokeMethod(this, "queueRemoveOverlayText", Qt::QueuedConnection, Q_ARG(int, channel), Q_ARG(int, index));
    QMetaObject::invokeMethod(this, "queueSetOverlayText", Qt::QueuedConnection, Q_ARG(int, channel), Q_ARG(int, index));
}

void OverlayTask::setAllOverlayText(int channel) {
    QMetaObject::invokeMethod(this, "queueSetAllOverlayText", Qt::QueuedConnection, Q_ARG(int, channel));
}

void OverlayTask::removeAllOverlayText(int channel) {
    QMetaObject::invokeMethod(this, "queueRemoveAllOverlayText", Qt::QueuedConnection, Q_ARG(int, channel));
}

void OverlayTask::removeTimer(int channel) {
    QMetaObject::invokeMethod(this, "queueRemoveTimer", Qt::QueuedConnection, Q_ARG(int, channel));
}

void OverlayTask::showTimer(int channel) {
    QMetaObject::invokeMethod(this, "queueShowTimer", Qt::QueuedConnection, Q_ARG(int, channel));
}

void OverlayTask::resetAllOverlayText(int channel) {
    this->removeAllOverlayText(channel);
    this->setAllOverlayText(channel);
}

void OverlayTask::loadFont(int channel) {
    //this->removeAllOverlayText(channel);
    QMetaObject::invokeMethod(this, "queueLoadFont", Qt::QueuedConnection, Q_ARG(int, channel));
    //this->setAllOverlayText(channel);
}

void OverlayTask::setOverlayImageAlpha(int channel, int index, int alpha){
    //this->removeAllOverlayText(channel);
    QMetaObject::invokeMethod(this, "queueOverlayImageAlpha", Qt::QueuedConnection, Q_ARG(int, channel), Q_ARG(int, index), Q_ARG(int, alpha));
    //this->setAllOverlayText(channel);
}


void OverlayTask::addOverlayImage(int channel) {
    QMetaObject::invokeMethod(this, "queueAddOverlayImage", Qt::QueuedConnection, Q_ARG(int, channel));

    /*QFuture<bool> future = QtConcurrent::run(this, &OverlayTask::queueAddOverlayImage, channel);
    future.waitForFinished();
    if (!future.result()) {
        emit signalAddOverlayImageFailed(channel);
    }*/
}

void OverlayTask::removeOverlayImage(int channel) {
    QMetaObject::invokeMethod(this, "queueRemoveOverlayImage", Qt::QueuedConnection, Q_ARG(int, channel));
}

void OverlayTask::resetOverlayImage(int channel) {
    //QMetaObject::invokeMethod(this, "queueRemoveOverlayImage", Qt::QueuedConnection, Q_ARG(int, channel));
    QMetaObject::invokeMethod(this, "queueAddOverlayImage", Qt::AutoConnection, Q_ARG(int, channel));

    /*QFuture<bool> future = QtConcurrent::run(this, &OverlayTask::queueAddOverlayImage, channel);
    future.waitForFinished();
    if (!future.result()) {
        emit signalAddOverlayImageFailed(channel);
    }*/
}

void OverlayTask::setPrivacyMask(int channel, int index) {
    QMetaObject::invokeMethod(this, "queueSetPrivacyMask", Qt::QueuedConnection, Q_ARG(int, channel), Q_ARG(int, index));
}

void OverlayTask::removePrivacyMask(int channel, int index) {
    QMetaObject::invokeMethod(this, "queueRemovePrivacyMask", Qt::QueuedConnection, Q_ARG(int, channel), Q_ARG(int, index));
}

void OverlayTask::resetPrivacyMask(int channel, int index) {
    QMetaObject::invokeMethod(this, "queueRemovePrivacyMask", Qt::QueuedConnection, Q_ARG(int, channel), Q_ARG(int, index));
    QMetaObject::invokeMethod(this, "queueSetPrivacyMask", Qt::QueuedConnection, Q_ARG(int, channel), Q_ARG(int, index));
}

void OverlayTask::updatePrivacyMaskColor(int channel, int index) {
    QMetaObject::invokeMethod(this, "queueUpdatePrivacyMaskColor", Qt::QueuedConnection, Q_ARG(int, channel), Q_ARG(int, index));
}

// slots
void OverlayTask::queueSetOverlayText(int channel, int index) {
    if (!OverlayManager::shared()->isFontLoaded(channel)) {
        QFileInfo fontfileinfo(OverlayManager::shared()->getOverlayFont(channel)->fontPath);
        engine_overlay_load_font(channel, (char*)fontfileinfo.absoluteFilePath().toStdString().c_str(), OverlayManager::shared()->getOverlayFont(channel)->fontSize);
        OverlayManager::shared()->setFontLoaded(channel, true);
    }

    OverlayText *text = OverlayManager::shared()->getOverlayText(channel, index);
    if (engine_overlay_add_text(channel, (char*) text->text.toStdString().c_str(), text->xOffset, text->yOffset, &index) < 0) {
        qWarning() << StringUtils::shared()->channelToString((video_channel_t)channel)<<": Failed to add burnin text: "<<index;
        emit signalAddOverlayTextFailed(channel, index);
    }
}
void OverlayTask::queueRemoveOverlayText(int channel, int index) {
    if (engine_overlay_remove_text(channel, index) < 0) {
        qWarning() << StringUtils::shared()->channelToString((video_channel_t)channel)<<": Failed to remove burnin text: "<<index;
    } else {
        qDebug()<<StringUtils::shared()->channelToString((video_channel_t)channel)<<": Text removed";
    }
}
void OverlayTask::queueRemoveAllOverlayText(int channel) {
    for (int index = 0; index < OverlayManager::shared()->getNumOverlayText(); index++) {
        if (engine_overlay_remove_text(channel, index) < 0) {
            qWarning() << StringUtils::shared()->channelToString((video_channel_t)channel)<<": Failed to remove burnin text: "<<index;
        }
    }
    queueRemoveTimer(channel);

    if (emitdone) {
        moveToThread(QApplication::instance()->thread());
        emit signalCleanupDone();
    }
}
void OverlayTask::queueSetAllOverlayText(int channel) {
    if (!OverlayManager::shared()->isFontLoaded(channel)) {
        QFileInfo fontfileinfo(OverlayManager::shared()->getOverlayFont(channel)->fontPath);
        if (engine_overlay_load_font(channel, (char*)fontfileinfo.absoluteFilePath().toStdString().c_str(), OverlayManager::shared()->getOverlayFont(channel)->fontSize) < 0) {
            qWarning() << "Failed to load font";
        } else {
            OverlayManager::shared()->setFontLoaded(channel, true);
        }
    }

    for (int index = 0; index < OverlayManager::shared()->getNumOverlayText(); index++) {
        OverlayText* overlay = OverlayManager::shared()->getOverlayText(channel, index);
        if (overlay->enabled) {
            qDebug() << "Add overlay text"<< overlay->text << "at index "<<overlay->index<<overlay->xOffset<<overlay->yOffset;
            if (engine_overlay_add_text(channel, (char*) overlay->text.toStdString().c_str(), overlay->xOffset, overlay->yOffset, &overlay->index) < 0) {
                qWarning() << StringUtils::shared()->channelToString((video_channel_t)channel)<<": Failed to add burnin text: "<<overlay->index;
                emit signalAddOverlayTextFailed(channel, index);
            }
            usleep(50000);
        }
    }
    //usleep(100000);
    queueShowTimer(channel);
}
void OverlayTask::queueShowTimer(int channel) {
    OverlayTime* time = OverlayManager::shared()->getOverlayTime(channel, true);
    if (engine_overlay_set_time(channel, time->xOffset, time->yOffset, time->hours, time->minutes, time->seconds, true) < 0) {
        qWarning() << StringUtils::shared()->channelToString((video_channel_t)channel)<<": Failed to set overlay time";
        emit signalAddOverlayTimeFailed(channel);
    } else {
        if (engine_overlay_show_time(channel) < 0) {
            qWarning() << StringUtils::shared()->channelToString((video_channel_t)channel)<<": Failed to show overlay time";
            emit signalAddOverlayTimeFailed(channel);
        }
    }
}
void OverlayTask::queueRemoveTimer(int channel) {
    if (engine_overlay_hide_time(channel) < 0) {
        qWarning() << StringUtils::shared()->channelToString((video_channel_t)channel)<<": Failed to hide overlay time";
    }
}
void OverlayTask::queueAddOverlayImage(int channel) {
    OverlayImage *image = OverlayManager::shared()->getOverlayImage(channel);
    QFileInfo logoinfo(image->image);
    QString alphafile = "";
    if (!image->alphaFile.isEmpty()) {
        QFileInfo alphainfo(image->alphaFile);
        alphafile = alphainfo.absoluteFilePath();
    }
    if (engine_overlay_add_image(channel, image->width, image->height, image->xOffset, image->yOffset, logoinfo.absoluteFilePath().toLatin1().data(), &image->id, image->alpha, alphafile.isEmpty()?NULL:alphafile.toLatin1().data()) < 0) {
        qWarning() << StringUtils::shared()->channelToString((video_channel_t)channel)<<": Failed to add logo image";
        emit signalAddOverlayImageFailed(channel);
    } else {
        qDebug() << StringUtils::shared()->channelToString((video_channel_t)channel)<<": Logo added";
    }
}

void OverlayTask::queueRemoveOverlayImage(int channel) {
    OverlayImage* object = OverlayManager::shared()->getOverlayImage(channel);
    // Remove overlay image
    if (object->id != -1) {
        if (engine_overlay_remove_image(channel, object->id) < 0) {
            qWarning() << StringUtils::shared()->channelToString((video_channel_t)channel)<<": Failed to remove burnin image "<<object->id;
        } else {
            qDebug() << StringUtils::shared()->channelToString((video_channel_t)channel)<<": Logo removed";
            object->id = -1;
        }
    }
}

void OverlayTask::queueLoadFont(int channel) {
    this->queueRemoveAllOverlayText(channel);
    OverlayFont *font = OverlayManager::shared()->getOverlayFont(channel);
    if (engine_overlay_load_font(channel, (char*)font->fontPath.toStdString().c_str(), font->fontSize) < 0) {
        qWarning() << "Failed to load font";
    } else {
        this->queueSetAllOverlayText(channel);
    }
}

void OverlayTask::queueOverlayImageAlpha(int channel, int index, int alpha) {
    if (engine_overlay_image_alpha(channel, index, alpha) < 0) {
        qWarning() << "Failed to set alpha";
    }
}


void OverlayTask::queueSetPrivacyMask(int channel, int index) {
    PrivacyMask *mask = OverlayManager::shared()->getPrivacyMask(channel, index);
    if (mask->points == NULL) {
        // Convert color rgb444 to yuv444
        QColor color = mask->color;
        int y = (0.257 * color.red()) + (0.504 * color.green()) + (0.098 * color.blue()) + 16;
        int u = -(0.148 * color.red()) - (0.291 * color.green()) + (0.439 * color.blue()) + 128;
        int v = (0.439 * color.red()) - (0.368 * color.green()) - (0.071 * color.blue()) + 128;
        uint32_t yuva = (y << 24) | (u << 16) | (v << 8) | color.alpha();

        if (engine_overlay_privacy_add_mask_rect(channel, mask->xOffset, mask->yOffset, mask->width, mask->height, yuva, index) < 0) {
            qWarning() << StringUtils::shared()->channelToString((video_channel_t)channel)<<": Failed to add privacy mask: "<<index;
            emit signalAddMaskFailed(channel, index);
        } else {
            emit signalAddMaskSuccess(channel, index);
        }
    }
}

void OverlayTask::queueRemovePrivacyMask(int channel, int index) {
    if (engine_overlay_privacy_remove_mask(channel, index) < 0) {
        qWarning() << StringUtils::shared()->channelToString((video_channel_t)channel)<<": Failed to remove privacy mask: "<<index;
    }
}

void OverlayTask::queueUpdatePrivacyMaskColor(int channel, int index) {
    PrivacyMask *mask = OverlayManager::shared()->getPrivacyMask(channel, index);

    // Convert color rgb444 to yuv444
    QColor color = mask->color;
    int y = (0.257 * color.red()) + (0.504 * color.green()) + (0.098 * color.blue()) + 16;
    int u = -(0.148 * color.red()) - (0.291 * color.green()) + (0.439 * color.blue()) + 128;
    int v = (0.439 * color.red()) - (0.368 * color.green()) - (0.071 * color.blue()) + 128;
    uint32_t yuva = (y << 24) | (u << 16) | (v << 8) | color.alpha();

    if (engine_overlay_privacy_update_mask_color(channel, yuva, index) < 0) {
        qWarning() << StringUtils::shared()->channelToString((video_channel_t)channel)<<": Failed to update privacy mask color: "<<index;
    }
}
