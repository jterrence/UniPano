/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef CHANNELSTATE_H
#define CHANNELSTATE_H

#include <QMap>

class ChannelState
{
public:

    static ChannelState *shared() {
        if (!instance) {
            instance = new ChannelState();
        }
        return instance;
    }

    void init(int channel);
    void deinit();

    void setVideoEnabled(int channel, bool enabled);
    bool getVideoEnabled(int channel);
    void setRenderEnabled(int channel, bool enabled);
    bool getRenderEnabled(int channel);
    void setOptimizeEnabled(int channel, bool enabled);
    bool getOptimizeEnabled(int channel);
    void setBufferFullnessEnabled(int channel, bool enabled);
    bool getBufferFullnessEnabled(int channel);
    void setBufferFullnessDuration(int channel, int duration);
    int getBufferFullnessDuration(int channel);

private:
    ChannelState() {}

    static ChannelState *instance;

    bool videoEnabled;
    bool renderEnabled;
    bool optimizeEnabled;
    bool bufferFullnessEnabled;
    int bufferFullnessDuration;

    QMap<int,ChannelState*> mapChannelStates;
};

#endif // CHANNELSTATE_H
