/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include "scrollablewindow.h"
#include "mainwindow.h"
#include "log.h"

ScrollableWindow::ScrollableWindow(QWidget *parent) :
    QScrollArea(parent)
{
    this->setPalette(QColor(45,45,45));
    this->setAttribute( Qt::WA_DeleteOnClose );
    this->setAttribute(Qt::WA_OpaquePaintEvent);

    cbKeepOnTop = NULL;

    this->setAlignment(Qt::AlignCenter);
}

ScrollableWindow::~ScrollableWindow() {
    if (cbKeepOnTop != NULL) {
        delete cbKeepOnTop;
    }
}

void ScrollableWindow::resizeEvent(QResizeEvent *event) {
    QScrollArea::resizeEvent(event);
    emit(signalResized());
}

void ScrollableWindow::keyPressEvent(QKeyEvent *event )
{
    switch(event->key())
    {
        case Qt::Key_F1:
            if (mainWindow != NULL) {
                ((MainWindow*)mainWindow)->setF1KeyPressed();
            } else {
                qWarning() << "No mainwindow reference set";
            }
            break;
        case Qt::Key_Escape:
            this->close();
            break;
        default:
            break;
    }
}

// private slots
void ScrollableWindow::onCheckBoxToggled(bool checked) {
    this->hide();
    if (checked) {
        this->setWindowFlags(this->windowFlags() | Qt::WindowStaysOnTopHint);
    } else {
        this->setWindowFlags(this->windowFlags() & ~Qt::WindowStaysOnTopHint);
    }
    this->show();
}
