/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef CHANNELLAYOUT_H
#define CHANNELLAYOUT_H

#include <QWidget>
#include <QList>

class DisplayChannel;

namespace Ui {
    class ChannelLayout;
}

class ChannelLayout : public QWidget
{
    Q_OBJECT

public:
    explicit ChannelLayout(QWidget *parent = 0);
    ~ChannelLayout();

    void restartAllVideo();
    void stopAllVideo();
    void setF1KeyPressed();
    void propagateKeyPressEvent(QKeyEvent*);
    QList<DisplayChannel*> getChannelList();

private:
    Ui::ChannelLayout *ui;
};

#endif // CHANNELLAYOUT_H
