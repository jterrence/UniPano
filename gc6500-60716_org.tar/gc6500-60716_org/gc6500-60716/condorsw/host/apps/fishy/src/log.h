/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef LOG_H
#define LOG_H

#include <QDebug>
#include <QString>

#ifndef QT5
#define TAG (QString("[").append(__FILE__).append("->").append(__func__).append("():").append(QString::number(__LINE__)).append("]").toStdString().c_str())

#define qDebug() (qDebug()<<TAG)
#define qWarning() (qWarning()<<TAG)
#define qCritical() (qCritical()<<TAG)
#define qFatal(message) (qFatal("%s",QString(TAG).append(" ").append(message).toStdString().c_str()))
#endif

#endif // LOG_H
