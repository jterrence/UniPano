/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef VIDEOSETTINGS_H
#define VIDEOSETTINGS_H

#include <QWidget>
#ifdef WITH_ENGINE
#include "interface.h"
#endif

class OverlayImage;
class OverlayText;

namespace Ui {
    class VideoSettings;
}
enum SupportedResolution    { FHD=0, HD, VGA, WVGA, QVGA, WQVGA };
enum Level                  { L21=0, L22,L30, L31};
enum Quality                { Q80=0, Q90, Q100};

class VideoSettings : public QWidget
{
    Q_OBJECT

public:
    explicit VideoSettings(QWidget *parent, int channel);
    ~VideoSettings();

    void setBitRateSilder(int val,bool enabled=true);
    void setFrameRateSilder(int val,bool enabled=true);
    void setGopSilder(int val,bool enabled=true);


    /**
     * @brief Update the resolution dropdown
     *
     * @param w The new width
     * @param h The new height
     * @param enabled Whether to enable remove the resolution tab
     */
    void setResolution(int w, int h,bool enabled=true);
    void setProfile(int,bool enabled=true);
    void setLevel(int,bool enabled=true);
    void setQuality(int,bool enabled=true);

    void setVideoFormat(video_format_t format);

    bool isVideoRunning(){return videoRunning;}
    void setChannelDetails(int ch, int w, int h);
    void setStopVideoDisplay(bool val);
    void setChType(int val);

    void setComposite(bool composite, int numpanels = 0);
    void setCurrentPanel(int panel);
    void setCurrentPanelMode(int mode);

    void setVideoEnabled(bool enabled);
    void setRenderingEnabled(bool enabled);
    void setBufferFullness(bool enabled, int duration);
    void setOverlayEnabled(bool enabled);
    void setOverlayTextEnabled(bool enabled);
    void setOverlayImageEnabled(bool enabled);
    void setOverlayTimeEnabled(bool enabled);
    void updateOverlayImageConfig(int,int);
    void updateOverlayTextConfig(int,int);
    void updatePrivacyMaskConfig(int,int);

    inline bool hasBeenMoved() {
        return moved;
    }

protected:
    bool eventFilter(QObject *obj, QEvent *event);
    void showEvent(QShowEvent *event);
    void hideEvent(QHideEvent *event);
    void mousePressEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);

private:
    Ui::VideoSettings *ui;
    int channel;
    int chWidth;
    int chHeight;
    int bitRate;
    int frameRate;
    int gop;
    bool videoRunning;
    bool overlayEnabled;
    panel_params_t currentPanelParams;
    bool moved;
    QWidget prevTab;

    void bitSliderValue(int val);
    void resetTabs();
    void populatePanelModes(int mode);

    void removeTab(QWidget*);
    void setTabsForVideoFormat(video_format_t format);

    void enableControls(bool enable);

signals:
    void bitRateSliderMoved(int);
    void frameRateSliderMoved(int);
    void gopSliderMoved(int);
    void signalChannelEnabled(bool);
    void signalRenderToggled(bool);
    void signalBFToggled(bool);
    void signalBufferDurationChanged(int);
    void profileIndexChanged(int);
    void resolutionIndexChanged(int);
    void levelIndexChanged(int);
    void qualitySliderChanged(int);
    void iFrameRequested();
    void signalFormatChanged(int);

    void signalResolutionChanged(int width, int height);

    void signalOnPanelChanged(int panel);
    void signalOnPanelModeChanged(int panel, int mode);

    void signalHighlightCurrentPanel(bool);

    // Overlay
    void signalOverlayImagePathChanged(QString, int, int, int, int, int, QString);
    void signalOverlayImageToggled(bool);
    void signalOverlayTextToggled(bool,int index=-1);
    void signalOverlayTextChanged(int, QString, int, int);
    void signalOverlayTimeToggled(bool);
    void signalOverlayFontChanged(QString,int);
    void signalOverlayImageAlphaChanged(int, int);

    // Overlay Privacy
    void signalPrivacyMaskChanged(int index);

private slots:
    void onChannelEnabled(bool);

    void onSmartMotionToggled(bool);

    void onPanelChanged(int);
    void onPanelModeChanged(int);
    void onFormatChanged(int);
    void onButtonApplyResolutionClicked();

    // Overlay
    void onButtonApplyOverlayImageClicked();
    void onImageXOffsetEditingFinished();
    void onImageYOffsetEditingFinished();
    void onOverlayImageToggled(bool);
    void onButtonApplyOverlayTextClicked();
    void onTextXOffsetEditingFinished();
    void onTextYOffsetEditingFinished();
    void onOverlayTextToggled(bool);
    void onOverlayTextLineChanged(int);
    void onButtonLoadFontClicked();
    void onOverlayLineToggled(bool);
    void onOverlayAlphaValueChanged(int);
    void onAlphaEditingFinished();
    void onButtonAlphaFileClearClicked();

    // Overlay Privacy
    void onButtonApplyPrivacyMaskClicked();

    void onBufferDurationChanged();

    void onResolutionWidthEditingFinished();
    void onResolutionHeightEditingFinished();
};

#endif // CHANNELSETTINGS_H
