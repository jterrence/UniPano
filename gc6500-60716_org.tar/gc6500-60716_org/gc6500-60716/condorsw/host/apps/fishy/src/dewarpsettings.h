/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef DEWARPSETTINGS_H
#define DEWARPSETTINGS_H

#include <QDialog>
#include <QSlider>
#include <QGridLayout>
#include "displaychannel.h"
#include "customslider.h"
#include <QComboBox>
#include "engineapimanager.h"

namespace Ui {
    class DewarpSettings;
}

#define DEWARP_CONFIG_FILE "/tmp/dewarpconfig.cfg"

#if 0
typedef union
{
    int Phi0;
    int Rx;
    int Ry;
    int Rz;
    int Gshift; //in degrees
    int CylinderHeightMicro;    // The unit is Micro, i.e. millionth
    int HPan;   //Horizontal Pan in Pixels
    int VPan;   //Vertical   Pan in Pixels
    int Tilt;   // in degrees
    int Zoom;
    int HPanStart;
    int VPanStart;
    int ZoomStart;
    int TiltStart;
    int HPanInc;
    int VPanInc;
    int TiltInc;
    int ZoomInc;
    int Period; //in the number of maps
    int zoom;
    int radius;
    int xCenter;
    int yCenter;
}__attribute__((packed)) DewarpParams;
#endif
#define MAX_SETTING_ELEMENTS 10

class DewarpSettings : public QDialog
{
    Q_OBJECT

public:
    explicit DewarpSettings(QWidget *parent);
    ~DewarpSettings();
    void showEvent(QShowEvent *event);
    void keyPressEvent(QKeyEvent *);

private:
    //functions
    void populateUIDewarpModesDropdown();
    void populateUIPanelsDropdown(int num);

    /**
     * @brief Get the dewarp mode (@dewarp_mode_t) and params (@dewarp_params_t) for a channel (if not composited) or a panel of a composited channel
     *
     * @param channel   The video channel
     * @param panel     The panel (if the video channel is composited). Default is 0
     */
    //bool getDewarpParams(int channel, int panel);

    void generateUIForDewarpMode(dewarp_mode_t);
    void generateSettingsLayout(int);

    QString dewarpModeToString(dewarp_mode_t mode);

    void cleanUpUiLayout();
    //int setDewarpParams(int ch,int panel,dewarp_mode_t dmode_t,dewarp_params_t* params);
    int getModeValueFromIndex(dewarp_mode_t selectedMode);
    void setUpMagnifyUiView();
    void setUpZclCylinderUiView();
    void setUpZclStretchUiView();
    void setUp1PanelEPZUiView();
    void setUpZclUiView();
    void setUpSweepUiView();
    void setUpDefaultUiView();

    int getNumSettingsForDewarpMode(dewarp_mode_t);

    /**
     * @brief Draw a dewarp grid map for a dewarp mode
     *
     */
    void DrawDewarpMap();


    /**
     * @brief Update the dewarp mode and dewarp params
     *
     */
    void updateDewarpParams();

    //slots
private slots:

    void onChannelChanged(int channel);
    void onZoomChanged(int);
    void onDewarpModeChanged(int);
    void onPanelChanged(int);
    void onSliderValueChanged(int);

    // EngineApiHandler slots
    void onDewarpGetSuccess(video_channel_t channel, int panel, dewarp_mode_t mode, dewarp_params_t params);
    void onDewarpGetFailed(video_channel_t channel, int panel);
    void onDewarpSetSuccess(video_channel_t channel, int panel, dewarp_mode_t mode, dewarp_params_t params);
    void onDewarpSetFailed(video_channel_t channel, int panel);

    void onFinished() {
        this->done(1);
    }

    void onVideoStarted(video_channel_t channel);

private:

    void setComposition(bool enabled);
    void setDewarp(bool enabled);

    CustomSlider *uiSlider[MAX_SETTING_ELEMENTS];
    QLabel *uiLabel[MAX_SETTING_ELEMENTS];

    QGridLayout *uiGridLayout;
    int defaultParamsStructure[MAX_SETTING_ELEMENTS];
    int uiParamsStructure[MAX_SETTING_ELEMENTS];
    Ui::DewarpSettings *ui;

    int oldResW;
    int oldResH;

    int mNumChannels;

    // Array of channels with dewarp
    // Will contain the actual channel ids of the channels listed in the dropdown
    int *channelsWithDewarp;
    int currentChannel;

    EngineApiManager *apiManager;

    QString mapPath;
};

#endif // DEWARPSETTINGS_H
