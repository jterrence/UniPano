/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef OVERLAYMANAGER_H
#define OVERLAYMANAGER_H

#include <QMap>
#include <QList>
#include <QCoreApplication>

#define MAX_OVERLAY_TEXTS 4
const QString DEFAULT_OVERLAY_TEXTS[MAX_OVERLAY_TEXTS] = {"GeoSemi","Overlay","Burn-in","txtdemo"};
#define DEFAULT_OVERLAY_FONT_FILE "assets/fonts/MGSysFont32.bin"
#define DEFAULT_OVERLAY_FONT_SIZE 32
#define DEFAULT_OVERLAY_IMAGE "assets/logo/geo-logo_432x200.yuv"
#define DEFAULT_OVERLAY_IMAGE_ALPHA "" //"assets/logo/alpha-geo-logo_432x200.yuv"
#define DEFAULT_OVERLAY_IMAGE_WIDTH 432
#define DEFAULT_OVERLAY_IMAGE_HEIGHT 200
#define DEFAULT_MASK_COLOR 0xFF303030 //0xFF000000
#define DEFAULT_IMAGE_ALPHA 255

// default paths
#define DEFAULT_LOGO_PATH "assets/logo"
#define DEFAULT_FONT_PATH "assets/fonts"

class OverlayText;
class OverlayImage;
class OverlayTime;
class OverlayFont;
class PrivacyMask;

typedef QList<OverlayText*> tlist;
typedef QList<PrivacyMask*> plist;

/**
 * @brief OverlayManager class
 *
 * Manages the overlay images, texts and timers for each channel
 *
 */
class OverlayManager
{
public:

    static OverlayManager *shared() {
        if (!s_instance) {
            s_instance = new OverlayManager();
        }
        return s_instance;
    }

    /**
     * @brief Initialize the overlay manager
     *
     * @param numOverlayText       The number of overlay texts
     * @param numPrivMasks         Max number of privacy masks
     */
    void init(int numOverlayText, int numPrivMasks);

    /**
     * @brief Initialze the overlay text objects for a channel
     *
     * @param channel   The video channel
     */
    void initOverlayTexts(int channel);

    /**
     * @brief Initialze the privacy mask objects for a channel
     *
     * @param channel   The video channel
     */
    void initPrivacyMasks(int channel);

    /**     * @brief Initialze the overlay font (@OverlayFont) object for a channel
     *
     * @param channel
     */
    void initOverlayFont(int channel);

    /**
     * @brief Return the number of overlay texts per channel
     *
     * @return int  The number of overlay texts per channel
     */
    int getNumOverlayText();

    /**
     * @brief Check if overlay text is enabled for a channel
     *
     * @param channel   The video channel
     * @return bool     True if enabled, False otherwise
     */
    bool isOverlayTextEnabled(int channel);

    /**
     * @brief Set whether overlay text is enabled/disabled for a channel
     *
     * @param channel   The video channel
     * @param enabled   boolean. enable if true, else disable
     */
    void setOverlayTextEnabled(int channel, bool enabled, int index=-1);

    /**
     * @brief Calculate the default overlay text x and y offsets for a channel
     *
     * @param channel       The video channel
     * @param channelWidth  The channel width
     * @param channelHeight The channel height
     */
    void calculateDefaultTextOffsets(int channel, int channelWidth, int channelHeight);

    /**
     * @brief Calculate the x and y offsets for an overlay text on a channel
     *
     * @param channel       The video channel
     * @param index         The overlay text index
     * @param channelWidth  The channel width
     * @param channelHeight The channel height
     */
    void calculateTextOffset(int channel, int index, int channelWidth, int channelHeight);

    /**
     * @brief Get the overlay font (@OverlayFont) object of a channel
     *
     * @param channel
     * @return OverlayFont *
     */
    OverlayFont* getOverlayFont(int channel);

    /**
     * @brief Get the overlay text (@OverlayText) object of a channel at a particular index
     *
     * @param channel       The video channel
     * @param index         The overlay text index
     * @return OverlayText* The overlay text (@OverlayText) object reference
     */
    OverlayText* getOverlayText(int channel, int index=0);

    // Overlay image

    /**
     * @brief Initialize the overlay image (@OverlayImage) objects for a channel
     *
     * @param channel       The video channel
     */
    void initOverlayImage(int channel);

    /**
     * @brief Calculate the default overlay image (@OverlayImage) x and y offsets for a channel
     *
     * @param channel       The video channel
     * @param channelWidth  The channel width
     * @param channelHeight The channel height
     */
    void calculateDefaultImageOffsets(int channel, int channelWidth, int channelHeight);

    /**
     * @brief Check whether overlay image is enabled for a channel
     *
     * @param channel   The video channel
     * @return bool     True if enabled, False otherwise
     */
    bool isOverlayImageEnabled(int channel);

    /**
     * @brief Set whether overlay image is enabled for a channel
     *
     * @param channel   The video channel
     * @param enabled   Boolean. Enabled if true, disabled if false.
     */
    void setOverlayImageEnabled(int channel, bool enabled);

    /**
     * @brief Get the overlay image (@OverlayImage) object for a channel
     *
     * @param channel           The video channel
     * @return OverlayImage*    The overlay image object reference
     */
    OverlayImage* getOverlayImage(int channel);

    // Overlay time

    /**
     * @brief Initialize the overlay time object (@OverlayTime) for a channel
     *
     * @param channel   The video channel
     */
    void initOverlayTime(int channel);

    /**
     * @brief Calculate the default overlay time offsets for a channel
     *
     * @param channel       The video channel
     * @param channelWidth  The channel width
     * @param channelHeight The channel height
     */
    void calculateDefaultTimeOffsets(int channel, int channelWidth, int channelHeight);

    /**
     * @brief Check whether overlay time is enabled for a channel
     *
     * @param channel   The video channel
     * @return bool     True if enabled, False otherwise
     */
    bool isOverlayTimeEnabled(int channel);

    /**
     * @brief Set whether overlay time should be enabled/disabled for a channel
     *
     * @param channel   The video channel
     * @param enabled   Boolean. Enabled if true, Disabled if false.
     */
    void setOverlayTimeEnabled(int channel, bool enabled);

    /**
     * @brief Get the overlay time object (@OverlayTime) for a channel
     *
     * @param channel       The video channel
     * @param updateTime    boolean. If true, update the time to current and return. Default is false.
     * @return OverlayTime* The overlay time (@OverlayTime) object reference
     */
    OverlayTime* getOverlayTime(int channel, bool updateTime=false);

    /**
     * @brief Return the number of privacy masks
     *
     * @return int  The number of privacy masks
     */
    int getNumPrivMasks();

    /**
     * @brief Check if privacy masks are enabled for a channel
     *
     * @param channel   The video channel
     * @return bool     True if enabled, False otherwise
     */
    bool isPrivacyMaskEnabled(int channel);

    /**
     * @brief Set whether privacy mask is enabled/disabled for a channel at an index
     *
     * @param channel   The video channel
     * @param enabled   boolean. enable if true, else disable
     */
    void setPrivacyMaskEnabled(int channel, bool enabled, int index=-1);

    /**
     * @brief Get the privacy mask (@PrivacyMask) object of a channel at a particular index
     *
     * @param channel       The video channel
     * @param index         The privacy mask index
     * @return PrivacyMask* The privacy mask (@PrivacyMask) object reference
     */
    PrivacyMask* getPrivacyMask(int channel, int index=0);

    /**
     * @brief Clean up all the overlay objects
     *
     */
    void cleanup();

    void setFontLoaded(int channel, bool loaded);
    bool isFontLoaded(int channel);
    void setOverlayTextSetupDone(int channel, bool done);
    bool isOverlayTextSetupDone(int channel);

private:
    static OverlayManager *s_instance;

    // private constructor
    OverlayManager():numOverlayText(1),numPrivMasks(4),fontLoaded(false) {
    }

    QMap<int, OverlayImage*> mapOverlayImage;
    QMap<int, bool> mapOverlayImageEnabled;

    QMap<int, OverlayFont*> mapOverlayFont;

    QMap<int, tlist> mapOverlayText;
    QMap<int, bool> mapOverlayTextEnabled;
    QMap<int, bool> mapOverlayTextSetupDone;
    QMap<int, plist> mapPrivacyMask;
    QMap<int, bool> mapPrivacyMaskEnabled;

    QMap<int, OverlayTime*> mapOverlayTime;
    QMap<int, bool> mapOverlayTimeEnabled;

    int numOverlayText;
    int numPrivMasks;
    bool fontLoaded;
};

#endif // OVERLAYMANAGER_H
