/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef DRAGGABLEFRAME_H
#define DRAGGABLEFRAME_H

#include <QImage>
#include <QFrame>
#include <QWidget>
#include <QMouseEvent>
#include <QLabel>

class DraggableFrame : public QFrame
{
    Q_OBJECT
public:
    explicit DraggableFrame(QWidget *parent, int index);
    ~DraggableFrame();

    void setDragLimit(QRect rect) {
        this->rectLimit = rect;
    }

    void setOriginalGeometry(QRect rect) {
        this->originalGeometry = rect;
    }

    void reset() {
        setGeometry(originalGeometry);
    }

    void select();
    void deselect();
    void dirty();

    void moveX(int xDiff);
    void moveY(int yDiff);

    void keyPressEvent(QKeyEvent *);

protected:
    void mousePressEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    void resizeEvent(QResizeEvent *);

signals:
    void signalMoved(int,int,int);

public slots:

private:
    QRect rectLimit;
    int index;
    QLabel *labelNumber;
    QRect originalGeometry;
    bool moved;

};

#endif // DRAGGABLEFRAME_H
