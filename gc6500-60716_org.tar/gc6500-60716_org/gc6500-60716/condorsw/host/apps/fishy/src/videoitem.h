/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef VIDEOITEM_H
#define VIDEOITEM_H

#include <QAbstractVideoSurface>
#ifdef QT5
#include <QWidget>
#include <QGraphicsItem>
#else
#include <QtGui/QGraphicsItem>
#endif
#include <QMutex>
#include <QTimer>
#include <QFont>
#include "stringutils.h"

#ifndef INT64_C
#define INT64_C(c) (c ## LL)
#define UINT64_C(c) (c ## ULL)
#endif
extern "C" {
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
}

// Thread to create green border image
class ImageDrawer : public QObject {
    Q_OBJECT

private:
    QSize imageSize;
    QRect borderRect;
    QList<QPoint> points;
    unsigned *buffer;

signals:
    void signalMapDrawn(QImage);
    void signalGreenBorderDrawn(QImage);
    void signalRectangleDrawn(QImage, int);
    void signalRgbImageReady(QImage);

public:
    ImageDrawer() {
        borderRect = QRect(0, 0, 0, 0);
        imageSize = QSize(-1, -1);
        buffer = NULL;
    }

    void setImageSize(QSize imageSize) {
        this->imageSize = imageSize;
    }

    void setBorderRect(QRect borderRect) {
        this->borderRect = borderRect;
    }

    QRect getBorderRect() {
        return this->borderRect;
    }

    void setPoints(QList<QPoint> points) {
        this->points = points;
    }

public slots:
    void drawGreenBorderImage();
    void drawMap();
    void drawMask(QRect rect, int index);
};

class VideoItem
    : public QAbstractVideoSurface,
      public QGraphicsItem
{
    Q_OBJECT
    //Q_INTERFACES(QGraphicsItem)
public:
    explicit VideoItem(QGraphicsItem *parentItem = 0);
    ~VideoItem();

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

    //video surface
    QList<QVideoFrame::PixelFormat> supportedPixelFormats(
            QAbstractVideoBuffer::HandleType handleType = QAbstractVideoBuffer::NoHandle) const;

    bool start(const QVideoSurfaceFormat &format);
    void stop();
    bool present(const QVideoFrame &frame);
    bool isActive(){ return QAbstractVideoSurface::isActive();}
    void setStatsEnabled(bool val){bStatsEnabled = val;}
    void setVideoFormat(video_format_t format){
        this->format = StringUtils::shared()->videoFormatToString(format);
        switch (format) {
            case VID_FORMAT_GREY_RAW:
                pixelFormat = PIX_FMT_GRAY8;
                break;
            case VID_FORMAT_NV12_RAW:
                pixelFormat = PIX_FMT_NV12;
                break;
            case VID_FORMAT_YUY2_RAW:
                pixelFormat = PIX_FMT_YUYV422;
                break;
            default:
                pixelFormat = PIX_FMT_YUV420P;
                break;
        }
    }
    void setGop(int val){gop = val;}
    void setResolution(QString val){res = val;}
    void setProfile(QString val){profile = val;}
    void setQuality(int val){quality = val;}
    void setImage(QImage image);
    void renderYuv();
    void renderStats();

    void setChannel(int ch) {
        channel = ch;
        channelStr=StringUtils::shared()->channelToString((video_channel_t)ch);
    }
    void setDrawTitle(bool drawTitle) {
        this->bDrawTitle = drawTitle;
    }

    void setJpegChannel(bool val){isJpegChannel = val;}
    void setRawChannel(bool val){
        isRawChannel = val;
    }
    void setRectCount(int rect);
    void setRect(int index,int x,int y,int w,int h){ rect[index] = QRect(x,y,w,h);}
    void setFrameSize(int w,int h){viewWidth= w;viewHeight=h;}
    void setImageSize(int w,int h){imageSize.setWidth(w);imageSize.setHeight(h);}
    int getviewWidth(){return viewWidth;}
    int getviewHeight(){return viewHeight;}
    bool getStatsEnabled(){return bStatsEnabled;}
    void setdrawMapEnable(bool val){ drawMap = val; }
    void setRender(bool val) {
        render = val;
    }
    void setConvertYuvToRgb(bool val) {
        convertYuvToRgb = val;
    }

    void setSmoothScaling(int val){smoothScaling = val;}

    void setRectToFocus(QRect rect);
    void setPoints(QList<QPoint> points);

    void setStats(Stats *stats);

    void initMasks(int num);
    void drawMask(QRect rect, int index);
    void removeMask(int index);

    void *data;
    int decodedWidth, decodedHeight;

    int yStatsPos;
    int hStatsPos;
    int timeBitrateMinMax;
    int bufferFullnessEnabled;

private:
    //QVideoFrame currentFrame;

    bool smoothScaling;
    bool drawMap;
    QImage dewarpMap;
    QImage::Format imageFormat;
    QSize imageSize;

    int numFramesDropped;
    int numFramesCaptured;
    int numFramesDelay;
    double duration;

    int numFramesRendered;
    int tempNumFramesRendered;

    int iFrameCount;
    int oldIFrameCount;
    int viewWidth;
    int viewHeight;

    int bps;
    int bps_min;
    int bps_max;
    int fps;
    int gop;
    int continuity;
    QString format;
    QString res;
    QString profile;
    int quality;
    int channel;
    QString channelStr;

    bool bDrawTitle;
    bool bStatsEnabled;
    bool isJpegChannel;
    bool isRawChannel;
    PixelFormat pixelFormat;
    int noOfRect;
    bool render;
    bool convertYuvToRgb;
    QRect rect[50];
    Qt::GlobalColor rectColor[50];
    QMutex *lockFrame;
    QMutex *lockStats;
    QTimer *statsTimer;

    QThread *drawThread;
    ImageDrawer *drawObject;
    QImage greenBorder;

    QList<QPoint> points;

    QFont normal, bold;
    QImage currentImage;
    QPixmap pixmap;
    bool userSentUpdate;

    QList<QImage> masks;

    uint8_t* buffer;
    QImage rgbImage;
    struct SwsContext *swsContext;
    uint8_t* dest_planes[3];
    int dest_stride[3];
    int src_stride[3];
    uint8_t* src_planes[3];
    AVFrame* avSrcFrame;

private slots:
    void onGreenBorderDrawn(QImage);
    void onRectangleDrawn(QImage, int);
    void onMapDrawn(QImage);
    void onRgbImageReady(QImage);
signals:
    void updateView();
    void signalPlotGraph();
};

#endif // VIDEOITEM_H
