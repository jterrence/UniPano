/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include <QPainter>
#include <QMessageBox>
#include "camerasettings.h"
#include "ui_camerasettings.h"
#ifdef WITH_ENGINE
#include "interface.h"
#endif
#include "log.h"
#include "alerthandler.h"
#include <QPushButton>

CameraSettings::CameraSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CameraSettings)
{
    ui->setupUi(this);

    // Set slider ranges and label width
    ui->sliderBrightness->setRange(-255, 255);
    ui->sliderBrightness->setLabelWidth(45);

    ui->sliderContrast->setRange(0, 200);
    ui->sliderContrast->setLabelWidth(45);

    ui->sliderSharpness->setRange(0, 255);
    ui->sliderSharpness->setLabelWidth(45);

    ui->sliderHue->setRange(-18000, 18000);
    ui->sliderHue->setLabelWidth(45);

    ui->sliderSaturation->setRange(0, 200);
    ui->sliderSaturation->setLabelWidth(45);

    ui->sliderGama->setRange(100, 300);
    ui->sliderGama->setLabelWidth(45);

    ui->sliderAnalogGain->setRange(0, 15);
    ui->sliderAnalogGain->setPageStep(1);
    ui->sliderAnalogGain->setLabelWidth(25);

    ui->sliderSharpenFilter->setRange(0, 2);
    ui->sliderSharpenFilter->setLabelWidth(25);
    ui->sliderSharpenFilter->setPageStep(1);

    ui->sliderExposureGainMultiplier->setRange(0, 256);
    ui->sliderExposureGainMultiplier->setLabelWidth(25);

    ui->sliderWhiteBalanceTemp->setRange(2800, 6500);
    ui->sliderWhiteBalanceTemp->setLabelWidth(40);

    ui->sliderWhiteBalanceZone->setRange(0, 63);
    ui->sliderWhiteBalanceZone->setLabelWidth(20);

    ui->sliderWdrStrength->setRange(0, 255);
    ui->sliderWdrStrength->setLabelWidth(25);

    ui->sliderExposureTime->setRange(0, 255);
    ui->sliderExposureTime->setLabelWidth(25);

    ui->sliderExposureZone->setRange(0, 62);
    ui->sliderExposureZone->setLabelWidth(20);

    ui->sliderNoiseFilterStrength->setRange(0, 100);
    ui->sliderNoiseFilterStrength->setLabelWidth(25);

    QPalette pal = palette();
    QColor color = pal.light().color();
    color.setAlpha(240);
    pal.setBrush(QPalette::Window, color);
    setPalette(pal);

    setAutoFillBackground(true);

    setWindowTitle(tr("Camera Settings"));
    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(close()));
    ui->buttonBox->button(QDialogButtonBox::Ok)->setText(tr("Done"));
}

CameraSettings::~CameraSettings()
{
    qDebug() << "Destroyed";
    delete ui;
}

void CameraSettings::keyPressEvent(QKeyEvent *event) {
    switch(event->key()) {
        case Qt::Key_Escape:
            this->close();
            break;
        case Qt::Key_Enter:
            event->accept();
            break;
    }
}

void CameraSettings::showEvent(QShowEvent *)
{
//    // ISP
//    disconnect(ui->sliderBrightness,SIGNAL(signalValueChanged(int)),this,SLOT(brightnessChanged(int)));
//    disconnect(ui->sliderContrast,SIGNAL(signalValueChanged(int)),this,SLOT(contrastChanged(int)));
//    disconnect(ui->sliderGama,SIGNAL(signalValueChanged(int)),this,SLOT(gamaChanged(int)));
//    disconnect(ui->sliderHue,SIGNAL(signalValueChanged(int)),this,SLOT(hueChanged(int)));
//    disconnect(ui->sliderSaturation,SIGNAL(signalValueChanged(int)),this,SLOT(saturationChanged(int)));
//    disconnect(ui->sliderSharpness,SIGNAL(signalValueChanged(int)),this,SLOT(sharpnessChanged(int)));

//    // PU Extension
//    disconnect(ui->cbVerticalFlip,SIGNAL(clicked()),this,SLOT(verticalFlipChanged()));
//    disconnect(ui->cbHorizontalFlip,SIGNAL(clicked()),this,SLOT(horizontalFlipChanged()));
//    disconnect(ui->cbHistogram,SIGNAL(clicked()),this,SLOT(histogramEqualizationChanged()));
//    disconnect(ui->sliderAnalogGain,SIGNAL(signalValueChanged(int)),this,SLOT(maxAnalogGainChanged(int)));
//    disconnect(ui->sliderSharpenFilter,SIGNAL(signalValueChanged(int)),this,SLOT(sharpenFilterChanged(int)));
//    disconnect(ui->sliderExposureGainMultiplier,SIGNAL(signalValueChanged(int)),this,SLOT(exposureGainMultiplierChanged(int)));

//    // White Balance
//    disconnect(ui->awb,SIGNAL(clicked(bool)),this,SLOT(wbChanged(bool)));
//    disconnect(ui->sliderBalanceTemp,SIGNAL(signalValueChanged(int)),this,SLOT(whiteBalanceTemperatureChanged(int)));
//    disconnect(ui->sliderBalanceZone,SIGNAL(signalValueChanged(int)),this,SLOT(autowhiteBalanceZoneChanged(int)));

//    // Wide Dynamic Range
//    disconnect(ui->awdr,SIGNAL(clicked(bool)),this,SLOT(autoWdrChanged(bool)));
//    disconnect(ui->sliderWdrStrength,SIGNAL(signalValueChanged(int)),this,SLOT(wdrChanged(int)));

//    // Exposure
//    disconnect(ui->gbox_exp_mode,SIGNAL(clicked(bool)),this,SLOT(exposureModeChanged(bool)));
//    disconnect(ui->sliderExposureTime,SIGNAL(signalValueChanged(int)),this,SLOT(exposureTimeChanged(int)));
//    disconnect(ui->sliderExposureZone,SIGNAL(signalValueChanged(int)),this,SLOT(sliderExposureZoneChanged(int)));

//    // Noise filter
//    disconnect(ui->gbox_noise_filter,SIGNAL(clicked(bool)),this,SLOT(onNoiseFilterModeChanged(bool)));
//    disconnect(ui->sliderNoiseFilterStrength,SIGNAL(signalValueChanged(int)),this,SLOT(onNoiseFilterStrengthChanged(int)));

    // Get all the UI values
    getValues();

    // ISP
    connect(ui->sliderBrightness,SIGNAL(signalValueChanged(int)),this,SLOT(brightnessChanged(int)));
    connect(ui->sliderContrast,SIGNAL(signalValueChanged(int)),this,SLOT(contrastChanged(int)));
    connect(ui->sliderGama,SIGNAL(signalValueChanged(int)),this,SLOT(gamaChanged(int)));
    connect(ui->sliderHue,SIGNAL(signalValueChanged(int)),this,SLOT(hueChanged(int)));
    connect(ui->sliderSaturation,SIGNAL(signalValueChanged(int)),this,SLOT(saturationChanged(int)));
    connect(ui->sliderSharpness,SIGNAL(signalValueChanged(int)),this,SLOT(sharpnessChanged(int)));

    // PU Extension
    connect(ui->cbVerticalFlip,SIGNAL(clicked()),this,SLOT(verticalFlipChanged()));
    connect(ui->cbHorizontalFlip,SIGNAL(clicked()),this,SLOT(horizontalFlipChanged()));
    connect(ui->cbHistogram,SIGNAL(clicked()),this,SLOT(histogramEqualizationChanged()));
    connect(ui->sliderAnalogGain,SIGNAL(signalValueChanged(int)),this,SLOT(maxAnalogGainChanged(int)));
    connect(ui->sliderSharpenFilter,SIGNAL(signalValueChanged(int)),this,SLOT(sharpenFilterChanged(int)));
    connect(ui->sliderExposureGainMultiplier,SIGNAL(signalValueChanged(int)),this,SLOT(exposureGainMultiplierChanged(int)));

    // White Balance
    connect(ui->awb,SIGNAL(clicked(bool)),this,SLOT(onWhiteBalanceModeChanged(bool)));
    connect(ui->sliderWhiteBalanceTemp,SIGNAL(signalValueChanged(int)),this,SLOT(onWhiteBalanceTemperatureChanged(int)));

    // White Balance Zone
    connect(ui->gbox_wbz, SIGNAL(clicked(bool)), this, SLOT(onWhiteBalanceZoneEnabledOrDisabled(bool)));
    connect(ui->sliderWhiteBalanceZone,SIGNAL(signalValueChanged(int)),this,SLOT(onWhiteBalanceZoneValueChanged(int)));

    // Wide Dynamic Range
    connect(ui->awdr,SIGNAL(clicked(bool)),this,SLOT(onWdrModeChanged(bool)));
    connect(ui->sliderWdrStrength,SIGNAL(signalValueChanged(int)),this,SLOT(onWdrStrengthChanged(int)));

    // Exposure
#if 0 //Disbaled as there is no MXUVC support for this fetaure
    connect(ui->gbox_exp_mode,SIGNAL(clicked(bool)),this,SLOT(onExposureModeChanged(bool)));
    connect(ui->sliderExposureTime,SIGNAL(signalValueChanged(int)),this,SLOT(onExposureTimeChanged(int)));
#endif
    // Zonal Exposure
    connect(ui->gbox_exposure_zone, SIGNAL(clicked(bool)), this, SLOT(onZonalExposureEnabledOrDisabled(bool)));
    connect(ui->sliderExposureZone,SIGNAL(signalValueChanged(int)),this,SLOT(onExposureZoneValueChanged(int)));

    // Noise filter
    connect(ui->gbox_noise_filter,SIGNAL(clicked(bool)),this,SLOT(onNoiseFilterModeChanged(bool)));
    connect(ui->sliderNoiseFilterStrength,SIGNAL(signalValueChanged(int)),this,SLOT(onNoiseFilterStrengthChanged(int)));
}

void CameraSettings::getValues()
{
#ifdef WITH_ENGINE
    int response = -1;
    int val = 0;
    uint32_t uint32val = 0;
    int32_t int32val = 0;

    response = engine_get_brightness(&int32val);
    if(response)
    {
        settingFailedResponse("get","brightness");
        qWarning()<<"Get on brightness failed";
    }
    else
    {
        qDebug()<<"Brightness:"<<int32val ;
        ui->sliderBrightness->setValue((int)int32val);
    }

    response = engine_get_contrast(&uint32val);
    if(response)
    {
        settingFailedResponse("get","contrast");
        qWarning()<<"Get on contrast failed";
    }
    else
    {
        qDebug()<<"Contrast:"<<uint32val;
        ui->sliderContrast->setValue((int)uint32val);
    }

    uint32val = 0;
    response = engine_get_sharpness(&uint32val);
    if(response)
    {
        settingFailedResponse("get","sharpness");
        qWarning()<<"Get on sharpness failed";
    }
    else
    {
        qDebug()<<"Sharpness:"<<uint32val;
        ui->sliderSharpness->setValue((int)uint32val);
    }

    int32val = 0;
    response = engine_get_hue(&int32val);
    if(response)
    {
        settingFailedResponse("get","hue");
        qWarning()<<"Get on hue failed";
    }
    else
    {
        qDebug()<<"Hue:"<<int32val;
        ui->sliderHue->setValue((int)int32val);
    }

    uint32val = 0;
    response = engine_get_saturation(&uint32val);
    if(response)
    {
        settingFailedResponse("get","saturation");
        qWarning()<<"Get on saturation failed";
    }
    else
    {
        qDebug()<<"Saturation"<<uint32val ;
        ui->sliderSaturation->setValue((int)uint32val);
    }

    uint32val = 0;
    response = engine_get_gama(&uint32val);
    if(response)
    {
        settingFailedResponse("get","gama");
        qWarning()<<"Get on gama failed";
    }
    else
    {
        qDebug()<<"Gama:"<<uint32val ;
        ui->sliderGama->setValue((int)uint32val);
    }

    val = 0;
    response = engine_get_flip_horizontal(&val);
    if(response)
    {
        settingFailedResponse("get","horizontal flip");
        qWarning()<<"Get on horizontal flip failed";
    }
    else
    {
        qDebug()<<"H Flip:"<<val ;
        ui->cbHorizontalFlip->setChecked(val);
    }

    val = 0;
    response = engine_get_flip_vertical(&val);
    if(response)
    {
        settingFailedResponse("get","vertical flip");
        qWarning()<<"Get on vertical flip failed";
    }
    else
    {
        qDebug()<<"V Flip:"<<val ;
        ui->cbVerticalFlip->setChecked(val);
    }

    val = 0;
    response = engine_get_histogram_eq(&val);
    if(response)
    {
        settingFailedResponse("get","histogram equalization");
        qWarning()<<"Get on histogram equalization failed";
    }
    else
    {
        qDebug()<<"Histogram eq:"<<val ;
        ui->cbHistogram->setChecked(val);
    }

    uint32val = 0;
    response = engine_get_max_analog_gain(&uint32val);
    if(response)
    {
        settingFailedResponse("get","max analog gain");
        qWarning()<<"Get on max analog gain failed";
    }
    else
    {
        qDebug()<<"Analog Gain:"<<uint32val ;
        ui->sliderAnalogGain->setValue((int)uint32val);
    }

    uint32val = 0;
    response = engine_get_sharpen_filter(&uint32val);
    if(response)
    {
        settingFailedResponse("get","sharpen filter level");
        qWarning()<<"Get on sharpen filter failed";
    }
    else
    {
        qDebug()<<"Sharpen Filter:"<<uint32val ;
        ui->sliderSharpenFilter->setValue((int)uint32val);
    }

    uint32val = 0;
    response = engine_get_gain_multiplier(&uint32val);
    if(response)
    {
        settingFailedResponse("get","gain multiplier");
        qWarning()<<"Get on max analog gain failed";
    }
    else
    {
        qDebug()<<"Exposure gain multiplier:"<<uint32val ;
        ui->sliderExposureGainMultiplier->setValue((int)uint32val);
    }

    white_balance_mode_t wbMode = WB_MODE_AUTO;
    uint32val = 0;
    response = engine_get_wb(&wbMode,&uint32val);
    if(response) {
        settingFailedResponse("get","white balance temperature");
        qWarning()<<"Get on white balance failed";
    } else {
        qDebug() << "White Balance Mode:" << ((wbMode==WB_MODE_MANUAL)?"WB_MODE_MANUAL":"WB_MODE_AUTO");
        ui->awb->setChecked(wbMode == WB_MODE_MANUAL);
        if (wbMode == WB_MODE_MANUAL) {
            ui->awb->setTitle("White Balance (Manual)");
            ui->sliderWhiteBalanceTemp->setValue((int)uint32val);
        } else {
            ui->awb->setTitle("White Balance (Auto)");
            ui->sliderWhiteBalanceTemp->setValue(5000);
        }
    }

    zone_wb_set_t zoneMode = ZONE_WB_DISABLE;
    uint32val = 0;
    response = engine_get_zone_wb(&zoneMode,&uint32val);
    if(response) {
        settingFailedResponse("get","zone white balance");
    } else {
        qDebug()<<"White balance Zone:"<< ((zoneMode == ZONE_WB_ENABLE)?"ZONE_WB_ENABLE":"ZONE_WB_DISABLE");
        ui->gbox_wbz->setChecked(zoneMode == ZONE_WB_ENABLE);
        ui->sliderWhiteBalanceZone->setValue((int)uint32val);
    }

    wdr_mode_t wdrMode = WDR_DISABLE;
    uint32val = 0;
    response = engine_get_wdr(&wdrMode,&uint32val);
    if(response) {
        settingFailedResponse("get","wide dynamic range");
    } else {
        if (wdrMode == WDR_DISABLE) {
            ui->awdr->setEnabled(false);
        } else {
            ui->awdr->setChecked(wdrMode == WDR_MANUAL);
            if (wdrMode == WDR_MANUAL) {
                ui->awdr->setTitle("Wide Dynamic Range (Manual)");
                qDebug()<<"WDR:"<<uint32val ;
                ui->sliderWdrStrength->setValue((int)uint32val);
            } else if (wdrMode == WDR_AUTO) {
                ui->awdr->setTitle("Wide Dynamic Range (Auto)");
                ui->sliderWdrStrength->setValue(100);
            }
        }
    }

#if 0 //Disbaled as there is no MXUVC support for this fetaure
    exp_set_t expMode = EXP_AUTO;
    uint32val = 0;
    response = engine_get_exposure(&expMode,&uint32val);
    if(response) {
        settingFailedResponse("get","exposure mode");
    } else {
        qDebug()<<"Get on exposure. value is "<<uint32val;
        ui->gbox_exp_mode->setChecked(expMode == EXP_MANUAL);
        if (expMode == EXP_MANUAL) {
            ui->gbox_exp_mode->setTitle("Exposure (Manual)");
            ui->sliderExposureTime->setValue((int)uint32val);
            readExposureZone = false;

            zone_exp_set_t zoneExpMode = ZONE_EXP_DISABLE;
            val = 0;
            response = engine_get_zone_exp(&zoneExpMode,&uint32val);
            if(response) {
                settingFailedResponse("get","zone exposure");
            } else {
                qDebug()<<"Zone Exposure:"<<uint32val ;
                ui->gbox_exposure_zone->setChecked(zoneExpMode == ZONE_EXP_ENABLE);
                ui->sliderExposureZone->setValue((int)uint32val);
            }
        } else {
            ui->gbox_exp_mode->setTitle("Exposure (Auto)");

            // No exposure zone available. Read it later once exposure mode is changed to manual
            readExposureZone = true;
            ui->sliderExposureTime->setValue(100);
            ui->sliderExposureZone->setValue(0);
        }
    }
#endif

    noise_filter_mode_t nfMode = NF_MODE_AUTO;
    uint32val = 0;
    response = engine_get_nf(&nfMode,&uint32val);
    if(response) {
        settingFailedResponse("get","noise filter");
    } else {
        if (nfMode == NF_MODE_MANUAL) {
            ui->gbox_noise_filter->setTitle("Noise Filter (Manual)");
        } else {
            ui->gbox_noise_filter->setTitle("Noise Filter (Auto)");
        }
        qDebug()<<"Noise Filter:"<<uint32val;
        ui->gbox_noise_filter->setChecked(nfMode == NF_MODE_MANUAL);
        if (nfMode == NF_MODE_MANUAL) {
            ui->sliderNoiseFilterStrength->setValue((int)uint32val);
        } else {
            ui->sliderNoiseFilterStrength->setValue(50);
        }
    }
#endif
}

void CameraSettings::brightnessChanged(int val)
{
#ifdef WITH_ENGINE
    int response = engine_set_brightness((int16_t)val);
    if(response) {
        settingFailedResponse("set","Brightness");
    }
#endif
}

void CameraSettings::contrastChanged(int val)
{
#ifdef WITH_ENGINE
    int response = engine_set_contrast((uint16_t)val);
    if(response)
        settingFailedResponse("set","Contrast");
#endif
}

void CameraSettings::gamaChanged(int val)
{
#ifdef WITH_ENGINE
    int response = engine_set_gama((uint16_t)val);
    if(response)
        settingFailedResponse("set","Gama");
#endif
}

void CameraSettings::hueChanged(int val)
{
#ifdef WITH_ENGINE
    int response = engine_set_hue((int16_t)val);
    if(response) {
        settingFailedResponse("set","Hue");
    }
#endif
}

void CameraSettings::saturationChanged(int val)
{
#ifdef WITH_ENGINE
    int response = engine_set_saturation(val);
    if(response)
        settingFailedResponse("set","Saturation");
#endif
}

void CameraSettings::sharpnessChanged(int val)
{
#ifdef WITH_ENGINE
    int response = engine_set_sharpness(val);
    if(response)
        settingFailedResponse("set","Sharpness");
#endif
}

void CameraSettings::verticalFlipChanged()
{
    qDebug()<<"Vertical flip changed - "<<ui->cbVerticalFlip->isChecked();
#ifdef WITH_ENGINE
    int response = engine_set_flip_vertical(ui->cbVerticalFlip->isChecked());
    if(response)
        settingFailedResponse("set","vertical flip");
#endif
}

void CameraSettings::horizontalFlipChanged()
{
#ifdef WITH_ENGINE
    int response = engine_set_flip_horizontal(ui->cbHorizontalFlip->isChecked());
    if(response)
        settingFailedResponse("set","horizontal flip");
#endif
}

void CameraSettings::histogramEqualizationChanged()
{
#ifdef WITH_ENGINE
    int response = engine_set_histogram_eq(ui->cbHistogram->isChecked());
    if(response)
        settingFailedResponse("set","histogram equalization");
#endif
}

void CameraSettings::maxAnalogGainChanged(int val)
{
#ifdef WITH_ENGINE
    int response = engine_set_max_analog_gain(val);
    if(response)
        settingFailedResponse("set","max analog gain");
#endif
}

void CameraSettings::sharpenFilterChanged(int val)
{
#ifdef WITH_ENGINE
    int response = engine_set_sharpen_filter(val);
    if(response)
        settingFailedResponse("set","sharpen filter");
#endif
}

void CameraSettings::exposureGainMultiplierChanged(int val)
{
#ifdef WITH_ENGINE
    int response = engine_set_gain_multiplier(val);
    if(response)
        settingFailedResponse("set","gain multiplier");
#endif
}

void CameraSettings::onWhiteBalanceModeChanged(bool val)
{
    if (val) {
        ui->awb->setTitle("White Balance (Manual)");
    } else {
        ui->awb->setTitle("White Balance (Auto)");
    }
#ifdef WITH_ENGINE
    qDebug() << "Setting WB:" << (val?"WB_MODE_MANUAL":"WB_MODE_AUTO") << "with value:" << ui->sliderWhiteBalanceTemp->value();
    int response = engine_set_wb(val?WB_MODE_MANUAL:WB_MODE_AUTO,ui->sliderWhiteBalanceTemp->value());
    if(response)
        settingFailedResponse("set","white balance");
#endif
}

void CameraSettings::onWhiteBalanceTemperatureChanged(int val)
{
#ifdef WITH_ENGINE
    qDebug() << "Setting WB:" << (ui->awb->isChecked()?"WB_MODE_MANUAL":"WB_MODE_AUTO") << "with value:" << val;
    int response = engine_set_wb(ui->awb->isChecked()?WB_MODE_MANUAL:WB_MODE_AUTO, val);
    if(response)
        settingFailedResponse("set","white balance temperature");
#endif
}

void CameraSettings::onWhiteBalanceZoneEnabledOrDisabled(bool enabled) {
#ifdef WITH_ENGINE
    qDebug() << "Setting WB Zone:" << (enabled?"ZONE_WB_ENABLE":"ZONE_WB_DISABLE") << "with value:" << ui->sliderWhiteBalanceZone->value();
    int error = engine_set_zone_wb(enabled?ZONE_WB_ENABLE:ZONE_WB_DISABLE, ui->sliderWhiteBalanceZone->value());
    if (error) {
        settingFailedResponse("set","white balance zone");
    }
#endif
}

void CameraSettings::onWhiteBalanceZoneValueChanged(int val)
{
#ifdef WITH_ENGINE
    qDebug() << "Setting WB Zone:" << (ui->gbox_wbz->isChecked()?"ZONE_WB_ENABLE":"ZONE_WB_DISABLE") << "with value:" << val;
    int response = engine_set_zone_wb(ui->gbox_wbz->isChecked()?ZONE_WB_ENABLE:ZONE_WB_DISABLE,val);
    if(response) {
        settingFailedResponse("set","white balance zone");
    }
#endif
}
void CameraSettings::onWdrModeChanged(bool val)
{
    if (val) {
        ui->awdr->setTitle("Wide Dynamic Range (Manual)");
    } else {
        ui->awdr->setTitle("Wide Dynamic Range (Auto)");
    }
#ifdef WITH_ENGINE
    qDebug() << "Setting WDR Mode:" << (val?"WDR_MANUAL":"WDR_AUTO") << "with value:" <<  ui->sliderWdrStrength->value();
    int response = engine_set_wdr(val?WDR_MANUAL:WDR_AUTO, ui->sliderWdrStrength->value());
    if(response) {
        settingFailedResponse("set","wdr");
    }
#endif
}

void CameraSettings::onWdrStrengthChanged(int val)
{
#ifdef WITH_ENGINE
    qDebug() << "Setting WDR Strength:" << (ui->awdr->isChecked()?"WDR_MANUAL":"WDR_AUTO") << "with value:" << val;
    int response = engine_set_wdr(ui->awdr->isChecked()?WDR_MANUAL:WDR_AUTO, val);
    if(response) {
        settingFailedResponse("set","wdr strength");
    }
#endif
}

#if 0 //Disbaled as there is no MXUVC support for this fetaure
void CameraSettings::onExposureModeChanged(bool val) {
    if (val == true) {
        ui->gbox_exp_mode->setTitle("Exposure (Manual)");
    } else {
        ui->gbox_exp_mode->setTitle("Exposure (Auto)");
    }
#ifdef WITH_ENGINE
    qDebug() << "Setting Exposure:" << (val?"EXP_MANUAL":"EXP_AUTO") << "with value:" << ui->sliderExposureTime->value();
    int response = engine_set_exposure(val?EXP_MANUAL:EXP_AUTO, ui->sliderExposureTime->value());
    if(response) {
        settingFailedResponse("set","exposure");
    } else {
        if (val && readExposureZone) {
            zone_exp_set_t zoneExpMode;
            uint32_t zval;
            int response = engine_get_zone_exp(&zoneExpMode,&zval);
            if(response) {
                settingFailedResponse("get","zone exposure");
            } else {
                ui->gbox_exposure_zone->setChecked(zoneExpMode);
                ui->sliderExposureZone->setValue((int)zval);
            }
        }
    }
#endif
}

void CameraSettings::onExposureTimeChanged(int val)
{
#ifdef WITH_ENGINE
    qDebug() << "Setting Exposure:" << (ui->gbox_exp_mode->isChecked()?"EXP_MANUAL":"EXP_AUTO") << "with value:" << val;
    int response = engine_set_exposure(ui->gbox_exp_mode->isChecked()?EXP_MANUAL:EXP_AUTO, val);
    if(response) {
        settingFailedResponse("set","exposure time");
    }
#endif
}

void CameraSettings::onZonalExposureEnabledOrDisabled(bool enabled) {
#ifdef WITH_ENGINE
    qDebug() << "Setting Zonal Exposure:" << (enabled?"ZONE_EXP_ENABLE":"ZONE_EXP_DISABLE") << "with value:" << ui->sliderExposureZone->value();
    int response = engine_set_zone_exp(enabled?ZONE_EXP_ENABLE:ZONE_EXP_DISABLE, ui->sliderExposureZone->value());
    if(response) {
        settingFailedResponse("set","exposure zone");
    }
#endif
}

void CameraSettings::onExposureZoneValueChanged(int val)
{
#ifdef WITH_ENGINE
    qDebug() << "Setting Zone Exposure:" << (ui->gbox_exposure_zone->isChecked()?"ZONE_EXP_ENABLE":"ZONE_EXP_DISABLE") << "with value:" << val;
    int response = engine_set_zone_exp(ui->gbox_exposure_zone->isChecked()?ZONE_EXP_ENABLE:ZONE_EXP_DISABLE, val);
    if(response)
        settingFailedResponse("set","exposure zone");
#endif
}
#endif

void CameraSettings::onNoiseFilterModeChanged(bool val)
{
    if (val == true) {
        ui->gbox_noise_filter->setTitle("Noise Filter (Manual)");
    } else {
        ui->gbox_noise_filter->setTitle("Noise Filter (Auto)");
    }
#ifdef WITH_ENGINE
    qDebug() << "Setting Noise filter mode to:" << (val?"NF_MODE_MANUAL":"NF_MODE_AUTO") << "with value:" << ui->sliderNoiseFilterStrength->value();
    int response = engine_set_nf(val?NF_MODE_MANUAL:NF_MODE_AUTO, ui->sliderNoiseFilterStrength->value());
    if(response) {
        settingFailedResponse("set","noise filter");
    }
#endif
}

void CameraSettings::onNoiseFilterStrengthChanged(int val)
{
#ifdef WITH_ENGINE
    qDebug() << "Setting Noise filter Strength:" << (ui->gbox_noise_filter->isChecked()?"NF_MODE_MANUAL":"NF_MODE_AUTO") << "with value:" << val;
    int response = engine_set_nf(ui->gbox_noise_filter->isChecked()?NF_MODE_MANUAL:NF_MODE_AUTO, val);
    if(response) {
        settingFailedResponse("set","noise filter strength");
    }
#endif
}

void CameraSettings::settingFailedResponse(QString  requestType, QString val) {
    qCritical() << "Failed to" << requestType << val;
    AlertHandler::displayError(requestType+" failed on "+val);
}
