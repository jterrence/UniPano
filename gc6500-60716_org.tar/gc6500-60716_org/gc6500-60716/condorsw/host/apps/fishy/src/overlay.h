/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef OVERLAY_H
#define OVERLAY_H

#include <QString>
#include <QColor>

#include <stdint.h>

class OverlayText {
public:
    QString text;
    int xOffset;
    int yOffset;
    int index;
    bool enabled;
};

class OverlayFont {
public:
    QString fontPath;
    int fontSize;
    bool loaded;
};

class OverlayImage {
public:
    QString image;
    int width;
    int height;
    int xOffset;
    int yOffset;
    int alpha;
    QString alphaFile;
    int id;
};

class OverlayTime {
public:
    int hours;
    int minutes;
    int seconds;
    int xOffset;
    int yOffset;
};

class PrivacyMask {
public:
    QColor color;
    int index;

    int xOffset;
    int yOffset;
    int width;
    int height;

    float ratio;

    int** points;

    bool enabled;
    bool highlight;
};

#endif // OVERLAY_H
