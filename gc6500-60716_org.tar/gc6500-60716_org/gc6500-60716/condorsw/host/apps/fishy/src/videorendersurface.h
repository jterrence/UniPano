/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef VIDEORENDERSURFACE_H
#define VIDEORENDERSURFACE_H

#include <QGraphicsView>

class VideoRenderSurface : public QGraphicsView
{
    Q_OBJECT
public:
    explicit VideoRenderSurface(QWidget *parent = 0);
    QGraphicsScene * scene;
signals:

public slots:
    void mousePressEvent(QMouseEvent * e);
    void mouseReleaseEvent(QMouseEvent * e);
    void showEvent(QShowEvent *event);
    void mouseDoubleClickEvent(QMouseEvent * e);
    void keyPressEvent(QKeyEvent *event);
    void wheelEvent(QWheelEvent *event);
    void resizeEvent(QResizeEvent *event);
};

#endif // VIDEORENDERSURFACE_H
