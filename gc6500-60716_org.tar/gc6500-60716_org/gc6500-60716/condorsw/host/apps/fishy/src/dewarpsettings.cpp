/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include <QFile>
#include "dewarpsettings.h"
#include "ui_dewarpsettings.h"
#include "cameraconfig.h"
#include "alerthandler.h"
#include "stringutils.h"
#include "log.h"
#include <QPushButton>

#ifdef WITH_ENGINE
#ifdef __i386__
    #include "ePTZ_eWARP.h"
#endif
#endif

DewarpSettings::DewarpSettings(QWidget *parent) :
    QDialog(parent),uiSlider(),uiLabel(),uiGridLayout(), defaultParamsStructure(),uiParamsStructure(),
    ui(new Ui::DewarpSettings)
{
    ui->setupUi(this);

    mapPath = "";
    channelsWithDewarp = NULL;

    apiManager = new EngineApiManager(0, "dewarp");

    connect(apiManager, SIGNAL(signalEngineGetDewarpParamsSuccess(video_channel_t,int,dewarp_mode_t,dewarp_params_t)), this, SLOT(onDewarpGetSuccess(video_channel_t,int,dewarp_mode_t,dewarp_params_t)));
    connect(apiManager, SIGNAL(signalEngineGetDewarpParamsFailed(video_channel_t,int)), this, SLOT(onDewarpGetFailed(video_channel_t,int)));
    connect(apiManager, SIGNAL(signalEngineSetDewarpParamsSuccess(video_channel_t,int,dewarp_mode_t,dewarp_params_t)), this, SLOT(onDewarpSetSuccess(video_channel_t,int,dewarp_mode_t,dewarp_params_t)));
    connect(apiManager, SIGNAL(signalEngineSetDewarpParamsFailed(video_channel_t,int)), this, SLOT(onDewarpSetFailed(video_channel_t,int)));

    connect(ui->displayChannel0, SIGNAL(signalOnVideoStarted(video_channel_t)), this, SLOT(onVideoStarted(video_channel_t)));
    connect(ui->displayChannel1, SIGNAL(signalOnVideoStarted(video_channel_t)), this, SLOT(onVideoStarted(video_channel_t)));

    mNumChannels = CameraConfig::shared()->getNumChannels();

    int referenceChannel = -1;
    int secondaryChannel = -1;

    channelsWithDewarp = new int[mNumChannels];

    for (int index = 0; index < MAX_SETTING_ELEMENTS; index++) {
        uiSlider[index] = NULL;
        uiLabel[index] = NULL;
    }

    ui->displayChannel0->setDrawTitle(false);
    ui->displayChannel1->setDrawTitle(false);

#ifdef WITH_ENGINE
    // populate the channel list. Show channels with dewarp or composition or both
    ui->cbChannel->clear();
    int index = 0;

    for (int channel = 0; channel < mNumChannels; channel++) {
        config_params_t *params = CameraConfig::shared()->getChannelConfigParams(channel);
        if (params->composite == 0 && params->dewarp == 0) {
            // No composition and no dewarp supported. Set as primary channel
            if (referenceChannel == -1) {
                referenceChannel = channel;
            }
        }

        if (params->composite == 1 || params->dewarp == 1) {
            if (secondaryChannel < 0) {
                secondaryChannel = channel;
            }
            channelsWithDewarp[index++] = channel;
            ui->cbChannel->addItem(StringUtils::shared()->channelToString((video_channel_t)channel));
        }
    }

    if (referenceChannel == 0) {
        ui->displayChannel0->setChannel(referenceChannel);
        ui->displayChannel0->setHandleMouseEvents(false);
        oldResW = ui->displayChannel0->getChannelWidth();
    } else {
        ui->displayChannel0->setChannel(-1);
        ui->displayChannel0->setHandleMouseEvents(false);
        ui->displayChannel0->setVisible(false);
    }

    populateUIDewarpModesDropdown();
#endif
//    this->setAttribute(Qt::WA_QuitOnClose);

    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(close()));
    ui->buttonBox->button(QDialogButtonBox::Ok)->setText(tr("Done"));
}

DewarpSettings::~DewarpSettings()
{
    qDebug() << "Destructor called";

    if (channelsWithDewarp != NULL) {
        delete [] channelsWithDewarp;
    }

    if (apiManager) {
        disconnect(apiManager, SIGNAL(signalEngineGetDewarpParamsSuccess(video_channel_t,int,dewarp_mode_t,dewarp_params_t)), this, SLOT(onDewarpGetSuccess(video_channel_t,int,dewarp_mode_t,dewarp_params_t)));
        disconnect(apiManager, SIGNAL(signalEngineGetDewarpParamsFailed(video_channel_t,int)), this, SLOT(onDewarpGetFailed(video_channel_t,int)));
        disconnect(apiManager, SIGNAL(signalEngineSetDewarpParamsSuccess(video_channel_t,int,dewarp_mode_t,dewarp_params_t)), this, SLOT(onDewarpSetSuccess(video_channel_t,int,dewarp_mode_t,dewarp_params_t)));
        disconnect(apiManager, SIGNAL(signalEngineSetDewarpParamsFailed(video_channel_t,int)), this, SLOT(onDewarpSetFailed(video_channel_t,int)));

        apiManager->cleanup();
    }

    cleanUpUiLayout();

    ui->displayChannel0->stopVideo();
    ui->displayChannel1->stopVideo();
    delete ui;
}

void DewarpSettings::showEvent(QShowEvent *event)
{
    event->accept();
    this->setPalette(QColor(45,45,45));

    if (ui->cbChannel->count() > 0) {
        this->onChannelChanged(0);
        connect(ui->cbChannel,SIGNAL(currentIndexChanged(int)),this,SLOT(onChannelChanged(int)));
    } else {
        ui->cbChannel->setEnabled(false);
        setDewarp(false);
        setComposition(false);
    }
}

void DewarpSettings::keyPressEvent(QKeyEvent *event) {
    if (event->key() == Qt::Key_Enter) {
        event->accept();
    } else if (event->key() == Qt::Key_Escape) {
        this->close();
    }
}

// end public function

void DewarpSettings::setComposition(bool enabled) {
    ui->labelPanel->setEnabled(enabled);
    ui->cbPanel->setEnabled(enabled);
}

void DewarpSettings::setDewarp(bool enabled) {
    ui->labelDewarpMode->setEnabled(enabled);
    ui->cbDewarpMode->setEnabled(enabled);
}

void DewarpSettings::populateUIDewarpModesDropdown() {
    ui->cbDewarpMode->clear();
    ui->cbDewarpMode->addItem(StringUtils::shared()->dewarpModeToString(EMODE_OFF));
    ui->cbDewarpMode->addItem(StringUtils::shared()->dewarpModeToString(EMODE_WM_ZCL));
    ui->cbDewarpMode->addItem(StringUtils::shared()->dewarpModeToString(EMODE_WM_ZCLCylinder));
    ui->cbDewarpMode->addItem(StringUtils::shared()->dewarpModeToString(EMODE_WM_ZCLStretch));
    ui->cbDewarpMode->addItem(StringUtils::shared()->dewarpModeToString(EMODE_WM_1PanelEPTZ));
    ui->cbDewarpMode->addItem(StringUtils::shared()->dewarpModeToString(EMODE_WM_Sweep_1PanelEPTZ));
    ui->cbDewarpMode->addItem(StringUtils::shared()->dewarpModeToString(EMODE_WM_Magnify));
}

void DewarpSettings::populateUIPanelsDropdown(int num) {
    ui->cbPanel->clear();
    for (int panel = 0; panel < num; panel++) {
        ui->cbPanel->addItem(QString("Panel "+QString::number(panel+1)));
    }
}

void DewarpSettings::setUpMagnifyUiView()
{
    ui->gbRunTime->setTitle("Magnify");
}

void DewarpSettings::setUpZclCylinderUiView()
{
    uiLabel[0]->setText("Cylinder Height - 0.95");
    ui->gbRunTime->setTitle("Zcl Cylinder");
}

void DewarpSettings::setUpZclStretchUiView()
{
    uiLabel[0]->setText("H-Pan (pixel)");
    uiLabel[1]->setText("V-Pan (pixel)");
    uiLabel[2]->setText("Zoom factor");

    uiSlider[0]->setRange(-ui->displayChannel1->getChannelWidth()/2,ui->displayChannel1->getChannelWidth()/2);
    uiSlider[0]->setValue(defaultParamsStructure[0]);

    uiSlider[1]->setRange(-ui->displayChannel1->getChannelHeight()/2,ui->displayChannel1->getChannelHeight()/2);
    uiSlider[1]->setValue(defaultParamsStructure[1]);

    uiSlider[2]->setRange(1,10);
    uiSlider[2]->setValue(defaultParamsStructure[2]);
    uiSlider[2]->setPageStep(1);
    ui->gbRunTime->setTitle("Zcl Stretch");

    onZoomChanged(uiSlider[2]->value());
    connect(uiSlider[2],SIGNAL(signalValueChanged(int)),this,SLOT(onZoomChanged(int)));
}

void DewarpSettings::setUp1PanelEPZUiView()
{
    uiLabel[0]->setText("H-Pan(deg)");
    uiLabel[1]->setText("V-Pan(deg)");
    uiLabel[2]->setText("Tilt(deg)");
    uiLabel[3]->setText("Zoom(deg)");

    uiSlider[0]->setRange(-50,50);
    uiSlider[0]->setValue(defaultParamsStructure[0]);
    uiSlider[1]->setRange(-50,50);
    uiSlider[1]->setValue(defaultParamsStructure[1]);
    uiSlider[2]->setRange(-180,180);
    uiSlider[2]->setValue(defaultParamsStructure[2]);
    uiSlider[3]->setRange(20,60);
    uiSlider[3]->setValue(defaultParamsStructure[3]);

    ui->gbRunTime->setTitle("1Panel EPTZ");
}

void DewarpSettings::setUpZclUiView()
{
    //label
    uiLabel[0]->setText("Phi0");
    uiLabel[1]->setText("Rx");
    uiLabel[2]->setText("Ry");
    uiLabel[3]->setText("Rz");
    uiLabel[4]->setText("Gshift(deg)");

    //slider
    uiSlider[0]->setRange(-100,100);
    uiSlider[0]->setValue(defaultParamsStructure[0]); //55

    uiSlider[1]->setRange(-100,100);
    uiSlider[1]->setValue(defaultParamsStructure[1]);//2

    uiSlider[2]->setRange(-100,100);
    uiSlider[2]->setValue(defaultParamsStructure[2]);//1

    uiSlider[3]->setRange(-100,100);
    uiSlider[3]->setValue(defaultParamsStructure[3]);//1

    uiSlider[4]->setRange(-180,180);
    uiSlider[4]->setValue(defaultParamsStructure[4]);//0

    ui->gbRunTime->setTitle("Zcl");
}

void DewarpSettings::setUpSweepUiView()
{
    ui->gbRunTime->setTitle("1Panel Sweep EPTZ");
}

void DewarpSettings::setUpDefaultUiView() {
    ui->gbRunTime->setTitle(" ");
}

void DewarpSettings::generateUIForDewarpMode(dewarp_mode_t mode) {
    uiGridLayout = new QGridLayout(ui->gbRunTime);
    switch(mode) {
        case EMODE_WM_ZCLCylinder:
            uiLabel[0] = new QLabel(ui->gbRunTime);
            uiGridLayout->addWidget(uiLabel[0],0,0,1,1);
            break;
        default:
            this->generateSettingsLayout(getNumSettingsForDewarpMode(mode));
            break;
    }
}

void DewarpSettings::generateSettingsLayout(int num)
{
    for(int i = 0;i<num;i++)
    {
        uiSlider[i] = new CustomSlider();
        if (defaultParamsStructure[i] > -1) {
            uiSlider[i]->setValue(defaultParamsStructure[i]);
        }
        uiSlider[i]->setLabelWidth(30);

        uiLabel[i] = new QLabel(ui->gbRunTime);
        uiGridLayout->addWidget(uiLabel[i],i,0,1,1);
        uiGridLayout->addWidget(uiSlider[i],i,1,1,1);
        connect(uiSlider[i],SIGNAL(signalValueChanged(int)),this,SLOT(onSliderValueChanged(int)));
    }
}

QString DewarpSettings::dewarpModeToString(dewarp_mode_t mode) {
    switch(mode) {
        case EMODE_OFF:
            return "EMODE_OFF";
        case EMODE_WM_ZCL:
            return "EMODE_WM_ZCL";
        case EMODE_WM_ZCLCylinder:
            return "EMODE_WM_ZCLCylinder";
        case EMODE_WM_ZCLStretch:
            return "EMODE_WM_ZCLStretch";
        case EMODE_WM_1PanelEPTZ:
            return "EMODE_WM_1PanelEPTZ";
        case EMODE_WM_Sweep_1PanelEPTZ:
            return "EMODE_WM_Sweep_1PanelEPTZ";
        case EMODE_WM_Magnify:
            return "EMODE_WM_Magnify";
        case EMODE_TM_1CircPanoViewPan:
            return "EMODE_TM_1CircPanoViewPan";
        case EMODE_CM_CircPanoViewPan:
            return "EMODE_CM_CircPanoViewPan";
        case EMODE_MAX:
            return "EMODE_MAX";
        default:
            return "-";
    }
}

void DewarpSettings::cleanUpUiLayout()
{
    for(int i = 0; i< MAX_SETTING_ELEMENTS; i++) {
        if(uiSlider[i]) {
            disconnect(uiSlider[i],SIGNAL(signalValueChanged(int)),0,0);
            delete uiSlider[i];
            uiSlider[i] = NULL;
        }

        if (uiLabel[i]) {
            delete uiLabel[i];
            uiLabel[i] = NULL;
        }
    }

    ui->gbRunTime->resize(0, 0);

    if(uiGridLayout != NULL) {
        delete uiGridLayout;
        uiGridLayout = NULL;
    }

    this->adjustSize();
}


// SLOTS

void DewarpSettings::onZoomChanged(int zoom) {
    uiLabel[0]->setEnabled(zoom>1);
    uiSlider[0]->setEnabled(zoom>1);
    uiLabel[1]->setEnabled(zoom>1);
    uiSlider[1]->setEnabled(zoom>1);
}

void DewarpSettings::onChannelChanged(int channel)
{
    currentChannel = channelsWithDewarp[channel];
    qDebug() << "Channel changed:" << StringUtils::shared()->channelToString((video_channel_t)currentChannel);

#ifdef WITH_ENGINE
    config_params_t *params = CameraConfig::shared()->getChannelConfigParams(currentChannel);
    if(params->composite == 0 && params->dewarp == 0) {

        setComposition(false);
        ui->displayChannel1->stopVideo();

    } else if (params->composite == 1) {

        setComposition(true);

        disconnect(ui->cbPanel,SIGNAL(currentIndexChanged(int)),0,0);
        this->populateUIPanelsDropdown(params->panels);
        ui->cbPanel->setCurrentIndex(0);
        connect(ui->cbPanel,SIGNAL(currentIndexChanged(int)),this,SLOT(onPanelChanged(int)));

        ui->displayChannel1->stopVideo();
        ui->displayChannel1->setChannel(currentChannel);
        ui->displayChannel1->setHandleMouseEvents(false);

    } else if(params->dewarp == 1) {

        //setDewarp(true);
        setComposition(false);

        disconnect(ui->cbPanel,SIGNAL(currentIndexChanged(int)),0,0);
        this->populateUIPanelsDropdown(1);

        ui->displayChannel1->stopVideo();
        ui->displayChannel1->setChannel(currentChannel);
        //ui->displayChannel1->startVideo();
        ui->displayChannel1->setHandleMouseEvents(false);

        //apiManager->engineGetDewarpParams((video_channel_t)currentChannel, 0);
    }
#endif
}

void DewarpSettings::onPanelChanged(int panel) {
    ui->displayChannel1->highlightPanel(panel);
    apiManager->engineGetDewarpParams((video_channel_t)channelsWithDewarp[ui->cbChannel->currentIndex()], panel);
}

void DewarpSettings::onDewarpModeChanged(int mode) {

    qDebug() << "Dewarp mode changed to " << dewarpModeToString((dewarp_mode_t)mode);

    cleanUpUiLayout();
    generateUIForDewarpMode((dewarp_mode_t)mode);

    switch(mode) {
        case EMODE_WM_Magnify:
            setUpMagnifyUiView();
            break;
        case EMODE_WM_ZCL:
            defaultParamsStructure[0] = 70;
            defaultParamsStructure[1] = -10;
            defaultParamsStructure[2] = 20;
            defaultParamsStructure[3] = -20;
            defaultParamsStructure[4] = 0;
            setUpZclUiView();
            break;
        case EMODE_WM_ZCLCylinder:
            defaultParamsStructure[0] = 0.95;
            setUpZclCylinderUiView();
            break;
        case EMODE_WM_ZCLStretch:
            defaultParamsStructure[0] = 0;
            defaultParamsStructure[1] = 0;
            defaultParamsStructure[2] = 1;
            setUpZclStretchUiView();
            break;
        case EMODE_WM_1PanelEPTZ:
            defaultParamsStructure[0] = 0;
            defaultParamsStructure[1] = 0;
            defaultParamsStructure[2] = 20;
            defaultParamsStructure[3] = 40;
            setUp1PanelEPZUiView();
            break;
        case EMODE_WM_Sweep_1PanelEPTZ:
            setUpSweepUiView();
            break;
        default:
            setUpDefaultUiView();
    }

    DrawDewarpMap();
    updateDewarpParams();
}

void DewarpSettings::onSliderValueChanged(int value) {
    qDebug() << "Slider value changed: " << value;
    DrawDewarpMap();
    updateDewarpParams();
}

void DewarpSettings::onDewarpGetSuccess(video_channel_t channel, int panel, dewarp_mode_t mode, dewarp_params_t params) {
    qDebug() << "====== engine_get_dewarp_params ======";
    qDebug() << "Channel: " << channel;
    qDebug() << "Panel: " << panel;
    qDebug() << "Mode: " << dewarpModeToString(mode);
    qDebug() << "======================================";

    disconnect(ui->cbDewarpMode,SIGNAL(currentIndexChanged(int)),this,SLOT(onDewarpModeChanged(int)));
    ui->cbDewarpMode->setCurrentIndex((int)mode);
    connect(ui->cbDewarpMode,SIGNAL(currentIndexChanged(int)),this,SLOT(onDewarpModeChanged(int)));

    cleanUpUiLayout();
    generateUIForDewarpMode(mode);

    switch(mode) {
        case EMODE_WM_ZCL:
        {
            int element = sizeof(params.eptz_mode_wm_zcl);
            memcpy(defaultParamsStructure,(const char*)&params,element);
            setUpZclUiView();
            break;
        }
        case EMODE_WM_ZCLCylinder:
        {
            int element = sizeof(params.eptz_mode_wm_zclcylinder);
            memcpy(defaultParamsStructure,(const char*)&params,element);
            setUpZclCylinderUiView();
        }
            break;
        case EMODE_WM_ZCLStretch:
        {
            int element = sizeof(params.eptz_mode_wm_zclstretch);
            memcpy(defaultParamsStructure,(const char*)&params,element);
            setUpZclStretchUiView();
        }
            break;
        case EMODE_WM_1PanelEPTZ:
        {
            int element = sizeof(params.eptz_mode_wm_1paneleptz);
            memcpy(defaultParamsStructure,(const char*)&params,element);
            setUp1PanelEPZUiView();
        }
            break;
        case EMODE_WM_Sweep_1PanelEPTZ:
        {
            int element = sizeof(params.eptz_mode_sweep_wm_1panelptz);
            memcpy(defaultParamsStructure,(const char*)&params,element);
            setUpSweepUiView();
        }
            break;
        case EMODE_WM_Magnify:
        {
            int element = sizeof(params.eptz_mode_magnify);
            memcpy(defaultParamsStructure,(const char*)&params,element);
            setUpMagnifyUiView();
        }
            break;
        default:
            for (int index = 0; index < MAX_SETTING_ELEMENTS; index++) {
                defaultParamsStructure[index] = -1;
            }
            setUpDefaultUiView();
            break;
    }

    DrawDewarpMap();
}

void DewarpSettings::onDewarpGetFailed(video_channel_t channel, int panel) {
    qDebug() << "Dewarp get failed for channel:"<<channel<<" and panel:"<<panel;
    AlertHandler::displayError("Failed to read dewarp mode/params");
}

void DewarpSettings::onDewarpSetSuccess(video_channel_t channel, int panel, dewarp_mode_t mode, dewarp_params_t) {
    qDebug() << "Dewarp params successfully set for channel:"<<channel<<", panel:"<<panel<<", mode:"<<dewarpModeToString(mode);
}

void DewarpSettings::onDewarpSetFailed(video_channel_t channel, int panel) {
    qDebug() << "Dewarp set failed for channel:"<<channel<<" and panel:"<<panel;
    AlertHandler::displayError("Failed to set dewarp mode/params");
}

void DewarpSettings::onVideoStarted(video_channel_t channel) {
    qDebug() << "DewarpSettings : Video started for" << StringUtils::shared()->channelToString(channel);
    if (ui->displayChannel1->getChannel() == channel) {
        if (ui->cbPanel->isEnabled()) {
            ui->displayChannel1->highlightPanel(ui->cbPanel->currentIndex());
        }
        apiManager->engineGetDewarpParams((video_channel_t)channel, ui->cbPanel->currentIndex());
    }
}

// END SLOTS

void DewarpSettings::DrawDewarpMap() {
#ifdef __i386__
    // Generate a grid map only if there is a reference channel
    if (ui->displayChannel0->getChannel() == -1) {
        return;
    }

    dewarp_mode_t dewarpMode = (dewarp_mode_t)ui->cbDewarpMode->currentIndex();

    if (dewarpMode == EMODE_OFF || dewarpMode == EMODE_WM_Magnify || dewarpMode == EMODE_WM_Sweep_1PanelEPTZ) {
        ui->displayChannel0->setMapPoints(QList<QPoint>());
        ui->displayChannel0->setDewarpMapEnable(false);
        return;
    }

    eptz_handle_struct* handle = eptz_init(getModeValueFromIndex(dewarpMode), ui->displayChannel0->getChannelWidth(),ui->displayChannel0->getChannelHeight());
    if (handle) {
        // populate the eptz params for the selected dewarp mode
        switch(dewarpMode) {
            case EMODE_OFF:
                break;
            case EMODE_WM_ZCL:
                handle->param[7] = 110;
                handle->param[6] = uiSlider[0]->value();
                handle->param[9] = uiSlider[1]->value();
                handle->param[10] = uiSlider[2]->value();
                handle->param[11] = uiSlider[3]->value();
                handle->param[12] = uiSlider[4]->value();

                //mapPath.append("zcl.txt");
                break;
            case EMODE_WM_ZCLCylinder:
                handle->param[7] = 111;
                handle->param[9] = 0.95;

                //mapPath.append("zclcylinder.txt");
                break;
            case EMODE_WM_ZCLStretch:
                handle->param[7] = 112;
                handle->param[0] = uiSlider[0]->value();
                handle->param[1] = uiSlider[1]->value();
                handle->param[6] = uiSlider[2]->value();

                //mapPath.append("zclstretch.txt");
                break;
            case EMODE_WM_1PanelEPTZ:
                handle->param[7] = 120;
                handle->param[0] = uiSlider[0]->value();
                handle->param[1] = uiSlider[1]->value();
                handle->param[6] = uiSlider[3]->value();
                handle->param[9] = uiSlider[2]->value();

                //mapPath.append("1paneleptz.txt");
                break;
            case EMODE_WM_Magnify:
                //Dynamic Mode no map can be generated as of now
                break;
            case EMODE_WM_Sweep_1PanelEPTZ:
                //Dynamic Mode no map can be generated as of now
                break;
            default:
                break;
        }

        if (mapPath.length() == 0) {
            mapPath = QString(DEWARP_MAP_RELATIVE_FILE_PATH).append("map.txt");
            QFile file(mapPath);
            if (file.exists()) {
                if (!file.open(QIODevice::WriteOnly)) {
                    qWarning()<<mapPath<<"is not writable";
                    mapPath = QString(DEWARP_MAP_RELATIVE_FILE_PATH).append("map1.txt");
                    file.setFileName(mapPath);
                } else {
                    file.close();
                }
            }

            qDebug()<<"Map file path:"<<mapPath;
        }

        eptz_generate_gridmap(handle, ui->displayChannel0->getChannelWidth(),ui->displayChannel0->getChannelHeight(),ui->displayChannel0->getChannelWidth(),ui->displayChannel0->getChannelHeight(),50, handle->param, NULL, (char*)mapPath.toStdString().c_str());
        eptz_deinit(handle);

        QFile file(mapPath);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            qCritical()<<mapPath<<": File open failed";
            return;
        }

        ui->displayChannel0->setDewarpMapEnable(true);

        QList<QPoint> points;

        while (!file.atEnd()) {
            QByteArray line = file.readLine();
            QString value(line);
            QStringList list2 = value.split(",");
            // list2: [ "a", "b", "c" ]
            QByteArray ba = list2.at(0).toLatin1();
            int x = atoi(ba.data());
            ba = list2.at(1).toLatin1();
            int y= atoi(ba.data());
            QPoint p(x,y);

            points.append(p);
        }

        ui->displayChannel0->setMapPoints(points);
    } else {
        ui->displayChannel0->setDewarpMapEnable(false);
    }
#endif
}

int DewarpSettings::getModeValueFromIndex(dewarp_mode_t selectedMode)
{
    int mode;
    switch(selectedMode)
    {
        case EMODE_OFF:
            mode = 0;
            break;
        case EMODE_WM_ZCL:
            mode = 110;
            break;
        case EMODE_WM_ZCLCylinder:
            mode = 111;
            break;
        case EMODE_WM_ZCLStretch:
            mode = 112;
            break;
        case EMODE_WM_1PanelEPTZ:
            mode = 120;
            break;
        case EMODE_WM_Magnify:
            mode = 401;
            break;
        case EMODE_WM_Sweep_1PanelEPTZ:
            mode = 400;
            break;
        default:
            mode = 0;
            break;
    }
    return mode;
}

int DewarpSettings::getNumSettingsForDewarpMode(dewarp_mode_t mode)
{
    dewarp_params_t selectedParams;
    int element;
    int numElementsToIgnore = 1;
    switch(mode)
    {
        case EMODE_OFF:
            element = 0;
            break;
        case EMODE_WM_ZCL:
            element = sizeof((selectedParams.eptz_mode_wm_zcl));
            break;
        case EMODE_WM_ZCLCylinder:
            element = 0;//sizeof((selectedParams.eptz_mode_wm_zclcylinder));
            break;
        case EMODE_WM_ZCLStretch:
            element = sizeof((selectedParams.eptz_mode_wm_zclstretch));
            break;
        case EMODE_WM_1PanelEPTZ:
            element = sizeof((selectedParams.eptz_mode_wm_1paneleptz));
            numElementsToIgnore += 3;
            break;
        case EMODE_WM_Magnify:
            element = 0;//sizeof((selectedParams.eptz_mode_magnify));
            break;
        case EMODE_WM_Sweep_1PanelEPTZ:
            element = 0;//sizeof((selectedParams.eptz_mode_sweep_wm_1panelptz));
            break;
        default:
            element = 0;
            break;
    }
    return (element/sizeof(int))-numElementsToIgnore;

}

void DewarpSettings::updateDewarpParams() {
    for (int i = 0; i < MAX_SETTING_ELEMENTS; i++) {
        uiParamsStructure[i] = 0;
    }

    int loopCounter = getNumSettingsForDewarpMode((dewarp_mode_t)ui->cbDewarpMode->currentIndex());
    if (loopCounter > 0) {
        for(int i = 0; i < loopCounter; i++) {
            uiParamsStructure[i] = uiSlider[i]->value();
        }
    } else {
        switch((dewarp_mode_t)ui->cbDewarpMode->currentIndex()) {
            case EMODE_WM_ZCLCylinder:
                // Fixed parameters
                uiParamsStructure[0] = 950000;
                break;
            case EMODE_WM_Sweep_1PanelEPTZ:
                // Fixed parameters
                uiParamsStructure[0] = -10;
                uiParamsStructure[1] = 0;
                uiParamsStructure[2] = 40;
                uiParamsStructure[3] = 0;
                uiParamsStructure[4] = 1;
                uiParamsStructure[5] = 1;
                uiParamsStructure[6] = 1;
                uiParamsStructure[7] = 1;
                uiParamsStructure[8] = 40;
            default:
                break;
        }
    }

    apiManager->engineSetDewarpParams((video_channel_t)channelsWithDewarp[ui->cbChannel->currentIndex()], ui->cbPanel->currentIndex(),(dewarp_mode_t) ui->cbDewarpMode->currentIndex(),(dewarp_params_t*)uiParamsStructure);
}
