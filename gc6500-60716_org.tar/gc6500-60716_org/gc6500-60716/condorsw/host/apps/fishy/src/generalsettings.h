/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef GENERALSETTINGS_H
#define GENERALSETTINGS_H

#include <QWidget>
#include <QTime>
#include <QTimer>
#include <QProcess>
#include <QHideEvent>

namespace Ui {
    class GeneralSettings;
}

class GeneralSettings : public QWidget
{
    Q_OBJECT

public:
    explicit GeneralSettings(QWidget *parent = 0);
    ~GeneralSettings();
    void setRawId(int);
    void hideEvent(QHideEvent *event);
    void mousePressEvent(QMouseEvent *);
    void renderRawStats(bool val);

    void setSingleChannelLayoutEnabled(bool);
    void setMultiChannelLayoutEnabled(bool);
    void selectSingleChannelLayout();
    void selectMultipleChannelLayout();
    void selectEPTZLayout();
    void selectPrivacyLayout();

    void setRawButtonEnabled(bool enabled);

    void setEptzLayoutEnabled(bool);

    void setPrivacyEnabled(bool);

    void setAudioEnabled(bool);

private:
    Ui::GeneralSettings *ui;

    QString queryDumpFile;

    bool singleChannelLayoutEnabled;
    bool multipleChannelLayoutEnabled;
    bool eptzLayoutEnabled;
    bool privacyEnabled;
    bool audioEnabled;

    void toggleNonLayoutButtons(bool enable);

signals:
    void settingsExitClicked(void);
    void signalOnDewarpClicked(void);
    void settingsCameraSettingsClicked(void);
    void settingsMicButtonClicked(void);
    void settingsLayoutClicked(void);
    void signalOnRawClicked();
    void signalDumpComplete(bool success=false);
    void signalOnEptzClicked(void);
    void signalOnPrivacyClicked();

    void signalSingleChannelLayout();
    void signalMultiChannelLayout();

    void signalVideoRecordClicked(void);

public slots:
    void deselectRawButton();
    void setVideoRecordingState(bool state);

private slots:
    void settingsAboutClicked();
    void selectRawButton();

    void onSingleChannelLayoutClicked();
    void onMultipleChannelLayoutClicked();
    void onEptzLayoutClicked();
    void onPrivacyClicked();
};

#endif // GENERALSETTINGS_H
