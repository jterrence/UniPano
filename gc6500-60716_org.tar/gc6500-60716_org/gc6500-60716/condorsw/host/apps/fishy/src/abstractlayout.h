/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef ABSTRACTLAYOUT_H
#define ABSTRACTLAYOUT_H

#ifdef WITH_ENGINE
#include "interface.h"
#endif

typedef struct{
    unsigned int fps:1;
    unsigned int bitrate:1;
    unsigned int framerate:1;
    unsigned int video:1;
    unsigned int audio:1;
    unsigned int startallvideo:1;
    unsigned int numChannelsPerPage:3;
    unsigned int overlay:1;
    unsigned int overlayImage:1;
    unsigned int numOverlayText:3;
    unsigned int overlayText:1;
    unsigned int eptz:1;
    unsigned int upload360:1;
    unsigned int render:1;
    unsigned int timeBps;
    unsigned int timeBpsMinMax;
    char *querydump;
}fields;

typedef struct{
    int chid;
    int fps;
    int bitrate;
    fields prop;
}bootProperties;

class AbstractLayout
{
public:
    virtual void setChannelValues(int chid)=0;
    virtual void onF1KeyPressed()=0;
    virtual void startVideo(int ch)=0;
    virtual void stopVideo(int ch)=0;
    virtual void setSettingsVisible(bool visible)=0;
    virtual void setAutoStartVideo(bool autoStartVideo)=0;
};

#endif // ABSTRACTLAYOUT_H
