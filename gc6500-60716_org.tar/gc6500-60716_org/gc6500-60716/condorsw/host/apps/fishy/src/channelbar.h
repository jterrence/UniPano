/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef CHANNELBAR_H
#define CHANNELBAR_H

#include <QWidget>

namespace Ui {
    class ChannelBar;
}

class ChannelBar : public QWidget
{
    Q_OBJECT

public:
    explicit ChannelBar(QWidget *parent = 0);
    ~ChannelBar();
    void setNumButtons(int val);

protected:
    void resizeEvent(QResizeEvent*);

private:
    Ui::ChannelBar *ui;
    int numButtons;
    int buttonIndex;

    void updateButtonStates();

private slots:
    void onPageClicked(int index);
    void onPagePreviousClicked();
    void onPageNextClicked();

signals:
    void signalPageChanged(int);
    void signalPagePrev();
    void signalPageNext();
    void signalResized();
};

#endif // CHANNELBAR_H
