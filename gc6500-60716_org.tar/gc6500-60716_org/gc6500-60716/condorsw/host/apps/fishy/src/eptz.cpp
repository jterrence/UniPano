/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include "eptz.h"
#include <QGridLayout>
#include <QtGui/QApplication>
#include <QDesktopWidget>
#include "log.h"
#include <QSlider>
#include <eptzcontrols.h>

#ifdef WITH_ENGINE
#ifdef __i386__
    #include "ePTZ_eWARP.h"
#endif
#endif

const int SOURCE_WIDTH = 1280;
const int SOURCE_HEIGHT = 720;

EPTZ::EPTZ(QWidget *parent) :
    QWidget(parent)
{
    // setup layout
    int screenWidth = qApp->desktop()->screenGeometry().width();
    int screenHeight = qApp->desktop()->screenGeometry().height();

    QGridLayout* gridLayout = new QGridLayout;
    gridLayout->setSizeConstraint (QLayout::SetMaximumSize);
    gridLayout->setSpacing(5);
    gridLayout->setContentsMargins(2, 2, 2, 2);

    eptzChannel = new DisplayChannel(parent);
    eptzChannel->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    eptzChannel->setMinimumSize(screenWidth-320-10, screenHeight-50);
    eptzChannel->setMaximumSize(screenWidth-320-10, screenHeight-50);
    gridLayout->addWidget(eptzChannel, 0, 0, 2, 1, Qt::AlignBottom);

    refChannel = new DisplayChannel(parent);
    refChannel->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    refChannel->setMinimumSize(320, 240);
    refChannel->setMaximumSize(320, 240);
    gridLayout->addWidget(refChannel, 1, 1, 1, 1, Qt::AlignBottom|Qt::AlignRight);

    QGridLayout* gridControlsLayout = new QGridLayout;
    gridControlsLayout->setSizeConstraint (QLayout::SetMaximumSize);
    gridControlsLayout->setSpacing(5);
    gridControlsLayout->setContentsMargins(5, 5, 5, 5);

    EptzControls *eptzControls = new EptzControls(parent);
    eptzControls->setMaximumWidth(300);
    eptzControls->setMinimumWidth(300);
    eptzControls->setMinimumHeight(250);
    eptzControls->setMaximumHeight(250);
    eptzControls->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    connect(eptzControls, SIGNAL(signalOnEPTZ(int,int,int,int,int)),this,SLOT(onEPTZ(int,int,int,int,int)));
    //gridControlsLayout->addWidget(eControls, 5, 0, 1, 3, Qt::AlignCenter);

    //gridLayout->addWidget(eptzControls, 0, 1, 1, 1, Qt::AlignCenter|Qt::AlignBottom);
    gridLayout->addWidget(eptzControls, 0, 1, 1, 1, Qt::AlignCenter|Qt::AlignBottom);

    eptzChannel->setChannel(1);
    eptzChannel->setHandleMouseEvents(false);
    refChannel->setChannel(0);
    refChannel->setHandleMouseEvents(false);

    this->setLayout(gridLayout);

    zoom = 1;
    hPan = 0;
    vPan = 0;
    tilt = 0;
    mode = 0;
    oldChannel = CH1;
    oldPanel = 0;
    oldMode = EMODE_OFF;

    apiManager = new EngineApiManager(0, "eptz");
    apiManager->startThread();
    connect(apiManager, SIGNAL(signalEngineGetDewarpParamsSuccess(video_channel_t,int,dewarp_mode_t,dewarp_params_t)), this, SLOT(onDewarpGetSuccess(video_channel_t,int,dewarp_mode_t,dewarp_params_t)));
    connect(apiManager, SIGNAL(signalEngineGetDewarpParamsFailed(video_channel_t,int)), this, SLOT(onDewarpGetFailed(video_channel_t,int)));

    connect(eptzChannel, SIGNAL(signalOnVideoStarted(video_channel_t)),this,SLOT(onVideoStarted(video_channel_t)));

    //setFocusPolicy(Qt::StrongFocus);
    eptzControls->setFocusPolicy(Qt::StrongFocus);
    eptzControls->setFocus();
}

EPTZ::~EPTZ() {
    cleanup();
}

void EPTZ::showEvent(QShowEvent *) {
    //setFocus();
}

void EPTZ::keyPressEvent(QKeyEvent *event) {
    switch(event->key())
    {
        case Qt::Key_Up:
        case Qt::Key_PageUp: {
            qDebug() << "Key Up";
        }
            break;
        case Qt::Key_Down:
        case Qt::Key_PageDown: {
            qDebug() << "Key Down";
        }
            break;
        default:
            break;
    }
}

void EPTZ::performEPTZ() {
    dewarp_params_t params;
    if (mode == 0) {
        params.eptz_mode_wm_zclstretch.HPan = hPan;
        params.eptz_mode_wm_zclstretch.VPan = vPan;
        params.eptz_mode_wm_zclstretch.Zoom = zoom;
        params.eptz_mode_wm_zclstretch.Divisor = 0;

        apiManager->engineSetDewarpParams(CH2, 0, EMODE_WM_ZCLStretch, &params);
    } else {
        params.eptz_mode_wm_1paneleptz.HPan = hPan;
        params.eptz_mode_wm_1paneleptz.VPan = vPan;
        params.eptz_mode_wm_1paneleptz.Tilt = tilt;
        params.eptz_mode_wm_1paneleptz.Zoom = zoom;
        params.eptz_mode_wm_1paneleptz.Divisor = 0;
        params.eptz_mode_wm_1paneleptz.XCenter = 0;
        params.eptz_mode_wm_1paneleptz.YCenter = 0;
        params.eptz_mode_wm_1paneleptz.HeightFromCenter = 0;

        apiManager->engineSetDewarpParams(CH2, 0, EMODE_WM_1PanelEPTZ, &params);
    }

    if (refChannel->isVideoRunning()) {
        drawDewarpMap();
    }
}

void EPTZ::cleanup() {
    // This is a workaround for EMODE_WM_1PanelEPTZ since the dewarp gets corrupted if the following values are not set to 0
    if (oldMode == EMODE_WM_1PanelEPTZ) {
        oldParams.eptz_mode_wm_1paneleptz.Divisor = 0;
        oldParams.eptz_mode_wm_1paneleptz.XCenter = 0;
        oldParams.eptz_mode_wm_1paneleptz.YCenter = 0;
        oldParams.eptz_mode_wm_1paneleptz.HeightFromCenter = 0;
    }
    engine_set_dewarp_params(oldChannel, oldPanel, oldMode, &oldParams);
    //apiManager->engineSetDewarpParams(oldChannel, oldPanel, oldMode, &oldParams);

    //delete eptzChannel;
    //delete refChannel;

    delete apiManager;
}

// Slots

void EPTZ::onVideoStarted(video_channel_t channel) {
    if (channel == (video_channel_t)eptzChannel->getChannel()) {
        apiManager->engineGetDewarpParams(channel, 0);
    } else {
        drawDewarpMap();
    }
}

void EPTZ::onDewarpGetSuccess(video_channel_t channel, int panel, dewarp_mode_t mode, dewarp_params_t params) {
    oldChannel = channel;
    oldPanel = panel;
    oldMode = mode;
    oldParams = params;

    performEPTZ();
}

void EPTZ::onDewarpGetFailed(video_channel_t channel, int panel) {
    qWarning()<<"Dewarp failed for channel"<<channel<<panel;
}

void EPTZ::onEPTZ(int mode, int zoom, int hpan, int vpan, int tilt) {
    qDebug() << mode << zoom << hpan << vpan << tilt;
    this->mode = mode;
    this->zoom = zoom;
    this->hPan = hpan;
    this->vPan = vpan;
    this->tilt = tilt;

    performEPTZ();
}

void EPTZ::drawDewarpMap() {
#ifdef __i386__
    int eptzMode = mode==0?112:120;
    eptz_handle_struct* handle = eptz_init(eptzMode, SOURCE_WIDTH, SOURCE_HEIGHT);
    if (handle) {
        handle->param[7] = eptzMode;
        handle->param[0] = hPan;
        handle->param[1] = vPan;
        handle->param[6] = zoom;
        if (eptzMode == 120) {
            handle->param[9] = tilt;
        }

        QString mapPath = "/tmp/eptz.txt";
        qDebug()<<refChannel->getChannelWidth()<<refChannel->getChannelHeight();
        eptz_generate_gridmap(handle, refChannel->getChannelWidth(), refChannel->getChannelHeight(),refChannel->getChannelWidth(),refChannel->getChannelHeight(),40, handle->param, NULL, (char*)mapPath.toStdString().c_str());
        eptz_deinit(handle);

        QFile file(mapPath);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            qCritical()<<mapPath<<": File open failed";
            return;
        }

        refChannel->setDewarpMapEnable(true);

        QList<QPoint> points;
        while (!file.atEnd()) {
            QByteArray line = file.readLine();
            QString value(line);
            QStringList list2 = value.split(",");
            // list2: [ "a", "b", "c" ]
            QByteArray ba = list2.at(0).toLatin1();
            int x = atoi(ba.data());
            ba = list2.at(1).toLatin1();
            int y= atoi(ba.data());
            QPoint p(x,y);

            points.append(p);
        }
        refChannel->setMapPoints(points);
    }
#endif
}
