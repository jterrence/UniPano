/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include <unistd.h>
#include <QDialog>
#include <QMainWindow>
#include <QVideoSurfaceFormat>
#include <QTime>
#include <QMenu>
#include <QMessageBox>
#include <QDesktopWidget>
#ifdef QT_OPENGL
# include <QtOpenGL/QGLWidget>
#endif
#include "displaychannel.h"
#include "ui_displaychannel.h"
#include "interface.h"
#include "cameraconfig.h"
#include "alerthandler.h"
#include "stringutils.h"
#include "scrollablewindow.h"
#include "mainwindow.h"
#include <QScrollBar>
#include "log.h"
#include <QFileInfo>
#include "overlaymanager.h"
#include "overlaytask.h"
#include <channelstate.h>

#ifdef QT5
#include <QtConcurrent/QtConcurrentRun>
#else
#include <QtConcurrentRun>
#endif

DisplayChannel::DisplayChannel(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DisplayChannel),
    isPopOut(false),
    popoutLabel(NULL),
    overlayEnabled(false),
    transparentImage(NULL)
{
    ui->setupUi(this);

    channel = -1;
    buf = NULL;
    handleMouseEvents = true;
    videoSettings = NULL;
    autoStartVideo = true;
    popoutChannel = NULL;
    scrollableParentWindow = NULL;
    bStatsEnabled = false;
    bStartVideoAfterGettingChannelInfo = false;
    videoEnabled = true;
    render = true;
    stopVideoOnDestroy = true;
    videoSettingsPositioned = false;
    reduceRender = false;
    optimize = false;
    test = 0;
    numFramesReceived = 0;
    timeBitrateMinMax = 10;

    // Buffer fullness
    plot = NULL;
    numFramesForBufferFullness = 0;
    bufferFullnessEnabled = false;

    renderSurface = new VideoRenderSurface(this);
#ifdef QT_OPENGL
    renderSurface->setViewport(new QGLWidget(QGLFormat(QGL::SingleBuffer | QGL::DirectRendering)));
    //renderSurface->setViewportUpdateMode(QGraphicsView::NoViewportUpdate);
    //renderSurface->setCacheMode(QGraphicsView::CacheBackground);
#endif

#ifdef WITH_MOTION_RECTS
    motionRegion = (Motion*)malloc(sizeof(Motion));
#endif

    videoItem = new VideoItem;
    connect(videoItem, SIGNAL(signalPlotGraph()), this, SLOT(plotGraph()), Qt::DirectConnection);
    videoItem->hide();

    renderSurface->scene->addItem(videoItem);

    clickTimer = new QTimer(this);
    clickTimer->setSingleShot(true);
    clickTimer->setInterval(200);
    connect(clickTimer, SIGNAL(timeout()), this, SLOT(handleSingleClick()));

    renderSurface->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    renderSurface->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    ui->horizontalLayout->addWidget(renderSurface);

    //This is to optimize the painting which starts avoiding any unnecessary overhead associated with repainting the widget's background.
    this->setAttribute(Qt::WA_OpaquePaintEvent, false);

    cbSmoothScaling = new QCheckBox(renderSurface);
    cbSmoothScaling->setFont(QFont("Arial", 11));
    //cbSmoothScaling->setStyleSheet(QString("QCheckBox {font: ").append(QString::number(int(parent->width()*0.04))).append("px \"Arial\"}"));
    connect(cbSmoothScaling,SIGNAL(clicked(bool)),this,SLOT(enableSmoothScaling(bool)));
    cbSmoothScaling->setText(tr("Accelerate"));
    cbSmoothScaling->setFocusPolicy(Qt::NoFocus);
    cbSmoothScaling->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
    cbSmoothScaling->hide();

    fps = 0;
    bps = 0;

    //this->setFocusPolicy(Qt::StrongFocus);

    if (QString(this->parentWidget()->metaObject()->className()).compare("ScrollableWindow") == 0) {
        isPopOut = true;
        scrollableParentWindow = (ScrollableWindow*)this->parentWidget();
        connect(scrollableParentWindow->horizontalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(positionToolbar()));
        connect(scrollableParentWindow->verticalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(positionToolbar()));
        connect(scrollableParentWindow->horizontalScrollBar(), SIGNAL(rangeChanged(int, int)), this, SLOT(onScrollWindowResized(int, int)));
        connect(scrollableParentWindow->verticalScrollBar(), SIGNAL(rangeChanged(int, int)), this, SLOT(onScrollWindowResized(int, int)));
    } else {
        popoutLabel = new QLabel(this);
        popoutLabel->setText("Channel popped out");
        popoutLabel->setFont(QFont("Arial", 12, QFont::Bold, true));
        popoutLabel->setAlignment(Qt::AlignCenter);
        popoutLabel->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
        popoutLabel->hide();
    }

    apiManager = new EngineApiManager(0, "dchannel");
    //apiManager->startThread();
    connect(apiManager, SIGNAL(signalEngineGetChannelInfoSuccess(video_channel_t,video_channel_info_t)), this, SLOT(onEngineGetChannelInfoSuccess(video_channel_t,video_channel_info_t)), Qt::DirectConnection);
    connect(apiManager, SIGNAL(signalEngineGetChannelInfoFailed(video_channel_t,int)), this, SLOT(onEngineGetChannelInfoFailed(video_channel_t,int)));
    connect(apiManager, SIGNAL(signalEngineResolutionNotSupported()), this, SLOT(onEngineResolutionNotSupported()));
    connect(apiManager, SIGNAL(signalEngineSetResolutionSuccess(int,int)), this, SLOT(onEngineSetResolutionSuccess(int,int)));
    connect(apiManager, SIGNAL(signalEngineSetResolutionFailed()), this, SLOT(onEngineSetResolutionFailed()));
    connect(apiManager, SIGNAL(signalEngineSetVideoFormatSuccess()), this, SLOT(onEngineSetVideoFormatSuccess()));
    connect(apiManager, SIGNAL(signalEngineSetVideoFormatFailed()), this, SLOT(onEngineSetVideoFormatFailed()));

    task = new OverlayTask(this);
    QObject::connect(task, SIGNAL(signalTaskDestroyed()), this, SIGNAL(signalCleanupDone()));
    QObject::connect(task, SIGNAL(signalAddOverlayImageFailed(int)), this, SLOT(onAddOverlayImageFailed(int)));
    QObject::connect(task, SIGNAL(signalAddOverlayTextFailed(int,int)), this, SLOT(onAddOverlayTextFailed(int,int)));
    QObject::connect(task, SIGNAL(signalAddOverlayTimeFailed(int)), this, SLOT(onAddOverlayTimeFailed(int)));
    //task->startThread(this);

    /*thread = new QThread;
    QObject::connect (this, SIGNAL(destroyed()), task, SLOT(deleteLater()));
    QObject::connect (task, SIGNAL(destroyed()), thread, SLOT(quit()));
    QObject::connect (thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    qDebug() << "Current thread id **: " << QThread::currentThreadId();
    task->moveToThread(thread);
    thread->start();*/

    setTransparentFrameTimer.setSingleShot(true);
    setTransparentFrameTimer.setInterval(200);
    connect(&setTransparentFrameTimer,SIGNAL(timeout()),this,SLOT(startTransparentVideoItem()));

    popupTimer.setSingleShot(true);
    popupTimer.setInterval(200);
    connect(&popupTimer,SIGNAL(timeout()),this,SLOT(openPopup()));

    this->setObjectName("DisplayChannel");
}

void DisplayChannel::cleanup() {
    emit signalCleanupDone();

    /*if (OverlayManager::shared()->isOverlayTextEnabled(channel)) {
        task->cleanup(channel);
    } else {
        emit signalCleanupDone();
    }*/
}

DisplayChannel::~DisplayChannel()
{
    qDebug() << "Destroy" << this->objectName();

    if (scrollableParentWindow) {
        disconnect(task, SIGNAL(signalTaskDestroyed()), this, SIGNAL(signalCleanupDone()));
        task->cleanup(-1);
    }

    if (apiManager != NULL) {
        apiManager->cleanup();
    }

    if (channel >= 0 && this->videoItem != NULL && this->videoItem->isVisible()) {
        stopRendering(true);
    }

    if(buf != NULL) {
        free(buf);
        buf = NULL;
    }

    deleteBufferGraph();

    videoItem = NULL;
    videoSettings = NULL;

    if (popoutChannel != NULL) {
        disconnect((ScrollableWindow*)popoutChannel->parentWidget()->parentWidget(), SIGNAL(destroyed()), this, SLOT(onPopoutDestroyed()));
        ((ScrollableWindow*)popoutChannel->parentWidget()->parentWidget())->close();
        delete popoutChannel;
    }

    if (setTransparentFrameTimer.isActive()) {
        setTransparentFrameTimer.stop();
    }

    delete cbSmoothScaling;

#ifdef WITH_MOTION_RECTS
    if (motionRegion != NULL) {
        delete motionRegion;
    }
#endif

    if (popoutLabel != NULL) {
        delete popoutLabel;
    }

    delete ui;
}

void DisplayChannel::keyPressEvent(QKeyEvent *event) {
    switch(event->key())
    {
        case Qt::Key_Up:
        case Qt::Key_PageUp:
            break;
        case Qt::Key_Down:
        case Qt::Key_PageDown:
            break;
        case Qt::Key_Escape:
            // Hide toolbar
            setToolbarVisible(false, false);
            event->ignore();
            break;
        default:
            break;
    }
}

#if 1
bool DisplayChannel::event(QEvent *event)
{
    switch(event->type())
    {
        case QEvent::MouseButtonPress:

            if (handleMouseEvents == false) {
                break;
            }

            if (popoutChannel != NULL) {
                qDebug()<<"This channel has been popped out. Do not handle MouseButtonPress";
                break;
            }

            if(!clickTimer->isActive()){
                clickTimer->start();
            }

            event->accept();
            return true;

        case QEvent::MouseButtonDblClick:

            if (handleMouseEvents == false) {
                break;
            }

            // Stop the timer so that handleSingleClick is not fired.
            clickTimer->stop();

            if (isPopOut) {
                qDebug()<<"This is a popped out channel. Do not handle MouseButtonDblClick";
                break;
            }

            if (popoutChannel != NULL) {
                qDebug()<<"This channel has been popped out. Do not handle MouseButtonDblClick";
                break;
            }

            if (videoEnabled == false) {
                qDebug()<<"Video is disabled. Do not handle MouseButtonDblClick";
                break;
            }

            handleDoubleClick();
            event->accept();
            return true;

        case QEvent::Hide:
            close();
            event->accept();
            return true;
        case QEvent::Show:
        case QEvent::WindowActivate:
        {
            if(event->type() == QEvent::Show) {
                if (channel >= 0) {

                    cbSmoothScaling->blockSignals(true);
                    cbSmoothScaling->setChecked(ChannelState::shared()->getOptimizeEnabled(channel));
                    cbSmoothScaling->blockSignals(false);

                    bStartVideoAfterGettingChannelInfo = autoStartVideo;
                    this->getChannelInfo();
                }
            }

            if(channel >= 0 && scrollableParentWindow == NULL) {
                cbSmoothScaling->hide();
                cbSmoothScaling->raise();
            } else {
                cbSmoothScaling->hide();
            }

            if (popoutLabel != NULL) {
                popoutLabel->setGeometry((this->width()-200)/2, (this->height()-30)/2,200,30);
            }
        }
            break;
#if 1
        case QEvent::Resize:
        {
            mResized = true;
            cbSmoothScaling->setGeometry(5, this->height()-24,cbSmoothScaling->width(),20);

            if (scrollableParentWindow != NULL) {
                if (videoSettings != NULL && videoSettings->isVisible()) {
                    positionToolbar();
                }
            }
            break;
        }
#endif
        default:
            break;
    }
    event->ignore();
    return true;
}
#endif

void DisplayChannel::setChannel(int channel) {
    this->channel = channel;

    if (channel > -1) {
        videoItem->setChannel(channel);
        cbSmoothScaling->show();
        if (this->isVisible()) {
            bStartVideoAfterGettingChannelInfo = true;
            this->getChannelInfo();
        }
    } else {
        cbSmoothScaling->hide();
    }
}

void DisplayChannel::setAutoStartVideo(bool autoStartVideo) {
    this->autoStartVideo = autoStartVideo;
    this->videoEnabled = autoStartVideo;
    if (videoSettings != NULL) {
        videoSettings->setVideoEnabled(autoStartVideo);
    }
}

void DisplayChannel::setStopVideoOnDestroy(bool stop) {
    this->stopVideoOnDestroy = stop;
}

void DisplayChannel::setRender(bool render) {
    this->render = render;
    videoItem->setRender(render);
}

void DisplayChannel::setOptimize(bool optimize) {
    this->optimize = optimize;
    videoItem->setConvertYuvToRgb(optimize);
}

void DisplayChannel::setOverlayEnabled(bool enabled) {
    this->overlayEnabled = enabled;
}

void DisplayChannel::setStatsEnabled(bool enabled) {
    bStatsEnabled = enabled;

    if (this->videoItem != NULL) {
        this->videoItem->setStatsEnabled(bStatsEnabled);
    }

    if (bStatsEnabled == false) {
        deleteBufferGraph();
    }
}

void DisplayChannel::setDrawTitle(bool drawTitle) {
    videoItem->setDrawTitle(drawTitle);
}

void DisplayChannel::startVideo()
{
    if (channel < 0) {
        qDebug() << this->objectName() <<  ": Do not start video for channel " << StringUtils::shared()->channelToString((video_channel_t)channel);
        return;
    }

    qDebug() << "Start video for channel" << StringUtils::shared()->channelToString((video_channel_t)channel) << "in" << this->objectName();

    videoItem->setVisible(true);

#ifdef WITH_ENGINE
    if(buf == NULL)
    {
        m_size = this->getChannelWidth()*this->getChannelHeight()*4;
        buf = (unsigned char*) malloc(m_size);
        decodedWidth = (int*) malloc(sizeof(int));
        decodedHeight = (int*) malloc(sizeof(int));

        *decodedWidth = 0;
        *decodedHeight = 0;
    }

    m_receiver = new EngineReceiver();

    qRegisterMetaType<Stats>("Stats");
    qRegisterMetaType<Stats*>("Stats*");
    
    connect(m_receiver, SIGNAL(signalFrameReceived()), this, SLOT(presentImage()), Qt::AutoConnection);
    connect(m_receiver, SIGNAL(signalStatsReceived(Stats*)), this, SLOT(renderStats(Stats*)), Qt::AutoConnection);

#ifdef WITH_MOTION_RECTS
    registeredCallback(render?DisplayChannel::renderCallback:NULL,this,channel,(char*)buf, decodedWidth, decodedHeight, m_size, DisplayChannel::updateStats, DisplayChannel::updateMotion);
#else
    registeredCallback(render?DisplayChannel::renderCallback:NULL,this,channel,(char*)buf, m_size, DisplayChannel::updateStats, NULL);
#endif

    videoItem->timeBitrateMinMax = timeBitrateMinMax;

    if (engine_startvideo(channel) == 0) {
        if (optimize) {
            engine_set_optimize(channel, optimize);
        }

        qDebug()<<StringUtils::shared()->channelToString((video_channel_t)channel)<<"started successfully";
        emit(signalOnVideoStarted((video_channel_t)channel));

        if (!render) {
            startTransparentVideoItem();
        }

        // if (overlayEnabled && OverlayManager::shared()->isOverlayImageEnabled(channel)) {
        //     task->resetOverlayImage(channel);
        // }

    } else {
        qDebug()<<"Channel"<<StringUtils::shared()->channelToString((video_channel_t)channel)<<"failed to start";
    }
#endif
}

void DisplayChannel::stopVideo() {
    if (popoutChannel != NULL) {
        disconnect((ScrollableWindow*)popoutChannel->parentWidget()->parentWidget(), SIGNAL(destroyed()), this, SLOT(onPopoutDestroyed()));
        ((ScrollableWindow*)popoutChannel->parentWidget()->parentWidget())->close();
        delete popoutChannel;
        popoutChannel = NULL;
    } else {
        setToolbarVisible(false, false);
        stopRendering();
    }
}

void DisplayChannel::openPopup() {
    ScrollableWindow *scrollableWindow = new ScrollableWindow(0);
    scrollableWindow->setMainWindow((MainWindow*)this->parentWidget()->parentWidget());

    popoutChannel = new DisplayChannel(scrollableWindow);
    popoutChannel->setObjectName("Popout");
    popoutChannel->setChannel(channel);
    popoutChannel->setStatsEnabled(bStatsEnabled);
    popoutChannel->setOverlayEnabled(overlayEnabled);
    popoutChannel->setOptimize(optimize);
    popoutChannel->setRender(render);
    popoutChannel->setTimeBitrateMinMax(timeBitrateMinMax);

    scrollableWindow->setWidget(popoutChannel);
    connect(scrollableWindow, SIGNAL(signalResized()), this, SLOT(onPopoutShown()));
    connect(scrollableWindow, SIGNAL(destroyed()), this, SLOT(onPopoutDestroyed()));

    scrollableWindow->show();
}

void DisplayChannel::handleDoubleClick()
{
    if (videoItem != NULL && videoItem->isVisible()) {
        videoSettingsPositioned = false;
        setToolbarVisible(false);
        stopRendering();

        popupTimer.start();
    }
}

void DisplayChannel::resizeScrollableParentWindow()
{
    int width = this->getChannelWidth()+5;
    int height = this->getChannelHeight()+5;

    if (width > qApp->desktop()->screenGeometry().width()) {
        width = qApp->desktop()->screenGeometry().width();
        height = width * this->getChannelHeight()/this->getChannelWidth();
    }

    if (height > qApp->desktop()->screenGeometry().height()) {
        height = qApp->desktop()->screenGeometry().height();
        width = height * this->getChannelWidth()/this->getChannelHeight();
    }

    int minWidth = 640;
    int minHeight = 480;
    int maxWidth = width < minWidth ? minWidth+5 : width;
    int maxHeight = height < minHeight ? minHeight+5 : height;

    scrollableParentWindow->setMinimumSize(minWidth, minHeight);
    scrollableParentWindow->setMaximumSize(maxWidth, maxHeight);
    scrollableParentWindow->resize(maxWidth, maxHeight);

    setMinimumSize(scrollableParentWindow->minimumWidth(), scrollableParentWindow->minimumHeight());
    setMaximumSize(scrollableParentWindow->maximumWidth(), scrollableParentWindow->maximumHeight());
    //resize(scrollableParentWindow->maximumWidth()-10, scrollableParentWindow->maximumHeight()-10);
    resize(this->getChannelWidth(), this->getChannelHeight());

    //scrollableParentWindow->window()->setWindowTitle(StringUtils::shared()->channelToString((video_channel_t)this->getChannel()) + " (" + QString::number(this->getChannelWidth()) + QString(" x ") + QString::number(this->getChannelHeight()) + ")");
    scrollableParentWindow->window()->setWindowTitle(tr("%1 (%2 x %3)")
                                                     .arg(StringUtils::shared()->channelToString((video_channel_t)this->getChannel()))
                                                     .arg(QString::number(this->getChannelWidth()))
                                                     .arg(QString::number(this->getChannelHeight())));
}

void DisplayChannel::handleSingleClick() {
    if (videoSettings == NULL || videoItem == NULL) {
        return;
    }

    if(videoSettings->isVisible()) {
        setToolbarVisible(false);
    } else {
        if (!videoSettingsPositioned) {
            positionToolbar();
        }
        setToolbarVisible(true);
    }

    /*if (plot != NULL and plot->isVisible()) {
        int width = isPopOut ? scrollableParentWindow->width() : this->width();
        plot->setGeometry(width-plot->rect().width(), 0, plot->rect().width(), plot->rect().height());
        //plot->setWindowOpacity(0.1);
        //plot->setBackground(QBrush(QColor(255, 255, 255, 100)));
    }

    //plot->setVisible(!plot->isVisible());*/
}

void DisplayChannel::onScrollWindowResized(int, int) {
    setToolbarVisible(false);
}

void DisplayChannel::positionToolbar() {
    if (videoSettingsPositioned && !isPopOut) {
        QPoint lpoint(videoSettings->x(), videoSettings->y());
        QPoint mpoint = mapToParent(lpoint);
        int overlap = (mpoint.y()+videoSettings->height())-(qApp->desktop()->screenGeometry().height()-((MainWindow*)this->parentWidget()->parentWidget())->getToolbarHeight());
        if (((MainWindow*)this->parentWidget()->parentWidget())->isToolbarVisible()) {
            if (overlap > 0) {
                videoSettings->setGeometry(videoSettings->x(),videoSettings->y()-overlap-2,videoSettings->width(),videoSettings->height());
            }
        } else {
            if (!videoSettings->hasBeenMoved()) {
                // If the user has not moved the toolbar re-position it
                videoSettings->setGeometry(videoSettings->x(),this->height()-videoSettings->height()-2,videoSettings->width(),videoSettings->height());
            }
        }
        return;
    }

    int width = isPopOut ? scrollableParentWindow->width()-5 : this->width();
    int height = isPopOut ? scrollableParentWindow->height()-5 : this->height();

    int x = (width/2) - (videoSettings->width()/2);
    int y = (height) - (videoSettings->height());

    if(width < videoSettings->width()) {
        // If the video settings toolbar width is greater than the parent channel display width, set its width to 5 pixels lesser than its parent
        width = this->width() - 5;
    } else {
        width = videoSettings->width();
    }

    if(height < videoSettings->height()) {
        height = this->height();
    } else {
        height = videoSettings->height();
    }

    if(x < 0) {
        x = 0;
    }
    if(y < 0) {
        y = 0;
    }

    if (isPopOut) {
        const QScrollBar *hbar = scrollableParentWindow->horizontalScrollBar();
        const QScrollBar *vbar = scrollableParentWindow->verticalScrollBar();

        x += hbar->value();
        y += vbar->value();
        if (hbar->isVisible()) {
            y -= hbar->height();
        }

        videoItem->yStatsPos = vbar->value();
        videoItem->hStatsPos = hbar->value();
    }

    if (!isPopOut && ((MainWindow*)this->parentWidget()->parentWidget())->isToolbarVisible()) {
        QPoint lpoint(x, y);
        QPoint mpoint = mapToParent(lpoint);
        int overlap = (mpoint.y()+height)-(qApp->desktop()->screenGeometry().height()-((MainWindow*)this->parentWidget()->parentWidget())->getToolbarHeight());
        if (overlap > 0) {
            y -= overlap;
        }
    }

    videoSettings->setGeometry(x,y-2,width,height);
    videoSettingsPositioned = true;
}

void DisplayChannel::wheelEvent(QWheelEvent *event)
{
    event->ignore();
}

void DisplayChannel::resizeEvent(QResizeEvent *event)
{
    setToolbarVisible(false);
    event->ignore();
}

void DisplayChannel::onPopoutShown() {
    popoutLabel->show();
}

void DisplayChannel::onPopoutDestroyed()
{
    if (videoItem == NULL) {
        return;
    }

    if (popoutLabel != NULL) {
        popoutLabel->hide();
    }

    cbSmoothScaling->setEnabled(true);

    bStartVideoAfterGettingChannelInfo = true;
    getChannelInfo();
    //startVideo();

    popoutChannel = NULL;
}

void DisplayChannel::deleteBufferGraph() {
    if (plot != NULL) {
        delete plot;
        plot = NULL;
    }
}

void DisplayChannel::startTransparentVideoItem() {
    resizeVideoItem();

    if (transparentImage == NULL) {
        transparentImage = new QImage(this->getChannelWidth(),this->getChannelHeight(),QImage::Format_ARGB32);
        transparentImage->fill(QColor(255,255,255,10));

        QPainter painter (transparentImage);
        painter.setPen(Qt::transparent);
        painter.setBackground(Qt::transparent);
        painter.drawRect(0,0,channelWidth,channelHeight);
    }

    /*if (transparentFrame == NULL) {
        // Create a transparent frame
        QImage im(this->getChannelWidth(),this->getChannelHeight(),QImage::Format_ARGB32);
        im.fill(QColor(255,255,255,10));

        QPainter painter (&im);
        painter.setPen(Qt::transparent);
        painter.setBackground(Qt::transparent);
        painter.drawRect(0,0,channelWidth,channelHeight);

        transparentFrame = new QVideoFrame(*transparentImage);
    }*/

    QVideoSurfaceFormat format(transparentImage->size(), QVideoFrame::Format_ARGB32);
    if (!videoItem->start(format)) {
        qWarning()<<"Video Item start failed";
        return;
    }
    videoItem->data = NULL;
    videoItem->setImage(*transparentImage);
}

void DisplayChannel::resizeVideoItem() {
    int surfaceWidth = renderSurface->rect().width();
    int surfaceHeight = renderSurface->rect().height();

    //qDebug()<<"Channel width:"<<getChWidth()<<"Channel height: "<<getChHeight()<< "  surfaceWidth: "<< surfaceWidth <<"  surfaceHeight: "<<surfaceHeight;

    int dwidth = getChannelWidth();
    int dheight = getChannelHeight();

    if (dwidth > surfaceWidth) {
        dwidth = surfaceWidth;
        dheight = dwidth * getChannelHeight()/getChannelWidth();
    }

    if (dheight > surfaceHeight) {
        dheight = surfaceHeight;
        dwidth = dheight * getChannelWidth()/getChannelHeight();
    }

    qDebug()<<"Display width"<<dwidth<<", Display height"<< dheight << "surfaceWidth" << surfaceWidth << surfaceHeight;

    //renderSurface->setGeometry(0, 0, dwidth, dheight);
    //renderSurface->setAlignment(Qt::AlignHCenter);
    //renderSurface->setBackgroundBrush(QBrush(QColor(255,255,255,10)));
    //renderSurface->setBackgroundBrush(QBrush(Qt::red));

    renderSurface->setSceneRect(0, 0, dwidth, dheight);
    videoItem->setFrameSize(dwidth, dheight);

    //this->setGeometry(geometry().x(), geometry().y(), dwidth, dheight);
    //this->adjustSize();
}

int numFrames = 0;
void DisplayChannel::presentImage()
{
    if(!isVisible()) {
        return;
    }

    if (videoItem == NULL || !videoItem->isVisible()) {
        qDebug() << "VideoItem is null/hidden";
        return;
    }

    //numFramesReceived ++;

    if (reduceRender) {
        numFrames ++;
        if (numFrames % 2) {
            if (numFrames > 1000) {
                numFrames = 0;
            }
            return;
        }
    }

//#ifdef QT_OPENGL
//QCoreApplication::processEvents(QEventLoop::AllEvents);
//#endif

    if((mResized || (videoItem->getviewWidth() == 0)) && isVisible()) {
        resizeVideoItem();
        mResized = false;
    }

    if (!optimize) {

        QImage im((uchar*)buf,this->getChannelWidth(),this->getChannelHeight(),QImage::Format_RGB32);

        if (im.isNull()) {
            qWarning()<<"Image frame is null";
            return;
        }

        if (reduceRender) {
            im = im.scaled(this->getChannelWidth()/4,this->getChannelHeight()/4,Qt::IgnoreAspectRatio,Qt::FastTransformation);
        }

        QVideoFrame frame(im);
        if (!frame.isValid()) {
            qCritical()<<"Video frame not valid";
            return;
        }

        QVideoSurfaceFormat currentFormat = videoItem->surfaceFormat();

        if (frame.pixelFormat() != currentFormat.pixelFormat()
            || frame.size() != currentFormat.frameSize()) {
            QVideoSurfaceFormat format(frame.size(), frame.pixelFormat());

            if (!videoItem->start(format)) {
                qCritical()<<"VideoItem start failed";
                return;
            }
        }

        videoItem->data = NULL;
        videoItem->setImage(im);
    } else {
        QVideoSurfaceFormat currentFormat = videoItem->surfaceFormat();
        if (!videoItem->isActive() || currentFormat.pixelFormat() != QVideoFrame::Format_RGB32) {
            QVideoSurfaceFormat format(QSize(getChannelWidth(), getChannelHeight()), QVideoFrame::Format_RGB32);
            if (!videoItem->start(format)) {
                qCritical()<<"VideoItem start failed";
                return;
            }
        }

        videoItem->data = (void*) buf;
        if (*decodedWidth == 0 || *decodedHeight == 0) {
            videoItem->decodedWidth = channelWidth;
            videoItem->decodedHeight = channelHeight;
        } else {
            if (videoItem->decodedWidth != *decodedWidth || videoItem->decodedHeight != *decodedHeight) {
                videoItem->decodedWidth = *decodedWidth;
                videoItem->decodedHeight = *decodedHeight;
            }
        }
        videoItem->renderYuv();
    }
}

void DisplayChannel::renderCallback(void *ptr)
{
    /*EngineReceiver *self = (EngineReceiver*)ptr;
    QMetaObject::invokeMethod(self, "emitFrameReceived");*/

    DisplayChannel *self = (DisplayChannel*)ptr;
    self->presentImage();

#ifdef PROFILING
    int profiling = QTime::currentTime().msec();
#endif
#ifdef PROFILING
    //qDebug()<<temp->chID<<"Total CB Profiling Time in msec" <<QTime::currentTime().msec()-profiling;
#endif
}

void DisplayChannel::stopRendering(bool destroying)
{
    if (channel < 0) {
        return;
    }

#ifdef WITH_ENGINE
    bool stop = true;
    if (destroying && !stopVideoOnDestroy) {
        stop = false;
    }
    if (stop) {
        if (engine_stopvideo(channel) == 0) {
            qDebug()<<"Channel"<<StringUtils::shared()->channelToString((video_channel_t)channel)<<"video stop success";
        } else {
            qWarning()<<"Channel"<<StringUtils::shared()->channelToString((video_channel_t)channel)<<"video stop failed";
        }
    }
    unRegister(channel);
#endif

    videoItem->stop();
    renderSurface->setSceneRect(0,0,0,0);
    videoItem->setFrameSize(0,0);
    renderSurface->setPalette(QPalette(Qt::black));
    if(buf != NULL) {
        free(buf);
        buf = NULL;
    }
    videoItem->hide();


    if (transparentImage != NULL) {
        delete transparentImage;
        transparentImage = NULL;
    }

    renderSurface->scene->update();
    QTimer::singleShot(200, this, SLOT(deleteBufferGraph()));
}

void DisplayChannel::updateFrameRate(int val)
{
#ifdef WITH_ENGINE
    engine_setvideo_fps(channel,val);
#endif
}

void DisplayChannel::updateBitRate(int val)
{
#ifdef WITH_ENGINE
    engine_setvideo_bps(channel,val);
#endif
}

void DisplayChannel::updateGop(int val)
{
#ifdef WITH_ENGINE
    engine_setvideo_gop(channel,val);
#endif
    videoItem->setGop(val);
}

void DisplayChannel::updateProfile(int val)
{
#ifdef WITH_ENGINE
    engine_setvideo_profile(channel,val);
#endif
    switch(val)
    {
        case PROFILE_BASELINE:
            videoItem->setProfile("baseline");
            break;
        case PROFILE_MAIN:
            videoItem->setProfile("main");
            break;
        case PROFILE_HIGH:
            videoItem->setProfile("high");
            break;
        default:
            break;
    }
}

void DisplayChannel::onResolutionChanged(int width, int height)
{
    this->positionToolbar();

    qDebug() << "Update Resolution for" << StringUtils::shared()->channelToString((video_channel_t)channel)<<"in"<<this->objectName()<<"to"<<width<<"x"<<height;

#ifdef WITH_ENGINE
    if (videoEnabled) {
        stopRendering();
    }
    apiManager->engineSetResolution((video_channel_t)channel, width, height);
#endif

    if (CameraConfig::shared()->getChannelConfigParams(channel)->composite == 1) {
        highlightCurrentPanel(false);
    }
}

void DisplayChannel::updateFormat(int format)
{
#ifdef WITH_ENGINE
    if (videoEnabled) {
        stopRendering();
    }
    apiManager->engineSetVideoFormat((video_channel_t)channel, (video_format_t)format);
#endif
}

void DisplayChannel::updateLevel(int val)
{
    int levVal = 0;
    switch(val)
    {
        case L21:
            levVal = 21;
            break;
        case L22:
            levVal = 22;
            break;
        case L30:
            levVal = 30;
            break;
        case L31:
            levVal = 31;
            break;
        default:
            break;
    }

#ifdef WITH_ENGINE
    engine_setvideo_avc_level(channel,levVal);
#endif
}

void DisplayChannel::updateQuality(int val)
{
#ifdef WITH_ENGINE
    engine_setvideo_compression_quality(channel,val);
#endif
    videoItem->setQuality(val);
}

void DisplayChannel::enableSmoothScaling(bool isEnabled)
{
    //videoItem->setSmoothScaling(isEnabled);
    //emit signalScalingEnabled(isEnabled);

    engine_update_render_callback(channel, NULL);
    ChannelState::shared()->setOptimizeEnabled(channel, isEnabled);
    engine_set_optimize(channel, isEnabled);
    optimize = ChannelState::shared()->getOptimizeEnabled(channel);
    engine_update_render_callback(channel, render?DisplayChannel::renderCallback:NULL);
    //QTimer::singleShot(1000, this, SLOT(setEngineOptimization()));
}

void DisplayChannel::setEngineOptimization() {
    optimize = ChannelState::shared()->getOptimizeEnabled(channel);
    //engine_set_optimize(channel, optimize);
    engine_update_render_callback(channel, render?DisplayChannel::renderCallback:NULL);
}

void DisplayChannel::onBufferFullnessToggled(bool enabled) {
    bufferFullnessEnabled = enabled;
    videoItem->bufferFullnessEnabled = enabled;

    if (!bufferFullnessEnabled) {
        deleteBufferGraph();
    }

    ChannelState::shared()->setBufferFullnessEnabled(channel, enabled);
    engine_enable_buffer_fullness(channel, enabled, ChannelState::shared()->getBufferFullnessDuration(channel));
}

void DisplayChannel::onBufferDurationChanged(int duration) {
    ChannelState::shared()->setBufferFullnessDuration(channel, duration);
    engine_enable_buffer_fullness(channel, ChannelState::shared()->getBufferFullnessEnabled(channel), duration);
}

void DisplayChannel::onRenderToggled(bool enabled) {
    render = enabled;
    ChannelState::shared()->setRenderEnabled(channel, enabled);
    engine_update_render_callback(channel, render?DisplayChannel::renderCallback:NULL);
    videoItem->setRender(render);
    if (!render) {
        setTransparentFrameTimer.start();
    }
}

void DisplayChannel::onVideoEnabled(bool enabled) {
    videoEnabled = enabled;
    ChannelState::shared()->setVideoEnabled(channel, enabled);
    if(videoEnabled) {
        startVideo();
    } else {
        stopRendering();
    }
}

Stats m_copy;
void DisplayChannel::updateStats(void *ptr, Stats *val)
{
    /*EngineReceiver *rec = (EngineReceiver*) ptr;
    m_copy.bps = val->bps;
    m_copy.continuty = val->continuty;
    m_copy.drops = val->drops;
    m_copy.fps = val->fps;
    m_copy.iFrame = val->iFrame;
    m_copy.numFramesDelay = val->numFramesDelay;
    m_copy.usbFrameCaptured = val->usbFrameCaptured;
    m_copy.duration = val->duration;
    QMetaObject::invokeMethod(rec, "emitStatsReceived", Q_ARG(Stats*, &m_copy));*/

    DisplayChannel *self = (DisplayChannel*) ptr;
    self->renderStats(val);

    //QMetaObject::invokeMethod(self, "emitSignalPlotGraph");
}

void DisplayChannel::plotGraph() {
    if (!bufferFullnessEnabled) {
        deleteBufferGraph();
        return;
    }

    if (plot == NULL) {
        plot = new QCustomPlot(this);
        int width = isPopOut ? scrollableParentWindow->width() : this->width();
        plot->setGeometry(width-360, 0, 360, 240);
        //plot->setAttribute(Qt::WA_OpaquePaintEvent);
        //plot->setWindowOpacity(0.5);

        plot->addGraph();

        plot->yAxis->setSubTicks(true);
        plot->yAxis2->setVisible(true);
        plot->yAxis2->setTickLabels(false);
        plot->xAxis2->setVisible(true);
        plot->xAxis2->setTickLabels(false);

        plot->xAxis->grid()->setVisible(true);
        plot->yAxis->grid()->setVisible(true);

        plot->xAxis->setLabel(tr("Frames"));
        plot->yAxis->setLabel(tr("Buffer size"));

        plot->graph(0)->setPen(QPen(QColor(40, 110, 255)));
        //plot->graph(0)->setBrush(QBrush(QColor(0, 20, 255, 200)));
        //plot->setBackground(QBrush(QColor(255, 255, 255, 0)));
        plot->setBackground(Qt::lightGray);
        plot->axisRect()->setBackground(Qt::white);
        plot->setInteractions(/*QCP::iRangeDrag | */QCP::iRangeZoom | QCP::iSelectPlottables);
        plot->setVisible(true);
    }

    if (numFramesForBufferFullness == 0) {
        return;
    }

    QVector<double> xdata(numFramesForBufferFullness, 0), ydata(numFramesForBufferFullness, 0);
    int max = -1;
    for (uint i=0; i<numFramesForBufferFullness; i++) {
        xdata[i] = i+1;
        ydata[i] = bufferFullnessList[i];
        if (ydata[i] > max) {
            max = ydata[i];
        }
    }
    if (max > plot->yAxis->range().upper) {
        plot->yAxis->setRange(0, max);
    } else if (plot->yAxis->range().upper > (2 * max)) {
        plot->yAxis->setRange(0, max);
    }

    plot->xAxis->setLabel(tr("%1 Frames").arg(numFramesForBufferFullness));
    plot->yAxis->setLabel(tr("Buffer size (peak %1)").arg(max));

    plot->xAxis->setRange(1, numFramesForBufferFullness);

    plot->graph(0)->setData(xdata, ydata);
    plot->graph(0)->rescaleAxes();
    plot->replot(QCustomPlot::rpQueuedReplot);
    plot->update();
}

void DisplayChannel::renderStats(Stats *stats)
{
    if (bufferFullnessEnabled) {
        numFramesForBufferFullness = stats->framesForBufferFullness;
        if (numFramesForBufferFullness > 0) {
            for (uint i = 0; i < numFramesForBufferFullness; i++) {
                bufferFullnessList[i] = stats->bufferFullness[i];
            }
        }
    }
    //}
    videoItem->setStats(stats);
    if (!this->render) {
        videoItem->renderStats();
    }
}

#ifdef WITH_MOTION_RECTS
void DisplayChannel::updateMotion(void *ptr, Motion *motion)
{
    DisplayChannel *temp = (DisplayChannel*)ptr;
    temp->videoItem->setRectCount(motion->nr);
    float factor = (temp->renderSurface->rect().width() / temp->renderSurface->rect().height());
    factor = 1;
    for(int i=0;i<motion->nr;i++)
        temp->videoItem->setRect(i,motion->rectangles[i].xOff*16 * factor
                                 ,motion->rectangles[i].yOff*16 *factor
                                 ,motion->rectangles[i].width*16 * factor
                                 ,motion->rectangles[i].height*16 *factor);
}
#endif

void DisplayChannel::getChannelInfo()
{
    if (channel < 0) {
        return;
    }

    qDebug()<<"Get Channel info for"<<StringUtils::shared()->channelToString((video_channel_t)channel);
    apiManager->engineGetCameraInfo((video_channel_t)channel);
}

// public functions

void DisplayChannel::setDewarpMapEnable(bool val)
{
    videoItem->setdrawMapEnable(val);
}

void DisplayChannel::setMapPoints(QList<QPoint> points) {
    videoItem->setPoints(points);
}

void DisplayChannel::setToolbarVisible(bool visible, bool emitSignal) {
    if (!this->isVisible()) {
        return;
    }

    if (videoSettings != NULL) {
        if (emitSignal) {
            if (videoSettings->isVisible() != visible) {
                //emit signalOnVideoSettingsShown(channel,visible);
            }
        }
        videoSettings->setVisible(visible);
    }
}

void DisplayChannel::initMasks(int num) {
    this->videoItem->initMasks(num);
}

void DisplayChannel::drawMask(QRect rect, int index) {
    this->videoItem->drawMask(rect, index);
}

void DisplayChannel::removeMask(int index) {
    this->videoItem->removeMask(index);
}

// END public

// Slots

void DisplayChannel::onOverlayImageChanged(QString image, int imageWidth, int imageHeight, int xOff, int yOff, int alpha, QString alphafile) {
    bool imagePathChanged = false;
    bool imageOffsetChanged = false;
    OverlayImage* overlayImage = OverlayManager::shared()->getOverlayImage(channel);

    imagePathChanged = (overlayImage->image.compare(image) != 0);
    imageOffsetChanged = (overlayImage->xOffset != xOff) || (overlayImage->yOffset != yOff);
    overlayImage->image = image;
    overlayImage->width = imageWidth;
    overlayImage->height = imageHeight;
    overlayImage->xOffset = xOff;
    overlayImage->yOffset = yOff;
    overlayImage->alpha = alpha;
    overlayImage->alphaFile = alphafile;

    if (imagePathChanged && !imageOffsetChanged) {
        OverlayManager::shared()->calculateDefaultImageOffsets(channel,channelWidth,channelHeight);
        if (videoSettings) {
            videoSettings->updateOverlayImageConfig(channelWidth,channelHeight);
        }
    }

    task->resetOverlayImage(channel);
}

void DisplayChannel::onOverlayImageAlphaChanged(int index, int alpha) {
    //Write code to pass alpha for overlay object
    OverlayImage* overlayImage = OverlayManager::shared()->getOverlayImage(channel);
    overlayImage->alpha = alpha;
    task->setOverlayImageAlpha(channel, index, alpha);
}

void DisplayChannel::onOverlayImageToggled(bool enabled) {
    OverlayManager::shared()->setOverlayImageEnabled(channel, enabled);

    if (enabled) {
        task->addOverlayImage(channel);
    } else {
        task->removeOverlayImage(channel);
    }
}

void DisplayChannel::onAddOverlayTimeFailed(int) {
    AlertHandler::displayError(tr("Failed to add time"));
}

void DisplayChannel::onAddOverlayTextFailed(int, int index) {
    AlertHandler::displayError(tr("Failed to add text at index %1").arg(index));
}

void DisplayChannel::onAddOverlayImageFailed(int) {
    AlertHandler::displayError(tr("Failed to add logo.\nPlease verify the image and/or size."));
}

// Privacy mask slots from videosettings

void DisplayChannel::onPrivacyMaskChanged(int index) {
    task->resetPrivacyMask(channel, index);
}

void DisplayChannel::onOverlayTextChanged(int index,QString text, int xoff, int yoff) {
    OverlayText* overlay = OverlayManager::shared()->getOverlayText(channel, index);
    overlay->text = text;
    overlay->xOffset = xoff;
    overlay->yOffset = yoff;

    task->resetOverlayText(channel, index);
}

void DisplayChannel::onOverlayTextToggled(bool enabled, int index) {
    OverlayManager::shared()->setOverlayTextEnabled(channel, enabled, index);
    if (index == -1) {
        if (enabled) {
            task->setAllOverlayText(channel);
        } else {
            task->removeAllOverlayText(channel);
        }
    } else {
        if (enabled) {
            task->setOverlayText(channel, index);
        } else {
            task->removeOverlayText(channel, index);
        }
    }
}


void DisplayChannel::onOverlayTimeToggled(bool enabled) {
    OverlayManager::shared()->setOverlayTimeEnabled(channel, enabled);
    if (enabled) {
        task->showTimer(channel);
    } else {
        task->removeTimer(channel);
    }
}

void DisplayChannel::onOverlayFontChanged(QString fontPath, int fontSize) {
    OverlayFont *font = OverlayManager::shared()->getOverlayFont(channel);
    if (font->fontPath.compare(fontPath) != 0 || font->fontSize != fontSize) {
        font->fontPath = fontPath;
        font->fontSize = fontSize;
    }

    OverlayManager::shared()->calculateDefaultTextOffsets(channel, channelWidth, channelHeight);
    OverlayManager::shared()->calculateDefaultTimeOffsets(channel, channelWidth, channelHeight);
    videoSettings->updateOverlayTextConfig(getChannelWidth(), getChannelHeight());
    task->loadFont(channel);
}

void DisplayChannel::requestIFrame()
{
#ifdef WITH_ENGINE
    engine_setvideo_force_iframe(channel);
#endif
}

void DisplayChannel::highlightPanel(int panel) {
    onPanelChanged(panel);
}

void DisplayChannel::onPanelChanged(int panel) {
    currentPanel = panel;

    panel_mode_t mode;
    panel_params_t params;
    if (this->getCompositorParams(channel, panel, &mode, &params)) {
        currentPanelParams = params;
        currentPanelMode = mode;

        if (videoSettings != NULL) {
            videoSettings->setCurrentPanelMode(mode);
        }
    }
    highlightCurrentPanel(true);
}

void DisplayChannel::onPanelModeChanged(int panel, int mode) {
    currentPanel = panel;

    panel_params_t params;
    panel_mode_t tmode;
    if (this->getCompositorParams(channel, panel, &tmode, &params)) {
        if (this->setCompositorParams(channel, panel, (panel_mode_t)mode, params)) {
            currentPanelParams = params;
            currentPanelMode = (panel_mode_t)mode;

            if (videoSettings != NULL) {
                videoSettings->setCurrentPanelMode(mode);
            }
        }
    }

    highlightCurrentPanel(true);
}

void DisplayChannel::highlightCurrentPanel(bool highlight) {
    return;

    panel_params_t params = currentPanelParams;
    if (highlight) {
        if (currentPanelMode == PMODE_SELECT) {
            params.x = 0;
            params.y = 0;
            params.width = getChannelWidth();
            params.height = getChannelHeight();
        }
    } else {
        params.x = 0;
        params.y = 0;
        params.width = 0;
        params.height = 0;
    }

    qDebug() << "====== Set panel rect to focus ======";
    qDebug() << "x" << params.x;
    qDebug() << "y" << params.y;
    qDebug() << "w" << params.width;
    qDebug() << "h" << params.height;
    qDebug() << "=====================================";

    // TODO
    params.x = 0;
    params.y = 0;
    params.width = 0;
    params.height = 0;
    this->videoItem->setRectToFocus(QRect(params.x, params.y, params.width, params.height));
}

void DisplayChannel::setF1KeyPressed() {
    bStatsEnabled = !bStatsEnabled;
    this->setStatsEnabled(bStatsEnabled);
    if (popoutChannel != NULL) {
        popoutChannel->setStatsEnabled(bStatsEnabled);
    }
}

// END slots

// Engine APIs

bool DisplayChannel::getCompositorParams(int channel, int panel, panel_mode_t *mode, panel_params_t *params) {
#ifdef WITH_ENGINE
    int ret = engine_get_compositor_params((video_channel_t)channel, panel, mode, params);
    if (ret == 0) {
        qDebug() << "====== engine_get_compositor_params ======";
        qDebug() << "Channel:" << StringUtils::shared()->channelToString((video_channel_t)channel);
        qDebug() << "Panel:" << panel;
        qDebug() << "Mode:" << StringUtils::shared()->panelModeToString(*mode);
        qDebug() << "Params: x=" << (*params).x << ", y=" << (*params).y << ", w=" << (*params).width << ", h=" << (*params).height;
        qDebug() << "==========================================";
        return true;
    } else {
        qCritical() << "Error: engine_get_compositor_params failed";
        AlertHandler::displayError("Failed to get compositor parameters");
        return false;
    }
#endif
}

bool DisplayChannel::setCompositorParams(int channel, int panel, panel_mode_t mode, panel_params_t params) {
#ifdef WITH_ENGINE
    int ret = engine_set_compositor_params((video_channel_t)channel, panel, mode, &params);
    if (ret == 0) {
        qDebug() << "====== engine_set_compositor_params ======";
        qDebug() << "Channel: " << StringUtils::shared()->channelToString((video_channel_t)channel);
        qDebug() << "Panel: " << panel;
        qDebug() << "Mode: " << StringUtils::shared()->panelModeToString(mode);
        qDebug() << "Params: x=" << params.x << ", y=" << params.y << ", w=" << params.width << ", h=" << params.height;
        qDebug() << "==========================================";
        return true;
    } else {
        qCritical() << "Error: engine_set_compositor_params failed";
        AlertHandler::displayError("Failed to set compositor parameters");
        return false;
    }
#endif
}

// END Engine APIs

//--- Start Engine API Manager slots

void DisplayChannel::onEngineStartVideoSuccess(video_channel_t channel) {
    qDebug() << "Video start success for channel " << channel;
}
void DisplayChannel::onEngineStartVideoFailed(video_channel_t channel) {
    qDebug() << "Video start failed for channel " << channel;
}
void DisplayChannel::onEngineGetChannelInfoSuccess(video_channel_t channel, video_channel_info_t info) {
    qDebug() << "Got channel info for channel " << channel;

    /*if (info.width >= 1920) {
        optimize = true;
        ChannelState::shared()->setOptimizeEnabled(channel, true);
    }*/

    if (videoSettings == NULL) {
        videoSettings = new VideoSettings(this, channel);

#ifdef WITH_ENGINE
        // Read compositor params
        config_params_t *configParams = CameraConfig::shared()->getChannelConfigParams(channel);
        if (configParams->composite == 1) {
            qDebug() << "Num panels " << configParams->panels;

            // composited channel
            if (configParams->panels > 0) {
                panel_mode_t mode;
                panel_params_t params;

                for (int panel = 0; panel < configParams->panels; panel++) {
                    if (this->getCompositorParams(channel, panel, &mode, &params)) {
                        if (panel == 0) {
                            currentPanel = panel;
                            currentPanelMode = mode;
                            currentPanelParams = params;
                        }

                        if (mode == PMODE_SELECT) {
                            currentPanel = panel;
                            currentPanelMode = mode;
                            currentPanelParams = params;
                        }
                    }
                }

                videoSettings->setComposite(true, configParams->panels);
                videoSettings->setCurrentPanel(currentPanel);
                videoSettings->setCurrentPanelMode(currentPanelMode);

                connect(videoSettings,SIGNAL(signalOnPanelChanged(int)),this,SLOT(onPanelChanged(int)));
                connect(videoSettings,SIGNAL(signalOnPanelModeChanged(int,int)),this,SLOT(onPanelModeChanged(int,int)));
                connect(videoSettings,SIGNAL(signalHighlightCurrentPanel(bool)),this,SLOT(highlightCurrentPanel(bool)));
            }
        } else {
            videoSettings->setComposite(false);
            this->videoItem->setRectToFocus(QRect(0, 0, 0, 0));
            disconnect(videoSettings,SIGNAL(signalOnPanelChanged(int)),this,SLOT(onPanelChanged(int)));
            disconnect(videoSettings,SIGNAL(signalOnPanelModeChanged(int,int)),this,SLOT(onPanelModeChanged(int,int)));
            disconnect(videoSettings,SIGNAL(signalHighlightCurrentPanel(bool)),this,SLOT(highlightCurrentPanel(bool)));
        }
#endif

        videoSettings->setVideoEnabled(autoStartVideo);
        videoSettings->setRenderingEnabled(render);
        videoSettings->setBufferFullness(ChannelState::shared()->getBufferFullnessEnabled(channel), ChannelState::shared()->getBufferFullnessDuration(channel));
        connect(videoSettings,SIGNAL(signalChannelEnabled(bool)),this,SLOT(onVideoEnabled(bool)));
        connect(videoSettings,SIGNAL(signalRenderToggled(bool)),this,SLOT(onRenderToggled(bool)));
        connect(videoSettings,SIGNAL(signalBFToggled(bool)),this,SLOT(onBufferFullnessToggled(bool)));
        connect(videoSettings,SIGNAL(signalBufferDurationChanged(int)),this,SLOT(onBufferDurationChanged(int)));
        setToolbarVisible(false);

        videoSettings->setOverlayEnabled(this->overlayEnabled);
    }

    optimize = ChannelState::shared()->getOptimizeEnabled(channel);
    bufferFullnessEnabled = ChannelState::shared()->getBufferFullnessEnabled(channel);
    //engine_set_interval_buffer_fullness(channel, ChannelState::shared()->getBufferFullnessDuration(channel));

    disconnect(videoSettings,SIGNAL(bitRateSliderMoved(int)),0,0);
    disconnect(videoSettings,SIGNAL(frameRateSliderMoved(int)),this,SLOT(updateFrameRate(int)));
    disconnect(videoSettings,SIGNAL(gopSliderMoved(int)),this,SLOT(updateGop(int)));
    disconnect(videoSettings,SIGNAL(profileIndexChanged(int)),this,SLOT(updateProfile(int)));
    disconnect(videoSettings,SIGNAL(signalResolutionChanged(int,int)), this, SLOT(onResolutionChanged(int,int)));
    disconnect(videoSettings,SIGNAL(levelIndexChanged(int)),this,SLOT(updateLevel(int)));
    disconnect(videoSettings,SIGNAL(qualitySliderChanged(int)),this,SLOT(updateQuality(int)));
    disconnect(videoSettings,SIGNAL(iFrameRequested()),this,SLOT(requestIFrame()));
    disconnect(videoSettings,SIGNAL(signalFormatChanged(int)),this,SLOT(updateFormat(int)));

    disconnect(videoSettings, SIGNAL(signalOverlayImagePathChanged(QString,int,int,int,int, int, QString)), this, SLOT(onOverlayImageChanged(QString,int,int,int,int,int,QString)));
    disconnect(videoSettings, SIGNAL(signalOverlayImageToggled(bool)), this, SLOT(onOverlayImageToggled(bool)));
    disconnect(videoSettings, SIGNAL(signalOverlayTextChanged(int,QString,int,int)), this, SLOT(onOverlayTextChanged(int,QString,int,int)));
    disconnect(videoSettings, SIGNAL(signalOverlayTextToggled(bool,int)), this, SLOT(onOverlayTextToggled(bool,int)));
    disconnect(videoSettings, SIGNAL(signalOverlayTimeToggled(bool)), this, SLOT(onOverlayTimeToggled(bool)));
    disconnect(videoSettings, SIGNAL(signalOverlayFontChanged(QString,int)), this, SLOT(onOverlayFontChanged(QString,int)));
    disconnect(videoSettings, SIGNAL(signalOverlayImageAlphaChanged(int,int)), this, SLOT(onOverlayImageAlphaChanged(int,int)));

    disconnect(videoSettings, SIGNAL(signalPrivacyMaskChanged(int)), this, SLOT(onPrivacyMaskChanged(int)));

#ifdef WITH_ENGINE

    this->setChannelWidth(info.width);
    this->setChannelHeight(info.height);
    videoSettings->setResolution(getChannelWidth(),getChannelHeight());
    videoItem->setResolution(QString(QString::number(getChannelWidth())+"x"+QString::number(getChannelHeight())));

    videoFormat = info.format;

    if (overlayEnabled) {
        // setup overlay settings
        if (OverlayManager::shared()->getNumOverlayText() > 0 && OverlayManager::shared()->getOverlayText(channel, 0)->xOffset == -1) {
            OverlayManager::shared()->calculateDefaultTextOffsets(channel, channelWidth, channelHeight);
        }
        if (OverlayManager::shared()->getOverlayImage(channel)->xOffset == -1) {
            OverlayManager::shared()->calculateDefaultImageOffsets(channel, channelWidth, channelHeight);
        }
        if (OverlayManager::shared()->getOverlayTime(channel)->xOffset == -1) {
            OverlayManager::shared()->calculateDefaultTimeOffsets(channel, channelWidth, channelHeight);
        }

        videoSettings->updateOverlayTextConfig(getChannelWidth(), getChannelHeight());
        videoSettings->updateOverlayImageConfig(getChannelWidth(), getChannelHeight());
        videoSettings->updatePrivacyMaskConfig(getChannelWidth(), getChannelHeight());

        videoSettings->setOverlayTextEnabled(OverlayManager::shared()->isOverlayTextEnabled(channel));
        videoSettings->setOverlayImageEnabled(OverlayManager::shared()->isOverlayImageEnabled(channel));
        videoSettings->setOverlayTimeEnabled(OverlayManager::shared()->isOverlayTimeEnabled(channel));

        connect(videoSettings, SIGNAL(signalOverlayImagePathChanged(QString,int,int,int,int,int,QString)), this, SLOT(onOverlayImageChanged(QString,int,int,int,int,int,QString)));
        connect(videoSettings, SIGNAL(signalOverlayImageToggled(bool)), this, SLOT(onOverlayImageToggled(bool)));
        connect(videoSettings, SIGNAL(signalOverlayTextChanged(int,QString,int,int)), this, SLOT(onOverlayTextChanged(int,QString,int,int)));
        connect(videoSettings, SIGNAL(signalOverlayTextToggled(bool,int)), this, SLOT(onOverlayTextToggled(bool,int)));
        connect(videoSettings, SIGNAL(signalOverlayTimeToggled(bool)), this, SLOT(onOverlayTimeToggled(bool)));
        connect(videoSettings, SIGNAL(signalOverlayFontChanged(QString,int)), this, SLOT(onOverlayFontChanged(QString,int)));
        connect(videoSettings, SIGNAL(signalOverlayImageAlphaChanged(int,int)), this, SLOT(onOverlayImageAlphaChanged(int,int)));

        connect(videoSettings, SIGNAL(signalPrivacyMaskChanged(int)), this, SLOT(onPrivacyMaskChanged(int)));
    }

    videoSettings->setVideoFormat((video_format_t)videoFormat);

    if(videoFormat == VID_FORMAT_MJPEG_RAW)
    {
        videoItem->setJpegChannel(true);
        videoSettings->setQuality(info.compression_quality);
        videoItem->setQuality(info.compression_quality);
    }
    else if(videoFormat == VID_FORMAT_GREY_RAW || videoFormat == VID_FORMAT_NV12_RAW || videoFormat == VID_FORMAT_YUY2_RAW )
    {
        //Set tab for RAW
        videoItem->setRawChannel(true);
    }
    else
    {
        videoSettings->setGopSilder(info.goplen);
        videoItem->setGop(info.goplen);

        videoSettings->setProfile(info.profile);
        videoItem->setProfile(StringUtils::shared()->videoProfileToString(info.profile));

        videoSettings->setBitRateSilder(info.bitrate);
    }

    videoSettings->setFrameRateSilder(info.framerate);
    //videoItem->setFramerate(info.framerate);
    videoItem->setVideoFormat(videoFormat);

#endif
    //Connecting the signals after updating the values in the UI so that it does not happen recursively
    connect(videoSettings,SIGNAL(bitRateSliderMoved(int)),this,SLOT(updateBitRate(int)));
    connect(videoSettings,SIGNAL(frameRateSliderMoved(int)),this,SLOT(updateFrameRate(int)));
    connect(videoSettings,SIGNAL(gopSliderMoved(int)),this,SLOT(updateGop(int)));
    connect(videoSettings,SIGNAL(profileIndexChanged(int)),this,SLOT(updateProfile(int)));
    connect(videoSettings,SIGNAL(signalResolutionChanged(int,int)), this, SLOT(onResolutionChanged(int,int)));
    connect(videoSettings,SIGNAL(levelIndexChanged(int)),this,SLOT(updateLevel(int)));
    connect(videoSettings,SIGNAL(qualitySliderChanged(int)),this,SLOT(updateQuality(int)));
    connect(videoSettings,SIGNAL(iFrameRequested()),this,SLOT(requestIFrame()));
    connect(videoSettings,SIGNAL(signalFormatChanged(int)),this,SLOT(updateFormat(int)));


    if (scrollableParentWindow) {
        this->resizeScrollableParentWindow();
    }

    if (bStartVideoAfterGettingChannelInfo) {
        bStartVideoAfterGettingChannelInfo = false;
        startVideo();
    }

    if (OverlayManager::shared()->isOverlayTextSetupDone(channel) == false && this->overlayEnabled && OverlayManager::shared()->isOverlayTextEnabled(channel)) {
        //task->setAllOverlayText(channel);
    }
    OverlayManager::shared()->setOverlayTextSetupDone(channel, true);
    if (this->overlayEnabled && OverlayManager::shared()->isOverlayImageEnabled(channel)) {
        //task->resetOverlayImage(channel);
    }

    if (videoSettingsPositioned) {
        videoSettingsPositioned = false;
        positionToolbar();
    }
}
void DisplayChannel::onEngineGetChannelInfoFailed(video_channel_t channel, int) {
    qCritical()<<"Failed to get channel info for channel"<<StringUtils::shared()->channelToString(channel);
    AlertHandler::displayError("Failed to get channel info for channel"+StringUtils::shared()->channelToString(channel));
}
void DisplayChannel::onEngineSetResolutionSuccess(int width, int height) {
    this->setChannelWidth(width);
    this->setChannelHeight(height);

    videoItem->setResolution(QString(QString::number(width)+"x"+QString::number(height)));
    if (overlayEnabled) {
        OverlayManager::shared()->calculateDefaultImageOffsets(channel, getChannelWidth(), getChannelHeight());
        OverlayManager::shared()->calculateDefaultTextOffsets(channel, getChannelWidth(), getChannelHeight());
        OverlayManager::shared()->calculateDefaultTimeOffsets(channel, getChannelWidth(), getChannelHeight());

        if (videoSettings != NULL) {
            videoSettings->updateOverlayImageConfig(getChannelWidth(), getChannelHeight());
            videoSettings->updateOverlayTextConfig(getChannelWidth(), getChannelHeight());
        }

        if (OverlayManager::shared()->isOverlayImageEnabled(channel)) {
            task->resetOverlayImage(channel);
        }
        if (OverlayManager::shared()->isOverlayTextEnabled(channel)) {
            task->resetAllOverlayText(channel);
        }
    }

    if (scrollableParentWindow != NULL) {
        this->resizeScrollableParentWindow();
    }

    if (videoEnabled) {
        startVideo();
    }
}
void DisplayChannel::onEngineSetResolutionFailed() {
    qWarning()<<"Failed to update resolution for channel"<<StringUtils::shared()->channelToString((video_channel_t)channel);
    AlertHandler::displayError("Failed to update resolution");
    videoSettings->setResolution(getChannelWidth(), getChannelHeight());
    if (videoEnabled) {
        startVideo();
    }
}
void DisplayChannel::onEngineResolutionNotSupported() {
    qWarning()<<"Resolution not supported for channel"<<StringUtils::shared()->channelToString((video_channel_t)channel);
    AlertHandler::displayError("Resolution not supported");
    videoSettings->setResolution(getChannelWidth(), getChannelHeight());
    apiManager->engineSetResolution((video_channel_t)channel, getChannelWidth(), getChannelHeight());
}
void DisplayChannel::onEngineSetVideoFormatSuccess() {
    bStartVideoAfterGettingChannelInfo = videoEnabled;
    getChannelInfo();
}
void DisplayChannel::onEngineSetVideoFormatFailed() {
    AlertHandler::displayError("Failed to set format");
    if (videoEnabled) {
        startVideo();
    }
}
//--- END Engine API Manager slots
