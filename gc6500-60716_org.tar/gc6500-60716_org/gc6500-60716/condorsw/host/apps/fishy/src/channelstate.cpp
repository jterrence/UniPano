/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include "channelstate.h"
#include <log.h>

ChannelState* ChannelState::instance = 0;

void ChannelState::init(int channel) {
    ChannelState *state = new ChannelState();
    state->videoEnabled = false;
    state->renderEnabled = false;
#ifdef FFMPEG
    // If FFMPEG UI decoding is enabled, set the channel state as optimized (since the engine is freed up from decoding overhead)
    state->optimizeEnabled = true;
#else
    state->optimizeEnabled = false;
#endif
    state->bufferFullnessEnabled = false;
    state->bufferFullnessDuration = 4; // 2 sec
    mapChannelStates[channel] = state;
}

void ChannelState::deinit() {
    QMap<int, ChannelState*>::iterator itState;
    for (itState = mapChannelStates.begin(); itState != mapChannelStates.end(); itState++) {
        delete (ChannelState*) itState.value();
    }
}

void ChannelState::setVideoEnabled(int channel, bool enabled) {
    ChannelState *state = mapChannelStates[channel];
    if (state == NULL) {
        qWarning()<<"Init not called";
        return;
    }
    state->videoEnabled = enabled;
}

bool ChannelState::getVideoEnabled(int channel) {
    if (!mapChannelStates.contains(channel)) {
        return false;
    }

    return mapChannelStates[channel]->videoEnabled;
}

void ChannelState::setRenderEnabled(int channel, bool enabled) {
    ChannelState *state = mapChannelStates[channel];
    if (state == NULL) {
        qWarning()<<"Init not called";
        return;
    }
    state->renderEnabled = enabled;
}

bool ChannelState::getRenderEnabled(int channel) {
    if (!mapChannelStates.contains(channel)) {
        return false;
    }

    return mapChannelStates[channel]->renderEnabled;
}

void ChannelState::setOptimizeEnabled(int channel, bool enabled) {
    ChannelState *state = mapChannelStates[channel];
    if (state == NULL) {
        qWarning()<<"Init not called";
        return;
    }
    state->optimizeEnabled = enabled;
}

bool ChannelState::getOptimizeEnabled(int channel) {
    if (!mapChannelStates.contains(channel)) {
        return false;
    }

    return mapChannelStates[channel]->optimizeEnabled;
}

void ChannelState::setBufferFullnessEnabled(int channel, bool enabled) {
    ChannelState *state = mapChannelStates[channel];
    if (state == NULL) {
        qWarning()<<"Init not called";
        return;
    }
    state->bufferFullnessEnabled = enabled;
}

bool ChannelState::getBufferFullnessEnabled(int channel) {
    if (!mapChannelStates.contains(channel)) {
        return false;
    }
    return mapChannelStates[channel]->bufferFullnessEnabled;
}

void ChannelState::setBufferFullnessDuration(int channel, int duration) {
    ChannelState *state = mapChannelStates[channel];
    if (state == NULL) {
        qWarning()<<"Init not called";
        return;
    }
    state->bufferFullnessDuration = duration;
}

int ChannelState::getBufferFullnessDuration(int channel) {
    if (!mapChannelStates.contains(channel)) {
        return false;
    }
    return mapChannelStates[channel]->bufferFullnessDuration;
}
