/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include <QDebug>
#include <QPainter>
#include "toastmessage.h"
#include "ui_toastmessage.h"

ToastMessage::ToastMessage(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ToastMessage)
{
    ui->setupUi(this);
    connect(&timer, SIGNAL(timeout()),this, SLOT(handleTimeout()));
    this->setAttribute(Qt::WA_TranslucentBackground);
}

ToastMessage::~ToastMessage()
{
    delete ui;
}

void ToastMessage::paintEvent(QPaintEvent*)
{
  QColor backgroundColor = palette().light().color();
  backgroundColor.setAlpha(200);
  QPainter customPainter(this);
  customPainter.fillRect(rect(),backgroundColor);
}

void ToastMessage::setText(QString text)
{
    ui->label->setText(text);
    timer.start(3000);
    timer.setSingleShot(true);
}

void ToastMessage::handleTimeout()
{
    this->hide();
    this->close();
}

void ToastMessage::getSize(int *w, int*h)
{
    *w = ui->label->fontMetrics().width(ui->label->text()) + 4;//4 is added to accomodate layout spacing
    *h = ui->label->fontMetrics().height()+ 4;//4 is added to accomodate layout spacing
}
