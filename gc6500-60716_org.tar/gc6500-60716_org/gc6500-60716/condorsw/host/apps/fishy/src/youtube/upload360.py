#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================

from oauth2client.tools import argparser
from apiclient.errors import HttpError
import uploadVideo
import add360MetaInfo as AddMeta
import os

argparser.add_argument("--infile", required=True, help="Video mp4 file without 360meta informatio")
argparser.add_argument("--outfile", required=True, help="Video file to upload with 360meta information")

#argparser.add_argument("--file", required=True, help="Video file to upload")
argparser.add_argument("--title", help="Video title", default="Geo 360 View")
argparser.add_argument("--description", help="Video description",
  default="Geo 360 View")
argparser.add_argument("--category", default="22",
  help="Numeric video category. " +
    "See https://developers.google.com/youtube/v3/docs/videoCategories/list")
argparser.add_argument("--keywords", help="Video keywords, comma separated",
  default="")
argparser.add_argument("--privacyStatus", choices=uploadVideo.VALID_PRIVACY_STATUSES,
  default=uploadVideo.VALID_PRIVACY_STATUSES[0], help="Video privacy status.")
args = argparser.parse_args()

if not os.path.exists(args.infile):
  print "Please specify a valid input file using the --infile= parameter."
  exit("Please specify a valid input file using the --infile= parameter.")

if not os.path.exists(uploadVideo.CLIENT_SECRETS_FILE):
	exit ("Error: auth file missing")

#if not os.path.exists(args.outfile):
#  exit("Please specify a valid output file using the --outfile= parameter.")

AddMeta.InjectMetadata(args.infile, args.outfile)

youtube = uploadVideo.get_authenticated_service(args)
try:
	uploadVideo.initialize_upload(youtube, args)
	# Delete outfile
	os.remove(args.outfile)	
except HttpError, e:
    print "An HTTP error %d occurred:\n%s" % (e.resp.status, e.content)

