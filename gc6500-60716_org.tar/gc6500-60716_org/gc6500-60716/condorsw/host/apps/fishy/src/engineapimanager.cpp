/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include "engineapimanager.h"
#include "cameraconfig.h"
#include "stringutils.h"
#include "log.h"

EngineApiManager::EngineApiManager(QObject *parent, QString name) :
    QObject(parent)
{
    thread = new QThread();
    //this->moveToThread(thread);

    qRegisterMetaType<video_channel_t>("video_channel_t");
    qRegisterMetaType<video_channel_info_t>("video_channel_info_t");
    qRegisterMetaType<dewarp_mode_t>("dewarp_mode_t");
    qRegisterMetaType<dewarp_params_t>("dewarp_params_t");
    qRegisterMetaType<video_format_t>("video_format_t");
    qRegisterMetaType<int*>("int*");

    thread->setObjectName(name);
    //thread->start();
    //qDebug() << "Thread id in constructor of EngineApiManager: " << QThread::currentThreadId();

    connect(this, SIGNAL(signalCleanup()), this, SLOT(deleteLater()));
    connect(this, SIGNAL(signalCleanup()), thread, SLOT(deleteLater()));
}

EngineApiManager::~EngineApiManager() {
    qDebug()<<"Destructor called";
}

void EngineApiManager::startThread() {

    this->moveToThread(thread);

    disconnect(this, SIGNAL(signalCleanup()), this, SLOT(deleteLater()));
    disconnect(this, SIGNAL(signalCleanup()), thread, SLOT(deleteLater()));

    connect(this, SIGNAL(signalCleanup()), thread, SLOT(quit()));
    connect(thread, SIGNAL(finished()), this, SLOT(deleteLater()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));

    thread->start();
}

void EngineApiManager::cleanup() {
    QMetaObject::invokeMethod(this, "queueCleanup", Qt::QueuedConnection);
}

void EngineApiManager::engineInit(int deviceID) {
    QMetaObject::invokeMethod(this, "queueEngineInit", Qt::QueuedConnection, Q_ARG(int, deviceID));
}

void EngineApiManager::engineGetCameraConfig() {
    QMetaObject::invokeMethod(this, "queueEngineGetCameraConfig", Qt::QueuedConnection);
}

void EngineApiManager::engineGetCameraInfo(video_channel_t channel) {
    this->channel = channel;
    QMetaObject::invokeMethod(this, "queueEngineGetCameraInfo", Qt::QueuedConnection);
}

void EngineApiManager::engineGetDewarpParams(video_channel_t channel, int panel) {
    this->channel = channel;
    this->panel = panel;

    QMetaObject::invokeMethod(this, "queueEngineGetDewarpParams", Qt::QueuedConnection);
}

void EngineApiManager::engineSetDewarpParams(video_channel_t channel, int panel, dewarp_mode_t mode, dewarp_params_t *params) {
    this->channel = channel;
    this->panel = panel;
    this->dmode = mode;
    this->dparams = *params;
    QMetaObject::invokeMethod(this, "queueEngineSetDewarpParams", Qt::QueuedConnection);
}

void EngineApiManager::engineStartVideo(video_channel_t channel) {
    this->channel = channel;
    QMetaObject::invokeMethod(this, "queueEngineStartVideo", Qt::QueuedConnection);
}

void EngineApiManager::engineSetResolution(video_channel_t channel, int width, int height) {
    QMetaObject::invokeMethod(this, "queueEngineSetResolution", Qt::QueuedConnection, Q_ARG(video_channel_t, channel), Q_ARG(int, width), Q_ARG(int, height));
}

void EngineApiManager::engineSetVideoFormat(video_channel_t channel, video_format_t format) {
    QMetaObject::invokeMethod(this, "queueEngineSetVideoFormat", Qt::QueuedConnection, Q_ARG(video_channel_t, channel), Q_ARG(video_format_t, format));
}

void EngineApiManager::engineGetMicVolume() {
    QMetaObject::invokeMethod(this, "queueEngineGetMicVolume", Qt::QueuedConnection);
}

void EngineApiManager::engineSetMicVolume(int volume) {
    QMetaObject::invokeMethod(this, "queueEngineSetMicVolume", Qt::QueuedConnection, Q_ARG(int, volume));
}

void EngineApiManager::engineMuteMic(int mute) {
    QMetaObject::invokeMethod(this, "queueEngineMuteMic", Qt::QueuedConnection, Q_ARG(int, mute));
}

void EngineApiManager::engineMuteMicLeft(int mute) {
    QMetaObject::invokeMethod(this, "queueEngineMuteMicLeft", Qt::QueuedConnection, Q_ARG(int, mute));
}

void EngineApiManager::engineMuteMicRight(int mute) {
    QMetaObject::invokeMethod(this, "queueEngineMuteMicRight", Qt::QueuedConnection, Q_ARG(int, mute));
}

void EngineApiManager::engineAGC(int enable) {
    QMetaObject::invokeMethod(this, "queueEngineAGC", Qt::QueuedConnection, Q_ARG(int, enable));
}

void EngineApiManager::engineAddOverlayText(int channel, QString text, int xoff, int yoff, int *id) {
    QMetaObject::invokeMethod(this, "queueEngineAddOverlayText", Qt::QueuedConnection, Q_ARG(int, channel), Q_ARG(QString, text), Q_ARG(int, xoff), Q_ARG(int, yoff), Q_ARG(int*, id));
}

// Functions run on the thread
void EngineApiManager::queueCleanup() {
    emit(signalCleanup());
}

void EngineApiManager::queueEngineInit(int deviceID) {
    int camType = engine_init(deviceID);
    if (camType < 0) {
        emit(signalEngineInitFailed());
        return;
    }
    if (engine_overlay_init() < 0) {
        emit(signalEngineInitFailed());
        return;
    }
    emit(signalEngineInitSuccess(camType));
}

void EngineApiManager::queueEngineGetCameraConfig() {
    CameraConfig *channelConfig = CameraConfig::shared();

    camer_mode_t mode = IPCAM;
    int error = engine_get_camera_mode(&mode);
    if (error != 0) {
        qWarning() << "Failed to get camera mode";
    }
    qDebug() << "Camera mode: " << mode;
    channelConfig->setCameraMode(mode);

    int numChannels = engine_getvideo_channel_count();
    int rawChannelID = -1;

    qDebug()<<"Total number of channels: "<<numChannels;
    config_params_t **configParams = (config_params_t**) malloc(numChannels * sizeof(config_params_t*));

    for(int i=0, format = 0;i< numChannels; i++) {
        engine_getvideo_format(i,&format);
        qDebug() << "Video Format for channel"<<StringUtils::shared()->channelToString((video_channel_t)i)<<"is"<<StringUtils::shared()->videoFormatToString((video_format_t)format);
        if (mode == IPCAM) {
            // treat raw separately only for IPCAM
            if(format == VID_FORMAT_GREY_RAW || format == VID_FORMAT_NV12_RAW || format == VID_FORMAT_YUY2_RAW ) {
                numChannels -= 1;
                rawChannelID = i;
            }
            if(format == VID_FORMAT_H264_RAW) {
                channelConfig->setNumH264Channels();
            }
        }

        configParams[i] = (config_params_t*)malloc(sizeof(config_params_t));
        int ret = engine_get_config_params((video_channel_t)i, configParams[i]);
        if(ret) {
            qCritical() << "Get Config Params failed for channel"<<StringUtils::shared()->channelToString((video_channel_t)i);
            configParams[i]->composite = 0;
            configParams[i]->dewarp = 0;
            configParams[i]->panels = 0;
        } else {
            qDebug()<<"========Channel Config========";
            qDebug()<<"Channel"<<StringUtils::shared()->channelToString((video_channel_t)i);
            qDebug()<<"Composite"<<configParams[i]->composite;
            qDebug()<<"Dewarp"<<configParams[i]->dewarp;
            qDebug()<<"Panels"<<configParams[i]->panels;
            qDebug()<<"==============================";
        }
    }

    channelConfig->setNumChannels(numChannels);
    channelConfig->setRawChannelID(rawChannelID);
    channelConfig->setChannelConfigParams(configParams);
    emit(signalEngineGetCameraConfigSuccess());
}

void EngineApiManager::queueEngineGetDewarpParams() {
    int ret = engine_get_dewarp_params(channel, panel, &dmode, &dparams);
    if (ret == 0) {
        // success
        emit(signalEngineGetDewarpParamsSuccess(channel, panel, dmode, dparams));
    } else {
        // failed
        emit(signalEngineGetDewarpParamsFailed(channel, panel));
    }
}

void EngineApiManager::queueEngineSetDewarpParams() {
    int ret = engine_set_dewarp_params(channel, panel, dmode, &dparams);
    if (ret == 0) {
        // success
        emit(signalEngineSetDewarpParamsSuccess(channel, panel, dmode, dparams));
    } else {
        // failed
        emit(signalEngineSetDewarpParamsFailed(channel, panel));
    }
}

void EngineApiManager::queueEngineStartVideo() {
    int ret = engine_startvideo((int)this->channel);
    if (ret == 0) {
        // success
        emit(signalEngineStartVideoSuccess(this->channel));
    } else {
        // failure
        emit(signalEngineStartVideoFailed(this->channel));
    }
}

void EngineApiManager::queueEngineGetCameraInfo() {
    video_channel_info_t info;
    int error = engine_getvideo_channel_info(this->channel, &info);
    if (error == 0) {
        qDebug()<<"========Channel Info========";
        qDebug()<<"Channel"<<StringUtils::shared()->channelToString(channel);
        qDebug()<<"Format"<<StringUtils::shared()->videoFormatToString(info.format);
        qDebug()<<"Resolution"<<info.width<<"x"<<info.height;
        qDebug()<<"Framerate"<<info.framerate;
        qDebug()<<"Goplen"<<info.goplen;
        qDebug()<<"Profile"<<StringUtils::shared()->videoProfileToString(info.profile);
        qDebug()<<"Bitrate"<<info.bitrate;
        qDebug()<<"Compression Quality"<<info.compression_quality;
        qDebug()<<"============================";
        emit(signalEngineGetChannelInfoSuccess(this->channel, info));
    } else {
        emit(signalEngineGetChannelInfoFailed(this->channel, error));
    }
}

void EngineApiManager::queueEngineSetResolution(video_channel_t channel, int width, int height) {
    int error = engine_setvideo_resolution((int)channel, width, height);
    if (error == 0) {
        int w = 0, h = 0;
        error = engine_getvideo_resolution((int)channel, &w, &h);
        if (error == 0) {
            if (w == width && h == height) {
                emit(signalEngineSetResolutionSuccess(width, height));
                return;
            } else {
                emit(signalEngineResolutionNotSupported());
                return;
            }
        }
    }

    emit(signalEngineSetResolutionFailed());
}

void EngineApiManager::queueEngineSetVideoFormat(video_channel_t channel, video_format_t format) {
    int error = engine_setvideo_format((int)channel, (int)format);
    if (error == 0) {
        emit(signalEngineSetVideoFormatSuccess());
    } else {
        emit(signalEngineSetVideoFormatFailed());
    }
}

void EngineApiManager::queueEngineGetMicVolume() {
    int volume = engine_audio_get_volume();
    if (volume >= 0) {
        emit(signalEngineGetMicVolumeSuccess(volume));
    } else {
        emit(signalEngineGetMicVolumeFailed());
    }
}

void EngineApiManager::queueEngineSetMicVolume(int volume) {
    int error = engine_audio_set_volume(volume);
    if (error == 0) {
        emit(signalEngineSetMicVolumeSuccess());
    } else {
        emit(signalEngineSetMicVolumeFailed());
    }
}

void EngineApiManager::queueEngineMuteMic(int mute) {
    int error = engine_audio_mute(mute);
    if (error == 0) {
        emit(signalEngineMuteMicSuccess());
    } else {
        emit(signalEngineMuteMicFailed());
    }
}

void EngineApiManager::queueEngineMuteMicLeft(int mute) {
    int error = engine_audio_mute_left(mute);
    if (error == 0) {
        emit(signalEngineMuteMicSuccess());
    } else {
        emit(signalEngineMuteMicFailed());
    }
}

void EngineApiManager::queueEngineMuteMicRight(int mute) {
    int error = engine_audio_mute_right(mute);
    if (error == 0) {
        emit(signalEngineMuteMicSuccess());
    } else {
        emit(signalEngineMuteMicFailed());
    }
}

void EngineApiManager::queueEngineAGC(int enable) {
    int error = engine_agc(enable);
    if (error == 0) {
        emit(signalEngineAGCSuccess());
    } else {
        emit(signalEngineAGCFailed());
    }
}

void EngineApiManager::queueEngineAddOverlayText(int channel, QString text, int xoff, int yoff, int *id) {
    int error = engine_overlay_add_text(channel, (char*)text.toStdString().c_str(), xoff, yoff, id);
    if (error == 0) {
        emit(signalEngineAddOverlayTextSuccess());
    } else {
        emit(signalEngineAddOverlayTextFailed());
    }
}
