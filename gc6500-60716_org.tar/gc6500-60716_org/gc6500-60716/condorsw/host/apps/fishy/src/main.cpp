/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include <getopt.h>
#ifdef QT5
#include <QtWidgets>
#include <QStyle>
#else
#include <QtGui/QApplication>
#endif
#include <QtOpenGL>
#include "mainwindow.h"
#ifdef WITH_ENGINE
#include "interface.h"
#endif
#include <QFile>
#include <QTextStream>
#include "overlaymanager.h"

#define MAX_CHANNELS_PER_PAGE  6
#define NUM_CHANNELS_PER_PAGE 4

extern "C" {
extern	const char*	rcs_ident_version_c_func() ;
}

void displayHelp()
{
    printf("Usage: [options]\n\n"
           "Options:\n"
           "-d, --device (mandatory)    Video device name\n"
           "-a, --audio                 if 1, auto start recording audio. If 0, do not start recording. Default is 1\n"
           "                            If mode=ipcam, AAC recording is started. (Data is dumped to /dev/null)\n"
           "                            If mode=skype, PCM recording is started. (Data is dumped to /dev/null)\n"
           "-v, --video                 if 1, auto start video for visible channels. If 0, no video channels are started. Default is 1\n"
           "-s, --startall-video        Start all available video channels.\n"
           "                            Switching pages will not stop the videos on the previous page (unless user stops the video manually)\n"
           "-n, --num                   Number of channels per page. Set to 'all' to display all available channels (except raw). Default is 4\n"
           "-b, --time-bps              Time interval in seconds to calculate bitrate. Default is 1 second\n"
           "-m, --time-bps-min-max      Time interval in seconds to calculate min/max bitrate. Default is 10 seconds\n"
           "--disable-render            Disable video render. Default is enabled\n"
           "--disable-overlay           Disable overlay feature. Default is enabled\n"
           "--disable-eptz              Disable ePTZ feature. Default is enabled\n"
           "--disable-upload360         Disable YouTube upload feature. Default is enabled\n"
           "--start-overlay-image       Whether to start all channels with/without overlay image. Default is disabled\n"
           "--start-overlay-text        Whether to start all channels with/without overlay text. Default is disabled\n"
           "-t, --num-overlay-text      Number of overlay text to start (if --start-overlay-text is set to 1). Default and max is 4\n"
           "-h, --help                  Print this message and exit\n"
           "\n");
    //"-q, --querydump             File to redirect querydump output. Default is ./query.dump\n"

    qDebug()<<"Application can be launched as:";
    qDebug()<<"/path/to/fishy -d/--device <device id/number>";
    qDebug()<< "\n" << rcs_ident_version_c_func() ;
    exit(2);
}

static const char short_options[] = "d:a:v:n:m:b:t:sh?";
static const struct option  \
long_options[] = {
    { "device", required_argument, NULL, 'd' },
    { "audio", required_argument, NULL, 'a' },
    { "video", required_argument, NULL, 'v' },
    { "startall-video", no_argument, NULL, 's' },
    { "num", required_argument, NULL, 'n' },
    { "time-bps", required_argument, NULL, 'b' },
    { "time-bps-min-max", required_argument, NULL, 'm' },
    { "disable-overlay", no_argument, NULL, 0 },
    { "disable-render", no_argument, NULL, 0 },
    { "num-overlay-text", required_argument, NULL, 't' },
    { "start-overlay-image", no_argument, NULL, 0 },
    { "start-overlay-text", no_argument, NULL, 0 },
    { "disable-eptz", no_argument, NULL, 0 },
    { "disable-upload360", no_argument, NULL, 0 },
    { "help", no_argument, NULL, 'h' },
    { NULL, no_argument, NULL, 0 }
};

#ifdef QT5
void fishyMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg) {
    QByteArray localMsg = msg.toLocal8Bit();
    switch (type) {
    case QtDebugMsg:
        if (context.file == NULL) {
            fprintf(stderr, "Debug: %s\n", localMsg.constData());
        } else {
            fprintf(stderr, "Debug: [%s->%s():%u] %s\n", context.file, context.function, context.line, localMsg.constData());
        }
        break;
    case QtWarningMsg:
        if (context.file == NULL) {
            fprintf(stderr, "Warning: %s\n", localMsg.constData());
        } else {
            fprintf(stderr, "Warning: [%s->%s():%u] %s\n", context.file, context.function, context.line, localMsg.constData());
        }
        break;
    case QtCriticalMsg:
        if (context.file == NULL) {
            fprintf(stderr, "Critical: %s\n", localMsg.constData());
        } else {
            fprintf(stderr, "Critical: [%s->%s():%u] %s\n", context.file, context.function, context.line, localMsg.constData());
        }
        break;
    case QtFatalMsg:
        if (context.file == NULL) {
            fprintf(stderr, "Fatal: %s\n", localMsg.constData());
        } else {
            fprintf(stderr, "Fatal: [%s->%s():%u] %s\n", context.file, context.function, context.line, localMsg.constData());
        }
        abort();
    }
}
#else
void fishyMessageOutput(QtMsgType type, const char *msg) {
    /*if (outputFile != NULL) {
        //QString dt = QDateTime::currentDateTime().toString("dd/MM/yyyy hh:mm:ss");
        QString txt = "";// QString("[%1] ").arg(dt);

        switch (type)
        {
            case QtDebugMsg:
                txt += QString("Debug: %1\n").arg(msg);
                break;
            case QtWarningMsg:
                txt += QString("Warning: %1\n").arg(msg);
                break;
            case QtCriticalMsg:
                txt += QString("Critical: %1\n").arg(msg);
                break;
            case QtFatalMsg:
                txt += QString("Fatal: %1\n").arg(msg);
                abort();
                break;
        }

        QFile outFile(outputFile);
        outFile.open(QIODevice::WriteOnly | QIODevice::Append);

        QTextStream textStream(&outFile);
        textStream << txt << endl;
    }*/

    switch (type) {
        case QtDebugMsg:
            fprintf(stderr, "Debug: %s\n", msg);
            break;
        case QtWarningMsg:
            fprintf(stderr, "Warning: %s\n", msg);
            break;
        case QtCriticalMsg:
            fprintf(stderr, "Critical: %s\n", msg);
            break;
        case QtFatalMsg:
            fprintf(stderr, "Fatal: %s\n", msg);
            abort();
    }
}
#endif

int main(int argc, char *argv[])
{
    bootProperties prop;
    memset(&prop, 0, sizeof(bootProperties));
    int id = -1;
    prop.prop.audio = 1;
    prop.prop.video = 1;
    prop.prop.startallvideo = 0;
    prop.prop.querydump = (char*)"query.dump";
    prop.prop.numChannelsPerPage = NUM_CHANNELS_PER_PAGE;
    prop.prop.upload360 = 1;
    prop.prop.overlay = 1;
    prop.prop.overlayImage = 0;
    prop.prop.overlayText = 0;
    prop.prop.eptz = 1;
    prop.prop.render = 1;
    prop.prop.numOverlayText = MAX_OVERLAY_TEXTS;
    prop.prop.timeBps = 1;
    prop.prop.timeBpsMinMax = 10;
    for (;;) {
        int idx = 0;
        int c;

        c = getopt_long(argc, argv,
                        short_options, long_options, &idx);

        if (-1 == c)
        {
            break;
        }
        switch (c) {
            case 0: /* getopt_long() flag */
                if (!strcmp(long_options[idx].name, "disable-overlay")) {
                    prop.prop.overlay = 0;
                }
                if (!strcmp(long_options[idx].name, "disable-eptz")) {
                    prop.prop.eptz = 0;
                }
                if (!strcmp(long_options[idx].name, "disable-upload360")) {
                    prop.prop.upload360 = 0;
                }
                if (!strcmp(long_options[idx].name, "start-overlay-image")) {
                    prop.prop.overlayImage = 1;
                }
                if (!strcmp(long_options[idx].name, "start-overlay-text")) {
                    prop.prop.overlayText = 1;
                }
                if (!strcmp(long_options[idx].name, "disable-render")) {
                    prop.prop.render = 0;
                }

                break;

            case 'd':
                id = atoi(optarg);
                break;
            case 'a':
                prop.prop.audio = atoi(optarg);
                break;
            case 'v':
                prop.prop.video = atoi(optarg);
                break;
            case 'n':
                if (strcmp(optarg, "all") == 0) {
                    prop.prop.numChannelsPerPage = 0;
                } else {
                    if (atoi(optarg) > 0 && atoi(optarg) <= MAX_CHANNELS_PER_PAGE) {
                        prop.prop.numChannelsPerPage = atoi(optarg);
                    }
                }
                break;
            case 's':
                prop.prop.startallvideo = 1;
                break;
            case 't':
                if (atoi(optarg) < MAX_OVERLAY_TEXTS) {
                    prop.prop.numOverlayText = atoi(optarg);
                }
                /*if (prop.prop.numOverlayText <= 0) {
                    prop.prop.numOverlayText = 1;
                }*/
                break;
            case 'q':
                prop.prop.querydump = optarg;
                qDebug()<<"querydump log file"<<prop.prop.querydump;
                break;
            case 'm':
                prop.prop.timeBpsMinMax = atoi(optarg);
                break;
            case 'b':
                prop.prop.timeBps = atoi(optarg);
                break;
            case '?':
            case 'h':
                displayHelp();
                exit(EXIT_SUCCESS);
            default:
                displayHelp();
                exit(EXIT_FAILURE);
        }
    }

    if (prop.prop.startallvideo == 1) {
        prop.prop.video = 1;
    }

    if (id == -1) {
        fprintf(stderr, "%s: missing option -d/--device\n", argv[0]);
        displayHelp();
        exit(EXIT_FAILURE);
    }

#ifdef QT5
    qInstallMessageHandler(fishyMessageOutput);
    QApplication::setStyle(QStyleFactory::create("windows"));
#else
    qInstallMsgHandler(fishyMessageOutput);
    QApplication::setStyle(new QWindowsStyle);
#endif

    QApplication::setGraphicsSystem("raster");
    QApplication a(argc, argv);
    QThread::currentThread()->setPriority(QThread::HighPriority);
    if (!((QGLFormat::openGLVersionFlags() & QGLFormat::OpenGL_Version_2_0) ||
          (QGLFormat::openGLVersionFlags() & QGLFormat::OpenGL_ES_Version_2_0))) {
        QMessageBox::information(0, "Mxview",
                                 "This system does not support OpenGL 2.0 or OpenGL ES 2.0, "
                                 "which is required for this application to work.");
        return 0;
    }

    a.setPalette(QColor(50,50,50));
    a.setStyleSheet("QLabel {color:white; font-family:Arial;}"
                    "QPushButton {color:#fff; font-family:Arial; font-size:13px;}"
                    "#toolbar {"
                    "background:rgb(50,50,50,230);"
                    "border-radius:4px;"
                    "border: 1px solid white;"
                    "padding:0px;}"
                    "#toolbar QPushButton {"
                    "color:#fff;}"
                    "#toolbar QPushButton:disabled {"
                    "color:#555;}"
                    "#toolbar QPushButton:checked {"
                    "border-radius:4px;"
                    "border: 1px solid;"
                    "background:rgb(205,205,205,225);}"
                    "#toolbar QPushButton:pressed {"
                    "border-radius:4px;"
                    "border: 1px solid;"
                    "background:rgb(205,205,205,225);}"
                    "QCheckBox {font-family: Arial;}");

    MainWindow w;

    w.init(id, prop);
    w.show();

    return a.exec();
}
