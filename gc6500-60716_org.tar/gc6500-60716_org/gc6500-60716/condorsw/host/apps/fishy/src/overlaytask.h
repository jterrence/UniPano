/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef OVERLAYTASK_H
#define OVERLAYTASK_H

#include <QObject>
#include <QThread>

class OverlayTask : public QObject
{
    Q_OBJECT
public:
    explicit OverlayTask(QObject *parent = 0);
    ~OverlayTask();

    void startThread(const QObject*);

    void resetAllOverlayText(int channel);
    void resetOverlayText(int channel, int index);

    void setOverlayText(int channel, int index);
    void removeOverlayText(int channel, int index);
    void setAllOverlayText(int channel);
    void removeAllOverlayText(int channel);
    void removeTimer(int channel);
    void showTimer(int channel);
    void addOverlayImage(int channel);
    void removeOverlayImage(int channel);
    void resetOverlayImage(int channel);
    void loadFont(int channel);
    void setOverlayImageAlpha(int channel,int index, int alpha);

    void setPrivacyMask(int channel, int index);
    void removePrivacyMask(int channel, int index);
    void resetPrivacyMask(int channel, int index);
    void updatePrivacyMaskColor(int channel, int index);

    void cleanup(int channel);

Q_SIGNALS:
    void signalTaskDestroyed();
    void signalCleanupDone();
    void signalAddMaskFailed(int ch, int index);
    void signalAddMaskSuccess(int ch, int index);
    void signalAddOverlayImageFailed(int ch);
    void signalAddOverlayTextFailed(int ch, int index);
    void signalAddOverlayTimeFailed(int ch);

private slots:
    void queueSetOverlayText(int channel, int index);
    void queueRemoveOverlayText(int channel, int index);
    void queueRemoveAllOverlayText(int channel);
    void queueSetAllOverlayText(int channel);
    void queueShowTimer(int channel);
    void queueRemoveTimer(int channel);
    void queueAddOverlayImage(int channel);
    void queueRemoveOverlayImage(int channel);
    void queueLoadFont(int channel);
    void queueOverlayImageAlpha(int channel, int index, int alpha);
    void queueSetPrivacyMask(int channel, int index);
    void queueRemovePrivacyMask(int channel, int index);
    void queueUpdatePrivacyMaskColor(int channel, int index);

private:
    bool emitdone;
    QThread *thread;
};

#endif // OVERLAYTASK_H
