/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef UTILITY_H
#define UTILITY_H
#include <QObject>
#include <QString>

class Utility : public QObject
{
    Q_OBJECT

    static Utility *s_instance;

    // private constructor
    Utility(){}

    void systemCommand(QString cmd);

public:
    static Utility *shared();

    ~Utility();
    QString exec(QString cmd);
    bool uploadYoutube(QString cmd);

signals:
    void updateText(QString);
    void commandDone();

};

#endif // UTILITY_H
