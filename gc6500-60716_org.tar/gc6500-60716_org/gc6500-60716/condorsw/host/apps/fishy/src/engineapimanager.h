/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef ENGINEAPIHANDLER_H
#define ENGINEAPIHANDLER_H

#include <QObject>
#include <QThread>
#ifdef WITH_ENGINE
#include "interface.h"
#endif

class EngineApiManager : public QObject
{
    Q_OBJECT

private:
    QThread *thread;
    video_channel_t channel;
    int panel;
    dewarp_mode_t dmode;
    dewarp_params_t dparams;

public:
    explicit EngineApiManager(QObject *parent, QString name);
    ~EngineApiManager();

    void engineInit(int deviceID);
    void engineGetCameraConfig();
    void engineGetCameraInfo(video_channel_t channel);
    void engineGetDewarpParams(video_channel_t channel, int panel);
    void engineSetDewarpParams(video_channel_t channel, int panel, dewarp_mode_t mode, dewarp_params_t *params);
    void engineStartVideo(video_channel_t channel);
    void engineSetResolution(video_channel_t channel, int width, int height);
    void engineSetVideoFormat(video_channel_t channel, video_format_t format);
    void engineGetMicVolume();
    void engineSetMicVolume(int volume);
    void engineMuteMic(int mute);
    void engineMuteMicLeft(int mute);
    void engineMuteMicRight(int mute);
    void engineAGC(int enable);

    // Overlay
    void engineAddOverlayText(int channel, QString text, int xoff, int yoff, int* id);

    void startThread();
    void cleanup();

signals:
    void signalEngineInitSuccess(int camType);
    void signalEngineInitFailed();
    void signalEngineGetCameraConfigSuccess();
    void signalEngineGetCameraConfigFailed(int error);
    void signalEngineGetDewarpParamsSuccess(video_channel_t channel, int panel, dewarp_mode_t mode, dewarp_params_t params);
    void signalEngineGetDewarpParamsFailed(video_channel_t channel, int panel);
    void signalEngineSetDewarpParamsSuccess(video_channel_t channel, int panel, dewarp_mode_t mode, dewarp_params_t params);
    void signalEngineSetDewarpParamsFailed(video_channel_t channel, int panel);
    void signalEngineStartVideoSuccess(video_channel_t channel);
    void signalEngineStartVideoFailed(video_channel_t channel);
    void signalEngineGetChannelInfoSuccess(video_channel_t channel, video_channel_info_t info);
    void signalEngineGetChannelInfoFailed(video_channel_t channel, int error);
    void signalEngineSetResolutionSuccess(int width, int height);
    void signalEngineResolutionNotSupported();
    void signalEngineSetResolutionFailed();
    void signalEngineSetVideoFormatSuccess();
    void signalEngineSetVideoFormatFailed();
    void signalEngineGetMicVolumeSuccess(int volume);
    void signalEngineGetMicVolumeFailed();
    void signalEngineSetMicVolumeSuccess();
    void signalEngineSetMicVolumeFailed();
    void signalEngineMuteMicSuccess();
    void signalEngineMuteMicFailed();
    void signalEngineAGCSuccess();
    void signalEngineAGCFailed();
    void signalEngineAddOverlayTextSuccess();
    void signalEngineAddOverlayTextFailed();

    void signalCleanup();

public slots:

private slots:
    void queueCleanup();
    void queueEngineInit(int deviceID);
    void queueEngineGetCameraConfig();
    void queueEngineGetCameraInfo();
    void queueEngineGetDewarpParams();
    void queueEngineSetDewarpParams();
    void queueEngineStartVideo();
    void queueEngineSetResolution(video_channel_t channel, int width, int height);
    void queueEngineSetVideoFormat(video_channel_t channel, video_format_t format);
    void queueEngineGetMicVolume();
    void queueEngineSetMicVolume(int volume);
    void queueEngineMuteMic(int mute);
    void queueEngineMuteMicLeft(int mute);
    void queueEngineMuteMicRight(int mute);
    void queueEngineAGC(int enable);
    void queueEngineAddOverlayText(int channel, QString text, int xoff, int yoff, int* id);
};

#endif // ENGINEAPIHANDLER_H
