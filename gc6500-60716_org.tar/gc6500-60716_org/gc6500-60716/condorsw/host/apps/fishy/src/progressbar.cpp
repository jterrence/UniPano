/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include <QProgressBar>
#include <QHBoxLayout>

#include "progressbar.h"

ProgressBar::ProgressBar(QWidget *parent) :
    QDialog(parent) {

    setWindowFlags(Qt::Popup|
                   Qt::Dialog|
                   Qt::FramelessWindowHint);
    //setAttribute(Qt::WA_DeleteOnClose);
    setModal(true);

    bar = new QProgressBar(this);
    bar->setTextVisible(false);
    bar->setRange(0,0);

    QHBoxLayout *layout = new QHBoxLayout;
    layout->setContentsMargins(6,6,6,6);
    layout->addWidget(bar);

    //setAutoFillBackground(true);
    setLayout(layout);

    bar->setStyleSheet("background-color: rgb(255,255,255);");
    setStyleSheet("border: 1px solid white; border-radius: 4px;");
    QPalette pal(palette());
    pal.setColor(QPalette::Background, QColor(100,100,100,150));
    setPalette(pal);
}

ProgressBar::~ProgressBar() {
    bar->reset();
    delete bar;
}
