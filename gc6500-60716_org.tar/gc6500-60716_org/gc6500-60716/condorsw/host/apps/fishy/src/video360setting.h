/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef VIDEO360SETTING_H
#define VIDEO360SETTING_H

#include <QWidget>
#include <QTimer>
#include <QTime>

namespace Ui {
    class Video360Setting;
}

class Video360Setting : public QWidget
{
    Q_OBJECT

public:
    explicit Video360Setting(QWidget *parent = 0);
    ~Video360Setting();

private:
    Ui::Video360Setting *ui;
    QTimer *timer ;
    QTime *time;
private slots:
    void on_buttonStartRecord_clicked();
    void updateCaption();
    void uploadVideo();

signals:
    void capture360VideoStart();
    void capture360VideoStop();
};

#endif // VIDEO360SETTING_H
