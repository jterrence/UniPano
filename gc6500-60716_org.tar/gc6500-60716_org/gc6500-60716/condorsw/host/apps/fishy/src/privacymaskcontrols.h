/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef PRIVACYMASK_H
#define PRIVACYMASK_H

#include <QWidget>
#include <QColor>
#include <QStringList>
#include <QStringListModel>
#include <QModelIndex>

namespace Ui {
    class PrivacyMaskControls;
}

class PrivacyMaskControls : public QWidget
{
    Q_OBJECT

public:
    explicit PrivacyMaskControls(QWidget *parent = 0);
    ~PrivacyMaskControls();

    void update();
    void updateHightlightAllCheckBox();
    void enableHightlight(int index, bool enable);
    void updatePosition(int index, int x, int y);
    void selectIndex(int index);

private:
    Ui::PrivacyMaskControls *ui;
    QStringListModel *model;

    bool validateControls();
    void setControlValues(int);

    void keyPressEvent(QKeyEvent *event);

protected:
    bool eventFilter(QObject *obj, QEvent *event);

signals:
    void signalAddPrivacyMask(int channel, int index, int x, int y, int w, int h, QColor color);
    void signalRemovePrivacyMask(int channel, int index);
    void signalHighlightChanged(int,int,bool);
    void signalRemoveAllMasks(int channel);
    void signalColorUpdated(int channel, int index, QColor color);
    void signalMaskSelected(int channel, int index);

    void signalXChanged(int);
    void signalYChanged(int);

private slots:
    void showColorPicker();
    void onButtonApplyClicked();
    void onXSpinBoxEditingFinished();
    void onYSpinBoxEditingFinished();
    void onWSpinBoxEditingFinished();
    void onHSpinBoxEditingFinished();
    void onButtonRemoveClicked();
    void onHighlightChanged(bool);
    void onRemoveAllMasks();
    void onHightlightAllToggled(bool);
    void onMaskSelected(QModelIndex);
};

#endif // PRIVACYMASK_H
