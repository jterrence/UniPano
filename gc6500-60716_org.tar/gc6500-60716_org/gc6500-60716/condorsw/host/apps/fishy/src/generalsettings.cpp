/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include <QDebug>
#include <QMessageBox>
#include "generalsettings.h"
#include "ui_generalsettings.h"
#include "alerthandler.h"
#include "log.h"

GeneralSettings::GeneralSettings(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GeneralSettings)
{
    ui->setupUi(this);

    queryDumpFile = "query.dump";
    singleChannelLayoutEnabled = true;
    multipleChannelLayoutEnabled = true;
    eptzLayoutEnabled = false;
    privacyEnabled = true;
    audioEnabled = true;

    ui->buttonAbout->setToolTip("About");
    //ui->buttonDump->setToolTip("Query Dump");
    ui->buttonMic->setToolTip("Audio Record");
    ui->buttonVideo->setToolTip("Video Record");
    ui->buttonRAW->setToolTip("Raw Video Channel");
    ui->buttonDewarp->setToolTip("Dewarp Settings");
    ui->buttonCameraSettings->setToolTip("Camera Settings");
    ui->buttonSingleChannel->setToolTip("Single Channel Layout");
    ui->buttonMultiChannel->setToolTip("Multi Channel Layout");
    ui->buttonEptz->setToolTip("EPTZ Layout");
    ui->buttonExit->setToolTip("Exit Application");
    ui->buttonPrivacy->setToolTip("Privacy Mask");

    connect(ui->buttonExit,SIGNAL(clicked()),SIGNAL(settingsExitClicked()));
    connect(ui->buttonDewarp,SIGNAL(clicked()),SIGNAL(signalOnDewarpClicked()));
    connect(ui->buttonEptz,SIGNAL(clicked()),SLOT(onEptzLayoutClicked()));
    connect(ui->buttonCameraSettings,SIGNAL(clicked()),SIGNAL(settingsCameraSettingsClicked()));
    connect(ui->buttonRAW,SIGNAL(clicked()),SLOT(selectRawButton()));
    connect(ui->buttonRAW,SIGNAL(clicked()),SIGNAL(signalOnRawClicked()));
    connect(ui->buttonVideo,SIGNAL(clicked()),SIGNAL(signalVideoRecordClicked()));

    connect(ui->buttonMic,SIGNAL(clicked()),SIGNAL(settingsMicButtonClicked()));
    connect(ui->buttonAbout,SIGNAL(clicked()),SLOT(settingsAboutClicked()));
    connect(ui->buttonMultiChannel,SIGNAL(clicked()),SLOT(onMultipleChannelLayoutClicked()));
    connect(ui->buttonSingleChannel,SIGNAL(clicked()),SLOT(onSingleChannelLayoutClicked()));

    connect(ui->buttonPrivacy, SIGNAL(clicked()), this, SLOT(onPrivacyClicked()));

    //connect(ui->buttonDump, SIGNAL(clicked()), this, SLOT(onDumpClicked()));

    ui->buttonMultiChannel->setCheckable(true);
    ui->buttonSingleChannel->setCheckable(true);

    //setAttribute(Qt::WA_OpaquePaintEvent);
}

GeneralSettings::~GeneralSettings()
{
    delete ui;
}

void GeneralSettings::setSingleChannelLayoutEnabled(bool enabled) {
    singleChannelLayoutEnabled = enabled;
    ui->buttonSingleChannel->setEnabled(enabled);
}

void GeneralSettings::setMultiChannelLayoutEnabled(bool enabled) {
    multipleChannelLayoutEnabled = enabled;
    ui->buttonMultiChannel->setEnabled(enabled);
}

void GeneralSettings::setEptzLayoutEnabled(bool enabled) {
    eptzLayoutEnabled = enabled;
    if (!enabled) {
        ui->buttonEptz->deleteLater();
    }
}

void GeneralSettings::setPrivacyEnabled(bool enabled) {
    privacyEnabled = enabled;
    ui->buttonPrivacy->setEnabled(enabled);
}

void GeneralSettings::setAudioEnabled(bool enabled) {
    audioEnabled = enabled;
    ui->buttonMic->setEnabled(enabled);
}

void GeneralSettings::selectSingleChannelLayout() {
    ui->buttonSingleChannel->setChecked(true);
    ui->buttonSingleChannel->setEnabled(false);
    ui->buttonMultiChannel->setChecked(false);
    if (multipleChannelLayoutEnabled) {
        ui->buttonMultiChannel->setEnabled(true);
    }
    if (eptzLayoutEnabled) {
        ui->buttonEptz->setEnabled(true);
        ui->buttonEptz->setChecked(false);
    }
    ui->buttonPrivacy->setChecked(false);
    //ui->buttonPrivacy->setEnabled(true);
    toggleNonLayoutButtons(true);
}

void GeneralSettings::selectMultipleChannelLayout() {
    ui->buttonMultiChannel->setChecked(true);
    ui->buttonMultiChannel->setEnabled(false);
    ui->buttonSingleChannel->setChecked(false);
    if (singleChannelLayoutEnabled) {
        ui->buttonSingleChannel->setEnabled(true);
    }
    if (eptzLayoutEnabled) {
        ui->buttonEptz->setEnabled(true);
        ui->buttonEptz->setChecked(false);
    }
    ui->buttonPrivacy->setChecked(false);
    //ui->buttonPrivacy->setEnabled(true);
    toggleNonLayoutButtons(true);
}

void GeneralSettings::selectEPTZLayout() {
    ui->buttonEptz->setChecked(true);
    ui->buttonEptz->setEnabled(false);
    ui->buttonSingleChannel->setChecked(false);
    ui->buttonMultiChannel->setChecked(false);
    if (singleChannelLayoutEnabled) {
        ui->buttonSingleChannel->setEnabled(true);
    }
    if (multipleChannelLayoutEnabled) {
        ui->buttonMultiChannel->setEnabled(true);
    }
    ui->buttonPrivacy->setChecked(false);
    toggleNonLayoutButtons(true);
}

void GeneralSettings::selectPrivacyLayout() {
    ui->buttonPrivacy->setChecked(true);
    ui->buttonPrivacy->setEnabled(false);
    if (eptzLayoutEnabled) {
        ui->buttonEptz->setEnabled(true);
        ui->buttonEptz->setChecked(false);
    }
    ui->buttonSingleChannel->setChecked(false);
    ui->buttonMultiChannel->setChecked(false);
    if (singleChannelLayoutEnabled) {
        ui->buttonSingleChannel->setEnabled(true);
    }
    if (multipleChannelLayoutEnabled) {
        ui->buttonMultiChannel->setEnabled(true);
    }
    toggleNonLayoutButtons(false);
}

void GeneralSettings::toggleNonLayoutButtons(bool enable) {
    ui->buttonCameraSettings->setEnabled(enable);
    ui->buttonDewarp->setEnabled(enable);
    ui->buttonVideo->setEnabled(enable);

    if (privacyEnabled) {
        ui->buttonPrivacy->setEnabled(enable);
    }

    if (audioEnabled) {
        ui->buttonMic->setEnabled(enable);
    }
}

void GeneralSettings::setRawButtonEnabled(bool enabled)
{
    ui->buttonRAW->setChecked(!enabled);
    ui->buttonRAW->setEnabled(enabled);
}

void GeneralSettings::hideEvent(QHideEvent *event)
{
    event->accept();
}

void GeneralSettings::selectRawButton() {
    ui->buttonRAW->setEnabled(false);
    ui->buttonRAW->setChecked(true);
}

void GeneralSettings::deselectRawButton() {
    ui->buttonRAW->setChecked(false);
    ui->buttonRAW->setEnabled(true);
}

void GeneralSettings::settingsAboutClicked()
{
    AlertHandler::displayMessage("Application Version : "+
                                 QString(VERSION)+
                                 "\nFishy demonstrates various dewarp modes of the camera and controls individual channel settings.",
                                 "Fishy");
}

void GeneralSettings::mousePressEvent(QMouseEvent *event)
{
    event->accept();
}

// PRIVATE SLOTS
void GeneralSettings::onSingleChannelLayoutClicked() {
    selectSingleChannelLayout();
    emit(signalSingleChannelLayout());
}
void GeneralSettings::onMultipleChannelLayoutClicked() {
    selectMultipleChannelLayout();
    emit(signalMultiChannelLayout());
}
void GeneralSettings::onEptzLayoutClicked() {
    selectEPTZLayout();
    emit(signalOnEptzClicked());
}
void GeneralSettings::onPrivacyClicked() {
    selectPrivacyLayout();
    emit signalOnPrivacyClicked();
}

void GeneralSettings::setVideoRecordingState(bool state) {
    QIcon icon;
    if (state) {
        icon.addFile(QString::fromUtf8(":/images/geoLogo/video-recording"), QSize(), QIcon::Normal, QIcon::Off);
    } else {
        icon.addFile(QString::fromUtf8(":/images/geoLogo/video"), QSize(), QIcon::Normal, QIcon::Off);
    }
    icon.addFile(QString::fromUtf8(":/images/geoLogo/video-disabled"), QSize(), QIcon::Disabled, QIcon::Off);
    ui->buttonVideo->setIcon(icon);
}

//void GeneralSettings::onQueryDumpFinished(int exitCode, QProcess::ExitStatus exitStatus) {
//    if (exitCode == 0 && exitStatus == QProcess::NormalExit) {
//        QFile file (queryDumpFile);
//        if (file.exists()) {
//            if (file.open(QIODevice::ReadOnly)) {
//                if (file.readAll().length() == 0) {
//                    file.close();
//                    file.remove();
//                    AlertHandler::displayError("Failed to execute querydump. Please run fishy with sudo permission");
//                } else {
//                    file.close();
//                    AlertHandler::displayInfo("Logs dumped to "+queryDumpFile);
//                }
//            } else {
//                AlertHandler::displayError("Querydump failed");
//            }
//        } else {
//            AlertHandler::displayError("Querydump failed");
//        }
//    } else {
//        AlertHandler::displayError("Querydump failed");
//    }
//}
//void GeneralSettings::onQueryDumpError(QProcess::ProcessError error) {
//    switch(error) {
//        case QProcess::FailedToStart:
//            AlertHandler::displayError("Failed to find/run querydump. Please make sure querydump is available and/or fishy is run with sudo permission");
//            break;
//        default:
//            AlertHandler::displayError("Querydump failed");
//            break;
//    }
//}
//void GeneralSettings::onDumpClicked() {
//    QString querydump(TOOLS);
//    querydump.append("/querydump/querydump");

//    QEventLoop waitLoop;
//    QFile file (queryDumpFile);
//    if (file.exists() && file.remove()==false) {
//        qWarning() << "Failed to delete file:" << queryDumpFile;
//    }

//    QProcess process;
//    //process.setStandardOutputFile(queryDumpFile);
//    connect(&process, SIGNAL(finished(int,QProcess::ExitStatus)), &waitLoop, SLOT(quit()));
//    connect(&process, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(onQueryDumpFinished(int,QProcess::ExitStatus)));
//    connect(&process, SIGNAL(error(QProcess::ProcessError)), this, SLOT(onQueryDumpError(QProcess::ProcessError)));
//    process.start(querydump, QStringList()<<queryDumpFile);
//    waitLoop.exec();
//}
// END PRIVATE SLOTS
