/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include "log.h"
#include "alerthandler.h"
#include "videorecord.h"
#include "ui_videorecord.h"
#include "cameraconfig.h"
#include "stringutils.h"
#include "utility.h"
#include <QTcpSocket>
#ifdef QT5
#include <QtConcurrent/QtConcurrentRun>
#else
#include <QtConcurrentRun>
#endif

VideoRecord::VideoRecord(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::VideoRecord)
{
    ui->setupUi(this);
    socket = NULL;
    time = new QTime();
    timer = new QTimer(this);
    autoRecordStopTimer = new QTimer(this);
    recordingChannel = CH1;

    youtubeUploads = 0;
    ui->cbUpload->setChecked(false);

    autoRecordStopTimer->setSingleShot(true);

    connect(timer, SIGNAL(timeout()), this, SLOT(updateCaption()));
    connect(autoRecordStopTimer, SIGNAL(timeout()), this, SLOT(uploadVideo()));

    setWindowTitle(tr("Video Record"));
    for(int i=0; i< CameraConfig::shared()->getNumH264Channels();i++)
        this->ui->cbChannel->addItem(StringUtils::shared()->channelToString((video_channel_t)i));
    ui->editDuration->setValidator( new QIntValidator(0, 65500, this) );

    ui->editFilePath->setText("out."+ui->cbChannel->itemText(0));

    socket = new QTcpSocket(this);
    connect(socket,SIGNAL(connected()),this,SLOT(networkConnected()));
    socketTimer = new QTimer();
    socketTimer->setSingleShot(true);
    socketTimer->setInterval(3000);
    connect(socketTimer,SIGNAL(timeout()),this,SLOT(networkDisconnected()));
}

VideoRecord::~VideoRecord()
{
    delete ui;
    if (socket) {
        delete socket;
    }
    if (socketTimer) {
        delete socketTimer;
    }
}

void VideoRecord::hideEvent(QHideEvent *) {
    emit(signalHidden());
}

void VideoRecord::showEvent(QShowEvent *) {
    if (this->youtubeUploads) {
        if (ui->buttonStartRecord->isEnabled() == false || ui->buttonStartRecord->text().compare("Stop") == 0) {
            // Record or upload in progress
            return;
        }

        ui->buttonStartRecord->setEnabled(false);
        ui->editLogs->setText("Checking network connectivity...");

        socket->connectToHost("www.youtube.com",80,QTcpSocket::ReadOnly);
        socketTimer->start();
    }
}

void VideoRecord::networkConnected() {
    if (socketTimer->isActive()) {
        socketTimer->stop();
    }
    socket->disconnectFromHost();

    ui->editLogs->append("Network connected!");
    ui->buttonStartRecord->setEnabled(true);
    ui->cbUpload->setEnabled(true);
}

void VideoRecord::networkDisconnected() {
    ui->editLogs->append("Network not available. Disabling YouTube uploads!\n");
    ui->buttonStartRecord->setEnabled(true);
    ui->cbUpload->setChecked(false);
    ui->cbUpload->setEnabled(false);
}

void VideoRecord::on_buttonStartRecord_clicked()
{
    if(this->isVisible() && ui->editFilePath->text().isEmpty()) {
        AlertHandler::displayError("Please enter file path");
        return;
    }

    if(ui->editDuration->text().isEmpty())
    {
        AlertHandler::displayError("Please enter duration in seconds");
        return;
    }
    if(!autoRecordStopTimer->isActive())
    {
        ui->buttonStartRecord->setText("Stop");
        recordingChannel = ui->cbChannel->currentIndex();
        fileName = ui->editFilePath->text().append(".mp4");

        // Set timer +1 to work around file dump delay
        autoRecordStopTimer->start((ui->editDuration->text().toInt()+1) * 1000);

        engineStartCapture();
        time->restart();
        timer->start(1000);
        updateCaption();
    }
    else
    {
        ui->buttonStartRecord->setText("Start");
        timer->stop();
        autoRecordStopTimer->stop();
        uploadVideo();
    }
}

void VideoRecord::updateCaption()
{
    int secs = time->elapsed() / 1000;
    int mins = (secs / 60) % 60;
    secs = secs % 60;
    QString local(QString("%2:%3")
                  .arg(mins, 2, 10, QLatin1Char('0'))
                  .arg(secs, 2, 10, QLatin1Char('0')));
    ui->editLogs->setText("Recording: "+local);
    recordTime = local;
}

void VideoRecord::uploadVideo()
{
    qDebug()<<"\nTimer elapsed upload now \n";
    ui->buttonStartRecord->setText("Start");
    timer->stop();
    ui->buttonStartRecord->setEnabled(true);
    ui->editLogs->append("Record data dumped to " + fileName + "\nRecord duration: " + recordTime);
    engineStopCapture();
}

void VideoRecord::engineStartCapture()
{
    //Starting mp4 video Capture
    engine_start_mp4_dump_video(recordingChannel, fileName.toStdString().c_str());
}

void uploadScript(QString commandName)
{
    int ret = system(commandName.toLatin1().data());
    qDebug()<<"python return script values is "<< ret<< endl;
}

void VideoRecord::engineStopCapture()
{
    //Stoping mp4 video Capture
    engine_stop_mp4_dump_video(recordingChannel);
    if(ui->cbUpload->isVisible() && ui->cbUpload->isChecked())
    {
        ui->buttonStartRecord->setEnabled(false);
        //Call the script in a separate thread to upload the video.

        ui->editLogs->append("Recording: done");
        if (Utility::shared()->uploadYoutube(fileName)) {
            connect(Utility::shared(),SIGNAL(updateText(QString)),this,SLOT(uploadStatus(QString)));
            connect(Utility::shared(),SIGNAL(commandDone()),this,SLOT(uploadComplete()));
            ui->editLogs->append("Started Upload...");
        } else {
            ui->editLogs->append("Error: upload script missing");
        }
    }
}

void VideoRecord::setYoutubeUpload(bool val) {
    this->youtubeUploads = val;
    ui->cbUpload->setVisible(val);
    ui->lblYoutube->setVisible(val);
    if(val) {
        ui->editFilePath->setText("mobi360");
    } else {
        ui->editFilePath->setText("out."+StringUtils::shared()->channelToString((video_channel_t)recordingChannel).toLower());
    }
}

void VideoRecord::on_buttonDone_clicked()
{
    this->close();
}

void VideoRecord::uploadStatus(QString text) {
    if(text.contains("Video id")) {
        int startid = text.indexOf("'")+1;
        int endid = text.lastIndexOf("'");
        ui->editLogs->append("Upload: complete");
        ui->editLogs->append("https://www.youtube.com/watch?v=" + text.mid(startid, endid-startid));
    } else if (text.contains("ImportError")) {
        ui->editLogs->append("YouTube ApiClient is not installed!\nPlease run ./youtube/setup.sh");
    } else if (text.toLower().contains("error")) {
        ui->editLogs->append(text);
    }
}

void VideoRecord::uploadComplete()
{
    ui->buttonStartRecord->setEnabled(true);
    disconnect(Utility::shared(),SIGNAL(updateText(QString)),this,SLOT(uploadStatus(QString)));
    disconnect(Utility::shared(),SIGNAL(commandDone()),this,SLOT(uploadComplete()));

}
