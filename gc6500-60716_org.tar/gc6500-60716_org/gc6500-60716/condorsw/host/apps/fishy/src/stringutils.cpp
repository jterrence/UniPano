/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include "stringutils.h"
#include "cameraconfig.h"

StringUtils *StringUtils::s_instance = 0;

QString StringUtils::videoFormatToString(video_format_t format) {
    switch (format) {
        case VID_FORMAT_H264_RAW:
            return "H264 ES";
        case VID_FORMAT_H264_TS:
            return "H264 TS";
        case VID_FORMAT_MJPEG_RAW:
            return "MJPEG";
        case VID_FORMAT_YUY2_RAW:
            return "YUY2 RAW";
        case VID_FORMAT_NV12_RAW:
            return "NV12 RAW";
        case VID_FORMAT_GREY_RAW:
            return "GREY RAW";
        case VID_FORMAT_H264_AAC_TS:
            return "H264 AAC TS";
        case VID_FORMAT_MUX:
            return "MUX";
        default:
            return "Unknown format";
    }
    return "Unknown format";
}

QString StringUtils::channelToString(video_channel_t channel) {
    if (CameraConfig::shared()->getCameraMode() == IPCAM) {
        return QString("CH").append(QString::number((int)channel+1));
    } else {
        switch (channel) {
            case CH_MAIN:
                return QString("CH_MAIN");
            case CH_PREVIEW:
                return QString("CH_PREVIEW");
            default:
                QString("CH%1").arg((int)channel);
        }
    }
    return QString("CH%1").arg((int)channel);
}

QString StringUtils::dewarpModeToString(dewarp_mode_t mode) {
    switch(mode) {
        case EMODE_OFF:
            return "Off";
        case EMODE_WM_ZCL:
            return "ZCL";
        case EMODE_WM_ZCLCylinder:
            return "ZCLCylinder";
        case EMODE_WM_ZCLStretch:
            return "ZCLStretch";
        case EMODE_WM_1PanelEPTZ:
            return "1PanelEPTZ";
        case EMODE_WM_Sweep_1PanelEPTZ:
            return "Sweep_1PanelEPTZ";
        case EMODE_WM_Magnify:
            return "Magnify";
        default:
            return "-";
    }

    return "-";
}

QString StringUtils::panelModeToString(panel_mode_t mode) {
    switch (mode) {
        case PMODE_OFF:
            return "Off";
        case PMODE_ON:
            return "On";
        case PMODE_SELECT:
            return "Select";
        case PMODE_UNSELECT:
            return "Unselect";
        default:
            return "-";
    }
    return "-";
}

QString StringUtils::videoProfileToString(video_profile_t profile) {
    switch (profile) {
        case PROFILE_BASELINE:
            return "Baseline";
        case PROFILE_MAIN:
            return "Main";
        case PROFILE_HIGH:
            return "High";
        default:
            return "-";
    }
    return "-";
}
