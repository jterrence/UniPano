#-------------------------------------------------
#
# Project created by QtCreator 2013-11-12T06:03:20
#
#-------------------------------------------------

QT       += core gui network
greaterThan(QT_MAJOR_VERSION, 4) {
    QT += widgets
}

exists( /usr/include/QtMultimedia ) {
        message( "Found QtMultimedia" )
        QT += multimedia
} exists( /usr/include/qt4/QtMultimedia ) {
        message( "Found QtMultimedia" )
        QT += multimedia
} exists( /usr/include/qt5/QtMultimedia ) {
        message( "Found QtMultimedia" )
        QT += multimedia
} exists( /usr/include/QtMobility ) {
    message( "Found QtMobility" )
    exists( /usr/include/QtMultimediaKit ) {
        message ("Found QtMultimediaKit")
        CONFIG += mobility
        MOBILITY = multimedia
    }
}

contains(QT_CONFIG, opengl): QT += opengl

SVN_VERSION	= $$system((which svnversion > /dev/null && svnversion) || (echo "exported") || (echo "Unversioned"))

LIB_PATH        = $$(GEOSW_ROOT)/installed/lib
MXUVC_INCPATH	= $$(GEOSW_ROOT)/condorsw/host/lib/mxuvc/include
BIN_DIR         = $$(GEOSW_ROOT)/installed/bin

TARGET		= ../fishy
TEMPLATE	= app
VERSION         = 3.0
INCLUDEPATH	+=  $$MXUVC_INCPATH
INCLUDEPATH += $$(GEOSW_ROOT)/thirdparty/installed/i686-linux/include
INCLUDEPATH	+= ../include

SOURCES	+=			\
        camerasettings.cpp  \
        channelbar.cpp  \
        dewarpsettings.cpp  \
        displaychannel.cpp  \
        generalsettings.cpp \
        main.cpp    \
        mainwindow.cpp  \
        toastmessage.cpp    \
        videoitem.cpp   \
        videorendersurface.cpp  \
        videosettings.cpp   \
        customslider.cpp    \
        audiorecord.cpp \
        alerthandler.cpp    \
        engineapimanager.cpp    \
        scrollablewindow.cpp    \
        cameraconfig.cpp    \
        stringutils.cpp \
        channellayout.cpp \
    overlaymanager.cpp \
    eptz.cpp \
    eptzcontrols.cpp \
	video360setting.cpp \
    videorecord.cpp \
    utility.cpp \
    overlaytask.cpp \
    channelstate.cpp \
    privacymasklayout.cpp \
    privacymaskcontrols.cpp \
    progressbar.cpp \
    draggableframe.cpp \
    qcustomplot.cpp

HEADERS	+=  \
        abstractlayout.h    \
        camerasettings.h    \
        channelbar.h    \
        dewarpsettings.h    \
        displaychannel.h    \
        generalsettings.h   \
        mainwindow.h    \
        toastmessage.h  \
        videoitem.h \
        videorendersurface.h    \
        videosettings.h \
        customslider.h  \
        audiorecord.h   \
        alerthandler.h  \
        engineapimanager.h  \
        scrollablewindow.h  \
        cameraconfig.h  \
        stringutils.h   \
        log.h   \
        channellayout.h \
    overlay.h \
    overlaymanager.h \
    eptz.h \
    eptzcontrols.h \
	video360setting.h \
    videorecord.h \
    utility.h \
    overlaytask.h \
    channelstate.h \
    privacymasklayout.h \
    privacymaskcontrols.h \
    progressbar.h \
    draggableframe.h \
    qcustomplot.h

FORMS	+=  \
        camerasettings.ui   \
        channelbar.ui   \
        dewarpsettings.ui   \
        displaychannel.ui   \
        generalsettings.ui  \
        mainwindow.ui   \
        toastmessage.ui \
        videosettings.ui    \
        customslider.ui \
        audiorecord.ui  \
        channellayout.ui \
        eptzcontrols.ui \
        video360setting.ui \
        videorecord.ui \
    privacymaskcontrols.ui

RESOURCES	+=		\
		geoLogo.qrc


DEFINES += WITH_ENGINE
DEFINES += WITH_MOTION_RECTS
DEFINES += DUAL_RENDERING
# Use ffmpeg to decode frames in the UI
DEFINES += FFMPEG
DEFINES += VERSION=\\\"$$unique(VERSION)\\\"
greaterThan(QT_MAJOR_VERSION, 4) {
DEFINES += QT5
}

QMAKE_CXXFLAGS += -Werror
QMAKE_LIBDIR_QT -= /usr/lib

LIBS		+=  -L$(GEOSW_ROOT)/thirdparty/installed/i686-linux/lib \
                    -L../lib/   \
                    -L/usr/lib  \
                    -lengine    \
                    -lavcodec   \
                    -lavfilter  \
                    -lavutil    \
                    -lswresample    \
                    -lswscale   \
                    $${LIB_PATH}/libmxuvc_$${VIDEO}_alsa_allplugins_trace1.a    \
                    -lasound    \
                    -lusb-1.0	\
                    -logg \
		    -lrt \
                    -lmp4v2

ARCH_X86 = $$ARCH_X86
equals(ARCH_X86, 1) {
    message("32 bit OS")
    INCLUDEPATH	+=  $$(GEOSW_ROOT)/condorsw/host/lib/ePTZ
    LIBS += -L$(GEOSW_ROOT)/algorithms/video/analytics/smartmotion/build_x86 \
            -lsmalg \
            -lsmplat \
            $${LIB_PATH}/libePTZ.a
} else {
    message("64 bit OS")
}

PRE_TARGETDEPS  += ../lib/libengine.a

target.path	= $${BIN_DIR}/
INSTALLS	+= target









