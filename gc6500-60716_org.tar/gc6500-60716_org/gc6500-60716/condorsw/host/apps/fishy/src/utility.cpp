/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include <QThread>
#ifdef QT5
#include <QtConcurrent/QtConcurrentRun>
#else
#include <QtConcurrentRun>
#endif
#include <iostream>
#include <stdio.h>
#include <QFileInfo>
#include "utility.h"

Utility *Utility::s_instance = 0;

Utility::~Utility() {
    if (s_instance) {
        delete s_instance;
    }
}

Utility* Utility::shared() {
    if (!s_instance) {
        s_instance = new Utility();
    }
    return s_instance;
}

QString Utility::exec(QString ) {

    return  "";
}

void Utility::systemCommand(QString cmd) {
    FILE* pipe = popen(cmd.toLatin1().constData(), "r");
    if (!pipe) {
        emit updateText("ERROR opening the pipe");
        return;
    }
    char buffer[128];
    QString result = "";
    while(!feof(pipe)) {
        if(fgets(buffer, 128, pipe) != NULL) {
            result = buffer;
            qDebug()<<result.trimmed();
            emit (updateText(result));
        }
    }
    pclose(pipe);
    emit (commandDone());
}

bool Utility::uploadYoutube(QString fileName) {
    QFileInfo fileInfo("src/youtube/upload360.py");
    if (!fileInfo.exists()) {
        return false;
    }
    QString commandName("python src/youtube/upload360.py --infile ");
    commandName.append(fileName + " --outfile /tmp/mobi360.mp4 2>&1");
    qDebug()<<commandName<<endl;
    QtConcurrent::run(this,&Utility::systemCommand, commandName);
    return  true;
}
