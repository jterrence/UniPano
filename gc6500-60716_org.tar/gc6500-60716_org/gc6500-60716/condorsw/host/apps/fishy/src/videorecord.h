/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef VIDEORECORD_H
#define VIDEORECORD_H

#include <QDialog>
#include <QTime>
#include <QTimer>

class QTcpSocket;

namespace Ui {
    class VideoRecord;
}

class VideoRecord : public QDialog
{
    Q_OBJECT

public:
    explicit VideoRecord(QWidget *parent = 0);
    ~VideoRecord();
    void setYoutubeUpload(bool val);

protected:
    void hideEvent(QHideEvent *);
    void showEvent(QShowEvent *);

private:
    QTimer *timer;
    QTimer *autoRecordStopTimer;
    QTime *time;
    QString recordTime;
    QString fileName;
    int recordingChannel;
    bool youtubeUploads;
    QTcpSocket *socket;
    QTimer *socketTimer;

    void engineStartCapture();
    void engineStopCapture();

private slots:
    void on_buttonStartRecord_clicked();
    void on_buttonDone_clicked();
    void updateCaption();
    void uploadVideo();
    void uploadStatus(QString);
    void uploadComplete();
    void networkConnected();
    void networkDisconnected();

signals:
    void captureVideoStart();
    void captureVideoStop();
    void signalHidden();

private:
    Ui::VideoRecord *ui;
};

#endif // VIDEORECORD_H
