/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include "videosettings.h"
#include "ui_videosettings.h"
#include "cameraconfig.h"
#include "stringutils.h"
#include "log.h"
#include <QFileDialog>
#include <QMouseEvent>
#include <QFileInfo>
#include <alerthandler.h>
#include "overlay.h"
#include "overlaymanager.h"

VideoSettings::VideoSettings(QWidget *parent, int ch) :
    QWidget(parent),
    ui(new Ui::VideoSettings),
    channel(ch),
    videoRunning(true),
    moved(false)
{
    ui->setupUi(this);

    connect(ui->bitrateSliderWidget, SIGNAL(signalValueChanged(int)), this, SIGNAL(bitRateSliderMoved(int)));
    connect(ui->framerateSliderWidget, SIGNAL(signalValueChanged(int)), this, SIGNAL(frameRateSliderMoved(int)));
    connect(ui->gopSliderWidget, SIGNAL(signalValueChanged(int)), this, SIGNAL(gopSliderMoved(int)));
    connect(ui->profileCombo,SIGNAL(currentIndexChanged(int)),this,SIGNAL(profileIndexChanged(int)));
    connect(ui->levelCombo,SIGNAL(currentIndexChanged(int)),this,SIGNAL(levelIndexChanged(int)));
    connect(ui->iFrameButton,SIGNAL(clicked()),this,SIGNAL(iFrameRequested()));
    connect(ui->buttonResolution, SIGNAL(clicked()), this, SLOT(onButtonApplyResolutionClicked()));
    connect(ui->formatCombo,SIGNAL(currentIndexChanged(int)),this,SLOT(onFormatChanged(int)));
    connect(ui->qualitySliderWidget,SIGNAL(signalValueChanged(int)),this,SIGNAL(qualitySliderChanged(int)));
    connect(ui->enableVideo,SIGNAL(toggled(bool)),this,SLOT(onChannelEnabled(bool)));
    connect(ui->enableSmartMotion,SIGNAL(toggled(bool)),this,SLOT(onSmartMotionToggled(bool)));
    connect(ui->cbRender,SIGNAL(toggled(bool)),this,SIGNAL(signalRenderToggled(bool)));
    connect(ui->gboxBufferFullness, SIGNAL(toggled(bool)), this, SIGNAL(signalBFToggled(bool)));
    connect(ui->editBFDuration, SIGNAL(returnPressed()), this, SLOT(onBufferDurationChanged()));

    // overlay image
    connect(ui->buttonImageApply,SIGNAL(clicked()),this,SLOT(onButtonApplyOverlayImageClicked()));
    connect(ui->gboxOverlayImage, SIGNAL(toggled(bool)), this, SLOT(onOverlayImageToggled(bool)));
    connect(ui->editImageWidth, SIGNAL(returnPressed()), ui->buttonImageApply, SIGNAL(clicked()));
    connect(ui->editImageHeight, SIGNAL(returnPressed()), ui->buttonImageApply, SIGNAL(clicked()));
    connect(ui->sbImageXOffset, SIGNAL(editingFinished()), this, SLOT(onImageXOffsetEditingFinished()));
    connect(ui->sbImageYOffset, SIGNAL(editingFinished()), this, SLOT(onImageYOffsetEditingFinished()));
    // Setup bitrate slider widget
    ui->sbAlpha->setRange(0, DEFAULT_IMAGE_ALPHA);
    ui->sbAlpha->setSingleStep(1);
    connect(ui->sbAlpha, SIGNAL(editingFinished()), this, SLOT(onAlphaEditingFinished()));
    connect(ui->buttonAlphaFileClear, SIGNAL(clicked()), this, SLOT(onButtonAlphaFileClearClicked()));
    ui->buttonAlphaFileClear->setToolTip(tr("Clear"));

    //connect(ui->sbAlpha,SIGNAL(valueChanged(int)),this,SLOT(onOverlayAlphaValueChanged(int)));

    // overlay text
    connect(ui->buttonTextApply,SIGNAL(clicked()),this,SLOT(onButtonApplyOverlayTextClicked()));
    connect(ui->gboxOverlayText, SIGNAL(toggled(bool)), this, SLOT(onOverlayTextToggled(bool)));
    connect(ui->cbEnableTime, SIGNAL(toggled(bool)), this, SIGNAL(signalOverlayTimeToggled(bool)));
    connect(ui->cbLines, SIGNAL(currentIndexChanged(int)), this, SLOT(onOverlayTextLineChanged(int)));
    connect(ui->buttonLoadFont, SIGNAL(clicked()), this, SLOT(onButtonLoadFontClicked()));
    connect(ui->cbLineEnable, SIGNAL(toggled(bool)), this, SLOT(onOverlayLineToggled(bool)));
    connect(ui->editText, SIGNAL(returnPressed()), ui->buttonTextApply, SIGNAL(clicked()));
    connect(ui->sbTextXOffset, SIGNAL(editingFinished()), this, SLOT(onTextXOffsetEditingFinished()));
    connect(ui->sbTextYOffset, SIGNAL(editingFinished()), this, SLOT(onTextYOffsetEditingFinished()));
    connect(ui->editFontSize, SIGNAL(returnPressed()), ui->buttonLoadFont, SIGNAL(clicked()));

    // Resolution controls
    connect(ui->sbWidth, SIGNAL(editingFinished()), this, SLOT(onResolutionWidthEditingFinished()));
    connect(ui->sbHeight, SIGNAL(editingFinished()), this, SLOT(onResolutionHeightEditingFinished()));

    // privacy mask
    connect(ui->buttonPrivMaskApply, SIGNAL(clicked()), this, SLOT(onButtonApplyPrivacyMaskClicked()));

    ui->editText->setMaxLength(24);

    // Remove quality tab
    ui->tabWidget->removeTab(ui->tabWidget->indexOf(ui->tabQuality));

    ui->enableSmartMotion->setVisible(false);

    ui->cbPanelMode->installEventFilter(this);
    ui->formatCombo->installEventFilter(this);
    ui->levelCombo->installEventFilter(this);
    ui->editImagePath->installEventFilter(this);
    ui->editAlphaFilePath->installEventFilter(this);
    ui->editFontPath->installEventFilter(this);
    ui->gboxOverlayImage->installEventFilter(this);
    ui->gboxOverlayText->installEventFilter(this);
    ui->gboxBufferFullness->installEventFilter(this);
    installEventFilter(this);

    // Setup bitrate slider widget
    ui->bitrateSliderWidget->setRange(100, 8000); // in kbps. 100 kbps to 8 Mbps (8000 kbps)
    ui->bitrateSliderWidget->setLabelWidth(65);
    ui->bitrateSliderWidget->setPageStep(1000); // to convert from kbps to bps
    ui->bitrateSliderWidget->setStep(1000);

    // Setup framerate slider widget
    ui->framerateSliderWidget->setRange(1, 60);
    ui->framerateSliderWidget->setPageStep(1);
    ui->framerateSliderWidget->setLabelWidth(20);

    // Setup gop slider widget
    ui->gopSliderWidget->setRange(0, 0xff);
    ui->gopSliderWidget->setPageStep(1);
    ui->gopSliderWidget->setPageStep(1);
    ui->gopSliderWidget->setLabelWidth(25);

    // Setup quality slider widget
    ui->qualitySliderWidget->setRange(0, 10000);
    ui->qualitySliderWidget->setLabelWidth(40);
    ui->qualitySliderWidget->setPageStep(1000);

    // Setup resolution spinboxes
    ui->sbWidth->setMinimum(0);
    ui->sbWidth->setMaximum(9999);
    ui->sbHeight->setMinimum(0);
    ui->sbHeight->setMaximum(9999);

    ui->iFrameButton->setToolTip("Force an iFrame");

    //setAttribute(Qt::WA_PaintOnScreen);
}

VideoSettings::~VideoSettings()
{
    delete ui;
}

void VideoSettings::showEvent(QShowEvent *)
{
    emit(signalHighlightCurrentPanel(true));
}

void VideoSettings::hideEvent(QHideEvent *) {
    emit(signalHighlightCurrentPanel(false));
}

bool mousePressed = false;
QPoint mousePosition;
void VideoSettings::mousePressEvent(QMouseEvent *event) {
    if ( event->button() == Qt::LeftButton ) {
        mousePressed = true;
        mousePosition = event->pos();
    }
}

void VideoSettings::mouseMoveEvent(QMouseEvent *event) {
    if (mousePressed) {
        QPoint toPoint = mapToParent( event->pos() - mousePosition );
        if (toPoint.x() < 0) {
            toPoint.setX(0);
        }
        if (toPoint.y() < 0) {
            toPoint.setY(0);
        }
        if ((toPoint.x()+this->width()) > this->parentWidget()->width()) {
            toPoint.setX(this->parentWidget()->width()-this->width());
        }
        if ((toPoint.y()+this->height()) > this->parentWidget()->height()) {
            toPoint.setY(this->parentWidget()->height()-this->height());
        }
        move( toPoint );
        if (!moved) {
            moved = true;
        }
    }
}

void VideoSettings::mouseReleaseEvent(QMouseEvent *event) {
    if ( event->button() == Qt::LeftButton ) {
        mousePressed = false;
        mousePosition = QPoint();
    }
}

bool VideoSettings::eventFilter(QObject *obj, QEvent *event)
{
    if(event->type() == QEvent::Wheel) {
        return true;
    }

    /*if (obj == this && event->type() == QEvent::MouseButtonPress) {
        return true;
    }*/

    if (event->type() == QEvent::MouseButtonDblClick) {
        return true;
    }

    if ((obj == ui->gboxOverlayImage || obj == ui->gboxOverlayText || obj == ui->gboxBufferFullness) && (event->type() == QEvent::MouseButtonPress || event->type() == QEvent::MouseMove || event->type() == QEvent::MouseButtonRelease)) {
        event->ignore();
        return false;
    }

    if (obj == ui->editImagePath && event->type() == QEvent::MouseButtonPress && ui->gboxOverlayImage->isChecked()) {
        QString dirPath = ui->editImagePath->text().length() == 0 ? (QCoreApplication::applicationDirPath() + "/" + DEFAULT_LOGO_PATH) : QFileInfo(ui->editImagePath->text()).absolutePath();
        QString filename = QFileDialog::getOpenFileName(0, "Select YUV image", dirPath, "Images (*.yuv *.YUV)");
        if (filename != NULL) {
            ui->editImagePath->setText(filename);
        }
        return true;
    }

    if (obj == ui->editAlphaFilePath && event->type() == QEvent::MouseButtonPress && ui->gboxOverlayImage->isChecked()) {
        QString dirPath = ui->editAlphaFilePath->text().length() == 0 ? (QCoreApplication::applicationDirPath() + "/" + DEFAULT_LOGO_PATH) : QFileInfo(ui->editAlphaFilePath->text()).absolutePath();
        QString filename = QFileDialog::getOpenFileName(0, "Select alpha YUV file", dirPath, "Images (*.yuv *.YUV)");
        if (filename != NULL) {
            ui->editAlphaFilePath->setText(filename);
        }
        return true;
    }

    if (obj == ui->editFontPath && event->type() == QEvent::MouseButtonPress && ui->gboxOverlayText->isChecked()) {
        QString dirPath = ui->editFontPath->text().length() == 0 ? (QCoreApplication::applicationDirPath() + "/" + DEFAULT_FONT_PATH) : QFileInfo(ui->editFontPath->text()).absolutePath();
        QString filename = QFileDialog::getOpenFileName(0, "Select font file", dirPath, "Fonts (*.bin)");
        if (filename != NULL) {
            ui->editFontPath->setText(filename);
        }
        return true;
    }

    return QWidget::eventFilter(obj, event);
}

void VideoSettings::setComposite(bool composite, int numpanels) {
    ui->cbPanel->setVisible(composite);
    ui->cbPanelMode->setVisible(composite);

    if (composite) {
        disconnect(ui->cbPanel,SIGNAL(currentIndexChanged(int)),this,SLOT(onPanelChanged(int)));
        ui->cbPanel->clear();
        for (int panel = 0; panel < numpanels; panel++) {
            ui->cbPanel->addItem(QString("Panel ")+QString::number(panel+1));
        }
        connect(ui->cbPanel,SIGNAL(currentIndexChanged(int)),this,SLOT(onPanelChanged(int)));
    }
}

void VideoSettings::setCurrentPanel(int panel) {
    disconnect(ui->cbPanel,SIGNAL(currentIndexChanged(int)),this,SLOT(onPanelChanged(int)));
    ui->cbPanel->setCurrentIndex(panel);
    connect(ui->cbPanel,SIGNAL(currentIndexChanged(int)),this,SLOT(onPanelChanged(int)));
}

void VideoSettings::setCurrentPanelMode(int mode) {
    this->populatePanelModes(mode);
}

void VideoSettings::setStopVideoDisplay(bool)
{
    ui->enableVideo->setChecked(false);
}

void VideoSettings::onChannelEnabled(bool isChecked) {
    enableControls(isChecked);
    ui->enableVideo->setToolTip(isChecked?"Stop video":"Start video");
    emit(signalChannelEnabled(isChecked));
}

void VideoSettings::onSmartMotionToggled(bool isChecked)
{
#ifdef WITH_ENGINE
    if(isChecked)
    {
        engine_stop_smartmotion();
        engine_start_smartmotion(channel, chWidth, chHeight);
    }
    else
    {
        engine_stop_smartmotion();
    }
#endif
}

// Slots
void VideoSettings::onPanelChanged(int panel) {
    qDebug() << "Panel index changed to" << panel;
    emit(signalOnPanelChanged(panel));
}

void VideoSettings::onPanelModeChanged(int mode) {
    if (ui->cbPanelMode->itemText(mode).compare(StringUtils::shared()->panelModeToString(PMODE_OFF)) == 0) {
        mode = PMODE_OFF;
    } else if (ui->cbPanelMode->itemText(mode).compare(StringUtils::shared()->panelModeToString(PMODE_ON)) == 0) {
        mode = PMODE_ON;
    } else if (ui->cbPanelMode->itemText(mode).compare(StringUtils::shared()->panelModeToString(PMODE_SELECT)) == 0) {
        mode = PMODE_SELECT;
    } else if (ui->cbPanelMode->itemText(mode).compare(StringUtils::shared()->panelModeToString(PMODE_UNSELECT)) == 0) {
        mode = PMODE_UNSELECT;
    }

    qDebug() << "Panel mode changed to:" << StringUtils::shared()->panelModeToString((panel_mode_t)mode);
    emit(signalOnPanelModeChanged(ui->cbPanel->currentIndex(), mode));
}

void VideoSettings::onFormatChanged(int format) {
    if (CameraConfig::shared()->getCameraMode() == IPCAM) {
        switch (format) {
            case 0:
                format = VID_FORMAT_NV12_RAW;
                break;
            case 1:
                format = VID_FORMAT_GREY_RAW;
                break;
        }
    } else {
        switch (format) {
            case 0:
                format = VID_FORMAT_H264_RAW;
                break;
            case 1:
                format = VID_FORMAT_MJPEG_RAW;
                break;
            case 2:
                format = VID_FORMAT_YUY2_RAW;
                break;
            case 3:
                format = VID_FORMAT_NV12_RAW;
                break;
        }
    }

    emit(signalFormatChanged(format));
}

void VideoSettings::onButtonApplyResolutionClicked() {
    ui->enableSmartMotion->setChecked(false);
    emit signalResolutionChanged(ui->sbWidth->value(), ui->sbHeight->value());
}

void VideoSettings::onButtonApplyOverlayTextClicked() {
    QString text = ui->editText->text();
    int xOff = ui->sbTextXOffset->text().toInt();
    int yOff = ui->sbTextYOffset->text().toInt();
    emit(signalOverlayTextChanged(ui->cbLines->currentIndex(), text, xOff, yOff));
}

void VideoSettings::onButtonApplyPrivacyMaskClicked() {
    PrivacyMask *mask = OverlayManager::shared()->getPrivacyMask(channel, ui->cbPrivacyMasks->currentIndex());
    mask->xOffset = ui->sbPrivMaskXOff->text().toInt();
    mask->yOffset = ui->sbPrivMaskYOff->text().toInt();
    mask->width = ui->sbPrivMaskWidth->text().toInt();
    mask->height = ui->sbPrivMaskHeight->text().toInt();

    emit signalPrivacyMaskChanged(ui->cbPrivacyMasks->currentIndex());
}

void VideoSettings::onBufferDurationChanged() {
    int duration = ui->editBFDuration->text().toInt();
    if (duration > 0) {
        emit signalBufferDurationChanged(duration);
    } else {
        AlertHandler::displayError(tr("Invalid duration"));
    }
}

void VideoSettings::onResolutionWidthEditingFinished() {
    if (ui->sbWidth->hasFocus()) {
        onButtonApplyResolutionClicked();
    }
}

void VideoSettings::onResolutionHeightEditingFinished() {
    if (ui->sbHeight->hasFocus()) {
        onButtonApplyResolutionClicked();
    }
}

void VideoSettings::onTextXOffsetEditingFinished() {
    if (ui->sbTextXOffset->hasFocus()) {
        onButtonApplyOverlayTextClicked();
    }
}
void VideoSettings::onTextYOffsetEditingFinished() {
    if (ui->sbTextYOffset->hasFocus()) {
        onButtonApplyOverlayTextClicked();
    }
}

void VideoSettings::onButtonAlphaFileClearClicked() {
    ui->editAlphaFilePath->clear();
}

void VideoSettings::onAlphaEditingFinished() {
    if (ui->sbAlpha->hasFocus()) {
        onButtonApplyOverlayImageClicked();
    }
}

void VideoSettings::onOverlayTextToggled(bool enabled) {
    //ui->gboxOverlayText->setTitle(enabled?"On":"Off");

    if (enabled) {
        QFileInfo fileInfo(ui->editFontPath->text());
        if (!fileInfo.exists()) {
            AlertHandler::displayError(tr("Font \"%1\" does not exist").arg(ui->editFontPath->text()));
        }
    }

    emit signalOverlayTextToggled(enabled);
}

void VideoSettings::onOverlayLineToggled(bool enabled) {
    ui->editText->setEnabled(enabled);
    ui->sbTextXOffset->setEnabled(enabled);
    ui->sbTextYOffset->setEnabled(enabled);
    emit signalOverlayTextToggled(enabled,ui->cbLines->currentIndex());
}

void VideoSettings::onOverlayTextLineChanged(int line) {
    OverlayText *text = OverlayManager::shared()->getOverlayText(channel, line);
    ui->cbLineEnable->setChecked(text->enabled);
    ui->editText->setText(text->text);
    ui->sbTextXOffset->setValue(text->xOffset);
    ui->sbTextYOffset->setValue(text->yOffset);
}

void VideoSettings::onButtonApplyOverlayImageClicked() {
    QFileInfo fileInfo(ui->editImagePath->text());
    if (fileInfo.exists()) {
        int imageWidth = ui->editImageWidth->text().toInt();
        int imageHeight = ui->editImageHeight->text().toInt();
        int xOff = ui->sbImageXOffset->text().toInt();
        int yOff = ui->sbImageYOffset->text().toInt();
        int alpha = ui->sbAlpha->text().toInt();
        QString alphafile = ui->editAlphaFilePath->text();
        //qDebug() << imageWidth << " " << imageHeight << " " << xOff << " " << yOff << alpha;
        emit(signalOverlayImagePathChanged(fileInfo.absoluteFilePath(), imageWidth, imageHeight, xOff, yOff, alpha, alphafile));
    } else {
        AlertHandler::displayError(ui->editImagePath->text()+" does not exist");
    }
}

void VideoSettings::onOverlayAlphaValueChanged(int currentAlpha){
    int index = 0;//Currently logo index is 0 as only one logo per channel
    emit(signalOverlayImageAlphaChanged(index,currentAlpha));
}


void VideoSettings::onImageXOffsetEditingFinished() {
    if (ui->sbImageXOffset->hasFocus()) {
        onButtonApplyOverlayImageClicked();
    }
}

void VideoSettings::onImageYOffsetEditingFinished() {
    if (ui->sbImageYOffset->hasFocus()) {
        onButtonApplyOverlayImageClicked();
    }
}

void VideoSettings::onButtonLoadFontClicked() {
    QFileInfo fileInfo(ui->editFontPath->text());
    if (fileInfo.exists()) {
        int fontSize = ui->editFontSize->text().toInt();
        if (fontSize == 0) {
            AlertHandler::displayError("Invalid font size");
            return;
        }
        emit(signalOverlayFontChanged(fileInfo.absoluteFilePath(), fontSize));
    } else {
        AlertHandler::displayError(ui->editFontPath->text()+" does not exist");
    }
}

void VideoSettings::onOverlayImageToggled(bool enabled) {
    //ui->gboxOverlayImage->setTitle(enabled?"On":"Off");

    if (enabled) {
        QFileInfo fileInfo(ui->editImagePath->text());
        if (!fileInfo.exists()) {
            AlertHandler::displayError(tr("Logo \"%1\" does not exist").arg(ui->editImagePath->text()));
            return;
        }
    }

    emit(signalOverlayImageToggled(enabled));
}

void VideoSettings::setBitRateSilder(int val, bool enabled)
{
    if(enabled) {
        ui->bitrateSliderWidget->setValue(val);
    } else {
        ui->tabWidget->removeTab(ui->tabWidget->indexOf(ui->tabBr));
    }
}

void VideoSettings::setFrameRateSilder(int val, bool enabled)
{
    if(enabled) {
        ui->framerateSliderWidget->setValue(val);
    } else {
        ui->tabWidget->removeTab(ui->tabWidget->indexOf(ui->tabFr));
    }
}

void VideoSettings::setGopSilder(int val, bool enabled)
{
    if(enabled) {
        ui->gopSliderWidget->setValue(val);
    } else {
        ui->tabWidget->removeTab(ui->tabWidget->indexOf(ui->tabGop));

    }
}

void VideoSettings::setResolution(int w, int h, bool enabled)
{
    if(enabled) {
        ui->sbWidth->setValue(w);
        ui->sbHeight->setValue(h);
    } else {
        ui->tabWidget->removeTab(ui->tabWidget->indexOf(ui->tabRes));
    }
}

void VideoSettings::setProfile(int prof,bool enabled)
{
    if(enabled)
    {
        switch(prof)
        {
            case PROFILE_BASELINE:
                ui->profileCombo->setCurrentIndex(PROFILE_BASELINE);
                break;
            case PROFILE_MAIN:
                ui->profileCombo->setCurrentIndex(PROFILE_MAIN);
                break;
            case PROFILE_HIGH:
                ui->profileCombo->setCurrentIndex(PROFILE_HIGH);
                break;
            default:
                break;
        }
    }
    else {
        ui->tabWidget->removeTab(ui->tabWidget->indexOf(ui->tabProfile));
        this->removeTab(ui->tabProfile);
    }

}

void VideoSettings::setLevel(int lev,bool enabled)
{
    if(enabled)
    {
        switch(lev)
        {
            case L21:
                ui->levelCombo->setCurrentIndex(L21);
                break;
            case L22:
                ui->levelCombo->setCurrentIndex(L22);
                break;
            case L30:
                ui->levelCombo->setCurrentIndex(L30);
                break;
            case L31:
                ui->levelCombo->setCurrentIndex(L31);
                break;
            default:
                break;
        }
    }
    else
    {
        ui->tabWidget->removeTab(ui->tabWidget->indexOf(ui->tablevel));
    }
}

void VideoSettings::setQuality(int quality, bool enabled)
{
    if(enabled) {
        ui->qualitySliderWidget->setValue(quality);
        ui->qualitySliderWidget->setLabelValue(QString::number(quality));
    } else {
        this->removeTab(ui->tabQuality);
    }
}

void VideoSettings::setVideoFormat(video_format_t format) {
    qDebug() << "Video format for Channel " << StringUtils::shared()->channelToString((video_channel_t)channel) << ": " << StringUtils::shared()->videoFormatToString(format);

    QWidget *prevTab = ui->tabWidget->currentWidget();

    setTabsForVideoFormat(format);

    // Show the video format tab only if
    // 1. Skype (mode) preview channel
    // 2. Ipcam (mode) raw channel
    if (((CameraConfig::shared()->getCameraMode()==SKYPE)) ||
        ((CameraConfig::shared()->getCameraMode()==IPCAM) && (format == VID_FORMAT_YUY2_RAW || format == VID_FORMAT_NV12_RAW || format == VID_FORMAT_GREY_RAW))) {

        ui->tabWidget->addTab(ui->tabFormat, "Format");

        disconnect(ui->formatCombo,SIGNAL(currentIndexChanged(int)),this,SLOT(onFormatChanged(int)));
        ui->formatCombo->clear();

        if (CameraConfig::shared()->getCameraMode()==SKYPE) {
            // Remove resolution tab
            //this->removeTab(ui->tabRes);

            ui->formatCombo->addItem(StringUtils::shared()->videoFormatToString(VID_FORMAT_H264_RAW));
            ui->formatCombo->addItem(StringUtils::shared()->videoFormatToString(VID_FORMAT_MJPEG_RAW));
            ui->formatCombo->addItem(StringUtils::shared()->videoFormatToString(VID_FORMAT_YUY2_RAW));
            ui->formatCombo->addItem(StringUtils::shared()->videoFormatToString(VID_FORMAT_NV12_RAW));
            ui->formatCombo->addItem(StringUtils::shared()->videoFormatToString(VID_FORMAT_H264_TS));

            switch(format) {
                case VID_FORMAT_H264_RAW:
                    ui->formatCombo->setCurrentIndex(0);
                    break;
                case VID_FORMAT_MJPEG_RAW:
                    ui->formatCombo->setCurrentIndex(1);
                    break;
                case VID_FORMAT_YUY2_RAW:
                    ui->formatCombo->setCurrentIndex(2);
                    break;
                case VID_FORMAT_NV12_RAW:
                    ui->formatCombo->setCurrentIndex(3);
                    break;
                default:
                    qDebug() << "Error: Is " << format << " an incorrect format for SKYPE mode preview channel?";
                    break;
            }
        } else {
            ui->formatCombo->addItem(StringUtils::shared()->videoFormatToString(VID_FORMAT_NV12_RAW));
            ui->formatCombo->addItem(StringUtils::shared()->videoFormatToString(VID_FORMAT_GREY_RAW));

            switch(format) {
                case VID_FORMAT_NV12_RAW:
                    ui->formatCombo->setCurrentIndex(0);
                    break;
                case VID_FORMAT_GREY_RAW:
                    ui->formatCombo->setCurrentIndex(1);
                    break;
                default:
                    qDebug() << "Error: Is " << format << " an incorrect format for IPCAM mode raw channel?";
                    break;
            }
        }
        connect(ui->formatCombo,SIGNAL(currentIndexChanged(int)),this,SLOT(onFormatChanged(int)));
    }

    if (ui->tabWidget->indexOf(prevTab) != -1) {
        ui->tabWidget->setCurrentIndex(ui->tabWidget->indexOf(prevTab));
    }
}

void VideoSettings::setTabsForVideoFormat(video_format_t format) {

    resetTabs();

    switch (format) {
        case VID_FORMAT_MJPEG_RAW:
            ui->iFrameButton->setVisible(false);
            this->removeTab(ui->tabBr);
            this->removeTab(ui->tabGop);
            this->removeTab(ui->tabProfile);
            //this->removeTab(ui->tabFormat);
            break;
        case VID_FORMAT_YUY2_RAW:
        case VID_FORMAT_NV12_RAW:
        case VID_FORMAT_GREY_RAW:
            ui->iFrameButton->setVisible(false);

#ifdef __i386__
            ui->enableSmartMotion->setVisible(true);
#endif

            this->removeTab(ui->tabQuality);
            this->removeTab(ui->tabBr);
            this->removeTab(ui->tabGop);
            this->removeTab(ui->tabProfile);
            break;
        default:
            this->removeTab(ui->tabQuality);
            //this->removeTab(ui->tabFormat);
            break;
    }

    //ui->tabWidget->adjustSize();
    //this->adjustSize();
}

void VideoSettings::enableControls(bool enable) {
    enable = true;
    ui->iFrameButton->setEnabled(enable);
    videoRunning = enable;

    ui->cbPanel->setEnabled(enable);
    ui->cbPanelMode->setEnabled(enable);

    ui->enableSmartMotion->setEnabled(enable);

    ui->cbRender->setEnabled(enable);

    ui->tabWidget->setEnabled(enable);
}

void VideoSettings::setVideoEnabled(bool enabled) {
    disconnect(ui->enableVideo,SIGNAL(toggled(bool)),this,SLOT(onChannelEnabled(bool)));
    ui->enableVideo->setChecked(enabled);
    enableControls(enabled);
    connect(ui->enableVideo,SIGNAL(toggled(bool)),this,SLOT(onChannelEnabled(bool)));

    ui->enableVideo->setToolTip(enabled?"Stop video":"Start video");
}

void VideoSettings::setRenderingEnabled(bool enabled) {
    disconnect(ui->cbRender,SIGNAL(toggled(bool)),this,SIGNAL(signalRenderToggled(bool)));
    ui->cbRender->setChecked(enabled);
    connect(ui->cbRender,SIGNAL(toggled(bool)),this,SIGNAL(signalRenderToggled(bool)));

    ui->cbRender->setToolTip(enabled?"Stop decoding & rendering":"Start decoding & rendering");
}

void VideoSettings::setBufferFullness(bool enabled, int duration) {
    ui->gboxBufferFullness->blockSignals(true);
    ui->editBFDuration->blockSignals(true);
    ui->gboxBufferFullness->setChecked(enabled);
    ui->editBFDuration->setText(QString::number(duration));
    ui->gboxBufferFullness->blockSignals(false);
    ui->editBFDuration->blockSignals(false);
}

void VideoSettings::setOverlayEnabled(bool enabled) {
    this->overlayEnabled = enabled;
    if (!enabled) {
        this->removeTab(ui->tabPrivacyMask);
        this->removeTab(ui->tabOverlayImage);
        this->removeTab(ui->tabOverlayText);
    } else {
        //ui->tabWidget->addTab(ui->tabOverlayImage, "Overlay Image");
        //ui->tabWidget->addTab(ui->tabOverlayText, "Overlay Text");
    }
}

void VideoSettings::setOverlayTextEnabled(bool enabled) {
    disconnect(ui->gboxOverlayText, SIGNAL(toggled(bool)), this, SLOT(onOverlayTextToggled(bool)));
    ui->gboxOverlayText->setChecked(enabled);
    //ui->gboxOverlayText->setTitle(enabled?"On":"Off");
    connect(ui->gboxOverlayText, SIGNAL(toggled(bool)), this, SLOT(onOverlayTextToggled(bool)));
}

void VideoSettings::setOverlayImageEnabled(bool enabled) {
    disconnect(ui->gboxOverlayImage, SIGNAL(toggled(bool)), this, SLOT(onOverlayImageToggled(bool)));
    ui->gboxOverlayImage->setChecked(enabled);
    //ui->gboxOverlayImage->setTitle(enabled?"On":"Off");
    connect(ui->gboxOverlayImage, SIGNAL(toggled(bool)), this, SLOT(onOverlayImageToggled(bool)));
}

void VideoSettings::setOverlayTimeEnabled(bool enabled) {
    disconnect(ui->cbEnableTime, SIGNAL(toggled(bool)), this, SIGNAL(signalOverlayTimeToggled(bool)));
    ui->cbEnableTime->setChecked(enabled);
    connect(ui->cbEnableTime, SIGNAL(toggled(bool)), this, SIGNAL(signalOverlayTimeToggled(bool)));
}

void VideoSettings::updateOverlayTextConfig(int channelWidth, int channelHeight) {
    if (ui->cbLines->count() != OverlayManager::shared()->getNumOverlayText()) {
        QStringList lines;
        for (int i = 0; i < OverlayManager::shared()->getNumOverlayText(); i++) {
            lines << QString("Line%1").arg(i+1);
        }
        ui->cbLines->clear();
        ui->cbLines->addItems(lines);
    }

    OverlayText *overlayText = OverlayManager::shared()->getOverlayText(channel, ui->cbLines->currentIndex());
    ui->editText->setText(overlayText->text);
    ui->sbTextXOffset->setMinimum(0);
    ui->sbTextXOffset->setMaximum(channelWidth);
    ui->sbTextYOffset->setMinimum(0);
    ui->sbTextYOffset->setMaximum(channelHeight);
    ui->sbTextXOffset->setValue(overlayText->xOffset);
    ui->sbTextYOffset->setValue(overlayText->yOffset);

    ui->cbLineEnable->setChecked(overlayText->enabled);
    if (ui->gboxOverlayText->isChecked()) {
        ui->editText->setEnabled(ui->cbLineEnable->isChecked());
        ui->sbTextYOffset->setEnabled(ui->cbLineEnable->isChecked());
        ui->sbTextXOffset->setEnabled(ui->cbLineEnable->isChecked());
    }

    //if (OverlayManager::shared()->isFontLoaded(channel)) {
    OverlayFont *font = OverlayManager::shared()->getOverlayFont(channel);
    ui->editFontPath->setText(font->fontPath);
    ui->editFontSize->setText(QString::number(font->fontSize));
    //}
}

void VideoSettings::updatePrivacyMaskConfig(int channelWidth, int channelHeight) {
    if (ui->cbPrivacyMasks->count() != OverlayManager::shared()->getNumPrivMasks()) {
        QStringList lines;
        for (int i = 0; i < OverlayManager::shared()->getNumPrivMasks(); i++) {
            lines << QString("Mask %1").arg(i+1);
        }
        ui->cbPrivacyMasks->clear();
        ui->cbPrivacyMasks->addItems(lines);
    }

    PrivacyMask *mask = OverlayManager::shared()->getPrivacyMask(channel, ui->cbPrivacyMasks->currentIndex());
    ui->gboxPrivMask->setChecked(mask->enabled);
    ui->sbPrivMaskXOff->setMinimum(0);
    ui->sbPrivMaskXOff->setMaximum(channelWidth);
    ui->sbPrivMaskYOff->setMinimum(0);
    ui->sbPrivMaskYOff->setMaximum(channelHeight);
    ui->sbPrivMaskWidth->setMinimum(0);
    ui->sbPrivMaskWidth->setMaximum(channelWidth);
    ui->sbPrivMaskHeight->setMinimum(0);
    ui->sbPrivMaskHeight->setMaximum(channelHeight);

    ui->sbPrivMaskXOff->setValue(mask->xOffset);
    ui->sbPrivMaskYOff->setValue(mask->yOffset);
    ui->sbPrivMaskWidth->setValue(mask->width);
    ui->sbPrivMaskHeight->setValue(mask->height);
}

void VideoSettings::updateOverlayImageConfig(int channelWidth, int channelHeight) {
    Q_UNUSED(channelWidth)
    Q_UNUSED(channelHeight)
    OverlayImage *overlayImage = OverlayManager::shared()->getOverlayImage(channel);
    // add overlay tabs
    ui->editImagePath->setText(overlayImage->image);
    ui->editImageWidth->setText(QString::number(overlayImage->width));
    ui->editImageHeight->setText(QString::number(overlayImage->height));
    ui->sbImageXOffset->setMinimum(0);
    ui->sbImageXOffset->setMaximum(99999);
    ui->sbImageYOffset->setMinimum(0);
    ui->sbImageYOffset->setMaximum(99999);
    ui->sbImageXOffset->setValue(overlayImage->xOffset);
    ui->sbImageYOffset->setValue(overlayImage->yOffset);
    ui->sbAlpha->setValue(overlayImage->alpha);
    ui->editAlphaFilePath->setText(overlayImage->alphaFile);
}

void VideoSettings::setChType(int val) {
    ui->formatCombo->setCurrentIndex(val);
}

void VideoSettings::resetTabs() {
    ui->tabWidget->clear();

    if (this->overlayEnabled) {
        // add overlay tabs
        //ui->tabWidget->addTab(ui->tabPrivacyMask, tr("Privacy Mask"));
        ui->tabWidget->addTab(ui->tabOverlayText, tr("Overlay Text"));
        ui->tabWidget->addTab(ui->tabOverlayImage, tr("Overlay Image"));
    }

    ui->tabWidget->addTab(ui->tabBr,"Bitrate (kbps)");
    ui->tabWidget->addTab(ui->tabFr,"Framerate");
    ui->tabWidget->addTab(ui->tabGop, "GOP");
    ui->tabWidget->addTab(ui->tabRes, "Resolution");
    ui->tabWidget->addTab(ui->tabProfile, "Profile");
    ui->tabWidget->addTab(ui->tabQuality, "Compression Quality");

    this->adjustSize();
}

void VideoSettings::populatePanelModes(int mode) {
    disconnect(ui->cbPanelMode,SIGNAL(currentIndexChanged(int)),this,SLOT(onPanelModeChanged(int)));

    ui->cbPanelMode->clear();
    switch (mode) {
        case PMODE_OFF:
            ui->cbPanelMode->addItem(StringUtils::shared()->panelModeToString(PMODE_OFF));
            ui->cbPanelMode->addItem(StringUtils::shared()->panelModeToString(PMODE_ON));
            break;
        case PMODE_ON:
            ui->cbPanelMode->addItem(StringUtils::shared()->panelModeToString(PMODE_OFF));
            ui->cbPanelMode->addItem(StringUtils::shared()->panelModeToString(PMODE_ON));
            ui->cbPanelMode->addItem(StringUtils::shared()->panelModeToString(PMODE_SELECT));
            ui->cbPanelMode->addItem(StringUtils::shared()->panelModeToString(PMODE_UNSELECT));
            break;
        case PMODE_SELECT:
            ui->cbPanelMode->addItem(StringUtils::shared()->panelModeToString(PMODE_SELECT));
            ui->cbPanelMode->addItem(StringUtils::shared()->panelModeToString(PMODE_UNSELECT));
            mode = 0;
            break;
        case PMODE_UNSELECT:
            ui->cbPanelMode->addItem(StringUtils::shared()->panelModeToString(PMODE_OFF));
            ui->cbPanelMode->addItem(StringUtils::shared()->panelModeToString(PMODE_ON));
            ui->cbPanelMode->addItem(StringUtils::shared()->panelModeToString(PMODE_SELECT));
            ui->cbPanelMode->addItem(StringUtils::shared()->panelModeToString(PMODE_UNSELECT));
            break;
    }

    ui->cbPanelMode->setCurrentIndex(mode);
    connect(ui->cbPanelMode,SIGNAL(currentIndexChanged(int)),this,SLOT(onPanelModeChanged(int)));
}

void VideoSettings::removeTab(QWidget *tab) {
    for (int index = 0; index < ui->tabWidget->count(); index++) {
        ui->tabWidget->setCurrentIndex(index);
        //qDebug() <<ui->tabWidget->currentWidget()->objectName();
        if (tab->objectName().compare(ui->tabWidget->currentWidget()->objectName()) == 0) {
            qDebug() << "Removing tab " << tab->objectName();
            ui->tabWidget->removeTab(index);
            ui->tabWidget->setCurrentIndex(0);
            return;
        }
    }
}
