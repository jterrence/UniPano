/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef CAMERACONFIG_H
#define CAMERACONFIG_H

#ifdef WITH_ENGINE
#include "interface.h"
#endif

/**
 * @brief ChannelConfig
 *
 * Singleton class that holds channel configuration
 *
 */
class CameraConfig
{
    static CameraConfig *s_instance;
    int mNumChannels;
    int mRawChannelID;
    int mNumH264Channels;

#ifdef WITH_ENGINE
    config_params_t **mConfigParams;
    camer_mode_t cameraMode;
#endif

    // private constructor
    CameraConfig():mNumChannels(0),mRawChannelID(0),mNumH264Channels(0) {}

public:
    static CameraConfig *shared() {
        if (!s_instance) {
            s_instance = new CameraConfig();
        }
        return s_instance;
    }

    ~CameraConfig() {
        if (mConfigParams) {
            delete mConfigParams;
        }
    }

    int getNumChannels() {
        return mNumChannels;
    }

    int getNumH264Channels() {
        return mNumH264Channels;
    }

    int setNumH264Channels() {
        return mNumH264Channels++;
    }

    void setNumChannels(int numChannels) {
        this->mNumChannels = numChannels;
    }

    int getRawChannelID() {
        return mRawChannelID;
    }

    void setRawChannelID(int id) {
        this->mRawChannelID = id;
    }

#ifdef WITH_ENGINE
    void setChannelConfigParams(config_params_t **configParams) {
        this->mConfigParams = configParams;
    }

    config_params_t* getChannelConfigParams(int channel) {
        return mConfigParams[channel];
    }

    void setCameraMode(camer_mode_t cameraMode) {
        this->cameraMode = cameraMode;
    }

    camer_mode_t getCameraMode() {
        return this->cameraMode;
    }

#endif
};

#endif // CAMERACONFIG_H
