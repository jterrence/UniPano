/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef STRINGUTILS_H
#define STRINGUTILS_H

#ifdef WITH_ENGINE
#include "interface.h"
#endif
#include <QString>

class StringUtils
{

    static StringUtils *s_instance;

    // private constructor
    StringUtils() {}

public:
    static StringUtils *shared() {
        if (!s_instance) {
            s_instance = new StringUtils();
        }
        return s_instance;
    }

    QString videoFormatToString(video_format_t format);
    QString channelToString(video_channel_t channel);
    QString dewarpModeToString(dewarp_mode_t mode);
    QString panelModeToString(panel_mode_t mode);
    QString videoProfileToString(video_profile_t profile);
};

#endif // STRINGUTILS_H
