/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include <QDesktopWidget>
#include <QGridLayout>
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "cameraconfig.h"
#include "alerthandler.h"
#include "log.h"
#include <QFileInfo>
#include "overlaymanager.h"
#include "eptz.h"
#include <QObjectList>
#include <channelstate.h>
#include "privacymasklayout.h"

// load font for each channel at startup
#define LOAD_FONTS

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

}

void MainWindow::init(int devId, bootProperties prop)
{
    ui->setupUi(this);

    bootProp = prop;
    statsEnabled = true;// prop.prop.render==0; //Disable stats Rendering by default
    dewarpSettings = NULL;
    pagingBar = NULL;
    generalSettings = NULL;
    rawChannel = NULL;
    labelMode = NULL;
    apiManager = NULL;
    cameraSettings = NULL;
    channelLayout = NULL;

    numPages = 0;
    numChannelsPerPage = bootProp.prop.numChannelsPerPage;
    numChannelsOnLastPage = 0;
    singleChannelLayoutSelected = false;

    QWidget *widget = this->centralWidget();
    widget->setParent(0);

    qDebug()<<"Screen Resolution:"<<  qApp->desktop()->screenGeometry().width()<<"x"<< qApp->desktop()->screenGeometry().height();
    this->showFullScreen();

    //setupBottomBar();

    // Setup paging bar
    pagingBar = new ChannelBar(this);
    pagingBar->hide();
    connect(pagingBar, SIGNAL(signalPageChanged(int)), this, SLOT(onPageChanged(int)));
    connect(pagingBar, SIGNAL(signalResized()), this, SLOT(onPagingBarResized()));

    toastMessage = new ToastMessage(this);
    toastMessage->hide();

    apiManager = new EngineApiManager(0, "mainwin");
    connect(apiManager, SIGNAL(signalEngineInitSuccess(int)), this, SLOT(onEngineInitSuccess(int)));
    connect(apiManager, SIGNAL(signalEngineInitFailed()), this, SLOT(onEngineInitFailed()));
    apiManager->startThread();
    apiManager->engineInit(devId);
}

MainWindow::~MainWindow() {
    cleanupLayout();
    delete generalSettings;
    delete audioRecord;
    delete videoRecord;
    delete ui;
#ifdef WITH_ENGINE
    //Stop all the channels
    if(bootProp.prop.video)
    {
        for(int i=0;i<CameraConfig::shared()->getNumChannels();i++) {
            engine_stopvideo(i);
        }
    }
    engine_deinit();
#endif
}

void MainWindow::keyPressEvent(QKeyEvent *event )
{
    event->ignore();
    channelLayout->propagateKeyPressEvent(event);
    if (event->isAccepted()) {
        return;
    }

    switch(event->key()) {
        case Qt::Key_Up:
        case Qt::Key_PageUp:
            showGeneralSettings(true);
            break;
        case Qt::Key_Down:
        case Qt::Key_PageDown:
            showGeneralSettings(false);
            break;
        case Qt::Key_F1:
            this->setF1KeyPressed();
            break;
        case Qt::Key_Escape:
            showGeneralSettings(false);
            break;
        default:
            break;
    }
}

void MainWindow::resizeEvent(QResizeEvent *) {
    if (pagingBar && pagingBar->isVisible()) {
        showPagingBar(true);
    }

    if (generalSettings && generalSettings->isVisible()) {
        showGeneralSettings(true);
    }

    if (labelMode) {
        labelMode->setGeometry(this->width()/2-labelMode->width()/2, 1, labelMode->width(), labelMode->height());
    }
}

void MainWindow::closeEvent(QCloseEvent *evt) {
    evt->setAccepted( !evt->spontaneous() );
    if (evt->spontaneous()) {
        this->onExitClicked();
    }
}

// Private functions

void MainWindow::cleanupLayout() {
    if (channelLayout != NULL) {
        channelLayout->stopAllVideo();
        /*const QObjectList childrenList = channelLayout->children();
        for (int index = 0; index < childrenList.count(); index++) {
            QObject *child = childrenList.at(index);
            qDebug() << child;
            if (qobject_cast<QWidget*>(child) != NULL) {
                delete child;
            }
        }*/
        delete channelLayout;
        channelLayout = NULL;
    }
}

int channelWithZclStretch = 0;
void MainWindow::layoutSetupPrivacyMask() {
    cleanupLayout();

    int screenWidth = qApp->desktop()->screenGeometry().width();
    int screenHeight = qApp->desktop()->screenGeometry().height();

    channelLayout = new ChannelLayout();
    channelLayout->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    channelLayout->setMaximumSize(screenWidth, screenHeight);
    channelLayout->setContentsMargins(0,0,0,0);
    setCentralWidget(channelLayout);
    channelLayout->lower();

    PrivacyMaskLayout *privacyMaskWidget = new PrivacyMaskLayout(channelLayout, channelWithZclStretch);
    channelLayout->setLayout(privacyMaskWidget->layout());
}

void MainWindow::layoutSetupEptz() {
    cleanupLayout();

    int screenWidth = qApp->desktop()->screenGeometry().width();
    int screenHeight = qApp->desktop()->screenGeometry().height();

    channelLayout = new ChannelLayout();
    channelLayout->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    channelLayout->setMaximumSize(screenWidth, screenHeight);
    channelLayout->setContentsMargins(0,0,0,0);
    setCentralWidget(channelLayout);
    channelLayout->lower();

    EPTZ *eptzWidget = new EPTZ(channelLayout);
    channelLayout->setLayout(eptzWidget->layout());

#if 0
    screenHeight -= generalSettings->height();

    int minHeight = screenHeight - generalSettings->height();// screenHeight/2 - 40;
    //int minWidth = screenWidth/2 - 10;
    //int maxWidth = minWidth;
    int maxHeight = minHeight;

    QGridLayout* gridLayout = new QGridLayout;
    gridLayout->setSizeConstraint (QLayout::SetMaximumSize);
    gridLayout->setSpacing(5);
    gridLayout->setContentsMargins(2, 30, 2, 2);

    DisplayChannel *eptzChannel = new DisplayChannel(channelLayout);
    eptzChannel->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    eptzChannel->setMinimumSize(screenWidth-320-30, minHeight);
    eptzChannel->setMaximumSize(screenWidth-320-30, maxHeight);
    gridLayout->addWidget(eptzChannel, 0, 0, 2, 1, Qt::AlignBottom);

    QGridLayout* gridControlsLayout = new QGridLayout;
    gridControlsLayout->setSizeConstraint (QLayout::SetMaximumSize);
    gridControlsLayout->setSpacing(5);
    gridControlsLayout->setContentsMargins(5, 5, 5, 5);

    QFrame *eptzControls = new QFrame(channelLayout);
    eptzControls->setObjectName("toolbar");
    eptzControls->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

    int eptzButtonSize = 50;
    QPushButton *zoomButton = new QPushButton(eptzControls);
    zoomButton->setText("+");
    zoomButton->setMinimumSize(eptzButtonSize, eptzButtonSize);
    zoomButton->setMaximumSize(eptzButtonSize, eptzButtonSize);
    zoomButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    gridControlsLayout->addWidget(zoomButton, 0, 0, 1, 1, Qt::AlignCenter);
    QWidget *dummy = new QWidget(eptzControls);
    dummy->setMinimumSize(10, eptzButtonSize);
    dummy->setMaximumSize(10, eptzButtonSize);
    gridControlsLayout->addWidget(dummy, 1, 0, 1, 1, Qt::AlignCenter);
    QPushButton *unzoomButton = new QPushButton(eptzControls);
    unzoomButton->setText("-");
    unzoomButton->setMinimumSize(eptzButtonSize, eptzButtonSize);
    unzoomButton->setMaximumSize(eptzButtonSize, eptzButtonSize);
    unzoomButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    gridControlsLayout->addWidget(unzoomButton, 0, 2, 1, 1, Qt::AlignCenter);
    QPushButton *upButton = new QPushButton(eptzControls);
    upButton->setText("^");
    upButton->setMinimumSize(eptzButtonSize, eptzButtonSize);
    upButton->setMaximumSize(eptzButtonSize, eptzButtonSize);
    upButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    gridControlsLayout->addWidget(upButton, 2, 1, 1, 1, Qt::AlignCenter);
    QPushButton *leftButton = new QPushButton(eptzControls);
    leftButton->setText("<");
    leftButton->setMinimumSize(eptzButtonSize, eptzButtonSize);
    leftButton->setMaximumSize(eptzButtonSize, eptzButtonSize);
    leftButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    gridControlsLayout->addWidget(leftButton, 3, 0, 1, 1, Qt::AlignCenter);
    QPushButton *rightButton = new QPushButton(eptzControls);
    rightButton->setText(">");
    rightButton->setMinimumSize(eptzButtonSize, eptzButtonSize);
    rightButton->setMaximumSize(eptzButtonSize, eptzButtonSize);
    rightButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    gridControlsLayout->addWidget(rightButton, 3, 2, 1, 1, Qt::AlignCenter);
    QPushButton *downButton = new QPushButton(eptzControls);
    downButton->setText("v");
    downButton->setMinimumSize(eptzButtonSize, eptzButtonSize);
    downButton->setMaximumSize(eptzButtonSize, eptzButtonSize);
    downButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    gridControlsLayout->addWidget(downButton, 4, 1, 1, 1, Qt::AlignCenter);

    eptzControls->setLayout(gridControlsLayout);

    gridLayout->addWidget(eptzControls, 0, 1, 1, 1, Qt::AlignCenter|Qt::AlignBottom);

    DisplayChannel *refchannel = new DisplayChannel(channelLayout);
    refchannel->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    refchannel->setMinimumSize(320, 240);
    refchannel->setMaximumSize(320, 240);
    gridLayout->addWidget(refchannel, 1, 1, 1, 1, Qt::AlignBottom|Qt::AlignRight);

    refchannel->setChannel(0);
    refchannel->setHandleMouseEvents(false);

    eptzChannel->setChannel(1);
    eptzChannel->setHandleMouseEvents(false);

    channelLayout->setLayout(gridLayout);
    //eptzControls->adjustSize();
#endif
}

void MainWindow::layoutSetup(int numChannels, int startingChannel)
{
    currentChLayout = numChannels;

    int screenWidth = qApp->desktop()->screenGeometry().width();
    int screenHeight = qApp->desktop()->screenGeometry().height();

    channelLayout = new ChannelLayout();
    channelLayout->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    channelLayout->setMaximumSize(screenWidth, screenHeight);
    channelLayout->setContentsMargins(0,0,0,0);
    setCentralWidget(channelLayout);
    channelLayout->lower();

    if (labelMode) {
        screenHeight -= generalSettings->height();
    }

    float minHeight = (screenHeight-10)/2;
    if (labelMode) {
        minHeight = (screenHeight-labelMode->height())/2;
    }
    float minWidth = minHeight * float(screenWidth)/float(screenHeight);
    //float maxWidth = screenWidth;
    //float maxHeight = screenHeight;

    int numCols = 1;

    if (numChannels == 1) {

        minHeight = screenHeight - generalSettings->height() * 2 - 20;
        minHeight = float(screenHeight) * 0.85;
        minWidth = float(minHeight) * float(screenWidth)/float(screenHeight);

        if (minWidth > float(screenWidth)*0.85) {
            minWidth = float(screenWidth)*0.85;
            minHeight = minWidth * float(screenHeight)/float(screenWidth);
        }

    } else if (numChannels == 2) {
        /*minWidth = screenWidth/2 - 10;
        minHeight = screenHeight - generalSettings->height();
        maxHeight = minHeight;*/

        minHeight = float(screenHeight)/2 - 10;
        minWidth = minHeight * float(screenWidth)/float(screenHeight);

        numCols = 2;
    } else {
        numCols = (numChannels%2)+(numChannels/2);

        minWidth = (float(screenWidth)/numCols) - 10;
        minHeight = minWidth * float(screenHeight)/float(screenWidth);

        if (minHeight > (float(screenHeight)/2)) {
            minHeight = float(screenHeight)/2 - 10;
            minWidth = minHeight * float(screenWidth)/float(screenHeight);
        }
    }

    QGridLayout* gridLayout = new QGridLayout;
    //gridLayout->setSizeConstraint (QLayout::SetMaximumSize);
    gridLayout->setSpacing(5);
    if (labelMode) {
        gridLayout->setContentsMargins(2, 30, 2, 2);
    } else {
        gridLayout->setContentsMargins(2, 2, 2, 2);
    }

    int row = 0, col = 0;
    for (int index = 0; index < numChannels; index++) {
        DisplayChannel *channel = new DisplayChannel(channelLayout);
        channel->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        if (CameraConfig::shared()->getCameraMode() == SKYPE && numChannels == 2) {
            if (index == CH_PREVIEW) {
                // Set the min width and height for preview channel
                //channel->setMinimumSize(480, 360);
                //channel->setMaximumSize(480, 360);

                minWidth = screenWidth * float(1)/float(4) - 5;
                minHeight = minWidth * float(3)/float(4);

                channel->setMinimumSize(minWidth, minHeight);
                channel->setMaximumSize(minWidth, minHeight);

            } else {

                minWidth = screenWidth * float(2)/float(3);
                minHeight = minWidth * float(screenHeight)/float(screenWidth);

                channel->setMinimumSize(minWidth, minHeight);
                channel->setMaximumSize(minWidth, minHeight);

            }
        } else {
            channel->setMinimumSize(minWidth, minHeight);
            channel->setMaximumSize(minWidth, minHeight);
        }

        if (numChannels == 3 && index == 2) {
            gridLayout->addWidget(channel, row, col, 1, 2, Qt::AlignHCenter);
        } else if (numChannels == 5 && index > 2) {
            gridLayout->addWidget(channel, row, col, 1, 2, Qt::AlignHCenter);
        } else {
            if (CameraConfig::shared()->getCameraMode() == SKYPE && index == CH_PREVIEW) {
                gridLayout->addWidget(channel, row, col, 1, 1, Qt::AlignBottom);
            } else {
                gridLayout->addWidget(channel, row, col, 1, 1, Qt::AlignHCenter);
            }
        }
        col++;
        if (col > (numCols-1)) {
            row += 1;
            col = 0;
        }

        connect(channel,SIGNAL(signalOnVideoSettingsShown(int,bool)), this, SLOT(onVideoSettingsShown(int,bool)));
        connect(channel, SIGNAL(signalScalingEnabled(bool)), this, SLOT(onScalingEnabled(bool)));
        channel->setAutoStartVideo(ChannelState::shared()->getVideoEnabled(startingChannel));
        channel->setRender(ChannelState::shared()->getRenderEnabled(startingChannel));
        channel->setOptimize(ChannelState::shared()->getOptimizeEnabled(startingChannel));
        channel->setStatsEnabled(statsEnabled);
        channel->setOverlayEnabled(bootProp.prop.overlay==1);
        channel->setStatsEnabled(statsEnabled);
        channel->setStopVideoOnDestroy(bootProp.prop.startallvideo != 1);
        channel->setChannel(startingChannel++);
        channel->setTimeBitrateMinMax(bootProp.prop.timeBpsMinMax);
    }

    channelLayout->setLayout(gridLayout);
}

void MainWindow::wheelEvent(QWheelEvent *event) {
    if (!channelLayout) {
        return;
    }

    int numDegrees = event->delta() / 8;
    int numSteps = numDegrees / 15;
    if(numSteps < 1) {
        showGeneralSettings(false);
    } else {
        showGeneralSettings(true);
    }
    event->accept();
}

void MainWindow::showGeneralSettings(bool show)
{
    if(show) {
        // Hide the video settings toolbar for the bottom row of channels
        int x = (this->width()/2) - (generalSettings->width()/2);
        int y = (this->height()) - (generalSettings->height()) - 1;
        generalSettings->setGeometry(x,y,generalSettings->width(),generalSettings->height());
        generalSettings->show();
    } else {
        generalSettings->hide();
    }

    for (int index = 2; index < channelLayout->children().size(); index++) {
        if (channelLayout->children().at(index)->objectName().compare("DisplayChannel") == 0) {
            //((DisplayChannel*)channelLayout->children().at(index))->setSettingsVisible(false);
            ((DisplayChannel*)channelLayout->children().at(index))->positionToolbar();
        }
    }
}

void MainWindow::showPagingBar(bool show) {
    if (show) {
        int x = this->width() - pagingBar->width() - 1;
        int y = this->height() - pagingBar->height() - 1;
        pagingBar->move(x,y);
        pagingBar->show();
    } else {
        pagingBar->hide();
    }
}

// Start - Paging functions
void MainWindow::calculateNumPages() {
    if (singleChannelLayoutSelected) {
        numChannelsPerPage = 1;
        numPages = CameraConfig::shared()->getNumChannels();
        numChannelsOnLastPage = 1;
    } else {
        numChannelsPerPage = bootProp.prop.numChannelsPerPage;
        numPages = CameraConfig::shared()->getNumChannels() / numChannelsPerPage;
        numChannelsOnLastPage = CameraConfig::shared()->getNumChannels() % numChannelsPerPage;
        if (numPages == 0) {
            numPages = 1;
        } else {
            if (numChannelsOnLastPage > 0) {
                numPages += 1;
            } else {
                numChannelsOnLastPage = numChannelsPerPage;
            }
        }
    }
    pagingBar->setNumButtons(numPages);
    qDebug() << "Num Pages: "<<numPages;
    qDebug() << "Num Channels On Last Page: "<<numChannelsOnLastPage;
}

int MainWindow::numChannelsOnPage(int page) {
    if (page == numPages) {
        return numChannelsOnLastPage;
    }
    return numChannelsPerPage;
}
// End - Paging functions

void MainWindow::popoutToastMessage(QString val)
{
    int w,h;
    toastMessage->setText(val);
    toastMessage->getSize(&w,&h);
    int x = (this->width()/2) - (w/2);
    if(h < toastMessage->height()) {
        h = toastMessage->height();
    }
    int y = (this->centralWidget()->height()/2) - (h/2);
    toastMessage->setGeometry(x,y,w,h);
    toastMessage->show();
}

void MainWindow::setupBottomBar()
{
    generalSettings = new GeneralSettings(this);
    generalSettings->hide();

    connect(generalSettings,SIGNAL(settingsExitClicked()),this,SLOT(onExitClicked()));
    connect(generalSettings,SIGNAL(signalSingleChannelLayout()),this,SLOT(onSingleChannelLayout()));
    connect(generalSettings,SIGNAL(signalMultiChannelLayout()),this,SLOT(onMultiChannelLayout()));
    connect(generalSettings,SIGNAL(settingsMicButtonClicked()),this,SLOT(onMicClicked()));
    connect(generalSettings,SIGNAL(signalOnDewarpClicked()),this,SLOT(onDewarpClicked()));
    connect(generalSettings,SIGNAL(signalOnEptzClicked()),this,SLOT(onEptzClicked()));
    connect(generalSettings,SIGNAL(signalOnRawClicked()),this,SLOT(onRawClicked()));
    connect(generalSettings,SIGNAL(settingsCameraSettingsClicked()),this,SLOT(onCameraSettingsClicked()));
    connect(generalSettings,SIGNAL(signalVideoRecordClicked()),this,SLOT(onVideoRecordClicked()));
    connect(generalSettings,SIGNAL(signalOnPrivacyClicked()),this,SLOT(onPrivacyClicked()));

    audioRecord = new AudioRecord(0);
    audioRecord->hide();
    connect(audioRecord, SIGNAL(signalHidden()), this, SLOT(onAudioRecordHidden()));

    videoRecord = new VideoRecord(0);
    videoRecord->hide();
    connect(videoRecord, SIGNAL(signalHidden()), this, SLOT(onAudioRecordHidden()));
    videoRecord->setYoutubeUpload(bootProp.prop.upload360);
}

void MainWindow::mousePressEvent(QMouseEvent *)
{
    showGeneralSettings(false);
}

void MainWindow::setF1KeyPressed() {
    statsEnabled = !statsEnabled;
    if (channelLayout != NULL) {
        // pass on the F1 event to the child DisplayChannels
        channelLayout->setF1KeyPressed();
    }
    if (rawChannel != NULL) {
        rawChannel->setF1KeyPressed();
    }
}

// private slots

// UI events

void MainWindow::onSingleChannelLayout() {
    singleChannelLayoutSelected = true;

    qDebug()<<"Single Channel Layout";

    calculateNumPages();
    showPagingBar(numPages > 1);

    layoutSetup(1, 0);
}

void MainWindow::onMultiChannelLayout() {
    singleChannelLayoutSelected = false;

    qDebug()<<"Multi Channel Layout";

    calculateNumPages();
    showPagingBar(numPages > 1);

    layoutSetup(numChannelsOnPage(1), 0);
}

void MainWindow::onCameraSettingsDialogDestroyed() {
    qDebug() << "Camera settings dialog closed";
    cameraSettings = NULL;
    generalSettings->setEnabled(true);
}

void MainWindow::onAudioRecordHidden() {
    generalSettings->setEnabled(true);
}

void MainWindow::onDewarpDialogDestroyed() {
    qDebug() << "Dewarp settings closed";
    dewarpSettings = NULL;

    // Restart all video channels
    channelLayout->setEnabled(true);
    channelLayout->restartAllVideo();
    generalSettings->setEnabled(true);
}

void MainWindow::onScalingEnabled(bool isEnabled)
{
    if(isEnabled)
        popoutToastMessage("Smoothing enabled, this may lead to CPU performance issues");
    else
        popoutToastMessage("Smoothing disabled, may cause image resize artifacts");
}

void MainWindow::onDewarpClicked()
{
    if (dewarpSettings == NULL) {
        channelLayout->stopAllVideo();
        channelLayout->setEnabled(false);
        qDebug()<<"Launch dewarp settings";
        dewarpSettings = new DewarpSettings(0);
        dewarpSettings->setAttribute(Qt::WA_DeleteOnClose);
        dewarpSettings->setModal(true);
        connect(dewarpSettings, SIGNAL(destroyed()), this, SLOT(onDewarpDialogDestroyed()));
        dewarpSettings->show();
        generalSettings->setEnabled(false);
    }
}

void MainWindow::onEptzClicked() {
    layoutSetupEptz();
}

void MainWindow::onPrivacyClicked() {
    layoutSetupPrivacyMask();
}

void MainWindow::onCameraSettingsClicked() {
    if (cameraSettings == NULL) {
        qDebug()<<"Launch camera settings";
        cameraSettings = new CameraSettings(0);
        cameraSettings->setAttribute(Qt::WA_DeleteOnClose);
        connect(cameraSettings, SIGNAL(destroyed()), this, SLOT(onCameraSettingsDialogDestroyed()));
        cameraSettings->show();
        generalSettings->setEnabled(false);
    }
}

void MainWindow::onMicClicked() {
    int x = (this->width()/2) - (audioRecord->width()/2);
    int y = (this->centralWidget()->height()/2) - (audioRecord->height()/2);
    audioRecord->setGeometry(x,y,audioRecord->width(),audioRecord->height());
    audioRecord->show();
    audioRecord->adjustSize();
    generalSettings->setEnabled(false);
}

void MainWindow::onRawClicked() {
    ScrollableWindow *rawWindow = new ScrollableWindow(0);
    rawWindow->setMainWindow(this);
    rawWindow->setMinimumSize(640+5, 480+5);
    connect(rawWindow, SIGNAL(destroyed()), this, SLOT(onRawWindowClosed()));
    connect(rawWindow, SIGNAL(destroyed()), generalSettings, SLOT(deselectRawButton()));

    rawChannel = new DisplayChannel(rawWindow);
    connect(rawWindow, SIGNAL(destroyed()), rawChannel, SLOT(deleteLater()));
    rawWindow->setWidget(rawChannel);

    rawChannel->setChannel(CameraConfig::shared()->getRawChannelID());
    rawChannel->setRender(bootProp.prop.render==1);
    rawChannel->setStopVideoOnDestroy(bootProp.prop.startallvideo != 1);
    rawChannel->setStatsEnabled(statsEnabled);
    rawChannel->setTimeBitrateMinMax(bootProp.prop.timeBpsMinMax);

    rawWindow->show();
}

void MainWindow::onRawWindowClosed() {
    rawChannel = NULL;
}

int numChildrenToDelete = 0;

void MainWindow::onChildDeleted() {
    numChildrenToDelete -= 1;
    qDebug() << "Children remaining "<<numChildrenToDelete;

    if (numChildrenToDelete <= 0) {
        delete channelLayout;
        channelLayout = NULL;
        if (bootProp.prop.overlay == 1) {
            OverlayManager::shared()->cleanup();
        }
        this->close();
    }
}

void MainWindow::onExitClicked() {
    if (rawChannel != NULL) {
        ((ScrollableWindow*)rawChannel->parentWidget()->parentWidget())->close();
        rawChannel = NULL;
    }
    if (audioRecord != NULL && audioRecord->isVisible() == true) {
        audioRecord->close();
    }
    if (cameraSettings != NULL && cameraSettings->isVisible() == true) {
        cameraSettings->close();
    }
    if (dewarpSettings != NULL && dewarpSettings->isVisible() == true) {
        dewarpSettings->close();
    }

    ChannelState::shared()->deinit();

    if (channelLayout != NULL) {
        if(bootProp.prop.startallvideo == 1) {
            int numChannels = CameraConfig::shared()->getNumChannels();
            for (int i = 0; i < numChannels; i++) {
                engine_stopvideo(i);
            }
            engine_stopvideo(CameraConfig::shared()->getRawChannelID());
        }
        channelLayout->stopAllVideo();
        QList<DisplayChannel*> channelList = channelLayout->getChannelList();
        numChildrenToDelete = channelList.count();
        for (int index = 0; index < channelList.count(); index++) {
            connect(channelList.at(index), SIGNAL(signalCleanupDone()),this,SLOT(onChildDeleted()));
            channelList.at(index)->cleanup();
        }
    }
}

void MainWindow::onPageChanged(int pageIndex) {
    qDebug() << "Page changed" << pageIndex+1;
    int startingChannel = (pageIndex)*numChannelsPerPage;
    qDebug() << "Starting channel:" <<startingChannel<<", Num channels to display:"<<numChannelsOnPage(pageIndex+1);
    layoutSetup(numChannelsOnPage(pageIndex+1), startingChannel);
}

void MainWindow::onPagingBarResized() {
    if (pagingBar && pagingBar->isVisible()) {
        showPagingBar(true);
    }
}

void MainWindow::onVideoSettingsShown(int channel, bool shown) {
    if (channel <= 1) {
        return;
    }

    // If the video settings toolbar for the bottom row of channels is visible, hide the general settings toolbar
    for (int index = 2; index < channelLayout->children().size(); index++) {
        if (channelLayout->children().at(index)->objectName().compare("DisplayChannel") == 0) {
            if (((DisplayChannel*)channelLayout->children().at(index))->getChannel() == channel) {
                // hide the bottom bar
                showGeneralSettings(false);
                if (shown) {
                    showPagingBar(false);
                } else {
                    if (numPages > 1) {
                        showPagingBar(true);
                    }
                }
                break;
            }
        }
    }
}

// END UI events

// EngineAPIManager events

void MainWindow::onEngineInitSuccess(int) {

#ifdef ENABLE_AUDIO
    renderAudio = new AudioRenderer;
    renderAudio->startPlayback();
#endif

    // Get the channel configuration
    connect(apiManager, SIGNAL(signalEngineGetCameraConfigSuccess()), this, SLOT(onCameraConfigGetSuccess()));
    connect(apiManager, SIGNAL(signalEngineGetCameraConfigFailed(int)), this, SLOT(onCameraConfigGetFailed(int)));
    apiManager->engineGetCameraConfig();
}

void MainWindow::onEngineInitFailed() {
    qFatal("Engine init failed");
    apiManager->cleanup();
    exit(2);
}

void MainWindow::onCameraConfigGetSuccess() {
    CameraConfig *channelConfig = CameraConfig::shared();

    /*if (channelConfig->getCameraMode() == SKYPE) {
        labelMode = new QLabel(this);
        labelMode->setFont(QFont("Arial", 12, QFont::Normal));
        labelMode->setAlignment(Qt::AlignCenter);
        //labelMode->setGeometry(this->width()-153, this->height()-33, 150, 30);
        labelMode->setGeometry(this->width()/2-75, 3, 150, 30);
        labelMode->setStyleSheet("color: #ffffff;");
        if (channelConfig->getCameraMode() == SKYPE) {
            labelMode->setText(tr("Webcam Mode"));
        } else {
            labelMode->setText(tr("IPCAM Mode"));
        }

        labelMode->show();
    }*/

    setupBottomBar();

    if(channelConfig->getRawChannelID() == -1) {
        generalSettings->setRawButtonEnabled(false);
    }

    generalSettings->setSingleChannelLayoutEnabled(channelConfig->getNumChannels() > 1);
    generalSettings->setMultiChannelLayoutEnabled(channelConfig->getNumChannels() > 1);
    generalSettings->setEptzLayoutEnabled(channelConfig->getNumChannels() >= 2 && bootProp.prop.eptz == 1);
    // default is multichannel layout
    generalSettings->selectMultipleChannelLayout();

    // Setup audio record options
    QList<AUDIO_FORMAT> formats;
#ifdef WITH_ENGINE
    if (CameraConfig::shared()->getCameraMode() == IPCAM) {
        formats.append(PCM);
        formats.append(AAC);
        audioRecord->setCaptureOptions(formats, 1);
    } else if (CameraConfig::shared()->getCameraMode() == SKYPE) {
        formats.append(PCM);
        audioRecord->setCaptureOptions(formats, 0);
    }
#else
    formats.append(PCM);
    formats.append(AAC);
    audioRecord->setCaptureOptions(formats, 1);
#endif

#ifdef WITH_ENGINE
        if (engine_is_audio_initialized()) {
#endif
            generalSettings->setAudioEnabled(true);

            if (bootProp.prop.audio == 1) {
                // Start audio recording
                audioRecord->startAudioRecord();
            }
        } else {
            generalSettings->setAudioEnabled(false);
        }

    if (bootProp.prop.numChannelsPerPage == 0) {
        bootProp.prop.numChannelsPerPage = channelConfig->getNumChannels();
    }

    calculateNumPages();

    if (numPages > 1) {
        showPagingBar(true);
    }

    //layoutSetup(numChannelsOnPage(1), 0);
    generalSettings->setPrivacyEnabled(false);
    if (bootProp.prop.overlay == 1) {
#ifdef LOAD_FONTS
        bool fontFileExists = true;
        QString assetFile = QCoreApplication::applicationDirPath() + "/" + DEFAULT_OVERLAY_FONT_FILE;
        QFileInfo fileinfo(assetFile);
        if (!fileinfo.exists()) {
            //AlertHandler::displayError(QString("Could not locate font file: ").append(assetFile).append("!"));
            fontFileExists = false;
        }

        assetFile = QCoreApplication::applicationDirPath() + "/" + DEFAULT_OVERLAY_IMAGE;
        fileinfo.setFile(assetFile);
        if (!fileinfo.exists()) {
            //AlertHandler::displayError(QString("Could not locate logo: ").append(assetFile).append("!"));
        }
#endif

#ifdef WITH_ENGINE
        for (int channel = 0; channel < channelConfig->getNumChannels(); channel++) {
            dewarp_mode_t dmode = EMODE_OFF;
            dewarp_params_t dparams;
            if (engine_get_dewarp_params((video_channel_t)channel, 0, &dmode, &dparams) == 0) {
                if (dmode == EMODE_WM_ZCLStretch) {
                    channelWithZclStretch = channel;
                    generalSettings->setPrivacyEnabled(true);
                    break;
                }
            }
        }

        OverlayManager::shared()->init(MAX_OVERLAY_TEXTS, engine_get_max_privacy_masks());

        engine_set_interval_bitrate(bootProp.prop.timeBps);
        engine_set_interval_bitrate_min_max(bootProp.prop.timeBpsMinMax);
#else
        OverlayManager::shared()->init(MAX_OVERLAY_TEXTS, 4);
#endif
        for (int channel = 0; channel < channelConfig->getNumChannels(); channel++) {
            OverlayManager::shared()->initOverlayImage(channel);
            OverlayManager::shared()->initOverlayTime(channel);
            OverlayManager::shared()->initOverlayTexts(channel);
            OverlayManager::shared()->initOverlayFont(channel);
            OverlayManager::shared()->initPrivacyMasks(channel);

#ifdef LOAD_FONTS
            if (fontFileExists) {
                QFileInfo fontfileinfo(OverlayManager::shared()->getOverlayFont(channel)->fontPath);
                qDebug() << "Loading font for channel" << StringUtils::shared()->channelToString((video_channel_t)channel);
                if (engine_overlay_load_font(channel, (char*)fontfileinfo.absoluteFilePath().toStdString().c_str(), OverlayManager::shared()->getOverlayFont(channel)->fontSize) == 0) {
                    OverlayManager::shared()->setFontLoaded(channel, true);
                }
            }
#endif

            OverlayManager::shared()->setOverlayImageEnabled(channel, bootProp.prop.overlayImage==1);
            OverlayManager::shared()->setOverlayTextEnabled(channel, bootProp.prop.overlayText==1);
            for (unsigned int index = bootProp.prop.numOverlayText; index < MAX_OVERLAY_TEXTS; index++) {
                OverlayManager::shared()->setOverlayTextEnabled(channel, false, index);
            }
            OverlayManager::shared()->setOverlayTimeEnabled(channel, bootProp.prop.overlay==1);
        }
    }

    for (int channel = 0; channel < channelConfig->getNumChannels(); channel++) {
        ChannelState::shared()->init(channel);
        ChannelState::shared()->setVideoEnabled(channel, bootProp.prop.video==1);
        ChannelState::shared()->setRenderEnabled(channel, bootProp.prop.render==1);
    }
    ChannelState::shared()->init(channelConfig->getRawChannelID());
    ChannelState::shared()->setVideoEnabled(channelConfig->getRawChannelID(), bootProp.prop.video==1);
    ChannelState::shared()->setRenderEnabled(channelConfig->getRawChannelID(), bootProp.prop.render==1);

    layoutSetup(numChannelsOnPage(1), 0);

    if (bootProp.prop.startallvideo == 1) {
        // start all remaining channels
        int numChannels = channelConfig->getNumChannels();
        for (int i = numChannelsOnPage(1); i < numChannels; i++) {
            engine_startvideo(i);
        }
        engine_startvideo(channelConfig->getRawChannelID());
    }

    apiManager->cleanup();
}

void MainWindow::onCameraConfigGetFailed(int) {
    qFatal("Failed to read channel configuration");
    apiManager->cleanup();
    exit(2);
}


void MainWindow::onVideoRecordClicked() {
    int x = (this->width()/2) - (videoRecord->width()/2);
    int y = (this->centralWidget()->height()/2) - (videoRecord->height()/2);
    videoRecord->setGeometry(x,y,videoRecord->width(),videoRecord->height());
    videoRecord->show();
    generalSettings->setEnabled(false);
}

// END EngineAPIManager events

// END private slots
