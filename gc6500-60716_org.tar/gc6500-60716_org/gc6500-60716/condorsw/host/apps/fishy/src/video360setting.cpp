/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include "video360setting.h"
#include "ui_video360setting.h"

Video360Setting::Video360Setting(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Video360Setting)
{
    ui->setupUi(this);
    time = new QTime();
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateCaption()));
}

Video360Setting::~Video360Setting()
{
    delete ui;
}

void Video360Setting::on_buttonStartRecord_clicked()
{
    timer->start(1000);
    time->restart();
    QTimer::singleShot(60000, this, SLOT(uploadVideo()));
    ui->buttonStartRecord->setEnabled(false);

}

void Video360Setting::updateCaption()
{

    int secs = time->elapsed() / 1000;
    int mins = (secs / 60) % 60;
    //int hours = (secs / 3600);
    secs = secs % 60;
    QString local(QString("%2:%3")
                  .arg(mins, 2, 10, QLatin1Char('0'))
                  .arg(secs, 2, 10, QLatin1Char('0')));
    ui->labelTimer->setText("Recording the file for 1 min " + local);
    emit(this->capture360VideoStart());
}

void Video360Setting::uploadVideo()
{
    qDebug("\nTimer elapsed upload now \n");
    timer->stop();
    ui->buttonStartRecord->setEnabled(true);
    ui->labelTimer->setText("Recording complete, uploading the file");
    emit(this->capture360VideoStop());
}
