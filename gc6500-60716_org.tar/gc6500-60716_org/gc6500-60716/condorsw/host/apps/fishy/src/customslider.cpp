/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include "customslider.h"
#include "ui_customslider.h"
#include <QLineEdit>

CustomSlider::CustomSlider(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CustomSlider)
{
    ui->setupUi(this);

    ui->slider->setTracking(false);

    connect(ui->lineEdit, SIGNAL(returnPressed()), this, SLOT(onLineEditReturnPressed()));

    connect(ui->slider, SIGNAL(sliderMoved(int)), this, SLOT(onSliderMoved(int)));
    connect(ui->slider, SIGNAL(valueChanged(int)), this, SLOT(onValueChanged(int)));

    step = 1;
}

CustomSlider::~CustomSlider()
{
    delete ui;
}

int CustomSlider::value() {
    return ui->slider->value();
}

void CustomSlider::setRange(int min, int max) {
    disconnect(ui->slider, SIGNAL(valueChanged(int)), 0, 0);
    ui->slider->setRange(min, max);
    ui->labelMin->setText(QString::number(min));
    ui->labelMax->setText(QString::number(max));
    this->setLabelValue(QString::number(ui->slider->value()));
    connect(ui->slider, SIGNAL(valueChanged(int)), this, SLOT(onValueChanged(int)));
}

void CustomSlider::setStep(int step) {
    this->step = step;
}

void CustomSlider::setPageStep(int step) {
    ui->slider->setPageStep(step);
}

void CustomSlider::setValue(int value, bool emitSignal) {
    if (!emitSignal) {
        disconnect(ui->slider, SIGNAL(valueChanged(int)), 0, 0);
    }
    ui->slider->setValue(value/step);
    ui->lineEdit->setText(QString::number(ui->slider->value()));
    if (!emitSignal) {
        connect(ui->slider, SIGNAL(valueChanged(int)), this, SLOT(onValueChanged(int)));
    }
}

void CustomSlider::setLabelValue(QString value) {
    ui->lineEdit->setText(value);
}

void CustomSlider::setLabelWidth(int width) {
    ui->lineEdit->setMinimumWidth(width+5);
    ui->lineEdit->setMaximumWidth(width+5);
}

void CustomSlider::removeLabel() {
    ui->lineEdit->setVisible(false);
}

// Private slots
void CustomSlider::onSliderMoved(int value) {
    ui->lineEdit->setText(QString::number(value));
}

void CustomSlider::onValueChanged(int value) {
    ui->lineEdit->setText(QString::number(value));
    //qDebug("Emitting value for %s -> %d", this->objectName().toStdString().c_str(), ui->slider->value()*step);
    emit(signalValueChanged(ui->slider->value()*step));
}

void CustomSlider::onLineEditReturnPressed() {
    int value = ui->lineEdit->text().toInt();
    if (value < ui->slider->minimum() || value > ui->slider->maximum()) {
        return;
    }

    this->setValue(value*step);
    emit(signalValueChanged(value*step));
}

// END private slots
