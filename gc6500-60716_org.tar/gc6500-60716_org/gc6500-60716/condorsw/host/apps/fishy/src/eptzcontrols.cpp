/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include "eptzcontrols.h"
#include "ui_eptzcontrols.h"
#include <qevent.h>
#include "log.h"

const int ZOOM_PIX_MIN_MAX[2] = {1, 4};
const int HPAN_PIX_MIN_MAX[4][2] = {{0,0},{-420, 420},{-560, 560},{-630, 630}};
const int VPAN_PIX_MIN_MAX[4][2] = {{0,0},{-270, 270},{-360, 360},{-410, 410}};

const int HPAN_DEG_MIN_MAX[2] = {-50, 50};
const int VPAN_DEG_MIN_MAX[2] = {-50, 50};
const int TILT_DEG_MIN_MAX[2] = {-180, 180};
const int ZOOM_DEG_MIN_MAX[2] = {20, 60};

const QString MODES[2] = {"EPTZ (Pixels)", "EPTZ (Degrees)"};

EptzControls::EptzControls(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EptzControls)
{
    ui->setupUi(this);

    ui->cbModes->addItem(MODES[0]);
    ui->cbModes->addItem(MODES[1]);
    ui->cbModes->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    //ui->cbModes->adjustSize();

    ui->sliderZoom->removeLabel();
    ui->sliderZoom->setRange(ZOOM_PIX_MIN_MAX[0], ZOOM_PIX_MIN_MAX[1]);
    ui->sliderZoom->setPageStep(1);
    ui->sliderZoom->setValue(1);

    ui->sliderHPan->removeLabel();
    ui->sliderHPan->setRange(HPAN_PIX_MIN_MAX[ui->sliderZoom->value()][0], HPAN_PIX_MIN_MAX[ui->sliderZoom->value()][1]);
    ui->sliderHPan->setValue(0);
    ui->sliderHPan->setEnabled(false);

    ui->sliderVPan->removeLabel();
    ui->sliderVPan->setRange(VPAN_PIX_MIN_MAX[ui->sliderZoom->value()][0], VPAN_PIX_MIN_MAX[ui->sliderZoom->value()][1]);
    ui->sliderVPan->setValue(0);
    ui->sliderVPan->setEnabled(false);

    ui->sliderTilt->removeLabel();
    ui->sliderTilt->setValue(0);
    ui->sliderTilt->hide();
    ui->labelTilt->hide();

    connect(ui->cbModes, SIGNAL(currentIndexChanged(QString)),this,SLOT(onModeChanged(QString)));
    connectSliders();
}

EptzControls::~EptzControls()
{
    delete ui;
}

void EptzControls::keyPressEvent(QKeyEvent *event) {
    switch(event->key()) {
        case Qt::Key_Up:
            ui->sliderVPan->setValue(ui->sliderVPan->value()+10, true);
            break;
        case Qt::Key_Down:
            ui->sliderVPan->setValue(ui->sliderVPan->value()-10, true);
            break;
        case Qt::Key_Left:
            if (ui->cbModes->currentIndex() == 0) {
                ui->sliderHPan->setValue(ui->sliderHPan->value()-10, true);
            } else {
                ui->sliderHPan->setValue(ui->sliderHPan->value()+10, true);
            }
            break;
        case Qt::Key_Right:
            if (ui->cbModes->currentIndex() == 0) {
                ui->sliderHPan->setValue(ui->sliderHPan->value()+10, true);
            } else {
                ui->sliderHPan->setValue(ui->sliderHPan->value()-10, true);
            }
            break;
        case Qt::Key_Plus:
            if (ui->cbModes->currentIndex() == 0) {
                ui->sliderZoom->setValue(ui->sliderZoom->value()+1, true);
            } else {
                ui->sliderZoom->setValue(ui->sliderZoom->value()-1, true);
            }
            break;
        case Qt::Key_Minus:
            if (ui->cbModes->currentIndex() == 0) {
                ui->sliderZoom->setValue(ui->sliderZoom->value()-1, true);
            } else {
                ui->sliderZoom->setValue(ui->sliderZoom->value()+1, true);
            }
            break;
        case Qt::Key_PageUp:
            if (ui->cbModes->currentIndex() == 1) {
                ui->sliderTilt->setValue(ui->sliderTilt->value()+10, true);
            }
            break;
        case Qt::Key_PageDown:
            if (ui->cbModes->currentIndex() == 1) {
                ui->sliderTilt->setValue(ui->sliderTilt->value()-10, true);
            }
            break;
        default:
            event->ignore();
            break;
    }
}

void EptzControls::connectSliders() {
    connect(ui->sliderZoom, SIGNAL(signalValueChanged(int)), this, SLOT(onZoomChanged(int)));
    connect(ui->sliderHPan, SIGNAL(signalValueChanged(int)), this, SLOT(onPanChanged(int)));
    connect(ui->sliderVPan, SIGNAL(signalValueChanged(int)), this, SLOT(onPanChanged(int)));
    connect(ui->sliderTilt, SIGNAL(signalValueChanged(int)), this, SLOT(onPanChanged(int)));
}

void EptzControls::disconnectSliders() {
    disconnect(ui->sliderZoom, SIGNAL(signalValueChanged(int)), this, SLOT(onZoomChanged(int)));
    disconnect(ui->sliderHPan, SIGNAL(signalValueChanged(int)), this, SLOT(onPanChanged(int)));
    disconnect(ui->sliderVPan, SIGNAL(signalValueChanged(int)), this, SLOT(onPanChanged(int)));
    disconnect(ui->sliderTilt, SIGNAL(signalValueChanged(int)), this, SLOT(onPanChanged(int)));
}

// Slots

void EptzControls::onModeChanged(QString mode) {
    disconnectSliders();

    if (mode.compare(MODES[0]) == 0) {
        // ZCL STRETCH
        ui->sliderZoom->setRange(ZOOM_PIX_MIN_MAX[0], ZOOM_PIX_MIN_MAX[1]);
        ui->sliderZoom->setValue(1);
        ui->sliderHPan->setRange(HPAN_PIX_MIN_MAX[ui->sliderZoom->value()][0], HPAN_PIX_MIN_MAX[ui->sliderZoom->value()][1]);
        ui->sliderHPan->setValue(0);
        ui->sliderVPan->setRange(VPAN_PIX_MIN_MAX[ui->sliderZoom->value()][0], VPAN_PIX_MIN_MAX[ui->sliderZoom->value()][1]);
        ui->sliderVPan->setValue(0);
        ui->sliderHPan->setEnabled(false);
        ui->sliderVPan->setEnabled(false);

        ui->sliderTilt->hide();
        ui->labelTilt->hide();
    } else {
        // 1 PANEL SWEEP
        ui->sliderZoom->setRange(ZOOM_DEG_MIN_MAX[0], ZOOM_DEG_MIN_MAX[1]);
        ui->sliderZoom->setValue(ZOOM_DEG_MIN_MAX[1]);
        ui->sliderHPan->setRange(HPAN_DEG_MIN_MAX[0], HPAN_DEG_MIN_MAX[1]);
        ui->sliderHPan->setValue(0);
        ui->sliderVPan->setRange(VPAN_DEG_MIN_MAX[0], VPAN_DEG_MIN_MAX[1]);
        ui->sliderVPan->setValue(0);
        ui->sliderTilt->setRange(TILT_DEG_MIN_MAX[0], TILT_DEG_MIN_MAX[1]);
        ui->sliderTilt->setValue(0);
        ui->sliderHPan->setEnabled(true);
        ui->sliderVPan->setEnabled(true);

        ui->sliderTilt->show();
        ui->labelTilt->show();
    }

    emit(signalOnEPTZ(ui->cbModes->currentIndex(), ui->sliderZoom->value(), ui->sliderHPan->value(), ui->sliderVPan->value(), ui->sliderTilt->value()));
    connectSliders();
    this->setFocus();
}

void EptzControls::onZoomChanged(int zoom) {
    if (ui->cbModes->currentIndex() == 0) {
        if (zoom > 1) {
            ui->sliderHPan->setRange(HPAN_PIX_MIN_MAX[zoom-1][0], HPAN_PIX_MIN_MAX[zoom-1][1]);
            ui->sliderVPan->setRange(VPAN_PIX_MIN_MAX[zoom-1][0], VPAN_PIX_MIN_MAX[zoom-1][1]);
            ui->sliderHPan->setEnabled(true);
            ui->sliderVPan->setEnabled(true);
        } else {
            ui->sliderHPan->setRange(HPAN_PIX_MIN_MAX[zoom][0], HPAN_PIX_MIN_MAX[zoom][1]);
            ui->sliderVPan->setRange(VPAN_PIX_MIN_MAX[zoom][0], VPAN_PIX_MIN_MAX[zoom][1]);
            ui->sliderHPan->setValue(0);
            ui->sliderVPan->setValue(0);
            ui->sliderHPan->setEnabled(false);
            ui->sliderVPan->setEnabled(false);
        }
    } else {
        // 1 panel eptz
    }
    emit(signalOnEPTZ(ui->cbModes->currentIndex(), ui->sliderZoom->value(), ui->sliderHPan->value(), ui->sliderVPan->value(), ui->sliderTilt->value()));
    this->setFocus();
}

void EptzControls::onPanChanged(int) {
    emit(signalOnEPTZ(ui->cbModes->currentIndex(), ui->sliderZoom->value(), ui->sliderHPan->value(), ui->sliderVPan->value(), ui->sliderTilt->value()));
    this->setFocus();
}
