/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
/*
 * fishynox.cpp
 *
 *  Created on: Jan 22, 2014
 *      Author: sudhishk
 */

#include <string>
#include <cstring>
#include <stdio.h>
#include <iostream>
#include "interface.h"

#include "fishynox.h"

void  ltrim(string& str, const locale& loc)
{
  string::size_type pos = 0;
  while (pos < str.size() && isspace(str[pos], loc)) pos++;
  str.erase(0, pos);
}

void  rtrim(string& str, const locale& loc)
{
  string::size_type pos = str.size();
  while (pos > 0 && isspace(str[pos - 1], loc)) pos--;
  str.erase(pos);
}

void btrim(string& str)
{
	const locale& loc = locale();
	ltrim(str, loc);
	rtrim(str, loc);
}

//typedef void (*callback) (void*);

FishyNox::FishyNox() {
	// TODO Auto-generated constructor stub
	quit = false;
	createPrimaryCommandArray();
	createSecondaryCommandArray();
}

FishyNox::~FishyNox() {
	// TODO Auto-generated destructor stub
}

int FishyNox::engineInit(int devId)
{
	//Call engine init here inside a thread
	int ret = engine_init(devId);
	if(ret < 0)
	{
		printf("\n\nEngine init failed\n\n");
		engine_deinit();
		exit(2);
	}
	int rawId = -1;
	numchannels = engine_getvideo_channel_count();

	for(int i=0;i<numchannels;i++)
	{
		engine_startvideo(i);
	}
	return 1;
}

int FishyNox::engineDeinit()
{
	for(int i=0;i<numchannels;i++)
	{
		engine_stopvideo(i);
	}
	return engine_deinit();
}

string FishyNox::getMultipleCommandParams(string command, int* p1, int *p2, int *p3, int *p4)
{
	string chid,timer;
//	int sdlim = command.find("(");
//	string keyread = command.erase(0,sdlim+1);
//	int edlim = keyread.find(")");
//	keyread = keyread.erase(edlim,keyread.length());
	string keyread = command;
//	printf("\n\nthe () removed string is %s \n\n",keyread.c_str());
#if 0
	int pdlim = keyread.find(",");
	if(pdlim != string::npos)
	{
		timer = strtok((char*)keyread.c_str(),",");
		chid = keyread.erase(0,pdlim+1);
		*p1 = atoi(chid.c_str());
	}
	*p2 = atoi(timer.c_str());
#endif
	if(!keyread.length())
	{
		//No args were passed to the function return default
		return keyread;
	}
	int counter = 0;
	int pdlim = keyread.find(",");
	if(pdlim == string::npos)
	{
		//only 1 param passed assign and return
		*p1 = atoi(keyread.c_str());
		return keyread;
	}
	while(pdlim != string::npos)
	{
		//For Multiple args passed
			chid = strtok((char*)keyread.c_str(),",");

//			printf("\nkeyread after strtok %s : %s\n",keyread.c_str(),chid.c_str());
			switch(counter)
			{
			case 0:
				*p1 = atoi(chid.c_str());
				break;
			case 1:
				*p2 = atoi(chid.c_str());
				break;
			case 2:
				*p3 = atoi(chid.c_str());
				break;
			default:
				break;
			}
			counter++;
			keyread = keyread.erase(0,pdlim+1);
			pdlim = keyread.find(",") ;
	}

	if(keyread.length())
	{
		switch(counter)
		{
		case 0:
			*p1 = atoi(keyread.c_str());
			break;
		case 1:
			*p2 = atoi(keyread.c_str());
			break;
		case 2:
			*p3 = atoi(keyread.c_str());
			break;
		case 3:
			*p4 = atoi(keyread.c_str());
			break;
		default:
			break;
		}
	}
	return keyread;
}

string FishyNox::getsingleCommandParams(string command, int* id)
{
	int sdlim = command.find("(");
	string keyread = command.erase(0,sdlim+1);
	int edlim = keyread.find(")");
	keyread = keyread.erase(edlim,keyread.length());
//	printf("\n\nthe () removed string is %s\n\n",keyread.c_str());
	if(keyread.length() == 0)
		*id = -1;
	else
		*id = atoi(keyread.c_str());
	return keyread;
}

string FishyNox::getFirstString(string &command)
{
	btrim(command);
//	printf("\n1st command  %s\n",command.c_str());
	if(command.length() == 0)
		return command;
	string temp = command;
//	string value = strtok((char*)temp.c_str()," ");
	string value;
	int dlim = command.find(" ");
	if(dlim != string::npos)
	{
		string t1 = command;
		value = t1.erase(dlim,t1.length());
		command = command.erase(0,value.length());
	}
	else
	{
		value = command;
		command = "";
	}
//	printf("\n2nd command %d: command : %s value:%s\n",value.length(),command.c_str(),value.c_str());

//	printf("\n 3rd command %s: value:%s\n",command.c_str(),value.c_str());
	return value;
}

void FishyNox::startVideo(string localcommand)
{
	string command = localcommand;

	command = getFirstString(localcommand);
//	printf("\nCommand in start_video is : %s\n",command.c_str());
	if(command.length() == 0)
	{
		//start all channels
		for(int i=0; i< numchannels; i++)
		{
			if(engine_startvideo(i) < 0)
				printf("\nVideo already running for ch:%d\n",i);
		}
	}
	else
	{
		btrim(command);
		string schid = getFirstString(command);
		int chid = atoi(schid.c_str());
		if(chid > numchannels)
			printf("\nInvalid channel id %d: max can be %d\n",chid,numchannels);
		else
		{
			if(engine_startvideo(chid) < 0)
				printf("\nVideo already running for ch:%d\n",chid);
		}
	}
	if(command.length() != 0)
	{
		string stimer = getFirstString(command);
		//get the timer value and start timer
	}
}

void FishyNox::stopVideo(string command)
{
	btrim(command);
	if(command.length() == 0)
	{
		//stop all channels
		for(int i=0; i< numchannels; i++)
		{
			if(engine_stopvideo(i) < 0)
				printf("\nVideo already stoppend for ch:%d\n",i);
		}
	}
	else
	{
		string schid = getFirstString(command);
		int chid = atoi(schid.c_str());
		//stop indivisual channel
		if(chid > numchannels)
			printf("\nInvalid channel id %d: maxcan be %d\n",chid,numchannels);
		else
		{
			if(engine_stopvideo(chid) < 0)
				printf("\nVideo already stopped for ch:%d\n",chid);
		}
	}
}
void FishyNox::startCaptureAudio(string command)
{
	btrim(command);
	//printf("\nStart audio capture with params %s\n",command);
	string schid = getFirstString(command);
	if(schid.length() == 0)
	{
		improperCommand();
		return;
	}
	int chid;
	if(strcmp(schid.c_str(),"aac"))
		chid = AUD_CH2;
	else if(strcmp(schid.c_str(),"pcm"))
		chid = AUD_CH1;
	else
	{
		improperCommand();
		return;
	}
	string sr = getFirstString(command);
	if(sr.length() == 0)
	{
		improperCommand();
		return;
	}
	int sRate = atoi(sr.c_str());
	string path = getFirstString(command);
	if(path.length() == 0)
	{
		improperCommand();
		return;
	}
	engine_startaudio(chid,sRate,(char*)path.c_str());
	engine_start_capture_audio();
}

void FishyNox::stopCaptureAudio(string command)
{
	btrim(command);
	engine_stop_capture_audio();
	engine_stopaudio();
}

void FishyNox::startCaptureVideo(string command)
{
	btrim(command);
#if 0
	int chid = -1,timer = 0;
	string keyread = getMultipleCommandParams(command, &chid,&timer);
	printf("\n command params %d, %d \n",chid,timer);
	if(chid < 0)
	{
		//Nothing was passed start all channels
		for(int i=0;i<numchannels;i++)
		{
			engine_start_capture(i);
//			engine_startvideo(i);
		}
	}
	else
	{
		//call start video on chid
		engine_start_capture(chid);
//		engine_startvideo(chid);
	}
#endif
	if(command.length() == 0)
	{
		//stop all channels
		for(int i=0; i< numchannels; i++)
		{
			engine_start_capture_video(i);
		}
	}
	else
	{
		string schid = getFirstString(command);
		int chid = atoi(schid.c_str());
		//stop indivisual channel
		if(chid > numchannels)
			printf("\nInvalid channel id %d: maxcan be %d\n",chid,numchannels);
		else
			engine_start_capture_video(chid);
	}
}

void FishyNox::stopCaptureVideo(string command)
{
	btrim(command);
#if 0
	int chid = -1,timer=0;
	string keyread = getMultipleCommandParams(command, &chid);
//	string keyread = getCommandParams(command);
	printf("\n stop command params %d, %d \n",chid,timer);
	if(chid < 0)
	{
		//Nothing was passed call stop video on all channels
		for(int i=0;i<numchannels;i++)
		{
			engine_stop_capture(i);
		}
	}
	else
	{
//		engine_stopvideo(chid);
		engine_stop_capture(chid);
	}
#endif
	if(command.length() == 0)
	{
		//stop all channels
		for(int i=0; i< numchannels; i++)
		{
			engine_stop_capture_video(i);
		}
	}
	else
	{
		string schid = getFirstString(command);
		int chid = atoi(schid.c_str());
		//stop indivisual channel
		if(chid > numchannels)
			printf("\nInvalid channel id %d: maxcan be %d\n",chid,numchannels);
		else
			engine_stop_capture_video(chid);
	}

}

void FishyNox::improperCommand()
{
	cout<<"Improper/Invalid command"<<endl;
	cout<<"Try usage() for supported command list"<<endl;
}

void FishyNox::usage()
{
	cout<< "usage"<< endl;
	cout<< "\tstart_capture_video "<< endl;
	cout<< "\tstart_capture_video chId"<< endl;
	cout<< "\tstop_capture_video"<< endl;
	cout<< "\tstop_capture_video chId"<< endl;
	cout<< "\tstart_capture_audio aac/pcm sample-rate dump-path"<< endl;
	cout<< "\tstop_capture_audio"<< endl;
	cout<< "\tset/get <command> <arguments>"<< endl;
	cout<< "\tquit"<< endl;
}

void FishyNox::commandList()
{
	cout<< "\tset/get <command> <arguments>"<< endl;

}

int FishyNox::parseCommandArgs(string command)
{
	int ch;
	btrim(command);
#if 0
	if(command.find("start_capture") != string::npos)
	{
		string trim("start_capture");
		command = command.erase(0,trim.length());
		startCaptureVideo(command);
	}
	else if(command.find("stop_capture(") != string::npos)
	{
		if(command.find(")") != string::npos)
			stopCaptureVideo(command);
	}
	else if(!command.compare("q") || !command.compare("quit"))
		quit = true;
	else if(command.find("set_params(") != string::npos)
	{
		if(command.find(")") != string::npos)
			setParams(command);
	}
	else if(!command.compare("usage()"))
		usage();
	else
	{
		improperCommand();
	}
#else
	string pc = getFirstString(command);
//	printf("\nWhy is this command supported %s****%s\n",command.c_str(), pc.c_str());
	if(pc.length() == 0)
	{
		//No space found only one value passed
		pc = command;
	}
	btrim(pc);
	if(findPrimaryCommandSupported(pc))
	{
		//Primary Command supported look for secondary after triming
//		printf("\nPrimary command supported :%s\n",command.c_str());
//		command = command.erase(0,pc.length());
		btrim(command);
		performPrimaryCommandAction(command);
	}
	else
	{
		improperCommand();
	}
#endif
	return 0;
}

void FishyNox::performPrimaryCommandAction(string command)
{
	switch(primaryCommandIndex)
	{
	case SET:
	{
		performSecondaryCommandAction(command);
	}
		break;
	case GET:
	{
		performSecondaryCommandAction(command,false);
	}
		break;
	case START_VIDEO:
//		printf("\ncase startVideo command is %s\n",command.c_str());
		startVideo(command);
		break;
	case STOP_VIDEO:
		stopVideo(command);
		break;
	case START_CAPTURE_VIDEO:
		startCaptureVideo(command);
		break;
	case STOP_CAPTURE_VIDEO:
		stopCaptureVideo(command);
		break;
	case START_CAPTURE_AUDIO:
		startCaptureAudio(command);
		break;
	case STOP_CAPTURE_AUDIO:
		stopCaptureAudio(command);
		break;
	case QUIT:
		quit = true;
		break;
	case USAGE:
		usage();
		break;
	default:
		break;
	}
}

void FishyNox::performSecondaryCommandAction(string command, bool callSet)
{
//	printf("\nDid it come here...??? %s\n",command.c_str());
	string sc = getFirstString(command);
	btrim(command);
	if(findSecondaryCommandSupported(sc))
	{
//		printf("\nSecondary command supported %s && %d\n",command.c_str(),scommandMap[secondaryCommandIndex].noOfArguments);
		//Write a generic interface to take te proper pointer and call set
		int v0=-1,v1=-1,v2=-1,v3=-1,i=0;
		if(callSet)
		{

			while( i < scommandMap[secondaryCommandIndex].noOfArguments)
			{
				string arg = getFirstString(command);
				switch(i)
				{
				case 0:
					v0 = atoi(arg.c_str());
					break;
				case 1:
					v1 = atoi(arg.c_str());
					break;
				case 2:
					v2 = atoi(arg.c_str());
					break;
				case 3:
					v3 = atoi(arg.c_str());
					break;
				default:
					printf("\nInvalid no of arguments\n");
					break;
				}
				i++;
				if(command.length() == 0)
					break;
			}
			if(command.length() || i != scommandMap[secondaryCommandIndex].noOfArguments)
			{
				printf("\n Improper no of args passed expected args:%d\n",scommandMap[secondaryCommandIndex].noOfArguments);
			}
			else
			{
				//call mxuvc api
				switch(scommandMap[secondaryCommandIndex].noOfArguments)
				{
				case 0:
				{
					if(scommandMap[secondaryCommandIndex].setPtr != NULL)
						((setmxcb0args)scommandMap[secondaryCommandIndex].setPtr)();
					else
						currentlyNotSupported();
					break;
				}
				case 1:
				{
					if(scommandMap[secondaryCommandIndex].setPtr != NULL)
						((setmxcb1args)scommandMap[secondaryCommandIndex].setPtr)((video_channel_t)v0);
					else
						currentlyNotSupported();
					break;
				}
				case 2:
				{
					if(scommandMap[secondaryCommandIndex].setPtr != NULL)
						((setmxcb2args)scommandMap[secondaryCommandIndex].setPtr)((video_channel_t)v0,(uint32_t)v1);
					else
						currentlyNotSupported();
					break;
				}
				case 3:
				{
					if(scommandMap[secondaryCommandIndex].setPtr != NULL)
						((setmxcb3args)scommandMap[secondaryCommandIndex].setPtr)((video_channel_t)v0,(uint32_t)v1,(uint32_t)v2);
					else
						currentlyNotSupported();
					break;
				}
				}
//				printf("\nargs values are %d-%d-%d-%d\n",v0,v1,v2,v3);
			}
		}
		else
		{
			//call get
			int v0=-1,v1=-1,v2=-1,v3=-1,i=0;
			int getParams = scommandMap[secondaryCommandIndex].noOfArguments -1;
			while( i < getParams)
			{
				string arg = getFirstString(command);
				switch(i)
				{
				case 0:
					v0 = atoi(arg.c_str());
					break;
				case 1:
					v1 = atoi(arg.c_str());
					break;
				case 2:
					v2 = atoi(arg.c_str());
					break;
				case 3:
					v3 = atoi(arg.c_str());
					break;
				default:
					printf("\nInvalid no of arguments\n");
					break;
				}
				i++;
				if(command.length() == 0)
					break;
			}
			if(command.length() || i != getParams)
			{
				printf("\n Improper no of args passed expected args:%d\n",getParams);
			}
			else
			{
				switch(scommandMap[secondaryCommandIndex].noOfArguments)
				{
				case 0:
				{
					if(scommandMap[secondaryCommandIndex].getPtr != NULL)
					{
						((getmxcb0args)scommandMap[secondaryCommandIndex].getPtr)();
					}
					else
						currentlyNotSupported();
					break;
				}
				case 1:
				{
					if(scommandMap[secondaryCommandIndex].getPtr != NULL)
					{
						((getmxcb1args)scommandMap[secondaryCommandIndex].getPtr)((video_channel_t)v0);
					}
					else
						currentlyNotSupported();
					break;
				}

				case 2:
				{
					if(scommandMap[secondaryCommandIndex].getPtr != NULL)
					{
						((getmxcb2args)scommandMap[secondaryCommandIndex].getPtr)((video_channel_t)v0,(uint32_t*)&v1);
						printf("\nValue of %s for ch %d is %d\n",scommandMap[secondaryCommandIndex].commandType,v0,v1);
					}
					else
						currentlyNotSupported();
					break;
				}
				case 3:
				{
					if(scommandMap[secondaryCommandIndex].getPtr != NULL)
					{
						((getmxcb3args)scommandMap[secondaryCommandIndex].getPtr)((video_channel_t)v0,(uint32_t*)&v1,(uint32_t*)&v2);
						printf("\nValue of %s for ch %d is %d:%d\n",scommandMap[secondaryCommandIndex].commandType,v0,v1,v2);
					}
					else
						currentlyNotSupported();
					break;
				}//case ends
				}//switch ends
			}
		}//get call ends
	}//secondary command sheck support ends
	else
	{
		//secondary command not supported
		printf("\nEnter proper set/get parameters\n");
	}

}
void FishyNox::currentlyNotSupported()
{
	printf("\nCurrently not supported\n");
}

void FishyNox::setParams(string command)
{
	int cmdType = -1,timer = 0,a,b,c,d;
	getMultipleCommandParams(command,&a,&b,&c,&d);
//	printf("\n command params %d :a %d : b %d :c %d : d %d",cmdType,a,b,c,d);
}

int FishyNox::findPrimaryCommandSupported(string val)
{
	for( map<int,string>::iterator ii=primaryCommandMap.begin(); ii!=primaryCommandMap.end(); ++ii)
	{
//	   cout << val<<" is " <<(*ii).first << ": " << (*ii).second << endl;
	   if(!val.compare((*ii).second))
	   {
		   primaryCommandIndex = (*ii).first;
		   return 1;
	   }
	}
	return 0;
}

int FishyNox::findSecondaryCommandSupported(string val)
{
	int i = 0;
	btrim(val);
	for( map<int,argumentStructure>::iterator ii=secondaryCommandMap.begin(); ii!=secondaryCommandMap.end(); ++ii)
	{
//	   cout <<val<< " "<< (*ii).first << ": " << scommandMap[i].commandType << endl;
	   if(!val.compare(scommandMap[i].commandType))
	   {
		   secondaryCommandIndex = (*ii).first;
		   return 1;
	   }
	   i++;
	}
	return 0;
}

void FishyNox::createPrimaryCommandArray()
{
	//Add the command from PrimaryCommands enum
	primaryCommandMap.insert(pair<int,string>(QUIT,"quit"));
	primaryCommandMap.insert(pair<int,string>(START_VIDEO,"start_video"));
	primaryCommandMap.insert(pair<int,string>(STOP_VIDEO,"stop_video"));
	primaryCommandMap.insert(pair<int,string>(START_CAPTURE_VIDEO,"start_capture_video"));
	primaryCommandMap.insert(pair<int,string>(STOP_CAPTURE_VIDEO,"stop_capture_video"));
	primaryCommandMap.insert(pair<int,string>(START_CAPTURE_AUDIO,"start_capture_audio"));
	primaryCommandMap.insert(pair<int,string>(STOP_CAPTURE_AUDIO,"stop_capture_audio"));
	primaryCommandMap.insert(pair<int,string>(SET,"set"));
	primaryCommandMap.insert(pair<int,string>(GET,"get"));
	primaryCommandMap.insert(pair<int,string>(USAGE,"usage"));
}

void FishyNox::createSecondaryCommandArray()
{
	int noOfRows = sizeof(scommandMap)/sizeof(scommandMap[0]);
	printf("\nInserting values %d\n",noOfRows);
	for(int i = 0; i< noOfRows;i++)
	{
		//i should be matching to values in SecondaryCommands enum;
		secondaryCommandMap.insert(pair<int,argumentStructure>(i,scommandMap[i]));
	}
}
