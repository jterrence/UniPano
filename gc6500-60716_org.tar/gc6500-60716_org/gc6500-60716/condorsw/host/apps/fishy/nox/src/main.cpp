/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
/*
 * main.c
 *
 *  Created on: Jan 21, 2014
 *      Author: sudhishk
 */
#include <getopt.h>

#include "fishynox.h"

#ifdef __cplusplus
extern "C"
{
#endif
#include "readline.h"
#include "histedit.h"
#ifdef __cplusplus
char * prompt(EditLine *e) {
  return "command>";
}

}
#endif

using namespace std;

static char *dev_name;

int readInput(char** argv, FishyNox &fishy)
{

#ifdef __cplusplus
	  /* This holds all the state for our line editor */
	  EditLine *el;

	  /* This holds the info for our history */
	  History *myhistory;

	  /* Temp variables */
	  int count;
	  const char *line;
	  int keepreading = 1;
	  HistEvent ev;

	  /* Initialize the EditLine state to use our prompt function and
	  emacs style editing. */

	  el = el_init(argv[0], stdin, stdout, stderr);
	  el_set(el, EL_PROMPT, &prompt);
	  el_set(el, EL_EDITOR, "emacs");

	  /* Initialize the history */
	  myhistory = history_init();
	  if (myhistory == 0) {
	    fprintf(stderr, "history could not be initialized\n");
	    return 1;
	  }

	  /* Set the size of the history */
	  history(myhistory, &ev, H_SETSIZE, 800);

	  /* This sets up the call back functions for history functionality */
	  el_set(el, EL_HIST, history, myhistory);

	  while (!fishy.quitApp()) {
	    /* count is the number of characters read.
	       line is a const char* of our command line with the tailing \n */
	    line = el_gets(el, &count);

	    /* In order to use our history we have to explicitly add commands
	    to the history */
	    if (count > 0) {
	      history(myhistory, &ev, H_ENTER, line);
//	      printf("You typed \"%s\"\n", line);
	    }
	    fishy.parseCommandArgs(line);
	  }


	  /* Clean up our memory */
	  history_end(myhistory);
	  el_end(el);


	  return 0;
#endif
}

static const char short_options[] = "d:vhl";
static const struct option
long_options[] = {
        { "device",     required_argument, NULL, 'd' },
        { "version",    no_argument,       NULL, 'v' },
        { "help",       no_argument,       NULL, 'h' },
        { "lua",        no_argument,       NULL, 'l' },
        { 0, 0, 0, 0 }
};

void help(FILE *fp, int argc, char **argv)
{
	fprintf(fp,
		 "Usage: %s [options]\n\n"
		 "Version %s\n"
		 "Options:\n"
		 "-v | --version       Display version\n"
		 "-d | --device name   Video device name [%s]\n"
		 "-h | --help          Print this message\n"
		 "-l | --lua filename  Lua script to run tests\n"
		 "",
		 argv[0],
		 FISHYNOX_VERSION,
		 dev_name);
}
int main(int argc, char *argv[])
{
	int lua_enable = 0;
	char * lua_arg = 0;
	dev_name = "0";

	for (;;) {
		int idx = 0;
		int c;

		c = getopt_long(argc, argv,
				short_options, long_options, &idx);

		if (-1 == c)
		{
			break;
		}
		switch (c) {
		case 0: /* getopt_long() flag */
			break;

		case 'v':
			printf("%s\n", FISHYNOX_VERSION);
			exit(EXIT_SUCCESS);
			break;

		case 'd':
			dev_name = optarg;
			break;

		case 'h':
			help(stdout, argc, argv);
			exit(EXIT_SUCCESS);

		case 'l':
			lua_enable = 1;
			lua_arg = optarg;
			break;

		default:
			help(stderr, argc, argv);
			exit(EXIT_FAILURE);
		}
	}
   if (optind <= 1) {
		fprintf(stderr, "Expected argument after options\n");
		help(stderr, argc, argv);
		exit(EXIT_FAILURE);
	}

	FishyNox fishy;
	fishy.engineInit(atoi(dev_name));
	readInput(argv,fishy);
	fishy.engineDeinit();
	return 1;
}
