/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
/*
 * fishynox.h
 *
 *  Created on: Jan 22, 2014
 *      Author: sudhishk
 */

#ifndef FISHYNOX_H_
#define FISHYNOX_H_
#include <string>
#include <stdlib.h>
#include <map>
#include "mxuvc.h"

using namespace std;

typedef int (*setmxcb0args) ();
typedef int (*setmxcb1args) (video_channel_t);
typedef int (*setmxcb2args) (video_channel_t,uint32_t);
typedef int (*setmxcb3args) (video_channel_t, uint32_t, uint32_t);

typedef int (*getmxcb0args) ();
typedef int (*getmxcb1args) (video_channel_t);
typedef int (*getmxcb2args) (video_channel_t,uint32_t*);
typedef int (*getmxcb3args) (video_channel_t, uint32_t*, uint32_t*);

typedef enum {
	NO_OF_ARGS=0,
	COMMAND_TYPE,
	SET_PTR,
	GET_PTR
}ARRAY_INDEX;

typedef struct
{
	int noOfArguments;
	char commandType[12];
	void (*setPtr);
	void (*getPtr);
}argumentStructure;

static argumentStructure scommandMap[]= {
		{ 3, "resolution",(void*)mxuvc_video_set_resolution,(void*)mxuvc_video_get_resolution},
		{ 2, "framerate", (void*)mxuvc_video_set_framerate,(void*)mxuvc_video_get_framerate},
		{ 2, "bitrate", (void*)mxuvc_video_set_bitrate,(void*)mxuvc_video_get_bitrate},
		{ 2, "gop", (void*)mxuvc_video_set_goplen,(void*)mxuvc_video_get_goplen},
		{ 1, "ch-count", NULL,(void*)mxuvc_video_get_channel_count},
		{ 2, "format", (void*)mxuvc_video_set_format,(void*)mxuvc_video_get_format},
		{ 1, "i-frame", NULL,(void*)mxuvc_video_force_iframe},
		{ 2, "brightness", (void*)mxuvc_video_set_brightness,(void*)mxuvc_video_get_brightness},
		{ 2, "contrast", (void*)mxuvc_video_set_contrast,(void*)mxuvc_video_get_contrast},
		{ 2, "hue", (void*)mxuvc_video_set_hue,(void*)mxuvc_video_get_hue},
		{ 2, "saturation", (void*)mxuvc_video_set_saturation,(void*)mxuvc_video_get_saturation},
		{ 2, "gain", (void*)mxuvc_video_set_gain,(void*)mxuvc_video_get_gain},
		{ 2, "zoom", (void*)mxuvc_video_set_zoom,(void*)mxuvc_video_get_zoom},
		{ 2, "pan", (void*)mxuvc_video_set_pan,(void*)mxuvc_video_get_pan},
		{ 2, "tilt", (void*)mxuvc_video_set_tilt,(void*)mxuvc_video_get_tilt},
		{ 3, "pan-tilt", (void*)mxuvc_video_set_pantilt,(void*)mxuvc_video_get_pantilt},
		{ 2, "gama", (void*)mxuvc_video_set_gamma,(void*)mxuvc_video_get_gamma},
		{ 2, "sharpness", (void*)mxuvc_video_set_sharpness,(void*)mxuvc_video_get_sharpness},
		{ 2, "profile", (void*)mxuvc_video_set_profile,(void*)mxuvc_video_get_profile},
		{ 2, "max-nal", (void*)mxuvc_video_set_maxnal,(void*)mxuvc_video_get_maxnal},
		{ 2, "v-flip", (void*)mxuvc_video_set_flip_vertical,(void*)mxuvc_video_get_flip_vertical},
		{ 2, "h-flip", (void*)mxuvc_video_set_flip_horizontal,(void*)mxuvc_video_get_flip_horizontal},
		{ 3, "wdr", (void*)mxuvc_video_set_wdr,(void*)mxuvc_video_get_wdr},
		{ 3, "exposure", (void*)mxuvc_video_set_exp,(void*)mxuvc_video_get_exp},
		{ 3, "zone-exposure", (void*)mxuvc_video_set_zone_exp,(void*)mxuvc_video_get_zone_exp},
		{ 2, "min-framerate", (void*)mxuvc_video_set_min_exp_framerate,(void*)mxuvc_video_get_min_exp_framerate},
		{ 2, "max-framesize", (void*)mxuvc_video_set_max_framesize,(void*)mxuvc_video_get_max_framesize},
		{ 2, "max-analog-gain", (void*)mxuvc_video_set_max_analog_gain,(void*)mxuvc_video_get_max_analog_gain},
		{ 2, "histogram", (void*)mxuvc_video_set_histogram_eq,(void*)mxuvc_video_get_histogram_eq},
		{ 2, "sharpen-filter", (void*)mxuvc_video_set_sharpen_filter,(void*)mxuvc_video_get_sharpen_filter},
		{ 2, "gain-multiplier", (void*)mxuvc_video_set_gain_multiplier,(void*)mxuvc_video_get_gain_multiplier},
		{ 2, "crop", (void*)mxuvc_video_set_crop,(void*)mxuvc_video_get_crop},
		{ 2, "vui", (void*)mxuvc_video_set_vui,(void*)mxuvc_video_get_vui},
		{ 2, "quality", (void*)mxuvc_video_set_compression_quality,(void*)mxuvc_video_get_compression_quality},
		{ 2, "level", (void*)mxuvc_video_set_avc_level,(void*)mxuvc_video_get_avc_level},
		{ 2, "ch-info", NULL,(void*)mxuvc_video_get_channel_info},
		{ 2, "pict-timing", (void*)mxuvc_video_set_pict_timing,(void*)mxuvc_video_get_pict_timing},
		{ 2, "gop-level", (void*)mxuvc_video_set_gop_hierarchy_level,(void*)mxuvc_video_get_gop_hierarchy_level},
		{ 3, "noise-filter", (void*)mxuvc_video_set_nf,(void*)mxuvc_video_get_nf},
		{ 3, "wb", (void*)mxuvc_video_set_wb,(void*)mxuvc_video_get_wb},
		{ 3, "zone-wb", (void*)mxuvc_video_set_zone_wb,(void*)mxuvc_video_get_zone_wb},
		{ 2, "pwr-line-freq", (void*)mxuvc_video_set_pwr_line_freq,(void*)mxuvc_video_get_pwr_line_freq},
		{ 2, "tf-strength", (void*)mxuvc_video_set_tf_strength,(void*)mxuvc_video_get_tf_strength}
};



class FishyNox {
	typedef enum {
		QUIT=0,
		START_VIDEO,
		STOP_VIDEO,
		START_CAPTURE_VIDEO,
		STOP_CAPTURE_VIDEO,
		START_CAPTURE_AUDIO,
		STOP_CAPTURE_AUDIO,
		SET,
		GET,
		USAGE
	}PrimaryCommands;

	typedef enum {
		RESOLUTION=0,
		FRAMERATE,
		BITRATE
	}SecondaryCommands;


public:
	FishyNox();
	virtual ~FishyNox();

	int engineInit(int devId);
	int engineDeinit();
	int parseCommandArgs(string command);
	bool quitApp(){return quit;}
private:
	int numchannels;
	bool quit;
	map<int,string> primaryCommandMap;
	map<int,argumentStructure> secondaryCommandMap;

	int primaryCommandIndex;
	int secondaryCommandIndex;

	void setParams(string command);
	void improperCommand();
	void usage();
	string getMultipleCommandParams(string command, int* p1, int *p2=0,int *p3=0,int *p4=0);
	string getsingleCommandParams(string command, int* id);
	void startCaptureVideo(string command);
	void stopCaptureVideo(string command);
	void startCaptureAudio(string command);
	void stopCaptureAudio(string command);

	void startVideo(string command);
	void stopVideo(string command);

	void createPrimaryCommandArray();
	void createSecondaryCommandArray();

	int findPrimaryCommandSupported(string val);
	int findSecondaryCommandSupported(string val);
	void performPrimaryCommandAction(string command);
	void performSecondaryCommandAction(string command, bool set=true);
	string getFirstString(string &command);

	void currentlyNotSupported();
};

#endif /* FISHYNOX_H_ */
