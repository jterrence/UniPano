/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include "stdafx.h"
#include "ISPTableItemDelegate.h"
#include "ISPTableWidget.h"

//////////////////////////////////////////////////////////////////////////
// Statics
ISPTableWidget *ISPTableWidget::m_focusedTableWidget = nullptr;




//////////////////////////////////////////////////////////////////////////
// QISPTableWidget

ISPTableWidget::ISPTableWidget(QList < QPair<double, double> > *minMaxList, QList< bool > *allowDecimalList, bool displayHexAsDecimal, QWidget *parent /*= 0*/)
	: QTableWidget(parent)
{
    m_delegate = new IspTableItemDelegate(minMaxList, allowDecimalList, displayHexAsDecimal);
	setItemDelegate(m_delegate);

	setAlternatingRowColors(true);
}

ISPTableWidget::~ISPTableWidget()
{
	delete m_delegate;
}

void ISPTableWidget::stopEdit()
{
	// Move the focus from the QLineEdit to this table widget; this commits the edit and removes the QLineEdit
    if (m_focusedTableWidget && (m_focusedTableWidget->state() == QAbstractItemView::State::EditingState)) {
		m_focusedTableWidget->setFocus();
    }
}

void ISPTableWidget::focusInEvent(QFocusEvent *)
{
	m_focusedTableWidget = this;
}

void ISPTableWidget::changeEvent(QEvent * event)
{
	// When table is disabled, remove zebra striping and de-select current cell,
	// to make table seem more grayed out
	if (event->type() == QEvent::EnabledChange)
	{
		bool enabled = isEnabled();
		if (!enabled)
			setCurrentCell(-1, -1);
		
		setAlternatingRowColors(enabled);
	}
}
