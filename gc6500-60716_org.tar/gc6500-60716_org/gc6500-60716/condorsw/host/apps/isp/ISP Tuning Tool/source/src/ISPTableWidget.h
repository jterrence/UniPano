/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#pragma once

#include <qobject.h>
#include <qtablewidget.h>

class QFocusEvent;
class IspTableItemDelegate;


/*
 A QTableWidget that has ISP Gui functionality:
	1) stopEdit() function is called by toolbar handlers to commit pending edits when toolbar button is pressed.
	   (Unfortunately Qt does not commit the edit when the toolbar button is pressed.)

	2) Automatically uses the custom IspTableItemDelegate.
*/
class ISPTableWidget : public QTableWidget
{
	Q_OBJECT

public:
    ISPTableWidget(QList < QPair<double, double> > *minMaxList, QList< bool > *allowDecimalList, bool displayHexAsDecimal = false, QWidget *parent = 0);
	~ISPTableWidget();

	// Save any current edits going on in a table cell, then stop the edit
	// Called by toolbar buttons since edits are not stopped automatically or saved when the toolbar button is pressed
	static void stopEdit();

protected:
		virtual void focusInEvent(QFocusEvent *) Q_DECL_OVERRIDE;
		virtual void changeEvent(QEvent * event) Q_DECL_OVERRIDE;

private:
	static ISPTableWidget	*m_focusedTableWidget;

	IspTableItemDelegate	*m_delegate;
};
