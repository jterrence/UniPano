#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================

import sys
import platform
import subprocess
import os, shutil
import time
import json
from subprocess import CalledProcessError
import argparse
from utils import fix_ownership
from xmodem import XMODEM, XMODEM1k, CRC, EOT, NAK
import logging

logging.basicConfig(level=logging.DEBUG, format='%(message)s')

try:
    import serial
    from serial.tools import list_ports
except ImportError:
    print >> sys.stderr, "Error: PySerial module missing. Please install from PySerial 2.7 from https://pypi.python.org/pypi/pyserial/2.7"
    sys.exit(1)

def usage():
	print "Usage: rawcap.py <device-type> <firmware> <raw-cap-json> <raw-cap-exp> <raw-cap-gain> <raw-cap-flip> <width> <height> <address> <output>"
	sys.exit(1)

# Serial port
port = None

# Function to read data from the serial port until specified character is encountered
def readUntil(char = None):
    def serialPortReader():
        while True:
            tmp = port.read(1)
            if not tmp or (char and char == tmp):
                break
            yield tmp
    return ''.join(serialPortReader())

# Getter function used by XModem class to receive data   
def getc(size, timeout=1):
	try:
		return port.read(size)
	except serial.serialutil.SerialException as ex:
		print >> sys.stderr, ex
		sys.exit(1)

# Setter function used by XModem class to write data
def putc(data, timeout=1):
	try:
		return port.write(data)
	except serial.serialutil.SerialException as ex:
		print >> sys.stderr, ex
		sys.exit(1)

if __name__ == "__main__":
	
	parser = argparse.ArgumentParser()
	subparsers = parser.add_subparsers(dest='device_type')
	
	parser_gc65xx = subparsers.add_parser('gc65xx')
	parser_gc65xx.add_argument('--raw-firmware', nargs="?")
	parser_gc65xx.add_argument('--raw-json', nargs="?")
	parser_gc65xx.add_argument('--address', nargs="?")
	parser_gc65xx.add_argument('--exposure', nargs="?")
	parser_gc65xx.add_argument('--gain', nargs="?")
	parser_gc65xx.add_argument('--flip', nargs="?")
	parser_gc65xx.add_argument('--width', nargs="?")
	parser_gc65xx.add_argument('--height', nargs="?")
	parser_gc65xx.add_argument('--bpp', nargs="?")
	parser_gc65xx.add_argument('--bayer-pattern', nargs="?")
	parser_gc65xx.add_argument('--output-name', nargs="?")
	parser_gc65xx.add_argument('--raw-capture', nargs="?", default="1", const="1")
	
	parser_gw4xx = subparsers.add_parser('gw4xx')
	parser_gw4xx.add_argument('--baudrate', nargs="?", default='115200')
	parser_gw4xx.add_argument('--x', nargs="?", default='0')
	parser_gw4xx.add_argument('--y', nargs="?", default='0')
	parser_gw4xx.add_argument('--width', nargs="?", default='1280')
	parser_gw4xx.add_argument('--height', nargs="?", default='720')
	parser_gw4xx.add_argument('--bpp', nargs="?")
	parser_gw4xx.add_argument('--bayer-pattern', nargs="?")
	parser_gw4xx.add_argument('--output-name', nargs="?")
	parser_gw4xx.add_argument('--raw-capture', nargs="?", default="1", const="1")
	
	parser_gw3xx = subparsers.add_parser('gw3xx')
	parser_gw3xx.add_argument('--baudrate', nargs="?", default='115200')
	parser_gw3xx.add_argument('--x', nargs="?", default='0')
	parser_gw3xx.add_argument('--y', nargs="?", default='0')
	parser_gw3xx.add_argument('--width', nargs="?", default='1280')
	parser_gw3xx.add_argument('--height', nargs="?", default='720')
	parser_gw3xx.add_argument('--bpp', nargs="?")
	parser_gw3xx.add_argument('--bayer-pattern', nargs="?")
	parser_gw3xx.add_argument('--output-name', nargs="?")
	parser_gw3xx.add_argument('--raw-capture', nargs="?", default="1", const="1")
	
	args = parser.parse_args()
	
	if args.width is None or \
		args.height is None or \
		args.bpp is None or \
		args.bayer_pattern is None or \
		args.output_name is None:
		print >> sys.stderr, "Error: Missing arguments"
		if args.device_type == 'gc65xx':
			parser_gc65xx.print_usage()
		elif args.device_type == 'gw4xx':
			parser_gw4xx.print_usage()
		elif args.device_type == 'gw3xx':
			parser_gw3xx.print_usage()
		sys.exit(1)
	
	output_dir = "raw-output"
	if not os.path.isdir(output_dir):
		os.makedirs(output_dir)
	
	shell_exec = platform.system() == 'Linux'

	if args.device_type == 'gw4xx' or args.device_type == 'gw3xx':
		
		output_file = os.path.join(output_dir, args.output_name)
		dump_file = r'{0}.{1}'.format(output_file, 'dump' if args.device_type == 'gw4xx' else 'raw')
		raw_file = r'{0}.raw'.format(output_file)
		tiff_file = r'{0}.tiff'.format(output_file)
		
		if args.raw_capture == "0":
			if not os.path.isfile(raw_file):
				print >> sys.stderr, "Raw dump file missing:", raw_file
				sys.exit(1)
		else:
			# Find the serial port
			comports = []
			for portnum, desc, hwid in list_ports.comports():
				if desc.find('USB Serial Port') > -1 or hwid.find('0403:6010') > -1:
					comports.append(portnum)
			
			if len(comports) is 0:
				print >> sys.stderr, "Error: No device detected"
				sys.exit(1)
				
			comports.sort(reverse=True)
			comport = comports[0]
			print >> sys.stdout, "Using port", comport
			sys.stdout.flush()

			try:
				port = serial.Serial(port=comport, parity=serial.PARITY_NONE, bytesize=serial.EIGHTBITS, stopbits=serial.STOPBITS_ONE, timeout=0, xonxoff=0, rtscts=0, dsrdtr=0, baudrate=args.baudrate)
			except serial.serialutil.SerialException as ex:
				print >> sys.stderr, ex
				sys.exit(1)

			# Trigger the raw capture
			trigger_command = r'~x,{0},{1},{2},{3}#\r\n'.format(args.x, args.y, args.width, args.height)
			port.write(trigger_command)
			time.sleep(2) # give device time to handle command
			consoleLogs = readUntil(EOT)
	
			# Read the pitch from the console logs
			lines = consoleLogs.split('\n')
			pitch = 0
			for line in lines:
				line = line.strip()
				if len(line) is 0:
					continue

				# Get the pitch
				if line.startswith('Pitch'):
					pitch = line[line.index("=")+1:]
					break
				elif line.startswith('Image Pitch'):
					pitch = line[line.index("=")+2:]
					break
	
			if pitch is 0:
				print >> sys.stderr, "Error: Failed to trigger raw capture. Try restarting your device or booting with the correct firmware."
				sys.exit(1)
				
			print >> sys.stdout, 'Pitch:', pitch
			sys.stdout.flush()
				
			# Set the read/write timeout
			port.timeout = 5
			
			stream = open(dump_file, 'wb')
			start = int(time.time())
	
			# XModem receive
			numbytes = XMODEM1k(getc, putc).recv(stream, crc_mode = 0, quiet = 1)
			end = int(time.time())
			stream.close()
			port.close()

			if numbytes is None:
				sys.exit(0)
	
			min = (end-start)/60
			sec = (end-start)%60
			if min > 0:
				print 'Time taken:', '{0} min {1} sec'.format(min, sec)
			else:
				print 'Time taken:', '{0} sec'.format(sec)
			sys.stdout.flush()

			if args.device_type == 'gw4xx':
				# Convert w4 dump to raw format
				w4dump2raw_script = "./scripts/W4dump2raw.py"
				if not os.path.isfile(w4dump2raw_script):
					print >> sys.stderr, "Error: Could not find W4dump2raw.py"
					sys.exit(1)
		
				command = r'python "{0}" "{1}" "{2}" {3}'.format(w4dump2raw_script, dump_file, raw_file, pitch)
				if subprocess.call(command, shell=shell_exec) is not 0:
					print >> sys.stderr, "Error: Failed to generate raw dump"
					sys.exit(1)

		# Raw file obtained. Convert to RGB tiff
		bayer2rgb = r'./bin/{0}/bayer2rgb'.format(args.device_type)
		if platform.system() == 'Windows':
			bayer2rgb = bayer2rgb + '.exe'

		command = r'{0} -i "{1}" -o "{2}" -w {3} -v {4} -b {5} -f {6} -t'.format(bayer2rgb, raw_file, tiff_file, args.width, args.height, args.bpp, args.bayer_pattern)
		if subprocess.call(command, shell=shell_exec) is not 0:
			print >> sys.stderr, "Error: Failed to convert bayer to rgb tiff. Please check the width / height / bits per pixel."
			sys.exit(1)

		print tiff_file
		sys.stdout.flush()
		
		if args.raw_capture == '1':
			# Reset the board
			try:
				print 'Resetting device...'
				sys.stdout.flush()
				subprocess.check_output(r'python ./scripts/resetCamera.py {0}'.format(args.device_type), shell=shell_exec)
			except subprocess.CalledProcessError as cpe:
				print >> sys.stderr, cpe.output.strip()

	elif args.device_type == 'gc65xx':
		
		if args.raw_firmware is None or \
			args.raw_json is None or \
			args.address is None or \
			args.exposure is None or \
			args.gain is None or \
			args.flip is None:
			print >> sys.stderr, "Error: Missing arguments"
			parser_gc65xx.print_usage()
			sys.exit(1)
		
		width = int(args.width)
		height = int(args.height)
		
		segment_width = 256
		segment_height = 32
		
		total_width = (width * 2 + segment_width - 1) & ~(segment_width - 1)
		total_height = (height + segment_height - 1) & ~(segment_height - 1)
		
		data_size = total_width * total_height
		
		mxcam = r'./bin/gc65xx/mxcam'
		memrw = r'./bin/gc65xx/memrw'
		l2f = r'./bin/gc65xx/l2f'
		bayer2rgb = r'./bin/gc65xx/bayer2rgb'
		
		if platform.system() == 'Windows':

			mxcam = mxcam + '.exe'
			memrw = memrw + '.exe'
			l2f = l2f + '.exe'
			bayer2rgb = bayer2rgb + '.exe'
			
		output_file = os.path.join(output_dir, args.output_name)
		y_file = output_file + '.y'
		raw_file = output_file + '.raw'
		tiff_file = output_file + '.tiff'

		if args.raw_capture == "1":
			try:
	
				# Read and save the existing camera configuration
				camera_config = subprocess.check_output(r'{0} readcfg'.format(mxcam), shell=shell_exec, stderr=subprocess.STDOUT)
				if camera_config.startswith("read"):
					camera_booted = True
				else:
					if camera_config.lower().find('no compatible device found') > -1:
						print >> sys.stderr, camera_config
						sys.exit(1)
					camera_booted = False
	
			except CalledProcessError as cpe:
				print ("Error: return code: {0}, output: {1}".format(cpe.returncode, cpe.output))
				sys.exit(1)
			
			if camera_booted:
				print >> sys.stderr, "Read the existing camera configuration. The camera will be reset back to this after raw capture"
	
				# Save the configuration read from the camera
				fp = open("{0}/camera_config.json".format(output_dir), "w")
				fp.write(camera_config[camera_config.find("{"):camera_config.rfind("}")+1])
				fp.close()
	
				# Read and save the existing camera isp configuration
				if subprocess.call(r'{0} readispcfg {1}/camera_isp.bin'.format(mxcam, output_dir), shell=shell_exec) != 0:
					print >> sys.stderr, "Error: Failed to read existing isp configuration."
	
				# Reset camera
				subprocess.call(r'{0} reset'.format(mxcam), shell=shell_exec)
			
				time.sleep(1)
	
			# Update the raw capture json
			fp = open(args.raw_json)
			json_object = json.load(fp)
			fp.close()
	
			if not json_object.has_key('system'):
				print >> sys.stderr, 'Invalid JSON for raw capture: "{0}"'.format(args.raw_json)
				sys.exit(1)
	
			json_object['system']['SENSOREXP'] = args.exposure
			json_object['system']['SENSOR_GAIN'] = args.gain
			json_object['system']['SENSORFLIP'] = args.flip
			raw_cap_json = os.path.join(output_dir, "_rawcap.json")
	
			fp = open(raw_cap_json, "w")
			json.dump(json_object, fp)
			fp.close()
	
			print >> sys.stderr, "Booting camera with raw capture json"
			command = r'{0} boot "{1}" "{2}"'.format(mxcam, args.raw_firmware, raw_cap_json)
			if subprocess.call(command, shell=shell_exec) is not 0:
			    sys.exit(1)
	
			time.sleep(1)
	
			# Function to reset the board back to its previous configuration
			def reset_board(reset=True):

				if reset:
					subprocess.call(r'{0} reset'.format(mxcam), shell=shell_exec)
	
				if camera_booted:
	
					print >> sys.stderr, "Reset camera back to its previous configuration"
					time.sleep(1)
	
					command = r'{0} boot "{1}" "{2}/camera_config.json" "{2}/camera_isp.bin"'.format(mxcam, args.raw_firmware, output_dir)
					if subprocess.call(command, shell=shell_exec) is not 0:
						sys.exit(1)
	
					if os.path.isfile(os.path.join(output_dir, "camera_isp.bin")):
						os.unlink(os.path.join(output_dir, "camera_isp.bin"))
	
					os.unlink(os.path.join(output_dir, "camera_config.json"))
	
	
			print >> sys.stderr, "Getting the raw dump from the camera and converting to TIFF"
	
			command = r'{0} 0 {1} -f {2} -m r -d {3}'.format(memrw, args.address, y_file, data_size)
			if subprocess.call(command, shell=shell_exec) is not 0:
				print >> sys.stderr, "Error: Failed to get raw dump"
				reset_board()
				sys.exit(1)
				
			if platform.system() == "Windows":
				# Reset board as a workaround for l2f. qhal_qcc_read does not work as expected on windows
				subprocess.call(r'{0} reset'.format(mxcam), shell=shell_exec)
				time.sleep(1)
	
			command = r'{0} {1} {2} {3} 0 0 {4}'.format(l2f, raw_file, width*2, height, output_file)
			if subprocess.call(command, shell=shell_exec) is not 0:
				print >> sys.stderr, "Error: Failed to convert raw dump to bayer"
				reset_board(platform.system() != "Windows")
				sys.exit(1)
			
			# cleanup
			os.unlink(raw_cap_json)
			reset_board(platform.system() != "Windows")
		else:
			if not os.path.isfile(raw_file):
				print >> sys.stderr, "Raw dump file missing:", raw_file
				sys.exit(1)
		    
		command = r'{0} -i {1} -o {2} -w {3} -v {4} -b {5} -f {6} -t'.format(bayer2rgb, raw_file, tiff_file, width, height, args.bpp, args.bayer_pattern)
		if subprocess.call(command, shell=shell_exec) is not 0:
			print >> sys.stderr, "Error: Failed to convert bayer to rgb tiff. Please check the width / height / bits per pixel."
			reset_board()
			sys.exit(1)

		print >> sys.stdout, tiff_file
		sys.stdout.flush()

	else:
		print ("Error: unrecognized device.")
		sys.exit(1)

	if platform.system() == 'Linux':
		fix_ownership(output_dir)

	sys.exit(0)
