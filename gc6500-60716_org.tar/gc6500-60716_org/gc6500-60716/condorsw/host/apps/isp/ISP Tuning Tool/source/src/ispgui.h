/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef ISPGUI_H
#define ISPGUI_H

#include "ui_ispgui.h"
#include "histogram/imageinfoscene.h"

class QLabel;
class QJsonDocument;
class ISPTableWidget;
class ConnectionManager;

class ISPGui : public QMainWindow
{
	Q_OBJECT

public:
	ISPGui(QWidget *parent = 0);
	~ISPGui();

    enum TargetDevice
    {
        GW4XX,
        GC65XX,
        GW3XX,

        // Number of devices
        NUMDEVICES
    };

protected:
    void closeEvent(QCloseEvent *);
    void resizeEvent(QResizeEvent *);

private:

	enum LaunchedApp
	{
		IspGui,
		ConnectCamera,
		DisconnectCamera,
		GetCurrentCamera,
		SetCurrentCamera,
		ResetCamera,
		CommitFlash,
		ReadSensorEV,
        FlipV,
        FlipH,
        RawCapture,
        SaveBinary,
        BayerToRgb,

		// Must be last
		NUMAPPS
	};

    enum ConnectionType
    {
        UART,
        BASIC_UART,
        I2C,
        USB,

        // Number of connection types
        NUMCONN
    };

	QString launchedAppName(LaunchedApp appType)
	{
		static const char *appNames[] = {
			"isp_gui",
			"connect_camera",
			"disconnect_camera",
			"get_camera",
			"set_camera",
			"reset_camera",
			"commit_flash",
            "read_sensor_ev",
            "flip_vertical",
            "flip_horizontal",
            "raw_capture",
            "save_binary",
            "bayer_to_rgb"
		};

		return appNames[appType];
	}

    QString targetDeviceName(TargetDevice device)
    {
        static const char* deviceNames[] = {
            "GW4XX",
            "GC65XX",
            "GW3XX"
        };

        return deviceNames[device];
    }

    ISPGui::TargetDevice targetDevice(QString deviceName)
    {
        for (int device = 0; device < NUMDEVICES; device++)
        {
            if (targetDeviceName((TargetDevice)device).compare(deviceName, Qt::CaseInsensitive) == 0)
            {
                return (TargetDevice)device;
            }
        }

        return GW4XX;
    }

    QString targetDeviceArg(TargetDevice device)
    {
        static const char* deviceArgs[] = {
            "-w 4",
            "",
            "-w 3"
        };

        return deviceArgs[device];
    }

    QString connectionTypeName(ConnectionType type)
    {
        static const char* connectionTypes[] = {
            "UART",
            "Basic UART",
            "I2C",
            "USB"
        };

        return connectionTypes[type];
    }

    ISPGui::ConnectionType connectionType(QString name)
    {
        for (int conn = 0; conn < NUMCONN; conn++)
        {
            if (connectionTypeName((ConnectionType)conn).compare(name, Qt::CaseInsensitive) == 0)
            {
                return (ConnectionType)conn;
            }
        }

        return UART;
    }

    QString connectionTypeArg(ConnectionType type)
    {
        static const char* connectionTypesArgs[] = {
            "-u",
            "-U %1",
            "-i",
            ""
        };

        return connectionTypesArgs[type];
    }

    void logMsg(QString msg, LaunchedApp appType = IspGui, bool updateLastLine = false);
    void launchApp(LaunchedApp appType);
	void launchApp(const QString &appPath, QStringList &arguments, LaunchedApp appType);

    bool loadSpecFile();
    void forceLoadConfigFile(QString message);
    void forceLoadSpecFile(QString message);
	void loadConfigFile();
	void clearSubsections();
	bool addSubsections(QJsonObject subsections);
	void stuffConfig(QJsonObject settings);
    QJsonValue findJsonValue(QString key, QJsonObject obj);
    void stuffTable(ISPTableWidget* table, QJsonArray arr);

    /**
     * @brief Clear the currently opened config's values and disable all controls
     *
     * Calling stuffConfig() will fill and enable the required controls
     */
    void clearConfig();

	bool saveConfigFile(const QString &configFilePath, bool writeUnchangedParameters);

    /**
     * @brief Save user settings like connection type, etc, to persitent storage
     */
    void saveUserSettings();

    /**
     * @brief Load user settings like connection type, etc, from persitent storage
     */
    void loadUserSettings();

	void writeJSONSubsection(bool &wroteSubsection, const QString &subsectionName, QString &json);
    bool initDevJson();

    /**
     * @brief Populate the ui components of the setup page
     *        1. Populate the "target device" combobox
     *        2. Populate the "connection type" combobox
     */
    void populateDeviceTypeComboBox();

    void populateConnectionTypeComboBox();

    void updateConnectionTypeConfigBox();

	void replaceMacros(QString &specFilePath);

	virtual bool eventFilter(QObject *obj, QEvent *event);

	void parseMinMax(const QJsonValue &minObject, const QJsonValue &maxObject, const QString &controlType,
                    bool &controlHex, double &controlMin, double &controlMax,
					const QString &paramDescription, QString &tooltipString);

    void formatTooltip(const QString &controlType, bool controlHex, double controlMin, double controlMax,
					   const QString &paramDescription, QString &tooltipString);

	void normalizeLabelForControl(QWidget *control);

	bool completeTables();

    /**
     * @brief validate the color matrix tables for:
     * 1. White point preservation
     * 2. CCM color temperature ascending values if adaptccmen = 1
     * @return True if valid, False otherwise
     */
    bool validateColorTables();

    /**
     * @brief Update the raw capture screen based on the target device
     */
    void updateRawCaptureUI();

    /**
     * @brief Check if the current config's minor and major version matches the spec
     * @return True if versions match, False otherwise
     */
    bool validateConfigVersion();

    /**
     * @brief Check if the user has made any modifications in the config
     * @return True if any unsaved changes are present. False otherwise
     */
    bool configIsModified();

    /**
     * @brief Set the "originalValue" property of all controls to the current value,
     *        so that we know all values have been set to the camera.
     */
    void resetOriginalValues();

    bool checkConnectedDevices(bool showErrorDialog = true);

    /**
     * @brief Reset all controls. Clear all values and disable all controls
     */
    void resetUI();

	QString			m_helpFilePath;
	QString			m_specFilePath;
    QString         m_defaultSpecFilePath;
	int				m_specVersion;
    int             m_specMajorVersionMin;
    int             m_specMinorVersionMin;
    int             m_specMajorVersionMax;
    int             m_specMinorVersionMax;
	QString			m_configFilePath;
    QString         m_configFilePathOld;
	QLabel			*m_pVersionLabel;
    QJsonDocument	*m_devJsonDoc;

    int				m_configMajorVersion;
    int				m_configMinorVersion;

    int             m_currentTargetDevice;

    QString         m_tableFilePath;

    ImageInfoScene *scene;

    bool            m_binaryLoaded;
    ConnectionManager *m_connectionManager;

    bool raw_capture;

	Ui::ISPGuiClass ui;

signals:
    /**
     * @brief Signal to indicate whether a backend app script has completed
     *
     * @param success True if the script executed successfully, False otherwise
     */
    void launchComplete(bool success);
};

#endif // ISPGUI_H
