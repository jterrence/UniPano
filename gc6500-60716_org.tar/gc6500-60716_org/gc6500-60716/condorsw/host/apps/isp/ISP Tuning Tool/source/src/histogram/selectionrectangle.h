/*******************************************************************************
*
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this
* copyright notice.
*
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
*
*******************************************************************************/

#ifndef SELECTIONRECTANGLE_H
#define SELECTIONRECTANGLE_H

#include <QGraphicsItem>
#include <QGraphicsRectItem>
#include <QPen>

class SelectionRectangle : public QObject, public QGraphicsRectItem
{
    Q_OBJECT

public:
    SelectionRectangle(const QRectF &rect, QGraphicsItem *parent = 0);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0) Q_DECL_OVERRIDE;
    QRectF boundingRect() const Q_DECL_OVERRIDE;
    QPainterPath shape() const Q_DECL_OVERRIDE;

    /**
     * @brief Set the boundary limits for this rectangle to move. The rectangle will not be able to move beyond this region.
     * @param limit The boundary rectangle
     */
    void setMoveLimit( QRectF limit );

    // Inline public functions
    inline qreal width() const
    {
        return mWidth;
    }
    inline void setWidth(qreal width)
    {
        mWidth = width;
    }
    inline qreal height() const
    {
        return mHeight;
    }
    inline void setHeight(qreal height)
    {
        mHeight = height;
    }
    inline bool isResizing()
    {
        return mResizing;
    }
    inline bool isResizingLeft()
    {
        return mResizeZoneL;
    }
    inline bool isResizingRight()
    {
        return mResizeZoneR;
    }
    inline bool isResizingUp()
    {
        return mResizeZoneU;
    }
    inline bool isResizingDown()
    {
        return mResizeZoneD;
    }
    int buffer() const;
    int border() const;

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void hoverMoveEvent(QGraphicsSceneHoverEvent *event);
    QVariant itemChange(GraphicsItemChange change, const QVariant &value);

signals:

public slots:

private:
    bool mResizing;
    bool mResizeZoneL;
    bool mResizeZoneR;
    bool mResizeZoneU;
    bool mResizeZoneD;

    /**
     * @brief Boundary within which this rectangle can be moved
     */
    QRectF mLimit;

    qreal mWidth, mHeight;

    /**
     * @brief Boolean indicating whether the mouse is pressed on this rectangle
     */
    bool mMousePressed;

    /**
     * @brief Pen used to draw this rectangle
     */
    QPen pen;
};

#endif // SELECTIONRECTANGLE_H
