/*******************************************************************************
*
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this
* copyright notice.
*
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
*
*******************************************************************************/

#include <QGraphicsSceneMouseEvent>
#include <QGraphicsView>
#include <QPixmap>
#include <QDebug>
#include "imageinfoscene.h"
#include "selectionrectangle.h"

// Minimum rectangle size
//const qreal MIN_RECT_SIZE = 10.0;

ImageInfoScene::ImageInfoScene(int width, int height, QObject *parent) :
    QGraphicsScene(parent)
  , mPixmapItem(NULL)
{
    setSceneRect( QRectF(0, 0, width, height) );
}

ImageInfoScene::~ImageInfoScene()
{
    if (mPixmapItem)
    {
        delete mPixmapItem;
    }

    if (rectangles.count() > 0)
    {
        rectangles.clear();
    }
}

void ImageInfoScene::clearScene()
{
    if (mPixmapItem != NULL)
    {
        removeItem( mPixmapItem );
        removeSelectionRectangles();
        mPixmapItem = NULL;
        update();
    }
}

void ImageInfoScene::setImage(QString image, QRectF size)
{
    setSceneRect( size );

    if ( image.length() )
    {
        QPixmap pixmap( image );
        if (mPixmapItem == NULL)
        {
            mPixmapItem = addPixmap( pixmap.scaled(sceneRect().width(), sceneRect().height(), Qt::KeepAspectRatio) );
            mPixmapItem->setZValue(-1);
        }
        else
        {
            mPixmapItem->setPixmap( pixmap.scaled(sceneRect().width(), sceneRect().height(), Qt::KeepAspectRatio) );
        }
    }
    else
    {
        if (mPixmapItem != NULL)
        {
            mPixmapItem->setPixmap( mPixmapItem->pixmap().scaled(sceneRect().width(), sceneRect().height(), Qt::KeepAspectRatio) );
        }
    }

    if (mPixmapItem != NULL)
    {
        mPixmapItem->setPos( sceneRect().width()/2 - mPixmapItem->pixmap().width()/2, sceneRect().height()/2 - mPixmapItem->pixmap().height()/2 );

        if (rectangles.count() > 0)
        {
            for (int index = 0; index < rectangles.count(); index++)
            {
                SelectionRectangle *rectangle = rectangles.at( index );
                QRectF limit = QRectF(mPixmapItem->pos().x(), mPixmapItem->pos().y(), mPixmapItem->pixmap().width(), mPixmapItem->pixmap().height());
                rectangle->setMoveLimit( limit );

                // Reposition the rectangle if it lies outside the pixmap
                if (!limit.contains( QRectF(rectangle->pos(), QSizeF(rectangle->width(), rectangle->height())) ))
                {
                    qreal x = (limit.x()+limit.width())/2 - rectangle->width()/2;
                    qreal y = (limit.y()+limit.height())/2 - rectangle->height()/2;
                    rectangle->setPos(x, y);
                }
            }

            calculateHistogramMean();
        }
    }
}

void ImageInfoScene::addSelectionRectangles(int num, qreal width, qreal height)
{
    if (rectangles.length() > 0)
    {
        return;
    }
    if (num == 1)
    {
        SelectionRectangle *rectangle = new SelectionRectangle (QRectF(0, 0, width, height));
        rectangle->setRect(0, 0, width, height);
        if (mPixmapItem != NULL)
        {
            qreal x = mPixmapItem->pixmap().width()/2 - width/2;
            qreal y = mPixmapItem->pixmap().height()/2 - height/2;
            rectangle->setPos(x, y);
            rectangle->setMoveLimit( QRectF(mPixmapItem->pos().x(), mPixmapItem->pos().y(), mPixmapItem->pixmap().width(), mPixmapItem->pixmap().height()) );
        }
        else
        {
            qreal x = sceneRect().width()/2 - width/2;
            qreal y = sceneRect().height()/2 - height/2;
            rectangle->setPos(x, y);
            rectangle->setMoveLimit(QRectF(0, 0, sceneRect().width(), sceneRect().height()));
        }
        rectangles.append(rectangle);
        addItem(rectangle);
        rectangle->setSelected(true);
    }
}

void ImageInfoScene::removeSelectionRectangles()
{
    if (rectangles.count() > 0)
    {
        for (int index = 0; index < rectangles.count(); index++)
        {
            removeItem( rectangles.at(index) );
        }
    }

    rectangles.clear();
}

void ImageInfoScene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    mDragStartPosition = event->scenePos();
    QGraphicsScene::mousePressEvent(event);
}

void ImageInfoScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->buttons() == Qt::LeftButton)
    {
        SelectionRectangle *rectangle;
        for (int index = 0; index < rectangles.length(); index++)
        {
            rectangle = rectangles.at(index);

            if (rectangle->isResizing())
            {
                // Handle rectangle resizing

                mDragEndPosition = event->scenePos();
                qreal xDiff = mDragEndPosition.x()-mDragStartPosition.x();
                qreal yDiff = mDragEndPosition.y()-mDragStartPosition.y();

                if (rectangle->isResizingRight())
                {
                    qreal width = rectangle->width() + xDiff;

                    /*if (width < MIN_RECT_SIZE)
                    {
                        width = MIN_RECT_SIZE;
                    }*/

                    rectangle->setWidth( width );
                }

                if (rectangle->isResizingUp())
                {
                    qreal height = rectangle->height() - yDiff;
                    qreal y =  rectangle->scenePos().y() + yDiff;

                    /*if (height < MIN_RECT_SIZE)
                    {
                        y += (height - MIN_RECT_SIZE);
                        height = MIN_RECT_SIZE;
                    }*/

                    rectangle->setPos( rectangle->scenePos().x(), y);
                    rectangle->setHeight( height );
                }

                if (rectangle->isResizingLeft())
                {
                    qreal width = rectangle->width() - xDiff;
                    qreal x = rectangle->scenePos().x() + xDiff;

                    /*if (width < MIN_RECT_SIZE)
                    {
                        x += (width - MIN_RECT_SIZE);
                        width = MIN_RECT_SIZE;
                    }*/

                    rectangle->setPos( x, rectangle->scenePos().y());
                    rectangle->setWidth( width );
                }

                if (rectangle->isResizingDown())
                {
                    qreal height = rectangle->height() + yDiff;

                    /*if (height < MIN_RECT_SIZE)
                    {
                        height = MIN_RECT_SIZE;
                    }*/

                    rectangle->setHeight( height );
                }

                mDragStartPosition = mDragEndPosition;

                // Update the scene to redraw the rectangle and clear any artifacts left over after resizing
                this->update();
            }
        }
    }

    QGraphicsScene::mouseMoveEvent(event);
}

void ImageInfoScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{    
    // Correct the rectangle's size and position if the width/height is negative, i.e,
    // the user resized in the opposite direction
    for (int index = 0; index < rectangles.length(); index++)
    {
        SelectionRectangle *rectangle = rectangles.at(index);
        if (rectangle->width() < 0)
        {
            rectangle->setPos(rectangle->scenePos().x()+rectangle->width(), rectangle->scenePos().y());
            rectangle->setWidth(-rectangle->width());

            this->update();
        }

        if (rectangle->height() < 0)
        {
            rectangle->setPos(rectangle->scenePos().x(), rectangle->scenePos().y()+rectangle->height());
            rectangle->setHeight(-rectangle->height());

            this->update();
        }
    }

    calculateHistogramMean();
    QGraphicsScene::mouseReleaseEvent(event);
}

void ImageInfoScene::calculateHistogramMean()
{
    for (int index = 0; index < rectangles.length(); index++)
    {
        SelectionRectangle *rectangle = rectangles.at(index);
        if (rectangle->isSelected())
        {
            rectangle->setRect(0, 0, rectangle->width(), rectangle->height());

            // Get the extra space around the rectangle, so that we can calculate the coordinates of the actual visible region
            int buffer = rectangle->buffer();
            int border = rectangle->border();

            int width = rectangle->width();
            int height = rectangle->height();

            // Get the x and y coordinate of the selection rectangle.
            // Since the rectangle can move beyond the pixmap, subtract the coordinates of the pixmap so that both,
            // the rectangle position lies within the pixmap's pixel region (which starts from 0,0).
            int x = rectangle->pos().x() + buffer + border - mPixmapItem->pos().x();
            int y = rectangle->pos().y() + buffer + border - mPixmapItem->pos().y();

            qreal total = 0;
            QImage image = mPixmapItem->pixmap().toImage();
            for (int row = y; row < y+height; row ++) {
                for (int col = x; col < x+width; col ++) {
                    QRgb val = image.pixel(col, row);
                    int red = qRed(val);
                    int green = qGreen(val);
                    int blue = qBlue(val);
                    total += std::max(std::max(red, green), blue);
                }
            }

            qreal mean = total/(width*height);
            //qDebug() << "Histogram Mean:" << mean;
            emit signalHistogramMean(mean);
        }
    }
}
