TEMPLATE = app
TARGET = ISPGui
CONFIG += c++11
INCLUDEPATH += ./GeneratedFiles
isEmpty(output_dir) {
    DESTDIR = $$PWD/output
} else {
    DESTDIR = "$$output_dir"
}

lessThan (QT_VERSION, 5.5) {
        error("Qt 5.5 or above required")
}

win32:VERSION = 4.0.3 # major.minor.patch.build
else:VERSION = 4.0.3 # major.minor.patch

# RC info
QMAKE_TARGET_COMPANY = "GEO Semiconductor, Inc."
QMAKE_TARGET_DESCRIPTION = $$TARGET
QMAKE_TARGET_COPYRIGHT = "Copyright 2013-2016 GEO Semiconductor, Inc."
QMAKE_TARGET_PRODUCT = $$TARGET

win32:RC_ICONS = "$$PWD/Resources/ISPGui.ico"

message(VERSION=$$VERSION)

CONFIG(debug, debug|release) {
    message(Building ISPGui Debug)
    INCLUDEPATH += ./GeneratedFiles/Debug
    MOC_DIR += ./GeneratedFiles/Debug
    OBJECTS_DIR += debug
} else {
    message(Building ISPGui Release)
    INCLUDEPATH += ./GeneratedFiles/release
    MOC_DIR += ./GeneratedFiles/release
    OBJECTS_DIR += release

    # Source
    # http://stackoverflow.com/questions/21008274/precompiled-headers-not-working-in-debug-build-with-qt-creator-qmake-mingw
    PRECOMPILED_HEADER = ./src/stdafx.h
}

QT += core widgets gui serialport
win32 {
  DEFINES += WIN64 QT_DLL
}
DEFINES += QT_WIDGETS_LIB
DEPENDPATH += .
UI_DIR += ./GeneratedFiles
RCC_DIR += ./GeneratedFiles
include(ISPGui.pri)
#RC_FILE = ISPGui.rc
win32:LIBS += -lshlwapi

# Copy/Install all files needed by the application to run
message(Copying files)

scripts_install.path = "$$DESTDIR"/scripts
scripts_install.files = "scripts/*.py"

xmodem_install.path = "$$DESTDIR"/scripts/xmodem
xmodem_install.files = "scripts/xmodem/*.py"

jsons_install.path = "$$DESTDIR"
jsons_install.files = "jsons/*.json"

docs_install.path = "$$DESTDIR"/docs
docs_install.files = "docs/*.pdf" "docs/*.txt"

conf_install.path = "$$DESTDIR"
conf_install.files = qt.conf

INSTALLS += \
    scripts_install \
    xmodem_install \
    jsons_install \
    conf_install \
    docs_install

win32 {
        dlls_install.path = $$DESTDIR
        dlls_install.files =  win/dlls/cygwin1.dll win/dlls/libgcc_s_dw2-1.dll win/dlls/libwinpthread-1.dll win/dlls/Qt5Core.dll win/dlls/Qt5Gui.dll win/dlls/Qt5SerialPort.dll win/dlls/Qt5Widgets.dll

        platforms_install.path = $$DESTDIR/plugins/platforms
        platforms_install.files = "win/dlls/platforms/*.dll"
		
        imageformats_install.path = $$DESTDIR/plugins/imageformats
        imageformats_install.files = "win/dlls/imageformats/*.dll"

        INSTALLS += dlls_install platforms_install imageformats_install

        # Qt Bug: Workaround needed to copy libstdc++-6.dll since it contains special character '+'
        # https://bugreports.qt.io/browse/QTBUG-16372
        dlls_install.depends += copy_libstdc
        copy_libstdc.target = copy_libstdc
        copy_libstdc.commands = $(COPY) \"$$PWD\win\dlls\libstdc++-6.dll\" \"$${dlls_install.path}\"

        QMAKE_EXTRA_TARGETS += copy_libstdc
}

unix {
    lib_install.path = "$$DESTDIR"/lib
    lib_install.files = "linux/lib/lib*"

    platforms_install.path = "$$DESTDIR"/plugins/platforms
    platforms_install.files = "linux/platforms/lib*"

    themes_install.path = "$$DESTDIR"/plugins/platformthemes
    themes_install.files = "linux/platformthemes/lib*"

    imageformats_install.path = "$$DESTDIR"/plugins/imageformats
    imageformats_install.files = "linux/imageformats/lib*"

    INSTALLS += lib_install platforms_install themes_install imageformats_install

    QMAKE_LFLAGS += -Wl,-rpath=\\\$\$ORIGIN/lib
}

!exists("$$DESTDIR"/bin) {
    mkpath($$DESTDIR/bin)
}
!exists("$$DESTDIR"/configs) {
    mkpath($$DESTDIR/configs)
}
!exists("$$DESTDIR"/plugins) {
    mkpath($$DESTDIR/plugins)
}
!exists("$$DESTDIR"/plugins/platforms) {
    mkpath($$DESTDIR/plugins/platforms)
}

DEFINES +=FIRMWARE_VERSION=\\\"$$fw_release\\\"
isEmpty(tool_version) {
    DEFINES +=TOOL_VERSION=\\\"$$VERSION\\\"
} else {
    DEFINES +=TOOL_VERSION=\\\"$$VERSION'_'$$tool_version\\\"
}
DEFINES += TOOL_COPYRIGHT="\\\"$$replace(QMAKE_TARGET_COPYRIGHT, " ", "_")\\\""
