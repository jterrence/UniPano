#ifndef ISPSERIALPORT_H
#define ISPSERIALPORT_H

#include <QStringList>
#include <QSerialPort>

class ISPSerialPort
{
public:
    ISPSerialPort();

    static QStringList getPorts();

private:
    QSerialPort serial;
};

#endif // ISPSERIALPORT_H
