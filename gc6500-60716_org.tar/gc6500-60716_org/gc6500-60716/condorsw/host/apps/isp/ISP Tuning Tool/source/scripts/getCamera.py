#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================

import sys
import platform
import subprocess
import os
from utils import fix_ownership

if __name__ == "__main__":
    device_type = sys.argv[1]
    json_path = sys.argv[2]
    connection_params = sys.argv[3]
    verbosity_level = sys.argv[4]
    spec_file_path = sys.argv[5]
    
    shell_exec = platform.system() == 'Linux'
    
    if device_type in ['gw4xx', 'gw3xx']:

        if platform.system() == 'Windows':
            command = r'./bin/{0}/read_table.exe {1} 2 ./bin/{0}/table.tbl'.format(device_type, connection_params)
        elif platform.system() == 'Linux':
            command = r'./bin/{0}/read_table {1} 2 ./bin/{0}/table.tbl'.format(device_type, connection_params)
            
        if subprocess.call(command, shell=shell_exec) is not 0:
            sys.exit(1)

        command = r'python ./scripts/table2json.py -s "{3}" -t ./bin/{0}/table.tbl -j "{1}" -d {2}'.format(device_type, json_path, verbosity_level, spec_file_path)
        if subprocess.call(command, shell=shell_exec) is not 0:
            sys.exit(1)

        os.unlink(r'./bin/{0}/table.tbl'.format(device_type))
            
    elif device_type == 'gc65xx':

        if platform.system() == 'Windows':
            mxcam = r'./bin/{0}/mxcam.exe'.format(device_type)
        elif platform.system() == 'Linux':
            mxcam = r'./bin/{0}/mxcam'.format(device_type)

        command = r'{0} readispcfg ./bin/{1}/read.bin'.format(mxcam, device_type)
        if subprocess.call(command, shell=shell_exec) is not 0:
            sys.exit(1)
            
        if not os.path.isfile("./bin/{0}/read.bin".format(device_type)):
            print >> sys.stderr, "Error: Failed to read isp configuration."
            sys.exit(1)

        command = r'python ./scripts/table2json.py -s "{3}" -t ./bin/{0}/read.bin -j "{1}" -d {2}'.format(device_type, json_path, verbosity_level, spec_file_path)
        if subprocess.call(command, shell=shell_exec) is not 0:
            sys.exit(1)

        os.unlink(r'./bin/{0}/read.bin'.format(device_type))
    
    else:

        print ("Error: unrecognized device.")
        sys.exit(1)
        
    if platform.system() == 'Linux':
        fix_ownership(json_path)

    sys.exit(0)
