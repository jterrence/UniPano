#!/usr/bin/python

import subprocess
import os
import threading
import array
from time import sleep
import sys
import struct
import string

#
# This script post-processes the output of W4 x-modem downloaded file and converts it
# into a rggb formated file

class BMP2RGB12():
    def __init__(self):
        pass

    # The following function assume input data is packed as the 12bpp W4 settings, i.e.
    # data are packed in 64-bit units, the first 4 bits are always zeros, then 
    # after that, pixel data are packed back to back, and each pixel is 12 bits long.
    # therefore, data are process as chunks of 64-bit
    def Convert(self, BmpFileName, RgbFileName, Pitch):
        StartOfFrameMarker = "START_OF_FRAME"
        ChunkSize = 8
        bpp = 12
        FileSize = os.stat(BmpFileName)
        Width  = (Pitch / ChunkSize) * (ChunkSize * 8 / bpp)
        with open(RgbFileName, "w+b") as fo:
            with open(BmpFileName, "rb") as fi:
                wholeImg = fi.read()     # read the whole image, since it is upside down
                imgEnd = wholeImg.find("END_OF_FRAME")
                imgStart = wholeImg.find(StartOfFrameMarker)
                if imgStart<0:
                    imgStart = 0
                else:
                    imgStart = imgStart + len(StartOfFrameMarker)

                if imgEnd > 0:
                    img = wholeImg[imgStart:imgEnd]
                else :
                    img = wholeImg[imgStart:FileSize.st_size]

                nLines = len(img) / Pitch
                print "Output Video Size is "+str(Width)+"x"+str(nLines)
                print "Each pixel is 16 bits long!"
                for y in range(0, nLines, 1):
                    line = img[y*Pitch : (y*Pitch + Pitch)]
                    for x in range(0, Pitch/ChunkSize):
                        i = x * ChunkSize
                        Chunk = line[i:(i+ChunkSize)]
                        pix  = struct.unpack(">HBHBH", Chunk)
                        # get the first pixel
                        npix = pix[0] & 0xfff
                        o = struct.pack(">H", npix)
                        fo.write(o)
                        # get the next 2 pixels
                        pp = (pix[1] << 16) | pix[2]
                        npix1 = pp >> bpp
                        npix2 = pp & 0xfff
                        o = struct.pack(">HH", npix1, npix2)
                        fo.write(o)
                        # get the last 2 pixels
                        pp = (pix[3] << 16) | pix[4]
                        npix1 = pp >> bpp
                        npix2 = pp & 0xfff
                        o = struct.pack(">HH", npix1, npix2)
                        fo.write(o)
                fi.close()
            fo.close()
        return 0


if __name__ == "__main__":
    app = BMP2RGB12()
    if (len(sys.argv) < 4):
        print "USAGE: "+sys.argv[0]+" Input_XModem_out Output.raw ImagePitch"
        exit()
    
    ImagePitch = int(sys.argv[3])
    app.Convert(sys.argv[1], sys.argv[2], ImagePitch)
