#!/usr/bin/env python
import struct
import param_type
import binascii

ID_FORMAT = param_type.get('uint16_t').format()
TYPE_FORMAT = param_type.get('uint8_t').format()
NUMROWS_FORMAT = param_type.get('uint16_t').format()
START_FORMAT = param_type.get('uint16_t').format()
END_FORMAT = param_type.get('uint16_t').format()
ENDIAN_FORMAT = {"be": '>', "le": '<'}

GLOBAL_HEADER_FORMAT = "<{}{}{}{}".format(param_type.get('uint16_t').format(),
                                          param_type.get('uint8_t').format(),
                                          param_type.get('uint8_t').format(),
                                          param_type.get('uint32_t').format())
GLOBAL_HEADER_V2_EXTENSION_FORMAT = "<{}{}".format(param_type.get('uint32_t').format(),
                                                   param_type.get('uint32_t').format())

MAGIC_CODE = 0xCEFA
V2_BIT = 7  # in header byte 2
FACTORY_TABLE_ID = 65500


class _BinaryBuffer(object):
    def __init__(self, buf):
        self._buf = buf
        self._cursor = 0

    def get(self, format):
        values = struct.unpack_from(format, self._buf, self._cursor)
        self._cursor += struct.calcsize(format)
        return values

    def skip(self, bytes):
        self._cursor += bytes

    def eof(self):
        return self._cursor >= len(self._buf)

# Parse JSON config file
class ParamConfig(object):
    def __init__(self, spec):
        self._spec = spec
        self._params = {}

    def params(self):
        return self._params

    def from_json(self, json_config, minmax):
        for section_name in json_config:
            for param_name in sorted(json_config[section_name]):
                value = json_config[section_name][param_name]
                params = self._spec.getFromName(param_name=param_name,
                                                section_name=section_name)

                if len(params) == 0:
                    raise Exception("Unknown parameter {} in section"
                                    " {}.".format(param_name, section_name))
                else:
                    for param in params:
                        param._initialized = 1
                        if param.is_array():
                            if minmax == "min" and param.id() != FACTORY_TABLE_ID:
                                self._params[param] = [param.min() for i in range(param.max_rows())]
                            elif minmax == "max" and param.id() != FACTORY_TABLE_ID:
                                self._params[param] = [param.max() for i in range(param.max_rows())]
                            else:
                                if len(value) > 0 and isinstance(value[0], list):
                                    self._params[param] = [row[param.dim_index()]
                                                           for row in value]
                                else:
                                    self._params[param] = value
                            size = len(self._params[param])
                            if (size < param.min_rows()) and (size != 0):
                                raise Exception("{}.{} has fewer rows"
                                                " than allowed ({} vs {})."
                                                .format(section_name, param_name,
                                                        size, param.min_rows()))
                            if size > param.max_rows():
                                raise Exception("{}.{} has more rows"
                                                " than allowed ({} vs {})."
                                                .format(section_name, param_name,
                                                        size, param.max_rows()))
                            for val in self._params[param]:
                                if val < param.min():
                                    raise Exception("{}.{} has an item smaller "
                                                    "than the minimum value allowed "
                                                    "({} vs {})."
                                                    .format(section_name, param_name,
                                                            val, param.min()))
                                if val > param.max():
                                    raise Exception("{}.{} has an item greater "
                                                    "than the maximum value allowed "
                                                    "({} vs {})."
                                                    .format(section_name, param_name,
                                                            val, param.max()))
                                if type(val) is not float and param_type.py_type(param.type()) is float:
                                    self._spec.vprint("WARNING: {}.{} has an item that does "
                                                        "not match the specified data type "
                                                        "({} instead of {})."
                                                        .format(section_name, param_name, type(val), 
                                                            param_type.py_type(param.type()), 4))
                                if type(val) is float and param_type.py_type(param.type()) is not float:
                                    raise Exception("{}.{} has an item that does not match the "
                                                    "specified data type ({} instead of {})."
                                                    .format(section_name, param_name, type(val),
                                                         param_type.py_type(param.type())))
                        else:
                            if minmax == "min":
                                self._params[param] = param.min()
                            elif minmax == "max":
                                self._params[param] = param.max()
                            else:
                                self._params[param] = value
                            if value < param.min():
                                raise Exception("{}.{} is smaller "
                                                "than the minimum value allowed "
                                                "({} vs {})."
                                                .format(section_name, param_name,
                                                        value, param.min()))
                            if value > param.max():
                                raise Exception("{}.{} is greater "
                                                "than the maximum value allowed "
                                                "({} vs {})."
                                                .format(section_name, param_name,
                                                        value, param.max()))
                            if type(value) is not float and param_type.py_type(param.type()) is float:
                                self._spec.vprint("WARNING: {}.{} does not match the "
                                                     "specified data type ({} instead of {})."
                                                     .format(section_name, param_name, type(value), 
                                                         param_type.py_type(param.type())), 4)
                            if type(value) is float and param_type.py_type(param.type()) is not float:
                                raise Exception("{}.{} does not match the "
                                                "specified data type ({} instead of {})."
                                                .format(section_name, param_name, type(value),
                                                    param_type.py_type(param.type())))
                            if section_name == "version":
                                self._spec.vprint("CONFIG {} = {}".format(param._full_name, value), 1)
        return self
        
    def to_json(self):
        sections = set(p.parent() for p in self._params)
        sections = sorted(sections, key=lambda k: k.name())
        section_entry = []
        for section in sections:
            params = [p for p in self._params if p.parent() == section
                      and not p.is_array()]
            params = sorted(params, key=lambda k: k.name())

            arrays = [p for p in self._params if p.parent() == section
                      and p.is_array()]
            array_names = set(p.name() for p in arrays)
            array_names = sorted(array_names)

            entry = []
            for p in params:
                entry.append("        \"{}\": {}"
                             .format(p.name(), self._params[p][0]))

            for name in array_names:
                dims = [dim for dim in arrays if dim.name() == name]
                dims = sorted(dims, key=lambda k: k.dim_index())
                nrows = len(self._params[dims[0]])
                ndims = len(dims)
                array_entry = []
                for i in range(nrows):
                    row = []
                    for j in range(ndims):
                        row.append(format(self._params[dims[j]][i]))
                    array_entry.append("            [{}]".format(', '.join(row)))
                inside_entry = ',\n'.join(array_entry)
                entry.append("        \"{}\": [\n{}\n        ]"
                             .format(name, inside_entry))

            section_entry.append("    \"{}\": {{\n{}\n    }}"
                                 .format(section.name(), ',\n'.join(entry)))

        return "{{\n{}\n}}".format(',\n'.join(section_entry))

    def from_bin(self, buf):
        binbuf = _BinaryBuffer(buf)

        # TODO: check whether the buffer size matches the size
        #       advertized in the header
        (magic_code, gtype, gsubtype, size) = binbuf.get(GLOBAL_HEADER_FORMAT)
        if magic_code != MAGIC_CODE:
            raise Exception("ERROR: Binary format not supported "
                            "(no FACE magic code).")
        if gtype & (1 << V2_BIT):
            (crc, reserved) = binbuf.get(GLOBAL_HEADER_V2_EXTENSION_FORMAT)

        endian = "be" if (gsubtype >> 1) & 0x1 == 1 else "le"

        while not binbuf.eof():
            # Get param id and type id
            (param_id, type_id) = binbuf.get("{}{}{}".format(
                ENDIAN_FORMAT[endian], ID_FORMAT, TYPE_FORMAT))

            # Get type object from type id
            type = param_type.getFromID(type_id)
            # Get param object from param id
            param = self._spec.getFromID(param_id)

            # Check whether the param type exists
            type = param_type.getFromID(type_id)
            if type is None:
                raise Exception("FATAl ERROR: Unsupported type id" + type_id)

            value = None

            # Handle array params
            if param_type.is_array(type_id):
                (numrows, start, end) = binbuf.get("{}{}{}{}".format(
                    ENDIAN_FORMAT[endian], NUMROWS_FORMAT,
                    START_FORMAT, END_FORMAT))
                value = list(binbuf.get(ENDIAN_FORMAT[endian]
                             + (type.format() * numrows)))
            # Handle non array params
            else:
                value = binbuf.get(ENDIAN_FORMAT[endian] + type.format())

            if param is None:
                self._spec.vprint("WARNING: skipping unknown parameter id {}".format(param_id), 1)
            else:
                self._params[param] = value

        return self

    def to_bin(self, endian, v2 = 0, filter = "", skip_header = False):
        buffer = ''
        ids = filter[4:].split(",")
        for (param, value) in sorted(self._params.iteritems(), key=lambda k: k[0].id()):
            if filter[:4] != "filt" or str(param.id()) in ids:
                if not param.is_array():
                    buffer += self._param_to_bin(param, value, endian)
                else:
                    buffer += self._param_array_to_bin(param, value, endian, skip_header)

        # add W4-style "v2" header extension
        # this contains the W4 table CRC (NOT the JSON framework CRC)
        buflen = len(buffer)
        rem = (32 - (buflen % 32)) % 32
        pad = struct.pack("<%dB" % rem, *([0xff] * rem))
        crc = binascii.crc32(buffer + pad) & 0xffffffff

        global_header = struct.pack(GLOBAL_HEADER_FORMAT,
                                    MAGIC_CODE, self._spec.id() | (v2 << V2_BIT),
                                    0x0 if endian == "le" else 0x02,
                                    len(buffer))
        global_header_v2_ext = struct.pack(GLOBAL_HEADER_V2_EXTENSION_FORMAT,
                                           crc, 0)

        if v2:
            return global_header + global_header_v2_ext + buffer
        else:
            return global_header + buffer

    @classmethod
    def _param_to_bin(cls, param, value, endian):
        format = "{}{}{}{}"\
                 .format(ENDIAN_FORMAT[endian], ID_FORMAT, TYPE_FORMAT,
                         param.type().format())
        return struct.pack(format, param.id(), param.type().id(), value)

    @classmethod
    def _param_array_to_bin(cls, param, value, endian, skip_header):
        format_header = "{}{}{}{}{}{}".format(ENDIAN_FORMAT[endian], ID_FORMAT,
                                              TYPE_FORMAT, NUMROWS_FORMAT,
                                              START_FORMAT, END_FORMAT)
        format_value = ENDIAN_FORMAT[endian]
        for i in range(len(value)):
            format_value += param.type().format()

        if skip_header:
            buffer = ""
        # 0-length array means we add it to the binary with size == 0
        # this tells the firmware to "delete" this table from JSON storage
        elif len(value) != 0:
            buffer = struct.pack(format_header, param.id(),
                                 param.type().id_array(),
                                 len(value), 0, len(value) - 1)
        else:
            buffer = struct.pack(format_header, param.id(),
                                 param.type().id_array(),
                                 0, 0, 0)
        buffer += struct.pack(format_value, *value)
        return buffer
