#ifndef CONNECTIONMANAGER_H
#define CONNECTIONMANAGER_H

#include <QObject>
#include "ispgui.h"

class ConnectionManager : public QObject
{
    Q_OBJECT

public:
    explicit ConnectionManager(QObject *parent = 0);

    bool isDeviceConnected(ISPGui::TargetDevice device, QString & errorString);

signals:

public slots:

private:
    QStringList getSerialPorts();
    bool isPortAvailable(QString portName);

};

#endif // CONNECTIONMANAGER_H
