/****************************************************************************
**
** This came from "C:\Qt\5.4\Src\qtbase\examples\widgets\itemviews\spinboxdelegate\delegate.cpp"
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
**     of its contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

/*
    delegate.cpp

    A delegate that allows the QLineEdit to accept decimal numbers, or hex numbers starting with "0x" or "0X"
*/

#include "stdafx.h"
#include "ISPTableItemDelegate.h"
#include "ISPValidator.h"
#include "util.h"

IspTableItemDelegate::IspTableItemDelegate(QList < QPair<double, double> > *minMaxList, QList < bool > *allowDecimalList, bool displayHexAsDecimal, QObject *parent)
    : QStyledItemDelegate(parent)
	, minMaxList(minMaxList)
    , allowDecimalList(allowDecimalList)
    , displayHexAsDecimal(displayHexAsDecimal)
{
}


IspTableItemDelegate::~IspTableItemDelegate()
{
	if (minMaxList)
		delete minMaxList;
}

QWidget *IspTableItemDelegate::createEditor(QWidget *parent,
    const QStyleOptionViewItem &/* option */,
    const QModelIndex &index) const
{
	// If previous cell is empty, show error and do not create any editor to prevent
	// user from editing this cell
	bool checkPrevCell = true;
    bool checkNextCell = true;
    int allowEmptyCell = 1;
	int checkRow = index.row();
	int checkCol = index.column();
	if (checkCol > 0)		// not first column
	{
		// check previous column in same row
		checkCol--;			
	}
	else					// first column
	{
		if (checkRow > 0)	// not first row
		{
			// check last column in previous row
			checkRow--;		
			checkCol = index.model()->columnCount() - 1;
		}
		else                 // first row, first column
		{
			// No previous cell to check, allow edit
			checkPrevCell = false;
		}
	}

    auto model = dynamic_cast<const QAbstractTableModel *>(index.model());
	if (checkPrevCell)
    {
		auto checkModelIndex = model->index(checkRow, checkCol);
		auto prevCellText = model->data(checkModelIndex).toString();
		if (prevCellText.isEmpty())
		{
			QMessageBox::warning(parent, tr("Cannot Edit"),
				tr("Please enter data into the table without leaving any preceding empty cells.\r\nPlease start at the top left of the table and fill cells from there without leaving blank ones."));

			// Prevent the line editor from being created, user can't
			// edit this cell
			return nullptr;
		}
    }

    int nextColumn = index.column()+1;
    int nextRow = index.row();
    if (nextColumn == model->columnCount())
    {
        nextColumn = 0;
        nextRow ++;
        if (nextRow == model->rowCount())
        {
            // rows and columns over. No next cell available
            checkNextCell = false;
        }
    }

    if (checkNextCell)
    {
        // Check if the next cell is empty. If so, then allow this cell to be deleted/cleared
        auto checkModelIndex = model->index(nextRow, nextColumn);
        auto nextCellText = model->data(checkModelIndex).toString();
        allowEmptyCell = (nextCellText.length() == 0);
    }

	// Allow this item to be edited
	// Get min and max for the column
	int col = index.column();
	Q_ASSERT(minMaxList);
	if ((col >= minMaxList->count()) && (minMaxList->count() == 1))		// 1D array with "pitch" columns
		col = 0;		// all columns use same min/max specified in the 1 column
	Q_ASSERT(col < minMaxList->count());
	QPair<int, int> minMax = minMaxList->at(col);
    double min = minMax.first;
    double max = minMax.second;
    bool allowDecimal = allowDecimalList->at(col);

	// Create a line editor that accepts decimal numbers, or hex numbers starting with "0x" or "0X"
	auto lineEdit = new QLineEdit(parent);
    QString regex = displayHexAsDecimal ? "(-)?[0-9]*.{0,1}[0-9]+" : (allowDecimal ? "(0x|0X)?(?(1)[a-fA-F0-9]*|(-)?[0-9.]*)" : "(0x|0X)?(?(1)[a-fA-F0-9]*|(-)?[0-9]*)");
    auto validator = new ISPValidator(QRegularExpression(regex), min, max, lineEdit, allowEmptyCell);
    lineEdit->setValidator(validator);
	return lineEdit;
}

/*
void IspTableItemDelegate::paint(QPainter *painter,
                                 const QStyleOptionViewItem &option,
                                 const QModelIndex &index) const
{
    if (displayHexAsDecimal) {
        QString text = index.model()->data(index, Qt::EditRole).toString();
        if (text.startsWith("0x", Qt::CaseInsensitive))
        {
            // Convert hex to dec
            QString dec = "";
            if (!Util::fixedPointHexToDec(text, dec))
            {
                Q_ASSERT(true);
            }
            text = dec;
        }

        painter->drawText(option.rect, Qt::AlignCenter, text);
    } else{
        QStyledItemDelegate::paint(painter, option, index);
    }
}
*/

void IspTableItemDelegate::setEditorData(QWidget *editor,
                                    const QModelIndex &index) const
{
    auto text = index.model()->data(index, Qt::EditRole).toString();

    if (displayHexAsDecimal) {
        // Convert to decimal in case the value is in hex
        if (text.startsWith("0x", Qt::CaseInsensitive))
        {
            // Convert hex to dec
            if (!Util::fixedPointHexToDec(text, text))
            {
                Q_ASSERT(false);
            }
        }
    }

    QLineEdit *lineEdit = static_cast<QLineEdit*>(editor);
    lineEdit->setText(text);
}

void IspTableItemDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                   const QModelIndex &index) const
{
	QLineEdit *lineEdit = static_cast<QLineEdit*>(editor);
    auto text = lineEdit->text();

    if (displayHexAsDecimal || text.indexOf(".") > 0)
    {
        double val = text.toDouble(NULL);
        text = QString::number(val, 'f', 6);
    }
    else
    {
        int val = text.toInt(NULL);
        text = QString::number(val);
    }


    model->setData(index, text, Qt::EditRole);
}

void IspTableItemDelegate::updateEditorGeometry(QWidget *editor,
    const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
    editor->setGeometry(option.rect);
}
