/*******************************************************************************
*
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this
* copyright notice.
*
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
*
*******************************************************************************/

#ifndef IMAGEINFOSCENE_H
#define IMAGEINFOSCENE_H

#include <QGraphicsScene>
#include <QList>

class SelectionRectangle;

class ImageInfoScene : public QGraphicsScene
{
    Q_OBJECT

public:
    explicit ImageInfoScene(int width, int height, QObject *parent = 0);
    ~ImageInfoScene();

    /**
     * @brief Set the Image for this scene. All calculations will be done on this image.
     * @param image The image path
     */
    void setImage(QString image, QRectF size);

    /**
     * @brief Add selection rectangles/regions to the image info scene
     * @param num       Number of rectangles to add. Default is 1.
     * @param width     Width of each rectangle. Default is 100.
     * @param height    Height of each rectangle. Default is 100.
     */
    void addSelectionRectangles(int num = 1, qreal width = 100.0, qreal height = 100.0);

    /**
     * @brief Calculate the mean histogram value for a selected region.
     *        Emits signalHistogramMean with the mean value.
     */
    void calculateHistogramMean();

    void clearScene();

signals:

    void signalHistogramMean( qreal mean );

public slots:

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

private:
    void removeSelectionRectangles();

    QPointF mDragStartPosition;
    QPointF mDragEndPosition;

    QList < SelectionRectangle * > rectangles;
    QGraphicsPixmapItem *mPixmapItem;
};

#endif // IMAGEINFOSCENE_H
