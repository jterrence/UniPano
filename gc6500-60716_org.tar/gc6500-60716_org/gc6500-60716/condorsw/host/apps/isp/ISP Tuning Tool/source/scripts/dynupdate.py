#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================

import sys
import platform
import subprocess
from subprocess import CalledProcessError
import os

if __name__ == "__main__":
	device_type = sys.argv[1]
	json_path = sys.argv[2]
	connection_params = sys.argv[3]
	verbosity_level = sys.argv[4]
	spec_file_path = sys.argv[5]
	
	shell_exec = platform.system() == 'Linux'
	
	if device_type == 'gw4xx':
		
		command = r'python ./scripts/genbin.py "{3}" "{0}" be ./bin/{1}/dynupdate.tbl -d {2} -v2'.format(json_path, device_type, verbosity_level, spec_file_path)
		if subprocess.call(command, shell=shell_exec) is not 0:
			sys.exit(1)

		if platform.system() == 'Windows':
			command = r'./bin/{0}/json_update.exe {1} ./bin/{0}/dynupdate.tbl'.format(device_type, connection_params)
		elif platform.system() == 'Linux':
			command = r'./bin/{0}/json_update {1} ./bin/{0}/dynupdate.tbl'.format(device_type, connection_params)

		if subprocess.call(command, shell=shell_exec) is not 0:
			sys.exit(1)
			
		os.unlink(r'./bin/{0}/dynupdate.tbl'.format(device_type))
			
	elif device_type == 'gw3xx':
		
		command = r'python ./scripts/genbin.py "{3}" "{0}" be ./bin/{1}/dynupdate.tbl -d {2} -v1'.format(json_path, device_type, verbosity_level, spec_file_path)
		if subprocess.call(command, shell=shell_exec) is not 0:
			sys.exit(1)
		
		if platform.system() == 'Windows':
			command = r'./bin/{0}/json_update.exe {1} ./bin/{0}/dynupdate.tbl'.format(device_type, connection_params)			
		elif platform.system() == 'Linux':
			command = r'./bin/{0}/json_update {1} ./bin/{0}/dynupdate.tbl'.format(device_type, connection_params)
			
		if subprocess.call(command, shell=shell_exec) is not 0:
			sys.exit(1)
			
		os.unlink(r'./bin/{0}/dynupdate.tbl'.format(device_type))
			
	elif device_type == 'gc65xx':
		
		command = r'python ./scripts/genbin.py "{3}" "{0}" le ./bin/{1}/dynupdate.bin -d {2}'.format(json_path, device_type, verbosity_level, spec_file_path)
		if subprocess.call(command, shell=shell_exec) is not 0:
			sys.exit(1)
			
		if platform.system() == 'Windows':			
			mxcam = r'./bin/{0}/mxcam.exe'.format(device_type)
		elif platform.system() == 'Linux':
			mxcam = r'./bin/{0}/mxcam'.format(device_type)

		command = r'{0} ispcfg ./bin/{1}/dynupdate.bin'.format(mxcam, device_type)
		try:
			output = subprocess.check_output(command, shell=shell_exec)
			if output.lower().find("no compatible device") > -1:
				print ("Error: %s" % output.lower())
				sys.exit(1)
			elif output.lower().find("command is not supported") > -1:
				print ("Error: Device may not be booted")
				sys.exit(1)
		except CalledProcessError as cpe:
			if cpe.returncode < 0:
				print ("Error: return code: {0}, output: {1}".format(cpe.returncode, cpe.output))
				sys.exit(1)
			
		os.unlink(r'./bin/{0}/dynupdate.bin'.format(device_type))

	else:
		print ("Error: unrecognized device.")
		sys.exit(1)

	sys.exit(0)
