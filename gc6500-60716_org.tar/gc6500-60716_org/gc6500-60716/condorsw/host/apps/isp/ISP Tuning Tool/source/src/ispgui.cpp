/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include <QSettings>
#include <QRegExp>

#include "stdafx.h"
#include "ISPTableWidget.h"
#include "about.h"
#include "ispgui.h"
#include "util.h"
#include "ispserialport.h"
#include "connectionmanager.h"

using std::numeric_limits;

// This macro is not present in "stdlib.h" on Linux
#ifndef _countof
#define _countof(arr) (sizeof(arr)/sizeof(arr[0]))
#endif

// Temp directory
static const QString tempDir = QDir::currentPath().append("/.tmp");

//////////////////////////////////////////////////////////////////////////
// Control parsing

static QStringList s_intControlTypes(QStringList() << "int8_t" << "uint8_t" << "int16_t" << "uint16_t" << "int32_t" << "uint32_t");
static QStringList s_floatControlTypes(QStringList() << "float" << "double");
int c_doublePrecision = 4;

//////////////////////////////////////////////////////////////////////////
// ISPGui

ISPGui::ISPGui(QWidget *parent)
    : QMainWindow(parent)
    , m_devJsonDoc(nullptr)
    , m_configMajorVersion(-1), m_configMinorVersion(-1)
    , m_tableFilePath(".")
    , scene(NULL)
    , m_connectionManager(NULL)
{
    ui.setupUi(this);
    m_pVersionLabel = new QLabel;
    ui.statusBar->addPermanentWidget(m_pVersionLabel, 1);

    QSettings settings("GeoSemi", "ISPGui");
    if (settings.contains("geometry"))
    {
        restoreGeometry(settings.value("geometry").toByteArray());
    }
    else
    {
        // Set reasonably initial size, on gnu build the size specified in the .ui file is ignored
        resize(1200, 800);
    }

    // Create temp directory
    if (!QDir(tempDir).exists())
    {
        QDir().mkdir(tempDir);
#ifndef WIN32
        Util::fixPermissions(tempDir);
#endif
    }

    // Set initial console height
    //ui.textEditConsole->resize(ui.textEditConsole->size().width(), 300);

    // Slots for Menu and Toolbar actions

    // Quit app when this window is closed
    QObject::connect(ui.actionExit, &QAction::triggered, this, &QWidget::close);

    // Open Config action - read JSON
    QObject::connect(ui.actionOpenConfig, &QAction::triggered, this, [this]() {
        ISPTableWidget::stopEdit();

        QString configFilePath = m_configFilePath;
        if (configFilePath.isEmpty()) {
            //setWindowTitle(QString("%1: -").arg(ui.comboBoxTargetDevice->currentText()));

            // Prompt user to select the configuration file to open
            QDir configsDir = QString(QDir::currentPath()).append("/configs/");
            if (configsDir.exists()) {
                configFilePath = configsDir.path().append(tr("/%1").arg(ui.comboBoxTargetDevice->currentText().toLower()));
            }
        } else {
            // Get the previously opened directory
            configFilePath = QFileInfo(m_configFilePath).absoluteDir().absolutePath();
        }

        configFilePath = QFileDialog::getOpenFileName(this, tr("Open Configuration file"), configFilePath, tr("JSON files (*.json)"));
        if (!configFilePath.isEmpty())
        {
            m_configFilePath = configFilePath;
            loadConfigFile();
        }
    });

    // Load new spec
    QObject::connect(ui.actionLoadSpec, &QAction::triggered, this, [this](){
        ISPTableWidget::stopEdit();

        QString specFilePath = QFileDialog::getOpenFileName(this, tr("Select Spec file"), QDir::currentPath(), tr("JSON files (*.json)"));
        if (!specFilePath.isEmpty())
        {

            logMsg(tr("Loading spec file"));

            if (configIsModified())
            {
                QMessageBox *messageBox = new QMessageBox(this);
                QAbstractButton *continueButton = messageBox->addButton(tr("Continue anyway"), QMessageBox::ActionRole);
                messageBox->addButton(QMessageBox::Cancel);
                messageBox->setDefaultButton(QMessageBox::Cancel);

                messageBox->setIcon(QMessageBox::Question);
                messageBox->setWindowTitle(tr("Load new spec"));
                messageBox->setText(tr("Config changes have not been set to the camera."));
                messageBox->exec();

                if (messageBox->clickedButton() == continueButton)
                {
                    // Continue anyway
                }
                else
                {
                    // Cancel.
                    logMsg(tr("Load spec cancelled"));
                    return;
                }
            }

            QString oldSpecFilePath = m_specFilePath;
            m_specFilePath = specFilePath;
            if (loadSpecFile())
            {
                QSettings settings("GeoSemi", "ISPGui");
                settings.setValue("spec", m_specFilePath);

                // Load config again
                loadConfigFile();
            }
            else
            {
                m_specFilePath = oldSpecFilePath;
            }
        }
    });

    // Save Config action - write JSON
    QObject::connect(ui.actionSaveConfig, &QAction::triggered, this, [this]() {
        ISPTableWidget::stopEdit();

        if (m_configFilePath.isEmpty())		// no current file is open
        {
            // Prompt user to select the configuration file to save
            m_configFilePath = Util::getSaveFileName(this, tr("Save Configuration file"), QString("."));
        }

        if (!m_configFilePath.isEmpty())
        {
            if (m_configFilePath.endsWith(".bin"))
            {
                m_binaryLoaded = true;
                saveConfigFile(m_configFilePath, true);		// save unchanged parameters
                launchApp(LaunchedApp::SaveBinary);
            }
            else
            {
                m_binaryLoaded = false;
                saveConfigFile(m_configFilePath, true);		// save unchanged parameters
            }
        }
    });

    // Save Config As action - write JSON to new file
    QObject::connect(ui.actionSaveConfigAs, &QAction::triggered, this, [this]() {
        ISPTableWidget::stopEdit();

        // Prompt user to select the configuration file to save as
        QString new_configFilePath = Util::getSaveFileName(this, tr("Save Configuration file"), m_configFilePath.trimmed().length() > 0 ? m_configFilePath : QString("."));
        if (!new_configFilePath.isEmpty())
        {
            m_configFilePath = new_configFilePath;

            if (m_configFilePath.endsWith(".bin"))
            {
                m_binaryLoaded = true;
                saveConfigFile(m_configFilePath, true);		// save unchanged parameters
                launchApp(LaunchedApp::SaveBinary);
            }
            else
            {
                m_binaryLoaded = false;
                saveConfigFile(m_configFilePath, true);		// save unchanged parameters
            }
        }
    });

    // Connect Camera
    QObject::connect(ui.actionConnectCamera, &QAction::triggered, this, [this]() {
        ISPTableWidget::stopEdit();
        launchApp(LaunchedApp::ConnectCamera);
    });

    // Disconnect Camera
    QObject::connect(ui.actionDisconnectCamera, &QAction::triggered, this, [this]() {
        ISPTableWidget::stopEdit();
        launchApp(LaunchedApp::DisconnectCamera);
    });

    // Get Camera
    QObject::connect(ui.actionGetCamera, &QAction::triggered, this, [this]() {
        ISPTableWidget::stopEdit();

        if (!checkConnectedDevices())
        {
            return;
        }

        QMessageBox *messageBox = new QMessageBox(this);
        QAbstractButton *overwriteButton = NULL;
        if (!m_binaryLoaded)
        {
            overwriteButton = messageBox->addButton(tr("Overwrite"), QMessageBox::ActionRole);
        }
        QAbstractButton *saveAsButton = messageBox->addButton(tr("Save as"), QMessageBox::ActionRole);
        messageBox->addButton(QMessageBox::Cancel);
        messageBox->setDefaultButton(QMessageBox::Cancel);

        messageBox->setIcon(QMessageBox::Question);
        messageBox->setWindowTitle(tr("Read camera config"));
        messageBox->setText(tr("The JSON configuration from the camera will be read and saved to a file"));
        if (!m_binaryLoaded)
        {
            messageBox->setInformativeText(tr("To overwrite the current config file, click \"Overwrite\".\nTo save as a new config file, click \"Save as\".\nTo cancel, click \"Cancel\"."));
        }
        else
        {
            messageBox->setInformativeText(tr("To save as a new config file, click \"Save as\".\nTo cancel, click \"Cancel\"."));
        }
        messageBox->exec();

        if (messageBox->clickedButton() == overwriteButton) {

            // Overwrite. Send current path to the script to be overwritten

        } else if (messageBox->clickedButton() == saveAsButton) {
            // Save as. Get new file path
            QString filePath = Util::getSaveFileName(this, tr("Save Configuration file"), QString("."), tr("JSON file (*.json)"));
            if (filePath.isEmpty()) {
                return;
            }

            if (!filePath.endsWith(".json"))
            {
                filePath.append(".json");
            }

            m_configFilePathOld = m_configFilePath;
            m_configFilePath = filePath;
            m_binaryLoaded = false;

        } else {

            // Cancel
            return;
        }

        // Call the GetCamera batch script to read the json file from the camera
        launchApp(LaunchedApp::GetCurrentCamera);

        // Listen for the launchComplete signal. If success, load the updated config
        QObject::connect(this, &ISPGui::launchComplete, this, [this](bool success) {
            if (success) {
                loadConfigFile();
                m_binaryLoaded = false;
            } else {
                // Get camera failed. Revert the config path
                m_configFilePath = m_configFilePathOld;
                m_binaryLoaded = m_configFilePath.endsWith(".bin");
            }

            disconnect(this, &ISPGui::launchComplete, 0, 0);
        });
    });

    // Set Camera
    QObject::connect(ui.actionSetCamera, &QAction::triggered, this, [this]() {
        ISPTableWidget::stopEdit();

        if (!checkConnectedDevices())
        {
            return;
        }

        if (m_configMajorVersion == -1 || m_configMinorVersion == -1)
        {
            QMessageBox::warning(this, tr(""), tr("Please load a valid config to perform this action"));
            ui.actionOpenConfig->trigger();
        }

        if (m_configFilePath.isEmpty())
        {
            return;
        }

        // Save changed parameters
        QString tempFilePath("<setcamera-json-path>");
        replaceMacros(tempFilePath);
        logMsg(tr("Saving changed config to <setcamera-json-path>: %1").arg(QDir::fromNativeSeparators(tempFilePath)));
        tempFilePath = QDir::toNativeSeparators(tempFilePath);
        if (saveConfigFile(tempFilePath, false))		// don't save unchanged parameters
        {
            // Listen for the launchComplete signal. If success, load the updated config
            QObject::connect(this, &ISPGui::launchComplete, this, [this](bool success) {
                if (success) {
                    resetOriginalValues();
                }

                disconnect(this, &ISPGui::launchComplete, 0, 0);
            });

            launchApp(LaunchedApp::SetCurrentCamera);
        }
    });

    // Reset Camera
    QObject::connect(ui.actionResetCamera, &QAction::triggered, this, [this]() {
        ISPTableWidget::stopEdit();
        if (!checkConnectedDevices())
        {
            return;
        }
        launchApp(LaunchedApp::ResetCamera);
    });

    // Commit Flash
    QObject::connect(ui.actionCommitFlash, &QAction::triggered, this, [this]() {
        ISPTableWidget::stopEdit();

        if (!checkConnectedDevices())
        {
            return;
        }

        if (m_configMajorVersion == -1 || m_configMinorVersion == -1)
        {
            QMessageBox::warning(this, tr(""), tr("Please load a valid config to perform this action"));
            ui.actionOpenConfig->trigger();
        }

        if (m_configFilePath.isEmpty())
        {
            return;
        }

        if (m_configFilePath.isEmpty())		// no current file is open
        {
            // Prompt user to select the configuration file to save
            m_configFilePath = Util::getSaveFileName(this, tr("Save Configuration file"), QString("."));
        }

        if (!m_configFilePath.isEmpty())
        {
            if (m_configFilePath.endsWith(".bin"))
            {
                m_binaryLoaded = true;
                if (!saveConfigFile(m_configFilePath, true)) // save unchanged parameters
                {
                    return;
                }
                launchApp(LaunchedApp::SaveBinary);
            }
            else
            {
                m_binaryLoaded = false;
                if (!saveConfigFile(m_configFilePath, true)) // save unchanged parameters
                {
                    return;
                }
            }
        }

        // Listen for the launchComplete signal. If success, load the updated config
        QObject::connect(this, &ISPGui::launchComplete, this, [this](bool success) {
            if (success) {
                resetOriginalValues();
            }

            disconnect(this, &ISPGui::launchComplete, 0, 0);
        });

        launchApp(LaunchedApp::CommitFlash);
    });

    // EV
    QObject::connect(ui.actionReadSensorEV, &QAction::triggered, this, [this]() {
            ISPTableWidget::stopEdit();
            if (!checkConnectedDevices())
            {
                return;
            }
            launchApp(LaunchedApp::ReadSensorEV);
    });

    // Vertical Flip
    QObject::connect(ui.buttonVerticalFlip, &QPushButton::clicked, this, [this](bool) {
        launchApp(LaunchedApp::FlipV);
    });

    // Horizontal Flip
    QObject::connect(ui.buttonHorizontalFlip, &QPushButton::clicked, this, [this](bool) {
        launchApp(LaunchedApp::FlipH);
    });

    // About
    QObject::connect(ui.actionAbout, &QAction::triggered, this, [this]() {
        ISPTableWidget::stopEdit();

        // Show modal About dialog
        AboutDialog aboutDialog;
        aboutDialog.setReleaseVersion((QString)FIRMWARE_VERSION,(QString)TOOL_VERSION, QString(TOOL_COPYRIGHT).split("_").join(" "));
        aboutDialog.exec();
    });

    // Help
    QObject::connect(ui.actionHelp, &QAction::triggered, this, [this]() {
            ISPTableWidget::stopEdit();

            // Launch help file specified in dev.json
            if (m_helpFilePath.isEmpty())
            {
                    QMessageBox::warning(this, tr("Help Error"), tr("No Help file specified in dev.json \"help_file\"."));
                    return;
            }

            QDesktopServices::openUrl(QUrl::fromLocalFile(m_helpFilePath));
    });

	// Subsection tree item changed
    QObject::connect(ui.subsectionTree, &QTreeWidget::currentItemChanged, this, [this](QTreeWidgetItem *current, QTreeWidgetItem * /* previous */) {

		ISPTableWidget::stopEdit();

		// Source:  http://stackoverflow.com/questions/12557670/getting-rid-of-unnecessary-scrollbar-in-qscrollarea
		auto currentPage = ui.subsectionStackedWidget->currentWidget();
		if (currentPage)
			ui.subsectionStackedWidget->currentWidget()->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);

		ui.subsectionStackedWidget->setCurrentIndex(ui.subsectionTree->indexOfTopLevelItem(current));

		ui.subsectionStackedWidget->currentWidget()->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	});

    // Tuning button clicked
    QObject::connect(ui.tuningTool, &QToolButton::clicked, this, [this](bool) {
        if (ui.sectionStackedWidget->currentIndex() == 0) {
            ui.tuningTool->setChecked(true);
            return;
        }

        ISPTableWidget::stopEdit();
        ui.sectionStackedWidget->setCurrentIndex(0);
        ui.tuningTool->setChecked(true);
        ui.setupTool->setChecked(false);
        ui.rawCapTool->setChecked(false);
    });

    // Setup button clicked
    QObject::connect(ui.setupTool, &QToolButton::clicked, this, [this](bool) {
        if (ui.sectionStackedWidget->currentIndex() == 1) {
            ui.setupTool->setChecked(true);
            return;
        }

        ISPTableWidget::stopEdit();
        ui.sectionStackedWidget->setCurrentIndex(1);
        ui.setupTool->setChecked(true);
        ui.tuningTool->setChecked(false);
        ui.rawCapTool->setChecked(false);
    });

    // Raw Capture menu button clicked
    QObject::connect(ui.rawCapTool, &QToolButton::clicked, this, [this](bool){
        if (ui.sectionStackedWidget->currentIndex() == 2) {
            ui.rawCapTool->setChecked(true);
        }

        ISPTableWidget::stopEdit();
        ui.sectionStackedWidget->setCurrentIndex(2);
        ui.setupTool->setChecked(false);
        ui.tuningTool->setChecked(false);
        ui.rawCapTool->setChecked(true);

        if (ui.comboBoxTargetDevice->currentText().compare(targetDeviceName(GC65XX), Qt::CaseInsensitive) == 0)
        {
            if (ui.editRawCapAddress->text().trimmed().length() == 0)
            {
                ui.editRawCapAddress->setText("0x200a000");
            }

            if (ui.editRawCapExp->text().trimmed().length() == 0)
            {
                ui.editRawCapExp->setText("0x2000");
            }

            if (ui.editRawCapGain->text().trimmed().length() == 0)
            {
                ui.editRawCapGain->setText("1");
            }

            if (ui.editRawCapFlip->text().trimmed().length() == 0)
            {
                ui.editRawCapFlip->setText("0");
            }
        }

        if (ui.comboBoxTargetDevice->currentText().compare(targetDeviceName(GW4XX), Qt::CaseInsensitive) == 0 ||
            ui.comboBoxTargetDevice->currentText().compare(targetDeviceName(GW3XX), Qt::CaseInsensitive) == 0)
        {
            if (ui.editRawCapX->text().trimmed().length() == 0)
            {
                ui.editRawCapX->setText("0");
            }
            if (ui.editRawCapY->text().trimmed().length() == 0)
            {
                ui.editRawCapY->setText("0");
            }
            if (ui.editRawCapWidth->text().trimmed().length() == 0)
            {
                ui.editRawCapWidth->setText("1280");
            }
            if (ui.editRawCapHeight->text().trimmed().length() == 0)
            {
                ui.editRawCapHeight->setText("720");
            }
        }

        if (ui.editRawCapBpp->text().trimmed().length() == 0)
        {
            ui.editRawCapBpp->setText("16");
        }

        if (ui.editRawCapOutputName->text().trimmed().length() == 0)
        {
            ui.editRawCapOutputName->setText("output");
        }
    });

    // Raw Capture
    QObject::connect(ui.buttonRawCapture, &QPushButton::clicked, this, [this](bool) {
        // Validate raw capture parameters
        ISPGui::TargetDevice device = targetDevice(ui.comboBoxTargetDevice->currentText());

        if (device == GW4XX || device == GW3XX)
        {
            if (ui.editRawCapX->text().trimmed().length() == 0 ||
                ui.editRawCapY->text().trimmed().length() == 0 ||
                ui.editRawCapBpp->text().trimmed().length() == 0 ||
                ui.editRawCapWidth->text().trimmed().length() == 0 ||
                ui.editRawCapHeight->text().trimmed().length() == 0 ||
                ui.editRawCapOutputName->text().trimmed().length() == 0)
            {
                QMessageBox::warning(this, tr("Error"), tr("Please fill in all fields"));
                return;
            }
        }
        else if (device == GC65XX)
        {
            if (ui.editFirmwareLocation->text().trimmed().length() == 0 ||
                ui.editRawCapJsonLocation->text().trimmed().length() == 0 ||
                ui.editRawCapExp->text().trimmed().length() == 0 ||
                ui.editRawCapWidth->text().trimmed().length() == 0 ||
                ui.editRawCapHeight->text().trimmed().length() == 0 ||
                ui.editRawCapBpp->text().trimmed().length() == 0 ||
                ui.editRawCapGain->text().trimmed().length() == 0 ||
                ui.editRawCapFlip->text().trimmed().length() == 0 ||
                ui.editRawCapOutputName->text().trimmed().length() == 0 ||
                ui.editRawCapAddress->text().trimmed().length() == 0)
            {
                QMessageBox::warning(this, tr("Error"), tr("Please fill in all fields"));
                return;
            }
        }

        // Warn user if output file exists.
        QString outputFileName = QString("raw-output/%1.tiff").arg(ui.editRawCapOutputName->text());
        if (QFileInfo(outputFileName).exists())
        {
            QMessageBox::StandardButton userResponse = QMessageBox::warning(this,
                                                                            tr("File exists"),
                                                                            tr("Output file \"%1\" already exists. Do you want to overwrite?").arg(ui.editRawCapOutputName->text()),
                                                                            QMessageBox::StandardButton::Yes|QMessageBox::StandardButton::No, QMessageBox::StandardButton::Yes);
            if (userResponse != QMessageBox::StandardButton::Yes)
            {
                ui.editRawCapOutputName->setFocus();
                return;
            }
        }

        // Check only for gc65xx since we currently do not connect to gw4xx for raw capture
        if (!checkConnectedDevices())
        {
            return;
        }

        // Listen for the launchComplete signal. If success, load the updated config
        QObject::connect(this, &ISPGui::launchComplete, this, [this](bool success) {
            if (success) {
                //ui.actionResetCamera->trigger();
            }

            disconnect(this, &ISPGui::launchComplete, 0, 0);
            ui.buttonRawCapture->setEnabled(true);
            ui.buttonRegenRGB->setEnabled(true);
        });

        if (scene != NULL)
        {
            delete scene;
            scene = NULL;
        }
        raw_capture = true;
        ui.buttonRegenRGB->setEnabled(false);
        ui.buttonRawCapture->setEnabled(false);
        launchApp(LaunchedApp::RawCapture);
    });

    QObject::connect(ui.buttonRegenRGB, &QPushButton::clicked, this, [this](bool) {
        if (ui.editRawCapOutputName->text().trimmed().length() == 0)
        {
            QMessageBox::warning(this, tr("Error"), tr("Please fill in output name"));
            return;
        }

        // Listen for the launchComplete signal. If success, load the updated config
        QObject::connect(this, &ISPGui::launchComplete, this, [this](bool success) {
            if (success) {
                //ui.actionResetCamera->trigger();
            }

            disconnect(this, &ISPGui::launchComplete, 0, 0);
            ui.buttonRegenRGB->setEnabled(true);
            ui.buttonRawCapture->setEnabled(true);
        });

        raw_capture = false;
        ui.buttonRegenRGB->setEnabled(false);
        ui.buttonRawCapture->setEnabled(false);
        launchApp(LaunchedApp::BayerToRgb);
    });

    // Wait until GUI has appeared on screen...
    QTimer::singleShot(0, [this]() {
        // Select the first page
        ui.sectionStackedWidget->setCurrentIndex(0);

		// Set console edit control to black
		// Source:  https://wiki.qt.io/How_to_Change_the_Background_Color_of_QWidget
        ui.textEditConsole->setStyleSheet(
                "QTextEdit {"
					"background-color:black; "
				"},"
				"QTextEdit:ContextMenu {"
					"background-color: white;"
				"}"
		);

		logMsg(tr("Welcome to <i><b>ISP</b>Gui</i>."));

        if (!initDevJson())
        {
            ui.actionExit->trigger();
            return;
        }

        // Load override spec file specified in dev.json
        // If not specified, load embedded Spec file
        m_specFilePath = ":/ISPGui/default-spec.json";	// embedded spec

        // Init Spec file path
        QJsonObject root = m_devJsonDoc->object();
        QJsonObject::const_iterator specIterator = root.find("spec_override");
        QString overrideSpecFilePath = specIterator.value().toString();
        if (!overrideSpecFilePath.isEmpty())    // specified
        {
            replaceMacros(overrideSpecFilePath);
            m_specFilePath = QDir::fromNativeSeparators(overrideSpecFilePath);
            m_defaultSpecFilePath = m_specFilePath;
        }

		if (!loadSpecFile())		// spec was not successfully loaded
		{
            m_specFilePath.clear();
            forceLoadSpecFile(tr("ISPGui requires a Spec File to operate.\r\nClick OK to select, or Cancel to quit."));

            if (m_specFilePath.isEmpty())
            {
                // Abort without prompting for a Config file
                ui.actionExit->trigger();		// ... must shutdown
                return;
            }
		}

        // Init Help file path
        QJsonObject::const_iterator helpIterator = root.find("help_file");
        QString helpFilePath = QDir::fromNativeSeparators(helpIterator.value().toString());
        if (!helpFilePath.isEmpty())		// specified
        {
            replaceMacros(helpFilePath);
            m_helpFilePath = (helpFilePath);
        }

        populateDeviceTypeComboBox();
        if (ui.comboBoxTargetDevice->count() == 0)
        {
            logMsg(tr("No command-line binaries found"));
            QMessageBox::warning(this, tr("Error"), tr("ISP command-line binaries missing"));
            ui.actionExit->trigger();
            return;
        }

        ui.spinBoxComPort->setMinimum(0);
        ui.spinBoxComPort->setMaximum(numeric_limits<int>::max());

        ui.spinBoxBaudrate->setMinimum(0);
        ui.spinBoxBaudrate->setMaximum(numeric_limits<int>::max());

        ui.comboBoxParity->addItems(QStringList()<<"No parity"<<"Odd"<<"Even");

        ui.imageView->setStyleSheet("background: #000000; border: none;");

        ui.spinBoxCCMErrorMargin->setRange(0, 1);
        ui.spinBoxCCMErrorMargin->setDecimals(6);
        ui.spinBoxCCMErrorMargin->setSingleStep(0.005);
        ui.spinBoxCCMErrorMargin->setValue(0.01); // The default error margin

#ifdef WIN32
        ui.checkBoxDetectDevices->setChecked(true);
#else
        ui.checkBoxDetectDevices->setChecked(true);
#endif

        loadUserSettings();

        m_currentTargetDevice = ui.comboBoxTargetDevice->currentIndex();
        updateRawCaptureUI();

        QObject::connect(ui.comboBoxTargetDevice, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, [this] (int) {
            /*m_configFilePath.clear();
            forceLoadConfigFile(tr("Please select a %1 Config File to operate.\r\nClick OK to select, or Cancel to undo.").arg(ui.comboBoxTargetDevice->currentText()));
            if (m_configFilePath.isEmpty())		// user really didn't select a config file
            {
                ui.comboBoxTargetDevice->blockSignals(true);
                ui.comboBoxTargetDevice->setCurrentIndex(m_currentTargetDevice);
                ui.comboBoxTargetDevice->blockSignals(false);
                return;
            }*/

            checkConnectedDevices(false);

            ui.comboBoxConnType->blockSignals(true);
            populateConnectionTypeComboBox();
            updateConnectionTypeConfigBox();
            saveUserSettings();
            ui.comboBoxConnType->blockSignals(false);

            m_configFilePath.clear();
            ui.actionOpenConfig->trigger();

            m_currentTargetDevice = ui.comboBoxTargetDevice->currentIndex();
            updateRawCaptureUI();

            if (m_configFilePath.isEmpty())
            {
                logMsg(tr("Warning: No config selected"));
                resetUI();
            }
        });

        QObject::connect(ui.comboBoxConnType, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, [this] (int) {
            updateConnectionTypeConfigBox();
            saveUserSettings();

            checkConnectedDevices(false);
        });

        QObject::connect(ui.spinBoxComPort, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), this, [this] (int) {
            saveUserSettings();
        });

        QObject::connect(ui.spinBoxBaudrate, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), this, [this] (int) {
            saveUserSettings();
        });

        QObject::connect(ui.comboBoxParity, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, [this] (int) {
            saveUserSettings();
        });

        QObject::connect(ui.checkBoxProfile2, &QCheckBox::stateChanged, this, [this] (int) {
            saveUserSettings();
        });

        QObject::connect(ui.comboBoxVerbosity, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, [this] (int) {
            saveUserSettings();
        });

        QObject::connect(ui.spinBoxCCMErrorMargin, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), this, [this] (double value) {
            QSettings settings("GeoSemi","ISPGui");
            settings.setValue("setup/ccm", value);
        });

        ui.editFirmwareLocation->installEventFilter(this);
        ui.editRawCapJsonLocation->installEventFilter(this);

        setWindowTitle(QString("%1: No config selected").arg(ui.comboBoxTargetDevice->currentText()));
        ui.actionOpenConfig->trigger();
        if (m_configFilePath.isEmpty())
        {
            logMsg(tr("Warning: No config selected"));
        }

        checkConnectedDevices(false);
    });
}

ISPGui::~ISPGui()
{
    if (m_devJsonDoc)
        delete m_devJsonDoc;

    if (scene)
    {
        delete scene;
    }
}

// Window Events
void ISPGui::closeEvent(QCloseEvent *event)
{
    QSettings settings("GeoSemi", "ISPGui");
    settings.setValue("geometry", saveGeometry());
    QMainWindow::closeEvent(event);
}

void ISPGui::resizeEvent(QResizeEvent *)
{
    if (scene)
    {
        scene->setImage( QString(""), ui.imageView->rect());
    }
}

void ISPGui::logMsg(QString msg, LaunchedApp appType, bool updateLastLine)
{
    // We are outputting HTML text, so use HTML "line-breaks"
    if (msg.indexOf("\r\n") > -1 && msg.split("\r\n").length() > 2)
    {
        msg.replace(QRegularExpression("\r\n"), "<br/>");
    }
    else if (msg.indexOf("\n") > -1 && msg.split("\n").length() > 2)
    {
        msg.replace(QRegularExpression("\n"), "<br/>");
    }
    else
    {
        msg.replace(QRegularExpression("\r\n"), "");
        msg.replace(QRegularExpression("\n"), "");
    }

    const char *colors[] = {"white", "cyan", "lawngreen", "plum", "orchid", "linen", "lightcoral", "tan", "tomato", "orange", "plum", "orange", "plum" };
	Q_ASSERT(_countof(colors) == LaunchedApp::NUMAPPS);
    const char *color = (appType == LaunchedApp::IspGui) ? (msg.startsWith("error:", Qt::CaseInsensitive) ? "red" : (msg.startsWith("warning:", Qt::CaseInsensitive) ? "yellow" : colors[appType])) : colors[appType];

    QString prepend(QString("<font color=palegoldenrod>%1</font>&nbsp;<font color=%2>").arg(QTime::currentTime().toString(QLocale::system().timeFormat())).arg(color));
	QString postpend("</font>");
	QString final(prepend + msg + postpend);
    if (updateLastLine)
    {
        ui.textEditConsole->moveCursor( QTextCursor::End, QTextCursor::MoveAnchor );
        ui.textEditConsole->moveCursor( QTextCursor::StartOfLine, QTextCursor::MoveAnchor );
        ui.textEditConsole->moveCursor( QTextCursor::EndOfLine, QTextCursor::KeepAnchor );
        ui.textEditConsole->textCursor().removeSelectedText();
        ui.textEditConsole->moveCursor( QTextCursor::StartOfLine, QTextCursor::MoveAnchor );

        ui.textEditConsole->insertHtml(final);
    }
    else
    {
        ui.textEditConsole->append(final);		// "<b>Hello</b> my good <font color=blue>friend!"
    }

    // Scroll the console to bottom
    ui.textEditConsole->verticalScrollBar()->setValue(ui.textEditConsole->verticalScrollBar()->maximum());
}

void ISPGui::launchApp(LaunchedApp appType)
{
	// Read appPath and arguments for the specified appType and launch them
    if (!m_devJsonDoc)
        return;

    /*QSerialPort serial;
    serial.setPortName("COM13");
    serial.setBaudRate(QSerialPort::Baud115200);
    serial.setDataBits(QSerialPort::Data8);
    serial.setParity(QSerialPort::NoParity);
    serial.setStopBits(QSerialPort::OneStop);
    serial.setFlowControl(QSerialPort::NoFlowControl);
    if (serial.open(QIODevice::ReadWrite)) {
        logMsg(tr("Opened!"));
        int bytes = serial.write("~x,0,0,1280,720#");
        logMsg(tr("Bytes written: %1").arg(bytes));
        if (bytes != -1) {
            //char data[512];
            //int bytesToRead = serial.bytesAvailable();

            //qDebug() << "bytesToRead" << bytesToRead;
            serial.waitForReadyRead(1000*5);
            QByteArray byteArray = serial.readAll();
            qDebug() << byteArray.size();
            //int result = serial.read(data, bytesToRead);
            //qDebug() << "D: " << QString(QByteArray::fromRawData(data, bytesToRead));
            QString hex;
            for (int i = 0; i < byteArray.size(); i++) {
                hex.append((char)QString::number(byteArray.at(i), 16).toInt(NULL, 16));

                //qDebug() << (char)QString::number(byteArray.at(i), 16).toInt(NULL, 16);
            }

            qDebug() << hex;
            //qDebug() << QByteArray::fromHex(byteArray.toHex()).data();
            //qDebug() << "byteArray" << QString::fromUtf8(QByteArray::fromHex(hex.toUtf8().constData()));
            //logMsg(tr("Data %1").arg(QString(data)));
            //QByteArray byteData;
            //
            //byteData.append(serial.readAll());
            //logMsg(tr("Data %1").arg(QString(byteData)));
        }
    } else {
        logMsg(tr("Failed to open!"));
    }
    //serial.write("ok*");
    return;*/

    QJsonObject root = m_devJsonDoc->object();
	QJsonObject::const_iterator cmdlinesIterator = root.find("cmdlines");
	QJsonObject cmdlines = cmdlinesIterator.value().toObject();

	QJsonObject::const_iterator appIterator = cmdlines.find(launchedAppName(appType));
	QJsonObject app = appIterator.value().toObject();

	QJsonObject::const_iterator pathIterator = app.find("path");
	QString path = pathIterator.value().toString();

    if (path.isEmpty())
    {
        QMessageBox::warning(this, tr(""), tr("No action to be performed"));
        return;
    }
	replaceMacros(path);

	QStringList argList;
	QJsonObject::const_iterator argsIterator = app.find("arguments");
	QJsonArray arrArgs = argsIterator.value().toArray();
	foreach(const QJsonValue &arg, arrArgs)
	{
		QString argString(arg.toString());
		replaceMacros(argString);
        if (argString.compare(arg.toString()) == 0)
        {
            // The macro wasn't replaced
            continue;
        }
        if (argString.startsWith("--") && argString.indexOf(" ") > -1)
        {
            argList.append(argString.mid(0, argString.indexOf(" ")));
            argList.append(argString.mid(argString.indexOf(" ")+1));
        }
        else
        {
            argList.append(argString);
        }
    }

    launchApp(path, argList, appType);
}

void ISPGui::launchApp(const QString &appPath, QStringList &arguments, LaunchedApp appType)
{
    // Log
    logMsg(tr("Launching %1").arg(QDir::fromNativeSeparators(appPath)), appType);
	foreach(auto argument, arguments)
	{
		logMsg(QString("&nbsp;&nbsp;%1").arg(argument), appType);
	}

    QProcess *process = new QProcess(this);
    process->setProcessChannelMode(QProcess::MergedChannels);		// merge stdout and stderr

	// Log all stdout and stderr
    QObject::connect(process, &QProcess::readyReadStandardOutput, this, [this, appType]() {
        QProcess *process = dynamic_cast<QProcess*> (sender());
        QString output(process->readAllStandardOutput());

        static bool wasDataBlock = false;
        if (appType == RawCapture)
        {
            logMsg(output, appType, wasDataBlock && output.indexOf("recv: data block") > -1);
            wasDataBlock = output.indexOf("recv: data block") > -1;
        }
        else
        {
            logMsg(output, appType);
        }

        if (appType == RawCapture || appType == BayerToRgb)
        {
            // Check for tiff file name
            QStringList lines = output.split("\n");
            foreach (QString line, lines)
            {
                if (line.indexOf(".tiff") > -1)
                {
                    if (scene == NULL)
                    {
                        scene = new ImageInfoScene( ui.imageView->rect().width(), ui.imageView->rect().height() );
                        connect (scene, &ImageInfoScene::signalHistogramMean, this, [this](qreal mean){
                            ui.labelHistogramMean->setText( QString::number(mean, 'f', 2) );
                        });

                        ui.imageView->setScene( scene );
                    }

                    scene->addSelectionRectangles( );
                    scene->setImage( line.trimmed(), ui.imageView->rect() );
                }
            }
        }
	});

	// Handle process error
	// Source: http://tiku.io/questions/2399638/qt-connect-function-signal-disambiguation-using-lambdas
	void (QProcess::* errorOverloadPtr)(QProcess::ProcessError error) = &QProcess::error;
    QObject::connect(process, errorOverloadPtr, this, [this, appType](QProcess::ProcessError error) {
        QProcess *process = dynamic_cast<QProcess*> (sender());
        if (error == QProcess::FailedToStart)
        {
            if (process->errorString().contains("No such file or directory", Qt::CaseInsensitive))
            {
                logMsg(tr("Error: Process failed. Please make sure Python 2.7 is installed and defined in the PATH."));
            }
        }
        else
        {
            logMsg(tr("Process error %1").arg(process->errorString()), appType);
        }
	});

	// When finished, log exit code and cleanup
	void (QProcess::* finishedOverloadPtr)(int exitCode) = &QProcess::finished;
    QObject::connect(process, finishedOverloadPtr, this, [this, appType](int exitCode) {
        QProcess *process = dynamic_cast<QProcess*> (sender());
		logMsg(tr("Ended with exit code: %1").arg(exitCode), appType);
		process->deleteLater();

        emit launchComplete(exitCode == 0);

        if (exitCode != 0)
        {
            QString appName = launchedAppName(appType).replace("_", " ");
            Util::capitalize(appName);
            logMsg(tr("Error: %1 failed").arg(appName));
            QMessageBox::warning(this, tr("Error"), tr("%1 failed").arg(appName));
        }
    });

    // Start process
    if (appPath.endsWith(".py"))
    {
        arguments.insert(0, appPath);
        process->start("python", arguments);
    }
    else
    {
        process->start(appPath, arguments);
    }
}

void ISPGui::forceLoadSpecFile(QString message)
{
    // Force user to load a spec file
    QMessageBox::StandardButton userResponse;
    do
    {
        ui.actionLoadSpec->trigger();		// clicks the Load Spec menu button, shows File Open dialog for user to select spec file

        // We're done if user selected a spec file and we successfully loaded it
        if (!m_specFilePath.isEmpty())
            break;		// we are done

        // Warn user we are going to exit if he doesn't select a spec file
        userResponse = QMessageBox::warning(this, tr("Select Spec File"),
            message,
            QMessageBox::StandardButton::Ok | QMessageBox::StandardButton::Cancel);

    } while (userResponse == QMessageBox::StandardButton::Ok);
}

bool ISPGui::loadSpecFile()
{
    if (m_specFilePath.isEmpty())
    {
        return false;
    }

	QFile specFile(m_specFilePath);
	specFile.open(QIODevice::ReadOnly | QIODevice::Text);
	QByteArray contents = specFile.readAll();
	QJsonParseError error;
	QJsonDocument specDoc = QJsonDocument::fromJson(contents, &error);
	if (error.error != QJsonParseError::NoError)
	{
        logMsg(tr("Failed to load spec: %1").arg(m_specFilePath));
		QString sourceFragment = (error.offset == contents.length()) ? "<eof>" : contents.mid(error.offset, 80);
		QMessageBox::warning(this, tr("JSON error"), tr("File: %1\r\nError: %2\r\nLocation: %3").arg(m_specFilePath).arg(error.errorString()).arg(sourceFragment));
		return false;
	}

    QJsonObject root = specDoc.object();

    // Parse the Spec version and show in status bar
    QJsonObject::const_iterator vers = root.find("spec_id");
    if (vers == root.end())
    {
        logMsg(tr("Error: Invalid spec"));
        QMessageBox::warning(this, tr("Error"), tr("Invalid spec: %1").arg(m_specFilePath));
        return false;
    }

    clearSubsections();

    bool ok = addSubsections(root);

	m_specVersion = vers.value().toInt();
    m_specMinorVersionMax = root["version"].toObject()["minor"].toObject()["max"].toInt();
    m_specMinorVersionMin = root["version"].toObject()["minor"].toObject()["min"].toInt();
    m_specMajorVersionMax = root["version"].toObject()["major"].toObject()["max"].toInt();
    m_specMajorVersionMin = root["version"].toObject()["major"].toObject()["min"].toInt();
	m_pVersionLabel->setText(tr("<html><b>Spec Id</b> %1</html>").arg(m_specVersion));

    if (m_defaultSpecFilePath.compare(m_specFilePath) == 0)
    {
        logMsg(tr("Loaded default spec: %1").arg(m_specFilePath));
    }
    else
    {
        logMsg(tr("Loaded spec: %1").arg(m_specFilePath));
        logMsg(tr("Application will load default spec on relaunch: %1").arg(m_defaultSpecFilePath));
    }
	return ok;
}

void ISPGui::forceLoadConfigFile(QString message)
{
    // Force user to load a config file
    QMessageBox::StandardButton userResponse;
    do
    {
        ui.actionOpenConfig->trigger();		// clicks the Open Config toolbar button, shows File Open dialog for user to select config file

        // We're done if user selected a config file and we successfully loaded it
        if (!m_configFilePath.isEmpty())
            break;		// we are done

        // Warn user we are going to exit if he doesn't select a config file
        userResponse = QMessageBox::warning(this, tr("Select Config File"),
            message,
            QMessageBox::StandardButton::Ok | QMessageBox::StandardButton::Cancel);

    } while (userResponse == QMessageBox::StandardButton::Ok);
}

void ISPGui::loadConfigFile()
{
    if (m_configFilePath.isEmpty())
    {
        return;
    }

	// Process Config file
	QFile configFile(m_configFilePath);
	configFile.open(QIODevice::ReadOnly | QIODevice::Text);
	QByteArray contents = configFile.readAll();
	QJsonParseError error;
	QJsonDocument configDoc = QJsonDocument::fromJson(contents, &error);
	if (error.error != QJsonParseError::NoError)
	{
		// No valid config file loaded
        logMsg(tr("Failed to load config: %1").arg(QDir::fromNativeSeparators(m_configFilePath)));
		QString sourceFragment = (error.offset == contents.length()) ? "<eof>" : contents.mid(error.offset, 80);
		QMessageBox::warning(this, tr("JSON error"), tr("File: %1\r\nError: %2\r\nLocation: %3").arg(m_configFilePath).arg(error.errorString()).arg(sourceFragment));

		m_configFilePath.clear();		// the config file path is not valid since file was not successfully loaded
	}
    else
    {
        logMsg(tr("Loaded config: %1").arg(QDir::fromNativeSeparators(m_configFilePath)));
    }

    setWindowFilePath(m_configFilePath);
    setWindowTitle(QString("%1: %2").arg(ui.comboBoxTargetDevice->currentText()).arg(QFileInfo(m_configFilePath).fileName()));
	setWindowModified(false);

	// Set UI controls with values from Config file
    stuffConfig(configDoc.object());

    m_binaryLoaded = m_configFilePath.endsWith(".bin");
}

void ISPGui::clearSubsections()
{
	// Clear subsection tree
	ui.subsectionTree->clear();

	// Clear stacked widget pages
	while (QWidget *pageWidget = ui.subsectionStackedWidget->widget(0))
	{
		ui.subsectionStackedWidget->removeWidget(pageWidget);	// presumably this deletes the widgets for each parameter in the page
		delete pageWidget;
	}
}

// Find the key in a JSON object and return its value
QJsonValue ISPGui::findJsonValue(QString key, QJsonObject jsonObject)
{
    QJsonValue jValue = jsonObject.value(key);
    if (jValue != QJsonValue::Undefined)
    {
        // Found key at first level
        return jValue;
    }

    // search for key recursively
    foreach (QString tkey, jsonObject.keys())
    {
        jValue = findJsonValue(key, jsonObject.value(tkey).toObject());
        if (jValue != QJsonValue::Undefined)
        {
            return jValue;
        }
    }

    return QJsonValue::Undefined;
}

void ISPGui::resetUI()
{
    setWindowFilePath("");
    setWindowTitle(QString("%1: No config selected").arg(ui.comboBoxTargetDevice->currentText()));
    setWindowModified(false);

    m_configMajorVersion = -1;
    m_configMinorVersion = -1;

    int numSections = ui.subsectionTree->topLevelItemCount();
    for (int i = 0; i < numSections; i++)
    {
        // Iterate parameters of the subsection
        auto pageWidget = ui.subsectionStackedWidget->widget(i);
        const QObjectList &params = pageWidget->children();
        foreach(QObject *object, params)
        {
            // Skip disabled controls - they were not initialized from a Config file
            QWidget *control = dynamic_cast<QWidget *>(object);
            if (!control || !control->isEnabled())
                continue;

            // Skip empty parameter names
            QString objectName = control->objectName();	// <subsection>:<parameter>
            QString paramName = objectName.mid(objectName.indexOf(":") + 1);
            if (paramName.isEmpty())
                continue;

            auto comboBox = dynamic_cast<QComboBox *>(control);
            auto spinBox = dynamic_cast<QSpinBox *>(control);
            auto doubleSpinBox = dynamic_cast<QDoubleSpinBox *>(control);
            auto table = dynamic_cast<ISPTableWidget *>(control);
            if (comboBox || spinBox || doubleSpinBox || table)
            {
                control->setDisabled(true);
                normalizeLabelForControl(control);
            }
        }
    }
}

bool ISPGui::addSubsections(QJsonObject subsections)
{
	// Add item for each subsection
	QJsonObject::const_iterator s;
	for (s = subsections.begin(); s != subsections.end(); ++s)
	{
		// Add item to Subsection tree.  Item is top-level.
		QString name(s.key());

		// Ignore top level items that aren't subsections
		if (name == "spec_name" || name == "spec_id" || name == "version")
			continue;

		auto subsectionItem = new QTreeWidgetItem(QStringList() << name);
		ui.subsectionTree->addTopLevelItem(subsectionItem);

		// Add page for this subsection
		auto pageWidget = new QWidget;
		auto pageLayout = new QFormLayout;
		pageWidget->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
		pageLayout->setContentsMargins(-1, 0, 0, 0);
		pageLayout->setLabelAlignment(Qt::AlignRight | Qt::AlignVCenter);
		pageLayout->setVerticalSpacing(8);
        pageWidget->setLayout(pageLayout);

		// Add control for each field of subsection

		if (s.value().isObject())
		{
			auto paramsObject = s.value().toObject();
			QJsonObject::const_iterator p;
			for (p = paramsObject.begin(); p != paramsObject.end(); ++p)
			{
				QString paramName(p.key());
				if (p.value().isObject())
				{
					auto paramObject = p.value().toObject();
					QString paramDescription = (paramObject.contains("description")) ? paramObject.value("description").toString().trimmed() : "";
					if (!paramDescription.isEmpty())
						paramDescription += "<br/>";

					if (paramObject.contains("id"))
					{
						QWidget *editor = nullptr;
						auto idValue = paramObject.value("id");
						if (idValue.isArray())		// Array parameter
						{
                            QRegExp regex(QString("usr_[1-9]_corr_tbl"), Qt::CaseSensitive, QRegExp::RegExp);
                            bool displayHexAsDecimal = regex.exactMatch(paramName);

							// Create table widget with max_rows rows and appropriate number of columns
							int minRows = paramObject.value("min_rows").toInt(-1);
							if (minRows < 0)
							{
								QMessageBox::warning(this, tr("Invalid Spec File"), tr("<html>The JSON Spec file <i>%1</i>, <b>%2</b>:<b>%3</b> does not have required field:  <i>min_rows</i>.  ISPGui will now exit.</html>").arg(m_specFilePath).arg(name).arg(paramName));

								// Clear the subsections that have already been loaded to indicate failure
								// No longer necessary since false is returned to indicate the same thing
								//clearSubsections();

								// Post a quit message, it doesn't take effect immediately
								ui.actionExit->trigger();
								return false;
							}

							int maxRows = paramObject.value("max_rows").toInt(2);
							Q_ASSERT(maxRows > 0);

							// MAX and MIN values for each column
							QList<bool>	controlHexList;
                            auto minMaxList = new QList < QPair<double, double> >;
                            auto allowDecimalList = new QList < bool >;
							QJsonArray minArray = paramObject.value("min").toArray();
							QJsonArray maxArray = paramObject.value("max").toArray();
                            QJsonArray typeArray = paramObject.value("type").toArray();
							Q_ASSERT((minArray.count() == maxArray.count()) && (minArray.count() > 0));

							int numElems = minArray.count();
							for (int i = 0; i < numElems; i++)
							{
								// If Min is a string, convert it to hex, otherwise it is a decimal
                                auto minValue = minArray.at(i);
                                double min = (minValue.isString()) ? minValue.toString().toInt(nullptr, 16) : minValue.toDouble(numeric_limits<double>::min());
                                if (displayHexAsDecimal)
                                {
                                    QString conv;
                                    Util::fixedPointHexToDec("0xFFFF", conv);
                                    min = conv.toDouble(nullptr);
                                }
                                Q_ASSERT(min != numeric_limits<double>::min());

								// If Max is a string, convert it to hex, otherwise it is a decimal
                                auto maxValue = maxArray.at(i);
                                double max = (maxValue.isString()) ? maxValue.toString().toInt(nullptr, 16) : maxValue.toDouble(numeric_limits<double>::min());
                                if (displayHexAsDecimal)
                                {
                                    QString conv;
                                    Util::fixedPointHexToDec("0x7FFF", conv);
                                    max = conv.toDouble(nullptr);
                                }
                                Q_ASSERT(max != numeric_limits<double>::min());

                                minMaxList->append(QPair<double,double>(min, max));

								// If Max starts with "0x", interpret this to mean the control should be in Hex
								bool controlHex = maxValue.isString() && maxValue.toString().startsWith("0x", Qt::CaseInsensitive);
                                if (displayHexAsDecimal)
                                {
                                    // if displayHexAsDecimal, do not treat hex as hex. The values will be displayed as decimal
                                    controlHex = false;
                                }
                                controlHexList.append(controlHex);

                                auto type = typeArray.at(i);
                                allowDecimalList->append(s_floatControlTypes.contains(type.toString()));
							}

							QJsonArray headerArray = paramObject.value("dim_name").toArray();
							int numCols = headerArray.count();
							Q_ASSERT(numCols == numElems);
							int numValidCells = maxRows * numCols;

							// 1D arrays have "pitch" columns
							int pitch = paramObject.value("pitch").toInt(-1);
							if (pitch > 0)
							{
								numCols = pitch;
								maxRows = ceil( (double) maxRows / numCols);
                            }

                            auto tableWidget = new ISPTableWidget(minMaxList, allowDecimalList, displayHexAsDecimal);
							tableWidget->setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::MinimumExpanding));
							tableWidget->setSizeAdjustPolicy(QAbstractScrollArea::SizeAdjustPolicy::AdjustToContents);
							tableWidget->setRowCount(maxRows);
							tableWidget->setProperty("minRows", minRows);
                            tableWidget->setProperty("displayHexAsDecimal", displayHexAsDecimal);
							tableWidget->setColumnCount(numCols);
							tableWidget->setProperty("originalNumCols", headerArray.count());
							tableWidget->setObjectName(QString("%1:%2").arg(name).arg(paramName));	// "<subsection>:<parameter>"

							tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);

							tableWidget->verticalHeader()->setDefaultAlignment(Qt::AlignCenter);

							tableWidget->horizontalHeader()->setStyleSheet(
								"QHeaderView::section {"
								"	background-color:#f8f8f8;"
								"	border-top: 0px solid silver;"
								"	border-left: 0px solid silver;"
								"	border-bottom: 1px solid silver;"
								"	border-right: 1px solid silver;"
								"}"
								""
								"QTableCornerButton::section {"
								"	border-top: 0px solid silver;"
								"    	border-left: 0px solid silver;"
								"	border-right: 1px solid silver;"
								"	border-bottom: 1px solid silver;"
								"}");

							tableWidget->verticalHeader()->setStyleSheet(
								"QHeaderView::section {"
								"	background-color:#f8f8f8;"
								"	border-top: 0px solid silver;"
								"	border-left: 0px solid silver;"
								"	border-bottom: 1px solid silver;"
								"	border-right: 1px solid silver;"
								"   padding-left: 5px;"
								"   padding-right: 5px;"
								"}"
								""
								"QTableCornerButton::section {"
								"	border-top: 0px solid silver;"
								"    	border-left: 0px solid silver;"
								"	border-right: 1px solid silver;"
								"	border-bottom: 1px solid silver;"
								"}");


							tableWidget->setStyleSheet(
								"alternate-background-color: #E5EBF8;"		// GEO blue
								"background-color: #FFFFFF;"
								);

							// Initialize column headers
							if (numCols == headerArray.count())
							{
								// headerArray contains a column label for each column; just use it
								for (int i = 0; i < headerArray.count(); i++)
								{
									auto header = headerArray.at(i);
									auto headerText = header.toString();
									tableWidget->setHorizontalHeaderItem(i, new QTableWidgetItem(headerText));
								}
							}
							else
							{
								// Assume 1D array with "pitch" cols - label should be headerArray.at(0) + "0", "1", etc.
								auto headerText = headerArray.count() > 0 ? headerArray.at(0).toString() : "";
								for (int i = 0; i < numCols; i++)
								{
									tableWidget->setHorizontalHeaderItem(i, new QTableWidgetItem( QString("%1 %2").arg(headerText).arg(i) ));

									// Shorten columns to be enough room for "0xabcd", as that seems to be the longest string edited in these
									// tables with 1D tables... 2D tables have "addr" and "val" which seem to have longer hex strings
									tableWidget->setColumnWidth(i, 60);
								}

								// Initialize row headers with 0 based offset
								for (int i = 0; i < maxRows; i++)
									tableWidget->setVerticalHeaderItem(i, new QTableWidgetItem(QString("%1").arg(i*numCols)));

								// Disable some cells in last rows, beyond numValidCells
								int firstInvalidCellCol = (numValidCells % numCols);
								if (firstInvalidCellCol == 0)
									firstInvalidCellCol = numCols;

								for (int i = firstInvalidCellCol; i < numCols; i++)
								{
									// use existing item, if it exists
                                    auto tableWidgetItem = tableWidget->takeItem(maxRows - 1, i);
									if (!tableWidgetItem)		// it doesn't exist
										tableWidgetItem = new QTableWidgetItem();
									tableWidgetItem->setFlags(tableWidgetItem->flags() & ~Qt::ItemIsEditable); // non editable
                                    tableWidget->setItem(maxRows-1, i, tableWidgetItem);
								}
							}

							// Disable first column named "addr"
							if (numCols > 0 && (headerArray.at(0).toString().compare("addr", Qt::CaseInsensitive) == 0))
							{
								for (int row = 0; row < maxRows; row++)
								{
									// use existing item, if it exists
                                    auto tableWidgetItem = tableWidget->takeItem(row, 0);
									if (!tableWidgetItem)		// it doesn't exist
										tableWidgetItem = new QTableWidgetItem();
									tableWidgetItem->setFlags(tableWidgetItem->flags() & ~Qt::ItemIsEditable); // non editable
									tableWidget->setItem(row, 0, tableWidgetItem);
								}
							}

							// Set alignment and tooltip of each cell
							for (int row = 0; row < maxRows; row++)
							{
								for (int col = 0; col < numCols; col++)
								{
									// use existing item, if it exists
                                    auto tableWidgetItem = tableWidget->takeItem(row, col);
                                    if (!tableWidgetItem)		// it doesn't exist
                                        tableWidgetItem = new QTableWidgetItem();
									tableWidgetItem->setTextAlignment(Qt::AlignCenter);		// center text

									// Get min and max for the column
									int c = col;
									if ((c >= minMaxList->count()) && (minMaxList->count() == 1))		// 1D array with "pitch" columns
										c = 0;		// all columns use same min/max specified in the 1 column
									Q_ASSERT(c < minMaxList->count());
                                    QPair<double, double> minMax = minMaxList->at(c);
                                    double min = minMax.first;
                                    double max = minMax.second;

									Q_ASSERT(c < controlHexList.count());
									bool controlHex = controlHexList.at(c);

                                    QString tooltip;
                                    QString controlType = paramObject.value("type").toArray()[c].toString();
									formatTooltip(
                                        displayHexAsDecimal?"double":controlType, // controlType - doesn't matter (except if displayHexAsDecimal), float or double is not supported by QLineEdit
                                        controlHex,
										min,
										max,
										paramDescription,
										tooltip);
                                    tableWidgetItem->setToolTip(tooltip);

                                    tableWidget->setItem(row, col, tableWidgetItem);
								}
							}

							editor = tableWidget;
						}
						else
						{
							// Scalar parameter
							QString controlType = paramObject.value("type").toString();

							bool controlHex;				// whether control value is shown/edited in hex or not
                            double controlMin, controlMax;		// Min value, Max value
							QString tooltipString;
							parseMinMax(paramObject.value("min"), paramObject.value("max"),
										controlType, controlHex,
										controlMin, controlMax,
										paramDescription, tooltipString);

							// Add specific control, based on type
							if (s_intControlTypes.contains(controlType))	// Scalar int
							{
								if (controlMax <= 10)		// ComboBox
								{
									auto comboBox = new QComboBox(pageWidget);
									comboBox->setObjectName(QString("%1:%2").arg(name).arg(paramName));
									comboBox->setMinimumSize(QSize(80, 16777215));
									comboBox->setMaximumSize(QSize(80, 16777215));

									// Add possible values to combobox
									QStringList comboStrings;
                                    for (int i = (int)controlMin; i <= (int)controlMax; i++)
									{
										comboStrings.append(QString().setNum(i));
									}

									comboBox->insertItems(0, comboStrings);
									comboBox->setProperty("originalValue", comboBox->currentIndex());
									comboBox->installEventFilter(this);
                                    comboBox->setToolTip(tooltipString);
									editor = comboBox;
								}
								else                      	// SpinBox
								{
									auto spinBox = new QSpinBox(pageWidget);
									spinBox->setObjectName(QString("%1:%2").arg(name).arg(paramName));
									spinBox->setMinimumSize(QSize(80, 16777215));
									spinBox->setMaximumSize(QSize(80, 16777215));
                                    spinBox->setRange((int)controlMin, (int)controlMax);		// respect Min/Max
									spinBox->setToolTip(tooltipString);
									spinBox->setDisplayIntegerBase((controlHex) ? 16 : 10);
									if (controlHex)
										spinBox->setPrefix("0x");
									spinBox->setProperty("originalValue", spinBox->value());
									spinBox->installEventFilter(this);
									editor = spinBox;
								}
							}
							else if (s_floatControlTypes.contains(controlType))		// Scalar float
							{
								// DoubleSpinBox
								auto doubleSpinBox = new QDoubleSpinBox(pageWidget);
								doubleSpinBox->setObjectName(QString("%1:%2").arg(name).arg(paramName));
								doubleSpinBox->setMinimumSize(QSize(80, 16777215));
								doubleSpinBox->setMaximumSize(QSize(80, 16777215));
								doubleSpinBox->setRange(controlMin, controlMax);		// respect Min/Max
								doubleSpinBox->setToolTip(tooltipString);
								doubleSpinBox->setProperty("originalValue", doubleSpinBox->value());
								doubleSpinBox->installEventFilter(this);
                                doubleSpinBox->setDecimals(c_doublePrecision);
								editor = doubleSpinBox;
							}
						}

						// Editor control is disabled until config file is loaded and stuffConfig() sets value
						Q_ASSERT(editor);
                        editor->setDisabled(true);

						// Create this row's label - when it is clicked, click to enable control
						auto labelText = "<html><a href=\"x\">" + paramName + "</a></html>";
						auto label = new QLabel(labelText, pageWidget);
						auto labelObjectName = QString("label-%1").arg(editor->objectName());
						label->setObjectName(labelObjectName);
						label->setProperty("paramName", paramName);		// so stuffConfig can set this to be the label text and take out the hyperlink anchor

						QString disabledTooltip = tr("<html>This parameter is <b>not set</b> in your Config file.&nbsp;&nbsp;It will <b>not be written</b> when you save the Config file.&nbsp;&nbsp;<b>Click to enable</b> this parameter to set it and save it.</html>");
						QString enabledTooltip = tr("<html>This parameter is <b>not set</b> in your Config file.&nbsp;&nbsp;It <b>has been enabled and will be written</b> when you save the Config file.&nbsp;&nbsp;<b>Click to disable</b> this parameter.</html>");
						label->setToolTip(disabledTooltip);

						// When label is clicked:
						QObject::connect(label, &QLabel::linkActivated, this, [label, editor, disabledTooltip, enabledTooltip](const QString & /*link*/) {
							// Toggle editor enabled state
							bool enabled = !editor->isEnabled();
							editor->setEnabled(enabled);

                            // Set the control's "new" property.
                            // True -> This control is newly enabled, and is flagged to be set to the camera the next time the user hits "Set Camera"
                            editor->setProperty("new", enabled);

							// Change label tooltip
							label->setToolTip((enabled) ? enabledTooltip : disabledTooltip);
						});

                        if (idValue.isArray()) // Add a "load" link button for tables
                        {
                            auto labelContainer = new QWidget;
                            labelContainer->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
                            labelText = "<html><a href=\"x\">load</a></html>";
                            auto labelLoad = new QLabel(labelText, labelContainer);
                            labelLoad->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

                            auto table = (ISPTableWidget*) editor;
                            int minRows = table->property("minRows").toInt();
                            int originalNumCols = table->property("originalNumCols").toInt();
                            int numCols = table->columnCount();
                            int numRows = table->rowCount();

                            if (numCols == originalNumCols)
                            {
                                QString row = "[";
                                for (int i = 1; i <= numCols; i++)
                                {
                                    row.append(tr("val%1, ").arg(i));
                                }
                                row = row.simplified();
                                row = row.mid(0, row.lastIndexOf(","));
                                row.append("]");
                                labelLoad->setToolTip(tr("<html>Load this table from a JSON file. File <strong>must</strong> contain<br/>&quot;%1&quot;: [<br/>%2,<br/>%3,<br/>...,<br/>min %4 or max %5 elements<br/>]</q></html>").arg(paramName).arg(row).arg(row).arg(minRows).arg(numRows));
                            }
                            else
                            {
                                // 1D arrays
                                labelLoad->setToolTip(tr("<html>Load this table from a JSON file. File <strong>must</strong> contain<br/>&quot;%1&quot;: [<br/>val1, val2, ..., min %2 or max %3 elements<br/>]</q></html>").arg(paramName).arg(minRows).arg(numRows*numCols));
                            }

                            // Handle a click on the "load" link button
                            QObject::connect(labelLoad, &QLabel::linkActivated, this, [&, editor, paramName](const QString & /*link*/) {
                                // Stop editing
                                if (editor->isEnabled())
                                {
                                    ISPTableWidget::stopEdit();
                                }

                                QString tableFileName = QFileDialog::getOpenFileName(this, tr("Open json file"), m_tableFilePath, tr("JSON files (*.json);; All files(*.*)"));
                                if (!tableFileName.isEmpty())
                                {
                                    m_tableFilePath = QString(QFileInfo(tableFileName).absoluteDir().absolutePath());

                                    QFile tableFile(tableFileName);
                                    tableFile.open(QIODevice::ReadOnly | QIODevice::Text);
                                    QByteArray contents = tableFile.readAll();
                                    QJsonParseError error;
                                    QJsonDocument doc = QJsonDocument::fromJson(contents, &error);
                                    if (error.error != QJsonParseError::NoError)
                                    {
                                        // Invalid JSON file
                                        logMsg(tr("Failed to load json: %1").arg(tableFileName));
                                        QString sourceFragment = (error.offset == contents.length()) ? "<eof>" : contents.mid(error.offset, 80);
                                        QMessageBox::warning(this, tr("JSON error"), tr("File: %1\r\nError: %2\r\nLocation: %3").arg(tableFileName).arg(error.errorString()).arg(sourceFragment));
                                    }
                                    else
                                    {
                                        logMsg(tr("Loaded json: %1").arg(tableFileName));
                                        QJsonObject jsonObject = doc.object();
                                        QJsonValue jValue = findJsonValue(paramName, jsonObject);
                                        if (jValue != QJsonValue::Undefined)
                                        {
                                            if (jValue.isArray())
                                            {
                                                logMsg(tr("Found table: %1").arg(paramName));
                                                stuffTable(dynamic_cast<ISPTableWidget*>(editor), jValue.toArray());
                                            }
                                            else
                                            {
                                                logMsg(tr("Did not find table: %1").arg(paramName));
                                                QMessageBox::warning(this, tr("Error"), tr("Table not found: %1").arg(paramName));
                                            }
                                        }
                                        else
                                        {
                                            logMsg(tr("Did not find table: %1").arg(paramName));
                                            QMessageBox::warning(this, tr("Error"), tr("Table not found: %1").arg(paramName));
                                        }
                                    }
                                }
                            });

                            auto labelLayout = new QVBoxLayout;
                            labelLayout->setContentsMargins(0, 0, 0, 0);
                            labelLayout->setSpacing(5);

                            labelLayout->addWidget(label);
                            labelLayout->addWidget(labelLoad);

                            labelContainer->setLayout(labelLayout);
                            pageLayout->addRow(labelContainer, editor);
                        }
                        else
                        {
                            pageLayout->addRow(label, editor);
                        }
					}
				}
			}
		}

		// Top align these items
		// Source:  http://stackoverflow.com/questions/30800551/qt-qscrollarea-align-added-widgets-to-top
		//pageLayout->addStretch(1);

        ui.subsectionStackedWidget->addWidget(pageWidget);
	}

	// Select first one
	ui.subsectionTree->setCurrentItem(ui.subsectionTree->topLevelItem(0));

	return true;
}

void ISPGui::clearConfig()
{
    for (int i = 0; i < ui.subsectionStackedWidget->count(); i++)
    {
        auto pageWidget = ui.subsectionStackedWidget->widget(i);
        for (int j = 0; j < pageWidget->children().count(); j++)
        {
            auto control = dynamic_cast<QWidget *>(pageWidget->children().at(j));
            if (control)
            {
                auto comboBox = dynamic_cast<QComboBox *>(control);
                auto spinBox = dynamic_cast<QSpinBox *>(control);
                auto doubleSpinBox = dynamic_cast<QDoubleSpinBox *>(control);
                auto table = dynamic_cast<ISPTableWidget *>(control);

                if (comboBox || spinBox || doubleSpinBox || table)
                {
                    if (table)
                    {
                        for (int row = 0; row < table->rowCount(); row++)
                        {
                            for (int col = 0; col < table->columnCount(); col++)
                            {
                                table->item(row, col)->setText("");
                                table->item(row, col)->setData(Qt::UserRole, "");
                            }
                        }
                    }
                    else if (spinBox)
                    {
                        spinBox->setValue(0);
                    }
                    else if (doubleSpinBox)
                    {
                        doubleSpinBox->setValue(0);
                    }
                    else if (comboBox)
                    {
                        comboBox->setCurrentIndex(0);
                    }

                    // Disable the control and set label back to link. This control will get set during stuffConfig
                    // if this parameter is present
                    control->setDisabled(true);
                    normalizeLabelForControl(control);

                    QString labelObjectName = QString("label-%1").arg(control->objectName());
                    auto label = pageWidget->findChild<QLabel *>(labelObjectName);
                    label->setText("<html><a href=\"x\">" + label->property("paramName").toString() + "</a></html>");
                    QString disabledTooltip = tr("<html>This parameter is <b>not set</b> in your Config file.&nbsp;&nbsp;It will <b>not be written</b> when you save the Config file.&nbsp;&nbsp;<b>Click to enable</b> this parameter to set it and save it.</html>");
                    label->setToolTip(disabledTooltip);
                }
            }
        }
    }
}

// Populate a table with an array
void ISPGui::stuffTable(ISPTableWidget* table, QJsonArray arr)
{
    // Iterate value array and stuff the table cells
    int rowNum = 0;
    int colNum = 0;
    const bool displayHexAsDecimal = table->property("displayHexAsDecimal").toBool();

    foreach(const QJsonValue &row, arr)
    {
        Q_ASSERT(rowNum < table->rowCount());	// ensure table is not overfilled

        if (row.isArray() && row.toArray().count() > 1)
        {
            // Stuff multiple columns
            QJsonArray cols = row.toArray();
            colNum = 0;

            foreach(const QJsonValue &col, cols)
            {
                Q_ASSERT(colNum < table->columnCount());
                QString val;

                if (col.isString())
                {
                    val = col.toString();	// "0x<hex>"
                }
                else if (col.isDouble())
                {
                    // Set the max number of significant digits (6 by default)
                    val = QString("%1").arg(col.toDouble(), 0, 'g', 10);
                }
                else
                {
                    val = QString("%1").arg(col.toInt());
                }

                // Use existing item, if it exists
                auto tableWidgetItem = table->takeItem(rowNum, colNum);
                if (!tableWidgetItem)
                    tableWidgetItem = new QTableWidgetItem();

                if (displayHexAsDecimal)
                {
                    // Convert 8.8-bit fixed-point hex to decimal
                    if (!Util::fixedPointHexToDec(val, val))
                    {
                        logMsg(tr("Error (%1): Invalid value \"%2\". Must be 8.8-bit fixed-point hex.").arg(table->objectName()).arg(val));
                    }
                }
                tableWidgetItem->setText(val);
                tableWidgetItem->setData(Qt::UserRole, val);
                table->setItem(rowNum, colNum, tableWidgetItem);

                colNum++;
            }
            rowNum++;
        }
        else    // row has 1 scalar value or an array containing 1 scalar value
        {
            QJsonValue jRow = row;
            if (jRow.isArray())
            {
                // If the value is a single element array, treat it as a scalar value
                jRow = jRow.toArray().at(0);
            }

            // Put the scalar value into the next available column of the table
            QString val;
            if (jRow.isString())
            {
                val = jRow.toString();	// "0x<hex>"
            }
            else if (jRow.isDouble())
            {
                // Set the max number of significant digits (6 by default)
                val = QString("%1").arg(jRow.toDouble(), 0, 'g', 10);
            }
            else
            {
                val = QString("%1").arg(jRow.toInt());
            }

            Q_ASSERT(!val.isEmpty());

            // use existing item, if it exists
            auto tableWidgetItem = table->takeItem(rowNum, colNum);
            if (!tableWidgetItem)		// it doesn't exist
                tableWidgetItem = new QTableWidgetItem();

            if (displayHexAsDecimal)
            {
                // Convert 8.8-bit fixed-point hex to decimal
                if (!Util::fixedPointHexToDec(val, val))
                {
                    logMsg(tr("Error (%1): Invalid value \"%2\". Must be 8.8-bit fixed-point hex.").arg(table->objectName()).arg(val));
                }
            }

            tableWidgetItem->setText(val);
            tableWidgetItem->setData(Qt::UserRole, val);

            table->setItem(rowNum, colNum++, tableWidgetItem);

            if (colNum >= table->columnCount())		// row is filled
            {
                // goto next row, first column
                rowNum++;
                colNum = 0;
            }
        }
    }
}

void ISPGui::stuffConfig(QJsonObject settings)
{
    // First clear the currently populated config's values (if any)
    clearConfig();

	// Add item for each subsection
	QJsonObject::const_iterator s;
	for (s = settings.begin(); s != settings.end(); ++s)
	{
		// Get name of top-level Subsection
		QString name(s.key());

		// Capture version to write to __setcamera.json
		if (name == "version")
		{
			auto versionObject = s.value().toObject();

			auto verMajorValue = versionObject.find("major").value();
			m_configMajorVersion = (verMajorValue.isString()) ? verMajorValue.toString().toInt() : verMajorValue.toInt();

			auto verMinorValue = versionObject.find("minor").value();
			m_configMinorVersion = (verMinorValue.isString()) ? verMinorValue.toString().toInt() : verMinorValue.toInt();

            validateConfigVersion();

			continue;
		}

		// find the correct top level item in the subsection tree
		bool foundSubsection = false;
		for (int i = 0; i < ui.subsectionTree->topLevelItemCount() && !foundSubsection; i++)
		{
			auto topLevelItem = ui.subsectionTree->topLevelItem(i);
			if (topLevelItem->text(0) == name)
			{
				// Get corresponding page in stacked widget
				auto pageWidget = ui.subsectionStackedWidget->widget(i);

				// Iterate parameters of the subsection
				if (s.value().isObject())
				{
					// Stuff this subsection's parameters
					auto paramsObject = s.value().toObject();
					QJsonObject::const_iterator p;
					for (p = paramsObject.begin(); p != paramsObject.end(); ++p)
					{
						// Get control for this parameter
						QString paramName(p.key());
						QString controlObjectName(QString("%1:%2").arg(name).arg(paramName));	// "<subsection>:<param>"
						auto control = pageWidget->findChild<QWidget *>(controlObjectName);
						if (control)
						{
							auto comboBox = dynamic_cast<QComboBox *>(control);
							auto spinBox = dynamic_cast<QSpinBox *>(control);
							auto doubleSpinBox = dynamic_cast<QDoubleSpinBox *>(control);
							auto table = dynamic_cast<ISPTableWidget *>(control);
							if (comboBox)               // control is a combo box
							{
								QString valueString = (p.value().isString()) ? p.value().toString() : QString("%1").arg(p.value().toInt(numeric_limits<int>::min()));
								int index = comboBox->findText(valueString);
								Q_ASSERT(index != -1);		// should be found in dialog
								comboBox->setCurrentIndex(index);
								comboBox->setProperty("originalValue", index);
							}
							else if (spinBox)           // control is a spin box
							{
								int value = (p.value().isString()) ? p.value().toString().toInt(nullptr, 16) : p.value().toInt(numeric_limits<int>::min());
								Q_ASSERT(value != numeric_limits<int>::min());		// should not be default
								spinBox->setValue(value);
								spinBox->setProperty("originalValue", value);
							}
							else if (doubleSpinBox)     // control is a spinbox that shows a double
							{
								double value = p.value().toDouble(-1.0);
								Q_ASSERT(value != -1.0);		// should not be default
								doubleSpinBox->setValue(value);
								doubleSpinBox->setProperty("originalValue", value);
							}
							else if (table)				// control is a table
							{
								Q_ASSERT(p.value().isArray());
                                QJsonArray arr = p.value().toArray();
                                stuffTable(dynamic_cast<ISPTableWidget*>(table), arr);
							}

							// The control has been initialized with a value from the Config file.
                            // Enable control and normalize associated label
                            control->setEnabled(true);
                            normalizeLabelForControl(control);
						}
					}
				}

				// Stop searching
				foundSubsection = true;
			}
		}
	}
}

void ISPGui::saveUserSettings()
{
    QSettings settings("GeoSemi","ISPGui");
    settings.beginGroup("setup");
    settings.setValue("targetdevice",ui.comboBoxTargetDevice->currentText());
    settings.setValue("conntype",ui.comboBoxConnType->currentText());
    settings.setValue("comport",ui.spinBoxComPort->value());
    settings.setValue("baudrate",ui.spinBoxBaudrate->value());
    settings.setValue("parity",ui.comboBoxParity->currentIndex());
    settings.setValue("profile2",ui.checkBoxProfile2->isChecked()?1:0);
    settings.setValue("verbosity",ui.comboBoxVerbosity->currentIndex());
    settings.endGroup();
}

void ISPGui::loadUserSettings()
{
    QSettings settings("GeoSemi", "ISPGui");
    settings.beginGroup("setup");
    if (settings.contains("targetdevice"))
    {
        for (int index = 0; index < ui.comboBoxTargetDevice->count(); index++)
        {
            if (ui.comboBoxTargetDevice->itemText(index).compare(settings.value("targetdevice").toString(), Qt::CaseInsensitive) == 0)
            {
                ui.comboBoxTargetDevice->setCurrentIndex(index);
                break;
            }
        }
    }

    populateConnectionTypeComboBox();

    if (settings.contains("conntype"))
    {
        for (int index = 0; index < ui.comboBoxConnType->count(); index++)
        {
            if (ui.comboBoxConnType->itemText(index).compare(settings.value("conntype").toString(), Qt::CaseInsensitive) == 0)
            {
                ui.comboBoxConnType->setCurrentIndex(index);
                break;
            }
        }
    }

    updateConnectionTypeConfigBox();

    if (settings.contains("comport"))
    {
        ui.spinBoxComPort->setValue(settings.value("comport").toInt());
    }
    else
    {
        ui.spinBoxComPort->setValue(0);
    }

    if (settings.contains("baudrate"))
    {
        ui.spinBoxBaudrate->setValue(settings.value("baudrate").toInt());
    }
    else
    {
        ui.spinBoxBaudrate->setValue(115200);
    }

    if (settings.contains("parity"))
    {
        ui.comboBoxParity->setCurrentIndex(settings.value("parity").toInt());
    }
    else
    {
        ui.comboBoxParity->setCurrentIndex(0);
    }

    if (settings.contains("profile2"))
    {
        ui.checkBoxProfile2->setChecked(settings.value("profile2").toInt() == 1);
    }
    else
    {
        ui.checkBoxProfile2->setChecked(false);
    }

    if (settings.contains("verbosity"))
    {
        ui.comboBoxVerbosity->setCurrentIndex(settings.value("verbosity").toInt());
    }
    else
    {
        ui.comboBoxVerbosity->setCurrentIndex(0);
    }

    if (settings.contains("ccm"))
    {
        ui.spinBoxCCMErrorMargin->setValue(settings.value("ccm").toDouble());
    }
    else
    {
        ui.spinBoxCCMErrorMargin->setValue(0.01); // Default CCM error margin
    }

    settings.endGroup();
    logMsg(tr("Loaded user settings"));
}

bool ISPGui::saveConfigFile(const QString &configFilePath, bool writeUnchangedParameters)
{
    //logMsg(tr("Saving config: %1").arg(QDir::fromNativeSeparators(m_configFilePath)));
	// Abort if any table is invalid
	if (!completeTables())
    {
        logMsg(tr("Failed to save config: %1").arg(QDir::fromNativeSeparators(configFilePath)));
		return false;
    }

	QString json;
	json += "{\n";		// start of document

	// Version
	if (m_configMajorVersion >= 0 && m_configMinorVersion >= 0)
	{
		json += QString(
"\t\"version\": {\n  \
\t\t\"major\": %1,\n \
\t\t\"minor\": %2\n \
\t},\n\n").arg(m_configMajorVersion).arg(m_configMinorVersion);
	}
	else
	{
		logMsg("Warning:  No version number, did you remember to open a config file?");
	}


	// Sending to Camera if we don't write unchanged parameters
	// Reset original values in this case
    bool resetOriginalValues = false;// !writeUnchangedParameters;

	// Iterate subsections in the tree
	int numSections = ui.subsectionTree->topLevelItemCount();
	for (int i = 0; i < numSections; i++)
	{
		bool wroteJSONSubsection = false;

		auto topLevelItem = ui.subsectionTree->topLevelItem(i);
		QString subsectionName(topLevelItem->text(0));

		// Iterate parameters of the subsection
		auto pageWidget = ui.subsectionStackedWidget->widget(i);
		const QObjectList &params = pageWidget->children();
		int curParam = 0;
		foreach(QObject *object, params)
		{
			// Skip disabled controls - they were not initialized from a Config file
			QWidget *control = dynamic_cast<QWidget *>(object);
			if (!control || !control->isEnabled())
				continue;

            bool controlIsNew = control->property("new").isValid() && control->property("new").toBool();

			// Skip empty parameter names
			QString objectName = control->objectName();	// <subsection>:<parameter>
			QString paramName = objectName.mid(objectName.indexOf(":") + 1);
			if (paramName.isEmpty())				
				continue;

			// Get the value of the control, depending on what type of control it is
			QString valueString;
			auto comboBox = dynamic_cast<QComboBox *>(control);
			auto spinBox = dynamic_cast<QSpinBox *>(control);
			auto doubleSpinBox = dynamic_cast<QDoubleSpinBox *>(control);
			auto table = dynamic_cast<ISPTableWidget *>(control);
			if (comboBox)               // control is a combo box
			{
				int originalValue = comboBox->property("originalValue").toInt();
				int index = comboBox->currentIndex();
                if ((index != originalValue) || writeUnchangedParameters || controlIsNew)
				{
					if (resetOriginalValues)
						comboBox->setProperty("originalValue", index);		// set original value to the latest one
					valueString = comboBox->itemText(index);
					Q_ASSERT(!valueString.isEmpty());
					writeJSONSubsection(wroteJSONSubsection, subsectionName, json);
					json += QString("\t\t\"%1\": %2,\n").arg(paramName).arg(valueString);
				}
			}
			else if (spinBox)			// control is a spin box
			{
				int value = spinBox->value();
				int originalValue = spinBox->property("originalValue").toInt();

                if ((value != originalValue) || writeUnchangedParameters || controlIsNew)
				{
					if (resetOriginalValues)
						spinBox->setProperty("originalValue", value);		// set original value to the latest one
					valueString = (spinBox->displayIntegerBase() == 16) ? QString("\"0x%1\"").arg(value, 0, 16) : QString("%1").arg(value);
					Q_ASSERT(!valueString.isEmpty());

					writeJSONSubsection(wroteJSONSubsection, subsectionName, json);
					json += QString("\t\t\"%1\": %2,\n").arg(paramName).arg(valueString);
				}
			}
			else if (doubleSpinBox)			// control is a double spin box
			{
				double value = doubleSpinBox->value();
				double originalValue = doubleSpinBox->property("originalValue").toDouble();
                if ((value != originalValue) || writeUnchangedParameters || controlIsNew)
				{
					if (resetOriginalValues)
						doubleSpinBox->setProperty("originalValue", value);		// set original value to the latest one
					valueString = QString("%1").arg(value);
					Q_ASSERT(!valueString.isEmpty());

					writeJSONSubsection(wroteJSONSubsection, subsectionName, json);
					json += QString("\t\t\"%1\": %2,\n").arg(paramName).arg(valueString);
				}
			}
			else if (table)					// control is a table
			{
				// Determine if this subsection has changed
				int lastFilledRow = -1;
				int numRows = table->rowCount();
				int numCols = table->columnCount();
                bool tableChanged = false;
                bool displayHexAsDecimal = table->property("displayHexAsDecimal").toBool();

				for (int row = 0; row < numRows; row++)
				{
					for (int col = 0; col < numCols; col++)
					{
                        auto tableWidgetItem = table->item(row, col);
						if (tableWidgetItem)
						{
                            tableChanged = tableChanged || (tableWidgetItem->text() != tableWidgetItem->data(Qt::UserRole).toString());
							if (!tableWidgetItem->text().isEmpty())		// the item is non-blank
								lastFilledRow = row;		// thus the row is filled
						}
					}
				}

                if (tableChanged || writeUnchangedParameters || controlIsNew)
				{
					writeJSONSubsection(wroteJSONSubsection, subsectionName, json);
					json += QString("\t\t\"%1\": [\n").arg(paramName);
					bool writeArray = (table->property("originalNumCols").toInt() > 1);  // not 1D array
					for (int row = 0; row <= lastFilledRow; row++)
					{
						json += QString("\t\t\t");
						if (writeArray)
							json += QString("[ ");	// write opening '[' for this row of array values

						int numCols = table->columnCount();
						for (int col = 0; col < numCols; col++)
						{
                            auto tableWidgetItem = table->item(row, col);
							if (tableWidgetItem)
							{
                                valueString = tableWidgetItem->text();
								if (resetOriginalValues)
									tableWidgetItem->setData(Qt::UserRole, valueString);
								bool hex = valueString.startsWith("0x", Qt::CaseSensitivity::CaseInsensitive);
								if (!valueString.isEmpty())
								{
                                    if (hex)
                                    {
										json += QString("\"%1\", ").arg(valueString);
                                    }
                                    else if (displayHexAsDecimal)
                                    {
                                        if (!Util::decToFixedPointHex(valueString, valueString))
                                        {
                                            logMsg(tr("Failed to convert %1 to signed 8.8-bit fixed-point hex").arg(valueString));
                                        }
                                        json += QString("\"%1\", ").arg(valueString);
                                    }
									else
                                    {
                                        json += QString("%1, ").arg(valueString);
                                    }
								}
							}
						}

						if (writeArray)
							json += QString("],");	// write closing ']' for this row of array values
						json += QString("\n");	// write closing ']' for this row of array values
					}
					json += QString("\t\t]");
					json += QString(",\n");
				}
			}


			// Now that this control has been written, it is specified in the Config file
			// so ensure that it's label shows that
			normalizeLabelForControl(control);

			curParam++;
		}

		// End brace for the subsection	
		if (wroteJSONSubsection)
			json += QString("\t},\n");
	}

	json += "}\n";		// end of document

	// Remove commas "," before ']' and '}', as the JSON standard prohibits them
	json.replace(QRegularExpression(",(\\s*[}|\\]])"), "\\1");

	// Save file
#if 0
	WCHAR szJsonFilePath[MAX_PATH];
	GetModuleFileName(NULL, szJsonFilePath, _countof(szJsonFilePath));
	*PathFindFileName(szJsonFilePath) = 0;		// null terminate after the path's final '\'
	QString newConfigFilePath(QString().fromUtf16((const ushort *)szJsonFilePath));
	newConfigFilePath.append("new_config.json");
#endif

    QString configPath(configFilePath);
    if (m_binaryLoaded)
    {
        configPath = tempDir + "/" + QFileInfo(configPath).fileName().replace(".bin", ".json");
    }

    QFile newConfigFile(configPath);
	newConfigFile.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text);
	newConfigFile.write(json.toLatin1().constData());
	newConfigFile.close();

#ifndef WIN32
	Util::fixPermissions(configFilePath);
#endif

	// Update window title with this file, if we saved the config file entirely
	// Otherwise it is to Set Camera, which does not affect the displayed config file
	if (writeUnchangedParameters)
	{
		setWindowFilePath(configFilePath);
        QString fileName = QFileInfo(configFilePath).fileName();
        if (m_binaryLoaded)
        {
            fileName = fileName.replace(".json", ".bin");
        }
        setWindowTitle(QString("%1: %2").arg(ui.comboBoxTargetDevice->currentText()).arg(fileName));
		setWindowModified(false);
	}

    logMsg(tr("Saved config: %1").arg(QDir::fromNativeSeparators(configFilePath)));
	return true;
}

void ISPGui::writeJSONSubsection(bool &wroteSubsection, const QString &subsectionName, QString &json)
{
	if (!wroteSubsection)
	{
		json += QString("\t\"%1\": {\n").arg(subsectionName);
		wroteSubsection = true;
	}
}

bool ISPGui::initDevJson()
{
#ifdef WIN32
        WCHAR szJsonFilePath[MAX_PATH];

#ifdef Q_OS_WIN
	GetModuleFileName(NULL, szJsonFilePath, _countof(szJsonFilePath));
	*PathFindFileName(szJsonFilePath) = 0;		// null terminate after the path's final '\'
#endif
        QString devFilePath(QString().fromUtf16((const ushort *)szJsonFilePath));
        devFilePath.append("dev.json");
#else
        QString devFilePath(QDir::currentPath().append("/dev.json"));
#endif

	QFile devFile(devFilePath);
	if (devFile.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QByteArray contents = devFile.readAll();
		QJsonParseError error;

        if (m_devJsonDoc)
            delete m_devJsonDoc;

        m_devJsonDoc = new QJsonDocument(QJsonDocument::fromJson(contents, &error));
		if (error.error != QJsonParseError::NoError)
		{
            logMsg(tr("Failed to load %1").arg(QDir::fromNativeSeparators(devFilePath)));
			QString sourceFragment = (error.offset == contents.length()) ? "<eof>" : contents.mid(error.offset, 80);
			QMessageBox::warning(this, tr("JSON error"), tr("File: %1\r\nError: %2\r\nLocation: %3").arg(devFilePath).arg(error.errorString()).arg(sourceFragment));
            return false;
		}
        else
        {
            logMsg(tr("Loaded %1").arg(QDir::fromNativeSeparators(devFilePath)));
        }
        return true;
	}
	else
	{
		logMsg(tr("Warning: %1 file not found or could not be read").arg(QDir::fromNativeSeparators(devFilePath)));
        QMessageBox::warning(this, tr("Error"), tr("File missing: %1").arg(QDir::currentPath().append("/dev.json")));
        return false;
	}
}

void ISPGui::populateDeviceTypeComboBox()
{
    QStringList targetDeviceList;

    for (int device = 0; device < NUMDEVICES; device++)
    {
        QString deviceString = targetDeviceName((TargetDevice)device);
        QDir binDir = QString(QDir::currentPath()).append("/bin/%1").arg(deviceString.toLower());
        if (binDir.exists() &&
            binDir.entryInfoList(QDir::NoDotAndDotDot|QDir::AllEntries).count() > 0)
        {
            targetDeviceList << deviceString;
        }
    }
    ui.comboBoxTargetDevice->clear();
    ui.comboBoxTargetDevice->addItems(targetDeviceList);
}

void ISPGui::populateConnectionTypeComboBox()
{
    ui.comboBoxConnType->clear();
    if (ui.comboBoxTargetDevice->currentText().compare(targetDeviceName(GW4XX), Qt::CaseInsensitive) == 0 ||
        ui.comboBoxTargetDevice->currentText().compare(targetDeviceName(GW3XX), Qt::CaseInsensitive) == 0)
    {
        ui.comboBoxConnType->addItem(connectionTypeName(UART));
        ui.comboBoxConnType->addItem(connectionTypeName(BASIC_UART));
        ui.comboBoxConnType->addItem(connectionTypeName(I2C));
    }
    else if (ui.comboBoxTargetDevice->currentText().compare(targetDeviceName(GC65XX), Qt::CaseInsensitive) == 0)
    {
        ui.comboBoxConnType->addItem(connectionTypeName(USB));
    }
}

void ISPGui::updateConnectionTypeConfigBox()
{
    ConnectionType connType = connectionType(ui.comboBoxConnType->currentText());
    ui.groupBoxConnectionConfig->setTitle(tr("%1 Configuration").arg(ui.comboBoxConnType->currentText()));
    switch (connType)
    {
    case UART:
        ui.frameUartConfig->show();
        ui.frameBasicUartConfig->hide();
        ui.frameI2cConfig->hide();
        ui.groupBoxConnectionConfig->show();
        ((QFormLayout*)ui.groupBoxConnectionConfig->layout())->setVerticalSpacing(0);
        break;
    case BASIC_UART:
        ui.frameUartConfig->show();
        ui.frameBasicUartConfig->show();
        ui.frameI2cConfig->hide();
        ui.groupBoxConnectionConfig->show();
        ((QFormLayout*)ui.groupBoxConnectionConfig->layout())->setVerticalSpacing(5);
        break;
    case I2C:
        ui.frameI2cConfig->show();
        ui.frameUartConfig->hide();
        ui.frameBasicUartConfig->hide();
        ui.groupBoxConnectionConfig->show();
        ((QFormLayout*)ui.groupBoxConnectionConfig->layout())->setVerticalSpacing(0);
        break;
    default:
        ui.groupBoxConnectionConfig->hide();
        break;
    }
}

void ISPGui::replaceMacros(QString &specFilePath)
{
	// Replace <exepath>
	int index = specFilePath.indexOf("<exepath>/");
	if (index >= 0)
	{
#if defined(WIN32)
            WCHAR szExePath[MAX_PATH];
#ifdef Q_OS_WIN
            GetModuleFileName(NULL, szExePath, _countof(szExePath));
            *PathFindFileName(szExePath) = 0;		// null terminate after the path's final '\'
#endif

            specFilePath.replace(index, 10, QString().fromUtf16((const ushort *)szExePath));
#else
            specFilePath.replace(index, 9, QDir::currentPath());
#endif
	}

	// Replace <setcamera-json-path>
	index = specFilePath.indexOf("<setcamera-json-path>");
	if (index >= 0)
	{
        specFilePath.replace(index, 21, tempDir + "/__setcamera.json");
	}

    // Replace <current-json-path>
    index = specFilePath.indexOf("<current-json-path>");
    if (index >= 0)
    {
        if (m_binaryLoaded)
        {
            specFilePath = tempDir + "/" + QFileInfo(m_configFilePath).fileName().replace(".bin", ".json");
        }
        else
        {
            specFilePath = QString(m_configFilePath);
        }
    }

    // Replace <bin-path>
    index = specFilePath.indexOf("<bin-path>");
    if (index >= 0)
    {
        specFilePath = m_configFilePath;
    }

    // Replace <device-type>
    index = specFilePath.indexOf("<device-type>");
    if (index >= 0)
    {
        specFilePath.replace(index, 13, ui.comboBoxTargetDevice->currentText().toLower());
    }

    // Replace <connection-params>
    index = specFilePath.indexOf("<connection-params>");
    if (index >= 0)
    {
        QString params = "";
        QString connType;
        TargetDevice device = targetDevice(ui.comboBoxTargetDevice->currentText());
        switch (device)
        {
        case GW4XX:
        case GW3XX:
            params.append(targetDeviceArg(device)).append(" ");
            connType = connectionTypeArg(connectionType(ui.comboBoxConnType->currentText()));
            switch (connectionType(ui.comboBoxConnType->currentText()))
            {
            case UART:
                params.append(connType).append(" ");
                params.append(QString(" -b %1").arg(ui.spinBoxBaudrate->value()));
                params.append(QString(" -p %1").arg(ui.comboBoxParity->currentIndex()));
                break;
            case BASIC_UART:
                params.append(connType.arg(ui.spinBoxComPort->value())).append(" ");
                params.append(QString(" -b %1").arg(ui.spinBoxBaudrate->value()));
                params.append(QString(" -p %1").arg(ui.comboBoxParity->currentIndex()));
                break;
            case I2C:
                params.append(connType);
                if (ui.checkBoxProfile2->isChecked())
                {
                    params.append(" -2");
                }
                break;
            default:
                break;
            }
            break;
        case GC65XX:
            break;
        default:
            break;
        }
        specFilePath.replace(index, 19, QString(params));
    }

    // Replace <verbosity-level>
    index = specFilePath.indexOf("<verbosity-level>");
    if (index >= 0)
    {
        specFilePath.replace(index, 17, QString::number(ui.comboBoxVerbosity->currentIndex()));
    }

    // Replace <spec-file-path>
    index = specFilePath.indexOf("<spec-file-path>");
    if (index >= 0)
    {
        specFilePath = QString(m_specFilePath);
    }

    if (ui.comboBoxTargetDevice->currentText().compare(targetDeviceName(GC65XX), Qt::CaseInsensitive) == 0)
    {
        // Replace <raw-firmware>
        index = specFilePath.indexOf("<raw-firmware>");
        if (index >= 0)
        {
            specFilePath.replace(index, QString("<raw-firmware>").length(), QString("--raw-firmware %1").arg(ui.editFirmwareLocation->text()));
        }

        // Replace <raw-cap-json>
        index = specFilePath.indexOf("<raw-json>");
        if (index >= 0)
        {
            specFilePath.replace(index, QString("<raw-json>").length(), QString("--raw-json %1").arg(ui.editRawCapJsonLocation->text()));
        }

        // Replace <raw-exp>
        index = specFilePath.indexOf("<raw-exp>");
        if (index >= 0)
        {
            specFilePath.replace(index, QString("<raw-exp>").length(), QString("--exposure %1").arg(ui.editRawCapExp->text()));
        }

        // Replace <raw-gain>
        index = specFilePath.indexOf("<raw-gain>");
        if (index >= 0)
        {
            specFilePath.replace(index, QString("<raw-gain>").length(), QString("--gain %1").arg(ui.editRawCapGain->text()));
        }

        // Replace <raw-flip>
        index = specFilePath.indexOf("<raw-flip>");
        if (index >= 0)
        {
            specFilePath.replace(index, QString("<raw-flip>").length(), QString("--flip %1").arg(ui.editRawCapFlip->text()));
        }

        // Replace <raw-address>
        index = specFilePath.indexOf("<raw-address>");
        if (index >= 0)
        {
            specFilePath.replace(index, QString("<raw-address>").length(), QString("--address %1").arg(ui.editRawCapAddress->text()));
        }
    }
    else if (ui.comboBoxTargetDevice->currentText().compare(targetDeviceName(GW4XX), Qt::CaseInsensitive) == 0 ||
             ui.comboBoxTargetDevice->currentText().compare(targetDeviceName(GW3XX), Qt::CaseInsensitive) == 0)
    {
        // Replace <raw-x>
        index = specFilePath.indexOf("<raw-x>");
        if (index >= 0)
        {
            specFilePath.replace(index, QString("<raw-x>").length(), QString("--x %1").arg(ui.editRawCapX->text()));
        }

        // Replace <raw-y>
        index = specFilePath.indexOf("<raw-y>");
        if (index >= 0)
        {
            specFilePath.replace(index, QString("<raw-y>").length(), QString("--y %1").arg(ui.editRawCapY->text()));
        }
    }

    // Replace <raw-width>
    index = specFilePath.indexOf("<raw-width>");
    if (index >= 0)
    {
        specFilePath.replace(index, QString("<raw-width>").length(), QString("--width %1").arg(ui.editRawCapWidth->text()));
    }

    // Replace <raw-height>
    index = specFilePath.indexOf("<raw-height>");
    if (index >= 0)
    {
        specFilePath.replace(index, QString("<raw-height>").length(), QString("--height %1").arg(ui.editRawCapHeight->text()));
    }

    // Replace <raw-bayer-pattern>
    index = specFilePath.indexOf("<raw-bayer-pattern>");
    if (index >= 0)
    {
        specFilePath.replace(index, QString("<raw-bayer-pattern>").length(), QString("--bayer-pattern %1").arg(ui.comboBoxRawCapBayerPattern->currentText()));
    }

    // Replace <raw-bpp>
    index = specFilePath.indexOf("<raw-bpp>");
    if (index >= 0)
    {
        specFilePath.replace(index, QString("<raw-bpp>").length(), QString("--bpp %1").arg(ui.editRawCapBpp->text()));
    }

    // Replace <raw-output-name>
    index = specFilePath.indexOf("<raw-output-name>");
    if (index >= 0)
    {
        specFilePath.replace(index, QString("<raw-output-name>").length(), QString("--output-name %1").arg(ui.editRawCapOutputName->text()));
    }

    // Replace <raw-capture>
    index = specFilePath.indexOf("<raw-capture>");
    if (index >= 0)
    {
        specFilePath.replace(index, QString("<raw-capture>").length(), QString("--raw-capture %1").arg(raw_capture?"1":"0"));
    }
}

bool ISPGui::eventFilter(QObject *obj, QEvent *event)
{
	// Disable wheel event to parameter controls (comboboxes/spinboxes/double spinboxes)
	// Source:  http://www.qtcentre.org/threads/33099-How-to-disable-wheelEvent()
	bool ret = false;		// don't block
	if (event->type() == QEvent::Wheel)
	{
		auto comboBox = dynamic_cast<QComboBox *>(obj);
		auto spinBox = dynamic_cast<QSpinBox *>(obj);
		auto doubleSpinBox = dynamic_cast<QDoubleSpinBox *>(obj);
		ret = (comboBox || spinBox || doubleSpinBox);		// true if a wheel event went to a type of our child control
	}

    if(event->type() == QEvent::MouseButtonRelease) {
        if (obj == ui.editFirmwareLocation)
        {
            QString fileDir(".");
            QSettings settings("GeoSemi","ISPGui");
            if (ui.editFirmwareLocation->text().length() != 0)
            {
                fileDir = QFileInfo(ui.editFirmwareLocation->text()).absolutePath();
            }
            else if (settings.contains("rawFirmwareLocation"))
            {
                QString previousLocation = settings.value("rawFirmwareLocation").toString();
                if (QFileInfo(previousLocation).exists())
                {
                    fileDir = QFileInfo(previousLocation).absolutePath();
                }
            }
            else if (ui.editRawCapJsonLocation->text().length() != 0)
            {
                fileDir = QFileInfo(ui.editRawCapJsonLocation->text()).absolutePath();
            }

            QString filename = QFileDialog::getOpenFileName(this, tr("Select firmware"), fileDir);
            if (!filename.isEmpty())
            {
                ui.editFirmwareLocation->setText(filename);
                settings.setValue("rawFirmwareLocation", filename);
            }
        }
        else if (obj == ui.editRawCapJsonLocation)
        {
            QString fileDir(".");
            QSettings settings("GeoSemi","ISPGui");
            if (ui.editRawCapJsonLocation->text().length() != 0)
            {
                fileDir = QFileInfo(ui.editRawCapJsonLocation->text()).absolutePath();
            }
            else if (settings.contains("rawJsonLocation"))
            {
                QString previousLocation = settings.value("rawJsonLocation").toString();
                if (QFileInfo(previousLocation).exists())
                {
                    fileDir = QFileInfo(previousLocation).absolutePath();
                }
            }
            else if (ui.editFirmwareLocation->text().length() != 0)
            {
                fileDir = QFileInfo(ui.editFirmwareLocation->text()).absolutePath();
            }
            QString filename = QFileDialog::getOpenFileName(this, tr("Select raw capture json"), fileDir, tr("JSON files (*.json)"));
            if (!filename.isEmpty())
            {
                ui.editRawCapJsonLocation->setText(filename);
                settings.setValue("rawJsonLocation", filename);

                // Auto populate raw capture json in UI
                QFile rawCapJsonFile(filename);
                rawCapJsonFile.open(QIODevice::ReadOnly | QIODevice::Text);
                QByteArray contents = rawCapJsonFile.readAll();
                rawCapJsonFile.close();
                QJsonParseError error;
                QJsonDocument* rawCapJson = new QJsonDocument(QJsonDocument::fromJson(contents, &error));
                if (error.error != QJsonParseError::NoError)
                {
                    QString sourceFragment = (error.offset == contents.length()) ? "<eof>" : contents.mid(error.offset, 80);
                    logMsg(tr("Error: Failed to parse raw capture json. %1.\r\nLocation: %2").arg(error.errorString()).arg(sourceFragment));
                }
                else
                {
                    QJsonObject jsonObject = rawCapJson->object();
                    QJsonObject::const_iterator systemIterator = jsonObject.find("system");
                    if (systemIterator != jsonObject.end())
                    {
                        QJsonObject systemNode = systemIterator.value().toObject();
                        if (systemNode.find("SENSOR_GAIN") != systemNode.end())
                        {
                            logMsg(tr("Sensor gain from json: %1").arg(systemNode.find("SENSOR_GAIN").value().toString()));
                            ui.editRawCapGain->setText(systemNode.find("SENSOR_GAIN").value().toString());
                        }
                        if (systemNode.find("SENSOREXP") != systemNode.end())
                        {
                            logMsg(tr("Sensor exposure from json: %1").arg(systemNode.find("SENSOREXP").value().toString()));
                            ui.editRawCapExp->setText(systemNode.find("SENSOREXP").value().toString());
                        }
                        if (systemNode.find("SENSORFLIP") != systemNode.end())
                        {
                            logMsg(tr("Sensor flip from json: %1").arg(systemNode.find("SENSORFLIP").value().toString()));
                            ui.editRawCapFlip->setText(systemNode.find("SENSORFLIP").value().toString());
                        }
                    }
                    delete rawCapJson;
                }
            }
        }
    }

	return ret;
}

void ISPGui::parseMinMax(const QJsonValue &minObject, const QJsonValue &maxObject, const QString &controlType,
                        bool &controlHex, double &controlMin, double &controlMax,
						const QString &paramDescription, QString &tooltipString)
{
	// If Max starts with "0x", interpret this to mean the control should be in Hex
	controlHex = (maxObject.isString() && maxObject.toString().startsWith("0x", Qt::CaseInsensitive));

	// If Max is a string, convert it to hex, otherwise it is a decimal
    controlMax = (maxObject.isString()) ? maxObject.toString().toInt(nullptr, 16) : maxObject.toDouble(numeric_limits<double>::min());
    Q_ASSERT(controlMax != numeric_limits<double>::min());

	// If Min is a string, convert it to hex, otherwise it is a decimal
    controlMin = (minObject.isString()) ? minObject.toString().toInt(nullptr, 16) : minObject.toDouble(numeric_limits<double>::min());
    Q_ASSERT(controlMin != numeric_limits<double>::min());

	formatTooltip(controlType, controlHex, controlMin, controlMax, paramDescription, tooltipString);
}


void ISPGui::formatTooltip(	const QString &controlType, bool controlHex, double controlMin, double controlMax,
							const QString &paramDescription, QString &tooltipString)
{
	QString minTooltipString, maxTooltipString;
	if (controlHex)										// hex
        minTooltipString = QString("0x%1").arg((int)controlMin, 0, 16);
	else if (s_floatControlTypes.contains(controlType))	// float
    {
        minTooltipString = QString("%1").arg(controlMin, 0, 'f', c_doublePrecision);
        //Util::removeTrailingDecimalZeroes(minTooltipString);
    }
	else												// decimal
        minTooltipString = QString("%1").arg((int)controlMin);

	if (controlHex)											// hex
        maxTooltipString = QString("0x%1").arg((int)controlMax, 0, 16);
    else if (s_floatControlTypes.contains(controlType))		// float
    {
        maxTooltipString = QString("%1").arg(controlMax, 0, 'f', c_doublePrecision);
        //Util::removeTrailingDecimalZeroes(maxTooltipString);
    }
	else                                                    // decimal
        maxTooltipString = QString("%1").arg((int)controlMax);

	// Final tooltip - Add lines:  Description/Min/Max
	QString minMaxString;
	if (!minTooltipString.isEmpty() || !maxTooltipString.isEmpty())
	{
		if (!minTooltipString.isEmpty())
		{
			minMaxString += QString("<b>Min</b> %1").arg(minTooltipString);
		}

		if (!maxTooltipString.isEmpty())
		{
			if (!minMaxString.isEmpty())
				minMaxString += "<br/>";
			minMaxString += QString("<b>Max</b> %1").arg(maxTooltipString);
		}
	}

	if (!paramDescription.isEmpty() || !minMaxString.isEmpty())		// non-empty tooltip
	{
		tooltipString = "<html>";
		tooltipString += paramDescription;
		tooltipString += minMaxString;

		tooltipString += "</html>";
	}

}

void ISPGui::normalizeLabelForControl(QWidget *control)
{
    // Remove link and tooltip from label
    QString labelObjectName = QString("label-%1").arg(control->objectName());
    auto label = control->parent()->findChild<QLabel *>(labelObjectName);
    if (label)
    {
        auto noLinkText = label->property("paramName").toString();
        label->setText(noLinkText);
        label->setToolTip("");
    }
}

bool ISPGui::completeTables()
{
	bool valid = true;

	// Iterate subsections in the tree
	int numSections = ui.subsectionTree->topLevelItemCount();
	for (int i = 0; i < numSections; i++)
	{
        //auto topLevelItem = ui.subsectionTree->topLevelItem(i);
        //QString subsectionName(topLevelItem->text(0));

		// Iterate parameters of the subsection
		auto pageWidget = ui.subsectionStackedWidget->widget(i);
		const QObjectList &params = pageWidget->children();
		foreach(QObject *object, params)
		{
			// Skip disabled controls - they were not initialized from a Config file
			QWidget *control = dynamic_cast<QWidget *>(object);
			if (!control || !control->isEnabled())
				continue;

			// Check table controls
			auto table = dynamic_cast<ISPTableWidget *>(control);
			if (table)					// control is a table
			{
				// Determine if table is all full or all empty
				bool empty = true;
				int numRows = table->rowCount();
				int numCols = table->columnCount();
				int minRows = table->property("minRows").toInt();
				int numFullCells = 0;
				for (int row = 0; row < numRows; row++)
				{
					for (int col = 0; col < numCols; col++)
					{
                        auto tableWidgetItem = table->item(row, col);
						if (!tableWidgetItem || tableWidgetItem->text().isEmpty())
						{
							// this cell is empty
						}
						else
						{
							// this cell has a value
							numFullCells++;
							empty = false;    
						}
					}
				}

				int originalNumCols = table->property("originalNumCols").toInt();		// num columns in JSON
				int minFullCells = originalNumCols * minRows;

                valid = (empty || ((numFullCells >= minFullCells) && (numFullCells % originalNumCols == 0)));
				if (!valid)
				{
					int minUIRows = minFullCells / numCols;		// convert from Cells to Rows (which has 'Pitch" columns)

					QString tableName = control->objectName();
					QMessageBox::warning(this, "Partially Complete Table",
                        tr("<html>The table <b>%1</b> is only partially complete.\r\nIt must be completely empty or the first %2 or more row(s) must be filled <b>completely</b> before the Configuration can be saved.&nbsp;&nbsp;Please fix and try again.")
						.arg(tableName)
						.arg(minUIRows));

					return valid;
                }
			}
		}
	}

    valid = validateColorTables();

	return valid;
}

bool ISPGui::validateColorTables() {
    // Iterate subsections in the tree
    int numSections = ui.subsectionTree->topLevelItemCount();
    for (int i = 0; i < numSections; i++)
    {
        // Iterate parameters of the subsection
        auto pageWidget = ui.subsectionStackedWidget->widget(i);
        QComboBox *comboBoxAdaptCcmen = pageWidget->findChild<QComboBox *>("color:adaptccmen");
        if (comboBoxAdaptCcmen)
        {
            // This is the page containing color tables
            int adaptCCmen = comboBoxAdaptCcmen->currentText().toInt();

            const QObjectList &params = pageWidget->children();
            int prevCorrTemp = -1;

            // Validate the tables to be white point preserving
            bool continueWithTableErrors = false;
            bool continueWithTempErrors = false;
            foreach(QObject *object, params)
            {
                // Skip disabled controls - they were not initialized from a Config file
                QWidget *control = dynamic_cast<QWidget *>(object);
                if (!control || !control->isEnabled())
                {
                    continue;
                }

                // Do not validate white point preservation if user wants to continue with errors
                if (continueWithTableErrors != true)
                {
                    auto table = dynamic_cast<ISPTableWidget *>(control);
                    if (table)
                    {
                        int numRows = table->rowCount();
                        int numCols = table->columnCount();
                        for (int row = 0; row < numRows; row++)
                        {
                            float rowTotal = 0;
                            for (int col = 0; col < numCols; col++)
                            {
                                auto tableWidgetItem = table->item(row, col);
                                rowTotal += tableWidgetItem->text().toFloat(nullptr);
                            }

                            double errorMargin = ui.spinBoxCCMErrorMargin->value();
                            if (!(fabsf(rowTotal - 1) >= 0 && fabsf(rowTotal - 1) <= errorMargin))
                            {
                                QString tableName = control->objectName();
                                logMsg(tr("Table %1: row index %2 adds up to %3. Must add up to 1 (+/- %4)").arg(tableName).arg(row).arg(rowTotal).arg(errorMargin));

                                QMessageBox *messageBox = new QMessageBox(this);
                                QAbstractButton *continueButton = messageBox->addButton(tr("Continue"), QMessageBox::ActionRole);
                                messageBox->addButton(QMessageBox::Cancel);
                                messageBox->setDefaultButton(QMessageBox::Cancel);

                                messageBox->setIcon(QMessageBox::Question);
                                messageBox->setWindowTitle(tr("Table Not White Point Preserving"));
                                messageBox->setText(tr("<html>Table <b>%1</b> is <b>not</b> white point preserving. Row index %2 does not add up to 1</html>").arg(tableName).arg(row));
                                messageBox->setInformativeText(tr("To continue anyway, click \"Continue\".\nTo cancel, click \"Cancel\"."));
                                messageBox->exec();

                                if (messageBox->clickedButton() == continueButton)
                                {
                                    continueWithTableErrors = true;
                                    break;
                                }
                                else
                                {
                                    // Cancel
                                    return false;
                                }
                            }
                        }
                    }
                }

                // Validate the corr_temperature values to be ascending if adaptCCmen == 1 and the user does not want to continue with errors
                if (adaptCCmen == 1 && continueWithTempErrors != true)
                {
                    auto spinBox = dynamic_cast<QSpinBox *>(control);
                    if (spinBox)
                    {
                        QRegExp regex(QString("color:usr_[1-9]_corr_temperature"), Qt::CaseSensitive, QRegExp::RegExp);
                        if (regex.exactMatch(spinBox->objectName()))
                        {
                            if (spinBox->value() > prevCorrTemp)
                            {
                                prevCorrTemp = spinBox->value();
                            }
                            else
                            {
                                QMessageBox *messageBox = new QMessageBox(this);
                                QAbstractButton *continueButton = messageBox->addButton(tr("Continue"), QMessageBox::ActionRole);
                                messageBox->addButton(QMessageBox::Cancel);
                                messageBox->setDefaultButton(QMessageBox::Cancel);

                                messageBox->setIcon(QMessageBox::Question);
                                messageBox->setWindowTitle(tr("Color temperature values not ascending"));
                                messageBox->setText(tr("<html>Color temperature values must be ascending in values:<br/>usr_1_corr_temperature &lt; usr_2_corr_temperature &lt; usr_3_corr_temperature</html>"));
                                messageBox->setInformativeText(tr("To continue anyway, click \"Continue\".\nTo cancel, click \"Cancel\"."));
                                messageBox->exec();

                                if (messageBox->clickedButton() == continueButton)
                                {
                                    continueWithTempErrors = true;
                                    continue;
                                }
                                else
                                {
                                    // Cancel
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return true;
}

void ISPGui::updateRawCaptureUI() {
    if (ui.comboBoxTargetDevice->currentText().compare(targetDeviceName(GC65XX), Qt::CaseInsensitive) == 0) {
#ifndef WIN32
        ui.rawCapTool->setEnabled(true);
#else
        ui.rawCapTool->setEnabled(true);
#endif
        ui.frameRawR2->show();
        ui.groupBoxR2->show();
        ui.spacerR2->changeSize(20, 20, QSizePolicy::Fixed, QSizePolicy::Fixed);
        ui.comboBoxRawCapBayerPattern->setCurrentIndex(2);
        ui.frameRawW4->hide();

        ((QFormLayout*)ui.frameRawR2->layout())->setVerticalSpacing(5);
        ((QFormLayout*)ui.frameRawW4->layout())->setVerticalSpacing(0);

        ui.buttonRawCapture->setText(tr("Capture Raw"));
    } else if (ui.comboBoxTargetDevice->currentText().compare(targetDeviceName(GW4XX), Qt::CaseInsensitive) == 0 ||
               ui.comboBoxTargetDevice->currentText().compare(targetDeviceName(GW3XX), Qt::CaseInsensitive) == 0) {
        ui.rawCapTool->setEnabled(true);
        ui.frameRawR2->hide();
        ui.groupBoxR2->hide();
        ui.spacerR2->changeSize(0, 0, QSizePolicy::Fixed, QSizePolicy::Fixed);
        ui.comboBoxRawCapBayerPattern->setCurrentIndex(3);
        ui.frameRawW4->show();

        ((QFormLayout*)ui.frameRawR2->layout())->setVerticalSpacing(0);
        ((QFormLayout*)ui.frameRawW4->layout())->setVerticalSpacing(5);

        ui.buttonRawCapture->setText(tr("Capture Raw"));
    } else {
        ui.rawCapTool->setEnabled(false);
    }
}

bool ISPGui::validateConfigVersion()
{
    if (m_configMajorVersion > m_specMajorVersionMax)
    {
        logMsg(tr("Major version (%1) is greater than the maximum allowed by the spec (%2)").arg(m_configMajorVersion).arg(m_specMajorVersionMax));
        QMessageBox::warning(this, tr("Version error"), tr("This config's major version is greater than the maximum allowed by the spec"));
        return false;
    }
    else if (m_configMajorVersion < m_specMajorVersionMin)
    {
        logMsg(tr("Major version (%1) is smaller than the minimum allowed by the spec (%2)").arg(m_configMajorVersion).arg(m_specMajorVersionMin));
        QMessageBox::warning(this, tr("Version error"), tr("This config's major version is smaller than the minimum allowed by the spec"));
        return false;
    }


    if (m_configMinorVersion > m_specMinorVersionMax)
    {
        logMsg(tr("Minor version (%1) is greater than the maximum allowed by the spec (%2)").arg(m_configMinorVersion).arg(m_specMinorVersionMax));
        QMessageBox::warning(this, tr("Version error"), tr("This config's minor version is greater than the maximum allowed by the spec"));
        return false;
    }
    else if (m_configMinorVersion < m_specMinorVersionMin)
    {
        logMsg(tr("Minor version (%1) is smaller than the minimum allowed by the spec (%2)").arg(m_configMinorVersion).arg(m_specMinorVersionMin));
        QMessageBox::warning(this, tr("Version error"), tr("This config's minor version is smaller than the minimum allowed by the spec"));
        return false;
    }

    return true;
}

// Check if this config has been modified, but not set to the camera
bool ISPGui::configIsModified()
{
    int numSections = ui.subsectionTree->topLevelItemCount();
    for (int i = 0; i < numSections; i++)
    {
        // Iterate parameters of the subsection
        auto pageWidget = ui.subsectionStackedWidget->widget(i);
        const QObjectList &params = pageWidget->children();
        foreach(QObject *object, params)
        {
            // Skip disabled controls - they were not initialized from a Config file
            QWidget *control = dynamic_cast<QWidget *>(object);
            if (!control || !control->isEnabled())
                continue;

            // Consider this control as modified if it is "new", i.e, it has been enabled
            if (control->property("new").isValid() && control->property("new").toBool())
            {
                return true;
            }

            // Skip empty parameter names
            QString objectName = control->objectName();	// <subsection>:<parameter>
            QString paramName = objectName.mid(objectName.indexOf(":") + 1);
            if (paramName.isEmpty())
                continue;

            // Get the value of the control, depending on what type of control it is
            auto comboBox = dynamic_cast<QComboBox *>(control);
            auto spinBox = dynamic_cast<QSpinBox *>(control);
            auto doubleSpinBox = dynamic_cast<QDoubleSpinBox *>(control);
            auto table = dynamic_cast<ISPTableWidget *>(control);
            if (comboBox)               // control is a combo box
            {
                int originalValue = comboBox->property("originalValue").toInt();
                int index = comboBox->currentIndex();
                if ((index != originalValue))
                {
                    //logMsg(tr("comboBox %1 is changed").arg(objectName));
                    return true;
                }
            }
            else if (spinBox)			// control is a spin box
            {
                int value = spinBox->value();
                int originalValue = spinBox->property("originalValue").toInt();
                if ((value != originalValue))
                {
                    //logMsg(tr("spinBox %1 is changed").arg(objectName));
                    return true;
                }
            }
            else if (doubleSpinBox)			// control is a double spin box
            {
                double value = doubleSpinBox->value();
                double originalValue = doubleSpinBox->property("originalValue").toDouble();
                if ((value != originalValue))
                {
                    //logMsg(tr("doubleSpinBox %1 is changed").arg(objectName));
                    return true;
                }
            }
            else if (table)					// control is a table
            {
                // Determine if this subsection has changed
                int numRows = table->rowCount();
                int numCols = table->columnCount();
                bool tableChanged = false;

                for (int row = 0; row < numRows; row++)
                {
                    for (int col = 0; col < numCols; col++)
                    {
                        auto tableWidgetItem = table->item(row, col);
                        if (tableWidgetItem)
                        {
                            tableChanged = tableChanged || (tableWidgetItem->text() != tableWidgetItem->data(Qt::UserRole).toString());
                        }
                    }
                }

                if (tableChanged)
                {
                    //logMsg(tr("Table %1 is changed").arg(objectName));
                    return true;
                }
            }
        }
    }

    return false;
}

// Set all controls in this config as "not modified".
void ISPGui::resetOriginalValues()
{
    int numSections = ui.subsectionTree->topLevelItemCount();
    for (int i = 0; i < numSections; i++)
    {
        // Iterate parameters of the subsection
        auto pageWidget = ui.subsectionStackedWidget->widget(i);
        const QObjectList &params = pageWidget->children();
        foreach(QObject *object, params)
        {
            // Skip disabled controls - they were not initialized from a Config file
            QWidget *control = dynamic_cast<QWidget *>(object);
            if (!control || !control->isEnabled())
                continue;

            // Set this control as "not new".
            control->setProperty("new", false);

            // Skip empty parameter names
            QString objectName = control->objectName();	// <subsection>:<parameter>
            QString paramName = objectName.mid(objectName.indexOf(":") + 1);
            if (paramName.isEmpty())
                continue;

            // Get the value of the control, depending on what type of control it is
            auto comboBox = dynamic_cast<QComboBox *>(control);
            auto spinBox = dynamic_cast<QSpinBox *>(control);
            auto doubleSpinBox = dynamic_cast<QDoubleSpinBox *>(control);
            auto table = dynamic_cast<ISPTableWidget *>(control);
            if (comboBox)               // control is a combo box
            {
                int originalValue = comboBox->property("originalValue").toInt();
                int index = comboBox->currentIndex();
                if ((index != originalValue))
                {
                    comboBox->setProperty("originalValue", index);
                }
            }
            else if (spinBox)			// control is a spin box
            {
                int value = spinBox->value();
                int originalValue = spinBox->property("originalValue").toInt();
                if ((value != originalValue))
                {
                    spinBox->setProperty("originalValue", value);
                }
            }
            else if (doubleSpinBox)			// control is a double spin box
            {
                double value = doubleSpinBox->value();
                double originalValue = doubleSpinBox->property("originalValue").toDouble();
                if ((value != originalValue))
                {
                    doubleSpinBox->setProperty("originalValue", value);
                }
            }
            else if (table)					// control is a table
            {
                // Determine if this subsection has changed
                int numRows = table->rowCount();
                int numCols = table->columnCount();

                for (int row = 0; row < numRows; row++)
                {
                    for (int col = 0; col < numCols; col++)
                    {
                        auto tableWidgetItem = table->item(row, col);
                        if (tableWidgetItem)
                        {
                            if (tableWidgetItem->text() != tableWidgetItem->data(Qt::UserRole).toString())
                            {
                                tableWidgetItem->setData(Qt::UserRole, tableWidgetItem->text());
                            }
                        }
                    }
                }
            }
        }
    }
}

bool ISPGui::checkConnectedDevices(bool showErrorDialog)
{
    if (!ui.checkBoxDetectDevices->isChecked())
    {
        return true;
    }

    if (m_connectionManager == NULL)
    {
        m_connectionManager = new ConnectionManager();
    }
    QString errorMessage;
    if (!m_connectionManager->isDeviceConnected(targetDevice(ui.comboBoxTargetDevice->currentText()), errorMessage))
    {
        logMsg(errorMessage);
        if (showErrorDialog)
        {
            //QMessageBox::warning(this, tr("No device connected"), tr("Could not detect any %1 device").arg(ui.comboBoxTargetDevice->currentText()));
            QMessageBox *messageBox = new QMessageBox(this);
            QAbstractButton *continueButton = messageBox->addButton(tr("Continue anyway"), QMessageBox::ActionRole);
            messageBox->addButton(QMessageBox::Cancel);
            messageBox->setDefaultButton(QMessageBox::Cancel);

            messageBox->setIcon(QMessageBox::Question);
            messageBox->setWindowTitle(tr("No device connected"));
            messageBox->setText(tr("Could not detect any working %1 device").arg(ui.comboBoxTargetDevice->currentText()));
            messageBox->exec();

            if (messageBox->clickedButton() == continueButton)
            {
                // Continue anyway
                return true;
            }
            else
            {
                // Cancel.
                return false;
            }
        }
    }

    return true;
}
