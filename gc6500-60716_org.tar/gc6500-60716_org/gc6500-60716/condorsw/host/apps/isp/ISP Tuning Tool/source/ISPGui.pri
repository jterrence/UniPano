HEADERS += $$PWD/src/stdafx.h \
    $$PWD/src/ispgui.h \
    $$PWD/src/ISPTableWidget.h \
    $$PWD/src/ISPTableItemDelegate.h \
    $$PWD/src/ISPValidator.h \
    $$PWD/src/about.h \
    $$PWD/src/util.h \
    $$PWD/src/ispserialport.h \
    $$PWD/src/histogram/imageinfoscene.h \
    $$PWD/src/histogram/selectionrectangle.h \
    $$PWD/src/connectionmanager.h

SOURCES += $$PWD/src/ispgui.cpp \
    $$PWD/src/main.cpp \
    $$PWD/src/ISPTableWidget.cpp \
    $$PWD/src/ISPTableItemDelegate.cpp \
    $$PWD/src/ISPValidator.cpp \
    $$PWD/src/about.cpp \
    $$PWD/src/stdafx.cpp \
    $$PWD/src/ispserialport.cpp \
    $$PWD/src/histogram/imageinfoscene.cpp \
    $$PWD/src/histogram/selectionrectangle.cpp \
    $$PWD/src/connectionmanager.cpp

FORMS += $$PWD/ui/ispgui.ui \
         $$PWD/ui/about.ui

RESOURCES += ispgui.qrc
