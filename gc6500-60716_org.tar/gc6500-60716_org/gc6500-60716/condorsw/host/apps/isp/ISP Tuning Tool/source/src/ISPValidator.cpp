/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include "stdafx.h"
#include "ISPValidator.h"

ISPValidator::ISPValidator(const QRegularExpression &re, double min, double max, QObject * parent, int allowEmptyCell)
	: QRegularExpressionValidator(re, parent)
	, min(min)
	, max(max)
    , allowEmptyCell(allowEmptyCell)
{
}

QValidator::State ISPValidator::validate(QString& input, int& pos) const
{
    auto state = QRegularExpressionValidator::validate(input, pos);

	if (state != QValidator::State::Acceptable)		// error condition
    {
        return state;
    }

    if (input.isEmpty())
    {
        if (allowEmptyCell)
        {
            return QValidator::State::Acceptable;
        }
        else
        {
            return QValidator::State::Intermediate;
        }
    }

	// Intermediate state if no decimal or no hex digits
    if (input.compare("0x", Qt::CaseInsensitive) == 0)
    {
        return QValidator::State::Intermediate;
    }
    else if (input.indexOf(".") == 0)
    {
        return QValidator::State::Intermediate;
    }
    else if (min < 0)
    {
        if (input.compare("-", Qt::CaseInsensitive) == 0)
        {
            return QValidator::State::Intermediate;
        }
    }

	// Input matches regular expression,
	// now check if it is within min/max
	int base = input.startsWith("0x", Qt::CaseInsensitive) ? 16 : 10;
	bool ok;
    double val = 0;
    if (input.indexOf(".") > 0)
    {
        val = input.toDouble(&ok);
    }
    else
    {
        val = input.toInt(&ok, base);
    }

    if (!ok)
    {
        return QValidator::State::Invalid;
    }

    return (val >= min && val <= max) ? QValidator::State::Acceptable : QValidator::State::Invalid;
}
