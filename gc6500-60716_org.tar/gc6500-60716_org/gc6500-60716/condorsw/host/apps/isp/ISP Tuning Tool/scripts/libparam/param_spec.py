#!/usr/bin/env python

import json
import param_type
import string


def create_param(name, from_json, parent):
    if type(from_json['type']) is list:
        params = []
        for i in range(len(from_json['type'])):
            params.append(ParamArray(name, from_json, i, parent))
        return params
    else:
        return [Param(name, from_json, parent)]


class Param(object):
    param_fields = ["description", "id", "type", "raw", "min", "max"]

    def __init__(self, name, from_json, parent):
        self._name = name
        self._parent = parent
        for whitespacechar in string.whitespace:
            if whitespacechar in name:
                raise ValueError("Whitespace found in parameter name '%s' in group '%s'!"
                                    % (name, parent.name()))
        for field in self.param_fields:
            if field != "raw":
                if field not in from_json:
                    raise ValueError("Field %s is missing from the " % field
                                        + "parameter specification of %s:\n" % name
                                        + json.dumps(from_json, indent=4))
            else:
                # special handling for "raw" which is an optional field with a default value
                if field not in from_json:
                    self._raw = False
                else:
                    self._raw = from_json["raw"]

            if field == "type":
                self._type = param_type.get(from_json["type"])
            elif field != "raw":
                setattr(self, "_" + field, from_json[field])

        self.check_constraints(from_json)
        self._full_name = self.full_name()
        self._group = self.group()
        self._initialized = 0

    def is_array(self):
        return False

    def check_constraints(self, from_json):
        if self._type not in param_type.list():
            raise ValueError(
                "Type %s does not exists in the parameter specification "
                "of %s:\n%s" % (self._type, self._name,
                                json.dumps(from_json, indent=4)))

        if self._min <  self._type.min():
            raise ValueError(
                "The minimum value of {}.{} is less than what its type allows"
                " ({}):\n{}".format(self._parent.name(), self._name,
                                    self._type.min(),
                                    json.dumps(from_json, indent=4)))

        if self._max >  self._type.max():
            raise ValueError(
                "The maximum value of {}.{} is greater than what its type allows"
                " ({}):\n{}".format(self._parent.name(), self._name,
                                    self._type.max(),
                                    json.dumps(from_json, indent=4)))

    def full_name(self):
        names = []
        parent = self
        while parent is not None:
            names.append(parent.name())
            parent = parent.parent()
        names.reverse()
        return u"_".join(names)

    def id(self):
        return self._id

    def type(self):
        return self._type

    def name(self):
        return self._name

    def min(self):
        return self._min

    def max(self):
        return self._max

    def is_raw(self):
        return self._raw

    def parent(self):
        return self._parent

    def group(self):
        return self._parent.name()

    def offset(self):
        return self._offset

    def set_offset(self, offset):
        self._offset = offset

    def payload_size(self, nonraw_only):
        return (0 if (nonraw_only and self.is_raw()) else self._type.size())

    def align_size(self):
        return self._type.size()

    def bin_output_size(self, nonraw_only):
        # id 2 bytes + type 1 byte
        return (0 if (nonraw_only and self.is_raw()) else (2 + 1 + self.payload_size(nonraw_only)))

    def initialized(self):
        return self._initialized


class ParamArray(Param):
    param_fields = ["description", "id", "type", "raw", "min", "max",
                    "dim_name", "min_rows", "max_rows"]

    def __init__(self, name, from_json, dim_index, parent):
        for whitespacechar in string.whitespace:
            if whitespacechar in name:
                raise ValueError("Whitespace found in parameter name '%s' in group '%s'!"
                                    % (name, parent.name()))
        self._name = name
        self._parent = parent
        self._dim_index = dim_index
        for field in self.param_fields:
            if field != "raw":
                if field not in from_json:
                    raise ValueError("Field %s is missing from the " % field
                                        + "parameter specification of %s:\n" % name
                                        + json.dumps(from_json, indent=4))
            else:
                # special handling for "raw" which is an optional field
                if field not in from_json:
                    self._raw = False
                else:
                    self._raw = from_json["raw"]


            if field != "raw":
                if(type(from_json[field]) is list):
                    if field == "type":
                        self._type = param_type.get(from_json["type"][dim_index])
                        self._num_dim = len(from_json["type"])
                    else:
                        setattr(self, "_" + field, from_json[field][dim_index])
                else:
                    setattr(self, "_" + field, from_json[field])

        for whitespacechar in string.whitespace:
            if whitespacechar in self._dim_name:
                raise ValueError("Whitespace found in dim_name '%s' in parameter '%s' in group '%s'!"
                                    % (self._dim_name, name, parent.name()))

        self.check_constraints(from_json)
        self._full_name = self.full_name()
        self._group = self.group()
        self._initialized = 0

    def is_array(self):
        return True

    def full_name(self):
        return u"{}_{}".format(Param.full_name(self), self._dim_name)

    def is_raw(self):
        return self._raw

    def payload_size(self, nonraw_only):
        return (0 if (nonraw_only and self.is_raw()) else (self._max_rows * self._type.size()))

    def align_size(self):
        return self._type.size()

    def bin_output_size(self, nonraw_only):
        # id 2 bytes + type 1 byte + num_rows 2 bytes + start_offset 2 bytes + end_offset 2 bytes
        return (0 if (nonraw_only and self.is_raw()) else (2 + 1 + 2 + 2 + 2 + self.payload_size(nonraw_only)))

    def min_rows(self):
        return self._min_rows

    def max_rows(self):
        return self._max_rows

    def dim_index(self):
        return self._dim_index

    def num_dim(self):
        return self._num_dim

    def initialized(self):
        return self._initialized


class ParamSection(object):
    param_fields = ["id"]

    def __init__(self, name, from_json, parent):
        for whitespacechar in string.whitespace:
            if whitespacechar in name:
                raise ValueError("Whitespace found in group name '%s'!" % name)
        self._name = name
        self._parent = parent
        self._params = []
        self._offset = 0
        for param_name in from_json:
            param_json = from_json[param_name]
            params = create_param(param_name, param_json, self)
            self._params.extend(params)

        align_sizes = set(p.align_size() for p in self._params)
        for align_size in sorted(align_sizes, reverse = True):
            for param in self.params_of_align_size(align_size):
                param.set_offset(self._offset)
                self._offset += param.payload_size(True)

        self._full_name = self.full_name()

    def combine(self, from_json):
        for param_name in from_json:
            param_json = from_json[param_name]
            params = create_param(param_name, param_json, self)
            self._params.extend(params)
        self._offset = 0
        align_sizes = set(p.align_size() for p in self._params)
        for align_size in sorted(align_sizes, reverse = True):
            for param in self.params_of_align_size(align_size):
                param.set_offset(self._offset)
                self._offset += param.payload_size(True)

    def full_name(self):
        names = []
        parent = self
        while parent is not None:
            names.append(parent.name())
            parent = parent.parent()
        names.reverse()
        return "_".join(names)

    def name(self):
        return self._name

    def parent(self):
        return self._parent

    def params(self):
        return sorted(self._params, key=lambda k: k.id())

    def params_of_align_size(self, size):
        return [p for p in self._params if p.align_size() == size]

    def payload_size(self, nonraw_only):
        return sum(p.payload_size(nonraw_only) for p in self._params)

    def bin_output_size(self, nonraw_only):
        return sum(p.bin_output_size(nonraw_only) for p in self._params)


# Parse JSON specification file
class ParamSpec(object):

    def __init__(self, from_json, verbosity_level=4):
        if "spec_name" not in from_json:
            raise ValueError("Field 'spec_name' is missing "
                             + "from the specification")

        self._name = from_json.pop("spec_name")
        self._id = from_json.pop("spec_id")
        self._sections = []
        self._verbosity = verbosity_level

        for section_name in from_json:
            section_json = from_json[section_name]
            section = ParamSection(section_name, section_json, self)
            self._sections.append(section)

        self.check_duplicate_ids()
        self.print_version()

    def combine_spec(self, from_json):
        if "spec_name" not in from_json:
            raise ValueError("Field 'spec_name' is missing "
                             + "from the second spec")

        new_name = from_json.pop("spec_name")
        if self._name != new_name: 
            raise ValueError("'spec_name' {} from the second spec does not match 'spec_name' from the first spec!".format(new_name, self._name))
        new_id = from_json.pop("spec_id")
        if self._id != new_id: 
            raise ValueError("'spec_id' {} from the second spec does not match 'spec_id' from the first spec!".format(new_id, self._id))

        for section_name in from_json:
            section_json = from_json[section_name]
            existing_section = self.getSectionFromName(section_name)
            if existing_section == None:
                section = ParamSection(section_name, section_json, self)
                self._sections.append(section)
            else:
                existing_section.combine(section_json)

        self.check_duplicate_ids()
        self.print_version()

        return self

    def check_duplicate_ids(self):
        ids = {}
        for param in self.params():
            if param.id() in ids:
                raise ValueError("Duplicate ID: '%s' and '%s' "
                                 % (ids[param.id()].full_name(),
                                    param.full_name())
                                 + "are both assigned ID %i" % param.id())
            ids[param.id()] = param

    def scan_uninitialized(self):
        for param in self.params():
            if param.initialized() == 0:
                self.vprint("uninitialized parameter: {}".format(param.full_name()), 3)
                if param._group == "version":
                    self.vprint("ERROR: found uninitialized VERSION parameter, stopping!!", 0)
                    return 1
        return 0

    def has_uninitialized(self):
        for param in self.params():
            if param.initialized() == 0:
                return 1
        return 0

    def print_version(self):
        for param in self.params():
            if param._group == "version":
                self.vprint("SPEC {}: ({} to {})".format(param._full_name, param._min, param._max), 1)

    def name(self):
        return self._name

    def id(self):
        return self._id

    def parent(self):
        return None

    def params(self):
        params = []
        for section in self._sections:
            params.extend(section.params())
        return sorted(params, key=lambda k: k.id())

    def sections(self):
        return sorted(self._sections, key=lambda k: k.name())

    def getFromID(self, param_id):
        params = [p for p in self.params() if p.id() == param_id]
        if len(params) > 0:
            return params[0]
        else:
            return None

    def getFromName(self, param_name, section_name):
        params = [p for p in self.params()
                  if p.parent().name() == section_name
                  and p.name() == param_name]
        if len(params) > 0:
            return params
        else:
            return []

    def getFromType(self, ptype):
        return [p for p in self.params() if p.type() == ptype]

    def getSectionFromName(self, section_name):
        section = [s for s in self.sections()
                    if s.name() == section_name]
        if len(section) > 0:
            return section[0]
        else:
            return None

    def bin_output_size(self, nonraw_only):
        # start with FACE header
        size = 16
        for section in self._sections:
            size = size + section.bin_output_size(nonraw_only)
        return size

    # messagelevel: priority of message (0 = always print, then decreasingly important up to 4)
    #               generally, 0 = error, 1 = warning, 2 = informational, 3 = details, 4 = trivia
    def vprint(self, message, messagelevel=0):
        if messagelevel <= self._verbosity: 
            print message
