/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#pragma once

#include <QStyledItemDelegate>

/*
 IspTableItemDelegate - create a QLineEdit with validation to edit table (aka 'spreadsheet') values.
 Each table column has its own acceptable range of MIN/MAX values.
 A 1D table can actually have 'pitch' columns.  In this case, the minMaxList has only one MIN/MAX value pair,
 it is understood that this is for the 'pitch' number of columns actually in the table.
*/
class IspTableItemDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    IspTableItemDelegate(QList < QPair<double, double> > *minMaxList, QList < bool > *allowDecimalList, bool displayHexAsDecimal = false, QObject *parent = 0);
	~IspTableItemDelegate();

/*
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
*/

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const Q_DECL_OVERRIDE;

    void setEditorData(QWidget *editor, const QModelIndex &index) const Q_DECL_OVERRIDE;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
                      const QModelIndex &index) const Q_DECL_OVERRIDE;

    void updateEditorGeometry(QWidget *editor,
        const QStyleOptionViewItem &option, const QModelIndex &index) const Q_DECL_OVERRIDE;


private:

    QList < QPair<double, double> >	*minMaxList;
    QList < bool > *allowDecimalList;
    bool displayHexAsDecimal;
};
