#!/usr/bin/env python


class _ParamType(object):
    def __init__(self, type):
        self._type = type

    def __str__(self):
        return self._type

    def size(self):
        return _type_info[self._type]["size"]

    def format(self):
        return _type_info[self._type]["format"]

    def id(self):
        return _type_info[self._type]["id"]

    def id_array(self):
        return _type_info[self._type]["id_array"]

    def c_type_suffix(self):
        return _type_info[self._type]["c_type_suffix"]
        
    def py_type(self):
        return _type_info[self._type]["py_type"]

    def min(self):
        return _type_limits[self._type]["min"]

    def max(self):
        return _type_limits[self._type]["max"]


def toarray(id):
    return id + (1 << 7)

_type_info = {
    "uint8_t":  {"id":  2, "id_array":  toarray(2),  "size": 1, "format": 'B', "c_type_suffix": "U", "py_type": int},
    "int8_t":   {"id":  3, "id_array":  toarray(3),  "size": 1, "format": 'b', "c_type_suffix": "", "py_type": int},
    "uint16_t": {"id":  4, "id_array":  toarray(4),  "size": 2, "format": 'H', "c_type_suffix": "U", "py_type": int},
    "int16_t":  {"id":  5, "id_array":  toarray(5),  "size": 2, "format": 'h', "c_type_suffix": "", "py_type": int},
    "uint32_t": {"id":  6, "id_array":  toarray(6),  "size": 4, "format": 'I', "c_type_suffix": "UL", "py_type": long},
    "int32_t":  {"id":  7, "id_array":  toarray(7),  "size": 4, "format": 'i', "c_type_suffix": "L", "py_type": int},
    "uint64_t": {"id":  8, "id_array":  toarray(8),  "size": 8, "format": 'Q', "c_type_suffix": "ULL", "py_type": long},
    "int64_t":  {"id":  9, "id_array":  toarray(9),  "size": 8, "format": 'q', "c_type_suffix": "LL", "py_type": long},
    "float":    {"id": 10, "id_array":  toarray(10), "size": 4, "format": 'f', "c_type_suffix": "f", "py_type": float},
    "double":   {"id": 11, "id_array":  toarray(11), "size": 8, "format": 'd', "c_type_suffix": "", "py_type": float}
}

_type_limits = {
    "uint8_t":  {"min": 0,                "max": 2 ** 8  - 1},
    "int8_t":   {"min": -2 ** 7  + 1,     "max": 2 ** 7  - 1},
    "uint16_t": {"min": 0,                "max": 2 ** 16 - 1},
    "int16_t":  {"min": -2 ** 15 + 1,     "max": 2 ** 15 - 1},
    "uint32_t": {"min": 0,                "max": 2 ** 32 - 1},
    "int32_t":  {"min": -2 ** 31 + 1,     "max": 2 ** 31 - 1},
    "uint64_t": {"min": 0,                "max": 2 ** 64 - 1},
    "int64_t":  {"min": -2 ** 63 + 1,     "max": 2 ** 63 - 1},
    "float":    {"min": -3.4 * 10 ** 38,  "max": 3.4 * 10 ** 38},
    "double":   {"min": -1.7 * 10 ** 308, "max": 1.7 * 10 ** 308}
}

_all_types = {type: _ParamType(type) for type in _type_info}


def list():
    return sorted(sorted(_all_types.values(), key=lambda k: k.id()), key=lambda k: k.size())


def get(type):
    if type in _all_types:
        return _all_types[type]
    else:
        return None


def getFromID(id):
    types = [_all_types[type] for type in _all_types
             if _all_types[type].id() == id
             or _all_types[type].id_array() == id]
    if len(types) > 0:
        return types[0]
    else:
        return None


def c_type_suffix(type):
    return type.c_type_suffix()
    

def py_type(type):
    return type.py_type()

    
def is_array(id):
    return (id & (1 << 7)) > 0
