#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================

import sys
import platform
import subprocess
import os

if __name__ == "__main__":
    device_type = sys.argv[1]
    connection_params = sys.argv[2] if len(sys.argv) > 2 else ''
    
    linux = platform.system() == 'Linux'
    
    if device_type in ['gw4xx', 'gw3xx']:
        
        device_reset = r'./bin/{0}/device_reset'.format(device_type) if linux else r'./bin/{0}/device_reset.exe'.format(device_type)
            
        if os.path.isfile(device_reset):
            command = r'{0} {1}'.format(device_reset, connection_params)
        else:
            direct_access = r'./bin/{0}/direct_access'.format(device_type) if linux else r'./bin/{0}/direct_access.exe'.format(device_type)
            
            if not os.path.isfile(direct_access):
                if linux:
                    print >> sys.stderr, "Error: device_reset missing"
                else:
                    print >> sys.stderr, "Error: device_reset.exe missing"
                sys.exit(1)
            
            command = r'{0} {1} 40400040 1'.format(direct_access, connection_params)

        try:
            subprocess.check_output(command, shell=linux)
        except subprocess.CalledProcessError as cpe:
            if "Failed to connect to device" in cpe.output.strip():
                print >> sys.stderr, cpe.output.strip()
                sys.exit(1)

        print >> sys.stderr, "Camera reset successfully"

    if device_type == 'gc65xx':
        
        print "Error: {0} currently does not support this feature".format(device_type)
        sys.exit(1)

    sys.exit(0)