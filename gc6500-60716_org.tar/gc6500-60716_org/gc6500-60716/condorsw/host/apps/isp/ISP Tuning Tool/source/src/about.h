/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#pragma once

#include "ui_about.h"

class AboutDialog : public QDialog
{
	Q_OBJECT

private:
    Ui::AboutClass ui;

public:
	AboutDialog(QWidget *parent = 0);
    void setReleaseVersion(QString fw_info, QString tool_info, QString copyright);
};
