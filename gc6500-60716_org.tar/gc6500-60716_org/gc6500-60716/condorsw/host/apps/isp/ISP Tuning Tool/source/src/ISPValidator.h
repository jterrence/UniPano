/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#pragma once

#include <QValidator>


/*
 ISPValidator - Validate input to be sure it is decimal or hex characters, and range check min and max values.
 Hex alpha characters are only accepted if input starts with "0x" or "0X".
*/
class ISPValidator : public QRegularExpressionValidator
{
	Q_OBJECT

public:
    ISPValidator(const QRegularExpression &re, double min, double max, QObject *parent = 0, int allowEmptyCell = 0);
	virtual QValidator::State validate(QString& input, int& pos) const;

protected:
    double		min;
    double		max;

private:
    int allowEmptyCell;
};