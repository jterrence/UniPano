#include "ispserialport.h"
#include <QSerialPortInfo>

ISPSerialPort::ISPSerialPort()
{
}

QStringList ISPSerialPort::getPorts() {
    QStringList lines;

    QString description;
    QString manufacturer;
    QString serialNumber;
    const char blankString[] = QT_TRANSLATE_NOOP("ISPGui", "N/A");
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {

        description = info.description();

        if (description.isEmpty()) {
            continue;
        }

        if (description.compare("USB Serial Port", Qt::CaseInsensitive) == 0) {
            QStringList list;
            manufacturer = info.manufacturer();
            serialNumber = info.serialNumber();
            list << info.portName()
                 << (!description.isEmpty() ? description : blankString)
                 << (!manufacturer.isEmpty() ? manufacturer : blankString)
                 << (!serialNumber.isEmpty() ? serialNumber : blankString)
                 << info.systemLocation()
                 << (info.vendorIdentifier() ? QString::number(info.vendorIdentifier(), 16) : blankString)
                 << (info.productIdentifier() ? QString::number(info.productIdentifier(), 16) : blankString);
            lines.append(list.first());
            //qDebug() << list;
        }
    }

    return lines;
}
