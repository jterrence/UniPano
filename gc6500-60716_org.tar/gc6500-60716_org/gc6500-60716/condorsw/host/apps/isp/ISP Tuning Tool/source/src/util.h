/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#ifndef UTIL_H
#define UTIL_H

#include <QObject>
#include <QFileDialog>
#ifdef WIN32
#include <basetsd.h>
#endif
#include <QFile>

class Util
{
public:
    /**
     * @brief Convert a signed 8.8-bit fixed-point hex to decimal
     * @param hex The signed 8.8-bit fixed-point hex to convert
     * @param decimal Will hold the converted decimal
     * @return true if success, false otherwise
     */
    static bool fixedPointHexToDec(QString hex, QString &decimal)
    {
        if (!hex.startsWith("0x", Qt::CaseInsensitive))
        {
            // If hex is an int, then convert to hex
            bool ok;
            int val = hex.toInt(&ok, 10);

            if (!ok)
            {
                return false;
            }

            hex = QString("0x%1").arg(val, 4, 16, QChar('0'));
        }
#ifdef WIN32
        INT16 hexInInt = hex.toInt(nullptr, 16);
#else
        int16_t  hexInInt = hex.toInt(nullptr, 16);
#endif
        float decimalFloat = ((hexInInt & 0x7F00) >> 8) + ((hexInInt & 0xFF)*1.0)/ (1<<8) * 1.0;
        if ((hexInInt & 0x8000) > 0)
        {
            decimalFloat = -decimalFloat;
        }

        decimal = QString::number(decimalFloat, 'f', 6);
        return true;
    }

    /**
     * @brief Convert a decimal to signed 8.8-bit fixed-point hex
     * @param decimal The decimal to convert
     * @param hex Will hold the converted signed 8.8-bit fixed-point hex
     * @return true if success, false otherwise
     */
    static bool decToFixedPointHex(const QString decimal, QString &hex)
    {
        if (decimal.isEmpty() || decimal.startsWith("0x", Qt::CaseInsensitive))
        {
            // return false if the input string is empty or already hex
            return false;
        }

        int hexInt = 0;
        QString decimalString(decimal);

        bool ok = false;
        float decimalInFloat = decimalString.toFloat(&ok);
        if (!ok)
        {
            return false;
        }

        if (decimalInFloat < 0)
        {
            hexInt += 1<<15;
            decimalString = decimalString.mid(1);
        }

        QString intPart = decimalString.mid(0, decimalString.indexOf("."));
        hexInt += intPart.toInt() << 8;
        if (decimalString.indexOf(".") > -1)
        {
            QString decPart = decimalString.mid(decimalString.indexOf("."));
            hexInt += int(round((1 << 8) * decPart.toFloat()));
        }
        hex = QString("0x%1").arg(hexInt, 4, 16, QChar('0'));
        return true;
    }

    static void removeTrailingDecimalZeroes(QString &input)
    {
        if (input.indexOf(".") > 0)
        {
            size_t lastNonZeroPos = input.toStdString().find_last_not_of("0");
            input = input.mid(0, lastNonZeroPos+1);
            if (input.indexOf(".") == input.length()-1)
            {
                input.chop(1);
            }
        }
    }

    static void capitalize(QString &string)
    {
        QStringList words = string.split(' ');
        for (int i = 0; i < words.length(); i++)
        {
            QString word = words[i];
            word[0] = word[0].toUpper();
            words[i] = word;
        }
        string = words.join(' ');
    }

    /**
     * @brief Fix the file permissions, allowing it to be accessed by any user.
     *        This is needed on Linux when running using sudo.
     *
     * @param fileName The file/directory whose permissions are to be changed
     * @return None
     */
    static void fixPermissions(QString fileName)
    {
        QFile file(fileName);
        QFile::Permissions permissions =  file.permissions();
        permissions = permissions | QFile::ReadOther|QFile::WriteOther|QFile::ExeOther;
        file.setPermissions(permissions);
    }

    /**
     * @brief Create a file save dialog.
     *        Allows the user to select a file to be saved (file need not exist).
     *
     * @param parent The file dialog's parent widget
     * @param caption The file dialog's caption
     * @param directory The file dialog's working directory. If it included a file name, the file will be selected.
     * @param filter The file filters
     * @return Selected file name
     */
    static QString getSaveFileName(QWidget * parent, const QString & caption, const QString & directory, const QString & filter = QString("JSON file (*.json);; Binary file (*.bin)"))
    {
        QString selectedFilter;
        if (directory.isEmpty())
        {
            selectedFilter = "JSON file (*.json)";
        }
        else
        {
            if (directory.endsWith(".bin"))
            {
                selectedFilter = "Binary file (*.bin)";
            }
            if (directory.endsWith(".json"))
            {
                selectedFilter = "JSON file (*.json)";
            }
        }
#if defined(WIN32)
        QString fileName = QFileDialog::getSaveFileName(parent, caption, directory, filter, &selectedFilter);
#else
        QString fileName = QFileDialog::getSaveFileName(parent, caption, directory, filter, &selectedFilter, QFileDialog::DontUseNativeDialog);
#endif

        if (fileName.isEmpty())
        {
            return fileName;
        }

        if (fileName.endsWith(".bin") || fileName.endsWith(".json"))
        {
            return fileName;
        }
        else
        {
            if (selectedFilter.contains(".bin"))
            {
                return fileName.append(".bin");
            }

            if (selectedFilter.contains(".json"))
            {
                return fileName.append(".json");
            }
        }

        return QString();
    }
};

#endif // UTIL_H
