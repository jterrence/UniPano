Prerequisites:
1. Qt 5.5
2. Python 2.7

Installing Qt 5.5:

- Download Qt 5.5 online installer from https://www.qt.io/download-open-source/
- Install Qt under your home directory (~/qt/)
- If you install in another directory, you must specify it during compilation via the QT_BIN makefile parameter

Compiling ISPGui:

- Set the environment variable "GEOSW_ROOT" to the root directory (the directory which contains "condorsw", "thirdparty", etc)

- Go to the tool's directory
$ cd isp_tool_package/ISP Tuning Tool

- Compile the tool. This assumes that Qt 5.5 is installed in ~/qt/5.5 or in /usr/bin
$ make

- If Qt 5.5 is installed in another directory
$ make QT_BIN=/another/directory/qt/5.5/gcc/bin

- Run the application using sudo
$ sudo ./ISPGui
