/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include "stdafx.h"
#include "about.h"


//////////////////////////////////////////////////////////////////////////
// AboutDialog

AboutDialog::AboutDialog(QWidget *parent)
    : QDialog(parent)
{
	ui.setupUi(this);
	// Remove '?' button in caption
	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

void AboutDialog::setReleaseVersion(QString fw_info, QString tool_info, QString copyright)
{
    ui.labelTool->setText(tool_info);
    if (fw_info.trimmed().length() > 0)
    {
        ui.labelFirmware->setText(fw_info);
    }
    else
    {
        ui.labelFirmware->setText("-");
    }
    ui.labelCopyright->setText(copyright);
}
