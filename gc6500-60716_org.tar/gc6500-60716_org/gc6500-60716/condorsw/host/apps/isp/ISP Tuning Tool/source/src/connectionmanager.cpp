#include <QSerialPort>
#include <QSerialPortInfo>
#include <QProcess>
#include <QDebug>

#include "connectionmanager.h"

ConnectionManager::ConnectionManager(QObject *parent) :
    QObject(parent)
{
}

bool ConnectionManager::isDeviceConnected(ISPGui::TargetDevice device, QString & errorString)
{
    QStringList ports;
    QProcess p;
    QString tdevice;
    switch(device)
    {
    case ISPGui::GW4XX:
    case ISPGui::GW3XX:

        if (device == ISPGui::GW4XX)
        {
            tdevice = "gw4xx";
        }
        if (device == ISPGui::GW3XX)
        {
            tdevice = "gw3xx";
        }
        errorString = tr("Error: no %1 device detected.").arg(tdevice);

#ifdef WIN32
        ports = getSerialPorts();
        if (ports.length() > 0)
        {
            return true;
        }
        /*for (const QString port : ports)
        {
            if (isPortAvailable(port))
            {
                return true;
            }
        }*/
#else
        ports << "./scripts/deviceManager.py" << tdevice;

        p.setProcessChannelMode(QProcess::MergedChannels);
        p.start("python", ports);
        if (p.waitForStarted())
        {
           p.waitForFinished();
           if (p.exitCode() == 0)
           {
               return true;
           }
           return false;
        }

        return true;

#endif
        return false;
    case ISPGui::GC65XX:

        ports << "./scripts/deviceManager.py" << "gc65xx";

        p.setProcessChannelMode(QProcess::MergedChannels);
        p.start("python", ports);
        if (p.waitForStarted())
        {
           p.waitForFinished();
           if (p.exitCode() == 0)
           {
               return true;
           }

           if (p.exitCode() == 1)
           {
               errorString = tr("Error: no gc65xx device detected (Permission denied). Please run using sudo");
               return false;
           }

           if (p.exitCode() == 2)
           {
               errorString = tr("Error: no gc65xx device detected.");
               return false;
           }

           if (p.exitCode() == 3)
           {
               errorString = tr("Error: gc65xx may not be booted.");
               return false;
           }

           errorString = tr("Error: no gc65xx device detected.");
           return false;
        }

        return true;
    default:
        return false;
    }

    errorString = "Error: No device detected";
    return false;
}

QStringList ConnectionManager::getSerialPorts()
{
    QStringList ports;
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {

        QString description = info.description();

        if (description.isEmpty()) {
            continue;
        }

        if (description.compare("USB Serial Port", Qt::CaseInsensitive) == 0) {
            ports << info.portName();
        }
    }

    return ports;
}

bool ConnectionManager::isPortAvailable(QString portName)
{
    QSerialPort serial;
    serial.setPortName(portName);
    serial.setBaudRate(QSerialPort::Baud115200);
    serial.setDataBits(QSerialPort::Data8);
    serial.setParity(QSerialPort::NoParity);
    serial.setStopBits(QSerialPort::OneStop);
    serial.setFlowControl(QSerialPort::NoFlowControl);
    if (serial.open(QSerialPort::ReadWrite))
    {
        serial.close();
        return true;
    }
    return false;
}
