/*******************************************************************************
*
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this
* copyright notice.
*
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
*
*******************************************************************************/

#include <QGraphicsSceneHoverEvent>
#include <QPointF>
#include <QDebug>
#include <QCursor>
#include <QPainter>
#include <QGraphicsScene>
#include <QPainterPath>
#include <QGraphicsView>

#include "selectionrectangle.h"

// Buffer area to keep around the rectangle.
// This is to allow for extra area to hover around the rectangle borders
const qreal BUFFER = 1.0;
const qreal BORDER = 1.0;

SelectionRectangle::SelectionRectangle(const QRectF &rect, QGraphicsItem *parent):
    QGraphicsRectItem(rect, parent),
    mResizing(false),
    mResizeZoneL(false),
    mResizeZoneR(false),
    mResizeZoneU(false),
    mResizeZoneD(false),
    mLimit(QRectF(0, 0, 100, 100))
{
    this->setFlags(QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemSendsScenePositionChanges);
    this->setAcceptHoverEvents(true);

    this->mWidth = rect.width();
    this->mHeight = rect.height();

    pen.setWidth( BORDER );
}

QRectF SelectionRectangle::boundingRect() const
{
    return QRectF( 0, 0, mWidth+BORDER*2+BUFFER*2, mHeight+BORDER*2+BUFFER*2 );
}

QPainterPath SelectionRectangle::shape() const
{
    QPainterPath path;
    path.addRect( 0, 0, mWidth+BORDER*2+BUFFER*2, mHeight+BORDER*2+BUFFER*2 );
    return path;
}

void SelectionRectangle::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED (option);
    Q_UNUSED (widget);

    painter->setBrush( Qt::transparent );

    // Draw boundary rect
    // painter->setPen( QPen( Qt::red, BORDER, Qt::SolidLine) );
    // painter->drawRect( boundingRect() );

    pen.setBrush( Qt::white );
    pen.setStyle( Qt::SolidLine );
    painter->setPen( pen );
    const QRectF rectW( BUFFER+BORDER/2.0, BUFFER+BORDER/2.0, mWidth, mHeight );
    painter->drawRect( rectW );

    pen.setBrush( Qt::black );
    pen.setStyle( Qt::DashLine );
    painter->setPen( pen );
    const QRectF rectB( BUFFER+BORDER/2.0, BUFFER+BORDER/2.0, mWidth, mHeight );
    painter->drawRect( rectB );
}

// This event gets fired when moving this rectangle
// Make sure that it cannot be moved beyond its limit (mLimit)
QVariant SelectionRectangle::itemChange(GraphicsItemChange change, const QVariant &value)
{
    if (change == QGraphicsItem::ItemPositionChange && scene())
    {
        QPointF newPos = value.toPointF();

        if (newPos.x() < mLimit.x()-(BUFFER-BORDER))
        {
            newPos.setX(mLimit.x()-(BUFFER-BORDER));
        }
        if (newPos.y() < mLimit.y()-(BUFFER-BORDER))
        {
            newPos.setY(mLimit.y()-(BUFFER-BORDER));
        }
        if ( (newPos.x() + this->rect().width() + 2*BUFFER) > (mLimit.x() + mLimit.width()) )
        {
            newPos.setX( mLimit.x() + mLimit.width() - this->rect().width() - 2*BUFFER  );
        }
        if ( (newPos.y() + this->rect().height() + 2*BUFFER) > (mLimit.y() + mLimit.height()) )
        {
            newPos.setY( mLimit.y() + mLimit.height() - this->rect().height() - 2*BUFFER );
        }
        return newPos;
    }
    return QGraphicsRectItem::itemChange(change, value);
}

void SelectionRectangle::setMoveLimit(QRectF limit)
{
    mLimit = limit;
}

void SelectionRectangle::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    prepareGeometryChange();
    mMousePressed = true;
    QGraphicsRectItem::mousePressEvent(event);
}

void SelectionRectangle::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->buttons() == Qt::LeftButton && !mResizing && (mResizeZoneR || mResizeZoneL || mResizeZoneU || mResizeZoneD))
    {
        mResizing = true;
        this->setFlag(QGraphicsItem::ItemIsMovable, false);
        this->setFlag(QGraphicsItem::ItemSendsScenePositionChanges, false);
    }
    QGraphicsRectItem::mouseMoveEvent(event);
}

void SelectionRectangle::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    prepareGeometryChange();
    mMousePressed = false;
    mResizing = false;
    mResizeZoneR = mResizeZoneL = mResizeZoneU = mResizeZoneD = false;

    this->setFlag(QGraphicsItem::ItemIsMovable, true);
    this->setFlag(QGraphicsItem::ItemSendsScenePositionChanges, true);
    QGraphicsRectItem::mouseReleaseEvent(event);
}

void SelectionRectangle::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
    mResizeZoneR = mResizeZoneL = mResizeZoneU = mResizeZoneD = false;

    qreal spacing = BUFFER + BORDER * 2;

    if ( event->pos().x() >= (width()-spacing-1) && event->pos().y() >= (height()-spacing-1) )
    {
        setCursor(QCursor(Qt::SizeFDiagCursor));
        mResizeZoneR = true;
        mResizeZoneD = true;
    }
    else if ( event->pos().y() <= (spacing+1) && event->pos().x() >= (width()-spacing-1) )
    {
        setCursor(QCursor(Qt::SizeBDiagCursor));
        mResizeZoneR = true;
        mResizeZoneU = true;
    }
    else if ( event->pos().x() <= (spacing+1) && event->pos().y() <= (spacing+1) )
    {
        setCursor(QCursor(Qt::SizeFDiagCursor));
        mResizeZoneL = true;
        mResizeZoneU = true;
    }
    else if ( event->pos().x() <= (spacing+1) && event->pos().y() >= (height()-spacing-1) )
    {
        setCursor(QCursor(Qt::SizeBDiagCursor));
        mResizeZoneD = true;
        mResizeZoneL = true;
    }
    else if ( event->pos().x() >= (width()-spacing) )
    {
        setCursor(QCursor(Qt::SizeHorCursor));
        mResizeZoneR = true;
    }
    else if ( event->pos().x() <= spacing )
    {
        setCursor(QCursor(Qt::SizeHorCursor));
        mResizeZoneL = true;
    }
    else if ( event->pos().y() <= spacing )
    {
        setCursor(QCursor(Qt::SizeVerCursor));
        mResizeZoneU = true;
    }
    else if ( event->pos().y() >= (height()-spacing) )
    {
        setCursor(QCursor(Qt::SizeVerCursor));
        mResizeZoneD = true;
    }
    else
    {
        setCursor(QCursor(Qt::SizeAllCursor));
    }

    QGraphicsRectItem::hoverMoveEvent(event);
}

int SelectionRectangle::buffer() const
{
    return BUFFER;
}

int SelectionRectangle::border() const
{
    return BORDER;
}
