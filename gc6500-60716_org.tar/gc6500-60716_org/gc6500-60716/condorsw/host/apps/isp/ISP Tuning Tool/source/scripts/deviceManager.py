import sys
import platform
import subprocess
from subprocess import CalledProcessError

if __name__ == "__main__":
    device_type = sys.argv[1]

    shell_exec = platform.system() == 'Linux'

    if device_type == 'gw4xx':
        if platform.system() == 'Linux':
            try:
                output = subprocess.check_output(['lsusb', '-d', '0403:6010'])
                if not len(output):
                    sys.exit(1)
            except CalledProcessError as cpe:
                print ("Error: return code: {0}, output: {1}".format(cpe.returncode, cpe.output))
                sys.exit(1)
            except OSError as err:
                # lsusb missing. Cannot determine if gw4xx is connected
                print ("lsusb missing")
                sys.exit(0)
    
    if device_type == 'gc65xx':
        if platform.system() == 'Windows':
            mxcam = r'./bin/{0}/mxcam.exe'.format(device_type)
        elif platform.system() == 'Linux':
            mxcam = r'./bin/{0}/mxcam'.format(device_type)
            
        try:
            command = [mxcam] + ['list']
            output = subprocess.check_output(command, stderr=subprocess.STDOUT)
            if output.lower().find("permission denied") > -1:
                sys.exit(1)
            if output.lower().find("no compatible device found") > -1:
                sys.exit(2)
            if output.lower().find("waiting for usb boot") > -1:
                sys.exit(3)
        except CalledProcessError as cpe:
            print ("Error: return code: {0}, output: {1}".format(cpe.returncode, cpe.output))
            sys.exit(4)
    
    sys.exit(0)
