/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include "main.h"

#include <stdio.h>
#include <stdlib.h> /* free(), exit() */
#include <string.h> /* strncpy() */
#include <assert.h> /* assert() */
#include <getopt.h>
#include <signal.h> /* signal() */
#include <unistd.h> /* sleep(), fork() */
#include <sys/types.h> /* pid_t */
#include <fcntl.h> /* open() */
#include <pthread.h> /* mutex */
#include <errno.h>

#include "parsecmd.h"
#include "mxuvc.h"
#include "mxuvc_dewarp.h"

extern	const	char*	rcs_ident_version_c_func( void ) ;

#define DECLARE_SUBCMD(cmd) \
    static int app_subcmd_ ## cmd(struct app_subcmd *cmd, \
                                  struct option_val **optval,   \
                                  struct arg_val **argval)
#define DECLARE_SUBCMD_EXTERN(cmd) \
    extern int app_subcmd_ ## cmd(struct app_subcmd *cmd,               \
                                  struct option_val **optval,           \
                                  struct arg_val **argval)

int verbose=0;
video_channel_t ch = CH1;
int dw_panel = 0;
int bOverlayInit = 0;
overlay_image_params_t ovimages[8];

/*****************
 *    Options    *
 *****************/
/* Global options are considered as always OPTIONAL */
const struct app_option app_global_options[] = {
    {opt_offset, "offset", OPTIONAL, required_argument, arg_int,
     NULL, "v4l2 offset (from /dev/video0)"},
    {opt_vendor_id, "vid", OPTIONAL, required_argument, arg_int,
     NULL, "vendor id - applicable only if video backend is libusb-uvc"},
    {opt_product_id, "pid", OPTIONAL, required_argument, arg_int,
     NULL, "product id - applicable only if video backend is libusb-uvc"},
    {opt_channel, "ch", OPTIONAL, required_argument, arg_int,
     NULL,
     "channel the command should take effect on (default is 1).\n"
     "                        " "  Note: has no effect on 'video' subcommand.",
    },
    {opt_panel, "panel", OPTIONAL, required_argument, arg_int,
     NULL,
     "dewarp panel should take effect on (default is 0).\n"},
    {0, NULL, 0, 0, 0, NULL, NULL}
};

const struct app_option app_options[] = {
    {opt_vout1, "vout1", OPTIONAL, required_argument, arg_string,
     "file_path1", "stream video from channel 1 and "
     "save the content to <file_path1>"},
    {opt_vout2, "vout2", OPTIONAL, required_argument, arg_string,
     "file_path2", "stream video from channel 2 and "
     "save the content to <file_path2>"},
    {opt_vout3, "vout3", OPTIONAL, required_argument, arg_string,
     "file_path3", "stream video from channel 3 and "
     "save the content to <file_path3>"},
    {opt_vout4, "vout4", OPTIONAL, required_argument, arg_string,
     "file_path4", "stream video from channel 4 and "
     "save the content to <file_path4>"},
    {opt_vout5, "vout5", OPTIONAL, required_argument, arg_string,
     "file_path5", "stream video from channel 5 and "
     "save the content to <file_path5>"},
    {opt_vout6, "vout6", OPTIONAL, required_argument, arg_string,
     "file_path6", "stream video from channel 6 and "
     "save the content to <file_path6>"},
    {opt_vout7, "vout7", OPTIONAL, required_argument, arg_string,
     "file_path7", "stream video from channel 7 and "
     "save the content to <file_path7>"},
    {opt_aout1, "aout1", OPTIONAL, required_argument, arg_string,
     "file_path1", "stream audio from channel 1 and "
     "save the content to <file_path1>"},
    {opt_aout2, "aout2", OPTIONAL, required_argument, arg_string,
     "file_path2", "stream audio from channel 2 and "
     "save the content to <file_path2>"},
    {opt_time, "time", OPTIONAL, required_argument, arg_int,
     "seconds", "number of seconds after which to stop streaming"},
    {opt_asr1, "asr1", OPTIONAL, required_argument, arg_int,
     "samplerate", "use <samplerate> as audio sampling frequency "
     "for channel 1"},
    {opt_asr2, "asr2", OPTIONAL, required_argument, arg_int,
     "samplerate", "use <samplerate> as audio sampling frequency "
     "for channel 2"},
    {opt_spkr_mode, "mode", OPTIONAL, required_argument, arg_string,
     "mode", "speaker mode: 'on' or 'off'"},
    {opt_spkr_srate, "srate", OPTIONAL, required_argument, arg_int,
     "samplerate", "speaker sample rate in Hz"},
    {opt_spkr_gain, "gain", OPTIONAL, required_argument, arg_int,
     "gain", "speaker gain in dB"},
    {opt_vid_stats, "stats", OPTIONAL, no_argument, arg_int,
     "x", "display video statistics for each channel"},
    {opt_qparam_type, "type", OPTIONAL, required_argument, arg_string,
     "type", "parameter type (e.g: params, preProcParams) (default: params)"},
    {opt_qparam_noactivate, "noactivate", OPTIONAL, no_argument, arg_int,
     "x", "do not trigger activateCfg (default: activateCfg is triggered)"},
    {opt_ov_handle, "ovhandle", OPTIONAL, required_argument, arg_int, 
     "ovhandle", "ov object handle"},
    {opt_vsel1, "vsel1", OPTIONAL, no_argument, arg_int,
     "x", "channel selected for this particular command"},
    {opt_vsel2, "vsel2", OPTIONAL, no_argument, arg_int,
     "x", "channel selected for this particular command"},
    {opt_vsel3, "vsel3", OPTIONAL, no_argument, arg_int,
     "x", "channel selected for this particular command"},
    {opt_vsel4, "vsel4", OPTIONAL, no_argument, arg_int,
     "x", "channel selected for this particular command"},
    {opt_vsel5, "vsel5", OPTIONAL, no_argument, arg_int,
     "x", "channel selected for this particular command"},
    {opt_vsel6, "vsel6", OPTIONAL, no_argument, arg_int,
     "x", "channel selected for this particular command"},
    {opt_vsel7, "vsel7", OPTIONAL, no_argument, arg_int,
     "x", "channel selected for this particular command"},
    {0, NULL, 0, 0, 0, NULL, NULL}
};

/*******************
 *    Arguments    *
 *******************/
const struct app_arg app_args[] = {
    {arg_help_subcmd, "subcommand", OPTIONAL, arg_string,
     "display help for subcommand"},
    {arg_framerate,      "framerate",         OPTIONAL, arg_int, "framerate to set"},
    {arg_res_width,      "width",             OPTIONAL, arg_int, "width to use"},
    {arg_res_height,     "height",            OPTIONAL, arg_int, "height to use"},
    {arg_gop,            "gop_size",          OPTIONAL, arg_int, "gop size to set"},
    {arg_bitrate,        "bitrate",           OPTIONAL, arg_int, "bitrate to set (in bits/s)"},
    {arg_profile,        "profile",           OPTIONAL, arg_string, "profile to use: baseline, main or high"},
    {arg_brightness,     "brightness",        OPTIONAL, arg_int, "brightness to set"},
    {arg_contrast,       "contrast",          OPTIONAL, arg_int, "contrast to set"},
    {arg_saturation,     "saturation",        OPTIONAL, arg_int, "saturation to set"},
    {arg_hue,            "hue",               OPTIONAL, arg_int, "hue to set"},
    {arg_sharpness,      "sharpness",         OPTIONAL, arg_int, "sharpness to set"},
    {arg_gamma,          "gamma",             OPTIONAL, arg_int, "gamma to set"},
    {arg_histogram_eq,   "histogram_eq",      OPTIONAL, arg_int, "histogram equalization enable(1) or disable(0)"},
    {arg_max_analog_gain,"max_analog_gain",   OPTIONAL, arg_int, "max analog gain to set"},
    {arg_sharpen_filter, "sharpen_filter",    OPTIONAL, arg_int, "sharpen filter to set"},
    {arg_gain_multiplier,"gain_multiplier",   OPTIONAL, arg_int, "gain multiplier to set"},
    {arg_wb_mode,        "wb_mode",           OPTIONAL, arg_int, "wb mode to use (0:Auto, 1:Manual)"},
    {arg_wb_strength,    "wb_strength",       OPTIONAL, arg_int, "wb strength to use; use 0 to keep it unchanged"},
    {arg_vflip,          "vflip",             OPTIONAL, arg_int, "vflip enable(1) or disable (0)"},
    {arg_hflip,          "hflip",             OPTIONAL, arg_int, "hflip enable(1) or disable (0)"},
    {arg_max_framesize,  "max_framesize",     OPTIONAL, arg_int, "maximum framesize to set"},
    {arg_comp_quality,   "compression_quality",OPTIONAL, arg_int, "JPEG compression quality to set"},
    {arg_wdr_mode,       "wdr_mode",          OPTIONAL, arg_int, "wdr mode to use"},
    {arg_wdr_strength,   "wdr_strength",      OPTIONAL, arg_int, "wdr strength to use in manual mode."},
    {arg_sinter_mode,    "sinter_mode",       OPTIONAL, arg_int, "sinter mode to use"},
    {arg_sinter_min_nr,  "sinter_min_strn",   OPTIONAL, arg_int, "sinter min strength  to use"},
    {arg_sinter_max_nr,  "sinter_max_strn",   OPTIONAL, arg_int, "sinter max strength  to use"},
    {arg_sinter_min_thr, "sinter_min_thr",    OPTIONAL, arg_int, "sinter min threshold to use"},
    {arg_sinter_max_thr, "sinter_max_thr",    OPTIONAL, arg_int, "sinter max threshold to use"},
    {arg_sinter_trig_pt, "sinter_trig_pt",    OPTIONAL, arg_int, "sinter trigger point to use"},
    {arg_saturation_mode,"saturation_mode",   OPTIONAL, arg_int, "saturation mode; 0=AUTO, 1=MANUAL"},
    {arg_brightness_mode,"brightness_mode",   OPTIONAL, arg_int, "brightness mode; 0=AUTO, 1=MANUAL"},
    {arg_contrast_mode,  "contrast_mode",     OPTIONAL, arg_int, "contrast mode; 0=AUTO, 1=MANUAL"},
    {arg_videoformat,    "format",            OPTIONAL, arg_string, "video format to use"},
    {arg_dwmode,         "dewarp mode",       OPTIONAL, arg_int, "dewarp mode to set"},
    {arg_dweptz_hpan,    "dweptz_hpan",      MANDATORY, arg_int, "dewarp eptz HPan in degrees"},
    {arg_dweptz_vpan,    "dweptz_vpan",      MANDATORY, arg_int, "dewarp eptz VPan in degrees"},
    {arg_dweptz_tilt,    "dweptz_tilt",      MANDATORY, arg_int, "dewarp eptz Tilt in degrees"},
    {arg_dweptz_zoom,    "dweptz_zoom",      MANDATORY, arg_int, "dewarp eptz Zoom in degrees"},
    {arg_dweptz_Divisor, "dweptz_Divisor",   MANDATORY, arg_int, "dewarp eptz Divisor, all parameters will be divided by this"},
    {arg_dweptz_xcenter, "dweptz_xcenter",   OPTIONAL, arg_int, "dewarp ROI eptz X coordinate of the rectangle center as a ratio of the width of the video channel"},
    {arg_dweptz_ycenter, "dweptz_ycenter",   OPTIONAL, arg_int, "dewarp ROI eptz Y coordinate of the rectangle center as a ratio of the height of the video channel"},
    {arg_dweptz_heightfromcenter, "dweptz_heightfromcenter",    OPTIONAL, arg_int, "dewarp ROI eptz Distance from center to top of rectangle as a ratio of the height of the video channel"},
    {arg_dwzcl_phi0,     "dwzcl_phi0",       MANDATORY, arg_int, "dewarp ZCL Phi0"},
    {arg_dwzcl_rx,       "dwzcl_rx",         MANDATORY, arg_int, "dewarp ZCL Rx"},
    {arg_dwzcl_ry,       "dwzcl_ry",         MANDATORY, arg_int, "dewarp ZCL Ry"},
    {arg_dwzcl_rz,       "dwzcl_rz",         MANDATORY, arg_int, "dewarp ZCL Rz"},
    {arg_dwzcl_gshift,   "dwzcl_gshift",     MANDATORY, arg_int, "dewarp ZCL Gshift in degrees"},
    {arg_dwzcl_Divisor,  "dwzcl_Divisor",    MANDATORY, arg_int, "dewarp ZCL Divisor, all previous parameters will be divided by this"},
    {arg_dwzclc_height,  "dwzclc_height",    MANDATORY, arg_int, "dewarp ZCL Cylinder Height in micro"},
    {arg_dwzcls_hpan,    "dwzcls_hpan",      MANDATORY, arg_int, "dewarp ZCL Stretch HPan in pixel"},
    {arg_dwzcls_vpan,    "dwzcls_vpan",      MANDATORY, arg_int, "dewarp ZCL Stretch VPan in pixel"},
    {arg_dwzcls_zoom,    "dwzcls_zoom",      MANDATORY, arg_int, "dewarp ZCL Stretch Zoom Factor"},
    {arg_dwzcls_Divisor, "dwzcls_Divisor",   MANDATORY, arg_int, "dewarp ZCL Stretch Divisor, all previous parameters will be divided by this"},
    {arg_dwtm1pano_anglec,    "dwtm1pano_anglec",    MANDATORY, arg_int, "dewarp TM 1PanoView Angle Center"},
    {arg_dwtm1pano_angle,     "dwtm1pano_angle",     MANDATORY, arg_int, "dewarp TM 1PanoView Angle"},
    {arg_dwcm1pano_r0mili,    "dwcm1pano_r0mili",    MANDATORY, arg_int, "dewarp CM 1PanoView Radius0 in mili"},
    {arg_dwcm1pano_r1mili,    "dwcm1pano_r1mili",    MANDATORY, arg_int, "dewarp CM 1PanoView Radius1 in mili"},
    {arg_dwcm1pano_phi1,      "dwcm1pano_phi1",      MANDATORY, arg_int, "dewarp CM 1PanoView Phi1"},
    {arg_dwcm1pano_phishift1, "dwcm1pano_phishift1", MANDATORY, arg_int, "dewarp CM 1PanoView PhiShift1"},
    {arg_dwcm1pano_Divisor,   "dwcm1pano_Divisor",   MANDATORY, arg_int, "dewarp CM 1PanoView Divisor, all previous parameters will be divided by this"},
    {arg_dwroieptz_xcenter,        "dwroieptz_xcenter",        MANDATORY, arg_int, "dewarp ROI eptz X coordinate of the rectangle center in pixels"},
    {arg_dwroieptz_ycenter,        "dwroieptz_ycenter",        MANDATORY, arg_int, "dewarp ROI eptz Y coordinate of the rectangle center in pixels "},
    {arg_dwroieptz_heightfromcenter,        "dwroieptz_heightfromcenter",        MANDATORY, arg_int, "dewarp ROI eptz Distance from center to top of rectangle"},
    {arg_dwroieptz_sourcechannel, "dwroieptz_sourcechannel", MANDATORY, arg_int, "dewarp ROI eptz source channel - starts from 0"},
    {arg_dwroieptz_sourcepanel,   "dwroieptz_sourcepanel",   MANDATORY, arg_int, "dewarp ROI eptz source panel"},
    {arg_compositor_mode,     "compositor_mode", OPTIONAL, arg_int,    "compositor mode to set: 0-OFF, 1-ON, 2-SELECT, 3-UNSELECT"}, 
    {arg_compositor_x,        "panel_x_offset",  OPTIONAL, arg_int,    "panel x offset in pixels"},
    {arg_compositor_y,        "panel_y_offset",  OPTIONAL, arg_int,    "panel y offset in pixels "},
    {arg_compositor_width,    "panel_width",     OPTIONAL, arg_int,    "panel width in pixels"},
    {arg_compositor_height,   "panel_height",    OPTIONAL, arg_int,    "panel height in pixels"},
    {arg_aec_mode,            "aec_mode",        MANDATORY, arg_string,  "AEC mode: 'on' or 'off'"},
    {arg_volume,              "volume",          OPTIONAL, arg_int,  "volume: value between 0 and 100"},
    {arg_qparam_object,       "object",          MANDATORY, arg_string, "name of the object to set the parameter for (e.g. vcap0)"},
    {arg_qparam_name,         "parameter",       MANDATORY, arg_string, "name of the parameter to set (e.g. Q_VCAP_CMP_CROP_OFFSET_X)"},
    {arg_qparam_value,        "value",           MANDATORY, arg_int,    "value (int) to set the parameter to"},
    {arg_vbrparams_minbitrate,        "minbitrate",        OPTIONAL, arg_int, "minimum bitrate to set (in bits/s)"},
    {arg_pause360demo_mode,           "pause360demo",      MANDATORY, arg_int,    "boolean value, set to pause, clear to unpause"},
    {arg_ovimage_file,        "ov_image_file",   MANDATORY, arg_string, "overlay image (logo) filename/path"},
    {arg_ovimage_cmd,         "ov_image_cmd",    MANDATORY, arg_int,    "ov image Add (1), Remove (0)"},
    {arg_ovimage_xoff,        "ov_image_xoff",   MANDATORY, arg_int,    "ov image startX"}, 
    {arg_ovimage_yoff,        "ov_image_yoff",   MANDATORY, arg_int,    "ov image startY"}, 
    {arg_ovimage_width,       "ov_image_width",  MANDATORY, arg_int,    "ov image width"}, 
    {arg_ovimage_height,      "ov_image_height", MANDATORY, arg_int,    "ov image height"}, 
    {arg_burnin_fontfile,     "burnin_fontfile", MANDATORY, arg_string, "burnin font file"}, 
    {arg_burnin_fontsize,     "burnin_fontsize", MANDATORY, arg_int,    "burnin font size"},
    {arg_burnin_text_id,      "burnin_text_id",  MANDATORY, arg_int,    "burnin text index"},
    {arg_burnin_text_cmd,     "burnin_text_cmd", MANDATORY, arg_int,    "burnin text command {0/1}"},
    {arg_burnin_text_string,  "burnin_text_str", MANDATORY, arg_string, "burnin text string"},
    {arg_burnin_text_xoff,    "burnin_text_xoff",MANDATORY, arg_int,    "burnin text xstart"},
    {arg_burnin_text_yoff,    "burnin_text_yoff",MANDATORY, arg_int,    "burnin text ystart"},
    {arg_burnin_time_show,    "burnin_time_show",MANDATORY, arg_int,    "burnin time show/hide"},
    {arg_burnin_time_xoff,    "burnin_time_xoff",OPTIONAL,  arg_int,    "burnin time x.start"},
    {arg_burnin_time_yoff,    "burnin_time_yoff",OPTIONAL,  arg_int,    "burnin time y.start"},
    {arg_ispcfg_file,         "ispcfg_file",    MANDATORY, arg_string, "ispcfg binary file"}, 
    {arg_crop_enable,	      "crop_enable",    OPTIONAL, arg_int,    "crop enable/disable : 0-DISABLE, 1-ENABLE"}, 
    {arg_crop_x,		      "crop_x_offset",  OPTIONAL, arg_int,    "crop x offset in pixels"},
    {arg_crop_y,		      "crop_y_offset",  OPTIONAL, arg_int,    "crop y offset in pixels "},
    {arg_crop_width,	      "crop_width",	    OPTIONAL, arg_int,    "crop width in pixels"},
    {arg_crop_height,         "crop_height",	OPTIONAL, arg_int,    "crop height in pixels"},
    {arg_sensorframerate,     "sensorframerate",OPTIONAL, arg_int,    "sensor framerate to set"},
    {arg_aeroi_mode,          "aeroi_mode",     OPTIONAL, arg_int,   "ae roi mode : 0-Reset to default ROI, 1-Use the passed ROI only, 2-Add the passed ROI, 3-Remove the passed ROI"}, 
    {arg_aeroi_x,             "aeroi_x_offset", OPTIONAL, arg_int,    "ae roi x offset in pixels"},
    {arg_aeroi_y,             "aeroi_y_offset", OPTIONAL, arg_int,    "ae roi y offset in pixels "},
    {arg_aeroi_width,         "aeroi_width",    OPTIONAL, arg_int,    "ae roi width in pixels"},
    {arg_aeroi_height,        "aeroi_height",   OPTIONAL, arg_int,    "ae roi height in pixels"},
    {arg_tsvc_level,    "tsvc_level",	 OPTIONAL, arg_int,    "TSVC level"},

    {0, NULL, 0, 0, NULL}
};

/*********************
 *    Subcommands    *
 *********************/
DECLARE_SUBCMD(help);

/* Core video commands: in core_video.c */
DECLARE_SUBCMD_EXTERN(stream);
DECLARE_SUBCMD_EXTERN(iframe);
DECLARE_SUBCMD_EXTERN(resolution);
DECLARE_SUBCMD_EXTERN(framerate);
DECLARE_SUBCMD_EXTERN(gop);
DECLARE_SUBCMD_EXTERN(bitrate);
DECLARE_SUBCMD_EXTERN(profile);
DECLARE_SUBCMD_EXTERN(framesize);
DECLARE_SUBCMD_EXTERN(compqual);
DECLARE_SUBCMD_EXTERN(vidformat);
DECLARE_SUBCMD_EXTERN(sensorframerate);
DECLARE_SUBCMD_EXTERN(tsvc_level);

/* ISP commands: in isp.c */
DECLARE_SUBCMD_EXTERN(brightness);
DECLARE_SUBCMD_EXTERN(contrast);
DECLARE_SUBCMD_EXTERN(saturation);
DECLARE_SUBCMD_EXTERN(hue);
DECLARE_SUBCMD_EXTERN(wdr);
DECLARE_SUBCMD_EXTERN(sinter);
DECLARE_SUBCMD_EXTERN(vflip);
DECLARE_SUBCMD_EXTERN(hflip);
DECLARE_SUBCMD_EXTERN(saturationmode);
DECLARE_SUBCMD_EXTERN(brightnessmode);
DECLARE_SUBCMD_EXTERN(contrastmode);
DECLARE_SUBCMD_EXTERN(sharpness);
DECLARE_SUBCMD_EXTERN(gamma);
DECLARE_SUBCMD_EXTERN(histogrameq);
DECLARE_SUBCMD_EXTERN(maxanaloggain);
DECLARE_SUBCMD_EXTERN(sharpenfilter);
DECLARE_SUBCMD_EXTERN(gainmultiplier);
DECLARE_SUBCMD_EXTERN(wb);

/* Dewarp commands: in dewarp.c */
DECLARE_SUBCMD_EXTERN(dwmode);
DECLARE_SUBCMD_EXTERN(dweptz);
DECLARE_SUBCMD_EXTERN(dwroieptz);
DECLARE_SUBCMD_EXTERN(dwzcl);
DECLARE_SUBCMD_EXTERN(dwzclc);
DECLARE_SUBCMD_EXTERN(dwzcls);
DECLARE_SUBCMD_EXTERN(dwtm1pano);
DECLARE_SUBCMD_EXTERN(dwcm1pano);
DECLARE_SUBCMD_EXTERN(pause360demo);

/* Compositor commands: in compositor.c */
DECLARE_SUBCMD_EXTERN(compositor);

/* Audio commands: in audio.c */
DECLARE_SUBCMD_EXTERN(volume);
DECLARE_SUBCMD_EXTERN(aec);
DECLARE_SUBCMD_EXTERN(speaker);
DECLARE_SUBCMD_EXTERN(audstats);

/* QParam */
DECLARE_SUBCMD_EXTERN(qparam);

/* Overlay: in overlay.c */
DECLARE_SUBCMD_EXTERN(burnininit);
DECLARE_SUBCMD_EXTERN(burnintext);
DECLARE_SUBCMD_EXTERN(burnintime);
DECLARE_SUBCMD_EXTERN(ovimage);
DECLARE_SUBCMD_EXTERN(ispcfg);

DECLARE_SUBCMD_EXTERN(vbrparams);
DECLARE_SUBCMD_EXTERN(crop);
DECLARE_SUBCMD_EXTERN(aeroi);


struct app_subcmd app_subcmds[] = {
    {"help", "describe the usage of mxcam or subcommands", SUBCMD_OTHER,
     app_subcmd_help, {0}, {arg_help_subcmd} },
    {"stream", "stream from the selected channels.",
     SUBCMD_VIDEO | SUBCMD_AUDIO, app_subcmd_stream,
     {opt_time, opt_vout1, opt_vout2, opt_vout3, opt_vout4, opt_vout5,
      opt_vout6, opt_vout7,
    #ifdef AUDIO_BACKEND
      opt_aout1, opt_aout2, opt_asr1, opt_asr2,
    #endif
     opt_vid_stats}, {0} },
    {"iframe", "force AVC iframe", SUBCMD_VIDEO,
     app_subcmd_iframe, {0}, {0} },
    {"resolution", "set/get the resolution", SUBCMD_VIDEO,
     app_subcmd_resolution, {0}, {arg_res_width, arg_res_height} },
    {"vidformat", "set/get the video format", SUBCMD_VIDEO,
     app_subcmd_vidformat, {0}, {arg_videoformat} },
    {"framerate", "set/get the framerate", SUBCMD_VIDEO,
     app_subcmd_framerate, {0}, {arg_framerate} },
    {"gop", "set/get the AVC gop size", SUBCMD_VIDEO,
     app_subcmd_gop, {0}, {arg_gop} },
    {"bitrate", "set/get the AVC bitrate", SUBCMD_VIDEO,
     app_subcmd_bitrate, {0}, {arg_bitrate} },
    {"profile", "set/get the AVC profile", SUBCMD_VIDEO,
     app_subcmd_profile, {0}, {arg_profile} },
    {"framesize", "set/get the maximum AVC frame size", SUBCMD_VIDEO,
     app_subcmd_framesize, {0}, {arg_max_framesize} },
    {"compqual", "set/get the JPEG compression quality", SUBCMD_VIDEO,
     app_subcmd_compqual, {0}, {arg_comp_quality} },
    {"brightness", "set/get the brightness", SUBCMD_VIDEO,
     app_subcmd_brightness, {0}, {arg_brightness} },
    {"contrast", "set/get the contrast", SUBCMD_VIDEO,
     app_subcmd_contrast, {0}, {arg_contrast} },
    {"saturation", "set/get the saturation", SUBCMD_VIDEO,
     app_subcmd_saturation, {0}, {arg_saturation} },
    {"hue", "set/get the hue", SUBCMD_VIDEO,
     app_subcmd_hue, {0}, {arg_hue} },
    {"wdr", "set/get the wdr mode settings", SUBCMD_VIDEO,
     app_subcmd_wdr, {0}, {arg_wdr_mode, arg_wdr_strength} },
    {"sinter", "set/get the sinter mode settings", SUBCMD_VIDEO,
     app_subcmd_sinter, {0}, {arg_sinter_mode, arg_sinter_min_nr,
     arg_sinter_max_nr, arg_sinter_min_thr, arg_sinter_max_thr,
     arg_sinter_trig_pt} },
    {"saturationmode", "set/get the saturation mode", SUBCMD_VIDEO,
     app_subcmd_saturationmode, {0}, {arg_saturation_mode} },
    {"brightnessmode", "set/get the brightness mode", SUBCMD_VIDEO,
     app_subcmd_brightnessmode, {0}, {arg_brightness_mode} },
    {"contrastmode", "set/get the contrast mode", SUBCMD_VIDEO,
     app_subcmd_contrastmode, {0}, {arg_contrast_mode} },
    {"sharpness", "set/get the sharpness", SUBCMD_VIDEO,
     app_subcmd_sharpness, {0}, {arg_sharpness} },
    {"gamma", "set/get the gamma", SUBCMD_VIDEO,
     app_subcmd_gamma, {0}, {arg_gamma} },
    {"histogrameq", "enable/disable the histogram equalization", SUBCMD_VIDEO,
     app_subcmd_histogrameq, {0}, {arg_histogram_eq} },
    {"maxanaloggain", "set/get the max analog gain", SUBCMD_VIDEO,
     app_subcmd_maxanaloggain, {0}, {arg_max_analog_gain} },
    {"sharpenfilter", "set/get the sharpen filter", SUBCMD_VIDEO,
     app_subcmd_sharpenfilter, {0}, {arg_sharpen_filter} },
    {"gainmultiplier", "set/get the gain multiplier", SUBCMD_VIDEO,
     app_subcmd_gainmultiplier, {0}, {arg_gain_multiplier} },
    {"wb", "set/get the wb settings", SUBCMD_VIDEO,
     app_subcmd_wb, {0}, {arg_wb_mode, arg_wb_strength} },
    {"vflip", "set/get vflip", SUBCMD_VIDEO,
     app_subcmd_vflip, {0}, {arg_vflip} },
    {"hflip", "set/get hflip", SUBCMD_VIDEO,
     app_subcmd_hflip, {0}, {arg_hflip} },
    {"dweptz", "set the dewarp EPTZ mode settings", SUBCMD_VIDEO,
     app_subcmd_dweptz, {0}, {arg_dweptz_hpan, arg_dweptz_vpan,
                              arg_dweptz_tilt, arg_dweptz_zoom, arg_dweptz_Divisor,
                              arg_dweptz_xcenter, arg_dweptz_ycenter, arg_dweptz_heightfromcenter}},
    {"dwzcl",  "set the dewarp ZCL settings", SUBCMD_VIDEO,
     app_subcmd_dwzcl, {0}, {arg_dwzcl_phi0, arg_dwzcl_rx, arg_dwzcl_ry,
                             arg_dwzcl_rz, arg_dwzcl_gshift, arg_dwzcl_Divisor}},
    {"dwzclc", "set the dewarp ZCL Cylinder settings", SUBCMD_VIDEO,
     app_subcmd_dwzclc, {0}, {arg_dwzclc_height} },
    {"dwzcls", "set the dewarp ZCL Stretch settings", SUBCMD_VIDEO,
     app_subcmd_dwzcls, {0}, {arg_dwzcls_hpan, arg_dwzcls_vpan,
                              arg_dwzcls_zoom, arg_dwzcls_Divisor} },
    {"dwtm1pano", "set the dewarp TM 1pano view pan settings", SUBCMD_VIDEO,
     app_subcmd_dwtm1pano, {0}, {arg_dwtm1pano_anglec, arg_dwtm1pano_angle, arg_dwtm1pano_Divisor} },
    {"dwcm1pano", "set the dewarp CM 1pano view pan settings", SUBCMD_VIDEO,
     app_subcmd_dwcm1pano, {0}, {arg_dwcm1pano_r0mili, arg_dwcm1pano_r1mili,
                                 arg_dwcm1pano_phi1, arg_dwcm1pano_phishift1, arg_dwcm1pano_Divisor} },
    {"dwroieptz", "set the dewarp ROI EPTZ mode settings", SUBCMD_VIDEO,
     app_subcmd_dwroieptz, {0}, {arg_dwroieptz_xcenter, arg_dwroieptz_ycenter,
                              arg_dwroieptz_heightfromcenter,
                              arg_dwroieptz_sourcechannel, arg_dwroieptz_sourcepanel}},
    {"dwmode", "set/get the dewarp mode(set will restore default map params)",
     SUBCMD_VIDEO, app_subcmd_dwmode, {0}, {arg_dwmode} },
    {"pause360demo", "pause/unpause ME360 dewarp demo mode", SUBCMD_VIDEO,
     app_subcmd_pause360demo, {0}, {arg_pause360demo_mode} },
    {"compositor", "set/get the compositor mode and panel parameters "
     "(required in mode 1 only)", SUBCMD_VIDEO,
     app_subcmd_compositor, {0}, {arg_compositor_mode, arg_compositor_x,
                                  arg_compositor_y, arg_compositor_width,
                                  arg_compositor_height} },
    {"aec", "enable/disable AEC", SUBCMD_CUSTOM_CONTROL,
     app_subcmd_aec, {0}, {arg_aec_mode}},
    {"volume", "set/get the audio volume", SUBCMD_AUDIO,
     app_subcmd_volume, {0}, {arg_volume}},
    {"speaker", "configure the speaker", SUBCMD_CUSTOM_CONTROL,
     app_subcmd_speaker, {opt_spkr_mode, opt_spkr_srate, opt_spkr_gain}, {0}},
    {"qparam", "perform a codec setParam", SUBCMD_CUSTOM_CONTROL,
     app_subcmd_qparam, {opt_qparam_type, opt_qparam_noactivate},
     {arg_qparam_object,arg_qparam_name, arg_qparam_value}},
    {"audstats", "display audio stats", SUBCMD_CUSTOM_CONTROL,
     app_subcmd_audstats, {0}, {0}},
    {"vbrparams", "get/set the VBR parameters", SUBCMD_VIDEO,
     app_subcmd_vbrparams, {0}, {arg_vbrparams_minbitrate}},
    {"ovimage", "add/remove overlay image", SUBCMD_OVERLAY_CTRL, 
      app_subcmd_ovimage, {opt_ov_handle},  
     {arg_ovimage_file, arg_ovimage_cmd,
      arg_ovimage_xoff, arg_ovimage_yoff, arg_ovimage_width, arg_ovimage_height}},
    {"burnin.init", "initializing burnin with font files/size", SUBCMD_CUSTOM_CONTROL,
      app_subcmd_burnininit, {opt_vsel1, opt_vsel2, opt_vsel3, opt_vsel4, opt_vsel5, opt_vsel6, opt_vsel7}, 
      {arg_burnin_fontfile, arg_burnin_fontsize}},
    {"burnin.text", "add/remove burnin text", SUBCMD_CUSTOM_CONTROL,
      app_subcmd_burnintext, {0}, 
      {arg_burnin_text_id, arg_burnin_text_cmd, arg_burnin_text_string, arg_burnin_text_xoff, arg_burnin_text_yoff}},
    {"burnin.time", "show/hide the burnin time", SUBCMD_CUSTOM_CONTROL,
      app_subcmd_burnintime, {0}, 
      {arg_burnin_time_show, arg_burnin_time_xoff, arg_burnin_time_yoff}},
    {"ispcfg", "updating ISP configuration", SUBCMD_CUSTOM_CONTROL,
      app_subcmd_ispcfg, {0},
      {arg_ispcfg_file}},
    {"crop", "set/get the crop window", SUBCMD_VIDEO,
      app_subcmd_crop, {0}, {arg_crop_enable, arg_crop_x, arg_crop_y, arg_crop_width, arg_crop_height} },
    {"aeroi", "set/get the ae roi window", SUBCMD_VIDEO,
      app_subcmd_aeroi, {0}, {arg_aeroi_mode, arg_aeroi_x, arg_aeroi_y, arg_aeroi_width, arg_aeroi_height} },  
    {"sensorframerate", "set/get the sensor framerate", SUBCMD_VIDEO,
     app_subcmd_sensorframerate, {0}, {arg_sensorframerate} },
    {"tsvc_level", "set/get the TSVC level", SUBCMD_VIDEO,
     app_subcmd_tsvc_level, {0}, {arg_tsvc_level} },
    {NULL, NULL, 0, NULL, {0}, {0}}
};


/* HELP */
int app_subcmd_help(struct app_subcmd *cmd, struct option_val **optval, \
                    struct arg_val **argval)
{
    const char *sc;
    struct app_subcmd *subcmd;

    /* No subcommand arg given on command line. Display general help */
    if(!has_arg(arg_help_subcmd, argval)) {
        available_subcommands();
        return 0;
    }

    sc = get_value_from_arg(arg_help_subcmd, argval);

    subcmd = get_subcmd_from_name(sc);
    /* Unknown subcommand */
    if(subcmd == NULL) {
        printf("Cannot display help for unknown subcommand '%s'.\n", sc);
        return 1;
    }

    print_usage(subcmd);
    return 0;
}


int main(int in_argc, char **in_argv) {

    int ret;
    //int waitCnt=15000;
    struct app_subcmd *subcmd;
    char *subcmd_name;
    struct option_val *options_val[MAX_OPTIONS] = {NULL};
    struct arg_val *args_val[MAX_OPTIONS] = {NULL};
    struct option long_options[MAX_TOTAL_OPTIONS];
    int status = 0;
    int i;

    ret = parsecmd(in_argc, in_argv, &subcmd, &subcmd_name,
                   (struct option_val **) &options_val,
                   (struct arg_val **) &args_val,
                   (struct option *) &long_options);

    if(ret != 0)
        return 1;

    if((strcmp(subcmd_name, "help")==0) || (strcmp(subcmd_name, "version")==0)) {
        ret = subcmd->func(subcmd, options_val, args_val);
        return 0;
    }

    /* Check for verbose global OPTION */
    if (has_option(opt_verbose, options_val)){
        verbose=1;
    }

    /* Record channel if given */
    if(has_option(opt_channel, options_val))
        ch = ((video_channel_t) (get_value_from_option(opt_channel,
                                                      options_val))) - 1;
    if(has_option(opt_panel, options_val))
        dw_panel = ((intptr_t) (get_value_from_option(opt_panel, options_val)));

    /* Things to do before the subcommand is run */
    if(subcmd->type & SUBCMD_VIDEO) {

        /* Init mxuvc video */
        char video_backend[32] = "v4l2";
        char opt_str[256];

#ifdef VIDEO_BACKEND
        if(strncmp(VIDEO_BACKEND, "libusb-uvc", 10) == 0) {
            int vid=DEFAULT_VENDOR_ID;
            int pid=DEFAULT_PRODUCT_ID;

            /* Set vid & pid if given */
            if(has_option(opt_vendor_id, options_val))
                vid = (intptr_t) (get_value_from_option(opt_vendor_id, options_val));
            if(has_option(opt_product_id, options_val))
                pid = (intptr_t) (get_value_from_option(opt_product_id, options_val));

            sprintf(opt_str, "vid=0x%x;pid=0x%x;", vid,pid);
            strncpy(video_backend, "libusb-uvc",10);
        }
        else
#endif
        {
            int dev_offset = 0;

            /* Set dev_offset if given */
            if(has_option(opt_offset, options_val))
                dev_offset = (intptr_t) (get_value_from_option(opt_offset, options_val));

            sprintf(opt_str, "dev_offset=%i", dev_offset);
        }

#if 0
	ret = 0;
        while (waitCnt-- > 0) {
            ret = mxuvc_video_init("v4l2",offset_str);
            if ( ret >=0 ) break;
	    usleep(10000);
        }
#else
        ret = mxuvc_video_init(video_backend, opt_str);
#endif  
        if(ret < 0)
        {
            status = 1;
            goto app_exit;
        }

        //printf("-> watiCnt=%d wait time = %d ms\n", waitCnt, (15000-waitCnt)*10 );
    }

    #ifdef AUDIO_BACKEND
    if(subcmd->type & SUBCMD_AUDIO) {
        ret = mxuvc_audio_init("alsa","device = Condor");
        if(ret < 0) {
            ret = mxuvc_audio_init("alsa","device = MAX64380");
            /* Allow 'stream' command to go through even
               if audio fails to start */
            if(ret < 0 && strcmp(subcmd_name, "stream"))
            {
                status = 1;
                goto app_exit;
            }
        }
    }
    #endif

    if(subcmd->type & SUBCMD_CUSTOM_CONTROL)
        mxuvc_custom_control_init();

    /* Execute subcommand */
    ret = subcmd->func(subcmd, options_val, args_val);

    /* Things to do after the subcommand is run */
    if(subcmd->type &  SUBCMD_VIDEO)
        mxuvc_video_deinit();

    #ifdef AUDIO_BACKEND
    if(subcmd->type & SUBCMD_AUDIO)
        mxuvc_audio_deinit();
    #endif

    if(subcmd->type & SUBCMD_CUSTOM_CONTROL)
        mxuvc_custom_control_deinit();

app_exit:

    for(i=0;i<MAX_OPTIONS;i++)
        if(options_val[i] != NULL)
            free(options_val[i]);

    return status;
}
