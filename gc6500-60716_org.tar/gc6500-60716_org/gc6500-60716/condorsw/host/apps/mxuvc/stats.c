/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include "stats.h"
#include <stdio.h> /* printf() */
#include <sys/time.h> /* gettimeofday() */

inline int ringbuffer_next(int idx) { return (idx+1) & (RINGBUFFER_SIZE-1); }
inline int ringbuffer_prev(int idx) { return (idx-1) & (RINGBUFFER_SIZE-1); }

void video_stats_update(stats_ringbuffer_t *rb, unsigned int size, int bIsKeyFrm)
{
    int i;
    int cur_idx  = rb->cur_idx;
    int prev_idx = ringbuffer_prev(cur_idx);
    int next_idx = ringbuffer_next(cur_idx);

    /* Save the new timestamp */
    gettimeofday(&(rb->timestamp[cur_idx]), NULL);

    /* Cumulative sum of the frame sizes */
    rb->framesize[cur_idx] = rb->framesize[prev_idx] + size;

    /* key frame */
    rb->frm_type[cur_idx] = bIsKeyFrm;

    /* curr frame size */
    rb->frm_size[cur_idx] = size;

    /* Make sure the cumulative frame size sum does not overflow.
       We simply substract the value of the sum at index 0 from all elements
       when we reach the end of the ringbuffer. */
    if(next_idx == 0) {
        for(i=0; i< RINGBUFFER_SIZE; i++)
            rb->framesize[i] -= rb->framesize[0];
    }

    /* Update current index */
    rb->cur_idx = next_idx;
}

void video_stats_get(stats_ringbuffer_t *rb, struct video_stats *stats)
{
    struct timeval tv_now, tv;
    int i, idx, num_frames;
    int idx_last;
    int rolling_time_sec[3] = {LOW_AVG_SECS, MED_AVG_SECS, HGH_AVG_SECS};

    gettimeofday(&tv_now, NULL);
    stats->nCurSec = tv_now.tv_sec;
    idx = idx_last = ringbuffer_prev(rb->cur_idx);
    num_frames = 0;

    /* Gathering stats for the three rolling windows */
    for(i=0; i<3; i++) {
        stats->maxfrmsize[i] = 0;
        stats->ifrmcount[i] = 0;
        tv.tv_sec  = tv_now.tv_sec - rolling_time_sec[i];
        tv.tv_usec = tv_now.tv_usec;

        while(timercmp(&(rb->timestamp[idx]), &tv, >)) {
            if (rb->frm_size[idx] > stats->maxfrmsize[i]) {
                stats->maxfrmsize[i] = rb->frm_size[idx];
            }
            if (rb->frm_type[idx] == 1) { stats->ifrmcount[i] += 1; }
            num_frames++;
            idx = ringbuffer_prev(idx);
        }

        int ms_elapsed = 1000 * rolling_time_sec[i];
        stats->framerate_x10[i] = 10*num_frames*1000 / ms_elapsed;
        stats->bitrate[i] =
            8 * (rb->framesize[idx_last] - rb->framesize[idx])
            / ms_elapsed;
    }
}
