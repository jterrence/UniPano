/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef __PARSECMD_H__
#define __PARSECMD_H__

#include <getopt.h>
#include <string.h>

#define printf_bold(args...) \
    do {                             \
        printf("%c[1m", 27);         \
        printf(args);                \
        printf("%c[0m", 27);         \
    } while(0)

#define MAX_TOTAL_OPTIONS 50 /* max number of different options in mxcam */
#define MAX_OPTIONS 20 /* max number of options per sub command */
#define MAX_ARGS 20 /* max number of args per sub commands */

/* Options that have no short option char should use an identifying
 * integer equal to or greater than this. */
#define FIRST_LONGOPT_ID 256
#define MANDATORY 0
#define OPTIONAL  1

typedef enum {
    arg_notype,
    arg_int,
    arg_string,
    arg_uint,
} arg_type_t;


/*****************
 *    Options    *
 *****************/
struct app_option {
    int id; /* app_longopt_id_t */
    const char *long_name;
    int is_optional;
    int has_arg;
    arg_type_t arg_type;
    const char *arg_str;
    const char *help;
};

struct option_val {
    const struct app_option *opt;
    const char *val;
};

//const struct app_option app_global_options[];
//const struct app_option app_options[];

/*******************
 *    Arguments    *
 *******************/
struct app_arg {
    int id; /* app_arg_id_t */
    const char *name;
    int is_optional;
    arg_type_t type;
    const char *help;
};

struct arg_val {
	const struct app_arg *arg;
	const char *val;
};

//const struct app_arg app_args[];

/*********************
 *    Subcommands    *
 *********************/
typedef enum {
    SUBCMD_OTHER          = 0,
    SUBCMD_VIDEO          = 1 << 0,
    SUBCMD_AUDIO          = 1 << 1,
    SUBCMD_CUSTOM_CONTROL = 1 << 2,
    SUBCMD_OVERLAY_CTRL   = 1 << 3,
} subcmd_type_t;

struct app_subcmd {
    const char *name;
    const char *help;
    subcmd_type_t type;
    int (*func)(struct app_subcmd *cmd,
                struct option_val **optval,
                struct arg_val **argval);
    int options[MAX_OPTIONS];
    arg_type_t args[MAX_ARGS];
};

//struct app_subcmd app_subcmds[];

/**************************
 *    Helper functions    *
 **************************/
const struct app_option* get_option_from_id(int id); /* app_longopt_id_t */
const struct app_arg* get_arg_from_id(int id); /* app_arg_id_t */
struct app_subcmd* get_subcmd_from_name(const char *name);
int has_option(int id, struct option_val **optval); /* app_longopt_id_t */
const void* get_value_from_option(int id, /* app_longopt_id_t */
                                  struct option_val **optval);
int has_arg(int id, struct arg_val **argval); /* app_arg_id_t */
const void* get_value_from_arg(int id, struct arg_val **argval); /* app_arg_id_t */
void available_subcommands();
void print_usage(struct app_subcmd *cmd);

int parsecmd(int in_argc, char **in_argv,
             struct app_subcmd **subcmd,
             char **subcmd_name,
             struct option_val **options_val,
             struct arg_val **args_val,
             struct option *long_options);

#endif
