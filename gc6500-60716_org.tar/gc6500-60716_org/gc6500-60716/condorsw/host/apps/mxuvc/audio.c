/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include <stdio.h>
#include <unistd.h> /* sleep() */

#include "parsecmd.h"
#include "main.h"
#include "mxuvc.h"

extern audio_channel_t ch;

/* Volume */
int app_subcmd_volume(struct app_subcmd *cmd,
                      struct option_val **optval,
                      struct arg_val **argval)
{
    int volume;

    if(has_arg(arg_volume, argval)) {
        volume = (intptr_t) get_value_from_arg(arg_volume, argval);
        if(volume < 0 || volume > 100) {
            printf("<volume> must be a value between 0 and 100.\n");
            return -1;
        }

        if(mxuvc_audio_set_volume(volume) < 0) {
            printf("Unable to set the AEC mode.\n");
            return -1;
        }
    } else {
        volume = mxuvc_audio_get_volume();
        if(volume < 0)
            return -1;
        printf("%i\n", volume);
    }

    return 0;
}

/* AEC */
int app_subcmd_aec(struct app_subcmd *cmd,
                   struct option_val **optval,
                   struct arg_val **argval)
{
    const char *aec_mode;
    int ret = -1;

    aec_mode = (const char*) get_value_from_arg(arg_aec_mode, argval);

    if(strncmp(aec_mode, "on", 2) == 0)
        ret = mxuvc_custom_control_enable_aec();
    else if (strncmp(aec_mode, "off", 2) == 0)
        ret = mxuvc_custom_control_disable_aec();
    else {
        printf("<aec_mode> argument must be either 'on' or 'off'\n");
        ret = -1;
    }

    if(ret < 0)
        printf("Unable to set the AEC mode.\n");

    return ret;
}

/* Speaker sample rate */
int app_subcmd_speaker(struct app_subcmd *cmd,
                       struct option_val **optval,
                       struct arg_val **argval)
{
    uint32_t samplerate, gain;
    const char *spkr_mode;
    int ret = -1;
    int is_a_get = 1;

    if(has_option(opt_spkr_mode, optval)) {
        is_a_get = 0;
        spkr_mode = (const char*) get_value_from_option(opt_spkr_mode, optval);

        if(strncmp(spkr_mode, "on", 2) == 0)
            ret = mxuvc_custom_control_enable_spkr();
        else if (strncmp(spkr_mode, "off", 2) == 0)
            ret = mxuvc_custom_control_disable_spkr();
        else {
            printf("<mode> must be either 'on' or 'off'\n");
            ret = -1;
        }

        if(ret < 0)
            printf("Unable to set the speaker mode.\n");
    }

    if(has_option(opt_spkr_srate, optval)) {
        is_a_get = 0;
        samplerate = (uintptr_t) get_value_from_option(opt_spkr_srate, optval);

        ret = mxuvc_custom_control_set_spkr_samplerate(samplerate);

        if(ret < 0)
            printf("Unable to set the speaker sample rate.\n");
    }

    if(has_option(opt_spkr_gain, optval)) {
        is_a_get = 0;
        gain = (uintptr_t) get_value_from_option(opt_spkr_gain, optval);

        ret = mxuvc_custom_control_set_spkr_gain(gain);

        if(ret < 0)
            printf("Unable to set the speaker gain.\n");
    }

    if(is_a_get == 1) {
        ret  = mxuvc_custom_control_get_spkr_samplerate(&samplerate);
        if(ret < 0) {
            printf("Unable to get the speaker sample rate.\n");
            return ret;
        }
        ret = mxuvc_custom_control_get_spkr_gain(&gain);;
        if(ret < 0) {
            printf("Unable to get the speaker gain.\n");
            return ret;
        }
        printf("Samplerate : %5i Hz\n", samplerate);
        printf("Gain       : %5i dB\n", gain);
    }
    return ret;
}

/* Audio Stats */
int app_subcmd_audstats(struct app_subcmd *cmd,
                        struct option_val **optval,
                        struct arg_val **argval)
{
    asp_metadata audio_info;
    int ret;

    ret = mxuvc_custom_control_get_audio_stats(&audio_info);
    if(ret < 0) {
        printf("Unable to get the audio statistics.\n");
    } else {
        printf("Gain       : %7i\n", audio_info.gain);
        printf("STE        : %7.3f\n", audio_info.frameStats.ste);
        printf("ZCR        : %7.3f\n", audio_info.frameStats.zcr);
        printf("Pitch      : %7.3f\n", audio_info.frameStats.pitch);
        printf("Bulk delay : %7i\n", audio_info.bulk_delay_found
               ? audio_info.bulk_delay_samples : 0);
    }

    return ret;
}
