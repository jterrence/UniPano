/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef __STATS_H__
#define __STATS_H__

#include <sys/time.h> /* struct timeval */

#define LOW_AVG_SECS 1
#define MED_AVG_SECS 10
#define HGH_AVG_SECS 30
#define RINGBUFFER_SIZE 2048 // must be a power of 2

typedef struct {
    int cur_idx;
    int frm_type[RINGBUFFER_SIZE];
    int frm_size[RINGBUFFER_SIZE];
    int framesize[RINGBUFFER_SIZE];
    struct timeval timestamp[RINGBUFFER_SIZE];
} stats_ringbuffer_t;


int ringbuffer_next(int idx);
int ringbuffer_prev(int idx);

struct video_stats {
    int nCurSec;
    int framerate_x10[3];
    int bitrate[3];
    int maxfrmsize[3];
    int ifrmcount[3];
};

void video_stats_update(stats_ringbuffer_t *rb, unsigned int size, int bIsKeyFrm);
void video_stats_get(stats_ringbuffer_t *rb, struct video_stats *stats);

#endif
