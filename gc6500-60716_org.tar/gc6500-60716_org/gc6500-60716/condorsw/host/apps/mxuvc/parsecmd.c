/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include "parsecmd.h"
#include <stdio.h>
#include <stdlib.h> /* free(), exit() */
#include <stdint.h> /* intptr_t */
#include <assert.h>

extern const struct app_option app_global_options[];
extern const struct app_option app_options[];
extern const struct app_arg app_args[];
extern struct app_subcmd app_subcmds[];

const struct app_option* get_option_from_id(int id) /* app_longopt_id_t */
{
	int i;
	for (i=0; app_global_options[i].long_name; i++) {
		if (app_global_options[i].id == id)
			return &app_global_options[i];
	}
	for (i=0; app_options[i].long_name; i++) {
		if (app_options[i].id == id)
			return &app_options[i];
	}
	return NULL;
}

const struct app_arg* get_arg_from_id(int id) /* app_arg_id_t */
{
	int i;
	for (i=0; app_args[i].id; i++) {
		if (app_args[i].id == id)
			return &app_args[i];
	}
	return NULL;
}

struct app_subcmd* get_subcmd_from_name(const char *name)
{
	int i;
	for (i=0; app_subcmds[i].name; i++) {
		if (strcmp(app_subcmds[i].name, name) == 0)
			return &app_subcmds[i];
	}
	return NULL;
}

int has_option(int id, struct option_val **optval) /* app_longopt_id_t */
{
	int i;
	for(i=0; optval[i]; i++) {
		if(optval[i]->opt->id == id) {
			return 1;
		}
	}
	return 0;
}
const void* get_value_from_option(int id, /* app_longopt_id_t */
                                  struct option_val **optval)
{
	int i;
	for(i=0; optval[i]; i++) {
		if(optval[i]->opt->id == id) {
			if(optval[i]->opt->arg_type == arg_string)
				return (const void*) optval[i]->val;
			else if(optval[i]->opt->arg_type == arg_int)
				return (const void*)(intptr_t) atoi(optval[i]->val);
		}
	}

	/* Option not found: exit.
	 * If the option is mandatory, it presence should have already been
	 * check during the options parsing phase.
	 * If the option is optional, its presence should be checked using
	 * has_option() function before using this function */
	printf("Unexpected error in %s():\nOption (id = '%i') not found on "
			"command line.\n", __func__ , id);
	exit(1);
}

int has_arg(int id, struct arg_val **argval) /* app_arg_id_t */
{
	int i;
	for(i=0; argval[i]; i++) {
		if(argval[i]->arg->id == id) {
			return 1;
		}
	}
	return 0;
}

const void* get_value_from_arg(int id, struct arg_val **argval) /* app_arg_id_t */
{
	int i;
	for(i=0; argval[i]; i++) {
		if(argval[i]->arg->id == id) {
			if(argval[i]->arg->type == arg_string)
				return (const void*) argval[i]->val;
			else if(argval[i]->arg->type == arg_int)
				return (const void*)(intptr_t) atoi(argval[i]->val);
			else if(argval[i]->arg->type == arg_uint)
				return (const void*)(uintptr_t) strtoul(argval[i]->val, NULL, 16);
		}
	}

	/* Argument not found: exit.
	 * If the argument is mandatory, it presence should have already been
	 * check during the arguments parsing phase.
	 * If the argument is optional, its presence should be checked using
	 * has_arg() function before using this function */
	printf("Unexpected error in %s():\nArgument (id = '%i') not found on "
			"command line.\n", __func__ , id);
	exit(1);
}

void available_subcommands()
{
	int i;

	printf("Usage: ");
	printf("mxuvc <subcommands> [options] [arguments]\n");
	printf("Type 'mxuvc help <subcommand>' for help on a specific subcommand\n\n");
	printf("Available subcommands:\n");

	for(i=0; app_subcmds[i].name; i++) {
		printf_bold("  %-22s: ", app_subcmds[i].name);
		printf("%s\n", app_subcmds[i].help);
	}
	for (i=0; app_global_options[i].long_name; i++) {
		if (i == 0)
			printf("\nGlobal options:\n");
		printf("  --%-20s: ", app_global_options[i].long_name);
		printf("%s\n", app_global_options[i].help);
	}
}

void print_usage(struct app_subcmd *cmd)
{
	int i=0;

	if (cmd == NULL) {
		printf("\nType 'app help' for usage\n");
		return;
	}

	printf("Description:\n  %s\n\n", cmd->help);
	printf("Usage:\n  mxuvc %s ", cmd->name);
	for(i=0; i<MAX_ARGS && cmd->args[i]; i++) {
		const struct app_arg *arg;
		arg = get_arg_from_id(cmd->args[i]);
		assert(arg != NULL);
		if (arg->is_optional)
			printf("[%s] ", arg->name);
		else
			printf("<%s> ", arg->name);
	}
	for (i=0; cmd->options[i]; i++) {
		const struct app_option *opt;
		opt = get_option_from_id(cmd->options[i]);
		assert(opt != NULL);
		if (opt->is_optional)
			printf("[");
		printf("--%s", opt->long_name);
		if (opt->has_arg == required_argument)
			printf(" <%s>", opt->arg_str);
		else if(opt->has_arg == optional_argument)
			printf(" [%s]", opt->arg_str);
		if (opt->is_optional)
			printf("]");
		printf(" ");

	}
	printf("\n");

	for(i=0; i<MAX_ARGS && cmd->args[i]; i++) {
		const struct app_arg *arg;
		if (i == 0)
			printf("\nValid arguments:\n");
		arg = get_arg_from_id(cmd->args[i]);
		assert(arg != NULL);
		printf("  %-22s: ", arg->name);
		printf("%s\n", arg->help);

	}
	for (i=0; cmd->options[i]; i++) {
		const struct app_option *opt;
		if (i == 0)
			printf("\nValid options:\n");
		opt = get_option_from_id(cmd->options[i]);
		assert(opt != NULL);
		printf("  --%-20s: ", opt->long_name);
		printf("%s\n", opt->help);
	}
};

int parsecmd(int in_argc, char **in_argv,
             struct app_subcmd **subcmd,
             char **subcmd_name,
             struct option_val **options_val,
             struct arg_val **args_val,
             struct option *long_options)
{
    int ret, i=0, j=0;
    int argc;
    char **argv;

    argc = in_argc;
#if 0 //disabled below code to avoid memory leak as reported in GEOCONDSW-1655
    argv = calloc(1, sizeof(char*)*(argc+1));
    for(i=0;i<argc;i++){
        argv[i] = calloc(1, 768);
        strncpy(argv[i],in_argv[i], 768);
    }
    argv[i] = NULL;
#else
   argv = in_argv;
#endif
    i=0;

    /* Fill getopt structure with global and non global options */
    while(app_global_options[i].long_name) {
        long_options[i].name = app_global_options[i].long_name;
        long_options[i].has_arg = app_global_options[i].has_arg;
        long_options[i].flag = NULL;
        long_options[i].val = app_global_options[i].id;
        i++;
    }
    while(app_options[j].long_name) {
        long_options[i].name = app_options[j].long_name;
        long_options[i].has_arg = app_options[j].has_arg;
        long_options[i].flag = NULL;
        long_options[i].val = app_options[j].id;
        i++;
        j++;
    }
    long_options[i].name = NULL;
    long_options[i].has_arg = 0;
    long_options[i].flag = NULL;
    long_options[i].val = 0;

    /* Parse and process options */
    for(i=0;;) {
        int c, idx=0;
        struct option_val *optval;

        assert(i < MAX_OPTIONS);
        c = getopt_long(argc, argv, "", long_options, &idx);

        /* No more options */
        if (c == -1)
            break;

        switch(c) {
        case '?': /* Invalid option */
            if(c == '?') {
                ret = 1;
                goto main_out;
            }
            break;

        case 0: /* Flag set */
            break;

        default: /* Process option */
            optval = malloc(sizeof(struct option_val));
            optval->opt = get_option_from_id(c);
            assert(optval->opt != NULL);
            optval->val = optarg;
            options_val[i] = optval;
            i++;
        }
    }
    options_val[i] = NULL;

    /* Parse and process SUBCOMMAND */

    /* No SUBCOMMAND specified: display help */
    if(optind >= argc) {
        available_subcommands();
        ret = 1;
        goto main_out;
    }

    /* Retrieve and check SUBCOMMAND */
    *subcmd_name = argv[optind];
    *subcmd = get_subcmd_from_name(*subcmd_name);
    if (*subcmd == NULL) {
        printf("Unknown subcommand '%s'\n", *subcmd_name);
        ret = 1;
        goto main_out;
    }

    /* Check that all required OPTIONS for this SUBCOMMAND were given */
    for(i=0; (*subcmd)->options[i]; i++) {
        const struct app_option *opt;
        int found=0;
        opt = get_option_from_id((*subcmd)->options[i]);
        assert(opt != NULL);

        /* Skip optional options */
        if(opt->is_optional == 1)
            continue;

        /* Browse through the OPTIONS given on the command line to find
         * a match */
        for(j=0; options_val[j]; j++) {
            if(options_val[j]->opt->id != opt->id)
                continue;
            found=1;
            break;
        }

        /* No match */
        if(found==0) {
            printf("Option '--%s' is required for subcommand '%s'\n",
                   opt->long_name, *subcmd_name);
            return -1;
            ret = 1;
            goto main_out;
        }
    }

    /* Parse and process ARGUMENTS given on the command line */
    i=optind+1;
    for(j=0; (*subcmd)->args[j]; j++) { /* FIXME: unstable? */
        const struct app_arg *arg;
        struct arg_val *argval;
        if(i >= argc)
            break;
        argval = malloc(sizeof(struct arg_val));
        arg = get_arg_from_id((*subcmd)->args[j]);
        assert(arg != NULL);
        argval->arg = arg;
        argval->val = argv[i];
        args_val[j] = argval;
        i++;
    }
    args_val[j]=NULL;

    /* Check that no mandatory ARGUMENT are missing */
    for(;(*subcmd)->args[j]; j++) {
        const struct app_arg *arg;
        arg = get_arg_from_id((*subcmd)->args[j]);
        assert(arg != NULL);
        if(arg->is_optional == MANDATORY) {
            printf("Argument <%s> is required for subcommand '%s'\n",
                   arg->name, *subcmd_name);
            ret = 1;
            goto main_out;
        }

    }

    return 0;

main_out:
    return ret;

#if 1
    /* Free ressources */
    for(j=0; args_val[j] != NULL; j++)
        free(args_val[j]);
    for(j=0; options_val[j] != NULL; j++)
        free(options_val[j]);
#if 0 //disabled below code to avoid memory leak as reported in GEOCONDSW-1655
    if (argv != NULL) {
        for (i = 0; i < argc; i++) {
            if (argv[i] != NULL)
                free(argv[i]);
        }
        free(argv);
    }
#endif

    return ret;
#endif
}
