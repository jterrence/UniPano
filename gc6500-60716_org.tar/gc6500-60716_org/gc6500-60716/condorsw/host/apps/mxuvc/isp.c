/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include <stdio.h>
#include <unistd.h> /* sleep() */

#include "parsecmd.h"
#include "main.h"
#include "mxuvc.h"

extern int dw_panel;
extern video_channel_t ch;

IMPL_SUBCMD_GETSETINT(brightness, "brightness", int16_t, arg_brightness,
                      mxuvc_video_get_brightness, mxuvc_video_set_brightness);

IMPL_SUBCMD_GETSETINT(contrast, "contrast", uint16_t, arg_contrast,
                      mxuvc_video_get_contrast, mxuvc_video_set_contrast);

IMPL_SUBCMD_GETSETINT(sharpness, "sharpness", uint16_t, arg_sharpness,
                      mxuvc_video_get_sharpness, mxuvc_video_set_sharpness);

IMPL_SUBCMD_GETSETINT(saturation, "saturation", uint16_t, arg_saturation,
                      mxuvc_video_get_saturation, mxuvc_video_set_saturation);

IMPL_SUBCMD_GETSETINT(hue, "hue", int16_t, arg_hue,
                      mxuvc_video_get_hue, mxuvc_video_set_hue);

IMPL_SUBCMD_GETSETINT(gamma, "gamma", uint16_t, arg_gamma,
                      mxuvc_video_get_gamma, mxuvc_video_set_gamma);

IMPL_SUBCMD_GETSETINT(histogrameq, "histogrameq", histo_eq_t, arg_histogram_eq,
                      mxuvc_video_get_histogram_eq, mxuvc_video_set_histogram_eq);

IMPL_SUBCMD_GETSETINT(maxanaloggain, "maxanaloggain", uint32_t, arg_max_analog_gain,
                      mxuvc_video_get_max_analog_gain, mxuvc_video_set_max_analog_gain);

IMPL_SUBCMD_GETSETINT(sharpenfilter, "sharpenfilter", uint32_t, arg_sharpen_filter,
                      mxuvc_video_get_sharpen_filter, mxuvc_video_set_sharpen_filter);

IMPL_SUBCMD_GETSETINT(gainmultiplier, "gainmultiplier", uint32_t, arg_gain_multiplier,
                      mxuvc_video_get_gain_multiplier, mxuvc_video_set_gain_multiplier);

IMPL_SUBCMD_GETSETINT(vflip, "vflip", video_flip_t, arg_vflip,
                      mxuvc_video_get_flip_vertical,  mxuvc_video_set_flip_vertical);

IMPL_SUBCMD_GETSETINT(hflip, "hflip", video_flip_t, arg_hflip,
                      mxuvc_video_get_flip_horizontal,  mxuvc_video_set_flip_horizontal);


IMPL_SUBCMD_GETSETINT(saturationmode, "saturationmode", saturation_mode_t, arg_saturation_mode,
                      mxuvc_video_get_saturation_mode,  mxuvc_video_set_saturation_mode);

IMPL_SUBCMD_GETSETINT(brightnessmode, "brightnessmode", brightness_mode_t, arg_brightness_mode,
                      mxuvc_video_get_brightness_mode,  mxuvc_video_set_brightness_mode);

IMPL_SUBCMD_GETSETINT(contrastmode, "contrastmode", contrast_mode_t, arg_contrast_mode,
                      mxuvc_video_get_contrast_mode,  mxuvc_video_set_contrast_mode);

/* WB */
int app_subcmd_wb(struct app_subcmd *cmd,
                  struct option_val **optval,
                  struct arg_val **argval)
{
        white_balance_mode_t wb_info;
        uint16_t value;
        if(has_arg(arg_wb_mode,argval)){
                wb_info = (intptr_t) get_value_from_arg(arg_wb_mode, argval);
        }
        else {
                mxuvc_video_get_wb(ch, &wb_info, &value);
                printf("wb mode (%d) = ", wb_info);
                switch(wb_info) {
                case WB_MODE_MANUAL:
                        printf("MANUAL , value = %i\n", value);
                        break;
                case WB_MODE_AUTO:
                        printf("AUTO   \n");
                        break;
                default:
                        printf("????   \n");
                        break;
                }
                return 0;
        }
        if(has_arg(arg_wb_strength,argval)){
                value = (intptr_t) get_value_from_arg(arg_wb_strength, argval);
        }
        else {
                //printf("set : no strength\n");
        }
        printf("Setting WB to {%i,%i} on channel %i\n",wb_info,value,ch);
        mxuvc_video_set_wb(ch,wb_info,value);
        return 0;
}

/* WDR */
int app_subcmd_wdr(struct app_subcmd *cmd,
                   struct option_val **optval,
                   struct arg_val **argval)
{
	wdr_mode_t wdr_info;
	uint8_t value;
	if(has_arg(arg_wdr_mode,argval)){
		wdr_info = (intptr_t) get_value_from_arg(arg_wdr_mode, argval);
	}
	else {
		mxuvc_video_get_wdr(ch, &wdr_info, &value);
		printf("wdr mode (%d) = ", wdr_info);
		switch( wdr_info) {
		case WDR_DISABLE: 
			printf("DISABLE\n");
			break;
		case WDR_MANUAL:
			printf("MANUAL , value = %i\n", value);
			break;
		case WDR_AUTO:
			printf("AUTO   \n");
			break;
		default:
			printf("????   \n");
			break;
		}
		return 0;
	}
	if(has_arg(arg_wdr_strength,argval)){
		value = (intptr_t) get_value_from_arg(arg_wdr_strength, argval);
	}
	else {
		//printf("set : no strength\n");
	}
	printf("Setting WDR to {%i,%i} on channel %i\n",wdr_info,value,ch);
	mxuvc_video_set_wdr(ch,wdr_info,value);
	return 0;
}

/* SINTER */
int app_subcmd_sinter(struct app_subcmd *cmd,
                      struct option_val **optval,
                      struct arg_val **argval)
{
    sinter_info_t    sinter_info;
    if(has_arg(arg_sinter_mode,argval)){
        sinter_info.mode =
            (intptr_t) get_value_from_arg(arg_sinter_mode, argval);
    }
    else {
        mxuvc_video_get_sinter(ch,&sinter_info);
        printf("Ch(%d) Sinter mode = %i\n", ch, sinter_info.mode);
        printf("              NR       : min = %i, max = %i\n", sinter_info.minNR, sinter_info.maxNR);
        printf("              Threshold: min = %i, max = %i\n", sinter_info.minThresh, sinter_info.maxThresh);
        return 0;
    }
    if(has_arg(arg_sinter_min_nr,argval)){
        sinter_info.minNR =
            (intptr_t) get_value_from_arg(arg_sinter_min_nr, argval);
    }
    if(has_arg(arg_sinter_max_nr,argval)){
        sinter_info.maxNR =
            (intptr_t) get_value_from_arg(arg_sinter_max_nr, argval);
    }
    if(has_arg(arg_sinter_min_thr,argval)){
        sinter_info.minThresh =
            (intptr_t) get_value_from_arg(arg_sinter_min_thr, argval);
    }
    if(has_arg(arg_sinter_max_thr,argval)){
        sinter_info.maxThresh =
            (intptr_t) get_value_from_arg(arg_sinter_max_thr, argval);
    }
    if(has_arg(arg_sinter_trig_pt,argval)){
        sinter_info.triggerPt =
            (intptr_t) get_value_from_arg(arg_sinter_trig_pt, argval);
    }
    printf("Setting Sinter to {%i,%i,%i,%i,%i,%i} on channel %i\n",
           sinter_info.mode, sinter_info.minNR, sinter_info.maxNR,
           sinter_info.minThresh, sinter_info.maxThresh,
           sinter_info.triggerPt,ch);

    mxuvc_video_set_sinter(ch,&sinter_info);

    return 0;
}
