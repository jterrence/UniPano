/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include <stdio.h>
#include <time.h>
#include "parsecmd.h"
#include "main.h"
#include "mxuvc.h"

/* Codec set param */
int app_subcmd_ispcfg(struct app_subcmd *cmd,
                           struct option_val **optval,
                           struct arg_val **argval)
{
    int ret = 0;
    char *ispcfgfile = (const char*) get_value_from_arg(arg_ispcfg_file, argval);

    ret = mxuvc_ispcfg_init();

    if(ret != -1)
        ret = mxuvc_ispcfg_load_file(ispcfgfile);

    return ret;
}
