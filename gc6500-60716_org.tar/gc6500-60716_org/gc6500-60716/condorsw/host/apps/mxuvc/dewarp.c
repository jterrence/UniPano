/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include <stdio.h>
#include <unistd.h> /* sleep() */
#include <limits.h>

#include "parsecmd.h"
#include "main.h"
#include "mxuvc.h"

extern int dw_panel;
extern video_channel_t ch;

#define DPRINT printf
#define EPRINT printf

/* DEWARP INFO */
int app_subcmd_dwmode(struct app_subcmd *cmd,
                      struct option_val **optval,
                      struct arg_val **argval)
{
    int panel = dw_panel;
    dewarp_params_t dwParams;
    dewarp_params_t *p = &dwParams;
    dewarp_mode_t dwMode, dwModeSet = EMODE_MAX;

    if (has_arg(arg_dwmode, argval)) {
        dwModeSet = (intptr_t) get_value_from_arg(arg_dwmode, argval);
	if (dwModeSet >= EMODE_MAX) {
	    EPRINT("Error: try setting unsupported EMODE = %d\n", dwModeSet);
	    return 0;
	}
    }

	memset(&dwParams, 0, sizeof(dwParams));

    switch (dwModeSet) {
    case EMODE_OFF:
    case EMODE_TM_1CircPanoViewPan:
        p->eptz_mode_tm_1circpanoviewpan.ArchAngleCenter = 20;
	p->eptz_mode_tm_1circpanoviewpan.ArchAngle = 60;
        break;
    case EMODE_CM_CircPanoViewPan:
        // Radius0Mili;        // r0, range is [0..Radius1Mili), unit is in milis (i.e. it will be divided by 1000 by the codec)
        // Radius1Mili;        // r1, range is (Radius0Mili..1], unit is in milis (i.e. it will be divided by 1000 by the codec)
        // Phi1;               // Pano size, range is [0...360) degrees
        // PhiShift1;          // Pano shift, range is [0...360) degrees
	p->eptz_mode_cm_circpanoviewpan.Radius0Mili = 500;
	p->eptz_mode_cm_circpanoviewpan.Radius1Mili = 700;
	p->eptz_mode_cm_circpanoviewpan.Phi1        = 45;
	p->eptz_mode_cm_circpanoviewpan.PhiShift1   = 0;
	break;

    case EMODE_WM_ZCL:
        p->eptz_mode_wm_zcl.Phi0   = 55;
        p->eptz_mode_wm_zcl.Rx     = 2;
        p->eptz_mode_wm_zcl.Ry     = 1;
        p->eptz_mode_wm_zcl.Rz     = 1;
        p->eptz_mode_wm_zcl.Gshift = 0;
        break;
    case EMODE_WM_ZCLCylinder:
        p->eptz_mode_wm_zclcylinder.CylinderHeightMicro = (int)(0.95*1000000);
        break;
    case EMODE_WM_ZCLStretch:
        p->eptz_mode_wm_zclstretch.HPan = 0;
        p->eptz_mode_wm_zclstretch.VPan = 0;
        p->eptz_mode_wm_zclstretch.Zoom = 1;
        break;
    case EMODE_WM_1PanelEPTZ:
        p->eptz_mode_wm_1paneleptz.HPan = -50;
        p->eptz_mode_wm_1paneleptz.VPan = 0;
        p->eptz_mode_wm_1paneleptz.Zoom = 40;
        p->eptz_mode_wm_1paneleptz.Tilt = 0;
        break;
    case EMODE_WM_Sweep_1PanelEPTZ:
        p->eptz_mode_sweep_wm_1panelptz.HPanStart = -10;
        p->eptz_mode_sweep_wm_1panelptz.VPanStart = 0;
        p->eptz_mode_sweep_wm_1panelptz.ZoomStart = 40;
        p->eptz_mode_sweep_wm_1panelptz.TiltStart = 0;
        p->eptz_mode_sweep_wm_1panelptz.HPanInc = 1;
        p->eptz_mode_sweep_wm_1panelptz.TiltInc = 1;
        p->eptz_mode_sweep_wm_1panelptz.VPanInc = 1;
        p->eptz_mode_sweep_wm_1panelptz.ZoomInc = 1;
        p->eptz_mode_sweep_wm_1panelptz.Period  = 40;
        break;
    case EMODE_WM_Magnify:
	p->eptz_mode_magnify.zoom = 1;
        p->eptz_mode_magnify.radius = 100;
        p->eptz_mode_magnify.xCenter = 100;
        p->eptz_mode_magnify.yCenter = 100;
        break;
    case EMODE_WM_ROI_1PanelEPTZ:
        p->eptz_mode_wm_roi_1paneleptz.XCenter = 1920/2;
        p->eptz_mode_wm_roi_1paneleptz.YCenter = 1080/2;
        p->eptz_mode_wm_roi_1paneleptz.HeightFromCenter = 200;
        p->eptz_mode_wm_roi_1paneleptz.SourceChannel = 0;
        p->eptz_mode_wm_roi_1paneleptz.SourcePanel = 0;
        break;
    case EMODE_MAX:
        break;
    default:
        EPRINT("Error: Invalid dewarp mode set\n");
        break;
    }

    if (dwModeSet < EMODE_MAX) {
        // Set dewarp mode
        mxuvc_video_set_dewarp_params((video_channel_t) ch, panel,
                                      dwModeSet, &dwParams);
        //DPRINT("Set DeWarp Mode to %d @ panel = %d\n", dwModeSet, panel);
        //sleep(1);
        return 0;
    }

    DPRINT("<---- CH %d; PANEL %d ---->\n", ch, panel);
    int ret = mxuvc_video_get_dewarp_params(ch, panel, &dwMode,
                                             (dewarp_params_t *)&dwParams);
    if(ret < 0)
        return -1;

    switch (dwMode) {
    case EMODE_OFF:
        DPRINT("EM: OFF\n");
        break;
    case EMODE_TM_1CircPanoViewPan:
        DPRINT("EM: TM Circle PanoView\n");
	DPRINT("    %4d\n", p->eptz_mode_tm_1circpanoviewpan.ArchAngleCenter);
	DPRINT("    %4d\n", p->eptz_mode_tm_1circpanoviewpan.ArchAngle);
	DPRINT("    Divisor=%4d\n", p->eptz_mode_tm_1circpanoviewpan.Divisor);
	break;
    case EMODE_CM_CircPanoViewPan:
	DPRINT("EM: CM Circle PanoView\n");
	DPRINT("    r0        =%4d\n", p->eptz_mode_cm_circpanoviewpan.Radius0Mili);
	DPRINT("    r1        =%4d\n", p->eptz_mode_cm_circpanoviewpan.Radius1Mili);
	DPRINT("    phi1      =%4d\n", p->eptz_mode_cm_circpanoviewpan.Phi1);
	DPRINT("    phishift1 =%4d\n", p->eptz_mode_cm_circpanoviewpan.PhiShift1);
	DPRINT("    Divisor   =%4d\n", p->eptz_mode_cm_circpanoviewpan.Divisor);
	break;
    case EMODE_WM_ZCL:
        DPRINT("EM: ZCL (%4d)\n", dwMode);
	DPRINT("    Phi      =%4d\n", p->eptz_mode_wm_zcl.Phi0);
	DPRINT("    Rx       =%4d\n", p->eptz_mode_wm_zcl.Rx);
	DPRINT("    Ry       =%4d\n", p->eptz_mode_wm_zcl.Ry);
	DPRINT("    Rz       =%4d\n", p->eptz_mode_wm_zcl.Rz);
	DPRINT("    GShift   =%4d\n", p->eptz_mode_wm_zcl.Gshift);
	DPRINT("    Divisor  =%4d\n", p->eptz_mode_wm_zcl.Divisor);
	break;
    case EMODE_WM_ZCLCylinder:
        DPRINT("EM: ZCLCylinder(%4d)\n", dwMode);
	DPRINT("    CylinderHeight=%4d micro\n",
               p->eptz_mode_wm_zclcylinder.CylinderHeightMicro);
	break;
    case EMODE_WM_ZCLStretch:
        DPRINT("EM: ZCLStretch (%4d)\n", dwMode);
        DPRINT("    HPan     =%4d\n", p->eptz_mode_wm_zclstretch.HPan);
        DPRINT("    VPan     =%4d\n", p->eptz_mode_wm_zclstretch.VPan);
        DPRINT("    Zoom     =%4d\n", p->eptz_mode_wm_zclstretch.Zoom);
        DPRINT("    Divisor  =%4d\n", p->eptz_mode_wm_zclstretch.Divisor);
	break;
    case EMODE_WM_1PanelEPTZ:
        DPRINT("EM: 1PanelEPTZ (%4d)\n", dwMode);
        DPRINT("    HPan     =%4d\n", p->eptz_mode_wm_1paneleptz.HPan);
        DPRINT("    VPan     =%4d\n", p->eptz_mode_wm_1paneleptz.VPan);
        DPRINT("    Tilt     =%4d\n", p->eptz_mode_wm_1paneleptz.Tilt);
        DPRINT("    Zoom     =%4d\n", p->eptz_mode_wm_1paneleptz.Zoom);
        DPRINT("    Divisor  =%4d\n", p->eptz_mode_wm_1paneleptz.Divisor);
	break;
    case EMODE_WM_Sweep_1PanelEPTZ:
        DPRINT("EM: Sweep_1PanelEPTZ (%4d)\n", dwMode);
        // header file typo! wm_1panelptz ?? instead of wm_1paneleptz ??
	DPRINT("    HPanStart=%4d\n", p->eptz_mode_sweep_wm_1panelptz.HPanStart);
	DPRINT("    VPanStart=%4d\n", p->eptz_mode_sweep_wm_1panelptz.VPanStart);
	DPRINT("    ZoomStart=%4d\n", p->eptz_mode_sweep_wm_1panelptz.ZoomStart);
	DPRINT("    TiltStart=%4d\n", p->eptz_mode_sweep_wm_1panelptz.TiltStart);
	DPRINT("    HPanInc  =%4d\n", p->eptz_mode_sweep_wm_1panelptz.HPanInc);
	DPRINT("    VPanInc  =%4d\n", p->eptz_mode_sweep_wm_1panelptz.VPanInc);
	DPRINT("    TiltInc  =%4d\n", p->eptz_mode_sweep_wm_1panelptz.TiltInc);
	DPRINT("    ZoomInc  =%4d\n", p->eptz_mode_sweep_wm_1panelptz.ZoomInc);
	DPRINT("    Period   =%4d\n", p->eptz_mode_sweep_wm_1panelptz.Period);
	DPRINT("    Divisor  =%4d\n", p->eptz_mode_sweep_wm_1panelptz.Divisor);
	break;
    case EMODE_WM_Magnify:
        DPRINT("EM: Magnify (%4d)\n", dwMode);
	DPRINT("    zoom     =%4d\n", p->eptz_mode_magnify.zoom);
	DPRINT("    radius   =%4d\n", p->eptz_mode_magnify.radius);
	DPRINT("    xCenter  =%4d\n", p->eptz_mode_magnify.xCenter);
	DPRINT("    yCenter  =%4d\n", p->eptz_mode_magnify.yCenter);
	DPRINT("    Divisor  =%4d\n", p->eptz_mode_magnify.Divisor);
	break;
    case EMODE_WM_ROI_1PanelEPTZ:
        DPRINT("EM: ROI_1PanelEPTZ (%4d)\n", dwMode);
        DPRINT("    XCenter          =%4d\n", p->eptz_mode_wm_roi_1paneleptz.XCenter);
        DPRINT("    Ycenter          =%4d\n", p->eptz_mode_wm_roi_1paneleptz.YCenter);
        DPRINT("    HeightFromCenter =%4d\n", p->eptz_mode_wm_roi_1paneleptz.HeightFromCenter);
        DPRINT("    Source Channel =%4d\n", p->eptz_mode_wm_roi_1paneleptz.SourceChannel);
        DPRINT("    Source Panel   =%4d\n", p->eptz_mode_wm_roi_1paneleptz.SourcePanel);
	break;
    default:
        DPRINT("EM: %d\n", dwMode);
	break;
    }

   return 0;
}

/* DEWARP EPTZ */
int app_subcmd_dweptz(struct app_subcmd *cmd,
                      struct option_val **optval,
                      struct arg_val **argval)
{
    dewarp_params_t dw_params;
    dewarp_mode_t  dw_mode = EMODE_WM_1PanelEPTZ;
    STRUCT_Q_EPTZ_MODE_WM_1PANELEPTZ *eptz = &dw_params.eptz_mode_wm_1paneleptz;
    memset(eptz, 0, sizeof(STRUCT_Q_EPTZ_MODE_WM_1PANELEPTZ));

    eptz->HPan = (intptr_t) get_value_from_arg(arg_dweptz_hpan, argval);
    eptz->VPan = (intptr_t) get_value_from_arg(arg_dweptz_vpan, argval);
    eptz->Tilt = (intptr_t) get_value_from_arg(arg_dweptz_tilt, argval);
    eptz->Zoom = (intptr_t) get_value_from_arg(arg_dweptz_zoom, argval);
    eptz->Divisor = (intptr_t) get_value_from_arg(arg_dweptz_Divisor, argval);
    if(has_arg(arg_dweptz_xcenter, argval) && 
               has_arg(arg_dweptz_ycenter, argval) && 
               has_arg(arg_dweptz_heightfromcenter, argval))
    {
        eptz->XCenter = (intptr_t) get_value_from_arg(arg_dweptz_xcenter, argval);
        eptz->YCenter = (intptr_t) get_value_from_arg(arg_dweptz_ycenter, argval);
        eptz->HeightFromCenter = (intptr_t) get_value_from_arg(arg_dweptz_heightfromcenter, argval);
    }
    if ( (eptz->Zoom <= 0) || ((eptz->Zoom % 180) == 0) ) {
        EPRINT("Warning: Zoom angle should not be 0 or 180 degrees.\n");
    }

    return mxuvc_video_set_dewarp_params(ch, dw_panel, dw_mode, &dw_params);
}

/* DEWARP ZCL */
int app_subcmd_dwzcl(struct app_subcmd *cmd,
                     struct option_val **optval,
                     struct arg_val **argval)
{
    dewarp_params_t dw_params;
    dewarp_mode_t dw_mode = EMODE_WM_ZCL;
    STRUCT_Q_EPTZ_MODE_WM_ZCL *zcl = &dw_params.eptz_mode_wm_zcl;

    // Good parameters to use:
    //   ./mxuvc dwzcl 50 20 10 20 30 --ch 2
    zcl->Phi0   = (intptr_t) get_value_from_arg(arg_dwzcl_phi0, argval);
    zcl->Rx     = (intptr_t) get_value_from_arg(arg_dwzcl_rx, argval);
    zcl->Ry     = (intptr_t) get_value_from_arg(arg_dwzcl_ry, argval);
    zcl->Rz     = (intptr_t) get_value_from_arg(arg_dwzcl_rz, argval);
    zcl->Gshift = (intptr_t) get_value_from_arg(arg_dwzcl_gshift, argval);
    zcl->Divisor = (intptr_t) get_value_from_arg(arg_dwzcl_Divisor, argval);

    return mxuvc_video_set_dewarp_params(ch, dw_panel, dw_mode, &dw_params);
}

/* DEWARP ZCL CYLINDER*/
int app_subcmd_dwzclc(struct app_subcmd *cmd,
                      struct option_val **optval,
                      struct arg_val **argval)
{
    dewarp_params_t dw_params;
    dewarp_mode_t dw_mode = EMODE_WM_ZCLCylinder;
    STRUCT_Q_EPTZ_MODE_WM_ZCLCYLINDER *zclc = &dw_params.eptz_mode_wm_zclcylinder;

    zclc->CylinderHeightMicro =
        (intptr_t) get_value_from_arg(arg_dwzclc_height, argval);

    return mxuvc_video_set_dewarp_params(ch, dw_panel, dw_mode, &dw_params);
}

/* DEWARP ZCL STRETCH */
int app_subcmd_dwzcls(struct app_subcmd *cmd,
                      struct option_val **optval,
                      struct arg_val **argval)
{
    dewarp_params_t dw_params;
    dewarp_mode_t dw_mode = EMODE_WM_ZCLStretch;
    STRUCT_Q_EPTZ_MODE_WM_ZCLSTRETCH *zcls = &dw_params.eptz_mode_wm_zclstretch;

    zcls->HPan = (intptr_t) get_value_from_arg(arg_dwzcls_hpan, argval);
    zcls->VPan = (intptr_t) get_value_from_arg(arg_dwzcls_vpan, argval);
    zcls->Zoom = (intptr_t) get_value_from_arg(arg_dwzcls_zoom, argval);
    zcls->Divisor = (intptr_t) get_value_from_arg(arg_dwzcls_Divisor, argval);

    return mxuvc_video_set_dewarp_params(ch, dw_panel, dw_mode, &dw_params);
}

/* DEWARP TM CIRC 1PANO VIEW */
int app_subcmd_dwtm1pano(struct app_subcmd *cmd,
                       struct option_val **optval,
                       struct arg_val **argval)
{
    dewarp_params_t dw_params;
    dewarp_mode_t dw_mode = EMODE_TM_1CircPanoViewPan;
    STRUCT_Q_EPTZ_MODE_TM_1CIRCPANOVIEWPAN *pano =
        &dw_params.eptz_mode_tm_1circpanoviewpan;

    pano->ArchAngleCenter =
        (intptr_t) get_value_from_arg(arg_dwtm1pano_anglec, argval);
    pano->ArchAngle =
        (intptr_t) get_value_from_arg(arg_dwtm1pano_angle,  argval);
    pano->Divisor =
        (intptr_t) get_value_from_arg(arg_dwtm1pano_Divisor,  argval);

    return mxuvc_video_set_dewarp_params(ch, dw_panel, dw_mode, &dw_params);
}

/* DEWARP CM CIRC 1PANO VIEW */
int app_subcmd_dwcm1pano(struct app_subcmd *cmd,
                       struct option_val **optval,
                       struct arg_val **argval)
{
    dewarp_params_t dw_params;
    dewarp_mode_t dw_mode = EMODE_CM_CircPanoViewPan;
    STRUCT_Q_EPTZ_MODE_CM_CIRCPANOVIEWPAN *pano =
        &dw_params.eptz_mode_cm_circpanoviewpan;

    pano->Radius0Mili =
        (intptr_t) get_value_from_arg(arg_dwcm1pano_r0mili, argval);
    pano->Radius1Mili =
        (intptr_t) get_value_from_arg(arg_dwcm1pano_r1mili,  argval);
    pano->Phi1 = 
        (intptr_t) get_value_from_arg(arg_dwcm1pano_phi1, argval);
    pano->PhiShift1 = 
        (intptr_t) get_value_from_arg(arg_dwcm1pano_phishift1, argval);
    pano->Divisor = 
        (intptr_t) get_value_from_arg(arg_dwcm1pano_Divisor, argval);

    return mxuvc_video_set_dewarp_params(ch, dw_panel, dw_mode, &dw_params);
}

/* DEWARP ROI EPTZ */
int app_subcmd_dwroieptz(struct app_subcmd *cmd,
                      struct option_val **optval,
                      struct arg_val **argval)
{
    dewarp_params_t dw_params;
    dewarp_mode_t  dw_mode = EMODE_WM_ROI_1PanelEPTZ;
    STRUCT_Q_EPTZ_MODE_WM_ROI_1PANELEPTZ *eptz = &dw_params.eptz_mode_wm_roi_1paneleptz;

    eptz->XCenter = (intptr_t) get_value_from_arg(arg_dwroieptz_xcenter, argval);
    eptz->YCenter = (intptr_t) get_value_from_arg(arg_dwroieptz_ycenter, argval);
    eptz->HeightFromCenter = (intptr_t) get_value_from_arg(arg_dwroieptz_heightfromcenter, argval);
    eptz->SourceChannel = (intptr_t) get_value_from_arg(arg_dwroieptz_sourcechannel, argval);
    eptz->SourcePanel = (intptr_t) get_value_from_arg(arg_dwroieptz_sourcepanel, argval);

    return mxuvc_video_set_dewarp_params(ch, dw_panel, dw_mode, &dw_params);
}

