/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include <stdio.h> /* printf() */
#include <stdlib.h> /* free() */
#include <assert.h> /* assert() */
#include <signal.h> /* signal() */
#include <unistd.h> /* sleep() */
#include <fcntl.h> /* open() */
#include <pthread.h> /* mutex */
#include <errno.h> /* errno */
#include <sys/time.h> /* gettimeofday() */
#include <sys/stat.h> /* fstat() */
#include <sys/types.h> /* fstat() */

#include "parsecmd.h"
#include "main.h"
#include "mxuvc.h"
#include "stats.h"

#define MAX_VID_CHANNELS 6
#define MAX_AUD_CHANNELS 2

typedef enum {
    CHANNEL_STOPPED,
    CHANNEL_STOPPING,
    CHANNEL_STARTED,
    CHANNEL_STARTING
} channel_state_t;

typedef enum {
    VIDEO_CHANNEL,
    AUDIO_CHANNEL
} channel_type_t;

struct channel_info {
    int ch;
    channel_type_t type;
    channel_state_t state;
    int format;

    struct {
        int fd;
        const char *path;
    } out;

    struct {
        char *data;
        unsigned int offset;
        unsigned int size;
        unsigned int maxsize;
    } buffer;

    stats_ringbuffer_t stats_ringbuffer;
};

static pthread_mutex_t data_mutex = PTHREAD_MUTEX_INITIALIZER;
static int callback_event_fd[2];
static struct timeval tvStartStrming;
int gStrmingTotalTime = 0;
static int sig_close = 0;

/**************************
 *    Helper functions    *
 **************************/
static void wakeup_mainloop()
{
    int wakeup=1;
    write(callback_event_fd[1], &wakeup, sizeof(int));
}

/* Increase the size of the buffer is new_size > size */
static inline int adapt_buffer(char **buffer, unsigned int *size,
                               unsigned int new_size)
{
    /* If the new size is less than the current size,
       there is nothing to do */
    if(new_size <= *size)
        return 0;

    /* The new size is bigger than the current size */
    /* Free the current buffer */
    if(*buffer) free(*buffer);
    /* Allocate a new one */
    *buffer = malloc(new_size);
    if(*buffer== NULL) {
        fprintf(stderr, "Out of memory: could not "
                "allocate %i bytes\n", new_size);
        return -1;
    }
    *size = new_size;

    return 0;
}


/******************************
 *   Handle mxuvc callback    *
 ******************************/
static void process_cb_data(struct channel_info *ch_info, unsigned char *buffer,
                        unsigned int size)
{
    int retval;
    unsigned int bytes_written;

    if(ch_info->state == CHANNEL_STOPPING) {
        //printf("Channel stopping: skip\n");
        return;
    }

    /* Update the buffer only if it has been completely written out */
    if(ch_info->buffer.offset != ch_info->buffer.size) {
        fprintf(stderr, "Dropping %s frame on channel %i\n",
                ch_info->type == VIDEO_CHANNEL ? "a video frame"
                                               : "an audio frame",
                ch_info->ch+1);
        return;
    }

    /* Attempt a non-blocking write here.
       If we can write everything out in this write call,
       we save one memcpy(). */
    retval = write(ch_info->out.fd, buffer, size);

    /* A pipe error occured during the write.
       It is expected when the output file is a pipe and the pipe reader
       closes its end.
       We just mark the channel as to be stopped and wake up the mainloop
       so it can handle it */
    if(retval < 0 && errno == EPIPE) {
        ch_info->state = CHANNEL_STOPPING;
        wakeup_mainloop();
        return;
    }

    if(retval < 0) {
        bytes_written = 0;
        perror("");
    } else
        bytes_written = retval;


    /* The write() did not write all the data out.
       We memcpy() it and notify the mainloop so it can handle
       the left over data. */
    if(bytes_written < size) {
        /* Increase the size of the buffer if needed */
        int ret = adapt_buffer(&(ch_info->buffer.data),
                               &(ch_info->buffer.size),
                               size - bytes_written);
        if(ret < 0)
            return;

        ch_info->buffer.offset = 0;
        ch_info->buffer.size = size - bytes_written;

        //printf("retval (%i) vs size(%i)\n", retval, size);
        memcpy(ch_info->buffer.data, buffer + bytes_written,
               size - bytes_written);

        /* Wake up the main loop */
        wakeup_mainloop();
    }
}

static int isKeyFrame(unsigned char* buffer, int length) {
    long data, type;
    long status = 0;
    int offset = 0;

    while (length > 0) {
        data = buffer[offset];
        offset++;
        length--;

        switch (status) {
        case 0:
        case 1:
        case 2:
            if (data == 0x00) { // NAL Header first 3 byte = "0x00 00 00"
                status++;
            } else {
                status = 0;
            }
            break;

        case 3:
            if (data == 0x01) { // NAL Header 4th byte = '0x01'
                status++;
            } else {
                status = 0;
            }
            break;

        case 4: // 1st byte of NAL Header
            type = data & 0x1F;

            if (type == 1) { //check if this is non-IDR frame
                return 0;
            } else if (type == 5) { //Check if this is IDR frame
                return 1;
            } else {
                if (type == 20 || type == 14) {
                    status++; // To Next byte
                } else {
                    status = 0;
                }
            }
            break;

        default:
            return 0;
        } // end of switch
    } //end of while

    return 0;
}

/* Mxuvc video callback */
static void video_cb(unsigned char *buffer, unsigned int size,
                  video_info_t info, void *user_data)
{
    struct channel_info *vch_info = (struct channel_info*) user_data;
    int bIsKeyFrm = 0;
    assert(vch_info != NULL);

    pthread_mutex_lock(&data_mutex);
    if (vch_info->format == 0) {
        bIsKeyFrm = isKeyFrame(buffer, size);
    }
    video_stats_update(&(vch_info->stats_ringbuffer), size, bIsKeyFrm);
    process_cb_data(vch_info, buffer, size);
    pthread_mutex_unlock(&data_mutex);

    mxuvc_video_cb_buf_done(vch_info->ch, info.buf_index);
}


#ifdef AUDIO_BACKEND
static void audio_cb(unsigned char *buffer, unsigned int size,
        int format, uint64_t ts, void *user_data, audio_params_t *param)
{
    struct channel_info *ach_info = (struct channel_info*) user_data;
    assert(ach_info != NULL);

    pthread_mutex_lock(&data_mutex);
    process_cb_data(ach_info, buffer, size);
    pthread_mutex_unlock(&data_mutex);
}
#endif


/********************************************
 *    Open output file in the background    *
 ********************************************/
static void *open_out_thread(void *data)
{
    struct channel_info *ch_info = (struct channel_info*) data;
    mode_t mask;

    /* Attempt opening and block on it if necessary. */
    ch_info->out.fd = open(ch_info->out.path, O_CREAT|O_WRONLY|O_TRUNC, 0666);

    /* Open error */
    if(ch_info->out.fd == -1) {
        fprintf(stderr, "Unable to open %s for writing.\n", ch_info->out.path);
        perror("");
        return NULL;
    }

    /* Make it non blocking */
    int flags = fcntl(ch_info->out.fd, F_GETFL);
    fcntl(ch_info->out.fd, F_SETFL, flags | O_NONBLOCK);

    /* Wake up the mainloop so that is can start the channel
       associated with the file */
    ch_info->state = CHANNEL_STARTING;
    wakeup_mainloop();

    return NULL;
}

static int open_out(struct channel_info *ch_info)
{
    pthread_t open_thread;
    pthread_attr_t thread_att;
    pthread_attr_init(&thread_att);
    pthread_attr_setdetachstate(&thread_att, PTHREAD_CREATE_DETACHED);

    if(pthread_create(&open_thread, &thread_att, &open_out_thread,
                      (void*) ch_info)) {
        fprintf(stderr, "Error creating thread\n");
        return -1;
    }

    pthread_attr_destroy(&thread_att);

    return 0;
}


/***************************************************
 *    Handling of the channels in the main loop    *
 ***************************************************/
static inline void channel_start(struct channel_info *ch_info,
                                 fd_set *wfds, int *largest_fd)
{
    if(ch_info->out.fd > *largest_fd)
        *largest_fd = ch_info->out.fd;

    ch_info->state = CHANNEL_STARTED;
    ch_info->buffer.size = 0;
    ch_info->buffer.offset = 0;

    if(ch_info->type == VIDEO_CHANNEL) {
        fprintf(stderr, "Starting video channel %d\n",
                ch_info->ch+1);
        mxuvc_video_start(ch_info->ch);
    }
    #ifdef AUDIO_BACKEND
    else {
        fprintf(stderr, "Starting audio channel %d\n",
                ch_info->ch+1);
        mxuvc_audio_start(ch_info->ch);
    }
    #endif
}

static inline void channel_stop(struct channel_info *ch_info,
                                          fd_set *wfds)
{
    struct stat sb;
    int watch = 0;

    if(ch_info->type == VIDEO_CHANNEL) {
        fprintf(stderr, "Stopping video channel %d\n",
                ch_info->ch+1);
        mxuvc_video_stop(ch_info->ch);
    }
    #ifdef AUDIO_BACKEND
    else {
        fprintf(stderr, "Stopping audio channel %d\n",
                ch_info->ch+1);
        mxuvc_audio_stop(ch_info->ch);
    }
    #endif
    ch_info->state = CHANNEL_STOPPED;

    if(fstat(ch_info->out.fd, &sb) == -1) {
        perror("stat");
    }
    if(S_ISFIFO(sb.st_mode)) 
	watch = 1;

    close(ch_info->out.fd);

    /* Remove it from the ready-to-write watching list */
    FD_CLR(ch_info->out.fd, wfds);

    /* Reset the video statistics */
    if(ch_info->type == VIDEO_CHANNEL)
        memset(&(ch_info->stats_ringbuffer), 0, sizeof(stats_ringbuffer_t));

    /* Watch FIFO/pipe when the channel can be started again */
    if (watch)
    	open_out(ch_info);
}

static inline void channel_handle_data(struct channel_info *ch_info,
                                          fd_set *wfds)
{
    pthread_mutex_lock(&data_mutex);

    /* No data is available to write on output, return */
    if(ch_info->buffer.offset == ch_info->buffer.size) {
        pthread_mutex_unlock(&data_mutex);
        return;
    }

    /* Data is available to write on output */

    /* If not done yet, add its output file to the
       ready-to-write watching list */
    if(!FD_ISSET(ch_info->out.fd, wfds)) {
        FD_SET(ch_info->out.fd, wfds);
        pthread_mutex_unlock(&data_mutex);
        return;
    }

    /* Do a non blocking write. */
    int retval = write(ch_info->out.fd,
                   ch_info->buffer.data+ch_info->buffer.offset,
                   ch_info->buffer.size-ch_info->buffer.offset);
    if(retval > 0) {
        ch_info->buffer.offset += retval;
        /* All the content of the buffer has been written out.
           We can remove the output file from the ready-to-write
           watching list. */
        if(ch_info->buffer.offset == ch_info->buffer.size)
            FD_CLR(ch_info->out.fd, wfds);

    }
    /* The output file was a pipe and the other end of the
       pipe was closed (leading to a pipe error) */
    else if (errno == EPIPE) {
        ch_info->state = CHANNEL_STOPPING;
        wakeup_mainloop();
    } else {
        fprintf(stderr, "Should not happen: channel %i: retval = %i\n",
               ch_info->ch+1, retval);
        perror("");
    }

    pthread_mutex_unlock(&data_mutex);
}


/************************
 *    STREAM command    *
 ************************/
void print_stats(struct channel_info *ch_info)
{
    int i;
    struct video_stats stats;

    /* Clear the screen and go back to (0, 0) */
    fprintf(stderr, "\033[2J\033[;H");

    fprintf(stderr, "Video statistics            %is |      %is |      %is |\n\n",
            LOW_AVG_SECS, MED_AVG_SECS, HGH_AVG_SECS);

    for(i=0; i<MAX_VID_CHANNELS; i++) {
        /* Only print statistics for the video channel started */
        if(ch_info[i].state != CHANNEL_STARTED)
            continue;

        video_stats_get(&(ch_info[i].stats_ringbuffer), &stats);
        //fprintf(stderr, "CH%i: FMT=%d; @%16d\n", i+1, ch_info[i].format, stats.nCurSec);
        fprintf(stderr, "CH%i: FMT=%d; @%8d\n", i+1, ch_info[i].format, gStrmingTotalTime);
        //printf("    Framerate (fps) | %8.1f | %8.1f | %8.1f |\n",
        fprintf(stderr, "    Framerate (fps) | %8.1f | %8.1f | %8.1f |\n",
               (float)stats.framerate_x10[0] / 10,
               (float)stats.framerate_x10[1] / 10,
               (float)stats.framerate_x10[2] / 10);
        fprintf(stderr, "    Bitrate  (kbps) | %8i | %8i | %8i |\n",
               stats.bitrate[0],
               stats.bitrate[1],
               stats.bitrate[2]);
        fprintf(stderr, "    MaxFrmSize      | %8i | %8i | %8i |\n", 
               stats.maxfrmsize[0], 
               stats.maxfrmsize[1], 
               stats.maxfrmsize[2]); 
	if (ch_info[i].format == VID_FORMAT_H264_RAW) {
        fprintf(stderr, "    IFrmCount       | %8i | %8i | %8i |\n\n", 
               stats.ifrmcount[0], 
               stats.ifrmcount[1], 
               stats.ifrmcount[2]); 
        }
    }
}

void sig_handler(int signo)
{
        if (signo == SIGINT)
        {
                printf("received SIGINT\n");
        }
        else if (signo == SIGKILL)
        {
                printf("received SIGKILL\n");
        }
        else if (signo == SIGSTOP)
        {
                printf("received SIGSTOP\n");
        }
        else if (signo == SIGTERM)
        {
                printf("received SIGTERM\n");
        }

        sig_close = 1;
}

int app_subcmd_stream(struct app_subcmd *cmd,
                      struct option_val **optval,
                      struct arg_val **argval)
{
    int i, channel_selected = 0, stats_enabled = 0;
    fd_set rfds, wfds;
    int retval=0, stream_time=0;
    int largest_fd;;
    struct timeval tv, tv_prev, tv_done, *select_tv = NULL;
    struct channel_info ch_info[MAX_VID_CHANNELS+MAX_AUD_CHANNELS];
    video_channel_info_t info;

    pipe(callback_event_fd);
    memset(ch_info, 0, sizeof(ch_info));

    /* Ignore 'Broken Pipe' signal */
    signal(SIGPIPE, SIG_IGN);

    /* Parse --voutX parameters */
    for(i=0; i<MAX_VID_CHANNELS; i++) {
        ch_info[i].type = VIDEO_CHANNEL;
        if(!has_option(opt_vout1+i, optval))
            continue;

        ch_info[i].out.path =
            (const char *) get_value_from_option(opt_vout1+i, optval);
        ch_info[i].ch = CH1+i;
        mxuvc_video_register_cb(CH1+i, video_cb, &ch_info[i]);

	mxuvc_video_get_channel_info(ch_info[i].ch, &info); 
	ch_info[i].format = info.format;
	
        /* Start a thread to open the output file in the background.
         The reason for a thread is that open() can block if the output
         file is a pipe */
        open_out(&ch_info[i]);
        channel_selected++;
    }

    #ifdef AUDIO_BACKEND
    /* Parse --aoutX parameters */
    for(i=MAX_VID_CHANNELS; i<MAX_VID_CHANNELS + MAX_AUD_CHANNELS; i++) {
        int aud_i = i-MAX_VID_CHANNELS;
        ch_info[i].type = AUDIO_CHANNEL;
        if(!has_option(opt_aout1+aud_i, optval))
            continue;

        ch_info[i].out.path =
            (const char *) get_value_from_option(opt_aout1+aud_i, optval);
        ch_info[i].ch = AUD_CH1+aud_i;
        mxuvc_audio_register_cb(AUD_CH1+aud_i,(mxuvc_audio_cb_t) audio_cb, &ch_info[i]);
        /* Select the samplerate if specified (otherwise use the default one) */
        if(has_option(opt_asr1+aud_i, optval)) {
            long samplerate = (intptr_t) get_value_from_option(opt_asr1+aud_i, optval);
            if(mxuvc_audio_set_samplerate(ch_info[i].ch, samplerate) < 0)
                return -1;
        }
        /* Start a thread to open the output file in the background.
         The reason for a thread is that open() can block if the output
         file is a pipe */
        open_out(&ch_info[i]);
        channel_selected++;
    }
    #endif

    if(channel_selected == 0) {
        print_usage(cmd);
        return -1;
    }

    /* If the --time option was passed */
    if(has_option(opt_time, optval)) {
        stream_time = ((intptr_t) get_value_from_option(opt_time, optval));
        select_tv = &tv;

        gettimeofday(&tv_done, NULL);
        tv_done.tv_sec += stream_time;
    }

    /* If video statistics are enabled */
    if(has_option(opt_vid_stats, optval)) {
        stats_enabled = 1;
        gettimeofday(&tv_prev, NULL);
        select_tv = &tv;
    }

    largest_fd = callback_event_fd[0];
    FD_ZERO(&rfds);
    FD_ZERO(&wfds);

    gStrmingTotalTime = 0;
    gettimeofday(&tvStartStrming, NULL);

    signal(SIGINT, sig_handler);
    signal(SIGKILL, sig_handler);
    signal(SIGSTOP, sig_handler);
    signal(SIGTERM, sig_handler);

    /* Starting mainloop */
    while(1) {
        if(stream_time > 0 || stats_enabled == 1) {
            /* Timeout every 500ms */
            tv.tv_sec = 0;
            tv.tv_usec = 500000;
        }

        /* Make sure the mainloop is always listening for callback events */
        FD_SET(callback_event_fd[0], &rfds);

        /* Wait for:
             - data ready to be written out
             - output ready to be written to
             - the timeout
        */
        retval = select(largest_fd+1, &rfds, &wfds, NULL, select_tv);

        if(sig_close)
            break;

        if(stream_time > 0) {
            gettimeofday(&tv, NULL);
            if(timercmp(&tv, &tv_done, >))
                break;
        }

        if(stats_enabled == 1) {
            gettimeofday(&tv, NULL);

            struct timeval tvdiff;
            timersub(&tv, &tv_prev, &tvdiff);

            if(1000000*tvdiff.tv_sec + tvdiff.tv_usec >= 500000) {
                gStrmingTotalTime = tv.tv_sec - tvStartStrming.tv_sec;
                pthread_mutex_lock(&data_mutex);
                print_stats(&ch_info[0]);
                pthread_mutex_unlock(&data_mutex);
                tv_prev = tv;
            }
        }

        /* Select error */
        if (retval == -1) {
            perror("select()");
            continue;
        }

        /* Select timeout */
        if (retval == 0)
            continue;

        /* Clear the wakeup event */
        if(FD_ISSET(callback_event_fd[0], &rfds)) {
            int wakeup;
            read(callback_event_fd[0], &wakeup, sizeof(int));
        }

        /* Process every channels */
        for(i=0; i<MAX_VID_CHANNELS+MAX_AUD_CHANNELS; i++) {

            /* The channel has been marked to be started, start it */
            if(ch_info[i].state == CHANNEL_STARTING)
                channel_start(&ch_info[i], &wfds, &largest_fd);

            /* The channel is started, check for data  */
            if(ch_info[i].state == CHANNEL_STARTED)
                channel_handle_data(&ch_info[i], &wfds);

            /* The channel has been marked to be stopped, stop it  */
            if(ch_info[i].state == CHANNEL_STOPPING)
                channel_stop(&ch_info[i], &wfds);
        }
    }

    /* Stop started channels */
    for(i=0; i<MAX_VID_CHANNELS+MAX_AUD_CHANNELS; i++)
        if(ch_info[i].state == CHANNEL_STARTED)
            channel_stop(&ch_info[i], &wfds);

    return 0;
}
