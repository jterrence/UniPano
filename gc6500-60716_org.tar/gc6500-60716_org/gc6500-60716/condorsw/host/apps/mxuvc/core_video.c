/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include <stdio.h>
#include <assert.h>

#include "parsecmd.h"
#include "main.h"
#include "mxuvc.h"

extern int dw_panel;
extern video_channel_t ch;

IMPL_SUBCMD_GETSETINT(framerate, "framerate", uint32_t, arg_framerate,
                      mxuvc_video_get_framerate, mxuvc_video_set_framerate);

IMPL_SUBCMD_GETSETINT(gop, "gop size", uint32_t, arg_gop,
                      mxuvc_video_get_goplen, mxuvc_video_set_goplen);

IMPL_SUBCMD_GETSETINT(bitrate, "bitrate", uint32_t, arg_bitrate,
                      mxuvc_video_get_bitrate, mxuvc_video_set_bitrate);


IMPL_SUBCMD_GETSETINT(framesize, "maximum frame size", uint32_t,
                      arg_max_framesize,
                      mxuvc_video_get_max_framesize,
                      mxuvc_video_set_max_framesize);

IMPL_SUBCMD_GETSETINT(compqual, "JPEG compression quality", uint32_t,
                      arg_comp_quality,
                      mxuvc_video_get_compression_quality,
                      mxuvc_video_set_compression_quality);

IMPL_SUBCMD_GETSETINT(sensorframerate, "sensorframerate", uint32_t, arg_sensorframerate,
                      mxuvc_video_get_sensor_framerate, mxuvc_video_set_sensor_framerate);

IMPL_SUBCMD_GETSETINT(tsvc_level, "tsvc_level", uint32_t, arg_tsvc_level,
                      mxuvc_video_get_tsvc_level, mxuvc_video_set_tsvc_level);

/* IFRAME */
int app_subcmd_iframe(struct app_subcmd *cmd,
                      struct option_val **optval,
                      struct arg_val **argval)
{
    printf("Forcing I frame on channel %i\n", ch+1);
    mxuvc_video_force_iframe(ch);
    return 0;
}

/* RESOLUTION */
int app_subcmd_resolution(struct app_subcmd *cmd,
                          struct option_val **optval,
                          struct arg_val **argval)
{
    uint16_t w, h;
    if(has_arg(arg_res_width, argval) && has_arg(arg_res_height, argval)) {
        w = (intptr_t) get_value_from_arg(arg_res_width, argval);
        h = (intptr_t) get_value_from_arg(arg_res_height, argval);
        printf("Setting resolution to %ux%u on channel %i\n", w, h, ch+1);
        mxuvc_video_set_resolution(ch, w, h);
    } else {
        mxuvc_video_get_resolution(ch, &w, &h);
        printf("%ux%u\n", w, h);
    }
    return 0;
}

/* VIDEO FORMAT */
int app_subcmd_vidformat(struct app_subcmd *cmd,
                         struct option_val **optval,
                         struct arg_val **argval)
{
    static const char* format_map[NUM_VID_FORMAT] = {
            [VID_FORMAT_H264_RAW]    = "h264es",
            [VID_FORMAT_H264_TS]     = "h264ts",
            [VID_FORMAT_MJPEG_RAW]   = "mjpeg",
            [VID_FORMAT_YUY2_RAW]    = "yuy2",
            [VID_FORMAT_NV12_RAW]    = "nv12",
            [VID_FORMAT_GREY_RAW]    = "grey",
            [VID_FORMAT_H264_AAC_TS] = "h264_aac_ts",
            [VID_FORMAT_MUX]         = "mux",
    };

    const char *format;
    if(has_arg(arg_videoformat, argval)) {
        int i;
        format = (const char*) get_value_from_arg(arg_videoformat, argval);
        for(i=0; i<NUM_VID_FORMAT; i++) {
            const char *supported_format = format_map[i];
            int supported_format_len = strlen(supported_format);
            if(strncmp(format, supported_format, supported_format_len) == 0) {
                printf("Setting video format to %s on channel %i\n", format, ch+1);
                mxuvc_video_set_format(ch, i);
                break;
            }
        }
    } else {
        video_format_t fmt;
        if(mxuvc_video_get_format(ch, &fmt) >= 0) {
           assert(fmt < NUM_VID_FORMAT);
           printf("%s\n", format_map[fmt]);
        }
    }
    return 0;
}

/* PROFILE */
int app_subcmd_profile(struct app_subcmd *cmd,
                       struct option_val **optval,
                       struct arg_val **argval)
{
    const char *profile_str;
    video_profile_t profile;
    const char *profiles[NUM_PROFILE] = {
        [PROFILE_BASELINE] = "baseline",
        [PROFILE_MAIN] = "main",
        [PROFILE_HIGH] = "high"
    };

    if(has_arg(arg_profile, argval)) {
        profile_str = (const char*) get_value_from_arg(arg_profile, argval);

        for(profile=0; profile<NUM_PROFILE; profile++) {
            if(strlen(profile_str) == strlen(profiles[profile]) &&
               strncmp(profile_str, profiles[profile],
                       strlen(profiles[profile])) == 0)
                break;
        }

        if(profile >= NUM_PROFILE) {
            printf("ERROR: The profile should be either 'baseline', "
                   "'main' or 'high'\n.");
            return -1;
        } else {
            printf("Setting the profile to %s on channel %i\n",
                   profiles[profile], ch+1);
            mxuvc_video_set_profile(ch, profile);
        }
    } else {
        mxuvc_video_get_profile(ch, &profile);
        printf("%s\n", profiles[profile]);
    }
    return 0;
}

int app_subcmd_vbrparams(struct app_subcmd *cmd,
                          struct option_val **optval,
                          struct arg_val **argval)
{
    vbr_params_t params;
    if(has_arg(arg_vbrparams_minbitrate, argval)) {
        params.minBitrate = (intptr_t) get_value_from_arg(arg_vbrparams_minbitrate, argval);
        printf("Setting minimum bitrate to %u on channel %i\n", params.minBitrate, ch+1);
        mxuvc_video_set_vbr_params(ch, &params);
    } else {
        mxuvc_video_get_vbr_params(ch, &params);
        printf("Minimum bitrate: %u\n", params.minBitrate);
    }
    return 0;
}

/* CROP */
int app_subcmd_crop(struct app_subcmd *cmd,
                          struct option_val **optval,
                          struct arg_val **argval)
{
    crop_info_t crop;
    int rval;

    if (has_arg(arg_crop_enable,argval)) {
        crop.enable = (intptr_t) get_value_from_arg(arg_crop_enable, argval);
    }
    else
    {
        rval = mxuvc_video_get_crop(ch, &crop );
        if(rval == -1)
            return 0;
        printf("Crop for channel %d: enable=%d x=%d y=%d width=%d height=%d\n",
                ch+1, crop.enable,crop.x, crop.y,
                crop.width, crop.height);
        return 0;
    }
	
    if (has_arg(arg_crop_x,argval)) {
        crop.x = (intptr_t) get_value_from_arg(arg_crop_x, argval);
    }
    else if(crop.enable == 1)
    {
       printf("Error: Crop x,y,width and height not specified\n");
       return 0;
    }
    if (has_arg(arg_crop_y,argval)) {
        crop.y = (intptr_t) get_value_from_arg(arg_crop_y, argval);
    }
    else if(crop.enable == 1)
    {
       printf("Error: Crop y,width and height not specified\n");
       return 0;
    }
    if (has_arg(arg_crop_width,argval)) {
        crop.width =
            (intptr_t) get_value_from_arg(arg_crop_width, argval);
    }
    else if(crop.enable == 1)
    {
       printf("Error: Crop width and height not specified\n");
       return 0;
    }
    if (has_arg(arg_crop_height,argval)) {
        crop.height =
            (intptr_t) get_value_from_arg(arg_crop_height, argval);
    }
    else if(crop.enable == 1)
    {
       printf("Error: Crop height not specified\n");
       return 0;
    }

    mxuvc_video_set_crop(ch, &crop );
    return 0;
}

/* AEROI */
int app_subcmd_aeroi(struct app_subcmd *cmd,
                          struct option_val **optval,
                          struct arg_val **argval)
{
    isp_ae_roi_info_t aeroi;
    int rval;

    if (has_arg(arg_aeroi_mode, argval)) {
        aeroi.mode = (intptr_t) get_value_from_arg(arg_aeroi_mode, argval);
    }
    else
    {
        printf("Error: aeroi x,y,width and height not specified\n");
  
        rval = mxuvc_video_get_isp_roi(ch, &aeroi );
        if(rval == -1)
            return 0;
        printf("AEROI: for channel %d: mode=%d x=%d y=%d width=%d height=%d\n",
                ch+1, aeroi.mode,aeroi.x, aeroi.y,
                aeroi.width, aeroi.height);
        return 0;
    }
  
    if (has_arg(arg_aeroi_x,argval)) {
        aeroi.x = (intptr_t) get_value_from_arg(arg_aeroi_x, argval);
    }
    else if(aeroi.mode != 0)
    {
       printf("Error: aeroi x,y,width and height not specified\n");
       return 0;
    }

    if (has_arg(arg_aeroi_y,argval)) {
        aeroi.y = (intptr_t) get_value_from_arg(arg_aeroi_y, argval);
    }
    else if(aeroi.mode != 0) 
    {
       printf("Error: aeroi y,width and height not specified\n");
       return 0;
    }

    if (has_arg(arg_aeroi_width,argval)) {
        aeroi.width =
            (intptr_t) get_value_from_arg(arg_aeroi_width, argval);
    }
    else if(aeroi.mode != 0) 
    {
       printf("Error: aeroi width and height not specified\n");
       return 0;
    }
    
    if (has_arg(arg_aeroi_height,argval)) {
        aeroi.height =
            (intptr_t) get_value_from_arg(arg_aeroi_height, argval);
    }
    else if(aeroi.mode != 0)
    {
       printf("Error: aeroi height not specified\n");
       return 0;
    }
    
    mxuvc_video_set_isp_roi(ch, &aeroi);
    return 0;
}
