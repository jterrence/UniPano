/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include <stdio.h>

#include "parsecmd.h"
#include "main.h"
#include "mxuvc.h"

/* Codec set param */
int app_subcmd_qparam(struct app_subcmd *cmd,
                       struct option_val **optval,
                       struct arg_val **argval)
{
    int ret;
    const char *object_name, *param_name, *param_type;
    const char default_param_type[] = {'p','a','r','a','m','s','\0'};
    int param_value;
    char activate;

    object_name = (const char*) get_value_from_arg(arg_qparam_object, argval);
    param_name  = (const char*) get_value_from_arg(arg_qparam_name,  argval);
    param_value = (intptr_t) get_value_from_arg(arg_qparam_value, argval);

    if(has_option(opt_qparam_type, optval))
        param_type = (const char*) get_value_from_option(opt_qparam_type, optval);
    else
        param_type = (const char*) &default_param_type[0];

    activate = has_option(opt_qparam_noactivate, optval) ? 0 : 1;

    ret = mxuvc_custom_control_set_qparam(object_name, param_type,
                                          param_name, param_value,
                                          activate);

    if(ret < 0)
        printf("Unable to perform setParam for %s[%s] = %i\n", object_name,
               param_name, param_value);

    return ret;
}
