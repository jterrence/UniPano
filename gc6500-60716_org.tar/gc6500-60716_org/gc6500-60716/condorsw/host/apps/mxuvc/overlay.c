/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include <stdio.h>
#include <time.h>
#include "parsecmd.h"
#include "main.h"
#include "mxuvc.h"

#define DEFAULT_BURNIN_TIME_INDEX (4)
extern video_channel_t ch;

extern int bOverlayInit;
extern overlay_image_params_t ovimages[]; 

// 
// <DateTimeString construction>
//        
// if the offset is calculated as the following (+1) the burn shown as 
//     "YYYY-MM-DD  HH:MM:SS" for font32 
//
// if rmoving (+1) in the equiation, we get
//     "YYYY-MM-DDHH:MM:SS"   for font32
// 
#if 0
#define BurnIn_InitDateTimeString(chnum)                                               \
    {   ovtext.xoff = 0;                                                               \
        ret = mxuvc_overlay_add_text((chnum), &ovtext, ovStr,  nStrl, &ovtext.idx);    \
        ovtext.xoff += fontsize * (nStrl+1);                                           \
        ret = mxuvc_overlay_set_time((chnum), &ovtext, &btime);                        \
        ret = mxuvc_overlay_show_time((chnum));                                        \
    }
#else
#define BurnIn_InitDateTimeString(chnum)                                               \
    {                                                                                  \
        ovtext.xoff = fontsize * (nStrl+1);                                            \
        ret = mxuvc_overlay_set_time((chnum), &ovtext, &btime);                        \
    }
#endif //#if 0

// update the overlay_time structure to current time as well as curr date to szDate
int SetCurrOVDateTime( char *szDate, overlay_time_t *pOVtime ) 
{
    time_t curtime;
    struct tm *loc_time;
    int ret = 0;
    curtime = time (NULL); 
    loc_time = localtime (&curtime);
    printf("%s", asctime (loc_time));

    pOVtime->hh = loc_time->tm_hour;
    pOVtime->mm = loc_time->tm_min;
    pOVtime->ss = loc_time->tm_sec;

    if (szDate) {
        snprintf(szDate, 12, "%04d-%02d-%02d ", (loc_time->tm_year+1900), (loc_time->tm_mon+1), loc_time->tm_mday);
        printf("Date = \"%s\" strlen = %d\n", szDate, strlen(szDate));
        ret = strlen(szDate);
    }
    return ret;
}
 
/* Codec set param */
int app_subcmd_burnininit(struct app_subcmd *cmd,
                           struct option_val **optval,
                           struct arg_val **argval)
{
    int ret = 0;
    //const char *fontfile = (const char*) get_value_from_arg(arg_burnin_fontfile, argval);
    char *fontfile = (const char*) get_value_from_arg(arg_burnin_fontfile, argval);
    int fontsize  = (intptr_t) get_value_from_arg(arg_burnin_fontsize, argval);
    overlay_text_params_t ovtext;
    overlay_time_t btime;
    char ovStr[16] = {0};
    int nStrl = 0;

    printf("!!!! \n");
    printf("!!!! This command should only be issued ONCE after FW loaded !!!!\n");
    printf("!!!! \n");
    printf("---- BurnIn INIT %s size = %d CH = %d\n", fontfile, fontsize, ch);    

    ret = mxuvc_overlay_init();

    ovtext.xoff = 0;
    ovtext.yoff = 0;
    //ovtext.idx  = DEFAULT_BURIN_TIME_INDEX; // max # of burnin string = 5. time str will always be -1 -> 4

    nStrl = SetCurrOVDateTime( ovStr, &btime );
    btime.frame_num_enable = 0; //frame_num;

    if (has_option(opt_vsel1, optval)) {
        ret = mxuvc_overlay_load_font(0, fontsize, fontfile);
	//printf("load font @ size=%d\n", fontsize);
	//sleep(1);
        BurnIn_InitDateTimeString(0);
    }
    if (has_option(opt_vsel2, optval)) {
        ret = mxuvc_overlay_load_font(1, fontsize, fontfile);
        BurnIn_InitDateTimeString(1);
    }
    if (has_option(opt_vsel3, optval)) {
        ret = mxuvc_overlay_load_font(2, fontsize, fontfile);
        BurnIn_InitDateTimeString(2);
    }
    if (has_option(opt_vsel4, optval)) {
        ret = mxuvc_overlay_load_font(3, fontsize, fontfile);
        BurnIn_InitDateTimeString(3);
    }
    if (has_option(opt_vsel5, optval)) {
        ret = mxuvc_overlay_load_font(4, fontsize, fontfile);
        BurnIn_InitDateTimeString(4);
    }
    if (has_option(opt_vsel6, optval)) {
        ret = mxuvc_overlay_load_font(5, fontsize, fontfile);
        BurnIn_InitDateTimeString(5);
    }
    if (has_option(opt_vsel7, optval)) {
        ret = mxuvc_overlay_load_font(6, fontsize, fontfile);
        BurnIn_InitDateTimeString(6);
    }


    return ret;
}

int app_subcmd_burnintime(struct app_subcmd *cmd,
                           struct option_val **optval,
                           struct arg_val **argval)
{
    int ret = 0;
    int xstart = 0;
    int ystart = 0;
    int nCmd = (intptr_t) get_value_from_arg(arg_burnin_time_show,  argval);
    overlay_text_params_t ovtext;
    overlay_time_t btime;

    ret = mxuvc_overlay_init(); //This is required. every mxuvc cli command is a diferernt process.
    if (nCmd == 0) {
        ret = mxuvc_overlay_hide_time(ch);
    }
    else {
        if (has_arg(arg_burnin_time_xoff, argval)) {
            xstart = (intptr_t) get_value_from_arg(arg_burnin_time_xoff, argval); 
            printf("-> xstart = %4d\n", xstart);
        }
        if (has_arg(arg_burnin_time_yoff, argval)) {
            ystart = (intptr_t) get_value_from_arg(arg_burnin_time_yoff, argval); 
            printf("-> ystart = %4d\n", ystart);
        }
        if (xstart || ystart) {
            ovtext.xoff = xstart;
            ovtext.yoff = ystart;
            ovtext.idx  = DEFAULT_BURNIN_TIME_INDEX;
            SetCurrOVDateTime( 0, &btime );
            btime.frame_num_enable = 0; //frame_num;
            ret = mxuvc_overlay_set_time((ch), &ovtext, &btime);
        }
        ret = mxuvc_overlay_show_time(ch);
    }
}
 
int app_subcmd_burnintext(struct app_subcmd *cmd,
                           struct option_val **optval,
                           struct arg_val **argval)
{
    int ret = 0;
    const char *szText = (const char*) get_value_from_arg(arg_burnin_text_string, argval);
    int txtId  = (intptr_t) get_value_from_arg(arg_burnin_text_id,   argval);
    int txtCmd = (intptr_t) get_value_from_arg(arg_burnin_text_cmd,  argval);
    int txtXs  = (intptr_t) get_value_from_arg(arg_burnin_text_xoff, argval);
    int txtYs  = (intptr_t) get_value_from_arg(arg_burnin_text_yoff, argval);
    overlay_text_params_t ovtext;

    printf("---- BurnIn TEXT \"%s\" CMD(%d) ID(%d) @ (%4d, %4d) strlen=%d\n", 
           szText, txtCmd, txtId, txtXs, txtYs, strlen(szText));

    ret = mxuvc_overlay_init(); //This is required. every mxuvc cli command is a diferernt process.
 
    ovtext.xoff = txtXs;
    ovtext.yoff = txtYs;
    ovtext.idx  = txtId;
    if (txtCmd > 0) {
        //ret = mxuvc_overlay_add_text(ch, &ovtext, szText, strlen(szText), &ovtext.idx);
        ret = mxuvc_overlay_add_text(ch, &ovtext, szText, strlen(szText));
        printf("-> txtId = %d, ovtext.idx = %d\n", txtId, ovtext.idx);
    }
    else {
        ret = mxuvc_overlay_remove_text(ch, ovtext.idx);     
    }

    return ret;
}


 
int app_subcmd_ovimage(struct app_subcmd *cmd,
                       struct option_val **optval,
                       struct arg_val **argval)
{
    int ret = 0;
    //const char *logofile = (const char*) get_value_from_arg(arg_ovimage_file, argval);
    char *logofile = (const char*) get_value_from_arg(arg_ovimage_file, argval);
    int offsetX  = (intptr_t) get_value_from_arg(arg_ovimage_xoff,   argval);
    int offsetY  = (intptr_t) get_value_from_arg(arg_ovimage_yoff,   argval);
    int ovWidth  = (intptr_t) get_value_from_arg(arg_ovimage_width,  argval);
    int ovHeight = (intptr_t) get_value_from_arg(arg_ovimage_height, argval);
    int ovCmd    = (intptr_t) get_value_from_arg(arg_ovimage_cmd,    argval);
    int ovIdx = 0;

    printf("-> @ CH = %d: %s CMD(%d) @ %d %d %d %d\n", ch, logofile, ovCmd, offsetX, offsetY, ovWidth, ovHeight);
    overlay_image_params_t *pOvImages = &ovimages[ch];

    // initialize overlay engine if it is not yet been initialized.
    if (0 == bOverlayInit) {
        printf("mxuvc_overlay_init() ............\n");
        ret = mxuvc_overlay_init();
        if (ret < 0) {
            printf("ERROR: overlay init() failed %d\n", ret);
            return ret;
        }
        bOverlayInit = 1;
    }

    pOvImages->width  = ovWidth;
    pOvImages->height = ovHeight;
    pOvImages->xoff   = offsetX;
    pOvImages->yoff   = offsetY;

    if(has_option(opt_ov_handle, optval)) {
        ovIdx = get_value_from_option(opt_ov_handle, optval);
    }
    printf("ovIdx = 0x%08x\n", ovIdx);

    ovimages[0].idx = ovIdx;
printf("$1 %d, %d\n", ovimages[0].idx, ovimages[ch].idx);
    if (ovCmd == 1) {
        //ret = mxuvc_overlay_add_image(ch, pOvImages, &ovIdx, logofile);
        ret = mxuvc_overlay_add_image(ch, pOvImages, logofile, NULL);
        if (ret < 0) {
            printf("ERROR: overlay add image failed %d\n", ret);
        }
        pOvImages->idx = ovIdx;
printf("$ADD %d, %x\n", ovimages[0].idx, ovimages[ch].idx);
        mxuvc_overlay_set_colorkey (ch, pOvImages->idx, 0xFF8080FF);
    }

printf("$2 %d, %d\n", ovimages[0].idx, ovimages[ch].idx);
    if (ovCmd == 0) {
        printf("overlay remove image @ idx = %d. ? ovimages[0].idx = %d\n", pOvImages->idx, ovimages[0].idx);
        ret = mxuvc_overlay_remove_image(ch, pOvImages->idx);
        if (ret < 0) {
            printf("ERROR: overlay remove image failed %d\n", ret);
        }
    }    
printf("$3 %d, %x\n", ovimages[0].idx, ovimages[ch].idx);

   return ret;
}
