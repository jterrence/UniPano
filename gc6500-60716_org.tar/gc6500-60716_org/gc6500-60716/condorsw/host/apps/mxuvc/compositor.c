/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#include <stdio.h>
#include <unistd.h> /* sleep() */

#include "parsecmd.h"
#include "main.h"
#include "mxuvc.h"

extern int dw_panel;
extern video_channel_t ch;

/* COMPOSITOR */
int app_subcmd_compositor(struct app_subcmd *cmd,
                          struct option_val **optval,
                          struct arg_val **argval)
{
    int panel = dw_panel;
    panel_params_t panelParams={-1, -1, -1, -1};
    panel_mode_t   panelMode, panelModeSet=4;
    int rval;

    printf("<---- CH %d; PANEL %d ---->\n", ch, panel);

    if (has_arg(arg_compositor_mode,argval)) {
        panelModeSet = (intptr_t) get_value_from_arg(arg_compositor_mode, argval);
	if (panelModeSet >= PMODE_MAX) {
	    printf("Error: try setting unsupported MODE = %d\n", panelModeSet);
	    return 0;
	}
    }
    else
    {
        rval = mxuvc_video_get_compositor_params( ch, panel, &panelMode, &panelParams );
        if(rval == -1)
            return 0;
        printf("panel %d: mode=%d x=%-3d y=%-3d width=%-4d height=%-4d\n",
                panel, panelMode, panelParams.x, panelParams.y,
                panelParams.width, panelParams.height);
        return 0;
    }
    if (has_arg(arg_compositor_x,argval)) {
        panelParams.x = (intptr_t) get_value_from_arg(arg_compositor_x, argval);
    }
    else if(panelModeSet == 1)
    {
       printf("Error: Panel x,y,width and height not specfied\n");
       return 0;
    }
    if (has_arg(arg_compositor_y,argval)) {
        panelParams.y = (intptr_t) get_value_from_arg(arg_compositor_y, argval);
    }
    else if(panelModeSet == 1)
    {
       printf("Error: Panel y,width and height not specfied\n");
       return 0;
    }
    if (has_arg(arg_compositor_width,argval)) {
        panelParams.width =
            (intptr_t) get_value_from_arg(arg_compositor_width, argval);
    }
    else if(panelModeSet == 1)
    {
       printf("Error: Panel width and height not specfied\n");
       return 0;
    }
    if (has_arg(arg_compositor_height,argval)) {
        panelParams.height =
            (intptr_t) get_value_from_arg(arg_compositor_height, argval);
    }
    else if(panelModeSet == 1)
    {
       printf("Error: Panel height not specfied\n");
       return 0;
    }

    switch (panelModeSet) {
    case 0:
        printf("<---- CH %d; PANEL %d; MODE %d ---->\n",
               ch, panel, panelModeSet);
        mxuvc_video_set_compositor_params((video_channel_t) ch, panel,
                                          PMODE_OFF, &panelParams);
        break;
    case 1:
        printf("<---- CH %d; PANEL %d; MODE %d; PARAMS %d %d %d %d ---->\n",
               ch, panel, panelModeSet, panelParams.x, panelParams.y,
               panelParams.width, panelParams.height);
        mxuvc_video_set_compositor_params((video_channel_t) ch, panel,
                                          PMODE_ON, &panelParams);
        break;
    case 2:
        printf("<---- CH %d; PANEL %d; MODE %d ---->\n",
               ch, panel, panelModeSet);
        mxuvc_video_set_compositor_params((video_channel_t) ch, panel,
                                          PMODE_SELECT, &panelParams);
        break;
    case 3:
        printf("<---- CH %d; PANEL %d; MODE %d ---->\n",
               ch, panel, panelModeSet);
        mxuvc_video_set_compositor_params((video_channel_t) ch, panel,
                                          PMODE_UNSELECT, &panelParams);
        break;
    case 4:
        printf("<---- CH %d; PANEL %d; MODE %d (Pause Demo) ---->\n",
               ch, panel, panelModeSet);
        mxuvc_video_set_compositor_params((video_channel_t) ch, panel,
                                          PMODE_PAUSE_DEMO, &panelParams);
        break;
    case 5:
        printf("<---- CH %d; PANEL %d; MODE %d (Unpause Demo) ---->\n",
               ch, panel, panelModeSet);
        mxuvc_video_set_compositor_params((video_channel_t) ch, panel,
                                          PMODE_UNPAUSE_DEMO, &panelParams);
        break;
    default:
        printf("Error: Invalid compositor mode set\n");
        break;
    }

   return 0;
}

int app_subcmd_pause360demo(struct app_subcmd *cmd,
                      struct option_val **optval,
                      struct arg_val **argval)
{
    int lpause = 0;
    int panel = dw_panel;
    panel_params_t panelParams={-1, -1, -1, -1};
    panel_mode_t   panelMode;

    mxuvc_video_get_compositor_params( ch, panel, &panelMode, &panelParams );

    if (has_arg(arg_pause360demo_mode,argval)) {
        lpause = (intptr_t) get_value_from_arg(arg_pause360demo_mode, argval);
    }
    else {
        printf("Error: argument is missing\n");
        return(0);
    }
	
	if(lpause != 0)
    {
        mxuvc_video_set_compositor_params((video_channel_t) ch, panel,
                                          PMODE_PAUSE_DEMO, &panelParams);
    }
    else
    {
        mxuvc_video_set_compositor_params((video_channel_t) ch, panel,
                                          PMODE_UNPAUSE_DEMO, &panelParams);
    }

    return(0);
}
