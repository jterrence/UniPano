/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef __OPTARGS_H__
#define __OPTARGS_H__

#include "parsecmd.h"

#define DEFAULT_VENDOR_ID 0x29fe
#define DEFAULT_PRODUCT_ID 0x4d53

/* Options */
typedef enum {
    opt_verbose=FIRST_LONGOPT_ID,
    opt_offset,
    opt_channel,
    opt_panel,
    opt_vout1,
    opt_vout2,
    opt_vout3,
    opt_vout4,
    opt_vout5,
    opt_vout6,
    opt_vout7,
    opt_aout1,
    opt_aout2,
    opt_asr1,
    opt_asr2,
    opt_time,
    opt_spkr_mode,
    opt_spkr_srate,
    opt_spkr_gain,
    opt_vid_stats,
    opt_qparam_type,
    opt_qparam_noactivate,
    opt_ov_handle,
    opt_vsel1,
    opt_vsel2,
    opt_vsel3,
    opt_vsel4,
    opt_vsel5,
    opt_vsel6,
    opt_vsel7,
    opt_vendor_id,
    opt_product_id,
} app_longopt_id_t;

/* Arguments */
typedef enum {
    arg_help_subcmd = 1,
    arg_framerate,
    arg_res_width,
    arg_res_height,
    arg_wdr_mode,
    arg_wdr_strength,
    arg_sinter_mode,
    arg_sinter_min_nr,
    arg_sinter_max_nr,
    arg_sinter_min_thr,
    arg_sinter_max_thr,
    arg_sinter_trig_pt,
    arg_saturation_mode,
    arg_brightness_mode,
    arg_contrast_mode,
    arg_vflip,
    arg_hflip,
    arg_gop,
    arg_bitrate,
    arg_brightness,
    arg_contrast,
    arg_saturation,
    arg_hue,
    arg_max_framesize,
    arg_comp_quality,
    arg_profile,
    arg_videoformat,
    arg_dwmode,
    arg_dweptz_hpan,
    arg_dweptz_vpan,
    arg_dweptz_tilt,
    arg_dweptz_zoom,
    arg_dweptz_Divisor,
    arg_dweptz_xcenter,
    arg_dweptz_ycenter,
    arg_dweptz_heightfromcenter,
    arg_dwzcl_phi0,
    arg_dwzcl_rx,
    arg_dwzcl_ry,
    arg_dwzcl_rz,
    arg_dwzcl_gshift,
    arg_dwzcl_Divisor,
    arg_dwzclc_height,
    arg_dwzcls_hpan,
    arg_dwzcls_vpan,
    arg_dwzcls_zoom,
    arg_dwzcls_Divisor,
    arg_dwsweep_hstart,
    arg_dwsweep_vstart,
    arg_dwsweep_tstart,
    arg_dwsweep_zstart,
    arg_dwsweep_hinc,
    arg_dwsweep_vinc,
    arg_dwsweep_tinc,
    arg_dwsweep_zinc,
    arg_dwsweep_period,
    arg_dwsweep_Divisor,
    arg_dwmagnify_zoom,
    arg_dwmagnify_radius,
    arg_dwmagnify_xcenter,
    arg_dwmagnify_ycenter,
    arg_dwmagnify_Divisor,
    arg_dwtm1pano_anglec,
    arg_dwtm1pano_angle,
    arg_dwtm1pano_Divisor,
    arg_dwcm1pano_r0mili,
    arg_dwcm1pano_r1mili,
    arg_dwcm1pano_phi1,
    arg_dwcm1pano_phishift1,
    arg_dwcm1pano_Divisor,
    arg_dwroieptz_xcenter,
    arg_dwroieptz_ycenter,
    arg_dwroieptz_heightfromcenter,
    arg_dwroieptz_sourcechannel,
    arg_dwroieptz_sourcepanel,
    arg_compositor_mode,
    arg_compositor_x,
    arg_compositor_y,
    arg_compositor_width,
    arg_compositor_height,
    arg_aec_mode,
    arg_volume,
    arg_qparam_object,
    arg_qparam_name,
    arg_qparam_value,
    arg_vbrparams_minbitrate,
    arg_sharpness,
    arg_gamma,
    arg_histogram_eq,
    arg_max_analog_gain,
    arg_sharpen_filter,
    arg_gain_multiplier,
    arg_wb_mode,
    arg_wb_strength,
    arg_pause360demo_mode,
    arg_ovimage_file,
    arg_ovimage_cmd,
    arg_ovimage_xoff,
    arg_ovimage_yoff,
    arg_ovimage_width, 
    arg_ovimage_height,
    arg_burnin_fontfile, 
    arg_burnin_fontsize,
    arg_burnin_text_id,
    arg_burnin_text_cmd, 
    arg_burnin_text_string,
    arg_burnin_text_xoff, 
    arg_burnin_text_yoff,
    arg_burnin_time_show,
    arg_burnin_time_xoff,
    arg_burnin_time_yoff,
    arg_ispcfg_file, 
    arg_crop_enable,
    arg_crop_x,
    arg_crop_y,
    arg_crop_width,
    arg_crop_height,
    arg_sensorframerate,
    arg_aeroi_mode,
    arg_aeroi_x,
    arg_aeroi_y,
    arg_aeroi_width,
    arg_aeroi_height,
    arg_tsvc_level
} app_arg_id_t;


#define IMPL_SUBCMD_GETSETINT(cmd, cmd_str, val_type, arg_id, get_func, set_func) \
int app_subcmd_ ## cmd(struct app_subcmd *cmd,                      \
                       struct option_val **optval,                  \
                       struct arg_val **argval)                     \
{\
    val_type val;\
    if(has_arg(arg_id, argval)) {\
        val = (intptr_t) get_value_from_arg(arg_id, argval);\
        printf("Setting the %s to %i on channel %u\n", cmd_str, val, ch+1); \
        set_func(ch, val);\
    } else {\
        get_func(ch, &val);\
        printf("%i\n", val);\
    }\
    return 0;\
}

#endif
