/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

/*******************************************************************************
* @(#) $Id: devsetup.h		02-02-2011		raghav $
*******************************************************************************/

#ifndef __DEVSETUP_H__
#define __DEVSETUP_H__

#include <stdio.h>
#include <string.h>
#include <windows.h>  
#include <initguid.h>   // Guid definition
#include <devguid.h>    // Device guids
#include <setupapi.h>   // for SetupDiXxx functions.
#include <cfgmgr32.h>   // for SetupDiXxx functions.
#include "libmxcam.h"

#define RAPTOR_VID		0x0B6A
#define RAPTOR_PID		0x4D52

typedef struct {
	uint16_t	vid; /* vendor id */
	uint16_t	pid; /* product id */
	uint16_t	mi;  /* interface id */

} devlist;

typedef struct {
	uint16_t	vid; /* vendor id */
	uint16_t	pid; /* product id */
	DEVICE_TYPE	dev; /* device type BOOT or UVC ? */
	SOC_TYPE 	soc; /* soc type MAX64180 or MAX64380 ? */
	char    	desc[64]; /* description of device */
} MX_IdList;

void DevInstall(short vid, short pid, short mi);
void DevUninstall(short vid, short pid,short ifNum);
void DevRescan();

int FindDev(HDEVINFO DevInfo, DWORD Index, short VID, short PID, wchar_t *pDevID, long len);
int DevSetup(short vid, short pid, short ifNum);

#endif //__DEVSETUP_H__