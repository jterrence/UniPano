/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

/*******************************************************************************
* @(#) $Id: devsetup.c			02-02-2011		raghav $
*******************************************************************************/
#include "devsetup.h"
#include "libmxcam.h"

extern int verbose;

wchar_t DevID[1024] = {0};
wchar_t DeConPath[1024]={0};
short dev_vid=0 ;
short dev_pid=0;

int FindDev( HDEVINFO DevInfo, DWORD Index, short VID, short PID, wchar_t *pDevID, long len)
{
    SP_DEVINFO_DATA deviceInfoData;
    DWORD           errorCode;
    DWORD           bufferSize = 0;
    DWORD           dataType;
    LPTSTR          buffer = NULL;
	wchar_t			swVID1[32]={0};
	wchar_t			swPID1[32] ={0};
	wchar_t			swVID2[32]={0};
	wchar_t			swPID2[32] ={0};
    int             status = -1;
	BOOL			bBoot = FALSE;
	BOOL			result = 0;
	DWORD			ec = 0;

    deviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
    status = SetupDiEnumDeviceInfo( DevInfo, Index, &deviceInfoData);
    if ( status == FALSE ) {
        errorCode = GetLastError();
        if ( errorCode == ERROR_NO_MORE_ITEMS ) {
            //printf( "\nNo more devices.\n");
			status = -1;
        } else {
            printf( "SetupDiEnumDeviceInfo failed with error: %d\n", errorCode );
			status = -1;
        }
        return status;
    }
 
    status = SetupDiGetDeviceRegistryProperty( DevInfo,
												&deviceInfoData,
												SPDRP_HARDWAREID,
												&dataType,
												(PBYTE)buffer,
												bufferSize,
												&bufferSize);
    if ( status == FALSE ) {
        errorCode = GetLastError();
        if ( errorCode != ERROR_INSUFFICIENT_BUFFER ) {
            if ( errorCode != ERROR_INVALID_DATA ) {
                printf( "SetupDiGetDeviceInterfaceDetail failed with error: %d\n", errorCode );
            }
			return status;
        }
    }

    buffer = (LPTSTR)LocalAlloc(LPTR, bufferSize);
    
    status = SetupDiGetDeviceRegistryProperty( DevInfo,
											&deviceInfoData,
											SPDRP_HARDWAREID,
											&dataType,
											(PBYTE)buffer,
											bufferSize,
											&bufferSize);
    if ( status == FALSE ) {
        errorCode = GetLastError();
        if ( errorCode != ERROR_INVALID_DATA ) {
            printf( "SetupDiGetDeviceInterfaceDetail failed with error: %d\n", errorCode );
        }
		return status;
    }
	status = 0;
	swprintf_s(swVID1, 32, L"%04X",(VID&0xFFFF));
	swprintf_s(swPID1, 32, L"%04X",(PID&0xFFFF));
	swprintf_s(swVID2, 32, L"%04x",(VID&0xFFFF));
	swprintf_s(swPID2, 32, L"%04x",(PID&0xFFFF));
	if( ((wcsstr(buffer,swVID1)!=0)&&(wcsstr(buffer,swPID1)!=0)) ||
		((wcsstr(buffer,swVID2)!=0)&&(wcsstr(buffer,swPID2)!=0))){
		if(verbose)
			printf( "Device ID: %S\n",buffer );		
		status = 1;
		wcscpy_s(pDevID,len,buffer);
	}
    
    if (buffer) {
        LocalFree(buffer);
    }

    return status;
}


int DevSetup(short vid, short pid, short mi)
{
	wchar_t			TempDevID[1024] = {0};
    wchar_t         mi1[16] = {0};
    wchar_t         mi2[16] = {0};
	HDEVINFO        hDevInfo;
    DWORD           index=0;
    int             i=0,status=0,found = 0;

    swprintf_s(mi1,16,L"MI_%02d",mi);
    swprintf_s(mi2,16,L"mi_%02d",mi);

	hDevInfo = SetupDiGetClassDevs(NULL, L"USB", NULL, DIGCF_ALLCLASSES | DIGCF_PRESENT);
    if( hDevInfo == INVALID_HANDLE_VALUE ) {
        printf("SetupDiGetClassDevs failed with error: %d\n", GetLastError() );
		return -1;
    }

	dev_vid = vid;
	dev_pid = pid;

    //  Enumerate all USB devices
	index = 0;
	while((status==0) && !found) {
		status = FindDev( hDevInfo, index, dev_vid, dev_pid , DevID, 1024);
		if( status == -1 ){
			//printf("FindDev faild !!!!");
			break;
		} else if (status == 1 ) {

            if(mi==-1) {
                // bootloader mode, no specific interface.
                swprintf_s(DevID,1024,L"USB\\Vid_%04x&Pid_%04x&Rev_0000",(dev_vid&0xFFFF),(dev_pid&0xFFFF));
                found = 1;
                continue;
            } else {
                if((wcsstr(DevID, mi1)!=0) || (wcsstr(DevID, mi2)!=0)){
                    found = 1;
                    continue;
                } else {
                    status=0;
                }
            }
		} 

		index++;
	}

	if(!found){
        dev_vid = dev_pid = 0;
        status = -1;

	}

    SetupDiDestroyDeviceInfoList(hDevInfo);
	return status;
}


void GetDevConPath()
{
	DWORD dwRet=0;
	dwRet = GetCurrentDirectoryW(1024,(LPWSTR)DeConPath);
}

#define MAX_DEVS    4
devlist  GeoCams[MAX_DEVS] = {{0x29FE, 0xB00C, -1}, {0x29FE, 0x4D53, 2}, {0x1AEA, 0xB00B, -1}, {0x0B6A, 0x4D52, 2}};

short GetInterfaceNumber(short vid, short pid)
{
    int i=0;
    for(i=0;i<MAX_DEVS;i++) {
        if( (GeoCams[i].vid==vid)&&(GeoCams[i].pid==pid))
            return GeoCams[i].mi;
    }
    return 0;
}

void DevInstall(short vid, short pid,short ifNum)
{
    STARTUPINFO si;
    PROCESS_INFORMATION pi;
	wchar_t		szCmdln[512];
	int         status=-1;
    short       mi;

    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);
	si.dwFlags = STARTF_USESTDHANDLES;
    ZeroMemory(&pi, sizeof(pi));

	if(wcscmp(DeConPath, L"")==0){
		GetDevConPath();
	}

    if((vid !=0) && (pid !=0)) {
        if(ifNum !=0) {
            mi = ifNum;
        } else {
            mi = GetInterfaceNumber(vid,pid);
        }
        status = DevSetup(vid, pid, mi);
	    if(status == -1) 
		       return;
    } else {
        int index=0;
        while(index<MAX_DEVS) {
            status = DevSetup(GeoCams[index].vid, GeoCams[index].pid, GeoCams[index].mi);
	        if(status != -1) {
		        break;
	        }
            index++;
        }
        if(index >= 4)
            return;
    } 

	swprintf_s(szCmdln, 512, L"%s\\%s %s %s\\%s %s", DeConPath, L"devcon.exe", L"update", 
											DeConPath, L"winusb\\RaptorCam.inf", DevID);
	if(verbose)
		printf( "szCmdln : %S\n", szCmdln );

    // Start the child process. 
	if( !CreateProcess( NULL,			// No module name (use command line)
						szCmdln,        // Command line
						NULL,           // Process handle not inheritable
						NULL,           // Thread handle not inheritable
						FALSE,          // Set handle inheritance to FALSE
						0,              // No creation flags
						NULL,           // Use parent's environment block
						NULL,           // Use parent's starting directory 
						&si,            // Pointer to STARTUPINFO structure
						&pi )           // Pointer to PROCESS_INFORMATION structure
    ) {
        printf( "CreateProcess failed (%d).\n", GetLastError() );
		printf( "szCmdln : %S\n", szCmdln );
        return;
    }

    // Wait until child process exits.
    WaitForSingleObject( pi.hProcess, INFINITE );
	if(verbose)
		printf( "Installed winusb for DevID: %S \n", DevID);

    // Close process and thread handles. 
    CloseHandle( pi.hProcess );
    CloseHandle( pi.hThread );
}

void DevRescan()
{
    STARTUPINFO si;
    PROCESS_INFORMATION pi;
	wchar_t		szCmdln[512]={0};

    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);
    ZeroMemory(&pi, sizeof(pi));

	if(!verbose)
		si.dwFlags = STARTF_USESTDHANDLES;

	if(wcscmp(DeConPath, L"")==0){
		GetDevConPath();
	}

	swprintf_s(szCmdln, 512, L"%s\\%s", DeConPath, L"devcon.exe rescan");
	if(verbose)
		printf( "szCmdln : %S\n", szCmdln );

    // Start the child process. 
	if( !CreateProcess( NULL,			// No module name (use command line)
						szCmdln,        // Command line
						NULL,           // Process handle not inheritable
						NULL,           // Thread handle not inheritable
						FALSE,          // Set handle inheritance to FALSE
						0,              // No creation flags
						NULL,           // Use parent's environment block
						NULL,           // Use parent's starting directory 
						&si,            // Pointer to STARTUPINFO structure
						&pi )           // Pointer to PROCESS_INFORMATION structure
    ) {
        printf( "CreateProcess failed (%d).\n", GetLastError() );
		printf( "szCmdln : %S\n", szCmdln );
        return;
    }

    // Wait until child process exits.
    WaitForSingleObject( pi.hProcess, INFINITE );

    // Close process and thread handles. 
    CloseHandle( pi.hProcess );
    CloseHandle( pi.hThread );
}


void DevUninstall(short vid, short pid,short ifNum)
{
    STARTUPINFO si;
    PROCESS_INFORMATION pi;
	wchar_t		szCmdln[512]={0};
    short       mi;
	int         status=-1;

    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);
    ZeroMemory(&pi, sizeof(pi));

	if(wcscmp(DeConPath, L"")==0){
		GetDevConPath();
	}

	if(!verbose)
		si.dwFlags = STARTF_USESTDHANDLES;

    if((vid !=0) && (pid !=0)) {
        if(ifNum !=0) {
            mi = ifNum;
        } else {
            mi = GetInterfaceNumber(vid,pid);
        }
        status = DevSetup(vid, pid, mi);
	    if(status == -1) 
		       return;
    } else {
        int index=0;
        while(index<MAX_DEVS) {
            status = DevSetup(GeoCams[index].vid, GeoCams[index].pid, GeoCams[index].mi);
	        if(status != -1) {
		        break;
	        }
            index++;
        }
        if(index >= 4)
            return;
    }

	swprintf_s(szCmdln, 512, L"%s\\%s %s", DeConPath, L"devcon.exe remove", DevID);
	if(verbose)
		printf( "szCmdln : %S\n", szCmdln );
    // Start the child process. 
	if( !CreateProcess( NULL,			// No module name (use command line)
						szCmdln,        // Command line
						NULL,           // Process handle not inheritable
						NULL,           // Thread handle not inheritable
						FALSE,          // Set handle inheritance to FALSE
						0,              // No creation flags
						NULL,           // Use parent's environment block
						NULL,           // Use parent's starting directory 
						&si,            // Pointer to STARTUPINFO structure
						&pi )           // Pointer to PROCESS_INFORMATION structure
    ) {
        printf( "CreateProcess failed (%d).\n", GetLastError() );
		printf( "szCmdln : %S\n", szCmdln );
        return;
    }

    // Wait until child process exits.
    WaitForSingleObject( pi.hProcess, INFINITE );

    // Close process and thread handles. 
    CloseHandle( pi.hProcess );
    CloseHandle( pi.hThread );
	DevRescan();
}
