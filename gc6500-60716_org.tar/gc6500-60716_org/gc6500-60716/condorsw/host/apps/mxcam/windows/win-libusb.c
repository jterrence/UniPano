/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

/*******************************************************************************
* @(#) $Id: win-libusb.c			09-19-2011		raghav $
*******************************************************************************/
#include "win-libusb.h"
#include "libmxcam.h"

static const GUID DEVICE_INTERFACE = 
{ 0xB234AFE1, 0xACA7, 0x4668, { 0x85, 0xE9, 0x13, 0x1F, 0x9D, 0x3F, 0x1C, 0x00 } };

extern int verbose;

// list of commands which uses index field of usb setup packet.
// These commands should be redirected to device instead of interface to work on Windows platform.
int  CmdRedirectList[] = 
{
    GET_EEPROM_CONFIG,
    GET_JSON_SIZE,
	-1
};

HANDLE hDeviceHandle = INVALID_HANDLE_VALUE;
WINUSB_INTERFACE_HANDLE hWinUSBHandle = INVALID_HANDLE_VALUE;

BOOL GetDeviceHandle (GUID guidDeviceInterface, PHANDLE hDeviceHandle)
{
    BOOL bResult = TRUE;
    HDEVINFO hDeviceInfo;
    SP_DEVINFO_DATA DeviceInfoData;
    SP_DEVICE_INTERFACE_DATA deviceInterfaceData;
    PSP_DEVICE_INTERFACE_DETAIL_DATA pInterfaceDetailData = NULL;
    ULONG requiredLength=0;
    LPTSTR lpDevicePath = NULL;
    DWORD index = 0;
	size_t nLength;

    // Get information about all the installed devices for the specified
    // device interface class.
    hDeviceInfo = SetupDiGetClassDevs( &guidDeviceInterface,
										NULL, 
										NULL,
										DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);
    if (hDeviceInfo == INVALID_HANDLE_VALUE) { 
        // ERROR 
        printf("Error SetupDiGetClassDevs: %d.\n", GetLastError());
        goto done;
    }

    //Enumerate all the device interfaces in the device information set.
    DeviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);

    for (index = 0; SetupDiEnumDeviceInfo(hDeviceInfo, index, &DeviceInfoData); index++){
        //Reset for this iteration
        if (lpDevicePath){
            LocalFree(lpDevicePath);
        }
        if (pInterfaceDetailData){
            LocalFree(pInterfaceDetailData);
        }

        deviceInterfaceData.cbSize = sizeof(SP_INTERFACE_DEVICE_DATA);

        //Get information about the device interface.
        bResult = SetupDiEnumDeviceInterfaces( hDeviceInfo,
											   &DeviceInfoData,
											   &guidDeviceInterface,
											   index, 
											   &deviceInterfaceData);
        // Check if last item
        if (GetLastError () == ERROR_NO_MORE_ITEMS){
            break;
        }

        //Check for some other error
        if (!bResult) {
            printf("Error SetupDiEnumDeviceInterfaces: %d.\n", GetLastError());
            goto done;
        }

        //Interface data is returned in SP_DEVICE_INTERFACE_DETAIL_DATA
        //which we need to allocate, so we have to call this function twice.
        //First to get the size so that we know how much to allocate
        //Second, the actual call with the allocated buffer
        
        bResult = SetupDiGetDeviceInterfaceDetail( hDeviceInfo,
												&deviceInterfaceData,
												NULL, 0,
												&requiredLength,
												NULL);
        //Check for some other error
        if (!bResult) {
            if ((ERROR_INSUFFICIENT_BUFFER==GetLastError()) && (requiredLength>0)){
                //we got the size, allocate buffer
                pInterfaceDetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)LocalAlloc(LPTR, requiredLength);
                
                if (!pInterfaceDetailData) { 
                    // ERROR 
                    printf("Error allocating memory for the device detail buffer.\n");
                    goto done;
                }
            } else {
                printf("Error SetupDiEnumDeviceInterfaces: %d.\n", GetLastError());
                goto done;
            }
        }

        //get the interface detailed data
        pInterfaceDetailData->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

        //Now call it with the correct size and allocated buffer
        bResult = SetupDiGetDeviceInterfaceDetail( hDeviceInfo,
													&deviceInterfaceData,
													pInterfaceDetailData,
													requiredLength,
													NULL,
													&DeviceInfoData);        
        //Check for some other error
        if (!bResult) {
            printf("Error SetupDiGetDeviceInterfaceDetail: %d.\n", GetLastError());
            goto done;
        }

        //copy device path                
        nLength = wcslen (pInterfaceDetailData->DevicePath) + 1;  
        lpDevicePath = (TCHAR *) LocalAlloc (LPTR, nLength * sizeof(TCHAR));
        StringCchCopy(lpDevicePath, nLength, pInterfaceDetailData->DevicePath);
        lpDevicePath[nLength-1] = 0;                        
        //printf("Device path:  %S\n", lpDevicePath);
    }

    if (!lpDevicePath){
        //Error.
        printf("Error 0x%x \n", GetLastError());
        goto done;
    }

    //Open the device
    *hDeviceHandle = CreateFile ( lpDevicePath,
								GENERIC_READ | GENERIC_WRITE,
								FILE_SHARE_READ | FILE_SHARE_WRITE,
								NULL,
								OPEN_EXISTING,
								FILE_FLAG_OVERLAPPED,
								NULL);
    if (*hDeviceHandle == INVALID_HANDLE_VALUE){
        //Error.
        printf("Error 0x%x --- \n ", GetLastError());
        goto done;
    }

done:
    LocalFree(lpDevicePath);
    LocalFree(pInterfaceDetailData);    
    bResult = SetupDiDestroyDeviceInfoList(hDeviceInfo);
    
    return bResult;
}

BOOL GetWinUSBHandle(HANDLE hDeviceHandle, PWINUSB_INTERFACE_HANDLE phWinUSBHandle)
{
	BOOL bResult;

    if (hDeviceHandle == INVALID_HANDLE_VALUE){
        return FALSE;
    }

    bResult = WinUsb_Initialize(hDeviceHandle, phWinUSBHandle);
    if(!bResult){
        //Error.
        printf("WinUsb_Initialize Error %d.", GetLastError());
        return FALSE;
    }

    return bResult;
}

LPCTSTR ErrorMessage(DWORD error) 
{ 
    LPVOID lpMsgBuf;
    FormatMessage(	FORMAT_MESSAGE_ALLOCATE_BUFFER | 
					FORMAT_MESSAGE_FROM_SYSTEM |
					FORMAT_MESSAGE_IGNORE_INSERTS,
					NULL, error,
					MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
					(LPTSTR) &lpMsgBuf, 0, NULL );
    return((LPCTSTR)lpMsgBuf);
}


int libusb_init(libusb_context **ctx)
{	
	BOOL bResult = TRUE;

    bResult = GetDeviceHandle(DEVICE_INTERFACE, &hDeviceHandle);
	if(bResult){
		bResult = GetWinUSBHandle(hDeviceHandle, &hWinUSBHandle);
		*ctx = hDeviceHandle;
	}
	if(bResult==FALSE)		
		return -1;
	else
		return 0;
}

void  libusb_exit(libusb_context *ctx)
{
	if(hDeviceHandle != INVALID_HANDLE_VALUE) {
		CloseHandle(hDeviceHandle);
	}

	if(hWinUSBHandle != INVALID_HANDLE_VALUE) {
		WinUsb_Free(hWinUSBHandle);
	}

	return;
}

int  libusb_open(libusb_device *dev, libusb_device_handle **handle)
{
	BOOL bResult = TRUE;
	dev = *handle = hWinUSBHandle;
	return bResult;
}

void  libusb_close(libusb_device_handle *dev_handle)
{
 
}

libusb_device * libusb_get_device(libusb_device_handle *dev_handle)
{
	return dev_handle;
}

int libusb_detach_kernel_driver(libusb_device_handle *dev, int interface_number)
{
	return 0;
}

int libusb_attach_kernel_driver(libusb_device_handle *dev, int interface_number)
{
	return 0;
}

BOOL RedirectCmdToDevice(uint8_t bRequest)
{
	int i=0;
	BOOL bResult = FALSE;
	while(CmdRedirectList[i] != -1){
		if(CmdRedirectList[i] == bRequest){
			bResult = TRUE;
			break;
		}
		i++;
	}
	return bResult;
}

int libusb_control_transfer(libusb_device_handle *dev_handle,
	uint8_t request_type, uint8_t bRequest, uint16_t wValue, uint16_t wIndex,
	unsigned char *data, uint16_t wLength, unsigned int timeout)
{
	WINUSB_SETUP_PACKET SetupPacket ={0};
    ULONG cbSent = 0;
	BOOL bResult = TRUE;
	DWORD dwErr=0;
	int ret =0;
    LPCTSTR errMsg;

 
    //Create the setup packet
    if(request_type&LIBUSB_RECIPIENT_INTERFACE){
        SetupPacket.RequestType = request_type;
    } else if (request_type&LIBUSB_RECIPIENT_DEVICE){
        // Redirect the command to Device...so that we can use Index field of setup packet
        SetupPacket.RequestType = request_type&0xfc;
    }

    // Redirect the command to Device...  if the firmware uses the index field
    if(RedirectCmdToDevice(bRequest))
        SetupPacket.RequestType = request_type &0xfc;

    SetupPacket.Request		= bRequest;
    SetupPacket.Value		= wValue;
    SetupPacket.Index		= wIndex;
    SetupPacket.Length		= wLength;

	bResult = WinUsb_SetPipePolicy(hWinUSBHandle,0, PIPE_TRANSFER_TIMEOUT, sizeof(ULONG), &timeout);
    bResult = WinUsb_ControlTransfer(hWinUSBHandle, SetupPacket, data, wLength, &cbSent, 0);
	if(bResult==0){
		dwErr = GetLastError();

		if(verbose){
			errMsg = ErrorMessage(dwErr);
			printf("GetLastError: [0x%04x] - %S\n", dwErr, errMsg); 
			LocalFree((LPVOID)errMsg);
		}

		ret = LIBUSB_ERROR_OTHER;

		switch(dwErr){
			case ERROR_SEM_TIMEOUT :
				ret = LIBUSB_ERROR_TIMEOUT;
			break;

			case ERROR_GEN_FAILURE :
				ret = LIBUSB_ERROR_PIPE;
			break;
			default :
				ret = 1;
			break;
		}
	} else {
		ret = cbSent;
	}
	
	if(verbose)
		printf("libusb_control_transfer : 0x%02x 0x%02x r = %d [%d] cbSent = %d\n",
			SetupPacket.RequestType, SetupPacket.Request, ret,dwErr, cbSent);

	return ret;
}

int  libusb_get_device_descriptor(libusb_device *dev, struct libusb_device_descriptor *desc)
{
	ULONG LengthTransferred = 0;  
	BOOL bResult = TRUE;
	bResult = WinUsb_GetDescriptor(dev,     
									USB_DEVICE_DESCRIPTOR_TYPE,     
									0,     
									0,
									(PUCHAR)desc,     
									sizeof(struct libusb_device_descriptor),     
									&LengthTransferred); 

	return 0;
}

ssize_t  libusb_get_device_list(libusb_context *ctx, libusb_device ***list)
{ 
	*list = (libusb_device **) malloc (sizeof(libusb_device*));
	**list = (libusb_device *) malloc (sizeof(libusb_device*)*2);
	*(*list) = hWinUSBHandle;
	*(*list+1) = NULL;

	return 0;
}

void  libusb_free_device_list(libusb_device **list, int unref_devices)
{

}

int  libusb_get_configuration(libusb_device_handle *dev, int *config)
{
	return 0;
}

int libusb_set_configuration(libusb_device_handle *dev, int configuration)
{
	return 0;
}

int  libusb_get_config_descriptor(libusb_device *dev, uint8_t config_index, struct libusb_config_descriptor **config)
{
	ULONG LengthTransferred = 0;  
	struct libusb_config_descriptor *pConfDesc;
	struct libusb_interface *pInf;
	struct libusb_interface_descriptor *pAtlSetting;
	BOOL bResult = TRUE;
	int numInterface=0;
	int numEps=0;
	int i,j;
	struct libusb_interface_descriptor AltInfs[8];
	int numAltInfs=0;

	pConfDesc = malloc(sizeof(struct libusb_config_descriptor));
	*config = pConfDesc;
	memset(pConfDesc, 0, sizeof(struct libusb_config_descriptor));

	bResult = WinUsb_GetDescriptor(dev,     
									USB_CONFIGURATION_DESCRIPTOR_TYPE,     
									0,     
									0,
									(PUCHAR)pConfDesc,     
									sizeof(struct libusb_config_descriptor),     
									&LengthTransferred); 


	numInterface = pConfDesc->bNumInterfaces;
	pConfDesc->dev_interface = malloc(sizeof(struct libusb_interface)*numInterface);
	memset(pConfDesc->dev_interface, 0, sizeof(struct libusb_interface)*numInterface);

	for(i=0;i<numInterface;i++){

		pInf = &pConfDesc->dev_interface[i];

		bResult = j = 0;		
		while(1){
			bResult = WinUsb_QueryInterfaceSettings(dev, j, (PUSB_INTERFACE_DESCRIPTOR)&AltInfs[j]);
			if(!bResult)
				break;
			j++;
		}

		if(j==0) continue;

		pInf->num_altsetting = j;
		pInf->altsetting = malloc(sizeof(struct libusb_interface_descriptor)*pInf->num_altsetting);
		memcpy(pInf->altsetting,&AltInfs[0],(sizeof(struct libusb_interface_descriptor)*pInf->num_altsetting));
		pAtlSetting = pInf->altsetting;

#if 0
		for(j=0;j<pInf->num_altsetting;j++){
			
			if(pAtlSetting->bNumEndpoints > 0) {
				struct libusb_endpoint_descriptor *pEp;
				int k;
				pEp = malloc(sizeof(struct libusb_endpoint_descriptor)*(pAtlSetting->bNumEndpoints));
				memset(pEp,0,(sizeof(struct libusb_endpoint_descriptor)*(pAtlSetting->bNumEndpoints)));
				pAtlSetting->endpoint = pEp;
				for(k=0;k<pAtlSetting->bNumEndpoints;k++){
					LengthTransferred = 0;
					bResult = WinUsb_GetDescriptor(dev,     
											USB_ENDPOINT_DESCRIPTOR_TYPE,     
											k,     
											0,
											(PUCHAR)pEp,     
											sizeof(struct libusb_endpoint_descriptor),     
											&LengthTransferred); 
					pEp += sizeof(struct libusb_endpoint_descriptor);
				}
			}	

			pAtlSetting +=  sizeof(struct libusb_interface_descriptor);
		}	
#endif

	}

	return 0;
}

int  libusb_get_active_config_descriptor(libusb_device *dev, struct libusb_config_descriptor **config)
{
	libusb_get_config_descriptor(dev, 0, config);
	return 0;
}


int  libusb_get_config_descriptor_by_value(libusb_device *dev, uint8_t bConfigurationValue, struct libusb_config_descriptor **config)
{
	libusb_get_config_descriptor(dev, bConfigurationValue, config);
	return 0;
}

void libusb_free_config_descriptor(struct libusb_config_descriptor *config)
{
	int numInf,numAltSettings;
	struct libusb_interface *pInf;
	struct libusb_interface_descriptor *pAltSetting;

	if(config){
		if(config->dev_interface){
			numInf = config->bNumInterfaces;
			pInf = config->dev_interface;
			numInf--; // index starts from 0
			while(numInf>0){
				numAltSettings = config->dev_interface[numInf].num_altsetting;
				pAltSetting = config->dev_interface[numInf].altsetting;
				numAltSettings--;// index starts from 0
				while(numAltSettings > 0) {
					free(pAltSetting[numAltSettings].endpoint);
					numAltSettings--;
				}
				free(pAltSetting);
				numInf--;
			}
			free(config->dev_interface);
		}
		free(config);
	}
}

uint8_t  libusb_get_bus_number(libusb_device *dev)
{
	return 0;
}

uint8_t  libusb_get_device_address(libusb_device *dev)
{
	return 0;
}

int  libusb_release_interface(libusb_device_handle *dev, int interface_number)
{
	return 0;
}