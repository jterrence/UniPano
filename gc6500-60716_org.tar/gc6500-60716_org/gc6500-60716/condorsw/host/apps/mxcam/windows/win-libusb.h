/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

/*******************************************************************************
* @(#) $Id: devsetup.h		09-19-2011		raghav $
*******************************************************************************/

#ifndef __WIN_LIBUSB_H__
#define __WIN_LIBUSB_H__

// Include Windows headers
#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <strsafe.h>
#include <string.h>
#include <stdlib.h>

// Include WinUSB headers
#include <winusb.h>
#include <Usb100.h>
#include <Setupapi.h>

#ifdef _WIN64
  typedef __int64 ssize_t;
#else
  typedef int ssize_t;
#endif /* _WIN64 */

enum libusb_endpoint_direction {
	LIBUSB_ENDPOINT_IN	= 0x80,
	LIBUSB_ENDPOINT_OUT = 0x00
};

enum libusb_request_type {
	LIBUSB_REQUEST_TYPE_STANDARD= (0x00 << 5),
	LIBUSB_REQUEST_TYPE_CLASS	= (0x01 << 5),
	LIBUSB_REQUEST_TYPE_VENDOR	= (0x02 << 5),
	LIBUSB_REQUEST_TYPE_RESERVED= (0x03 << 5)
};

enum libusb_request_recipient {
	LIBUSB_RECIPIENT_DEVICE		= 0x00,
	LIBUSB_RECIPIENT_INTERFACE	= 0x01,
	LIBUSB_RECIPIENT_ENDPOINT	= 0x02,
	LIBUSB_RECIPIENT_OTHER		= 0x03
};

enum libusb_error {
	LIBUSB_SUCCESS				= 0,
	LIBUSB_ERROR_IO				= -1,
	LIBUSB_ERROR_INVALID_PARAM	= -2,
	LIBUSB_ERROR_ACCESS			= -3,
	LIBUSB_ERROR_NO_DEVICE		= -4,
	LIBUSB_ERROR_NOT_FOUND		= -5,
	LIBUSB_ERROR_BUSY			= -6,
	LIBUSB_ERROR_TIMEOUT		= -7,
	LIBUSB_ERROR_OVERFLOW		= -8,
	LIBUSB_ERROR_PIPE			= -9,
	LIBUSB_ERROR_INTERRUPTED	= -10,
	LIBUSB_ERROR_NO_MEM			= -11,
	LIBUSB_ERROR_NOT_SUPPORTED	= -12,
	LIBUSB_ERROR_OTHER			= -99
};

enum libusb_standard_request {
	LIBUSB_REQUEST_GET_STATUS		= 0x00,
	LIBUSB_REQUEST_CLEAR_FEATURE	= 0x01,
	LIBUSB_REQUEST_SET_FEATURE		= 0x03,
	LIBUSB_REQUEST_SET_ADDRESS		= 0x05,
	LIBUSB_REQUEST_GET_DESCRIPTOR	= 0x06,
	LIBUSB_REQUEST_SET_DESCRIPTOR	= 0x07,
	LIBUSB_REQUEST_GET_CONFIGURATION= 0x08,
	LIBUSB_REQUEST_SET_CONFIGURATION= 0x09,
	LIBUSB_REQUEST_GET_INTERFACE	= 0x0A,
	LIBUSB_REQUEST_SET_INTERFACE	= 0x0B,
	LIBUSB_REQUEST_SYNCH_FRAME		= 0x0C
};

typedef unsigned char uint8_t;
typedef signed char int8_t;
typedef unsigned short uint16_t;
typedef short int16_t;
typedef unsigned int uint32_t;
typedef int int32_t;
typedef __int64 int64_t;
typedef unsigned __int64 uint64_t;

typedef VOID libusb_context;
typedef VOID libusb_device;
typedef VOID libusb_device_handle;

struct libusb_endpoint_descriptor {
	uint8_t  bLength;
	uint8_t  bDescriptorType;
	uint8_t  bEndpointAddress;
	uint8_t  bmAttributes;
	uint16_t wMaxPacketSize;
	uint8_t  bInterval;
	uint8_t  bRefresh;
	uint8_t  bSynchAddress;
	const unsigned char *extra;
	int extra_length;
};

struct libusb_interface_descriptor {
	uint8_t  bLength;
	uint8_t  bDescriptorType;
	uint8_t  bInterfaceNumber;
	uint8_t  bAlternateSetting;
	uint8_t  bNumEndpoints;
	uint8_t  bInterfaceClass;
	uint8_t  bInterfaceSubClass;
	uint8_t  bInterfaceProtocol;
	uint8_t  iInterface;
	//const struct libusb_endpoint_descriptor *endpoint;
	struct libusb_endpoint_descriptor *endpoint;
	const unsigned char *extra;
	int extra_length;
};

struct libusb_interface {
	//const struct libusb_interface_descriptor *altsetting;
	struct libusb_interface_descriptor *altsetting;
	int num_altsetting;
};

struct libusb_config_descriptor {
	uint8_t  bLength;
	uint8_t  bDescriptorType;
	uint16_t wTotalLength;
	uint8_t  bNumInterfaces;
	uint8_t  bConfigurationValue;
	uint8_t  iConfiguration;
	uint8_t  bmAttributes;
	uint8_t  MaxPower;
	//const struct libusb_interface *inf;
	struct libusb_interface *dev_interface;
	const unsigned char *extra;
	int extra_length;
};

struct libusb_device_descriptor {
	uint8_t  bLength;
	uint8_t  bDescriptorType;
	uint16_t bcdUSB;
	uint8_t  bDeviceClass;
	uint8_t  bDeviceSubClass;
	uint8_t  bDeviceProtocol;
	uint8_t  bMaxPacketSize0;
	uint16_t idVendor;
	uint16_t idProduct;
	uint16_t bcdDevice;
	uint8_t  iManufacturer;
	uint8_t  iProduct;
	uint8_t  iSerialNumber;
	uint8_t  bNumConfigurations;
};

BOOL IsCmdForInterface(uint8_t bRequest);

BOOL GetDeviceHandle (GUID guidDeviceInterface, PHANDLE hDeviceHandle);
BOOL GetWinUSBHandle(HANDLE hDeviceHandle, PWINUSB_INTERFACE_HANDLE phWinUSBHandle);

int	 libusb_init(libusb_context **ctx);
void libusb_exit(libusb_context *ctx);
int  libusb_open(libusb_device *dev, libusb_device_handle **handle);
void libusb_close(libusb_device_handle *dev_handle);
int libusb_control_transfer(libusb_device_handle *dev_handle,
	uint8_t request_type, uint8_t bRequest, uint16_t wValue, uint16_t wIndex,
	unsigned char *data, uint16_t wLength, unsigned int timeout);
ssize_t  libusb_get_device_list(libusb_context *ctx, libusb_device ***list);
void  libusb_free_device_list(libusb_device **list, int unref_devices);
int  libusb_get_configuration(libusb_device_handle *dev, int *config);
int  libusb_get_device_descriptor(libusb_device *dev, struct libusb_device_descriptor *desc);
int  libusb_get_active_config_descriptor(libusb_device *dev, struct libusb_config_descriptor **config);
int  libusb_get_config_descriptor_by_value(libusb_device *dev, uint8_t bConfigurationValue, struct libusb_config_descriptor **config);
libusb_device * libusb_get_device(libusb_device_handle *dev_handle);
void libusb_free_config_descriptor(struct libusb_config_descriptor *config);
int libusb_detach_kernel_driver(libusb_device_handle *dev, int interface_number);
int libusb_attach_kernel_driver(libusb_device_handle *dev, int interface_number);
int libusb_set_configuration(libusb_device_handle *dev, int configuration);
uint8_t  libusb_get_bus_number(libusb_device *dev);
uint8_t  libusb_get_device_address(libusb_device *dev);
int  libusb_get_configuration(libusb_device_handle *dev, int *config);
int  libusb_release_interface(libusb_device_handle *dev, int interface_number);


#endif //__WIN_LIBUSB_H__