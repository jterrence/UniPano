/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "getopt-win.h"

extern int  opterr;		/* if error message should be printed */
extern int  optind;		/* index into parent argv vector */
extern int  optopt;		/* character checked for validity */
extern int  optreset;	/* reset getopt */
extern char	*optarg;	/* argument associated with option */

#if 0

#define __P(x) x
#define _DIAGASSERT(x) assert(x)

int getopt_internal __P((int, char * const *, const char *));


#define	BADCH	(int)'?'
#define	BADARG	(int)':'
#define	EMSG	""

/*
 * getopt -- Parse argc/argv argument vector.
 */
int getopt_internal(int nargc, char * const *nargv, const char *ostr)
{
	static char *place = EMSG;  /* option letter processing */
	char *oli;					/* option letter list index */

	_DIAGASSERT(nargv != NULL);
	_DIAGASSERT(ostr != NULL);

	if (optreset || !*place) {	 /* update scanning pointer */
		optreset = 0;
		if (optind >= nargc || *(place = nargv[optind]) != '-') {
			place = EMSG;
			return (-1);
		}
		if (place[1] && *++place == '-') {	/* found "--" */
			/* ++optind; */
			place = EMSG;
			return (-2);
		}
	}	/* option letter okay? */

	if ((optopt = (int)*place++) == (int)':' ||
	    !(oli = strchr(ostr, optopt))) {

		/* if the user didn't specify '-' as an option,
		 * assume it means -1.
		 */
		if (optopt == (int)'-')
			return (-1);
		if (!*place)
			++optind;
		if (opterr && *ostr != ':')
			(void)fprintf(stderr, "Error: illegal option -- %c\n", optopt);
		return (BADCH);
	}
	if (*++oli != ':') {			/* don't need argument */
		optarg = NULL;
		if (!*place)
			++optind;
	} else {				/* need an argument */
		if (*place)			/* no white space */
			optarg = place;
		else if (nargc <= ++optind) {	/* no arg */
			place = EMSG;
			if ((opterr) && (*ostr != ':'))
				(void)fprintf(stderr, "Ereor: option requires an argument -- %c\n", optopt);
			return (BADARG);
		} else				/* white space */
			optarg = nargv[optind];
		place = EMSG;
		++optind;
	}
	return (optopt); /* dump back option letter */
}

/*
 *getopt_long -- Parse argc/argv argument vector.
 */
int
getopt_long(nargc, nargv, options, long_options, index)
	int nargc;
	char ** nargv;
	char * options;
	struct option * long_options;
	int * index;
{
	int retval;

	_DIAGASSERT(nargv != NULL);
	_DIAGASSERT(options != NULL);
	_DIAGASSERT(long_options != NULL);
	/* index may be NULL */

	if ((retval = getopt_internal(nargc, nargv, options)) == -2) {
		char *current_argv = nargv[optind++] + 2, *has_equal;
		int i, current_argv_len, match = -1;

		if (*current_argv == '\0') {
			return(-1);
		}
		if ((has_equal = strchr(current_argv, '=')) != NULL) {
			current_argv_len = (int)(has_equal - current_argv);
			has_equal++;
		} else
			current_argv_len = (int)strlen(current_argv);

		for (i = 0; long_options[i].name; i++) { 
			if (strncmp(current_argv, long_options[i].name, current_argv_len))
				continue;

			if (strlen(long_options[i].name) == (unsigned)current_argv_len) { 
				match = i;
				break;
			}
			if (match == -1)
				match = i;
		}
		if (match != -1) {
			if (long_options[match].has_arg == required_argument ||
			    long_options[match].has_arg == optional_argument) {
				if (has_equal)
					optarg = has_equal;
				else
					optarg = nargv[optind++];
			}
			if ((long_options[match].has_arg == required_argument)
			    && (optarg == NULL)) {
				/*
				 * Missing argument, leading :
				 * indicates no error should be generated
				 */
				if ((opterr) && (*options != ':'))
					fprintf(stderr, "Error: option requires an argument -- %s\n", current_argv);
				return (BADARG);
			}
		} else { /* No matching argument */
			if ((opterr) && (*options != ':'))
				fprintf(stderr, "Error: illegal option -- %s\n", current_argv);
			return (BADCH);
		}
		if (long_options[match].flag) {
			*long_options[match].flag = long_options[match].val;
			retval = 0;
		} else 
			retval = long_options[match].val;
		if (index)
			*index = match;
	}
	return(retval);
}

#else

int move_arg(int argc,char * argv[], int fIdx, int tIdx)
{
	char szArg[256];
	int i;
	if((fIdx >= argc)||(tIdx >= argc)||(fIdx <= 0)||(tIdx <=0)) {
		printf("move_arg: Invalid param !!! \n");	
		return -1;
	}
	strcpy_s(szArg,256,argv[fIdx]);
	for(i=fIdx-1;i>=tIdx;i--){
		strcpy_s(argv[i+1],(256-i),argv[i]);
	}
	strcpy_s(argv[tIdx],256,szArg);
	return 0;
}

int getopt_long(int argc, 
					char * argv[], 
					char *optstring,
					struct option *longopts, 
					int *longindex) 
{
	char szOption[256];
	char szParam[256];
	struct option *pOpts = (struct option *)longopts;
	static int lastofs=1;
	int lastidx=0;
	int bFound = 0;
	int idx = 0,OptIdx=0, ret=-1,j=0,err=0,fParam=0;
	*longindex = 0;

	optarg = NULL;
	lastidx = optind;

	if(optind > 1){
		if((argv[optind-1][0] == '-')&&(argv[optind-1][1] == '-')) {
			move_arg(argc,argv,(optind-1),lastofs);
			lastofs++;
			optind = lastofs;
		} else if((argv[optind-2][0] == '-')&&(argv[optind-2][1] == '-')) {
			move_arg(argc,argv,(optind-2),lastofs);
			lastofs++;
			move_arg(argc,argv,(optind-1),lastofs);
			lastofs++;
			optind = lastofs;
		}
	}

	if (lastidx>=argc || !argv[lastidx]){
		//optind = argc-1;
		return ret;
	}

	while((lastidx<argc)&&(!bFound)){
		if((argv[lastidx][0]=='-')&&(argv[lastidx][1]=='-')){
			strcpy_s(szOption,sizeof(szOption),&(argv[lastidx][2]));
			idx = 0;
			while(longopts[idx].name){
				if(strcmp(longopts[idx].name,szOption)==0){
					if(longopts[idx].has_arg==1){
						lastidx++;
						optarg = _strdup(argv[lastidx]);
						if(lastidx>=argc){
							err = 1;
						} else {
							strcpy_s(szParam,sizeof(szParam),&(argv[lastidx-1][0]));					
							while(pOpts[j].name){
								if(strcmp(pOpts[j].name,szParam)==0){
									err = 1;
									break;
								}
								j++;
							}
						}

						if(err){
							printf("Error: option '--%s' requires an argument \n",szOption);
							lastidx--;
							return '?';
						}
					}

					*longindex = idx;
					//optind = lastidx;
					ret = (int)pOpts[idx].val;
					bFound = 1;
					break;
				}
				idx++;
			}
		}
		lastidx++;
	}

	if(bFound==0)
		lastidx--;
	else
		optind = lastidx;

	return ret;
}



#endif
