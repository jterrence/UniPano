#! /usr/bin/env python
"""
Module to generate circle PNG image for input to ewarp pro tool.
The settings are read from configuration file as input.
"""
import sys
import getopt
import os
import math
import configparser
import array
try:
    from PIL import Image
except ImportError:
    print("Python Image Library is missing... quitting")
    sys.exit(1)

import common.traces as logger
from common.progress import draw_progress_bar


def make_png(buffer, filename, width, height):
    image = Image.new('RGB', (width, height))
    pixels = image.load()

    for j in range(0, height):
        for i in range(0, width):
            offset = j * width + i
            y = buffer[offset]
            u = buffer[offset]
            v = buffer[offset]
            pixels[i, j] = y, u, v

    image.save(filename)
    return


def update_chroma(y, u, v, width, height):
    i = 0
    for j in range(0, width * height):
        if (j % 2 == 0) and ((j % (width * 2)) < width):
            pix = (y[j] + y[j + width]) / 2
            u[i] = int(pix)
            v[i] = int(pix)
            i += 1


def calculate_alpha(radius, angle, px_min_cx, py_min_cy, alpha_w, alpha_i, alpha_o):
    norm_angle = angle / 90.0
    ri = (1.0 - norm_angle) * (radius - alpha_w) + norm_angle * (radius - alpha_w)
    ro = (1.0 - norm_angle) * radius + norm_angle * radius
    r = math.sqrt((px_min_cx ** 2) + (py_min_cy ** 2))

    if r > ro:
        return int(255.0 * alpha_o)
    elif r < ri:
        return int(255.0 * alpha_i)
    else:
        dist = (r - ri) / (ro - ri)
        return int(255.0 * ((1 - dist) * alpha_i + dist * alpha_o))


def generate_circle(buffer, width, height, radius, center, gradient_width, gradient_start, gradient_end):
    r2pi = 180.0 / math.acos(-1.0)
    try:
        cx = int(center[0])
        cy = int(center[1])
        print(cx, cy, width, height)
    except ValueError:
        print('invalid center values')
        return

    print('starting circle generation...')
    draw_progress_bar(0)
    # Quadrant 0
    py = [i for i in range(0, cy + 1)]
    px = [i for i in range(cx + 1, width)]
    py_min_cy = -cy

    for i in py:
        px_min_cx = 1
        addr = (i * width) + (cx + 1)
        for j in px:
            angle = r2pi * math.atan2(-py_min_cy, px_min_cx)
            pix = calculate_alpha(radius, angle, px_min_cx, py_min_cy, gradient_width, gradient_start, gradient_end)
            buffer[addr] = pix
            px_min_cx += 1
            addr += 1

        py_min_cy += 1

    draw_progress_bar(0.25)
    # Quadrant 1
    py = [i for i in range(0, cy)]
    px = [i for i in range(0, cx + 1)]
    py_min_cy = -cy
    j = 0
    for i in py:
        px_min_cx = -cx
        addr = i * width
        for j in px:
            angle = r2pi * math.atan2(-py_min_cy, -px_min_cx)
            pix = calculate_alpha(radius, angle, px_min_cx, py_min_cy, gradient_width, gradient_start, gradient_end)
            buffer[addr] = pix
            px_min_cx += 1
            addr += 1

        py_min_cy += 1

    draw_progress_bar(0.5)
    # Quadrant 2
    py = [i for i in range(cy, height)]
    px = [i for i in range(0, cx)]
    py_min_cy = 0

    for i in py:
        px_min_cx = -cx
        addr = i * width
        for j in px:
            angle = r2pi * math.atan2(py_min_cy, -px_min_cx)
            pix = calculate_alpha(radius, angle, px_min_cx, py_min_cy, gradient_width, gradient_start, gradient_end)
            buffer[addr] = pix
            px_min_cx += 1
            addr += 1

        py_min_cy += 1

    draw_progress_bar(0.75)
    # Quadrant 3
    py = [i for i in range(cy, height)]
    px = [i for i in range(cx, width)]
    py_min_cy = 0

    for i in py:
        px_min_cx = 0
        addr = i * width + cx
        for j in px:
            angle = r2pi * math.atan2(py_min_cy, px_min_cx)
            pix = calculate_alpha(radius, angle, px_min_cx, py_min_cy, gradient_width, gradient_start, gradient_end)
            buffer[addr] = pix
            px_min_cx += 1
            addr += 1

        py_min_cy += 1

    draw_progress_bar(1.0)
    print('\ncircle generation complete...{} bytes'.format(buffer.__len__()))


def main(argv):
    """

    :rtype: None
    """
    try:
        opts, args = getopt.getopt(argv, "hi:", ["inputfile="])
    except getopt.GetoptError:
        print('cicle.py -i <path to cfg file>')
        return
    for opt, arg in opts:
        if opt == '-h':
            print('circle.py -i <path to cfg file>')
            return
        elif opt in ("-i", "--input"):
            inputfile = arg
            if not inputfile.lower().endswith('.cfg'):
                print('Input must be a {0} cfg file'.format(inputfile))
                return
                # print('Input file is ', inputfile)

    try:
        size = os.path.getsize(inputfile)
        if size == 0:
            print('ERROR: cfg file is empty!!\n quitting...')
            return
    except FileNotFoundError:
        print('ERROR: Invalid file path {0}!!\n quitting'.format(inputfile))
        return

    log = logger.Tracer(mode=1)

    print('reading {}...'.format(inputfile), end='')

    config = configparser.ConfigParser()
    config.read(inputfile)
    print(config.sections())

    if 'RESOLUTIONS' not in config:
        print('ERROR; Mandatory section: RESOLUTIONS is missing\n quitting')
        return

    try:
        width = int(config['RESOLUTIONS']['WIDTH'])
        height = int(config['RESOLUTIONS']['HEIGHT'])
    except KeyError:
        print('invalid or missing key\nquitting...')
        return
    except ValueError:
        print('invalid value\nquitting...')
        return

    y420 = array.array('B')
    u420 = array.array('B')
    v420 = array.array('B')
    print('done')

    print('generating empty yuv buffer...', end='')

    for i in range(0, height):
        for j in range(0, width):
            y420.append(0)

    for i in range(0, int(height / 2)):
        for j in range(0, int(width / 2)):
            u420.append(0)
            v420.append(0)

    print('done\n')

    if 'LEFT CIRCLE' in config:
        try:
            filename = config['LEFT CIRCLE']['YUV FILE PATH']
            pngname = config['LEFT CIRCLE']['PNG FILE PATH']
            center = config['LEFT CIRCLE']['CENTER'].strip().split(',')
            radius = float(config['LEFT CIRCLE']['RADIUS'])
            gradient_width = float(config['LEFT CIRCLE']['GRADIENT WIDTH'])
            gradient_start = float(config['LEFT CIRCLE']['GRADIENT START'])
            gradient_end = float(config['LEFT CIRCLE']['GRADIENT END'])
        except KeyError:
            print('invalid or missing key\nquitting...')
            return
        except ValueError:
            print('invalid value\nquitting...')
            return

        if radius > height / 2:
            print('radius cannot exceed height/2 = {0}'.format(height / 2))
            return

        generate_circle(y420, width, height, radius, center, gradient_width, gradient_start, gradient_end)
        update_chroma(y420, u420, v420, width, height)

        print('saving {}...'.format(filename), end='')
        with open(filename, 'wb') as fout:
            fout.write(y420)
            fout.write(u420)
            fout.write(v420)
        print('done')

        print('saving {}...'.format(pngname), end='')
        make_png(y420, pngname, width, height)
        print('done')

    if 'RIGHT CIRCLE' in config:
        try:
            filename = config['RIGHT CIRCLE']['YUV FILE PATH']
            pngname = config['RIGHT CIRCLE']['PNG FILE PATH']
            center = config['RIGHT CIRCLE']['CENTER'].strip().split(',')
            radius = float(config['RIGHT CIRCLE']['RADIUS'])
            gradient_width = float(config['RIGHT CIRCLE']['GRADIENT WIDTH'])
            gradient_start = float(config['RIGHT CIRCLE']['GRADIENT START'])
            gradient_end = float(config['RIGHT CIRCLE']['GRADIENT END'])
        except KeyError:
            print('invalid or missing key\nquitting...')
            return
        except ValueError:
            print('invalid value\nquitting...')
            return

        generate_circle(y420, width, height, radius, center, gradient_width, gradient_start, gradient_end)
        update_chroma(y420, u420, v420, width, height)

        print('saving {}...'.format(filename), end='')
        with open(filename, 'wb') as fout:
            fout.write(y420)
            fout.write(u420)
            fout.write(v420)
        print('done')

        print('saving {}...'.format(pngname), end='')
        make_png(y420, pngname, width, height)
        print('done')


if __name__ == "__main__":
    # print(sys.version)
    print(os.name)
    print(sys.platform)
    if sys.version_info >= (3, 5):
        try:
            main(sys.argv[1:])
            if os.name == 'nt':
                key = input('Launch ewarp pro: (yes/no)? ')
                if key == 'yes':
                    import subprocess
                    print('\nStarting ewarp...')
                    subprocess.check_call(["C:\\GEOSemi\\DTK-eWARPPro\\eWARPPro\\eWARPPro.exe"], shell=True)
                elif key == 'no':
                    print('Quitting')
                else:
                    print('Unknown Input: Quitting')


        except KeyboardInterrupt:
            print('\nProgram terminated')
        except OSError as e:
            print(e)
            print('Quitting')

    else:
        print('Minimum python version must be 3.5.')
        print('Please update the comment #!/usr/bin/python to python3.5 installation and retry')
