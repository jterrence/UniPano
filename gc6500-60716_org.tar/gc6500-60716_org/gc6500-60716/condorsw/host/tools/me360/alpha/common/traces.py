class Tracer:
    def __init__(self, mode):
        self.enabled = mode
        print('traces init with log level {0}'.format(str(mode)))

    def __call__(self, *args, **kwargs):
        def warp(*args, **kwargs):
            if self.enabled is True:
                print(*args)
        return warp
