#! /usr/bin/env python
"""Module to split a YUV 420 planar file into Y and UV components"""
import sys
import getopt
import gzip
import os


def split_yuv(argv):
    """

    :rtype: None
    """
    inputfile = 'alpha.yuv'
    yfile = 'alpha.y.gz'
    uvfile = 'alpha.uv.gz'
    try:
        opts, args = getopt.getopt(argv, "hi:y:u:", ["ifile=", "yfile=", "uvfile"])
    except getopt.GetoptError:
        print('yuvsplit.py -i <inputfile> -y <yfile> -u <uvfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('yuvsplit.py -i <inputfile> -y <yfile> -u <uvfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
            if not inputfile.lower().endswith('.yuv'):
                print('Input must be a {0} yuv file'.format(inputfile))
                sys.exit(2)
            # print('Input file is ', inputfile)
        elif opt in ("-y", "--yfile"):
            yfile = arg
            print('y file is ', yfile)
        elif opt in ("-u", "--uvfile"):
            uvfile = arg
            print('uv file is', arg)

    try:
        size = os.path.getsize(inputfile)
    except FileNotFoundError:
        print('ERROR: Invalid file path {0}...quitting'.format(inputfile))
        sys.exit(2)

    uvsize = size / 3
    ysize = size - uvsize

    print('total file size {0} MB'.format(size / (1024 * 1024)))
    print('luma size {0} MB'.format(ysize / (1024 * 1024)))
    print('chroma size {0} MB'.format(uvsize / (1024 * 1024)))

    # Open input yuv420 file
    with open(inputfile, "rb") as fo:
        ystr = fo.read(int(ysize))
        uvstr = fo.read(int(uvsize))

    # create compressed luma file
    with open(yfile, "wb+") as fy:
        fy.write(gzip.compress(ystr))

    # create compressed chroma file
    with open(uvfile, "wb+") as fuv:
        fuv.write(gzip.compress(uvstr))

    # sanity checks on the file size to ensure it can fit into SNOR.
    size = os.path.getsize(yfile)
    if size > 32 * 1024:
        print('ERROR: Compressed file {0} KB is higher than expected {1}'.format(int(size / 1024), '32KB'))
        os.remove(yfile)
    size = os.path.getsize(uvfile)
    if size > 32 * 1024:
        print('ERROR: Compressed file {0} KB is higher than expected {1}'.format(int(size / 1024), '32KB'))
        os.remove(uvfile)


if __name__ == "__main__":
    # print (sys.version)
    if sys.version_info >= (3, 5):
        split_yuv(sys.argv[1:])
    else:
        print('Minimum python version must be 3.5.')
        print('Please update the comment #!/usr/bin/python to python3.5 installation and retry')


