#! /usr/bin/env python
import gzip
import sys
import getopt
import os
import configparser
import time
import array
import common.traces as logger
from common.progress import draw_progress_bar
try:
    from PIL import Image
except ImportError:
    print("Python Image Library is missing... quitting")
    sys.exit(1)


def clip(px):
    if px < 0:
        return 0
    if px > 255:
        return 255
    return int(round(px))


def scan(xmin, ymin, xmax, ymax, pixels):
    # clean data pass 1
    r, g, b = (0, 0, 0)
    for j in range(ymin, ymax):
        for i in range(xmin, xmax):
            if pixels[i, j] != (int(r), int(g), int(b)):
                pixels[i, j] = int(r), int(g), int(b)
            draw_progress_bar((j - ymin) / (ymax - ymin))

    draw_progress_bar(1.0)
    print(' ')
    return


def main(argv):
    """

    :rtype: None
    """
    try:
        opts, args = getopt.getopt(argv, "hi:", ["inputfile="])
    except getopt.GetoptError:
        print('warp.py -i <path to .cfg file>')
        return

    for opt, arg in opts:
        if opt == '-h':
            print('warp.py -i <path to cfg file>')
            return
        elif opt in ("-i", "--input"):
            inputfile = arg
            if not inputfile.lower().endswith('.cfg'):
                print('Input must be a {0} cfg file'.format(inputfile))
                return
            print('Input file is ', inputfile)

    try:
        size = os.path.getsize(inputfile)
        if size == 0:
            print('ERROR: cfg file is empty!!\n quitting...')
            return
    except FileNotFoundError:
        print('ERROR: Invalid file path {0}!!\n quitting'.format(inputfile))
        return

    log = logger.Tracer(mode=1)

    print('reading configuration {}...'.format(inputfile), end='')
    config = configparser.ConfigParser()
    config.read(inputfile)

    if 'INPUT' not in config:
        print('ERROR; Mandatory section: INPUT is missing\n quitting')
        return

    if 'OUTPUT' not in config:
        print('ERROR; Mandatory section: OUTPUT is missing\n quitting')
        return

    try:
        width = int(config['OUTPUT']['WIDTH'])
        height = int(config['OUTPUT']['HEIGHT'])
        filename = config['INPUT']['FILE_PATH'].strip()
        output = config['OUTPUT']['FILE_PATH'].strip()
        stride = int(config['OUTPUT']['STRIDE'])
        offset = config['REGION']['OFFSET'].strip().split(',')
        region = config['REGION']['SIZE'].strip().split('x')
    except KeyError:
        print('invalid or missing key\nquitting...')
        return
    except ValueError:
        print('invalid value\nquitting...')
        return

    print('done')
    print('generating raw buffer...')

    out = Image.new('RGB', (stride, height))
    data = out.load()

    r, g, b = (0, 0, 0)
    for j in range(0, height):
        for i in range(0, stride):
            data[i, j] = int(r), int(g), int(b)
        draw_progress_bar(j/height)

    draw_progress_bar(1.0)
    print('\ndone')

    image = Image.open(filename, 'r')
    tmp = image.copy()
    image_width = image.size[0]
    image_height = image.size[1]
    print(image_width, image_height)
    # resize image
    size = width, height
    tmp2 = tmp.resize(size, Image.ANTIALIAS)
    # tmp2.show()
    pixels = tmp2.load()

    xscale = width/image_width
    yscale = height/image_height

    print('scaling factors: {:f}, {:f}'.format(xscale, yscale))
    xmin = int(int(offset[0])*xscale)
    ymin = int(int(offset[1])*yscale)

    xmax = xmin + int(int(region[0])*xscale)
    ymax = ymin + int(int(region[1])*yscale)

    print('starting pixel scan')
    # pass 3
    print(str(xmin)+','+str(ymin)+' '+str(xmax-xmin)+'x'+str(ymax-ymin))
    start = time.time()
    scan(xmin, ymin, xmax, ymax, pixels)
    end = time.time()
    print('scan: complete in: ' + str(end - start)[:5]+' seconds')

    # copy data
    for j in range(0, height):
        for i in range(0, width):
            data[i, j] = pixels[i, j]

    y444 = array.array('B')
    u444 = array.array('B')
    v444 = array.array('B')

    print('generating luma...')

    for j in range(0, height):
        for i in range(0, stride):
            y, u, v = data[i, j]
            y444.append(y)
            u444.append(u)
            v444.append(v)
        draw_progress_bar(j/height)

    draw_progress_bar(1.0)

    u420 = array.array('B')
    v420 = array.array('B')

    k = 0
    print('\ngenerating chroma...', end='')
    for j in range(0, stride*height):
        if (j % 2 == 0) and ((j % (stride * 2)) < stride):
            u420.append(clip((u444[j] + u444[j + stride])/2))
            v420.append(clip((v444[j] + v444[j + stride])/2))
            k += 1
    print('done')
    print('saving file {}...'.format(output), end='')
    with open(output, 'wb') as fout:
        fout.write(y444)
        fout.write(u420)
        fout.write(v420)

    print('done')
    print('saving file {}...'.format(output+'.gz'), end='')
    with open(output, 'rb') as fout:
        with open(output+'.gz', 'wb') as fgz:
            buffer = fout.read(int(stride*height*3/2))
            fgz.write(gzip.compress(buffer))
    print('done')
    print('generating preview...')

    # show image
    out.show()


if __name__ == '__main__':
    # print(sys.version)
    if sys.version_info >= (3, 5):
        try:
            main(sys.argv[1:])
        except KeyboardInterrupt:
            print('\nProgram terminated')
        except OSError as e:
            print(e)
            print('Quitting...')
    else:
        print('Minimum python version must be 3.5.')
        print('Please update the comment #!/usr/bin/python to python3.5 installation and retry')
