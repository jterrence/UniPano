/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

/************************************************************************************
 * @(#) $Header: http://svllsvn01.local.geosemi.com/repos/swrepos/trunk/condorsw/host/tools/querydump/usb_query_impl.cpp 60673 2017-02-22 09:19:43Z sagar.gupta $
 ************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <math.h>
#include "qstatus.h"
#include "usb_mem_interface.h"
#include "usb_query_impl.h"
//#include "uvc_stats.h"


#define XTENSA_PARTITION_ID        127
#define QMM_PARTITION_ID        125
#define PTR_TO_GLOBAL_POINTER_BLOCK (0xffc)
// number of attempts to obtain the GPB and command block addresses
#define ERROR_TOLERANCE_LEVEL   (200)
/// \brief Partition ID to use for accessing codec memory
#define CODEC_PARTITION         XTENSA_PARTITION_ID
// GPB pointer should be in the noncacheable memory section
#define NONCACHEABLE_MEMORY_BASE (0x40000000)

#define VINSTATUS_BLOCKID0    (0x41)
#define VINSTATUS_BLOCKID1    (0x49)
#define VINSTATUS_ADDR        (0x94)
#define VINSTATUS_SIZE        (4)
#define VINSTATUS_FIELD_MASK  (1 << 0xf)
#define VINSTATUS_CLK_MASK    (1 << 0xe)
#define VINSTATUS_VLINES_MASK (0x1fff)
#define VINSTATUS_HWIDTH      (0x10)
#define VINSTATUS_HWIDTH_MASK (0x1fff)
#define VINSTATUS_LOCK_MASK   (1 << 0xd)



typedef struct
{
    char                name[MAX_CTRLOBJ_NAME_LEN];
    int                 type;
    unsigned char       *statBlock;
    int                 prevNumFrames;
    struct timeval      prevSampleTime;
}USB_QUERY_OBJ_HANDLE;
typedef struct
{
    int id;
    unsigned int loadAverage;
    unsigned int interruptsPerSecond;
    unsigned int eventsPerSecond;
    char eventThreadState[64];
    SYSTEM_CONTROL_STATUS status;
} SYSTEM_CONTROL_QUERY_DATA;
typedef struct
{
   int id;
   AVCENC_STATUS *status;
   // Delta between frames of last time period in ms
   int minDelta;
   int maxDelta;
   int avgDelta;
   // Current bitrate
   float curBitrate;
} AVCENC_QUERY_DATA;
typedef struct
{
    int id;
    VPP_STATUS status;
} NVPP_QUERY_DATA;
typedef struct
{
    int id;
    VPP_STATUS status;
} LVPP_QUERY_DATA;

typedef struct
{
    int id;
    VPP_STATUS status;
} VCAP_QUERY_DATA;

typedef struct {
    int id;
    JPEGENC_STATUS status;
} JPEGENC_QUERY_DATA;
typedef struct {
    int id;
    AUDFLTR_STATUS status;
} AUDFLTR_QUERY_DATA;

typedef struct
{
   int id;
   AUDIOENC_STATUS status;
   // Delta between frames of last time period in ms
   int minDelta;
   int maxDelta;
   int avgDelta;
   // Current bitrate
   float curBitrate;
} AUDENC_QUERY_DATA;

typedef struct {
    int id;
    ISP_STATUS status;
} ISP_QUERY_DATA;

typedef struct {
    int id;
    DEWARP_STATUS status;
} DEWARP_QUERY_DATA;

typedef struct
{
    int                         globalPointerBlockAddr;
    SYSTEM_CONTROL_STATUS       *systemControlStatus;
    GLOBAL_POINTER_BLOCK        gpb;
    void*                       host_h;
    USB_QUERY_OBJ_HANDLE        attachedObjs[MAX_CTRLOBJ_ID];
    int                         numObjects;
    SYSTEM_CONTROL_QUERY_DATA   sysctl_q_data;
    LVPP_QUERY_DATA             lvpp_q_data;
    VCAP_QUERY_DATA             vcap_q_data;
    NVPP_QUERY_DATA             nvpp_q_data;
    AVCENC_QUERY_DATA           avcenc_q_data;
    JPEGENC_QUERY_DATA          jpgenc_q_data;
    AUDFLTR_QUERY_DATA          aflt_q_data;
    AUDENC_QUERY_DATA           aenc_q_data;
    ISP_QUERY_DATA              isp_q_data;
    DEWARP_QUERY_DATA           dewarp_q_data;
    FILE*                       p_file;
} USB_QUERY_HANDLE;


static int usb_query_set_gpb(void* ptr, unsigned int addr );
#if 0
static GLOBAL_POINTER_BLOCK *usb_query_get_gpb( void* ptr);
#endif
static int usb_query_get_attached_object(USB_QUERY_HANDLE* query_h, int i,
                                       char * name, CONTROLOBJ_DESC *pObjDesc);
static void usb_query_get_attached_objects(USB_QUERY_HANDLE* query_h);
static void usb_query_attached_objects(USB_QUERY_HANDLE* query_h);
static void usb_query_sysctl(USB_QUERY_HANDLE* query_h);
static void usb_query_nvpp(USB_QUERY_HANDLE* query_h, int id);
static void usb_query_audfltr(USB_QUERY_HANDLE* query_h, int id);
static void usb_query_isp(USB_QUERY_HANDLE* query_h, int id);
static void usb_query_audenc(USB_QUERY_HANDLE* query_h, int id);
static void usb_query_lvpp(USB_QUERY_HANDLE* query_h, int id);
static void usb_query_vcap(USB_QUERY_HANDLE* query_h, int id);
static void usb_query_avcenc(USB_QUERY_HANDLE* query_h, int id);
static void usb_query_jpegenc(USB_QUERY_HANDLE* query_h, int id);
static void usb_query_dewarp(USB_QUERY_HANDLE* query_h, int id);

#define QPRINTF(...) fprintf(query_h->p_file,__VA_ARGS__)

static int usb_query_set_gpb(void* ptr, unsigned int addr )
{
    USB_QUERY_HANDLE* query_h = (USB_QUERY_HANDLE*)ptr;
    if(NULL == query_h)
    {
        fprintf(stderr,"%s ERROR: Invalid Handle. \n",__FUNCTION__);
        return -1;
    }

    // set the gpb
    query_h->globalPointerBlockAddr = addr;

    // read the block and get the contents
    if (usb_mem_interface_read_words(query_h->host_h, CODEC_PARTITION,
            query_h->globalPointerBlockAddr, (long *)&(query_h->gpb),
              sizeof(GLOBAL_POINTER_BLOCK)/4) < 0)
    {
        fprintf(stderr,"ERROR: Failed to get GLOBAL_POINTER_BLOCK .\n");
        return -1;
    }
    query_h->systemControlStatus = (SYSTEM_CONTROL_STATUS *)(query_h->gpb.systemControlStatus);


    return 0;
}

#if 0
static GLOBAL_POINTER_BLOCK *usb_query_get_gpb( void* ptr)
{
    USB_QUERY_HANDLE* query_h = (USB_QUERY_HANDLE*)ptr;

    return(&(query_h->gpb));
}
#endif

static int usb_query_get_attached_object(USB_QUERY_HANDLE* query_h, int i,
                                       char * name, CONTROLOBJ_DESC *pObjDesc)
{
    GLOBAL_POINTER_BLOCK *gpb;
    CONTROLOBJ_DESC * pCtrlObjects;

    if(NULL == query_h)
    {
        fprintf(stderr,"%s ERROR: Invalid Handle. \n",__FUNCTION__);
        return -1;
    }

    // get global pointer and set object array
    gpb = (GLOBAL_POINTER_BLOCK *)&query_h->gpb;
    pCtrlObjects = (CONTROLOBJ_DESC *)gpb->ctrlObjects;

    // read the object id array
    if(usb_mem_interface_read_words(query_h->host_h, CODEC_PARTITION,
#ifdef ARCH_x64
            (unsigned long)((unsigned int*)pCtrlObjects + i), (long *) pObjDesc,
#else
            (unsigned long)(pCtrlObjects + i),(long *)pObjDesc,
#endif
            sizeof(CONTROLOBJ_DESC)/4) < 0)
    {
        fprintf(stderr,"ERROR: Failed to get attached object .\n");
        return 0;
    }

    // create object
    if (pObjDesc->obj != 0 && SYSCTRL_CTRLOBJ_TYPE != pObjDesc->type) {
#ifdef ARCH_x64
        CONTROLOBJ_DESC * pObj = (CONTROLOBJ_DESC *)((unsigned int *)pCtrlObjects + i);
#else
        CONTROLOBJ_DESC * pObj = pCtrlObjects + i;
#endif
        // copy name
        if(usb_mem_interface_read_bytes(query_h->host_h, CODEC_PARTITION,
                (unsigned long)(pObj->name),name, MAX_CTRLOBJ_NAME_LEN) < 0)
        {
            fprintf(stderr,"%s ERROR: Failed to get attached object "
                    "name .\n", __FUNCTION__);
            return 0;
        }
        return 1;
    }
    return 0;
}

static void usb_query_get_attached_objects(USB_QUERY_HANDLE* query_h)
{
    int i=0;

    if(NULL == query_h)
    {
        fprintf(stderr,"%s ERROR: Invalid Handle. \n",__FUNCTION__);
        return;
    }
    for (i = 0;i < MAX_CTRLOBJ_ID;i++) {
        char name[MAX_CTRLOBJ_NAME_LEN];
        CONTROLOBJ_DESC objDesc;
        int rval;

        // get name and object description
        rval = usb_query_get_attached_object(query_h, i, name, &objDesc);

        if (rval != 0) {
            switch (objDesc.type) {
                case LVPP_CTRLOBJ_TYPE:
                case VCAP_CTRLOBJ_TYPE:
                case NVPP_CTRLOBJ_TYPE:
                case AVOC_CTRLOBJ_TYPE:
                case AVCENC_CTRLOBJ_TYPE:
                case AVSN_CTRLOBJ_TYPE:
                case JPEGENC_CTRLOBJ_TYPE:
                case AUDFLTR_CTRLOBJ_TYPE:
                case ISP_CTRLOBJ_TYPE:
                case AUDENC_CTRLOBJ_TYPE:
                case DEWARP_CTRLOBJ_TYPE:
                    strcpy(query_h->attachedObjs[query_h->numObjects].name,name);
                    query_h->attachedObjs[query_h->numObjects].type = objDesc.type;
                    query_h->attachedObjs[query_h->numObjects].statBlock = (unsigned char *)objDesc.statBlock;
                    query_h->attachedObjs[query_h->numObjects].prevNumFrames = 0;
                    gettimeofday(&query_h->attachedObjs[query_h->numObjects].prevSampleTime, NULL);
                    QPRINTF("%s is attached type  = %d \n",
                            query_h->attachedObjs[query_h->numObjects].name,
                            query_h->attachedObjs[query_h->numObjects].type);
                    query_h->numObjects++;
                    break;
                default:
                    break;
            }
        }
    }
    QPRINTF("\n");
}

void* usb_query_init(char* filename)
{
    USB_QUERY_HANDLE* query_h = NULL;
    unsigned int gpb;
    int errTolerance;
    errTolerance = ERROR_TOLERANCE_LEVEL;

    query_h =  (USB_QUERY_HANDLE*)malloc(sizeof(USB_QUERY_HANDLE));
    if(NULL == query_h)
    {
        fprintf(stderr,"%s ERROR: Unable to allocate Memory "
                "for the Handle.\n",__FUNCTION__);
        return NULL;
    }

    query_h->p_file = fopen(filename,"w");
    if(NULL==query_h->p_file)
    {
        printf("Error opening the file for Query Dump.... \n "
                "Outputting to the Console \n");
        query_h->p_file = stdout;
    }

    query_h->numObjects = 0;
    query_h->host_h = usb_mem_interface_open();
    if(NULL==query_h->host_h){
        fprintf(stderr,"Error in opening the USB Host memory Interface \n");
        free(query_h);
        return NULL;
    }
    do {
        // if GPB is still invalid after many attempts, return error
        if(--errTolerance <= 0){
            fprintf(stderr,"ERROR: Failed to obtain valid GPB pointer.\n");
            goto err_exit;
        }
        // read the GPB
        if(usb_mem_interface_read_words(query_h->host_h,CODEC_PARTITION,
                PTR_TO_GLOBAL_POINTER_BLOCK, (long *)&gpb, 1)<0){
            fprintf(stderr,"ERROR: Failed to get GPB from QMM memory.\n");
            goto err_exit;
        };
        // sleep 30ms
        usleep(30*1000);
    } while(gpb < NONCACHEABLE_MEMORY_BASE);

    QPRINTF( "Core: Retrieved 0x%08x as GPB\n\n", gpb );
    if(0 != usb_query_set_gpb(query_h,gpb))
    {
        fprintf(stderr,"ERROR: Failed to set GPB \n");
        goto err_exit;
    }
    usb_query_get_attached_objects(query_h);
    return query_h;

    err_exit:
    usb_mem_interface_close(query_h->host_h);

    free(query_h);
    return NULL;

}

void usb_query_close(void* ptr)
{
    USB_QUERY_HANDLE* query_h = (USB_QUERY_HANDLE*)ptr;
    if(NULL == query_h)
    {
        fprintf(stderr,"%s ERROR: Invalid Handle . \n",__FUNCTION__);
        return ;
    }
    usb_mem_interface_close(query_h->host_h);
    free(query_h);

}

void usb_query_objects(void* ptr)
{
    USB_QUERY_HANDLE* query_h = (USB_QUERY_HANDLE*)ptr;
    if(NULL == query_h)
    {
        fprintf(stderr,"%s ERROR: Invalid Handle . \n",__FUNCTION__);
        return ;
    }


    usb_query_sysctl(query_h);
    usb_query_attached_objects(query_h);

}
static void usb_query_sysctl(USB_QUERY_HANDLE* query_h)
{
    if(NULL == query_h)
    {
        fprintf(stderr,"%s ERROR: Invalid Handle . \n",__FUNCTION__);
        return ;
    }
    SYSTEM_CONTROL_STATUS statBlock;
    int i;

    SYSTEM_CONTROL_QUERY_DATA *data;
    // read the status block
    if(usb_mem_interface_read_words(query_h->host_h, CODEC_PARTITION,
            (unsigned long)query_h->systemControlStatus,
            (long *)&(statBlock), sizeof(SYSTEM_CONTROL_STATUS)/4) < 0)
    {
        fprintf(stderr,"%s ERROR: Unable to Ready Sysctl status . \n",__FUNCTION__);
        return;
    }

    data = &query_h->sysctl_q_data;
    data->status.heartbeat =  statBlock.heartbeat;
    data->status.droppedEvents = statBlock.droppedEvents;
    data->status.evReadWritePtrs = statBlock.evReadWritePtrs;
    data->status.pendingEvent = statBlock.pendingEvent;
    data->status.loadAVCEncHW = statBlock.loadAVCEncHW;
    data->status.loadAVCEncSW = statBlock.loadAVCEncSW;
    data->status.loadAVCEncHWMB = statBlock.loadAVCEncHWMB;
    data->status.loadAVCDecHW = statBlock.loadAVCDecHW;
    data->status.loadAVCDecSW = statBlock.loadAVCDecSW;
    data->status.loadAudEnc = statBlock.loadAudEnc;
    data->status.loadAudDec = statBlock.loadAudDec;
    data->status.allocatedPoolMemory = statBlock.allocatedPoolMemory;
    data->status.totalPoolMemory = statBlock.totalPoolMemory;
    data->status.heap0 = statBlock.heap0;
    data->status.heapSize = statBlock.heapSize;
    data->status.loadMux = statBlock.loadMux;
    data->status.loadUSBIsr = statBlock.loadUSBIsr;
    data->status.loadCPUPt1s = statBlock.loadCPUPt1s;
    data->status.loadCPU1s = statBlock.loadCPU1s;
    data->status.loadCPU10s = statBlock.loadCPU10s;

    for (i = 0; i < 3; i++)
    {
        data->status.vinStatus[i].nowinttime = statBlock.vinStatus[i].nowinttime;
        data->status.vinStatus[i].maxinttime = statBlock.vinStatus[i].maxinttime;
        data->status.vinStatus[i].interrupts = statBlock.vinStatus[i].interrupts;
        data->status.vinStatus[i].mintime = statBlock.vinStatus[i].mintime;
        data->status.vinStatus[i].avgtime = statBlock.vinStatus[i].avgtime;
        data->status.vinStatus[i].maxtime = statBlock.vinStatus[i].maxtime;
        data->status.vinStatus[i].debug = statBlock.vinStatus[i].debug;
        data->status.vinStatus[i].syncerror = statBlock.vinStatus[i].syncerror;
        data->status.vinStatus[i].nullframes = statBlock.vinStatus[i].nullframes;
        data->status.vinStatus[i].twsync = statBlock.vinStatus[i].twsync;
        data->status.vinStatus[i].load_hw = statBlock.vinStatus[i].load_hw;
        data->status.vinStatus[i].load_idle = statBlock.vinStatus[i].load_idle;
        data->status.vinStatus[i].load_sw = statBlock.vinStatus[i].load_sw;
    }
    for (i = 0; i < 2; i++)
    {
        data->status.ainStatus[i].debug = statBlock.ainStatus[i].debug;
        data->status.ainStatus[i].interrupts = statBlock.ainStatus[i].interrupts;
        data->status.ainStatus[i].nowinttime = statBlock.ainStatus[i].nowinttime;
        data->status.ainStatus[i].maxinttime = statBlock.ainStatus[i].maxinttime;
        data->status.ainStatus[i].mintime = statBlock.ainStatus[i].mintime;
        data->status.ainStatus[i].avgtime = statBlock.ainStatus[i].avgtime;
        data->status.ainStatus[i].maxtime = statBlock.ainStatus[i].maxtime;
        data->status.ainStatus[i].framesCaptured = statBlock.ainStatus[i].framesCaptured;
        data->status.ainStatus[i].captureFailures = statBlock.ainStatus[i].captureFailures;
        data->status.ainStatus[i].captureOverflow = statBlock.ainStatus[i].captureOverflow;
        data->status.ainStatus[i].SampleNums = statBlock.ainStatus[i].SampleNums;
        data->status.ainStatus[i].RampTime = statBlock.ainStatus[i].RampTime;
    }

    data->status.XINClockHz = statBlock.XINClockHz;
    data->status.PLL0ClockHz = statBlock.PLL0ClockHz;
    data->status.PLL1ClockHz = statBlock.PLL1ClockHz;
    data->status.PLL2ClockHz = statBlock.PLL2ClockHz;
    data->status.CPUClockHz = statBlock.CPUClockHz;
    data->status.QMMClockHz = statBlock.QMMClockHz;
    data->status.AHBClockHz = statBlock.AHBClockHz;
    data->status.APBClockHz = statBlock.APBClockHz;
    data->status.UART0ClockHz = statBlock.UART0ClockHz;
    data->status.UART1ClockHz = statBlock.UART1ClockHz;
    data->status.I2C0ClockHz = statBlock.I2C0ClockHz;
    data->status.I2C1ClockHz = statBlock.I2C1ClockHz;
    data->status.AudClockHz = statBlock.AudClockHz;
    data->status.Aud2ClockHz = statBlock.Aud2ClockHz;
    data->status.VSClockHz = statBlock.VSClockHz;
    data->status.AVCClockHz = statBlock.AVCClockHz;
    data->status.JPEGClockHz = statBlock.JPEGClockHz;
    data->status.PMEClockHz = statBlock.PMEClockHz;
    data->status.VIDClockHz = statBlock.VIDClockHz;
    data->status.DWClockHz = statBlock.DWClockHz;
    data->status.GPUClockHz = statBlock.GPUClockHz;
    data->status.memoryClockHz  = statBlock.memoryClockHz;
    data->status.uvcXferSuccess = statBlock.uvcXferSuccess;
    data->status.uvcXferFailed = statBlock.uvcXferFailed;
    data->status.uvcXferCnt = statBlock.uvcXferCnt;
    data->status.uvcXferSuccess1 = statBlock.uvcXferSuccess1;
    data->status.uvcXferFailed1 = statBlock.uvcXferFailed1;
    data->status.uvcXferCnt1 = statBlock.uvcXferCnt1;
    data->status.uvcXferSuccess2 = statBlock.uvcXferSuccess2;
    data->status.uvcXferFailed2 = statBlock.uvcXferFailed2;
    data->status.uvcXferCnt2 = statBlock.uvcXferCnt2;
    data->status.uvcXferSuccess3 = statBlock.uvcXferSuccess3;
    data->status.uvcXferFailed3 = statBlock.uvcXferFailed3;
    data->status.uvcXferCnt3 = statBlock.uvcXferCnt3;

    QPRINTF("System Control : ID %d \n", 0 );
    QPRINTF("\tHeartbeat : %d\n", statBlock.heartbeat);
    QPRINTF("\tDropped Events: %d\n", statBlock.droppedEvents);
    //QPRINTF("\tEvent Handler State: %s\n", data->eventThreadState);
    QPRINTF("\tEvent Queue Pointers: Read %d / Write %d\n",
            (unsigned int)(statBlock.evReadWritePtrs >> 16),
            statBlock.evReadWritePtrs&0xFFFF);
    QPRINTF("\tEvent Pending: %d\n", statBlock.pendingEvent);
#if 0
    // Disabled print as not getting updated.
    QPRINTF("\tEvent Handler Load Average : %d%% ints/s %d events/s %d\n",
            data->loadAverage, data->interruptsPerSecond, data->eventsPerSecond);
    QPRINTF("\tCodec load: Enc HW %d%% (MB %d%%) "
            "Enc SW %d%% Dec HW %d%% Dec SW %d%%\n",
            statBlock.loadAVCEncHW, statBlock.loadAVCEncHWMB,
            statBlock.loadAVCEncSW, statBlock.loadAVCDecHW, statBlock.loadAVCDecSW );
    QPRINTF("\tAudio load: Enc %d%% Dec %d%%\n",statBlock.loadAudEnc, statBlock.loadAudDec);
    QPRINTF("\tMux load: %d%% \n",statBlock.loadMux);
    QPRINTF("\tUSB load: %d%% \n",statBlock.loadUSBIsr);
#endif
    QPRINTF("\tCPU load (last .1s): %d%% \n",statBlock.loadCPUPt1s);
    QPRINTF("\tCPU load (last 1s): %d%% \n",statBlock.loadCPU1s);
    QPRINTF("\tCPU load (last 10s): %d%% \n",statBlock.loadCPU10s);
    QPRINTF("\tAllocated pool memory : %d/%d\n", statBlock.allocatedPoolMemory,
            statBlock.totalPoolMemory);
    QPRINTF("\tHeap Utilization : %d/%d\n", statBlock.heap0, statBlock.heapSize);
    //3 video ports - TFCC0, VPP0 and VSCALE0
    //Note: VSCALE1 is used internally by firmware and not visible here
    for (i = 0;i < 3;i++) {
        float hwload = 0;
        float swload = 0;
        const char *portNames[]={"TFCC0", "VPP0", "VSCALE0"};
        unsigned int load = (statBlock.vinStatus[i].load_hw +
                statBlock.vinStatus[i].load_idle +
                statBlock.vinStatus[i].load_sw);

        if (load > 0) {
            hwload = (statBlock.vinStatus[i].load_hw * (float)100) / load;
            swload = (statBlock.vinStatus[i].load_sw * (float)100) / load;
        }

        QPRINTF("\t%s:\n", portNames[i]);
        if(i==0)
            QPRINTF("\t\tINT: now:%uus max:%uus n:%u\n",
                statBlock.vinStatus[i].nowinttime, statBlock.vinStatus[i].maxinttime,
                statBlock.vinStatus[i].interrupts);
        else
            QPRINTF("\t\tINT: n:%u\n",
                statBlock.vinStatus[i].interrupts);
        if(i==0)
            QPRINTF("\t\tISR: min:%uus avg:%uus max:%uus\n",
                statBlock.vinStatus[i].mintime, statBlock.vinStatus[i].avgtime,
                statBlock.vinStatus[i].maxtime);
        else
            QPRINTF("\t\tISR: min:%uus avg:%uus max:%uus hwload:%.2f%% "
                "swload:%.2f%%\n",
                statBlock.vinStatus[i].mintime, statBlock.vinStatus[i].avgtime,
                statBlock.vinStatus[i].maxtime, hwload, swload);
    }
    for (i = 0; i < 2;i++) {
        QPRINTF("\tAIN%u:\n", i);
        QPRINTF("\t\tINT: now:%uus max:%uus n:%u\n",
                statBlock.ainStatus[i].nowinttime, statBlock.ainStatus[i].maxinttime,
                statBlock.ainStatus[i].interrupts);
        QPRINTF("\t\tISR: min:%uus avg:%uus max:%uus dbg:%u\n",
                statBlock.ainStatus[i].mintime, statBlock.ainStatus[i].avgtime,
                statBlock.ainStatus[i].maxtime, statBlock.ainStatus[i].debug);
        QPRINTF("\t\tframesCaptured:%u captureFailures:%u captureOverflow:%u\n",
                statBlock.ainStatus[i].framesCaptured, statBlock.
                ainStatus[i].captureFailures,statBlock.
                ainStatus[i].captureOverflow);
        QPRINTF("\t\tSamplesPerFrame:%u RampTime:%uus\n",
                statBlock.ainStatus[i].SampleNums,
                statBlock.ainStatus[i].RampTime);
        QPRINTF("\n");
    }

    if(statBlock.mstatPeriodMs > 0)    //Display only if available
    {
        // memory bandwidth stats
        QPRINTF("\tMemory bandwidth (bytes/sec) over the last %dms:\n",
                statBlock.mstatPeriodMs);
        QPRINTF("\t-------------------------------------------------\n");
        QPRINTF("\t| Read Clients:          | Write Clients:       |\n");
        QPRINTF("\t|                        |                      |\n");
        QPRINTF("\t| CPU:         %9d | CPU:       %9d |\n",
                statBlock.mstatCpuRd, statBlock.mstatCpuWr);
        QPRINTF("\t| QMM:         %9d | QMM:       %9d |\n",
                statBlock.mstatQmmRd, statBlock.mstatQmmWr);
        QPRINTF("\t| SOC-AHB:     %9d | SOC-AHB:   %9d |\n",
                statBlock.mstatAhbPerRd, statBlock.mstatAhbPerWr);
        QPRINTF("\t| SOC-FBR:     %9d | SBC:       %9d |\n",
                statBlock.mstatSocFbr, statBlock.mstatSbcWr);
        QPRINTF("\t| SBC:         %9d | FBW0:      %9d |\n",
                statBlock.mstatSbcRd, statBlock.mstatFbw0); 
        QPRINTF("\t| TFILT:       %9d | VPP0:      %9d |\n",
                statBlock.mstatTfiltRd, statBlock.mstatVpp0Wr);
        QPRINTF("\t| FBR0:        %9d | VSCL0:     %9d |\n",
                statBlock.mstatFbr0, statBlock.mstatVscl0Wr);
        QPRINTF("\t| FBR1:        %9d | VSCL1:     %9d |\n",
                statBlock.mstatFbr1, statBlock.mstatVscl1Wr);
        QPRINTF("\t| VPP0_Reg:    %9d | AVC DB:    %9d |\n",
                statBlock.mstatVpp0RegRd, statBlock.mstatAvcDb);
        QPRINTF("\t| PME:         %9d | GPU:       %9d |\n",
                statBlock.mstatPmeRd, statBlock.mstatGpuWr);
        QPRINTF("\t| AVC EME rd0: %9d | AII_0:     %9d |\n",
                statBlock.mstatAvcRd0, statBlock.mstatAii0);
        QPRINTF("\t| AVC EME rd1: %9d | AII_1:     %9d |\n",
                statBlock.mstatAvcRd1, statBlock.mstatAii1);
        QPRINTF("\t| GPU:         %9d | JPEG:      %9d |\n",
                statBlock.mstatGpuRd, statBlock.mstatJpegWr); 
        QPRINTF("\t| JPEG:        %9d | DEWARP Wr: %9d |\n",
                statBlock.mstatJpegRd, statBlock.mstatDewarpWr);
        QPRINTF("\t| DEWARP Map:  %9d | SPI:       %9d |\n",
                statBlock.mstatDewarpMapRd, statBlock.mstatSpiRd);
        QPRINTF("\t| DEWARP Rd:   %9d | VPP0 Stat: %9d |\n",
                statBlock.mstatDewarpRd, statBlock.mstatVpp0StatWr);
        QPRINTF("\t| SPI:         %9d | PME Stat:  %9d |\n",
                statBlock.mstatSpiRd, statBlock.mstatPmeStatWr);
        QPRINTF("\t|                        | AVC Bits:  %9d |\n",
                statBlock.mstatAvcBits);
        QPRINTF("\t|                        | AVC Stats: %9d |\n",
                statBlock.mstatAvcStat);
        QPRINTF("\t-------------------------------------------------\n");
        unsigned int mstatTotal
        = statBlock.mstatCpuRd + statBlock.mstatCpuWr + statBlock.mstatQmmRd + statBlock.mstatQmmWr
        + statBlock.mstatAhbPerRd + statBlock.mstatAhbPerWr
        + statBlock.mstatVpp0RegRd + statBlock.mstatVpp0Wr + statBlock.mstatVpp0StatWr
        + statBlock.mstatVscl0Wr + statBlock.mstatVscl1Wr
        + statBlock.mstatTfiltRd + statBlock.mstatSocFbr + statBlock.mstatFbr0 + statBlock.mstatFbr1
        + statBlock.mstatFbw0 + statBlock.mstatAvcRd0 + statBlock.mstatAvcRd1 + statBlock.mstatAvcDb
        + statBlock.mstatAii0 + statBlock.mstatAii1
        + statBlock.mstatJpegRd + statBlock.mstatJpegWr
        + statBlock.mstatSbcRd + statBlock.mstatSbcWr + statBlock.mstatSpiRd + statBlock.mstatSpiWr
        + statBlock.mstatAvcBits + statBlock.mstatAvcStat
        + statBlock.mstatDewarpMapRd + statBlock.mstatDewarpRd + statBlock.mstatDewarpWr
        + statBlock.mstatGpuRd + statBlock.mstatGpuWr
        + statBlock.mstatPmeRd + statBlock.mstatPmeStatWr;
        QPRINTF("\tTotal: %u bytes/sec (%.2f%% of theoretical maximum %d bytes/sec)\n",
            mstatTotal, mstatTotal*100.0/(statBlock.DDRClockHz*2*2), statBlock.DDRClockHz*2*2);   //2 clock edges, 16 bits interface (2 bytes)
        QPRINTF("\n");
    }

    QPRINTF("\tClock rates\n");
    QPRINTF("\t\tXIN      : %u\n", statBlock.XINClockHz);
    QPRINTF("\t\tPLL0     : %u\n", statBlock.PLL0ClockHz);
    QPRINTF("\t\tPLL1     : %d\n", statBlock.PLL1ClockHz);
    QPRINTF("\t\tPLL2     : %d\n", statBlock.PLL2ClockHz);
    QPRINTF("\t\tCPU      : %u\n", statBlock.CPUClockHz);
    QPRINTF("\t\tQMM      : %d\n", statBlock.QMMClockHz);
    QPRINTF("\t\tAHB      : %d\n", statBlock.AHBClockHz);
    QPRINTF("\t\tAPB      : %d\n", statBlock.APBClockHz);
    QPRINTF("\t\tUART0    : %d\n", statBlock.UART0ClockHz);
    QPRINTF("\t\tUART1    : %d\n", statBlock.UART1ClockHz);
    QPRINTF("\t\tI2C0     : %d\n", statBlock.I2C0ClockHz);
    QPRINTF("\t\tI2C1     : %d\n", statBlock.I2C1ClockHz);
    QPRINTF("\t\tAUD      : %d\n", statBlock.AudClockHz);
    //QPRINTF("\t\tAUD2     : %d\n", statBlock.Aud2ClockHz);   //Not working
    QPRINTF("\t\tVS       : %d\n", statBlock.VSClockHz);
    QPRINTF("\t\tAVC      : %d\n", statBlock.AVCClockHz);
    QPRINTF("\t\tJPEG     : %d\n", statBlock.JPEGClockHz);
    QPRINTF("\t\tPME      : %d\n", statBlock.PMEClockHz);
    QPRINTF("\t\tVID      : %d\n", statBlock.VIDClockHz);
    QPRINTF("\t\tDW       : %d\n", statBlock.DWClockHz);
    QPRINTF("\t\tGPU      : %d\n", statBlock.GPUClockHz);
    QPRINTF("\t\tDDR      : %d\n", statBlock.DDRClockHz);
    QPRINTF("\t\tMEM      : %d\n", statBlock.memoryClockHz);

	QPRINTF("\tUVC statistics:\n");
	QPRINTF("\t\tVideo Main Channel\n");
	QPRINTF("\t\t\t XferCount        : %u\n",statBlock.uvcXferCnt);
	QPRINTF("\t\t\t complete        : %u\n",statBlock.uvcXferSuccess);
	QPRINTF("\t\t\t failure         : %u\n",statBlock.uvcXferFailed);
	QPRINTF("\t\tVideo Secondary/Preview Channel\n");
	QPRINTF("\t\t\t XferCount        : %u\n",statBlock.uvcXferCnt1);
	QPRINTF("\t\t\t complete        : %u\n",statBlock.uvcXferSuccess1);
	QPRINTF("\t\t\t failure         : %u\n",statBlock.uvcXferFailed1);
	QPRINTF("\t\tAudio PCM Channel\n");
	QPRINTF("\t\t\t XferCount        : %u\n",statBlock.uvcXferCnt2);
	QPRINTF("\t\t\t complete        : %u\n",statBlock.uvcXferSuccess2);
	QPRINTF("\t\t\t failure         : %u\n",statBlock.uvcXferFailed2);
	QPRINTF("\t\tAudio AAC/OPUS Channel\n");
	QPRINTF("\t\t\t XferCount        : %u\n",statBlock.uvcXferCnt3);
	QPRINTF("\t\t\t complete        : %u\n",statBlock.uvcXferSuccess3);
	QPRINTF("\t\t\t failure         : %u\n",statBlock.uvcXferFailed3);
}
static void usb_query_lvpp(USB_QUERY_HANDLE* query_h, int id)
{
    if(NULL == query_h)
    {
        fprintf(stderr,"%s ERROR: Invalid Handle . \n",__FUNCTION__);
        return ;
    }
    VPP_STATUS      status;

    usb_mem_interface_read_words(query_h->host_h, CODEC_PARTITION,
            (unsigned long)query_h->attachedObjs[id].statBlock,
            (long *)&(status), sizeof(VPP_STATUS)/4);
    LVPP_QUERY_DATA *data = &query_h->lvpp_q_data;
    data->id = id;
    data->status.framesCaptured = status.framesCaptured;
    data->status.captureFailures = status.captureFailures;
    data->status.captureOverflow = status.captureOverflow;
    data->status.avsynchDrops = status.avsynchDrops;
    data->status.avsynchRepeats = status.avsynchRepeats;
    data->status.outputPTS_High = status.outputPTS_High;
    data->status.outputPTS_Low = status.outputPTS_Low;
    data->status.inputDrops = status.inputDrops;
    data->status.destWidth = status.destWidth;
    data->status.destHeight = status.destHeight;
    data->status.tamperStatus= status.tamperStatus;
    data->status.tfStrength = status.tfStrength;
    data->status.horzFiltLvl = status.horzFiltLvl;

    QPRINTF("LVPP : ID %d \"%s\"\n", id+1, query_h->attachedObjs[id].name);
    QPRINTF("\tFrames Captured : %u\n", status.framesCaptured);
    QPRINTF("\tCapture Failures : %u\n", status.captureFailures);
    QPRINTF("\tCapture Overflow : %u\n", status.captureOverflow);
    QPRINTF("\tA/V Synch Drops : %u\n", status.avsynchDrops);
    QPRINTF("\tA/V Synch Repeats : %u\n", status.avsynchRepeats);
    QPRINTF("\tInput Drops : %u\n", status.inputDrops);
    QPRINTF("\tOutput PTS : 0x%x%08x\n", status.outputPTS_High, status.outputPTS_Low);
    QPRINTF("\tDest Size : %ux%u\n", status.destWidth, status.destHeight);
    QPRINTF("\tTamper frame number : %u\n", (status.tamperStatus).tamperFrame);
    QPRINTF("\tTamper Defocus Comp:Thd : %u : %u\n", (status.tamperStatus).
            tamperEdgeComp,(status.tamperStatus).tamperEdgeThd);
    QPRINTF("\tTamper FieldofView Comp:Thd : %u : %u\n", (status.tamperStatus).
            tamperLumaComp,(status.tamperStatus).tamperLumaThd);
    QPRINTF("\tTamper Block Comp:Thd : %u : %u\n", (status.tamperStatus).tamperVarComp,
            (status.tamperStatus).tamperVarThd);
    QPRINTF("\tTemporal Filter Strength : %u\n", status.tfStrength);
    QPRINTF("\tHorizontal Filter Level : %u\n", status.horzFiltLvl);

}
static void usb_query_vcap(USB_QUERY_HANDLE* query_h, int id)
{
    if(NULL == query_h)
    {
        fprintf(stderr,"%s ERROR: Invalid Handle . \n",__FUNCTION__);
        return ;
    }
    VPP_STATUS      status;

    usb_mem_interface_read_words(query_h->host_h, CODEC_PARTITION,
            (unsigned long)query_h->attachedObjs[id].statBlock,
            (long *)&(status), sizeof(VPP_STATUS)/4);
    VCAP_QUERY_DATA *data = &query_h->vcap_q_data;
    data->id = id;
    data->status.framesCaptured = status.framesCaptured;
    data->status.captureFailures = status.captureFailures;
    data->status.captureOverflow = status.captureOverflow;
    data->status.avsynchDrops = status.avsynchDrops;
    data->status.avsynchRepeats = status.avsynchRepeats;
    data->status.outputPTS_High = status.outputPTS_High;
    data->status.outputPTS_Low = status.outputPTS_Low;
    data->status.inputDrops = status.inputDrops;
    data->status.destWidth = status.destWidth;
    data->status.destHeight = status.destHeight;
    data->status.tamperStatus= status.tamperStatus;
    data->status.tfStrength = status.tfStrength;
    data->status.horzFiltLvl = status.horzFiltLvl;

    QPRINTF("VCAP : ID %d \"%s\"\n", id+1, query_h->attachedObjs[id].name);
    QPRINTF("\tFrames Captured : %u\n", status.framesCaptured);
    QPRINTF("\tCapture Failures : %u\n", status.captureFailures);
    QPRINTF("\tCapture Overflow : %u\n", status.captureOverflow);
    QPRINTF("\tA/V Synch Drops : %u\n", status.avsynchDrops);
    QPRINTF("\tA/V Synch Repeats : %u\n", status.avsynchRepeats);
    QPRINTF("\tInput Drops : %u\n", status.inputDrops);
    QPRINTF("\tOutput PTS : 0x%x%08x\n", status.outputPTS_High, status.outputPTS_Low);
    QPRINTF("\tDest Size : %ux%u\n", status.destWidth, status.destHeight);
#if 0    //Not supported
    QPRINTF("\tTamper frame number : %u\n", (status.tamperStatus).tamperFrame);
    QPRINTF("\tTamper Defocus Comp:Thd : %u : %u\n", (status.tamperStatus).
            tamperEdgeComp,(status.tamperStatus).tamperEdgeThd);
    QPRINTF("\tTamper FieldofView Comp:Thd : %u : %u\n", (status.tamperStatus).
            tamperLumaComp,(status.tamperStatus).tamperLumaThd);
    QPRINTF("\tTamper Block Comp:Thd : %u : %u\n", (status.tamperStatus).tamperVarComp,
            (status.tamperStatus).tamperVarThd);
    QPRINTF("\tTemporal Filter Strength : %u\n", status.tfStrength);
    QPRINTF("\tHorizontal Filter Level : %u\n", status.horzFiltLvl);
#endif
    QPRINTF("\tOutput queue occupancy   : %d/%d\n", status.outputQueueOccupancy, status.outputQueueSize);
}

static void usb_query_nvpp(USB_QUERY_HANDLE* query_h, int id)
{
    VPP_STATUS      status;

    if(NULL == query_h)
    {
        fprintf(stderr,"%s ERROR: Invalid Handle . \n",__FUNCTION__);
        return ;
    }
    usb_mem_interface_read_words(query_h->host_h, CODEC_PARTITION,
            (unsigned long)query_h->attachedObjs[id].statBlock,
            (long *)&(status), sizeof(VPP_STATUS)/4);
    NVPP_QUERY_DATA *data = &query_h->nvpp_q_data;
    data->id = id;
    data->status.framesCaptured = status.framesCaptured;
    data->status.captureFailures = status.captureFailures;
    data->status.captureOverflow = status.captureOverflow;
    data->status.avsynchDrops = status.avsynchDrops;
    data->status.avsynchRepeats = status.avsynchRepeats;
    data->status.outputPTS_High = status.outputPTS_High;
    data->status.outputPTS_Low = status.outputPTS_Low;
    data->status.inputDrops = status.inputDrops;
    data->status.destWidth = status.destWidth;
    data->status.destHeight = status.destHeight;
    data->status.mintime = status.mintime;
    data->status.avgtime = status.avgtime;
    data->status.maxtime = status.maxtime;
    QPRINTF("NVPP : ID %d \"%s\"\n", id+1, query_h->attachedObjs[id].name);
    if(status.maxhwtime != 0)
        QPRINTF("\tFrame hardware time (us): min:%u avg:%u max:%u\n",
               status.minhwtime, status.avghwtime, status.maxhwtime);
    QPRINTF("\tFrame transit time (us):  min:%u avg:%u max:%u\n",
           status.mintime, status.avgtime, status.maxtime);
    QPRINTF("\tFrames Captured : %u\n", status.framesCaptured);
    QPRINTF("\tCapture Failures : %u\n", status.captureFailures);
    QPRINTF("\tCapture Overflow : %u\n", status.captureOverflow);
    QPRINTF("\tA/V Synch Drops : %u\n", status.avsynchDrops);
    QPRINTF("\tA/V Synch Repeats : %u\n", status.avsynchRepeats);
    QPRINTF("\tInput Drops : %u\n", status.inputDrops);
    QPRINTF("\tOutput PTS : 0x%x%08x\n", status.outputPTS_High, status.outputPTS_Low);
    QPRINTF("\tDest Size : %ux%u\n", status.destWidth, status.destHeight);
    QPRINTF("\tOutput queue occupancy   : %d/%d\n", status.outputQueueOccupancy, status.outputQueueSize);

}
static void usb_query_avcenc(USB_QUERY_HANDLE* query_h, int id)
{
    if(NULL == query_h)
    {
        fprintf(stderr,"%s ERROR: Invalid Handle . \n",__FUNCTION__);
        return ;
    }
    AVCENC_QUERY_DATA *data = &query_h->avcenc_q_data;
    AVCENC_STATUS status;
    // PTRTHES MUST BE REPLICATED IN avcenclua.c
    const char * state[15] = { "Idle",
                         "Input frame required",
                         "Bitstream space required",
                         "Reference frame required",
                         "Statistics buffer required",
                         "Segment required",
                         "Frame ready to encode",
                         "PME results pending",
                         "Encode setup started",
                         "Encode ready",
                         "Encoding picture",
                         "Waiting for codec",
                         "Picture encoded",
                         "Frame encoded",
                         "Frame done" };

    usb_mem_interface_read_words(query_h->host_h, CODEC_PARTITION,
            (unsigned long)query_h->attachedObjs[id].statBlock,(long *)&status,
            sizeof(AVCENC_STATUS)/4);

    //.... measuring frame rate ....



    data->curBitrate = 0;
    status.frameRate = 0;
#if 0
    if(status.statCapInterval)
    {
        status.frameRate = ((float)status.nFramesInStatCapInterval * 1000.0 * 1000.0)/
                    (float)status.statCapInterval;
        data->curBitrate = ((float)status.bytesEncoded * 8 * 1000)/(float)status.statCapInterval;
    }
#endif
    // Copy query data
    data->id = id;

    QPRINTF("AVC Encoder : ID %d \"%s\"\n", id+1, query_h->attachedObjs[id].name);
    QPRINTF("\tFrames Encoded    : %d\n", status.framesEncoded);
    QPRINTF("\tPerformance (IPB) : %d/%d/%d\n", status.performance[2],
            status.performance[0], status.performance[1]);
    QPRINTF("\tPerformanceJitter (IPB) : %d/%d/%d\n", status.performanceJitter[2],
            status.performanceJitter[0], status.performanceJitter[1]);
    QPRINTF("\tLatency           : %d us\n", status.latency);
    QPRINTF("\tBuffer Fullness   : %d / %d (max %d)\n",
            status.bufferFullness,status.bufferSize,status.bufferMaxFullness );
    QPRINTF("\tBase QP           : %d\n", status.baseQP );
    QPRINTF("\tState             : %s\n", state[status.state] );
    QPRINTF("\tFrames Input          : %d\n", status.inputFrames );
    QPRINTF("\tInput frame drops     : %d\n", status.inputFrameDrops );
    QPRINTF("\tRate control drops    : %d\n", status.rateControlFrameDrops );
    QPRINTF("\tLast RC drop cause    : %d\n", status.rateControlFrameDropID );
    QPRINTF("\tLast RC drop pic type : %d\n", status.lastDroppedPicType );
    QPRINTF("\tLast RC drop pic size : %d\n", status.lastDroppedPicSize );
    QPRINTF("\tLast RC drop pic QP   : %d\n", status.lastDroppedPicQP );
    QPRINTF("\tLast RC drop pic Edge : %d\n", status.lastDroppedPicEdge );
    QPRINTF("\tTelecine field drops  : %d\n", status.telecineFieldDrops );
    QPRINTF("\tNalus exceed limit    : %d\n", status.nalusExceedLimit );
    QPRINTF("\tComplexity RC override: %d\n", status.complexityRCOverride );
    QPRINTF("\tLast block ready addr : 0x%x\n", status.lastBlockReadyAddr );
    QPRINTF("\tLast block ready size : 0x%x\n", status.lastBlockReadySize );
    QPRINTF("\tLast block done addr  : 0x%x\n", status.lastBlockDoneAddr );
    QPRINTF("\tLast block done size  : 0x%x\n", status.lastBlockDoneSize );

    QPRINTF("\tCurrent PTS:          : 0x%x%08x\n", status.currentPTS_High,
            status.currentPTS_Low );
    QPRINTF("\tY SNR:                : %.2f\n", status.distortion > 0 ? 10 *
            log10( (255*255) / status.distortion) : 50 );
    QPRINTF("\tRef Pool Used         : %d\n", status.refPoolUsed );
    QPRINTF("\tRecon Preview Drops   : %d\n", status.reconPreviewOverflows );

    QPRINTF("\tFrame output delta E  : %d/%d us (Min/Max)\n",
            status.minOutputDeltaENC, status.maxOutputDeltaENC);
    QPRINTF("\tFrame output delta S  : %d/%d us (Min/Max)\n",
            status.minOutputDeltaSYS, status.maxOutputDeltaSYS);
    QPRINTF("\tFrame signature       : %d\n", status.signature);
    QPRINTF("\tCurrent bitrate       : %f kb/s\n", data->curBitrate);
    QPRINTF("\tFrame Rate            : %6.2f\n", status.frameRate );

}

static void usb_query_jpegenc(USB_QUERY_HANDLE* query_h, int id)
{
    JPEGENC_STATUS status;
    char stateNames[][25] = {
    "Idle",
    "Ready to encode next pic",
    "Encoding"
    };
    if(NULL == query_h)
    {
        fprintf(stderr,"%s ERROR: Invalid Handle . \n",__FUNCTION__);
        return ;
    }
    usb_mem_interface_read_words(query_h->host_h, CODEC_PARTITION,
            (unsigned long)query_h->attachedObjs[id].statBlock,
              (long *)&status, sizeof(JPEGENC_STATUS)/4);
    JPEGENC_QUERY_DATA *data = &query_h->jpgenc_q_data;
    data->id = id;
    data->status.id = status.id;
    data->status.state = status.state;
    data->status.inDataTokens = status.inDataTokens;
    data->status.framesEncoded = status.framesEncoded;
    data->status.inDataTokensDropped = status.inDataTokensDropped;
    data->status.lastQualityFactor = status.lastQualityFactor;
    data->status.lastFrameSize = status.lastFrameSize;
    data->status.totalFrameSize = status.totalFrameSize;
    data->status.PTS_low = status.PTS_low;
    data->status.PTS_high = status.PTS_high;
    data->status.inputTokenStandbyTime = status.inputTokenStandbyTime;
    data->status.SWSetupTime = status.SWSetupTime;
    data->status.HWEncodeTime = status.HWEncodeTime;
    data->status.inputDQueLen = status.inputDQueLen;
    data->status.inputDQueAva = status.inputDQueAva;
    data->status.inputDQueOverflow = status.inputDQueOverflow;
    data->status.outputBufferSize = status.outputBufferSize;
    data->status.outputBufferFullness = status.outputBufferFullness;
    data->status.downstreamOverflows = status.downstreamOverflows;
    data->status.downstreamOverflowsCur = status.downstreamOverflowsCur;
    data->status.encStatus = status.encStatus;
    data->status.isrStatus = status.isrStatus;
    data->status.minQF = status.minQF;
    data->status.maxQF = status.maxQF;
    data->status.currBitrate = status.currBitrate;
    data->status.recentFramesEncoded = status.recentFramesEncoded;
    data->status.recentFramesSize = status.recentFramesSize;
    data->status.minBitrate = status.minBitrate;
    data->status.maxBitrate = status.maxBitrate;
    data->status.minFrameSize = status.minFrameSize;
    data->status.maxFrameSize = status.maxFrameSize;

    QPRINTF("JPEGENC : ID %d \"%s\"\n", id+1, query_h->attachedObjs[id].name);
    QPRINTF("\tEncoder index : %d\n", status.id);
    QPRINTF("\tEncoder state : %s(%d)\n", stateNames[status.state], status.state);
    QPRINTF("\tIn DATA tokens dequeued : %u\n", status.inDataTokens);
    QPRINTF("\tFrames encoded : %u\n", status.framesEncoded);
    QPRINTF("\tIn DATA tokens dropped : %u\n", status.inDataTokensDropped);
    QPRINTF("\tLast quality factor : %u\n", status.lastQualityFactor);
    QPRINTF("\tMin quality factor allowed : %u\n", status.minQF);
    QPRINTF("\tMax quality factor allowed : %u\n", status.maxQF);
    QPRINTF("\tLast frame size (bytes) : %u [min: %d max: %d]\n",
            status.lastFrameSize, status.minFrameSize, status.maxFrameSize);
    QPRINTF("\tTotal encoded frames size (bytes) : %u\n", status.totalFrameSize);
    QPRINTF("\tFrames encoded since last reset: %u\n", status.recentFramesEncoded);
    QPRINTF("\tEncoded frames size since last reset: %u\n", status.recentFramesSize);
    if (status.recentFramesEncoded > 100) {
        QPRINTF("\tCurrent bitrate (bits/s) [min/ave/max]: %u/%u/%u\n",
                status.minBitrate, status.currBitrate, status.maxBitrate);
    }
    else {
        QPRINTF("\tCurrent bitrate (bits/s): %u\n", status.currBitrate);
    }
    QPRINTF("\tCurrent PTS : 0x%08x%08x\n", status.PTS_high, status.PTS_low);
    QPRINTF("\tToken standby time (us) : %u\n", status.inputTokenStandbyTime);
    QPRINTF("\tSoftware setup time (us) : %u\n", status.SWSetupTime);
    QPRINTF("\tHardware encode time (us) : %u\n", status.HWEncodeTime);
    QPRINTF("\tInput data que occupancy : %u/%u\n", status.inputDQueLen,
            status.inputDQueLen + status.inputDQueAva);
    QPRINTF("\tInput data que overflows : %u\n", status.inputDQueOverflow);
    QPRINTF("\tOutput buffer fullness (bytes) : %u/%u\n",
            status.outputBufferFullness, status.outputBufferSize);
    QPRINTF("\tDownstream overflows (current)/(total) : %u/%u\n",
            status.downstreamOverflowsCur, status.downstreamOverflows);

    // cases match with enum JENC_STATUS in JPEGEncoder.h
    QPRINTF("\tEncoder status: ");
    switch (status.encStatus) {
        case 0:
            QPRINTF("Status block attached.\n");
            break;
        case 1:
            QPRINTF("Notified.\n");
            break;
        case 2:
            QPRINTF("New data queued.\n");
            break;
        case 3:
            QPRINTF("Flush queued.\n");
            break;
        case 4:
            QPRINTF("Start encoding.\n");
            break;
        case 5:
            QPRINTF("HW instruction sent.\n");
            break;
        case 6:
            QPRINTF("Getting output token.\n");
            break;
        case 7:
            QPRINTF("Releasing output token.\n");
            break;
        case 8:
            QPRINTF("Delivering output token.\n");
            break;
        case 9:
            QPRINTF("Post process image.\n");
            break;
        case 10:
            QPRINTF("Checking bitrate.\n");
            break;
        case 11:
            QPRINTF("Releasing input token.\n");
            break;
        default:
            QPRINTF("Unknown.\n");
            break;
    }

    // cases match with enum JISR_STATUS in jpegisr.h
    QPRINTF("\tMastser ISR status: ");
    switch (status.isrStatus) {
        case 0:
            QPRINTF("Status block attached.\n");
            break;
        case 1:
            QPRINTF("Notified.\n");
            break;
        case 2:
            QPRINTF("Receives Notify(CONTINUE) when overflowed.\n");
            break;
        case 3:
            QPRINTF("Start encoding during CONTINUE.\n");
            break;
        case 4:
            QPRINTF("DATA token received.\n");
            break;
        case 5:
            QPRINTF("Start encoding.\n");
            break;
        case 6:
            QPRINTF("Overflows.\n");
            break;
        case 7:
            QPRINTF("Encode done interrupt.\n");
            break;
        case 8:
            QPRINTF("JPEG core ready to encode next token.\n");
            break;
        case 9:
            QPRINTF("Interrput setup next encoding.\n");
            break;
        case 10:
            QPRINTF("Interrupt can't get output tokens. Overflows.\n");
            break;
        default:
            QPRINTF("Unknown.\n");
            break;
    }

}


static void usb_query_audfltr(USB_QUERY_HANDLE* query_h, int id)
{
    if(NULL == query_h)
    {
        fprintf(stderr,"%s ERROR: Invalid Handle . \n",__FUNCTION__);
        return ;
    }
    AUDFLTR_STATUS status;
    char stateNames[][35] = {
            "Idle",
            "Waiting for input frames",
            "Waiting for output buffer space",
            "Buffer is full",
            "Computing function"
    };
    int audioResamplePPMint;
    AUDFLTR_QUERY_DATA *data = &query_h->aflt_q_data;

    usb_mem_interface_read_words(query_h->host_h, CODEC_PARTITION,
            (unsigned long)query_h->attachedObjs[id].statBlock,
            (long *)&status, sizeof(AUDFLTR_STATUS)/4);

    data->id = id;
    data->status.streamID = status.streamID;
    data->status.state = status.state;
    data->status.lastInterrupt = status.lastInterrupt;
    data->status.inputPorts = status.inputPorts;
    data->status.outputPorts = status.outputPorts;
    data->status.recvFramesThisRound = status.recvFramesThisRound;
    data->status.inFramesRound = status.inFramesRound;
    data->status.sentFramesThisRound = status.sentFramesThisRound;
    data->status.outFramesRound = status.outFramesRound;
    data->status.currBufferFullCount = status.currBufferFullCount;
    data->status.totalBufferFullCount = status.totalBufferFullCount;
    data->status.inputBufferFullness = status.inputBufferFullness;
    data->status.inputBufferSize = status.inputBufferSize;
    data->status.outputBufferFullness = status.outputBufferFullness;
    data->status.outputBufferSize = status.outputBufferSize;
    data->status.audioResampleRatio = status.audioResampleRatio;
    data->status.audioResampleDelta = status.audioResampleDelta;
    data->status.latency = status.latency;

    if ((int)status.audioResampleRatio == 0)
        audioResamplePPMint = 0;
    else
        audioResamplePPMint = ((((int)(status.audioResampleRatio) - 0x00800000)
                * 10000)) / 83886;
    QPRINTF("AUDFLTR : ID %d \"%s\"\n", id+1, query_h->attachedObjs[id].name);
    QPRINTF("\tFilter stream ID : %d\n", status.streamID);
    QPRINTF("\tFilter state : %s (%d)\n", stateNames[status.state], status.state);
    QPRINTF("\tLast received command/interrupt : %d\n", status.lastInterrupt);
    QPRINTF("\tInput ports : %d\n", status.inputPorts);
    QPRINTF("\tOutput ports : %d\n", status.outputPorts);
    QPRINTF("\tInput frames received in current round : %d\n", status.recvFramesThisRound);
    QPRINTF("\tRounds of input frames received from src channels : %d\n",
            status.inFramesRound);
    QPRINTF("\tOutput frames sent in current round : %d\n", status.sentFramesThisRound);
    QPRINTF("\tRounds of output frames sent to output ports : %d\n",
            status.outFramesRound);
    QPRINTF("\tOutput buffer overflow count (current) : %d\n", status.currBufferFullCount);
    QPRINTF("\tOutput buffer overflow count (total)   : %d\n", status.totalBufferFullCount);
    QPRINTF("\tInput buffer occupancy: (%d/%d)\n", status.inputBufferFullness,
            status.inputBufferSize);
    QPRINTF("\tOutput buffer occupancy: (%d/%d)\n", status.outputBufferFullness,
            status.outputBufferSize);
    QPRINTF("\tAudio Sync: Delta %d Resample Ratio 0x%x (%d ppm)\n",
            (int) status.audioResampleDelta,
            (int) status.audioResampleRatio,
            (int) audioResamplePPMint);
    QPRINTF("\tComputation latency: %d us\n", status.latency);



}
static void usb_query_isp(USB_QUERY_HANDLE* query_h, int id)
{
    ISP_STATUS status;
    char algoStr[4][16] = { "gray", "weighted", "mesh", "sensor" };
    int i;
    if(NULL == query_h)
    {
        fprintf(stderr,"%s ERROR: Invalid Handle . \n",__FUNCTION__);
        return ;
    }
    ISP_QUERY_DATA *data = &query_h->isp_q_data;
    usb_mem_interface_read_words(query_h->host_h, CODEC_PARTITION,
               (unsigned long)query_h->attachedObjs[id].statBlock,
               (long *)&status, sizeof(ISP_STATUS)/4);

    data->id = id;
    data->status.sharpness = status.sharpness;
    data->status.saturation = status.saturation;
    data->status.nrStrength = status.nrStrength;
    data->status.nrThreshLong = status.nrThreshLong;
    data->status.gamma = status.gamma;
    data->status.iridix = status.iridix;
    data->status.aeEnable = status.aeEnable;
    data->status.awbEnable = status.awbEnable;
    data->status.exposure = status.exposure;
    data->status.analogGain = status.analogGain;
    data->status.histo[0] = status.histo[0];
    data->status.histo[1] = status.histo[1];
    data->status.histo[2] = status.histo[2];
    data->status.histo[3] = status.histo[3];
    data->status.histo[4] = status.histo[4];
    data->status.awbMired = status.awbMired;
    data->status.awbShift = status.awbShift;
    data->status.awbAlgo = status.awbAlgo;
    data->status.awbA1 = status.awbA1;
    data->status.awbB1 = status.awbB1;
    data->status.awbC1 = status.awbC1;
    data->status.awbP1 = status.awbP1;
    data->status.awbP2 = status.awbP2;
    data->status.awbQ1 = status.awbQ1;
    data->status.awbFinalRG = status.awbFinalRG;
    data->status.awbFinalGG = status.awbFinalGG;
    data->status.awbFinalBG = status.awbFinalBG;
    data->status.awbGrayRG = status.awbGrayRG;
    data->status.awbGrayBG = status.awbGrayBG;
    data->status.awbWeightedRG = status.awbWeightedRG;
    data->status.awbWeightedBG = status.awbWeightedBG;
    memcpy(data->status.sGain, status.sGain, 4 * sizeof(int));
    memcpy(data->status.ccm, status.ccm, 9 * sizeof(int));
    memcpy(data->status.awbZoneWeight, status.awbZoneWeight, 63 * sizeof(int));
//    memcpy(data->status.awbZoneRG, status.awbZoneRG, 63 * sizeof(int));
//    memcpy(data->status.awbBG, status.awbZoneBG, 63 * sizeof(int));
    memcpy(data->status.awbZoneSum, status.awbZoneSum, 63 * sizeof(int));
    QPRINTF("ISP : ID %d \"%s\"\n", id+1, query_h->attachedObjs[id].name);
    QPRINTF("\tSharpness                 : %d / 255\n", status.sharpness);
    QPRINTF("\tSaturation                : %d / 200\n", status.saturation);
    QPRINTF("\tGamma                     : %d\n", status.gamma);
    QPRINTF("\tIridix                    : %d / 255\n", status.iridix);
    QPRINTF("\tNoise Reduction Filter\n");
    QPRINTF("\t- Strength                : %d / 255\n", status.nrStrength);
    QPRINTF("\t- Long Threshold          : %d\n", status.nrThreshLong);
    QPRINTF("\tAuto Exposure\n");
    QPRINTF("\t- Enable                  : %d\n", status.aeEnable);
    QPRINTF("\t- Sensor exposure         : %d\n", status.exposure);
    QPRINTF("\t- Sensor analog gain      : %.2f\n", status.analogGain);
    QPRINTF("\t- Histogram stats         : %d %d %d %d %d\n",
            status.histo[0],status.histo[1],status.histo[2],
            status.histo[3],status.histo[4]);
    QPRINTF("\tAuto White Balance\n");
    QPRINTF("\t- Enable                  : %d\n", status.awbEnable);
    if (status.awbEnable)
        QPRINTF("\t- Algorithm               : %s\n", algoStr[status.awbAlgo]);
    if ((status.awbMired >> 8) > 0)
        QPRINTF("\t- Color temperature       : %d\n", 1000000 / (status.awbMired >> 8));
    QPRINTF("\t- Mired                   : %d\n", status.awbMired);
    QPRINTF("\t- Shift                   : %d\n", status.awbShift);
    QPRINTF("\t- Total Weight            : %d\n", status.awbWeight);
    QPRINTF("\t- Profile Parameters      : (a1) %d (b1) %d (c1) %d\n",
            status.awbA1, status.awbB1, status.awbC1);
    QPRINTF("\t                            (p1) %d (p2) %d (q1) %d\n",
            status.awbP1, status.awbP2, status.awbQ1);
    QPRINTF("\t- Static Gains            : (R) 0x%04x (Gr) 0x%04x (Gb) 0x%04x (B) 0x%04x\n",
            status.sGain[0], status.sGain[1],status.sGain[2], status.sGain[3]);
    QPRINTF("\t- Color Correction Matrix : 0x%04x 0x%04x 0x%04x\n",
            status.ccm[0], status.ccm[1],status.ccm[2]);
    QPRINTF("\t                            0x%04x 0x%04x 0x%04x\n",
            status.ccm[3], status.ccm[4],status.ccm[5]);
    QPRINTF("\t                            0x%04x 0x%04x 0x%04x\n",
            status.ccm[6], status.ccm[7], status.ccm[8]);
#if 0   //Not using this algorithm
    QPRINTF("\t- R/G B/G Ratios          : (Gray)     0x%04x 0x%04x\n", status.awbGrayRG,
            status.awbGrayBG);
#endif
    QPRINTF("\t- R/G B/G Ratios          : (Weighted) 0x%04x 0x%04x\n",
            status.awbWeightedRG, status.awbWeightedBG);              //Here suffix G stands for green
    QPRINTF("\t -Final AWB Gains         : (R) 0x%04x (G) 0x%04x (B) 0x%04x\n",
            status.awbFinalRG, status.awbFinalGG, status.awbFinalBG); //Here suffix G stands for gain 
#if 0
    QPRINTF("\t- Final AWB Gains         : (R) 0x%04x (G) 0x%04x (B) 0x%04x\n",
            status.awbGains[0], status.awbGains[1],
            status.awbGains[2]);
#endif
    QPRINTF("\t- Zone Weights            :\n\n");
    {
        for (i=0; i<225; i++)
        {
            if ((i % 15) == 0) QPRINTF("\t\t");
            QPRINTF("%4d ", status.awbZoneWeight[i]);
            if ((i % 15) == 14) QPRINTF("\n");
        }
        QPRINTF("\n");
    }
#if 0
    QPRINTF("\t- Zone RG/BG (Sum)        :\n\n");
    for (i=0; i<63; i++)
    {
        if ((i % 9) == 0) QPRINTF("\t\t");
        QPRINTF("%3x/%3x(%3d) ", status.awbZoneRG[i], status.awbZoneBG[i],
                status.awbZoneSum[i]/100);
        if ((i % 9) == 8) QPRINTF("\n");
    }
#endif
    QPRINTF("\n");

}
static void usb_query_audenc(USB_QUERY_HANDLE* query_h, int id)
{
    if(NULL == query_h)
    {
        fprintf(stderr,"%s ERROR: Invalid Handle . \n",__FUNCTION__);
        return ;
    }
    AUDENC_QUERY_DATA *data = &query_h->aenc_q_data;
    AUDIOENC_STATUS status;
    const char * state[4] = { "Idle",
                        "Input frame required",
                        "Output space required",
                        "Encode ready",
    };

    usb_mem_interface_read_words(query_h->host_h, CODEC_PARTITION,
                   (unsigned long)query_h->attachedObjs[id].statBlock,
                   (long *)&status, sizeof(AUDIOENC_STATUS)/4);
    // Copy query data
    data->id = id;
    memcpy(&(data->status), &status, sizeof(AUDIOENC_STATUS));
#if 0
    if(status.bitrateCntrInterval > 0)
    {
        data->curBitrate = ((float)status.bytesEncoded * 8 * 1000)/
                (float)status.bitrateCntrInterval;
    }
    else
    {
        data->curBitrate = 0;
    }
#else
        data->curBitrate = 0;
#endif
    QPRINTF("Audio Encoder : ID %d \"%s\"\n",  id+1, query_h->attachedObjs[id].name);
    QPRINTF("\tFrames Encoded    : %d\n", status.framesEncoded);
    QPRINTF("\tState             : %s\n", state[status.state]);
    QPRINTF("\tLatency           : %d us\n", status.latency);
    QPRINTF("\tBuffer Fullness   : %d / %d \n", status.bufferFullness, status.bufferSize);
    QPRINTF("\tFrames Input      : %d\n", status.inputFrames);
    QPRINTF("\tInput frame drops : %d\n", status.inputFrameDrops);
    QPRINTF("\tCurrent PTS:      : 0x%x%08x\n", status.currentPTS_High, status.currentPTS_Low);
    QPRINTF("\tFrame output delta E  : %d/%d us (Min/Max)\n",
            status.minOutputDeltaENC, status.maxOutputDeltaENC);
    QPRINTF("\tFrame output delta S  : %d/%d us (Min/Max)\n",
            status.minOutputDeltaSYS, status.maxOutputDeltaSYS);
    QPRINTF("\tCurrent bitrate   : %f kb/s\n", data->curBitrate);

}

static void usb_query_dewarp(USB_QUERY_HANDLE* query_h, int id)
{
    DEWARP_STATUS status;
    int i;

    if(NULL == query_h) {
        fprintf(stderr, "%s: ERROR: Invalid handle\n", __func__);
        return;
    }

    usb_mem_interface_read_words(query_h->host_h, CODEC_PARTITION,
               (unsigned long)query_h->attachedObjs[id].statBlock,
               (long *)&status, sizeof(DEWARP_STATUS)/4);

    QPRINTF("DEWARP : ID %d \"%s\"\n", id+1, query_h->attachedObjs[id].name);
    QPRINTF("\tState                  : %d\n", status.State);
    QPRINTF("\tFrame count            : %d\n", status.FramesCount);
    QPRINTF("\tMap type               : %d\n", status.MapType);
    QPRINTF("\tMap max length         : %d\n", status.MapMaxLen);
    QPRINTF("\tMap N                  : %d\n", status.MapN);
    QPRINTF("\tMap width              : %d\n", status.MapWidth);
    QPRINTF("\tMap height             : %d\n", status.MapHeight);
    QPRINTF("\tPicture Sizes\n");
    QPRINTF("\t- Source width         : %d\n", status.SrcWidth);
    QPRINTF("\t- Source height        : %d\n", status.SrcHeight);
    QPRINTF("\t- Dewarped width       : %d\n", status.DewarpedWidth);
    QPRINTF("\t- Dewarped height      : %d\n", status.DewarpedHeight);
    QPRINTF("\t- Output width         : %d\n", status.OutputWidth);
    QPRINTF("\t- Output height        : %d\n", status.OutputHeight);
    QPRINTF("\t- Composite width      : %d\n", status.CompositeWidth);
    QPRINTF("\t- Composite height     : %d\n", status.CompositeHeight);
    QPRINTF("\tLens Parameters\n");
    QPRINTF("\t- FOV                  : %d\n", status.lensFOV);
    QPRINTF("\t- Radius               : %d\n", status.lensRadius);
    QPRINTF("\t- H shift              : %d\n", status.lensHShift);
    QPRINTF("\t- V shift              : %d\n", status.lensVShift);
    QPRINTF("\tMap usage status\n");
    QPRINTF("\t- Last map             : %d\n", status.LastMap);
    QPRINTF("\t- Current map          : %d\n", status.CurrentMap);
    QPRINTF("\t- Next map             : %d\n", status.NextMap);

    QPRINTF("\tMapQue%d  Hdr codec addr: %p\n", (id+1), 
            query_h->attachedObjs[id].statBlock + ((char *)&status.mapQHeaders[0] - (char *)&status));
    QPRINTF("\tMapQue%d base codec addr: %p\n", (id+1), 
            query_h->attachedObjs[id].statBlock + ((char *)&status.mapQBase[0] - (char *)&status));
    QPRINTF("\tMapQue%d items        : ", (id+1));
    for (i = 0; i < MEDEWARP_INTERCPU_MAP_QUEUE_SIZE; i++)
        QPRINTF("0x%08x,", status.mapQBase[i]);
    QPRINTF("\n");

    QPRINTF("\tMapQue%d length       : %d\n", (id+1), status.mapQLen);
    QPRINTF("\tMapQue%d read         : %d\n", (id+1), status.mapQRead);
    QPRINTF("\tMapQue%d write        : %d\n", (id+1), status.mapQWrite);
    QPRINTF("\tHardware dewarp time (us): %d\n", status.hwtime);
    QPRINTF("\tTotal dewarp time (us)   : %d\n", status.totaltime);
    QPRINTF("\tInput queue occupancy    : %d\n", status.inputQueueOccupancy);
    QPRINTF("\tOutput queue occupancy   : %d/%d\n", status.outputQueueOccupancy, status.outputQueueSize);
    QPRINTF("\tOutput queue compositor occupancy   : %d/%d\n", status.outputQueueCompositorOccupancy, status.outputQueueCompositorSize);
}

static void usb_query_attached_objects(USB_QUERY_HANDLE* query_h)
{
    int i = 0;
    if(NULL == query_h)
    {
        fprintf(stderr,"%s ERROR: Invalid Handle . \n",__FUNCTION__);
        return ;
    }

#if 0
    QPRINTF("query_h->numObjects = %d\n",query_h->numObjects);
#endif
    for (i = 0;i < query_h->numObjects;i++) {
#if 0
        printf("query_h->attachedObjs[i].type = %d\n",
                query_h->attachedObjs[i].type);
#endif
        switch (query_h->attachedObjs[i].type) {
            case LVPP_CTRLOBJ_TYPE: {
                usb_query_lvpp(query_h,i);
                break;
            }
            case VCAP_CTRLOBJ_TYPE: {
                usb_query_vcap(query_h,i);
                break;
            }
            case NVPP_CTRLOBJ_TYPE: {;
                usb_query_nvpp(query_h,i);
                break;
            }
            case AVCENC_CTRLOBJ_TYPE: {
                usb_query_avcenc(query_h,i);
                break;
            }
            case JPEGENC_CTRLOBJ_TYPE: {
                usb_query_jpegenc(query_h,i);
                break;
            }
            case AUDFLTR_CTRLOBJ_TYPE: {
                usb_query_audfltr(query_h,i);
                break;
            }
            case ISP_CTRLOBJ_TYPE: {
                usb_query_isp(query_h,i);
                break;
            }
            case AUDENC_CTRLOBJ_TYPE:{
                usb_query_audenc(query_h,i);
                break;
            }
            case DEWARP_CTRLOBJ_TYPE:
                usb_query_dewarp(query_h, i);
                break;
            case AVOC_CTRLOBJ_TYPE:
            case AVSN_CTRLOBJ_TYPE:
                break;
            default:
                break;
        }
    }
}
