/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

/************************************************************************************
 * @(#) $Header: http://svllsvn01.local.geosemi.com/repos/swrepos/trunk/condorsw/host/tools/querydump/usb_mem_interface.c 57866 2016-04-28 11:12:50Z bsmith $
 ************************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <pthread.h>

#ifdef CHIP_REGS_DEFINED_LOCAL
#define   QCC_BID_PMU0           0x21

#define Reg_M8_MEMS_MEM0_PMU_x_PartBaseSeg0_ADDRESS     0x200
#define Reg_M8_MEMS_MEM0_PMU_x_PartBaseSeg1_ADDRESS     0x202
#define Reg_M8_MEMS_MEM0_PMU_x_PartBaseSeg0_NUM_BYTES   0x2
#define Reg_M8_MEMS_MEM0_PMU_x_VPATSegSize_ADDRESS      0x08
#define Reg_M8_MEMS_MEM0_PMU_x_VPATSegSize_NUM_BYTES    0x2
#else
#include "m8_reg.h"
#endif

#include "libusb.h"
#include "vendor_req.h"
#include "usb_mem_interface.h"

#define RESET_MS    5
static libusb_context *usb_context = NULL;

#define XTENSA_PARTITION_ID			127
#define TARGET_BYTE_ORDER			LITTLE_ENDIAN

#define USB_VENDOR_ID_GEO               0x29fe
#define USB_DEVICE_ID_GEO_CONDOR        0x4d53

/*
 *  See the MEMS spec for VPAT register details,
 *  particularly how to determine the partition base address
 */
#define VPAT_SXSW 9
#define VPAT_SYSW 6

#define QCCFIELD_VPATSEGXSIZE_SHIFT 0
#define QCCFIELD_VPATSEGXSIZE_MASK  0x3
#define QCCFIELD_VPATSEGYSIZE_SHIFT 2
#define QCCFIELD_VPATSEGYSIZE_MASK  0x3

#define EM_BID_OK(bid)              ((bid & 0xFF) == bid)
#define EM_ADDR_OK(addr)            ((addr & 0xFFFFFFFC) == addr)
#define EM_COUNT_OK(count)          (1)
#define EM_COUNT_ALIGNED(count)     ((count & 0x3) == count)


// right now the libusb/uvc drivers set the max transfer size to be just 2048 bytes
#define RAPTOR_USB_MAX_XFER_SZ 2048

#define EXIT_ON_ERROR(ret)      \
{                               \
    if (ret != USB_MEM_IF_SUCCESS)    \
        goto err_exit;          \
}

#define EXIT_WITH_ERROR(err)    \
{                               \
    ret = err;                  \
    goto err_exit;              \
}

// To check both transfer and proxied returns:
#define EXIT_ON_EITHER_ERROR(ret,r2)    \
{                               \
    if (ret != USB_MEM_IF_SUCCESS)    \
        goto err_exit;          \
    if (r2 != USB_MEM_IF_SUCCESS) {   \
        ret = r2;               \
        goto err_exit;          \
    }                           \
}


typedef struct usb_mem_interface_struct
{
    libusb_context *usb_context;
    libusb_device_handle *usb_h;
    pthread_mutex_t *lock_p;
    int curr_part_id;
    int curr_part_base;
} st_usb_mem_if;


static unsigned long GetPartitionBase(void* host_h, int partition);
static int usb_mem_interface_get_partbase_internal(void* host_h,
               unsigned char bid, unsigned long* base_p);


static int usb_mem_interface_read_block(void* host_h,
            unsigned long addr, unsigned long *data_p, int width, int length);


static int usb_mem_interface_write_block(void* host_h,
             unsigned long addr, unsigned long *data_p, int width, int length);
static int usb_mem_interface_lock(void* host_h);
static int usb_mem_interface_unlock(void* host_h);
static void *usb_mem_interface_malloc( int size);
static int usb_mem_interface_free(void* host_h);



static int usb_mem_interface_read_qcc_internal(void* host_h, unsigned long bid,
              unsigned long addr, unsigned long *data_p, int width);
static int usb_mem_interface_write_qcc_internal(void* host_h, unsigned long bid,
               unsigned long addr, unsigned long data, int width);
#if 0
static int usb_configure(st_usb_mem_if *host_p);
static int usb_mem_interface_wait(void* host_h,unsigned long time_ms);
static int usb_reset(st_usb_mem_if *host_p);
static inline int usb_reset_ep(st_usb_mem_if *host_p, raptor_usb_ep_t ep);
static int usb_rw(st_usb_mem_if *host_p,//TODO
          raptor_usb_vr_hdr_t *vr_p, void *data_p);
static int usb_mem_interface_reset(void* host_h);
static int usb_mem_interface_read(void* host_h,
          unsigned long addr, unsigned long *data_p, int width);
static int usb_mem_interface_wait_ns(void* host_h,unsigned long time_ms);
static int usb_mem_interface_write(void* host_h,
           unsigned long addr, unsigned long data, int width);

static int usb_configure(st_usb_mem_if *host_p)
{
    int config;
    int ret = 0;
    // Set Configuration:
    ret = libusb_get_configuration(host_p->usb_h, &config);
    if(ret)
    {
        fprintf(stderr,"%s usb get config failed ret = %d\n",__FUNCTION__, ret);
        return USB_MEM_IF_ERR_IO;
    }

    if (config != USB_CFG_QHAL) {
        ret = libusb_set_configuration(host_p->usb_h, USB_CFG_QHAL);
        if(ret)
        {
            fprintf(stderr,"%s usb set config failed ret = %d\n",__FUNCTION__,ret);
            return USB_MEM_IF_ERR_IO;
        }
    }
#if 0
    // Claim Interface:
    if (r = libusb_claim_interface(host_p->usb_h, USB_IF_QHAL)) {
    printf("claim intf error %d\n", r);
    return USB_MEM_IF_ERR_IO;
    }
#endif

    return USB_MEM_IF_SUCCESS;
}

#endif




/****************************************************************************
 *
 * Host Init
 *
 ****************************************************************************/

void* usb_mem_interface_open()
{

    st_usb_mem_if *host_p;
    static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
    int ret;
    struct libusb_device **devs;
    struct libusb_device *dev;
    int i;


    host_p = (st_usb_mem_if *)usb_mem_interface_malloc(sizeof(st_usb_mem_if));
    if (host_p == NULL)
    {
        fprintf(stderr,"Unable to Allocate usb mem if handle \n");
        return NULL;
    }

    // First opened handle, initialize device:
    //
    pthread_mutexattr_t attr;

    // Set locking mechanism to be process-shared:
    //
    pthread_mutexattr_init(&attr);
    pthread_mutexattr_setpshared(&attr, PTHREAD_PROCESS_SHARED);
    ret = pthread_mutex_init(&lock, &attr);
    pthread_mutexattr_destroy(&attr);
    if (ret)
    {

        fprintf(stderr,"%s mutext init Failed \n",__FUNCTION__);
        usb_mem_interface_free((void *) host_p); 
        return NULL;
    }

    // Init the lib and grab our reference:
    ret = libusb_init(&usb_context);
    if (ret) {
        fprintf(stderr,"%s libusb_init Failed \n",__FUNCTION__);
        pthread_mutex_destroy(&lock);
        goto err_exit;
    }

    libusb_get_device_list(usb_context, &devs);
    i = 0;
    while ((dev = devs[i++]))
    {
        struct libusb_device_descriptor desc;
        libusb_get_device_descriptor(dev, &desc);
        //printf("vid %d pid %d\n", desc.idVendor, desc.idProduct);
        if (desc.idVendor == USB_VENDOR_ID_GEO && desc.idProduct == USB_DEVICE_ID_GEO_CONDOR) {
            libusb_open(dev, &host_p->usb_h);
            break;
        }
    }
    libusb_free_device_list(devs, 1);

    if (host_p->usb_h == NULL)
    {
        fprintf(stderr,"%s Invalid Usb Handle \n",__FUNCTION__);
        pthread_mutex_destroy(&lock);
        libusb_exit(usb_context);
        usb_context = NULL;
        goto err_exit;
    }
#if 0
    if (usb_configure(host_p))
    {
        fprintf(stderr,"%s Error in Usb Configure \n",__FUNCTION__);
        libusb_close(host_p->usb_h);
        libusb_exit(usb_context);
        usb_context = NULL;
        goto err_exit;
    }
#endif
    host_p->usb_context = usb_context;
    host_p->lock_p = &lock;



    host_p->curr_part_id = -1;
    host_p->curr_part_base = -1;

    return (void*)host_p;

    err_exit:
    usb_mem_interface_free((void *)host_p);
    return NULL;
}


int usb_mem_interface_close(void* host_h)
{
    st_usb_mem_if*host_p  = (st_usb_mem_if*)host_h;
    pthread_mutex_destroy(host_p->lock_p);
//    libusb_release_interface(host_p->usb_h, USB_IF_QHAL);
    libusb_close(host_p->usb_h);
    libusb_exit(usb_context);
    usb_context = NULL;
    usb_mem_interface_free((void *)host_p);
    return USB_MEM_IF_SUCCESS;
}



static int usb_mem_interface_lock(void* host_h)
{
    st_usb_mem_if*host_p  = (st_usb_mem_if*)host_h;

    if (pthread_mutex_lock(host_p->lock_p))
    return USB_MEM_IF_ERR_LOCK;
    return USB_MEM_IF_SUCCESS;
}

static int usb_mem_interface_unlock(void* host_h)
{
    st_usb_mem_if*host_p  = (st_usb_mem_if*)host_h;

    if (pthread_mutex_unlock(host_p->lock_p))
    return USB_MEM_IF_ERR_LOCK;
    return USB_MEM_IF_SUCCESS;
}

static void *usb_mem_interface_malloc(int size)
{
    return malloc((size_t)size);
}

static int usb_mem_interface_free(void* host_h)
{
    if (host_h != NULL)
    free(host_h);
    return USB_MEM_IF_SUCCESS;
}


static int usb_mem_interface_write_block(void* host_h,
             unsigned long addr, unsigned long *data_p, int width, int length)
{
    st_usb_mem_if*host_p  = (st_usb_mem_if*)host_h;
    int ret;
    uint16_t bytes_transfer;
    uint16_t addr_hi;
    uint16_t addr_lo;
    unsigned char status[64];
    unsigned char cmd_sta;

    if (data_p == NULL)
        return USB_MEM_IF_ERR_BADPARAM;


    switch (width)
    {
        case 1:
        case 2:
        case 4:
            break;

        default:
            return USB_MEM_IF_ERR_BADPARAM;
    }

    addr_hi = (uint16_t)(addr >> 16);
    addr_lo = (uint16_t)(addr & 0xFFFF);
    bytes_transfer = (uint16_t)(width * length);

    ret = libusb_control_transfer(host_p->usb_h,
            /* bmRequestType */
            (LIBUSB_ENDPOINT_OUT | LIBUSB_REQUEST_TYPE_VENDOR |
                    LIBUSB_RECIPIENT_INTERFACE),
                    /* bRequest      */ MEM_WRITE,
                    /* wValue        */ (uint16_t)addr_hi,
                    /* MSB 4 bytes   */
                    /* wIndex        */ (uint16_t)addr_lo,
                    /* Data          */ (unsigned char *)data_p,
                    /* wLength       */ bytes_transfer,
                    /* timeout       */ 0
    );
    if (ret < 0) {
        printf("usb_mem_interface_write_block error: vendor req MEM_WRITE. %d\n", ret);
        return ret;
    }

    ret = libusb_control_transfer(host_p->usb_h,
            /* bmRequestType */
            (LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_VENDOR |
                    LIBUSB_RECIPIENT_INTERFACE),
                    /* bRequest      */ MEM_WRITE_STATUS,
                    /* wValue        */ 0,
                    /* wIndex        */ 0,
                    /* Data          */ status,
                    /* wLength       */ sizeof(status),
                    /* timeout       */ 0
    );

    if (ret < 0) {
        printf("usb_mem_interface_write_block error: vendor req MEM_WRITE_STATUS. %d\n", ret);
        return ret;
    }

    cmd_sta = *(unsigned char *)status;
    //if (cmd_sta >= MXCAM_ERR_FAIL) {
    if (cmd_sta >= 128) {
        return cmd_sta;
    }

    return USB_MEM_IF_SUCCESS;
}


/****************************************************************************
 *
 * Bus reads
 *
 ****************************************************************************/



static int usb_mem_interface_read_block(void* host_h,
            unsigned long addr, unsigned long *data_p, int width, int length)
{
    st_usb_mem_if*host_p  = (st_usb_mem_if*)host_h;
    int ret;
    int bytes_transfer;
    uint16_t addr_hi;
    uint16_t addr_lo;

    if (data_p == NULL)
        return USB_MEM_IF_ERR_BADPARAM;

    addr_hi = (uint16_t)(addr >> 16);
    addr_lo = (uint16_t)(addr & 0xFFFF);
    bytes_transfer = (width * length);

    ret = libusb_control_transfer(host_p->usb_h,
            /* bmRequestType */
            (LIBUSB_ENDPOINT_OUT | LIBUSB_REQUEST_TYPE_VENDOR |
                    LIBUSB_RECIPIENT_INTERFACE),
                    /* bRequest      */ MEM_READ,
                    /* wValue        */ (uint16_t)addr_hi,
                    /* MSB 4 bytes   */
                    /* wIndex        */ (uint16_t)addr_lo,
                    /* Data          */ (unsigned char *)&bytes_transfer,
                    /* wLength       */ sizeof(int),
                    /* timeout       */ 0
    );
    if (ret < 0) {
        printf("usb_mem_interface_read_block error: vendor req MEM_READ. %d\n", ret);
        return ret;
    }

    ret = libusb_control_transfer(host_p->usb_h,
            /* bmRequestType */
            (LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_VENDOR |
                    LIBUSB_RECIPIENT_INTERFACE),
                    /* bRequest      */ MEM_READ_STATUS,
                    /* wValue        */ 0,
                    /* wIndex        */ 0,
                    /* Data          */ (unsigned char*)data_p,
                    /* wLength       */ bytes_transfer,
                    /* timeout       */ 0
    );

    if (ret < 0) {
        printf("usb_mem_interface_read_block error: vendor req MEM_READ_STATUS. %d\n", ret);
        return ret;
    }

#if 0
    printf("usb_mem_interface_read_block: data:\n");
    p = data_p;
    for (ret = 0; ret < length; ret++) {
    int j;
    for (j = 0; j < width; j++) {
        printf("%02x ", *p++);
    }
    printf("\n");
    }
#endif

    return USB_MEM_IF_SUCCESS;
}


/****************************************************************************
 *
 * Platform-specific QCC access
 *
 ****************************************************************************/

#define QCC_BID_OK(bid)     ((bid & 0xFF) == bid)
#define QCC_ADDR_OK(addr)   ((addr & 0xFFFF) == addr)

static int usb_mem_interface_write_qcc_internal(void* host_h, unsigned long bid,
               unsigned long addr, unsigned long data, int width)
{
    st_usb_mem_if*host_p  = (st_usb_mem_if*)host_h;
    unsigned char status[64];
    unsigned char cmd_sta;
    int ret = USB_MEM_IF_SUCCESS;

    if (!QCC_BID_OK(bid) || !QCC_ADDR_OK(addr))
        return USB_MEM_IF_ERR_BADPARAM;

    switch (width)
    {
        case 1:
            data &= 0xFF;
            break;

        case 2:
            data &= 0xFFFF;
            break;

        case 4:
            break;

        default:
            return USB_MEM_IF_ERR_BADPARAM;
    }

    ret = libusb_control_transfer(host_p->usb_h,
            /* bmRequestType */
            (LIBUSB_ENDPOINT_OUT | LIBUSB_REQUEST_TYPE_VENDOR |
                    LIBUSB_RECIPIENT_INTERFACE),
                    /* bRequest      */ QCC_WRITE,
                    /* wValue        */ (uint16_t)bid,
                    /* MSB 4 bytes   */
                    /* wIndex        */ (uint16_t)addr,
                    /* Data          */ (unsigned char *)&data,
                    /* wLength       */ (uint16_t)width,
                    /* timeout       */ 0
    );
    if (ret < 0) {
        printf("usb_mem_interface_write_qcc error: vendor req QCC_WRITE. %d\n", ret);
        return ret;
    }

    ret = libusb_control_transfer(host_p->usb_h,
            /* bmRequestType */
            (LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_VENDOR |
                    LIBUSB_RECIPIENT_INTERFACE),
                    /* bRequest      */ QCC_WRITE_STATUS,
                    /* wValue        */ 0,
                    /* wIndex        */ 0,
                    /* Data          */ status,
                    /* wLength       */ sizeof(status),
                    /* timeout       */ 0
    );

    if (ret < 0) {
        printf("usb_mem_interface_write_qcc error: vendor req QCC_WRITE_STATUS. %d\n", ret);
        return ret;
    }

    cmd_sta = *(unsigned char *)status;
    //if (cmd_sta >= MXCAM_ERR_FAIL) {
    if (cmd_sta >= 128) {
        return cmd_sta;
    }

    return USB_MEM_IF_SUCCESS;
}

static int usb_mem_interface_read_qcc_internal(void* host_h, unsigned long bid,
              unsigned long addr, unsigned long *data_p, int width)
{
    st_usb_mem_if*host_p  = (st_usb_mem_if*)host_h;
    int mask, ret = USB_MEM_IF_SUCCESS;

    if (data_p == NULL)
        return USB_MEM_IF_ERR_BADPARAM;

    if (!QCC_BID_OK(bid) || !QCC_ADDR_OK(addr))
        return USB_MEM_IF_ERR_BADPARAM;

    switch (width)
    {
        case 1:
            mask = 0xFF;
            break;

        case 2:
            mask = 0xFFFF;
            break;

        case 4:
            mask = 0xFFFFFFFF;
            break;

        default:
            return USB_MEM_IF_ERR_BADPARAM;
    }

    ret = libusb_control_transfer(host_p->usb_h,
            /* bmRequestType */
            (LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_VENDOR |
                    LIBUSB_RECIPIENT_INTERFACE),
                    /* bRequest      */ QCC_READ,
                    /* wValue        */ (uint16_t)bid,
                    /* wIndex        */ (uint16_t)addr,
                    /* Data          */ (unsigned char*)data_p,
                    /* wLength       */ (uint16_t)width,
                    /* timeout       */ 0
    );

    if (ret < 0) {
        printf("usb_mem_interface_read_qcc_internal error: vendor req QCC_READ. %d\n", ret);
        return ret;
    }

    *data_p &= mask;

    return USB_MEM_IF_SUCCESS;
}

int usb_mem_interface_read_qcc(void* host_h,
        int bid, int addr, unsigned long *data_p, int len)
{

    int ret;

    if (usb_mem_interface_lock(host_h) != USB_MEM_IF_SUCCESS){
        printf("ERR: USB_MEM_IF_ERR_LOCK\n");
        return USB_MEM_IF_ERR_LOCK;
    }

    ret = usb_mem_interface_read_qcc_internal(host_h, bid, addr, data_p, len);
    usb_mem_interface_unlock(host_h);
    return ret;
}

int usb_mem_interface_write_qcc(void* host_h,
        int bid, int addr, unsigned long data, int len)
{
    int ret;

    if (usb_mem_interface_lock(host_h) != USB_MEM_IF_SUCCESS)
        return USB_MEM_IF_ERR_LOCK;

    ret = usb_mem_interface_write_qcc_internal(host_h, bid, addr, data, len);
    usb_mem_interface_unlock(host_h);
    return ret;
}



static unsigned long GetPartitionBase(void* host_h, int partition)
{
    unsigned long reg;
    int ret;
    int qba = PARTITION_BASE_ADDR(partition);
    ret = usb_mem_interface_read_qcc_internal(host_h, QCC_BID_PMU0, qba, &reg,
            REG_BYTES(MEMS_MEM0_PMU_x_PartBaseSeg0));
    if (ret != USB_MEM_IF_SUCCESS){
        printf("ERR: USB_MEM_IF_SUCCESS\n");
        return ret;
    }

    return reg;
}

static int usb_mem_interface_get_partbase_internal(void* host_h,
               unsigned char bid, unsigned long* base_p)
{
    unsigned long seg_aw;   // address width for this segment
    unsigned long reg;
    unsigned long reg2;
    int ret = 0;
    st_usb_mem_if*host_p  = (st_usb_mem_if*)host_h;

    if ( (host_p->curr_part_id == bid) && (host_p->curr_part_base >= 0) )
    {
        *base_p = host_p->curr_part_base;
        return USB_MEM_IF_SUCCESS;
    }

    ret = usb_mem_interface_read_qcc_internal(host_h, QCC_BID_PMU0,
                REG_ADDR(MEMS_MEM0_PMU_x_VPATSegSize), &reg,
                REG_BYTES(MEMS_MEM0_PMU_x_VPATSegSize));
    //printf("em_get_partbase reg 0x%x ret %d\n", reg, ret);

    EXIT_ON_ERROR(ret);

    seg_aw = VPAT_SXSW - 3
    + ((reg >> QCCFIELD_VPATSEGXSIZE_SHIFT)
       & QCCFIELD_VPATSEGXSIZE_MASK)
    + VPAT_SYSW - 3
    + ((reg >> QCCFIELD_VPATSEGYSIZE_SHIFT)
       & QCCFIELD_VPATSEGYSIZE_MASK);

    reg2 = GetPartitionBase(host_p, XTENSA_PARTITION_ID);
    reg  = GetPartitionBase(host_p, bid);

    *base_p = (reg - reg2) << seg_aw;
    host_p->curr_part_id = bid;
    host_p->curr_part_base = *base_p;

 err_exit:

    //printf("em_get_partbase DONE. reg 0x%x ret %d\n", reg, ret);

    return ret;
}

int usb_mem_interface_get_partbase(void* host_h,
            unsigned char bid, unsigned long* base_p)
{

    int ret;

    if (base_p == NULL)
    return USB_MEM_IF_ERR_BADPARAM;

    if (!EM_BID_OK(bid))
    return USB_MEM_IF_ERR_BADPARAM;


    if (usb_mem_interface_lock(host_h) != USB_MEM_IF_SUCCESS)
        return USB_MEM_IF_ERR_LOCK;

    ret = usb_mem_interface_get_partbase_internal(host_h, bid, base_p);

    usb_mem_interface_unlock(host_h);

    return ret;
}


int usb_mem_interface_read_bytes(void* host_h, unsigned char bid,
              unsigned long addr, char *buffer_p, int nBytes)
{

    unsigned long base;
    int ret;
    int bytesRemain = nBytes;
    int bytesToRead = 0;
    int offset = 0;

    //printf("qhalem_read_bytes bid %d addr 0x%x bytes %d\n", bid, (int)addr, nBytes);

    if (buffer_p == NULL)
        return USB_MEM_IF_ERR_BADPARAM;

    if (!EM_BID_OK(bid) || !EM_ADDR_OK(addr))
        return USB_MEM_IF_ERR_BADPARAM;


    if (usb_mem_interface_lock(host_h) != USB_MEM_IF_SUCCESS)
        return USB_MEM_IF_ERR_LOCK;

    ret = usb_mem_interface_get_partbase_internal(host_h, bid, &base);
    EXIT_ON_ERROR(ret);

#if 1
#if 0
    ret = usb_mem_interface_read_block(host_h, base + addr,
            (unsigned long *)buffer_p, 1, nBytes);
#else
    bytesRemain = nBytes;
    offset = 0;
    while (bytesRemain > 0)
    {
        bytesToRead = (bytesRemain > RAPTOR_USB_MAX_XFER_SZ) ? RAPTOR_USB_MAX_XFER_SZ : bytesRemain;
        ret = usb_mem_interface_read_block(host_h, base + addr + offset,
                (unsigned long *)(buffer_p + offset), 1, bytesToRead);
        offset += bytesToRead;
        bytesRemain -= bytesToRead;
    }
#endif
#else
    // for testing purpose
    ret = usb_mem_interface_read(host_h, base + addr,
            (unsigned long *)buffer_p, nBytes);
#endif


    err_exit:
    usb_mem_interface_unlock(host_h);

    //printf("qhalem_read_bytes DONE. bid %d addr 0x%x bytes %d\n", bid, (int)addr, nBytes);

    return ret;
}

int usb_mem_interface_read_words(void* host_h, unsigned char bid,
              unsigned long addr, long *buffer_p, int nWords)
{

    unsigned long base = 0;
    int ret;
    int bytesRemain = nWords * 4;
    int bytesToRead = 0;
    int offset = 0;
#if __BYTE_ORDER != TARGET_BYTE_ORDER
    int i;
    unsigned char *pbuf;
#endif

    //printf("qhalem_read_words bid %d addr 0x%x words %d\n", bid, (int)addr, nWords);

    if (buffer_p == NULL)
        return USB_MEM_IF_ERR_BADPARAM;

    if (!EM_BID_OK(bid) || !EM_ADDR_OK(addr))
        return USB_MEM_IF_ERR_BADPARAM;
    if (usb_mem_interface_lock(host_h) != USB_MEM_IF_SUCCESS){
         printf("ERR: USB_MEM_IF_ERR_LOCK");
        return USB_MEM_IF_ERR_LOCK;
    }
    ret = usb_mem_interface_get_partbase_internal(host_h, bid, &base);
    if(ret)
        printf("ERR: usb_mem_interface_get_partbase_internal failed\n");
    EXIT_ON_ERROR(ret);

#if 0
    ret = usb_mem_interface_read_block(host_h, base + addr,
            (unsigned long *)buffer_p, 4, nWords);
#else
    bytesRemain = nWords * 4;
    offset = 0;
    while (bytesRemain > 0) {
        bytesToRead = (bytesRemain > RAPTOR_USB_MAX_XFER_SZ) ? RAPTOR_USB_MAX_XFER_SZ : bytesRemain;
        ret = usb_mem_interface_read_block(host_h, base + addr + offset,
#ifdef ARCH_x64
		(unsigned long *)((unsigned long)buffer_p + offset),               
#else
 		(unsigned long *)((unsigned int)buffer_p + offset),
#endif
		 4, bytesToRead/4);
        offset += bytesToRead;
        bytesRemain -= bytesToRead;
    }
#endif
#if __BYTE_ORDER != TARGET_BYTE_ORDER
    pbuf = (unsigned char *)buffer_p;
    for (i = 0; i < nWords * 4; i += 4) {
        char tmp = pbuf[i];
        pbuf[i] = pbuf[i+3];
        pbuf[i+3] = tmp;
        tmp = pbuf[i+1];
        pbuf[i+1] = pbuf[i+2];
        pbuf[i+2] = tmp;
    }
#endif


    err_exit:
    usb_mem_interface_unlock(host_h);

    //printf("qhalem_read_words DONE. bid %d addr 0x%x words %d\n", bid, (int)addr, nWords);

    return ret;
}

int usb_mem_interface_write_bytes(void* host_h, unsigned char bid,
               unsigned long addr, char *buffer_p, int nBytes)
{
    unsigned long base;
    int ret;
    int bytesRemain = nBytes;
    int bytesToWrite = 0;
    int offset = 0;

    //printf("qhalem_write_bytes bid %d addr 0x%x bytes %d\n", bid, (int)addr, nBytes);

    if (buffer_p == NULL)
        return USB_MEM_IF_ERR_BADPARAM;

    if (!EM_BID_OK(bid) || !EM_ADDR_OK(addr))
        return USB_MEM_IF_ERR_BADPARAM;



    if (usb_mem_interface_lock(host_h) != USB_MEM_IF_SUCCESS)
        return USB_MEM_IF_ERR_LOCK;

    ret = usb_mem_interface_get_partbase_internal(host_h, bid, &base);
    EXIT_ON_ERROR(ret);

#if 0
    ret = usb_mem_interface_write_block(host_h, base + addr,
            (unsigned long *)buffer_p, 1, nBytes);
#else
    bytesRemain = nBytes;
    offset = 0;
    while (bytesRemain > 0) {
        bytesToWrite = (bytesRemain > RAPTOR_USB_MAX_XFER_SZ) ? RAPTOR_USB_MAX_XFER_SZ : bytesRemain;
        ret = usb_mem_interface_write_block(host_h, base + addr + offset,
                (unsigned long *)(buffer_p + offset), 1, bytesToWrite);
        offset += bytesToWrite;
        bytesRemain -= bytesToWrite;
    }
#endif

    err_exit:
    usb_mem_interface_unlock(host_h);

    //printf("qhalem_write_bytes DONE. bid %d addr 0x%x bytes %d\n", bid, (int)addr, nBytes);

    return ret;
}

int usb_mem_interface_write_words(void* host_h, unsigned char bid,
               unsigned long addr, long *buffer_p, int nWords)
{
    unsigned long base;
    int ret;
    int bytesRemain;
    int bytesToWrite = 0;
    int offset = 0;
#if __BYTE_ORDER == LITTLE_ENDIAN
    int i;
    unsigned char *pbuf;
#endif

    //printf("qhalem_write_words bid %d addr 0x%x words %d\n", bid, (int)addr, nWords);

    if (buffer_p == NULL)
        return USB_MEM_IF_ERR_BADPARAM;

    if (!EM_BID_OK(bid) || !EM_ADDR_OK(addr))
        return USB_MEM_IF_ERR_BADPARAM;


    if (usb_mem_interface_lock(host_h) != USB_MEM_IF_SUCCESS)
        return USB_MEM_IF_ERR_LOCK;

    ret = usb_mem_interface_get_partbase_internal(host_h, bid, &base);
    EXIT_ON_ERROR(ret);

#if __BYTE_ORDER == LITTLE_ENDIAN
    pbuf = (unsigned char *)buffer_p;
    for (i = 0; i < nWords * 4; i += 4) {
        char tmp = pbuf[i];
        pbuf[i] = pbuf[i+3];
        pbuf[i+3] = tmp;
        tmp = pbuf[i+1];
        pbuf[i+1] = pbuf[i+2];
        pbuf[i+2] = tmp;
    }
#endif

#if 0
    ret = usb_mem_interface_write_block(em_p->host_h, base + addr,
            (unsigned long *)buffer_p, 4, nWords);
#else
    bytesRemain = nWords * 4;
    offset = 0;
    while (bytesRemain > 0) {
        bytesToWrite = (bytesRemain > RAPTOR_USB_MAX_XFER_SZ) ? RAPTOR_USB_MAX_XFER_SZ : bytesRemain;
        ret = usb_mem_interface_write_block(host_h, base + addr + offset,
                (unsigned long *)(buffer_p + offset), 4, bytesToWrite/4);
        offset += bytesToWrite;
        bytesRemain -= bytesToWrite;
    }
#endif

    err_exit:
    usb_mem_interface_unlock(host_h);

    //printf("qhalem_write_words DONE. bid %d addr 0x%x words %d\n", bid, (int)addr, nWords);

    return ret;
}

#if 0
static int usb_mem_interface_wait(void* host_h,unsigned long time_ms)
{
    if (time_ms)
    if (usleep(time_ms*1000) < 0)
        return USB_MEM_IF_ERR_IO;
    return USB_MEM_IF_SUCCESS;
}
static int usb_reset(st_usb_mem_if *host_p)
{
    int ret = libusb_reset_device(host_p->usb_h);

    // Re-enumeration is necessary:
    if (ret == LIBUSB_ERROR_NOT_FOUND) {
    libusb_close(host_p->usb_h);
    host_p->usb_h = libusb_open_device_with_vid_pid(usb_context,
                            USB_VENDOR_ID_MAXIM, USB_DEVICE_ID_MAXIM_RAPTOR);
    if (host_p->usb_h == NULL)
        return USB_MEM_IF_ERR_NODEV;

    ret = usb_configure(host_p);
    }
    return ret ? USB_MEM_IF_ERR_IO : USB_MEM_IF_SUCCESS;
}

static inline int usb_reset_ep(st_usb_mem_if *host_p, raptor_usb_ep_t ep)
{
    int ret = libusb_clear_halt(host_p->usb_h, ep);

    return ret ? USB_MEM_IF_ERR_IO : USB_MEM_IF_SUCCESS;
}

static int usb_rw(st_usb_mem_if *host_p,//TODO
          raptor_usb_vr_hdr_t *vr_p, void *data_p)
{
    vr_p->data = 0;
    vr_p->ret = 0;
    return USB_MEM_IF_SUCCESS;
}
static int usb_mem_interface_reset(void* host_h)
{
    st_usb_mem_if*host_p  = (st_usb_mem_if*)host_h;
    raptor_usb_vr_hdr_t vr;
    int ret;

    // Request the reset:
    //vr.vr = USB_VR_RESET_CHIP;
    ret = usb_rw(host_p, &vr, NULL);
    EXIT_ON_EITHER_ERROR(ret, vr.ret);

    // Wait for the interface to come back:
    ret = usb_mem_interface_wait(host_h, RESET_MS);
    EXIT_ON_ERROR(ret);

    // Now reset our nexus:
    ret = usb_reset(host_p);

 err_exit:
    return ret;
}
static int usb_mem_interface_read(void* host_h,
          unsigned long addr, unsigned long *data_p, int width)
{
    st_usb_mem_if*host_p  = (st_usb_mem_if*)host_h;
    int mask, ret;
    unsigned char wid = (unsigned char)width;
    uint16_t addr_hi;
    uint16_t addr_lo;

    if (data_p == NULL)
        return USB_MEM_IF_ERR_BADPARAM;

    switch (width)
    {
        case 1:
            mask = 0xFF;
            break;

        case 2:
            mask = 0xFFFF;
            break;

        case 4:
            mask = 0xFFFFFFFF;
            break;

        default:
            return USB_MEM_IF_ERR_BADPARAM;
    }

    addr_hi = (uint16_t)(addr >> 16);
    addr_lo = (uint16_t)(addr & 0xFFFF);

    ret = libusb_control_transfer(host_p->usb_h,
            /* bmRequestType */
            (LIBUSB_ENDPOINT_OUT | LIBUSB_REQUEST_TYPE_VENDOR |
                    LIBUSB_RECIPIENT_INTERFACE),
                    /* bRequest      */ MEM_READ,
                    /* wValue        */ (uint16_t)addr_hi,
                    /* MSB 4 bytes   */
                    /* wIndex        */ (uint16_t)addr_lo,
                    /* Data          */ &wid,
                    /* wLength       */ 1,
                    /* timeout       */ 0
    );
    if (ret < 0) {
        printf("usb_mem_interface_read error: vendor req MEM_READ. %d\n", ret);
        return ret;
    }

    ret = libusb_control_transfer(host_p->usb_h,
            /* bmRequestType */
            (LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_VENDOR |
                    LIBUSB_RECIPIENT_INTERFACE),
                    /* bRequest      */ MEM_READ_STATUS,
                    /* wValue        */ 0,
                    /* wIndex        */ 0,
                    /* Data          */ (unsigned char*)data_p,
                    /* wLength       */ (uint16_t)width,
                    /* timeout       */ 0
    );

    if (ret < 0) {
        printf("usb_mem_interface_read error: vendor req MEM_READ_STATUS. %d\n", ret);
        return ret;
    }

    *data_p &= mask;

    return USB_MEM_IF_SUCCESS;
}

static int usb_mem_interface_wait_ns(void* host_h,unsigned long time_ns)
{
    struct timespec req = { 0, time_ns };
    if (time_ns)
    if (nanosleep(&req, NULL) < 0)
        return USB_MEM_IF_ERR_IO;
    return USB_MEM_IF_SUCCESS;
}

static int usb_mem_interface_write(void* host_h,
           unsigned long addr, unsigned long data, int width)
{
    st_usb_mem_if*host_p  = (st_usb_mem_if*)host_h;
    int ret;
    uint16_t addr_hi;
    uint16_t addr_lo;
    unsigned char status[64];
    unsigned char cmd_sta;

    switch (width)
    {
        case 1:
            data &= 0xFF;
            break;

        case 2:
            data &= 0xFFFF;
            break;

        case 4:
            break;

        default:
            return USB_MEM_IF_ERR_BADPARAM;
    }

    addr_hi = (uint16_t)(addr >> 16);
    addr_lo = (uint16_t)(addr & 0xFFFF);

    ret = libusb_control_transfer(host_p->usb_h,
            /* bmRequestType */
            (LIBUSB_ENDPOINT_OUT | LIBUSB_REQUEST_TYPE_VENDOR |
                    LIBUSB_RECIPIENT_INTERFACE),
                    /* bRequest      */ MEM_WRITE,
                    /* wValue        */ (uint16_t)addr_hi,
                    /* MSB 4 bytes   */
                    /* wIndex        */ (uint16_t)addr_lo,
                    /* Data          */ (unsigned char *)&data,
                    /* wLength       */ (uint16_t)width,
                    /* timeout       */ 0
    );
    if (ret < 0)
    {
        printf("usb_mem_interface_write error: vendor req MEM_WRITE. %d\n", ret);
        return ret;
    }

    ret = libusb_control_transfer(host_p->usb_h,
            /* bmRequestType */
            (LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_VENDOR |
                    LIBUSB_RECIPIENT_INTERFACE),
                    /* bRequest      */ MEM_WRITE_STATUS,
                    /* wValue        */ 0,
                    /* wIndex        */ 0,
                    /* Data          */ status,
                    /* wLength       */ sizeof(status),
                    /* timeout       */ 0
    );

    if (ret < 0)
    {
        printf("usb_mem_interface_write error: vendor req MEM_READ_STATUS. %d\n", ret);
        return ret;
    }

    cmd_sta = *(unsigned char *)status;
    //if (cmd_sta >= MXCAM_ERR_FAIL) {
    if (cmd_sta >= 128)
    {
        return cmd_sta;
    }

    return USB_MEM_IF_SUCCESS;
}
#endif
