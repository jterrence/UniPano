/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
#ifndef __DEWARP_TYPES_H
#define __DEWARP_TYPES_H

#define REAL float


typedef struct adj_list {
   float angle;
   float x_adj;
   float y_adj;
} ADJ_LIST_TYPE;

typedef struct radmap {
   int   valid;
   float angle;
   float x_scale;
   float y_scale;
} RADIAL_MAP_TYPE;

typedef struct
{
    float ** m;
    int r;
    int c;
} MATRIX2D;

typedef struct
{
    MATRIX2D *pX;
    MATRIX2D *pY;
} OUTPUT_MATRIX;











#endif
