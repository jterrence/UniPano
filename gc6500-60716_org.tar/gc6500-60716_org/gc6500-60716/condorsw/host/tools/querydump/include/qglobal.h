/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

/*******************************************************************************
* @(#) $Id: qglobal.h 50214 2014-10-14 16:49:48Z athai $
*******************************************************************************/

// ## Release Control Block
// ## Level 1

#ifndef __QGLOBAL_HH
#define __QGLOBAL_HH

#define PROD_CONFIG_OFFSET   0x40
#define SYS_APP_PCFG         (PROD_CONFIG_OFFSET / 4)

//////////////////////////////////////////////////////////////////////
//
// Commands which are accepted by all interface objects
//
#define Q_CMD_OPCODE_GLOBAL_START           1024

#define Q_CMD_OPCODE_SUBSCRIBE_EVENT        1024
#define Q_CMD_OPCODE_UNSUBSCRIBE_EVENT      1025
#define Q_CMD_OPCODE_CONFIGURE              1026
#define Q_CMD_OPCODE_SET_NAME               1027
#define Q_CMD_OPCODE_GET_NAME               1028

#define Q_CMD_OPCODE_GLOBAL_END             1028

#define MAX_COMMAND_ARGS                    (20)
#define MAX_COMMAND_RVALS                   (5)
#define MAX_EVENT_PAYLOAD                   (21)

#define SYSTEMCONTROL_CTRLOBJ_ID            1

#define SYSCTRL_CTRLOBJ_TYPE                1
#define LVPP_CTRLOBJ_TYPE                   2
#define NVPP_CTRLOBJ_TYPE                   3
#define AVOC_CTRLOBJ_TYPE                   4
#define AVCENC_CTRLOBJ_TYPE                 5
#define HVCAP_CTRLOBJ_TYPE                  6
#define HVOUT_CTRLOBJ_TYPE                  7
#define SAIN_CTRLOBJ_TYPE                   8
#define AVSN_CTRLOBJ_TYPE                   9
#define AVDEC_CTRLOBJ_TYPE                 10
#define AUDENC_CTRLOBJ_TYPE                11
#define HAOUT_CTRLOBJ_TYPE                 12
#define JPEGENC_CTRLOBJ_TYPE               13
#define ASPLIT_CTRLOBJ_TYPE                14
#define HACAP_CTRLOBJ_TYPE                 15
#define AUDFLTR_CTRLOBJ_TYPE               16
#define ISP_CTRLOBJ_TYPE                   17
#define USBR_CTRLOBJ_TYPE                  18
#define VCAP_CTRLOBJ_TYPE                  19
#define RAWCAP_CTRLOBJ_TYPE                20
#define DEWARP_CTRLOBJ_TYPE                21
#define FBR_CTRLOBJ_TYPE                   22
#define OV_CTRLOBJ_TYPE                    23

#define MAX_CTRLOBJ_ID                      (256)
#define MAX_EVENTS_PER_CTRLOBJ              (128)
#define MAX_CTRLOBJ_NAME_LEN                (16)

#define SENSOR_EXPOSURE_REG_NUM             32

//////////////////////////////////////////////////////////////////////
//
// Types
//

#define ENCODE_EVENT(OBJ, ID) ((OBJ << 16) | ID)

typedef int CONTROLOBJECT_ID;
typedef int EVENT_ID;
typedef int CONTROLOBJECT_STATE;
typedef int CONTROLOBJECT_ADDR;

/// \brief Event structure dispatched by QMM firmware
///
/// \ingroup structs
///
/// This event structure is very close to the QVEvent class
/// and should continue to track its development.  The
/// difference between this type and QVEvent are the first
/// two arguments.  In QVEvent, the first field is DATA, 
/// and the second is the event type whereas the first
/// here is the event type and the second is the 
/// control object ID.
typedef struct
{
    CONTROLOBJECT_ID    controlObjectId;
    EVENT_ID            eventId;
    unsigned int        timestamp;
    unsigned int        payload[MAX_EVENT_PAYLOAD];
} EVENT;


/// \brief Command block in QMM address space
///
/// \ingroup structs
///
typedef struct
{
    CONTROLOBJECT_ID    controlObjectId;
    unsigned int        opcode;
    unsigned int        arguments[MAX_COMMAND_ARGS];
    int                 returnCode;
    unsigned int        returnValues[MAX_COMMAND_RVALS];   
} COMMAND;

typedef struct
{
    CONTROLOBJECT_ID    controlObjectId;
    unsigned int        opcode;
    unsigned int        arguments[MAX_COMMAND_ARGS];
} SHORTCMD;

typedef struct
{
    unsigned int sec;
    unsigned int usec;
} NTP_TIME;

/// \brief NTP block
typedef struct
{
    NTP_TIME ntpBlock0;
    NTP_TIME ntpBlock1;
    unsigned int ntpIndex;
} NTP_BLOCK;

#ifdef ARCH_x64
// this change is required for querydump / host tools
// which can run on 64bit machine.
// ARCH_x64 is passed as defined from Makefiles of querydump.
typedef struct
{
    CONTROLOBJECT_ADDR obj;
    int type;
    unsigned int statBlock;
    unsigned int statBlockSize;
    char name[MAX_CTRLOBJ_NAME_LEN];
} CONTROLOBJ_DESC;
#else
// must match SystemControlCtrlObj.h CONTROLOBJ_DSCR
typedef struct
{
    CONTROLOBJECT_ADDR obj;
    int type;
    unsigned char *statBlock;
    unsigned int statBlockSize;
    char name[MAX_CTRLOBJ_NAME_LEN];
} CONTROLOBJ_DESC;
#endif

#define COMMAND_CONTROL_OBJECT_ID_OFFSET (0)
#define COMMAND_OPCODE_OFFSET (4)
#define COMMAND_ARGUMENTS_OFFSET (8)
#define COMMAND_RETURN_CODE_OFFSET (32)
#define COMMAND_RETURN_VALUES_OFFSET (36)

/// \brief Global pointer block in QMM address space
///
/// \ingroup structs
///
#ifdef ARCH_x64
// this change is required for querydump / host tools
// which can run on 64bit machine.
// ARCH_x64 is passed as defined from Makefiles of querydump.
typedef struct
{
    unsigned int                cmdBlock;
    unsigned int                evBlock;
    unsigned int                systemControlStatus;
    unsigned int                ctrlObjects;
    unsigned int                DebugLog;
    unsigned int                evReadWritePtrs;
    unsigned int                ntpBlock;
    unsigned int                cmdBlockList;
    unsigned int                ipcCmd;
    unsigned int                ipcEvt;
} GLOBAL_POINTER_BLOCK;
#else
typedef struct
{
    COMMAND                     *cmdBlock;
    EVENT                       *evBlock;
    void                        *systemControlStatus;
    void                        *ctrlObjects;
    void                        *DebugLog;
    unsigned int                 evReadWritePtrs;
    NTP_BLOCK                   *ntpBlock;
    SHORTCMD                    *cmdBlockList;
    void                        *ipcCmd;
    void                        *ipcEvt;
} GLOBAL_POINTER_BLOCK;
#endif

#define COMMAND_BLOCK_OFFSET            (0)
#define EVENT_BLOCK_OFFSET              (4)
#define SYSTEM_CONTROL_STATUS_OFFSET    (8)
#define ME_GLOBAL_STATUS_OFFSET         (12)
#define EVENT_PTRS_OFFSET               (20)
#define NTP_BLOCK_OFFSET                (24)
#define COMMAND_BLOCK_LIST_OFFSET       (28)
#define GLOBAL_POINTER_BLOCK_OFFSET     (0xffc)

// ## level 2
#define PRIVATE_LEVEL_2

// ## level 1
#define PUBLIC_1
#define PUBLIC_2
#define PUBLIC_3

#define PRIVATE_LEVEL_3 // ## level 3
#define PUBLIC_4

#endif  /* __QGLOBAL_HH */

