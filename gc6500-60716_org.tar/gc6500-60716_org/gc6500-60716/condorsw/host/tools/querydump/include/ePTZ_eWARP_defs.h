/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

/*******************************************************************************
* @(#) $Id: h.skel,v 1.17 2013/01/03 02:20:21 monsen Exp $
*******************************************************************************/
#ifndef EPTZ_EWARP_DEFS_H
#define EPTZ_EWARP_DEFS_H
#include "dewarp_types.h"

//////////////////////////////////////////////////////////////////////////////////////////
//
// ePTZ Modes
//
/// \brief The following constants are defined for use with function gridgenerator().  When
///  gridgenerator is called, params[7] is set to one of these constants to set the
/// dewarp mode. Please refer to GEO Semiconductor's "ePTZ Modes" document for more information
//
typedef enum __EPTZ_MAP_TYPES
{
    EMODE_EPTZ_OFF                             =   0,
    EMODE_EPTZ_INVALID                         =   1,
    EMODE_WM_ZCL                               = 110,
    EMODE_WM_ZCLCylinder                       = 111,
    EMODE_WM_ZCLStretch                        = 112,
    EMODE_WM_1PanelEPTZ                        = 120,
    EMODE_WM_1PanelEPTZ_4pt                    = 121,
    EMODE_WM_4PanelEPTZ                        = 130,
    EMODE_WM_4DepPanelEPTZ                     = 131,
    EMODE_WM_FullView_3PanelEPTZ               = 140,
    EMODE_WM_ZCLView_3PanelEPTZ                = 141,
    EMODE_WM_ZCLCylinderView_3PanelEPTZ        = 142,
    EMODE_WM_PanoView_2PanelEPTZ               = 150,
    EMODE_CM_2CircPanoViewPan                  = 160,
    EMODE_CM_CircPanoViewPan                   = 161,
    EMODE_CM_360CircPanoView_2PanelEPTZ        = 170,
    EMODE_CM_4PanelEPTZ                        = 180,
    EMODE_CM_FullView_3PanelEPTZ               = 190,
    EMODE_CM_FullViewTL_1PanelEPTZ             = 200,
    EMODE_TM_2CircPanoViewPan                  = 210,
    EMODE_TM_1CircPanoViewPan                  = 211,
    EMODE_TM_360CircPanoView_1PanelNonPerspPan = 212,
    EMODE_TM_360CircPanoView_2PanelNonPerspPan = 220,
    EMODE_TM_360CircPanoView_2PanelPan         = 221,
    EMODE_TM_360CircPanoView_4PanelPan         = 230,
    EMODE_SWEEP_WM_1PANELEPTZ                  = 400,
    EMODE_WM_Magnify                           = 401,
    EMODE_WM_Flush                             = 402,
    EMODE_CUSTOM_CAM360                        = 403,
    EMODE_DEMO_CUSTOM_CAM360                   = 404,
    EMODE_CAM360_EPTZ_LEFT_PANEL               = 405,
    EMODE_CAM360_EPTZ_RIGHT_PANEL              = 406,
    EMODE_CAM360_ZCLCYLINDER_LEFT              = 407,
    EMODE_CAM360_ZCLCYLINDER_RIGHT             = 408,
    EMODE_WM_ROI_1PanelEPTZ                    = 450,             //This is a special mode which requires conversion to EMODE_WM_1PanelEPTZ by mainapp 
    EMODE_EPTZ_MAX                             = 500,
}EPTZ_MAP_TYPES;


#define EMODE_MAX_NUM_OF_PARAMS               32

#define GEMAP_MAX_NUM_OF_COEEFS               2048

#define GEMAP_RETCODE_OK            0
#define GEMAP_RETCODE_ERR_NOMAP     -1
#define GEMAP_RETCODE_ERR_SOMETHING -1

#define MAP_MAX_NUM_OF_RADIAL_MAPS          360
#define MAP_MAX_NUM_OF_ADJ_POINTS           32

typedef struct __map_param_struct 
{
    short Divisor;
    short Panel;
    int Params[22];
}map_param_struct;

typedef struct __map_header_struct
{
    EPTZ_MAP_TYPES  Type;
    short InputFrameWidth;
    short InputFrameHeight;
    short DewarpedFrameWidth;   // this is the output from the dewarp engine.
    short DewarpedFrameHeight;  // this is the output from the dewarp engine.
    short MapN;
    short MapWidth;
    short MapHeight;
    map_param_struct    mapParam;
}map_header_struct;

#endif  /* EPTZ_EWARP_DEFS_H */
/*******************************************************************************
* vi: set shiftwidth=4 tabstop=8 softtabstop=4 expandtab nosmarttab:
*******************************************************************************/

