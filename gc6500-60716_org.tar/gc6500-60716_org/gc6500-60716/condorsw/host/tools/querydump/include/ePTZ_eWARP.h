/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

/*******************************************************************************
* @(#) $Id: h.skel,v 1.17 2013/01/03 02:20:21 monsen Exp $
*******************************************************************************/
#ifndef EPTZ_EWARP_H
#define EPTZ_EWARP_H
#include <stdlib.h>
#include <string.h>
#include "dewarp_types.h"
#include "ePTZ_eWARP_defs.h"

typedef struct __gemap_struct : map_header_struct
{
    int   MapMaxNumOfCoeffs;
    int   MapNumOfCoeffs;
    short *pMapCoeffs;
}gemap_struct;

typedef struct __eptz_map_state_struct {
    float mFloats[EMODE_MAX_NUM_OF_PARAMS];
    ADJ_LIST_TYPE mRadialAdjPoints[MAP_MAX_NUM_OF_ADJ_POINTS];
    short mAdjPointsLen;
    short inWidth;
    short inHeight;
    short outWidth;
    short outHeight;
    short MapN;
    __eptz_map_state_struct()
    {
        Reset();
    }
    void Reset()
    {
        memset(mFloats, 0, sizeof(mFloats));
        memset(mRadialAdjPoints, 0, sizeof(mRadialAdjPoints));
        inWidth   = 0;
        inHeight  = 0;
        outWidth  = 0;
        outHeight = 0;
        MapN = 0;
        mAdjPointsLen = 0;
    }
    bool isChange(short inW, short inH, short outW, short outH, short N,
                const float *pParams=NULL, ADJ_LIST_TYPE*pAdjPoints=NULL, short AdjPointsLen=0)
    {
        int nogoto;
        bool bChanged = true;
        for(nogoto = 0; nogoto < 1; nogoto++)
        {
            if(inWidth != inW)
                break;
            if(inHeight != inH)
                break;
            if(outWidth != outW)
                break;
            if(outHeight != outH)
                break;
            if(MapN != N)
                break;
            if(mAdjPointsLen != AdjPointsLen)
                break;
            if(pParams)
            {
                if(memcmp(mFloats, pParams, sizeof(mFloats)) != 0)
                    break;
            }
            if(pAdjPoints)
            {
                if(memcmp(mRadialAdjPoints, pAdjPoints, AdjPointsLen * sizeof(ADJ_LIST_TYPE)) != 0)
                    break;
            }
            bChanged = false;
        }

        if(bChanged)
        {
            // save stuffs
            inWidth   = inW;
            inHeight  = inH;
            outWidth  = outW;
            outHeight = outH;
            MapN = N;
            mAdjPointsLen = AdjPointsLen;
            if(pParams)
                memcpy(mFloats, pParams, sizeof(mFloats));
            if(pAdjPoints)
                memcpy(mRadialAdjPoints, pAdjPoints, AdjPointsLen * sizeof(ADJ_LIST_TYPE));
        }
        return(bChanged);
    }
}eptz_map_state_struct;

typedef struct __eptz_handle_struct {
    int  state;     // 0 = unused, 1 = used
    int  maxMapWidth;
    int  maxMapHeight;
    void *posXM;
    void *posYM;
    void *posXMR;
    void *posYMR;
    eptz_map_state_struct mapStatus;
    short maxNumOfRadialMaps;
    short maxNumOfAdjPoints;
    RADIAL_MAP_TYPE RadialMaps[MAP_MAX_NUM_OF_RADIAL_MAPS];
    ADJ_LIST_TYPE   RadialAdjPoints[MAP_MAX_NUM_OF_ADJ_POINTS];
}eptz_handle_struct;



//////////////////////////////////////////////////////////////////////////////////////////
//
// gridgenerator
//
/// \brief The purpose of function gridgenerator is to generate a grid type map for the
///        dewarp engine.  Please refer to GEO Semiconductor's "ePTZ Modes" document for
///        more information.
/// \param pHandle a private data structure which has been returned by function eptz_init()
/// \param inWidth  Width of input image in pixels
/// \param inHeight Height of input image in pixels
/// \param outWidth Width of dewarped image in pixels
/// \param outHeight Height of dewarped image in pixels
/// \MapN  MapN parameter
/// \param params a generic array of parameters.  Please refer to GEO Semiconductor's "ePTZ Modes"
///        document for more information.
/// \return On success, this function returns GEMAP_RETCODE_OK to indicate there's no error
/// and the variable pMap contains the output map.  Otherwise, an error code is returned
/// and the contents of pMap is undefined.
///
int eptz_generate_gridmap(eptz_handle_struct *pHandle, short inWidth, short inHeight,
        short outWidth, short outHeight,
        short MapN, const float *params, gemap_struct *pMap);


//////////////////////////////////////////////////////////////////////////////////////////
//
// eptz_init
//
/// \brief The purpose of function epgz_init is to initialize the ePTZ Map Generator module.
///   This function must be called and must be called before all other functions in this module.
/// \param maxMapWidth Maximum Map Width.
/// \param maxMapHeight Maximum Map Height.
/// \return On success, this function returns a pointer to structure eptz_handle_struct.  This
/// pointer is needed for subsequent calls to other functions in this module.
///

eptz_handle_struct *eptz_init(short maxMapWidth, short maxMapHeight);

//////////////////////////////////////////////////////////////////////////////////////////
//
// eptz_deinit
//
/// \brief The purpose of function epgz_deinit is to deinitialize the ePTZ Map Generator module.
/// It deallocates memory and deallocate resources.
/// \param pHandle handle to be deallocated
/// \return On success, this function returns GEMAP_RETCODE_OK to indicate there's no error.
///

int eptz_deinit(eptz_handle_struct *pHandle);

//////////////////////////////////////////////////////////////////////////////////////////
//
// eptz_reset
//
/// \brief The purpose of function eptz_reset is to reset the map generation algo back to
/// initial state.
///
void eptz_reset(eptz_handle_struct *pHandle);

int mkmap_bypass_map ( eptz_map_state_struct *pMapStatus,
    int             dewarp_width,   // Width   (Dewarp-frame)
    int             dewarp_height,  // Height  (Dewarp-frame)
    int             src_width,      // Width   (Source-frame)
    int             src_height,     // Height  (Source-frame)
    int             MapN,
    gemap_struct   *pMap,
    int flip_horz,
    int flip_vert);

typedef enum {
    EMODE_CUSTOM_CAM360_RIGHT_PANEL,
    EMODE_CUSTOM_CAM360_LEFT_PANEL
}EMODE_CUSTOM_CAM360_PANEL_TYPE;

int calc_map_wh(int dewapSize, int MapN);
int SetRadialMap(eptz_handle_struct *pHandle, int angle, float x, float y);
int RemoveRadialEntry(eptz_handle_struct *pHandle, int angle);
int ResetRadialTable(eptz_handle_struct *pHandle);

#endif  /* EPTZ_EWARP_H */
/*******************************************************************************
* vi: set shiftwidth=4 tabstop=8 softtabstop=4 expandtab nosmarttab:
*******************************************************************************/

