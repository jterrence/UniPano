/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

/*******************************************************************************
* @(#) $Id: qstatus.h 60673 2017-02-22 09:19:43Z sagar.gupta $
*******************************************************************************/

// ## Release Control Block
// ## Level 1

#ifndef __QSTATUS_HH 
#define __QSTATUS_HH

#ifndef __MEDIAWARE_H

#include "qglobal.h"
#include "ePTZ_eWARP_defs.h"

///////////////////////////////////////////////////////////////////////////////
// 
// Start status blocks

/// \brief Video input block structure.  This struct is contained within
/// the LVPP status block structure.
typedef struct
{
/// \brief Internal debug state variable.
    unsigned int       debug;
/// \brief Number of video input interrupts.
    unsigned int       interrupts;
/// \brief Most recent time between video interrupts (in usec)
    unsigned int       nowinttime;
/// \brief Maximum time between video interrupts (in usec)
    unsigned int       maxinttime;
/// \brief Minimum interrupt processing time (in usec)
    unsigned int       mintime;
/// \brief Average interrupt processing time (in usec)
    unsigned int       avgtime;
/// \brief Maximum interrupt processing time (in usec)
    unsigned int       maxtime;
/// \brief Number of synchronization errors in frame-interleaved mode.
    unsigned int       syncerror;
/// \brief Number of null-frames detected in frame-interleaved mode.
    unsigned int       nullframes;
/// \brief Sync value detected in frame-interleaved mode with Techwell Channel ID.
    unsigned int       twsync;
/// \brief Hardware processing time in non-live mode
    unsigned int       load_hw;
/// \brief Hardware idle time in non-live mode
    unsigned int       load_idle;
/// \brief Software processing time in non-live mode
    unsigned int       load_sw;
} VIN_STATUS;

/// \brief FBR2 status block
typedef struct
{
    unsigned int       interrupts;
    unsigned int       mintime;
    unsigned int       avgtime;
    unsigned int       maxtime;
    unsigned int       load_hw;
    unsigned int       load_idle;
    unsigned int       load_sw;
} RAW_FBR_STATUS;

/// \brief Overlay status block
typedef struct
{
    int state;
} OVERLAY_STATUS;

/// \brief Audio input block structure.  
typedef struct
{
/// \brief Internal debug state variable.
    unsigned int       debug;
/// \brief Number of audio input interrupts.
    unsigned int       interrupts;
/// \brief Most recent time between audio interrupts (in usec)
    unsigned int       nowinttime;
/// \brief Maximum time between audio interrupts (in usec)
    unsigned int       maxinttime;
/// \brief Minimum interrupt processing time (in usec)
    unsigned int       mintime;
/// \brief Average interrupt processing time (in usec)
    unsigned int       avgtime;
/// \brief Maximum interrupt processing time (in usec)
    unsigned int       maxtime;
/// \brief Total number of captured audio frames
    unsigned int       framesCaptured;
/// \brief Total number of dropped frames due to capture failures (configuration error, missing interrupt etc.)
    unsigned int       captureFailures;
/// \brief Total number of dropped frames due to input buffer overflow
    unsigned int       captureOverflow;
/// \brief Sample numbers of an audio frame
    unsigned int       SampleNums; 
/// \brief rampup/rampdown time of audio input port (in msec)
    unsigned int       RampTime; 
} AIN_STATUS;

typedef struct
{
/// \brief Heartbeat for internal command processing thread
    int             heartbeat;
/// \brief Total number of events dropped due to slow host processing
    unsigned int    droppedEvents;
/// \brief Read/Write pointers into the event queue
    unsigned int    evReadWritePtrs;
/// \brief Flag indicating that an event has been sent to the host but no EVENT_DONE has been received
    int             pendingEvent;
/// \brief Number of bytes allocated from the pool of memory allocated to buffers
    unsigned int    allocatedPoolMemory;
/// \brief Maximum number of bytes available for buffer allocation
    unsigned int    totalPoolMemory;
/// \brief Total number of bytes allocated by processor 0
    unsigned int    heap0;
/// \brief Maximum allowable heap size for each processor
    unsigned int    heapSize;
/// \brief Video input stats for video port 0 (TFCC0), 1 (VPP0) and 2 (VSCALE0)
    VIN_STATUS      vinStatus[3];
/// \brief Audio input stats for audio port 0 and 1
    AIN_STATUS      ainStatus[2];
/// \brief Raw USB status
    RAW_FBR_STATUS  rawStatus;
/// \brief Overlay status block
    OVERLAY_STATUS  ovlStatus;
/// \brief Load average for the firmware's AVC encoder software control thread
    unsigned int    loadAVCEncSW;
/// \brief Load average for the firmware's AVC decoder software control thread
    unsigned int    loadAVCDecSW;
/// \brief Load average for the firmware's AVC encoder h/w block control thread
    unsigned int    loadAVCEncHW;
/// \brief Load average for the firmware's AVC decoder h/w block control thread
    unsigned int    loadAVCDecHW;
/// \brief Clock frequency for input clock (always 24Mhz)
    unsigned int    XINClockHz;
/// \brief Clock frequency for PLL0
    unsigned int    PLL0ClockHz;
/// \brief Clock frequency for PLL1
    unsigned int    PLL1ClockHz;
/// \brief Clock frequency for PLL2
    unsigned int    PLL2ClockHz;
/// \brief Clock frequency for CPU
    unsigned int    CPUClockHz;
/// \brief Clock rate in Hz of the interal media processors.
    unsigned int    QMMClockHz;
/// \brief Clock rate in Hz of AHB.
    unsigned int    AHBClockHz;
/// \brief Clock rate in Hz of APB.
    unsigned int    APBClockHz;
/// \brief Clock rate in Hz of UART0.
    unsigned int    UART0ClockHz;
/// \brief Clock rate in Hz of UART1.
    unsigned int    UART1ClockHz;
/// \brief Clock rate in Hz of I2C0.
    unsigned int    I2C0ClockHz;
/// \brief Clock rate in Hz of I2C1.
    unsigned int    I2C1ClockHz;
/// \brief Clock rate in Hz of the AUD master clock.  This is an I/O clock.
    unsigned int    AudClockHz;
/// \brief Clock rate in Hz of the AUD2 master clock.  This is an I/O clock.
    unsigned int    Aud2ClockHz;
/// \brief Clock frequency for pixel processing in VPP0.  Note that this is not an I/O clock but an internal clock that runs faster than the I/O clock.
    unsigned int    VSClockHz;
/// \brief Clock rate in Hz of the AVC codec block.
    unsigned int    AVCClockHz;
/// \brief Clock rate in Hz of the JPEG codec block.
    unsigned int    JPEGClockHz;
/// \brief Clock rate in Hz of the PME motion-estimation block.
    unsigned int    PMEClockHz;
/// \brief Clock rate in Hz of the VID block.
    unsigned int    VIDClockHz;
/// \brief Clock rate in Hz of the DW block.
    unsigned int    DWClockHz;
/// \brief Clock rate in Hz of the GPU block.
    unsigned int    GPUClockHz;
/// \brief Clock rate in Hz of DDR.
    unsigned int    DDRClockHz;
/// \brief Load average for the audio encoder thread
    unsigned int    loadAudEnc;
/// \brief Load average for the audio decoder thread
    unsigned int    loadAudDec;
/// \brief Load average for the firmware's mux thread
    unsigned int    loadMux;
/// \brief Load average for the firmware's AVC encoder hardware macroblock encoding
    unsigned int    loadAVCEncHWMB;
/// \brief Load average for usb isr
    unsigned int    loadUSBIsr;
/// \brief CPU load for last .1s
    unsigned int    loadCPUPt1s;
/// \brief CPU load for last 1s
    unsigned int    loadCPU1s;
/// \brief CPU load for last 10s
    unsigned int    loadCPU10s;
/// \brief Memory Clock in Hz
    unsigned int    memoryClockHz;
/// \brief Duration of measurement of memory stats
    unsigned int    mstatPeriodMs;
/// \brief MSTAT for cpu_rd
    unsigned int    mstatCpuRd;
/// \brief MSTAT for qmm_rd
    unsigned int    mstatQmmRd;
/// \brief MSTAT for
    unsigned int    mstatAhbPerRd;
/// \brief MSTAT for
    unsigned int    mstatSocFbr;
/// \brief MSTAT for
    unsigned int    mstatSbcRd;
/// \brief MSTAT for
    unsigned int    mstatTfiltRd;
/// \brief MSTAT for
    unsigned int    mstatFbr0;
/// \brief MSTAT for
    unsigned int    mstatFbr1;
/// \brief MSTAT for
    unsigned int    mstatVpp0RegRd;
/// \brief MSTAT for
    unsigned int    mstatPmeRd;
/// \brief MSTAT for
    unsigned int    mstatAvcRd0;
/// \brief MSTAT for
    unsigned int    mstatAvcRd1;
/// \brief MSTAT for
    unsigned int    mstatGpuRd;
/// \brief MSTAT for
    unsigned int    mstatJpegRd;
/// \brief MSTAT for
    unsigned int    mstatDewarpMapRd;
/// \brief MSTAT for
    unsigned int    mstatDewarpRd;
/// \brief MSTAT for
    unsigned int    mstatSpiRd;
/// \brief MSTAT for
    unsigned int    mstatCpuWr;
/// \brief MSTAT for 
    unsigned int    mstatQmmWr;
/// \brief MSTAT for
    unsigned int    mstatAhbPerWr;
/// \brief MSTAT for
    unsigned int    mstatSbcWr;
/// \brief MSTAT for
    unsigned int    mstatFbw0;
/// \brief MSTAT for
    unsigned int    mstatVpp0Wr;
/// \brief MSTAT for
    unsigned int    mstatVscl0Wr;
/// \brief MSTAT for
    unsigned int    mstatVscl1Wr;
/// \brief MSTAT for
    unsigned int    mstatAvcDb;
/// \brief MSTAT for
    unsigned int    mstatGpuWr;
/// \brief MSTAT for
    unsigned int    mstatAii0;
/// \brief MSTAT for
    unsigned int    mstatAii1;
/// \brief MSTAT for
    unsigned int    mstatJpegWr;
/// \brief MSTAT for
    unsigned int    mstatDewarpWr;
/// \brief MSTAT for
    unsigned int    mstatSpiWr;
/// \brief MSTAT for
    unsigned int    mstatVpp0StatWr;
/// \brief MSTAT for
    unsigned int    mstatPmeStatWr;
/// \brief MSTAT for
    unsigned int    mstatAvcBits;
/// \brief MSTAT for
    unsigned int    mstatAvcStat;
/// \brief uvc transfer success for ch 0
    unsigned int    uvcXferSuccess;
/// \brief uvc transfer success for ch 1
    unsigned int    uvcXferSuccess1;
/// \brief uvc transfer success for ch 2
    unsigned int    uvcXferSuccess2;
/// \brief uvc transfer success for ch 3
    unsigned int    uvcXferSuccess3;
/// \brief uvc transfer failed for ch 0
    unsigned int    uvcXferFailed;
/// \brief uvc transfer failed for ch 1
    unsigned int    uvcXferFailed1;
/// \brief uvc transfer failed for ch 2
    unsigned int    uvcXferFailed2;
/// \brief uvc transfer failed for ch 3
    unsigned int    uvcXferFailed3;
/// \brief uvc transfer count for ch 0
    unsigned int    uvcXferCnt;
/// \brief uvc transfer count for ch 1
    unsigned int    uvcXferCnt1;
/// \brief uvc transfer count for ch 2
    unsigned int    uvcXferCnt2;
/// \brief uvc transfer count for ch 3
    unsigned int    uvcXferCnt3;
} SYSTEM_CONTROL_STATUS;

typedef struct
{
/// \brief Internal debug variable
    unsigned int       vdebug;
/// \brief Total number of video output interrupts
    unsigned int       vinterrupts;
/// \brief Maximum time between video interrupts (in usec)
    unsigned int       vinttimemax;
/// \brief Minimum video interrupt processing time
    unsigned int       vmintime;
/// \brief Average video interrupt processing time
    unsigned int       vavgtime;
/// \brief Maximum video interrupt processing time
    unsigned int       vmaxtime;
/// \brief Minimum video rendering time (composition mode only)
    unsigned int       vminrend;
/// \brief Average video rendering time (composition mode only)
    unsigned int       vavgrend;
/// \brief Maximum video rendering time (composition mode only)
    unsigned int       vmaxrend;
/// \brief Total number of times the display buffer has been rendered (composition mode only)
    unsigned int       vrendcount;
/// \brief Minimum video display latency
    unsigned int       vminlatency;
/// \brief Average video display latency
    unsigned int       vavglatency;
/// \brief Maximum video display latency
    unsigned int       vmaxlatency;
/// \brief Total number of audio frames output
    unsigned int       audOutputCount;
/// \brief 32MSB of the PTS of the most recently output audio frame
    unsigned int       aoutPTSHi;
/// \brief 32LSB of the PTS of the most recently output audio frame
    unsigned int       aoutPTSLo;
/// \brief Most recent time between video interrupts (in usec)
    unsigned int       vinttimenow;
/// \brief Number of frames not rendered in time (composition mode only)
    unsigned int       vrendlate;
} AVOC_STATUS;

/// \brief AVSN status block
typedef struct
{
/// \brief Total number of video frames displayed
    unsigned int vidOutputCount;
/// \brief 32MSB of the PTS of the most recently displayed video frame
    unsigned int voutPTSHi;
/// \brief 32LSB of the PTS of the most recently displayed video frame
    unsigned int voutPTSLo;
/// \brief Total number of video frames dropped due to synchronization
    unsigned int framesDropped;
/// \brief Total number of video frames repeated due to synchronization
    unsigned int framesRepeated;
/// \brief Total number of video frames currently queued for display
    unsigned int videoQueued;
/// \brief Total number of audio frames currently queued for display
    unsigned int audioQueued;
/// \brief Audio underflows (no frame was available to play)
    unsigned int audioUnderflows;
/// \brief Video underflows (no frame was available to display)
    unsigned int videoUnderflows;
/// \brief Measured audio latency for the most recent frame
    unsigned int audioLatency;
/// \brief Audio resampling ratio used for latency control
    unsigned int audioResampleRatio;
/// \brief Measured video latency for the most recent frame, in fixed latency mode
    unsigned int videoLatency;
/// \brief Video resampling ratio used for latency control
    unsigned int videoResampleRatio;
/// \brief Measured mininum audio latency for the most recent frame
    unsigned int audioLatencyMin;
/// \brief Measured maximum audio latency for the most recent frame
    unsigned int audioLatencyMax;
/// \brief Measured mininum video latency for the most recent frame
    unsigned int videoLatencyMin;
/// \brief Measured maximum video latency for the most recent frame
    unsigned int videoLatencyMax;
/// \brief 32MSB of the current NTP time (in ticks)
    unsigned int ntpTimeHi;
/// \brief 32LSB of the current NTP time (in ticks)
    unsigned int ntpTimeLo;
} AVSN_STATUS;

/// \brief CameraTamper status block
typedef struct
{
/// \brief frame number of the frames that are passed through the camera tamper block
    unsigned int	tamperFrame;	
/// \brief edge threshold set for triggering out of focus detection    
    unsigned int 	tamperEdgeThd;
/// \brief variance threshold set for triggering block detection    
    unsigned int 	tamperVarThd;
/// \brief luma threshold set for triggering field of view change detection    
    unsigned int 	tamperLumaThd;
/// \brief processed edge measurement compared to edge threshold     
    unsigned int 	tamperEdgeComp;
/// \brief processed variance measurement compared to variance threshold     
    unsigned int 	tamperVarComp;
/// \brief processed luma measurement compared to luma threshold     
    unsigned int 	tamperLumaComp;
} TAMPER_STATUS;

/// \brief LVPP and NVPP status block
typedef struct
{
/// \brief Total number of captured video frames
    unsigned int        framesCaptured;
/// \brief Total number of dropped frames due to capture failures (configuration error, missing interrupt etc.)
    unsigned int        captureFailures;
/// \brief Total number of dropped frames due to input buffer overflow
    unsigned int        captureOverflow;
/// \brief Total number of frames dropped due to A/V synch
    unsigned int        avsynchDrops;
/// \brief Total number of frames repeated due to A/V synch
    unsigned int        avsynchRepeats;
/// \brief PTS of the most recently output frame (32 MSBs)
    unsigned int        outputPTS_High;
/// \brief PTS of the most recently output frame (32 LSBs)
    unsigned int        outputPTS_Low;
/// \brief Total number of input frames decimated
    unsigned int        inputDrops;
/// \brief Destination picture width
    unsigned int        destWidth;
/// \brief Destination picture height
    unsigned int        destHeight;
/// \brief Minimum video frame transit time (NVPP Only)
    unsigned int        mintime;
/// \brief Average video frame transit time (NVPP Only)
    unsigned int        avgtime;
/// \brief Maximum video frame transit time (NVPP Only)
    unsigned int        maxtime;
/// \brief camera tamper data 
    TAMPER_STATUS       tamperStatus;
/// \brief temporal filter strength
    unsigned int        tfStrength;
/// \brief horizontal filter level
    unsigned int        horzFiltLvl;
/// \brief Output queue occupancy
    unsigned int  outputQueueOccupancy;
/// \brief Output queue size
    unsigned int  outputQueueSize;
/// \brief Minimum hardware processing time (in usec)
    unsigned int       minhwtime;
/// \brief Average hardware processing time (in usec)
    unsigned int       avghwtime;
/// \brief Maximum hardware processing time (in usec)
    unsigned int       maxhwtime;
} VPP_STATUS;


/// \brief AVDecoder status block
typedef struct
{
/// \brief Number of video frames decoded since start()
    unsigned int videoFramesDecoded;
/// \brief Number of audio frames decoded since start()
    unsigned int audioFramesDecoded;
/// \brief Number of video decoder errors detected since start()
    unsigned int videoDecodeErrors;
/// \brief Number of audio decoder errors detected since start()
    unsigned int audioDecodeErrors;
/// \brief Number of video frames dropped since start()
    unsigned int videoFramesDropped;
/// \brief Number of total video frames since start()
    unsigned int totalVideoFrames;
/// \brief Width of the most recently decoded video frame (pixels)
    unsigned int videoFrameWidth;
/// \brief Height of the most recently decoded video frame (pixels)
    unsigned int videoFrameHeight;
/// \brief 32 MSB of the PTS of the most recently demuxed video QBOX
    unsigned int vdmxPTSHi;
/// \brief 32 LSB of the PTS of the most recently demuxed video QBOX
    unsigned int vdmxPTSLo;
/// \brief 32 MSB of the PTS of the most recently demuxed audio QBOX
    unsigned int admxPTSHi;
/// \brief 32 LSB of the PTS of the most recently demuxed audio QBOX
    unsigned int admxPTSLo;
/// \brief 32 MSB of the PTS of the most recently decoded video frame
    unsigned int vdecPTSHi;
/// \brief 32 LSB of the PTS of the most recently decoded video frame
    unsigned int vdecPTSLo;
/// \brief 32 MSB of the PTS of the most recently demuxed audio frame
    unsigned int adecPTSHi;
/// \brief 32 LSB of the PTS of the most recently demuxed audio frame
    unsigned int adecPTSLo;
/// \brief Fullness of the video decoder input buffer (bytes)
    unsigned int vdecInputOccupancyBytes;
/// \brief Maximum size of the video decoder input buffer (bytes)
    unsigned int vdecInputBufferSizeBytes;
/// \brief Fullness of the video decoder input buffer (access units)
    unsigned int vdecInputOccupancyAus;
/// \brief Maximum size of the video decoder input buffer (access units)
    unsigned int vdecInputBufferSizeAus;
/// \brief Fullness of the audio decoder input buffer (bytes)
    unsigned int adecInputOccupancyBytes;
/// \brief Maximum size of the audio decoder input buffer (bytes)
    unsigned int adecInputBufferSizeBytes;
/// \brief Fullness of the audio decoder input buffer (access units)
    unsigned int adecInputOccupancyAus;
/// \brief Maximum size of the audio decoder input buffer (access units)
    unsigned int adecInputBufferSizeAus;
/// \brief The amount of time it takes to decode an I frame
    unsigned int vdecIFramePerf;
/// \brief The amount of time it takes to decode a P frame
    unsigned int vdecPFramePerf;
/// \brief The amount of time it takes to decode a B frame
    unsigned int vdecBFramePerf;
/// \brief Instantaneous frame frame, actually it's the average frame rate calculated between 2 queries.
    float vdecFrameRate;
/// \brief Fullness of the video decoder output buffer (frames)
    unsigned int vdecOutputOccupancyFrames;
/// \brief Maximum size of the video decoder output buffer (frames)
    unsigned int vdecOutputBufferSizeFrames;
/// \brief Video decoder debug state.  This is for debuggin purposes only, decoder is not making use of this state 
    int vdecState;
/// \brief Last error detected by video decoder.
    int vdecLastError;
/// \brief PTS of the frame where vdecLastError is detected
    unsigned int vdecLastErrorPTSHi;
    unsigned int vdecLastErrorPTSLo;
/// \brief Minimum video frame delta DEMUX
    unsigned int vdecMinOutputDeltaDMX;
/// \brief Maximum video frame delta DEMUX
    unsigned int vdecMaxOutputDeltaDMX;
/// \brief Minimum audio frame delta DEMUX
    unsigned int adecMinOutputDeltaDMX;
/// \brief Maximum audio frame delta DEMUX
    unsigned int adecMaxOutputDeltaDMX;
/// \brief Minimum video frame delta DECODE
    unsigned int vdecMinOutputDeltaDEC;
/// \brief Maximum video frame delta DECODE
    unsigned int vdecMaxOutputDeltaDEC;
/// \brief Minimum audio frame delta DECODE
    unsigned int adecMinOutputDeltaDEC;
/// \brief Maximum audio frame delta DECODE
    unsigned int adecMaxOutputDeltaDEC;
} AVDEC_STATUS;

/// \brief AVC Encoder debug state
typedef enum
{
/// \brief AVC decoder idle state(for debug only.)  AVC decoder is waiting to be called by the master thread.
    AVCDEC_CODEC_IDLE = 0,
/// \brief AVC decoder QBOX parsing state(for debug only.)  AVC decoder is parsing qboxes.  This state only exists for direct memory transfer mode.
    AVCDEC_CODEC_PARSING_QBOXES,
/// \brief AVC decoder Header Parsing state(for debug only.)  AVC decoder is pre-parsing NALU headers.
    AVCDEC_CODEC_PARSING_HEADERS,
/// \brief AVC decoder's Decoding state(for debug only.)  AVC decoder is decoding a frame, this is done by the worker thread
    AVCDEC_CODEC_DECODING,
/// \brief AVC decoder's VCIF state(for debug only.)  AVC decoder is waiting for hardware to finish decoding. This is done by the worker thread.
    AVCDEC_CODEC_VCIF,
/// \brief AVC decoder's Decoding operation is done, waiting for main thread to update DPB and output.
    AVCDEC_CODEC_UPDATE_DPB,
/// \brief AVC decoder's Waiting for Input state(for debug only.)  Input buffer is empty, AVC decoder is waiting for more input data.
    AVCDEC_CODEC_WAITING_FOR_INPUT,
/// \brief AVC decoder's Waiting for Output state(for debug only.)  Output buffer is full, AVC decoder is waiting for space in output buffer.
    AVCDEC_CODEC_WAITING_FOR_OUTPUT,
/// \brief AVC decoder is flushing its buffers.  Flushing is similar to decoding except that decoder will stop after it finishes this frame. 
    AVCDEC_CODEC_FLUSHING,
} AVCDEC_CODEC_STATE;

/// \brief AVC Encoder status block
/// \ingroup avcencgroup
typedef struct
{
/// \brief Time to encode I/P/B frames in microseconds
    unsigned int performance[3];
/// \brief Number of encoded I/P/B frames that has performance below target
    unsigned int performanceJitter[3];
/// \brief Count of numer of frames encoded
    unsigned int framesEncoded;
/// \brief Debug state of the object
    unsigned int state;
/// \brief Encoder latency measured from start of capture to bitstream output
    unsigned int latency;
/// \brief Current fullness in bytes of the bitstream output buffer
    unsigned int bufferFullness;
/// \brief Size of the bitstream output buffer
    unsigned int bufferSize;
/// \brief Highest maximum fullness reached during this encode session
    unsigned int bufferMaxFullness;
/// \brief Current picture level quantizer selected by rate control
    unsigned int baseQP;
/// \brief Number of frames input to the encoder
    unsigned int inputFrames;
/// \brief Number of frames dropped due to insufficient reference frames or bitstream space
    unsigned int inputFrameDrops;
/// \brief Number of frame drops initiated by rate control
    unsigned int rateControlFrameDrops;
/// \brief Last rate control frame drop reason 
    unsigned int rateControlFrameDropID;
/// \brief Last dropped picture type 
    unsigned int lastDroppedPicType;
/// \brief Last dropped picture size 
    unsigned int lastDroppedPicSize;
/// \brief Last dropped picture size 
    unsigned int lastDroppedPicQP;
/// \brief Last dropped picture edge strength 
    unsigned int lastDroppedPicEdge;
/// \brief Number of field drops initiated by inverse telecine
    unsigned int telecineFieldDrops;
/// \brief Address of the most recent bitstream block acknowledged by the host
    unsigned int lastBlockDoneAddr;
/// \brief Size of the most recent bitstream block acknowledged by the host
    unsigned int lastBlockDoneSize;
/// \brief Address of the most recent bitstream block sent to the host
    unsigned int lastBlockReadyAddr;
/// \brief Size of the most recent bitstream block sent to the host
    unsigned int lastBlockReadySize;
/// \brief Number of slices which have exceeded the maximum NALU size
    unsigned int nalusExceedLimit;
/// \brief Number of frames which have had their quantizer overridden by the frame complexity algorithm
    unsigned int complexityRCOverride;
/// \brief PTS of the currently encoding frame.  May move backwards in the case of B frames.  32 LSBs.
    unsigned int currentPTS_High;
/// \brief 32 MSBs of the PTS of the currently encoding frame.
    unsigned int currentPTS_Low;
///  \brief Y distortion
    unsigned int distortion;
/// \brief Internal frame buffers used
    unsigned int refPoolUsed;
/// \brief Reconstructed frame preview frames dropped due to overflow
    unsigned int reconPreviewOverflows;
/// \brief instantaneous frame rate
    float        frameRate;
/// \brief Buffer for generic debug data
    unsigned int debugBufferAddress;
/// \brief Number of bytes in debug buffer
    unsigned int debugBufferFullness;
/// \brief Minimum frame output delta ENCODER
    unsigned int minOutputDeltaENC;
/// \brief Maximum frame output delta ENCODER
    unsigned int maxOutputDeltaENC;
/// \brief Minimum frame output delta SYSTEM
    unsigned int minOutputDeltaSYS;
/// \brief Maximum frame output delta SYSTEM
    unsigned int maxOutputDeltaSYS;
/// \brief Signature of the frame
    unsigned int signature;
} AVCENC_STATUS;

typedef struct
{
/// \brief Total number of video frames captured
    unsigned int framesCaptured;
/// \brief Total number of times the capture object failed to obtain an empty frame buffer
/// from the datapath buffer pool
    unsigned int getTokenFailures;
} HVCAP_STATUS;

/// \brief Audio Encoder status block
typedef struct
{
/// \brief Count of numer of frames encoded
    unsigned int framesEncoded;
/// \brief Debug state of the object
    unsigned int state;
/// \brief Encoder latency measured from start of capture to bitstream output
    unsigned int latency;
/// \brief Current fullness in bytes of the bitstream output buffer
    unsigned int bufferFullness;
/// \brief Size of the bitstream output buffer
    unsigned int bufferSize;
/// \brief Number of frames input to the encoder
    unsigned int inputFrames;
/// \brief Number of frames dropped due to insufficient reference frames or bitstream space
    unsigned int inputFrameDrops;
/// \brief Minimum frame output delta ENCODER
    unsigned int minOutputDeltaENC;
/// \brief Maximum frame output delta ENCODER
    unsigned int maxOutputDeltaENC;
/// \brief Minimum frame output delta SYSTEM
    unsigned int minOutputDeltaSYS;
/// \brief Maximum frame output delta SYSTEM
    unsigned int maxOutputDeltaSYS;
/// \brief The lower 32-bits of the PTS of the last encoded frame.
    unsigned int currentPTS_Low;
/// \brief The higher 32-bits of the PTS of the last encoded frame.
    unsigned int currentPTS_High;
} AUDIOENC_STATUS;

/// \brief Audio Splitter status block
typedef struct
{
/// \brief Input AIN port index
    unsigned int inPortIdx;
/// \brief Number of output ports defined
    unsigned int numOutputPorts;
/// \brief Number of frames processed
    unsigned int numFrames;
/// \brief Number of frames dropped
    unsigned int framesDropped;
} ASPLIT_STATUS;

/// \brief JPEG Encoder status block
typedef struct
{
/// \brief encoder index
    int id;
/// \brief encoder state
    int state;
/// \brief Number of input DATA tokens dequeued from data-queue
    unsigned int inDataTokens;
/// \brief Number of frames encoded
    unsigned int framesEncoded;
/// \brief Number of input DATA tokens dropped
    unsigned int inDataTokensDropped;
/// \brief Quality factor used in the last encoded frame
    unsigned int lastQualityFactor;
/// \brief Size of last encoded frame, in bytes
    unsigned int lastFrameSize;
/// \brief Total size of all encoded frames
    unsigned int totalFrameSize;
/// \brief The lower 32-bits of the PTS of the last encoded frame.
    unsigned int PTS_low;
/// \brief The higher 32-bits of the PTS of the last encoded frame.
    unsigned int PTS_high;
/// \brief Time from the moment of notify to removal from data-queue for encoding 
    unsigned int inputTokenStandbyTime;
/// \brief Software setup time for the last encoded frame, in us
    unsigned int SWSetupTime;
/// \brief Time taken for sending HW instructions and waiting for encode done ISR
    unsigned int HWEncodeTime;
/// \brief Input DATA Queue occupancy
    unsigned int inputDQueLen;
/// \brief Input DATA Queue Availability
    unsigned int inputDQueAva;
/// \brief Input DATA Queue overflows counter
    unsigned int inputDQueOverflow;
/// \brief Output buffer size
    unsigned int outputBufferSize;
/// \brief Output buffer fullness
    unsigned int outputBufferFullness;
/// \brief Counts all downstream overflows
    unsigned int downstreamOverflows;
/// \brief Counts current downstream overflows
    unsigned int downstreamOverflowsCur;
/// \brief Encoder's own status
    int encStatus;
/// \brief Encoder's master ISR's status
    int isrStatus;
/// \brief Minimum quality factor allowed by the bitrate controller
    unsigned int minQF;
/// \brief Maximum quality factor allowed by the bitrate controller
    unsigned int maxQF;
/// \brief Current bitrate
    unsigned int currBitrate;
/// \brief Num of frames encoded since the last bitrate reset
    unsigned int recentFramesEncoded;
/// \brief Total frames size since the last bitrate reset
    unsigned int recentFramesSize;
/// \brief Minimum bitrate encountered
    unsigned int minBitrate;
/// \brief Maximum bitrate encountered
    unsigned int maxBitrate;
/// \brief Minimum frame size (bytes) encoded
    unsigned int minFrameSize;
/// \brief Maximum frame size (bytes) encoded
    unsigned int maxFrameSize;
} JPEGENC_STATUS;

/// \brief Host audio capture status block
typedef struct
{
/// \brief Stream ID
    unsigned int streamID;
/// \brief Total number of video frames captured
    unsigned int framesCaptured;
/// \brief Total number of times the capture object failed to obtain an empty frame buffer
/// from the datapath buffer pool
    unsigned int getTokenFailures;
} HACAP_STATUS;

/// \brief Audio Filter status block
typedef struct
{
/// \brief filter stream ID
    int streamID;
/// \brief filter state
    int state;
/// \brief the last notified interrupt type
    int lastInterrupt;
/// \brief number of connected input ports
    int inputPorts;
/// \brief number of output ports
    int outputPorts;
/// \brief number of input frames received in current round
    int recvFramesThisRound;
/// \brief number of rounds of successfully received input tokens from input ports
    int inFramesRound;
/// \brief number of output frames sent in current round
    int sentFramesThisRound;
/// \brief number of rounds of successfully sent output tokens to output ports
    int outFramesRound;
/// \brief current number of buffer full occurances; reset every time buffer space is available
    int currBufferFullCount;
/// \brief total number of buffer full occurances since filter started
    int totalBufferFullCount;
/// \brief Input buffer fullness
    int inputBufferFullness;
/// \brief Input buffer size
    int inputBufferSize;
/// \brief Output buffer fullness
    int outputBufferFullness;
/// \brief Output buffer size
    int outputBufferSize;
/// \brief Audio resampling delta used for clock sync
    int audioResampleDelta;
/// \brief Audio resampling ratio used for clock sync
    unsigned int audioResampleRatio;
/// \brief Computation latency (in us)
    unsigned int latency;
} AUDFLTR_STATUS;

/// \brief USB status block
typedef struct
{
    unsigned int debug;
} USB_STATUS;

/// \brief ISP status block
typedef struct
{
    /// \brief Sharpness strength
    unsigned int sharpness;
    /// \brief Saturation strength
    unsigned int saturation;
    /// \brief Gamma correction value
    unsigned int gamma;
    /// \brief Noise filter strength
    unsigned int nrStrength;
    /// \brief Noise filter threshold long
    unsigned int nrThreshLong;
    /// \brief Auto exposure enable
    unsigned int aeEnable;
    /// \brief Auto white balance enable
    unsigned int awbEnable;
    /// \brief Sensor exposure setting
    unsigned int exposure;
    /// \brief Sensor analog gain settings
    float analogGain;
    /// \brief Metering historgram data
    unsigned int histo[5];
    /// \brief Iridix strength
    unsigned int iridix;
    /// \brief AWB mired variable, can be used to calculate color temperature
    unsigned int awbMired;
    /// \brief AWB shift variable
    int awbShift;
    /// \brief AWB algorithm
    unsigned int awbAlgo;
    /// \brief The following 6 parameters describe the AWB profile settings
    int awbA1;
    int awbB1;
    int awbC1;
    int awbP1;
    int awbP2;
    int awbQ1;
    /// \brief The following 4 parameters represent the AWB static gains
    unsigned int sGain[4];
    /// \brief The following 9 parameters represent the CCM
    unsigned int ccm[9];
    /// \brief Final AWB gains
    unsigned int awbGains[3];
    /// \brief Total AWB weight
    unsigned int awbWeight;
    /// \brief Weight of each AWB zone
    unsigned int awbZoneWeight[225];
    /// \brief RG/GB ratios of each AWB zone
    unsigned int awbZoneRGBG[225];
    /// \brief Sum of likely white pixels of each AWB zone
    unsigned int awbZoneSum[225];
    /// \brief Final R/G B/G ratios in AWB
    unsigned int awbFinalRG;
    unsigned int awbFinalGG;    //RL: new gain since output gains are scaled to all be >= 1.0
    unsigned int awbFinalBG;
    /// \brief Intermediate weighted average and gray world R/G B/G ratios in AWB
    unsigned int awbGrayRG;
    unsigned int awbGrayBG;
    unsigned int awbWeightedRG;
    unsigned int awbWeightedBG;
} ISP_STATUS;


#define MEDEWARP_INTERCPU_MAP_QUEUE_SIZE    4

/// \brief Dewarp status block
/// Updating this structure? you must also update it's HOST structures
/// found in condorsw/host/lib/mxuvc/src/plugins/pi_ldmap/ldmap_sideload.c
typedef struct
{
    unsigned int State;
    unsigned int FramesCount;
    unsigned int MapType;
    unsigned int MapMaxLen;
    unsigned int MapN;
    unsigned int MapWidth;
    unsigned int MapHeight;
    // pic sizes
    unsigned int SrcWidth;
    unsigned int SrcHeight;
    unsigned int DewarpedWidth;
    unsigned int DewarpedHeight;
    unsigned int OutputWidth;
    unsigned int OutputHeight;
    unsigned int CompositeWidth;
    unsigned int CompositeHeight;
    // Lens Parameters
    unsigned int lensFOV;
    unsigned int lensRadius;
    unsigned int lensHShift;
    unsigned int lensVShift;
    // Map usage status
    unsigned int LastMap;
    unsigned int CurrentMap;
    unsigned int NextMap;
    /// \brief Map Load Queue, a Queue to allow back door load of maps.
    /// mapQBase has the memory locations where map coefficients can be stored, max size is MapMaxLen in bytes
    unsigned int mapQLen;
    unsigned int mapQRead;
    unsigned int mapQWrite;
    unsigned int hwtime;    //Time taken by hardware to do dewarping
    unsigned int totaltime; //Total time taken by dewarp hardware plus software overhead
    unsigned int mapQBase[MEDEWARP_INTERCPU_MAP_QUEUE_SIZE];
    map_header_struct mapQHeaders[MEDEWARP_INTERCPU_MAP_QUEUE_SIZE]; // this array contains the map headers of the maps in mapQBase
    unsigned int  inputQueueOccupancy;
    unsigned int  outputQueueOccupancy;
    unsigned int  outputQueueSize;
    unsigned int  outputQueueCompositorOccupancy;
    unsigned int  outputQueueCompositorSize;
} DEWARP_STATUS;

 
// End status block


#endif
#endif
