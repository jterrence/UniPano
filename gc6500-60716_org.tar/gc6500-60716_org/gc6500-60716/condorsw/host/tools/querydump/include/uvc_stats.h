/*
 * uvc_stats.h
 *
 *  Created on: Oct 6, 2015
 *      Author: sudhish
 */

#ifndef UVC_STATS_H_
#define UVC_STATS_H_

#ifdef __cplusplus
extern "C"
{
#endif
int uvc_stats_init(unsigned int* ptr, unsigned int* ptr1, unsigned int* ptr2);
#ifdef __cplusplus
}
#endif
#endif /* UVC_STATS_H_ */

