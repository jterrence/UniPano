/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

/************************************************************************************
 * @(#) $Header: http://svllsvn01.local.geosemi.com/repos/swrepos/trunk/condorsw/host/tools/querydump/usb_mem_interface.h 57866 2016-04-28 11:12:50Z bsmith $
 ************************************************************************************/

#ifndef USB_MEM_INTERFACE_H_
#define USB_MEM_INTERFACE_H_


#ifdef __cplusplus
extern "C"
{
#endif

// Error codes:
typedef enum
{
    USB_MEM_IF_SUCCESS        = ( 0),     // Any positive value means success
    USB_MEM_IF_ERR_MEM        = (-1),   // Memory allocation - fatal corruption
    USB_MEM_IF_ERR_ENDTX      = (-2),   // Wait for busy timed out
    USB_MEM_IF_ERR_DEVBUSY    = (-3),   // Busy is set before transfer started
    USB_MEM_IF_ERR_BADPARAM   = (-4),   // Wrong parameter
    USB_MEM_IF_ERR_NA         = (-5),   // Feature not available on this device
    USB_MEM_IF_ERR_HANDLE     = (-6),   // Bad handle
    USB_MEM_IF_ERR_IO         = (-7),   // I/O error from host
    USB_MEM_IF_ERR_CSRERR     = (-8),   // CSRStat returned an error
    USB_MEM_IF_ERR_NOINT      = (-9),   // Interrupt has not been handled
    USB_MEM_IF_ERR_LOCK       = (-10),  // Unable to lock the host
    USB_MEM_IF_ERR_WRONGPROD  = (-11),  // QHAL can't find the chip
    USB_MEM_IF_ERR_INTR       = (-12),  // QHAL call interrupted by a signal
    USB_MEM_IF_ERR_OVERFLOW   = (-13),  // Read or Write overflowed a buffer or FIFO
    USB_MEM_IF_ERR_IOCTL      = (-14),  // I/O error from driver proxy, not host
    USB_MEM_IF_ERR_INIT       = (-15),  // Unable to initialize necessary subsystems
    USB_MEM_IF_ERR_NODEV      = (-16),  // Unable to find the device
}e_usb_mem_if_err;

#define REG_ADDR(reg)       Reg_M8_##reg##_ADDRESS
#define REG_BYTES(reg)      Reg_M8_##reg##_NUM_BYTES
#define REG_TYPE(reg)       Reg_M8_##reg##_t
#define REG_INDEX(reg)      Reg_M8_##reg##_I
#define REG_FIELD_LSB(reg)  RegF_M8_##reg##_LSB

#define CHIPCTL_ADDR(reg)   REG_ADDR(CHIPCTL_x_##reg)
#define CHIPCTL_BYTES(reg)  REG_BYTES(CHIPCTL_x_##reg)
#define CHIPCTL_TYPE(reg)   REG_TYPE(CHIPCTL_x_##reg)

#define CHIPCTL_RESET_MS    1

#define PARTITION_BASE_ADDR(i)              \
    (REG_ADDR(MEMS_MEM0_PMU_x_PartBaseSeg0) + (i)*  \
     (REG_ADDR(MEMS_MEM0_PMU_x_PartBaseSeg1) -      \
      REG_ADDR(MEMS_MEM0_PMU_x_PartBaseSeg0)))

int usb_mem_interface_write_words(void* host_h, unsigned char bid,
               unsigned long addr, long *buffer_p, int nWords);
int usb_mem_interface_write_bytes(void* host_h, unsigned char bid,
               unsigned long addr, char *buffer_p, int nBytes);
int usb_mem_interface_read_words(void* host_h, unsigned char bid,
              unsigned long addr, long *buffer_p, int nWords);
int usb_mem_interface_read_bytes(void* host_h, unsigned char bid,
              unsigned long addr, char *buffer_p, int nBytes);
int usb_mem_interface_get_partbase(void* host_h,
            unsigned char bid, unsigned long* base_p);
void* usb_mem_interface_open();
int usb_mem_interface_close(void* host_h);

int usb_mem_interface_read_qcc(void* host_h,
        int bid, int addr, unsigned long *data_p, int len);

int usb_mem_interface_write_qcc(void* host_h,
        int bid, int addr, unsigned long data, int len);

#ifdef __cplusplus
}
#endif
#endif /* USB_MEM_INTERFACE_H_ */
