/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
/*
 * usb_mem_interface_test.c
 *
 *  Created on: Feb 18, 2013
 *      Author: bnatarajan
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

#include "usb_query_impl.h"



int main(int argc, char**argv)
{
    void* query_h = NULL;
    query_h = usb_query_init(argv[1]);
    if(query_h){
        usb_query_objects(query_h);
        usb_query_close(query_h);
    }
    printf( "usb mem interface World End \n");
    return 0;
}
