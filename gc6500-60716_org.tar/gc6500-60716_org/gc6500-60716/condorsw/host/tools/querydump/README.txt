/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/
OVERVIEW
========

The tool querydump is used to check if the system is working correctly. For example, the key things to check are:

- Heartbeat is increasing
- Frame capture count is increasing
- Any capture overflows
- Performance numbers for video capture and encoding
- Memory allocation

It first gives a system level information whose organization will be same irrespective of the datapath JSON used.
Then it gives information per object. That will vary depending on what objects are created in JSON.

It works over a working USB connection using libusb. So if the fimrware is hanged in away that USB interface is 
not working, then querydump will not work.

Invocation:

sudo ./querydump <outputfile>

Where <outputfile> is the output file name where the results will go.
If not specified, then output goes to console.

Normally to check if the system is working, querydump needs to nbe executed at least twice and then results compared to
check if heartbeat and frame counts are increasing.

OUTPUT
======

A typical output as obtained when using app_fishsye.json and while all channels (video and audio) are running is 
as follows:
 

Core: Retrieved 0x41e00288 as GPB

isp0 is attached type  = 17 
vcap0 is attached type  = 19 
nvpp0 is attached type  = 3 
nvpp1 is attached type  = 3 
nvpp2 is attached type  = 3 
nvpp3 is attached type  = 3 
dewarp1 is attached type  = 21 
dewarp2 is attached type  = 21 
dewarp3 is attached type  = 21 
avcenc0 is attached type  = 5 
avcenc1 is attached type  = 5 
jpegenc0 is attached type  = 13 
audfil0 is attached type  = 16 
audfil2 is attached type  = 16 
audfil3 is attached type  = 16 
audenc0 is attached type  = 11 

System Control : ID 0 
	Heartbeat : 106
	Dropped Events: 0
	Event Queue Pointers: Read 0 / Write 1
	Event Pending: 0
	CPU load (last .1s): 44% 
	CPU load (last 1s): 45% 
	CPU load (last 10s): 40% 
	Allocated pool memory : 112713728/234872832
	Heap Utilization : 12222956/21083616
	TFCC0:
		INT: now:33308us max:42782us n:683
		ISR: min:53us avg:42us max:332us
	VPP0:
		INT: n:1355
		ISR: min:73us avg:88us max:425us hwload:57.02% swload:1.25%
	VSCALE0:
		INT: n:24
		ISR: min:86us avg:25us max:147us hwload:0.64% swload:0.02%
	AIN0:
		INT: now:271us max:20138us n:1754
		ISR: min:12us avg:144us max:481us dbg:4
		framesCaptured:1132 captureFailures:2 captureOverflow:0
		SamplesPerFrame:320 RampTime:10us

	AIN1:
		INT: now:0us max:0us n:0
		ISR: min:0us avg:0us max:0us dbg:0
		framesCaptured:0 captureFailures:0 captureOverflow:0
		SamplesPerFrame:0 RampTime:0us

	Clock rates
		XIN      : 24000000
		PLL0     : 333000000
		PLL1     : 800000000
		PLL2     : 245760016
		CPU      : 333000000
		QMM      : 166500000
		AHB      : 111000000
		APB      : 111000000
		UART0    : 100000000
		UART1    : 100000000
		I2C0     : 20000000
		I2C1     : 20000000
		AUD      : 24576001
		VS       : 160000000
		AVC      : 200000000
		JPEG     : 132903225
		PME      : 200000000
		VID      : 24000000
		DW       : 160000000
		GPU      : 267000000
		DDR      : 534000000
		MEM      : 267000000
	UVC statistics:
		Video Main Channel
			 complete        : 1377
			 failure         : 0
		Video Secondary/Preview Channel
			 complete        : 677
			 failure         : 0
		Audio PCM Channel
			 complete        : 1131
			 failure         : 0
		Audio AAC/OPUS Channel
			 complete        : 353
			 failure         : 0
ISP : ID 1 "isp0"
	Sharpness                 : 50 / 255
	Saturation                : 100 / 200
	Gamma                     : 0
	Iridix                    : 255 / 255
	Noise Reduction Filter
	- Strength                : 0 / 255
	- Long Threshold          : 31
	Auto Exposure
	- Enable                  : 1
	- Sensor exposure         : 843
	- Sensor analog gain      : 6.26
	- Histogram stats         : 38632 19920 6106 528 349
	Auto White Balance
	- Enable                  : 1
	- Algorithm               : weighted
	- Color temperature       : 11904
	- Mired                   : 21759
	- Shift                   : 189
	- Total Weight            : 46115
	- Profile Parameters      : (a1) 187694 (b1) 128 (c1) -135401
	                            (p1) 70 (p2) 45 (q1) 141
	- Static Gains            : (R) 0x0118 (Gr) 0x0100 (Gb) 0x0100 (B) 0x01d5
	- Color Correction Matrix : 0x01b1 0x8076 0x803b
	                            0x804e 0x016c 0x801e
	                            0x8008 0x80d7 0x01df
	- R/G B/G Ratios          : (Weighted) 0x010d 0x0150
	                            (Final)    0x010c 0x0150
	- Zone Weights            :

		   0    1    1    1    1    1    1    1    1    1    1    1    1    1    0 
		   1    4    4    4    4    4    4    4    4    4    4    4    4    4    1 
		   1    4    4    4    4    4    4    4    4    4    4    4    4    4    1 
		   1    4    4    4    4    4    4    4    4    4    4    4    4    4    1 
		   1    4    4    4    4    4    4    4    4    4    4    4    4    4    1 
		   1    4    4    4    4    4    4    4    4    4    4    4    4    4    1 
		   1    4    4    4    4    4    4    4    4    4    4    4    4    4    1 
		   1    4    4    4    4    4    4    4    4    4    4    4    4    4    1 
		   1    4    4    4    4    4    4    4    4    4    4    4    4    4    1 
		   1    4    4    4    4    4    4    4    4    4    4    4    4    4    1 
		   1    4    4    4    4    4    4    4    4    4    4    4    4    4    1 
		   1    4    4    4    4    4    4    4    4    4    4    4    4    4    1 
		   1    4    4    4    4    4    4    4    4    4    4    4    4    4    1 
		   1    4    4    4    4    4    4    4    4    4    4    4    4    4    1 
		   0    1    1    1    1    1    1    1    1    1    1    1    1    1    0 


VCAP : ID 2 "vcap0"
	Frames Captured : 681
	Capture Failures : 0
	Capture Overflow : 0
	A/V Synch Drops : 0
	A/V Synch Repeats : 0
	Input Drops : 0
	Output PTS : 0x00041a9f7
	Dest Size : 1920x1080
NVPP : ID 3 "nvpp0"
	Frame transit time (us) min:13271 avg:13275 max:13621
	Frames Captured : 678
	Capture Failures : 0
	Capture Overflow : 0
	A/V Synch Drops : 0
	A/V Synch Repeats : 0
	Input Drops : 1
	Output PTS : 0x000419e41
	Dest Size : 1280x720
NVPP : ID 4 "nvpp1"
	Frame transit time (us) min:0 avg:0 max:0
	Frames Captured : 677
	Capture Failures : 0
	Capture Overflow : 0
	A/V Synch Drops : 0
	A/V Synch Repeats : 0
	Input Drops : 1
	Output PTS : 0x000419e41
	Dest Size : 1280x720
NVPP : ID 5 "nvpp2"
	Frame transit time (us) min:5926 avg:5928 max:6065
	Frames Captured : 24
	Capture Failures : 0
	Capture Overflow : 0
	A/V Synch Drops : 0
	A/V Synch Repeats : 0
	Input Drops : 654
	Output PTS : 0x000411d8d
	Dest Size : 640x480
NVPP : ID 6 "nvpp3"
	Frame transit time (us) min:5946 avg:5972 max:6287
	Frames Captured : 677
	Capture Failures : 0
	Capture Overflow : 0
	A/V Synch Drops : 0
	A/V Synch Repeats : 0
	Input Drops : 1
	Output PTS : 0x000419e41
	Dest Size : 320x240
DEWARP : ID 7 "dewarp1"
	State                  : 3
	Frame count            : 678
	Map type               : 1
	Map max length         : 3168
	Map N                  : 128
	Map width              : 23
	Map height             : 15
	Picture Sizes
	- Source width         : 1920
	- Source height        : 1080
	- Dewarped width       : 2560
	- Dewarped height      : 1472
	- Output width         : 1280
	- Output height        : 720
	- Composite width      : 1280
	- Composite height     : 720
	Lens Parameters
	- FOV                  : 180
	- Radius               : 833
	- H shift              : 0
	- V shift              : 0
	Map usage status
	- Last map             : 96739328
	- Current map          : 96739328
	- Next map             : 96739328
	MapQue7  Hdr codec addr: 0x40c07de0
	MapQue7 base codec addr: 0x40c07dd0
	MapQue7 items        : 0x05dd2000,0x05dd4000,0x05dd6000,0x05dd8000,
	MapQue7 length       : 4
	MapQue7 read         : 0
	MapQue7 write        : 0
	Hardware dewarp time (us): 5997
	Total dewarp time (us)   : 6147
DEWARP : ID 8 "dewarp2"
	State                  : 2
	Frame count            : 678
	Map type               : 1
	Map max length         : 3168
	Map N                  : 128
	Map width              : 23
	Map height             : 15
	Picture Sizes
	- Source width         : 1920
	- Source height        : 1080
	- Dewarped width       : 2560
	- Dewarped height      : 1472
	- Output width         : 1280
	- Output height        : 720
	- Composite width      : 1280
	- Composite height     : 720
	Lens Parameters
	- FOV                  : 180
	- Radius               : 833
	- H shift              : 0
	- V shift              : 0
	Map usage status
	- Last map             : 104308736
	- Current map          : 104308736
	- Next map             : 104308736
	MapQue8  Hdr codec addr: 0x40d0ff00
	MapQue8 base codec addr: 0x40d0fef0
	MapQue8 items        : 0x0650a000,0x0650c000,0x0650e000,0x06510000,
	MapQue8 length       : 4
	MapQue8 read         : 0
	MapQue8 write        : 0
	Hardware dewarp time (us): 6274
	Total dewarp time (us)   : 6313
DEWARP : ID 9 "dewarp3"
	State                  : 2
	Frame count            : 678
	Map type               : 1
	Map max length         : 3168
	Map N                  : 128
	Map width              : 23
	Map height             : 15
	Picture Sizes
	- Source width         : 1920
	- Source height        : 1080
	- Dewarped width       : 2560
	- Dewarped height      : 1472
	- Output width         : 1280
	- Output height        : 720
	- Composite width      : 1280
	- Composite height     : 720
	Lens Parameters
	- FOV                  : 180
	- Radius               : 833
	- H shift              : 0
	- V shift              : 0
	Map usage status
	- Last map             : 111878144
	- Current map          : 111878144
	- Next map             : 111878144
	MapQue9  Hdr codec addr: 0x40df5000
	MapQue9 base codec addr: 0x40df4ff0
	MapQue9 items        : 0x06c42000,0x06c44000,0x06c46000,0x06c48000,
	MapQue9 length       : 4
	MapQue9 read         : 0
	MapQue9 write        : 0
	Hardware dewarp time (us): 6066
	Total dewarp time (us)   : 6120
AVC Encoder : ID 10 "avcenc0"
	Frames Encoded    : 677
	Performance (IPB) : 18536/13981/0
	PerformanceJitter (IPB) : 20/630/0
	Latency           : 49427787 us
	Buffer Fullness   : 6808 / 2500000 (max 1084504)
	Base QP           : 23
	State             : Input frame required
	Frames Input          : 678
	Input frame drops     : 1
	Rate control drops    : 0
	Last RC drop cause    : 0
	Last RC drop pic type : 0
	Last RC drop pic size : 0
	Last RC drop pic QP   : 0
	Last RC drop pic Edge : 0
	Telecine field drops  : 0
	Nalus exceed limit    : 0
	Complexity RC override: 0
	Last block ready addr : 0x0
	Last block ready size : 0x0
	Last block done addr  : 0x0
	Last block done size  : 0x0
	Current PTS:          : 0x000419e41
	Y SNR:                : 45.12
	Ref Pool Used         : 2
	Recon Preview Drops   : 0
	Frame output delta E  : 20649/67511 us (Min/Max)
	Frame output delta S  : -1/0 us (Min/Max)
	Frame signature       : 83731368
	Current bitrate       : 0.000000 kb/s
	Frame Rate            :   0.00
AVC Encoder : ID 11 "avcenc1"
	Frames Encoded    : 676
	Performance (IPB) : 9505/11822/0
	PerformanceJitter (IPB) : 3/201/0
	Latency           : 47817781 us
	Buffer Fullness   : 3884 / 2500000 (max 1082052)
	Base QP           : 25
	State             : Input frame required
	Frames Input          : 677
	Input frame drops     : 1
	Rate control drops    : 0
	Last RC drop cause    : 0
	Last RC drop pic type : 0
	Last RC drop pic size : 0
	Last RC drop pic QP   : 0
	Last RC drop pic Edge : 0
	Telecine field drops  : 0
	Nalus exceed limit    : 0
	Complexity RC override: 0
	Last block ready addr : 0x0
	Last block ready size : 0x0
	Last block done addr  : 0x0
	Last block done size  : 0x0
	Current PTS:          : 0x000419e41
	Y SNR:                : 43.36
	Ref Pool Used         : 2
	Recon Preview Drops   : 0
	Frame output delta E  : 20476/68937 us (Min/Max)
	Frame output delta S  : -1/0 us (Min/Max)
	Frame signature       : 0
	Current bitrate       : 0.000000 kb/s
	Frame Rate            :   0.00
JPEGENC : ID 12 "jpegenc0"
	Encoder index : 0
	Encoder state : Ready to encode next pic(1)
	In DATA tokens dequeued : 24
	Frames encoded : 24
	In DATA tokens dropped : 0
	Last quality factor : 50
	Min quality factor allowed : 0
	Max quality factor allowed : 0
	Last frame size (bytes) : 15650 [min: -1 max: 0]
	Total encoded frames size (bytes) : 371664
	Frames encoded since last reset: 24
	Encoded frames size since last reset: 371664
	Current bitrate (bits/s): 120074
	Current PTS : 0x0000000000411d8d
	Token standby time (us) : 11048141
	Software setup time (us) : 190
	Hardware encode time (us) : 4292
	Input data que occupancy : 0/512
	Input data que overflows : 0
	Output buffer fullness (bytes) : 2/1500000
	Downstream overflows (current)/(total) : 0/0
	Encoder status: Notified.
	Mastser ISR status: Encode done interrupt.
AUDFLTR : ID 13 "audfil0"
	Filter stream ID : 0
	Filter state : Computing function (4)
	Last received command/interrupt : 3
	Input ports : 1
	Output ports : 1
	Input frames received in current round : 0
	Rounds of input frames received from src channels : 1386
	Output frames sent in current round : 1
	Rounds of output frames sent to output ports : 1386
	Output buffer overflow count (current) : 0
	Output buffer overflow count (total)   : 0
	Input buffer occupancy: (1/8)
	Output buffer occupancy: (1/8)
	Audio Sync: Delta 0 Resample Ratio 0x0 (0 ppm)
	Computation latency: 0 us
AUDFLTR : ID 14 "audfil2"
	Filter stream ID : 1
	Filter state : Waiting for input frames (1)
	Last received command/interrupt : 3
	Input ports : 1
	Output ports : 1
	Input frames received in current round : 0
	Rounds of input frames received from src channels : 1386
	Output frames sent in current round : 1
	Rounds of output frames sent to output ports : 1386
	Output buffer overflow count (current) : 0
	Output buffer overflow count (total)   : 0
	Input buffer occupancy: (0/8)
	Output buffer occupancy: (0/8)
	Audio Sync: Delta 0 Resample Ratio 0x0 (0 ppm)
	Computation latency: 0 us
AUDFLTR : ID 15 "audfil3"
	Filter stream ID : 2
	Filter state : Waiting for input frames (1)
	Last received command/interrupt : 3
	Input ports : 1
	Output ports : 1
	Input frames received in current round : 0
	Rounds of input frames received from src channels : 1386
	Output frames sent in current round : 0
	Rounds of output frames sent to output ports : 1386
	Output buffer overflow count (current) : 0
	Output buffer overflow count (total)   : 0
	Input buffer occupancy: (0/8)
	Output buffer occupancy: (0/8)
	Audio Sync: Delta 0 Resample Ratio 0x0 (0 ppm)
	Computation latency: 0 us
Audio Encoder : ID 16 "audenc0"
	Frames Encoded    : 353
	State             : Input frame required
	Latency           : 10747 us
	Buffer Fullness   : 4 / 131072 
	Frames Input      : 353
	Input frame drops : 0
	Current PTS:      : 0x00041add0
	Frame output delta E  : 51572/82640 us (Min/Max)
	Frame output delta S  : -1/0 us (Min/Max)
	Current bitrate   : 0.000000 kb/s

The description of each field is as follows:

System Control:
---------------

	Heartbeat:              If system is alive, this value should increment by 1 after every 500ms
	Dropped Events:         System wide events drop status (Internal purpose only)
	Event Queue Pointers:   System wide events read/write queue status (Internal purpose only)
	Event Pending:          System wide events delivery status (Internal purpose only)
	CPU load (last .1s):    CPU load in last 1/0 of a second 
	CPU load (last 1s):     CPU load in last second
	CPU load (last 10s):    CPU load in last 10 second 
	Allocated pool memory:  How much pool memory is allocated from the total pool memory.
                                Pool memory is above the 32MB range which is used by eCos.
                                In a 256MB system, total memory above 32MB = (256-32)*1024=234881024
                                The pool offset in memory above 32MB=8192
                                So total pool memory=234881024-8192=234872832
	Heap Utilization:       Heap used by eCos out of total heap

	TFCC0:
                This shows various statistics of TFCC0 block used for capturing the incoming video
                INT: Interrupt times for now and max and the interrupt count
                ISR: ISR minimum, average and maximum time
	VPP0:
                This shows various statistics of VPP0 block
                INT: Interrupt count
                ISR: ISR minimum, average and maximum time; hardware and software load
	VSCALE0:
                This shows various statistics of VSCALE0 block
                INT: Interrupt count
                ISR: ISR minimum, average and maximum time; hardware and software load
	AIN0:
                This shows various statistics of AIN0 block.
		This block will have some data only if audio is enabled in json and audio is getting captured
                INT: Interrupt count
                ISR: ISR minimum, average and maximum time; hardware and software load
		framesCaptured: Frame count with stats of overflows and failures while capturing audio samples

	AIN1:
                This shows various statistics of AIN1 block.
		This block will have some data only if audio, AEC is enabled and Speaker is ON in json.
                INT: Interrupt count
                ISR: ISR minimum, average and maximum time; hardware and software load
		framesCaptured: Frame count with stats of overflows and failures while capturing audio samples

        Clock rates:
                Clock rates in Hertz for various blocks

	UVC statistics:
                This shows number of frames transferred successfully / transfer failure.
		Video Main Channel: is Mux channel in IPCAM mode & Primary channel in SKYPE mode
		Video Secondrary/Preview Channel: is RAW channel in IPCAM mode or Preview channel in SKYPE mode
		Audio PCM Channel: Audio non encoded channel (PCM) transfer state
		Audio AAC/OPUS Channel: Audio encoded channel transfer state. It could be AAC or OPUS, based on 
					JSON configuration.

        ISP:
                ISP information. The names are self explanatory.


VCAP:
-----
NVPP:
-----
        The above are software objects representing the video inputs.
        VCAP (Video capture) is for the TFCC0 hardware where NVPP (Non real time video pre processor)
        is for the memory to memory scalers (implemented in hardware using VPP0 or VSCALE0).
        NVPP can have multiple instances (each mapped to same hardware for example VPP0) and 
        each instance uses the hardware in a time slice way.
        Each object shows:

                Frames Captured:  Total frames received on input
                Capture Failures: Total failures in processing (not common)
                Capture Overflow: Frames dropped due to lack of buffers in queue for processing
                A/V Synch Drops:  Frames dropped to maintain A/V synch
                A/V Synch Repeats:Frames repeated to maintain A/V synch
                Input Drops:      Input frame dropped due to frame rate decimation specified in configuration
                Output PTS:       Output PTS
                Dest Size:        Output resolution

DEWARP:
-------
        Shows information about the dewarp instance. There are multiple dewarp instances using the
        same hardware sequentially. Some of the parameters are for internal use only and hence not explained:
	
        State                    : Dewarp state
	Frame count              : Number of frames captured
	Map type                 : Map type
	Map max length           : Maximum length of map (for internal use)
	Map N                    : Spacing in grid. Lower the value, bigger is the grid size.
	Map width                : Map width (for internal use)
	Map height               : Map Height (for internal use)
	Picture Sizes
	- Source width           : Source video width
	- Source height          : Source video height
	- Dewarped width         : Internal width used for dewarped output before the scaler
	- Dewarped height        : Internal height used for dewarped output before the scaler
	- Output width           : Output width after the scaler
	- Output height          : Output height after the scaler
	- Composite width        : Width of compositor (if dewarp part of a compositor)
	- Composite height       : Height of compositor (if dewarp part of a compositor)
	Lens Parameters
	- FOV                    : Lens field of view in degrees
	- Radius                 : Lens radius in pixels
	- H shift                : Lens H shift in pixels
	- V shift                : Lens V shift in pixels
	Map usage status
	- Last map               : Internal use only
	- Current map            : Internal use only
	- Next map               : Internal use only
	MapQue7  Hdr codec addr  : Internal use only
	MapQue7 base codec addr  : Internal use only
	MapQue7 items            : Internal use only
	MapQue7 length           : Internal use only

	MapQue7 read             : Internal use only
	MapQue7 write            : Internal use only
	Hardware dewarp time (us): Time taken by dewarp hardware for this instance
	Total dewarp time (us)   : Time takens by dewarp hardware and software for this instance

AVC Encoder:
------------

	Frames Encoded    : Number of frame encoded
	Performance (IPB) : Time in us for last I/P/B frames
	PerformanceJitter (IPB) : performance jitter for I/P/B frames
	Latency           : Not supported in querydump
	Buffer Fullness   : Buffer fullness
	Base QP           : Base QP
	State             : State of encoder
	Frames Input          : Number of input frames
	Input frame drops     : Input frame drops
	Rate control drops    : Rate control drops
	Last RC drop cause    : Cause of Rate control algo dropping frame 
	Last RC drop pic type : type of frame dropped I/P/B by Rate control algo
	Last RC drop pic size : size of frame dropped by Rate control algo
	Last RC drop pic QP   : QP of frame dropped by Rate control algo
	Last RC drop pic Edge : Complexity of frame dropped by Rate control algo
	Telecine field drops  : Telecine drops
	Nalus exceed limit    : no of NALU exeeding the limit set
	Complexity RC override: Internal use only
	Last block ready addr : Internal use only
	Last block ready size : Internal use only
	Last block done addr  : Internal use only
	Last block done size  : Internal use only
	Current PTS:          : Current PTS
	Y SNR:                : Y SNR
	Ref Pool Used         : Internal use only
	Recon Preview Drops   : Reconstructed preview frame drops
	Frame output delta E  : Internal use only
	Frame output delta S  : Internal use only
	Frame signature       : Internal use only
	Current bitrate       : Not supported in querydump; use mxuvc --stats for getting this
	Frame Rate            : Not supported in querydump; use mxuvc --stats for getting this

JPEGENC:
--------
        The fields are self explanatory

AUDFLTR:
--------
        Shows information about the audio filter instance.
        Some of the parameters are for internal use only and hence not explained:

	Filter stream ID 					: Indicates the filter instance id
	Filter state 						: Indicates the current state of the audio filter
	Last received command/interrupt 			: Internal use only
	Input ports 						: no of input ports connected to the filter
	Output ports 						: no of output ports connected to the filter
	Input frames received in current round 			: no of frames received in current call of filter
	Rounds of input frames received from src channels 	: no of frames received to the filter
	Output frames sent in current round 			: no of frames sent in current call of filter
	Rounds of output frames sent to output ports 		: no of frames sent from the filter
	Output buffer overflow count (current) 			: Internal use only
	Output buffer overflow count (total)   			: Internal use only
	Input buffer occupancy					: Buffer occupancy of the input buffer
	Output buffer occupancy					: Buffer occupancy of the output buffer
	Audio Sync						: Internal use only
	Computation latency					: Internal use only

Audio Encoder:
--------------
        Shows information about the audio encoder instance.
        Some of the parameters are for internal use only and hence not explained:

	Frames Encoded    	: no of frames encoded by the audio encoder
	State             	: Indicates the current state of the audio encoder
	Latency	          	: Indicates the time taken for encode function
	Buffer Fullness   	: Buffer fullness level
	Frames Input      	: no of frames input to the encoder
	Input frame drops 	: no of frames dropped at the encoder
	Current PTS      	: PTS of the current audio frame
	Frame output delta E	: Internal use only
	Frame output delta S	: Internal use only
	Current bitrate   	: Internal use only

Memory Bandwidth:
-----------------

By default, memory bandwidth statistics are not computed by the firmware and hence not shown by querydump.
They can be enabled by adding following key in "system" section of JSON:

"MEMBW_QUERY": "1"

For app_fisheye.json, with the above parameter added and with command:

./mxuvc stream --vout1 /dev/null --vout2 /dev/null --vout3 /dev/null --vout4 /dev/null --aout1 /dev/null --aout2 /dev/null --stats

It looks like following when using AR0330 sensor:

        Memory bandwidth (bytes/sec) over the last 6480ms:
        -------------------------------------------------
        | Read Clients:          | Write Clients:       |
        |                        |                      |
        | CPU:          20752632 | CPU:         6016723 |
        | QMM:           1864316 | QMM:          289988 |
        | SOC-AHB:        772338 | SOC-AHB:           0 |
        | SOC-FBR:       2288813 | SBC:          222734 |
        | SBC:             99796 | FBW0:       94631050 |
        | TFILT:               0 | VPP0:       46640813 |
        | FBR0:        125304045 | VSCL0:        650847 |
        | FBR1:                0 | VSCL1:             0 |
        | VPP0_Reg:            0 | AVC DB:     82118870 |
        | PME:                 0 | GPU:               0 |
        | AVC EME rd0:  85173694 | AII_0:         64000 |
        | AVC EME rd1: 123448406 | AII_1:             0 |
        | GPU:                 0 | JPEG:              0 |
        | JPEG:           650847 | DEWARP Wr: 127276474 |
        | DEWARP Map:     122937 | SPI:               0 |
        | DEWARP Rd:   196256542 | VPP0 Stat:   3140971 |
        | SPI:                 0 | PME Stat:          0 |
        |                        | AVC Bits:     376949 |
        |                        | AVC Stats:   3565288 |
        -------------------------------------------------
        Total: 921729073 bytes/sec (43.15% of theoretical maximum 2136000000 bytes/sec)
 
