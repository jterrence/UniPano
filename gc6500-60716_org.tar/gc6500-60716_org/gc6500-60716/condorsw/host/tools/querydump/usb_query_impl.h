/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

/************************************************************************************
 * @(#) $Header: http://svllsvn01.local.geosemi.com/repos/swrepos/trunk/condorsw/host/tools/querydump/usb_query_impl.h 57866 2016-04-28 11:12:50Z bsmith $
 ************************************************************************************/

#ifndef USB_QUERY_IMPL_H_
#define USB_QUERY_IMPL_H_

#ifdef __cplusplus
extern "C"
{
#endif

void* usb_query_init(char* filename);
void usb_query_close(void* ptr);
void usb_query_objects(void* ptr);

#ifdef __cplusplus
}
#endif

#endif /* USB_QUERY_IMPL_H_ */
