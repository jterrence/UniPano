/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

/*******************************************************************************
* The purpose of this chunk of code is to have the RCS/CVS/SVN keyword string
* embedded in an object file.
*
* Requirements:
* . The char array containing the keywords must be used so as to avoid compiler
*   warning.  A bogus function is created to use the said char array.
*******************************************************************************/
#define CC_IDENT_NAMESPACE RCS_IDENT_MEMRW_C
#ifdef CC_IDENT_NAMESPACE
#define KW_JOIN(x,y) x ## _ ## y
#define KW_XJOIN(x,y) KW_JOIN(x,y)
#define CC_IDENT_FUNC_NAME KW_XJOIN(CC_IDENT_NAMESPACE, FUNC)
const char* CC_IDENT_FUNC_NAME()
{
    static char rcs_id[] = "$Header: http://svllsvn01.local.geosemi.com/repos/swrepos/trunk/condorsw/host/tools/memrw/memrw.c 59927 2001-05-07 08:46:46Z bsmith $" ;
    return rcs_id ;
}
#undef CC_IDENT_FUNC_NAME
#undef KW_XJOIN
#undef KW_JOIN
#endif // CC_IDENT_NAMESPACE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> // getopt
#include <fcntl.h>
#include <string.h>
#include <qhal_em.h>


#define DEFAULT_XFER_SIZE   2048  //(bytes)
#define DEFAULT_BURST_SIZE  8     //(words)
#define DEFAULT_THRESHOLD   8     //(words)
static void usage(char *prog)
{
    printf("$Header: http://svllsvn01.local.geosemi.com/repos/swrepos/trunk/condorsw/host/tools/memrw/memrw.c 59927 2001-05-07 08:46:46Z bsmith $\n\n" ) ;
    printf("Usage: %s part_id addr(hex) [options]\n", prog);
    printf("options:\n" ) ;
    printf("\t-u size(dec)\t:\tBurst size in words <8|16|32|64>, default %d\n", DEFAULT_BURST_SIZE);
    printf("\t-t size(dec)\t:\tFIFO threshold in words, default %d\n", DEFAULT_THRESHOLD);
    printf("\t-b size(dec)\t:\tdma transfer buffer size in bytes, default %d\n", DEFAULT_XFER_SIZE);
    printf("\t-d size(dec)\t:\ttotal data size in bytes, optional for write\n");
    printf("\t-f name\t\t:\tfile name, default to stdin|stdout\n");
    printf("\t-m mode(char)\t:\tr/w for read from/write to ipmd extmem\n");
    printf("\t             \t\t0 for memory test help, 1..3 for memory tests\n");
    printf("\t-p\t\t:\tpretty print data as readable hex\n");
    printf("\t-v\t\t:\tadd verbosity to memory tests\n");
    printf("\t-i intf(dec)\t:\tinterface that should be used for the transfer: 0=EM1, 1=EM2 PIO, 2=EM2 DMA\n");
    printf("\t-l\t\t:\tByteswap before downloading. Use this mode for Little Endian code (ARM).\n");
    printf("\t-h\t\t:\thelp!\n");
}

static void memtest_desc(void)
{
	printf("\n");
	printf("Memory Test 1\n");
	printf("=============\n");
	printf("Function:    memTestDataBus()\n");
	printf("Description: Test the data bus wiring in a memory region by\n");
	printf("             performing a walking 1's test at a fixed address\n");
	printf("             within that region.  The address (and hence the\n");
	printf("             memory region) is selected by the caller.\n");
	printf("\n");
	printf("Memory Test 2\n");
	printf("=============\n");
	printf("Function:    memTestAddressBus()\n");
	printf("Requires:    -d\n");
	printf("Description: Test the address bus wiring in a memory region by\n");
	printf("             performing a walking 1's test on the relevant bits\n");
	printf("             of the address and checking for aliasing. This test\n");
	printf("             will find single-bit address failures such as stuck\n");
	printf("             -high, stuck-low, and shorted pins.  The base address\n");
	printf("             and size of the region are selected by the caller.\n");
	printf("\n");
	printf("Notes:       For best results, the selected base address should\n");
	printf("             have enough LSB 0's to guarantee single address bit\n");
	printf("             changes.  For example, to test a 64-Kbyte region, \n");
	printf("             select a base address on a 64-Kbyte boundary.  Also, \n");
	printf("             select the region size as a power-of-two--if at all \n");
	printf("             possible.\n");
	printf("\n");
	printf("Memory Test 3\n");
	printf("=============\n");
	printf("Function:    memTestDevice()\n");
	printf("Requires:    -d\n");
	printf("Description: Test the integrity of a physical memory device by\n");
	printf("             performing an increment/decrement test over the\n");
	printf("             entire region.  In the process every storage bit \n");
	printf("             in the device is tested as a zero and a one.  The\n");
	printf("             base address and the size of the region are\n");
	printf("             selected by the caller.\n");
	printf("\n");
}

static unsigned char verbose = 0;
static unsigned int max_error_cnt = 0xffffffff;
/**********************************************************************
 *
 * Function:    memTestDataBus()
 *
 * Description: Test the data bus wiring in a memory region by
 *              performing a walking 1's test at a fixed address
 *              within that region.  The address (and hence the
 *              memory region) is selected by the caller.
 *
 * Notes:       
 *
 * Returns:     0 if the test succeeds.  
 *              A non-zero result is the first pattern that failed.
 *
 **********************************************************************/
static unsigned int memTestDataBus(qhalem_handle_t em_h, int part_id,
		unsigned int address)
{
	unsigned int pattern, readback_data;
	int ret, err = 0;

	/*
	 * Perform a walking 1's test at the given address.
	 */
	printf("Starting memTestDataBus: address 0x%08x... %s", 
			address, (verbose ? "\n" : ""));
	fflush(stdout);
	for (pattern = 1; pattern != 0; pattern <<= 1) {
		/*
		 * Write the test pattern.
		 */
		if ((ret = qhalem_write_bytes(em_h, part_id, address, (char *)&pattern, 4)) < 0)
		{
			fprintf(stderr, "qhal_em write failed, returned %d\n", ret);
			err = -1;
			goto out;
		}

		readback_data = 0x0;
		if ((ret = qhalem_read_bytes(em_h, part_id, address, (char *)&readback_data, 4)) < 0)
		{
			fprintf(stderr, "qhal_em read failed, returned %d\n", ret);
			err = -1;
			goto out;
		}

		/*
		 * Read it back (immediately is okay for this test).
		 */
		if (*(unsigned int *)(&readback_data) != 
				*(unsigned int *)(&pattern)) {

			fprintf(stderr, "%saddress 0x%08x, expected 0x%08x, got 0x%08x, xor 0x%08x... ERROR\n",
					((err == 0 && !verbose) ? "\n" : ""),
					address, 
					*(unsigned int *)(&pattern),
					*(unsigned int *)(&readback_data),
					(*(unsigned int *)(&pattern) ^ *(unsigned int *)(&readback_data)));
			err = -2;

		} else if (verbose) {
			fprintf(stderr, "address 0x%08x: expected 0x%08x, got 0x%08x\n",
					address, 
					*(unsigned int *)(&pattern),
					*(unsigned int *)(&readback_data));
		}
	}

out:
	if (!err)
		printf("PASSED\n");

	return err;
}   /* memTestDataBus() */

/**********************************************************************
 *
 * Function:    memTestAddressBus()
 *
 * Description: Test the address bus wiring in a memory region by
 *              performing a walking 1's test on the relevant bits
 *              of the address and checking for aliasing. This test
 *              will find single-bit address failures such as stuck
 *              -high, stuck-low, and shorted pins.  The base address
 *              and size of the region are selected by the caller.
 *
 * Notes:       For best results, the selected base address should
 *              have enough LSB 0's to guarantee single address bit
 *              changes.  For example, to test a 64-Kbyte region, 
 *              select a base address on a 64-Kbyte boundary.  Also, 
 *              select the region size as a power-of-two--if at all 
 *              possible.
 *
 * Returns:     NULL if the test succeeds.  
 *              A non-zero result is the first address at which an
 *              aliasing problem was uncovered.  By examining the
 *              contents of memory, it may be possible to gather
 *              additional information about the problem.
 *
 **********************************************************************/
static int memTestAddressBus(qhalem_handle_t em_h, int part_id,
		unsigned int baseAddress, unsigned long nBytes)
{
	unsigned long addressMask = (nBytes/sizeof(unsigned int) - 1);
	unsigned long offset;
	unsigned long testOffset = 0;
	unsigned int pattern     = (unsigned int) 0xAAAAAAAA;
	unsigned int antipattern = (unsigned int) 0x55555555;
	unsigned int readback_data;
	int ret, err = 0, errors = 0;

	printf("Starting memTestAddressBus: address 0x%08x, nBytes %ld... %s",
			baseAddress, nBytes, (verbose ? "\n" : ""));
	fflush(stdout);

	/*
	 * Write the default pattern at each of the power-of-two offsets.
	 */
	for (offset = 1; (offset & addressMask) != 0; offset <<= 1) {
		if ((ret = qhalem_write_bytes(em_h, part_id, baseAddress + (offset*4), (char *)&pattern, 4)) < 0)
		{
			fprintf(stderr, "qhal_em write failed, returned %d\n", ret);
			err = -1;
			goto out;
		}
	}

	/* 
	 * Check for address bits stuck high.
	 */
	if ((ret = qhalem_write_bytes(em_h, part_id, baseAddress + (testOffset*4), (char *)&antipattern, 4)) < 0)
	{
		fprintf(stderr, "qhal_em write failed, returned %d\n", ret);
		err = -1;
		goto out;
	}

	errors = 0;
	for (offset = 1; (offset & addressMask) != 0; offset <<= 1) {
		if ((ret = qhalem_read_bytes(em_h, part_id, baseAddress + (offset*4), (char *)&readback_data, 4)) < 0)
		{
			fprintf(stderr, "qhal_em read failed, returned %d\n", ret);
			err = -1;
			goto out;
		}
		if (readback_data != pattern) {
			fprintf(stderr, "%saddress 0x%08lx, expected 0x%08x, got 0x%08x, xor 0x%08x... ERROR\n",
					((err == 0 && !verbose) ? "\n" : ""),
						baseAddress+(offset*4), 
						pattern, *(unsigned int *)(&readback_data),
						(pattern ^ *(unsigned int *)(&readback_data)));
			err = -2;
			if (++errors >= max_error_cnt) {
				fprintf(stderr, "Max error counted exceeded, exiting...\n");
				goto out;
			}
		} else if (verbose) {
			fprintf(stderr, "address 0x%08lx: expected 0x%08x, got 0x%08x\n",
						baseAddress+(offset*4), pattern,
						*(unsigned int *)(&readback_data));
		}
	}
	if (err)
		fprintf(stderr, "\n");

	if ((ret = qhalem_write_bytes(em_h, part_id, baseAddress + (testOffset*4), (char *)&pattern, 4)) < 0)
	{
		fprintf(stderr, "qhal_em write failed, returned %d\n", ret);
		err = -1;
		goto out;
	}

	/*
	 * Check for address bits stuck low or shorted.
	 */
	errors = 0;
	for (testOffset = 1; (testOffset & addressMask) != 0; testOffset <<= 1) {
		if ((ret = qhalem_write_bytes(em_h, part_id, baseAddress + (testOffset*4), (char *)&antipattern, 4)) < 0)
		{
			fprintf(stderr, "qhal_em write failed, returned %d\n", ret);
			err = -1;
			goto out;
		}

		readback_data = 0xdeadbeaf;
		if ((ret = qhalem_read_bytes(em_h, part_id, baseAddress, (char *)&readback_data, 4)) < 0)
		{
			fprintf(stderr, "qhal_em read failed, returned %d\n", ret);
			err = -1;
			goto out;
		}
		if (readback_data != pattern) {
			fprintf(stderr, "%saddress 0x%08lx, expected 0x%08x, got 0x%08x, xor 0x%08x... ERROR\n",
					((err == 0 && !verbose) ? "\n" : ""),
						baseAddress+(offset*4), 
						pattern, *(unsigned int *)(&readback_data), 
						(pattern ^ *(unsigned int *)(&readback_data)));
			err = -2;
			if (++errors >= max_error_cnt) {
				fprintf(stderr, "Max error counted exceeded, exiting...\n");
				goto out;
			}
		} else if (verbose) {
			fprintf(stderr, "address 0x%08lx: expected 0x%08x, got 0x%08x\n",
						baseAddress+(offset*4), pattern,
						*(unsigned int *)(&readback_data));
		}

		errors = 0;
		for (offset = 1; (offset & addressMask) != 0; offset <<= 1) {
			readback_data = 0xdeadbeaf;
			if ((ret = qhalem_read_bytes(em_h, part_id, baseAddress + (offset*4), (char *)&readback_data, 4)) < 0)
			{
				fprintf(stderr, "qhal_em read failed, returned %d\n", ret);
				err = -1;
				goto out;
			}

			if ((readback_data != pattern) && (offset != testOffset)) {
				fprintf(stderr, "%saddress 0x%08lx, data 0x%08x != 0x%08x "
						"&& offset 0x%08lx != testOffset 0x%lx... ERROR\n", 
						((err == 0 && !verbose) ? "\n" : ""),
						baseAddress+(offset*4), 
						pattern, *(unsigned int *)(&readback_data),
						offset, testOffset);
				err = -2;
				if (++errors >= max_error_cnt) {
					fprintf(stderr, "Max error counted exceeded, exiting...\n");
					goto out;
				}
			} else {
				if (verbose)
					fprintf(stderr, "address 0x%08lx, 0x%08x == 0x%08x "
							"&& offset 0x%08lx == testOffset 0x%lx\n", 
							baseAddress+(offset*4), pattern,
							*(unsigned int *)(&readback_data),
							offset, testOffset);
			}

		}
		if ((ret = qhalem_write_bytes(em_h, part_id, baseAddress + (testOffset*4), (char *)&pattern, 4)) < 0)
		{
			fprintf(stderr, "qhal_em write failed, returned %d\n", ret);
			err = -1;
			goto out;
		}
	}

out:
	if (!err)
		printf("PASSED\n");
	return err;
}   /* memTestAddressBus() */

/**********************************************************************
 *
 * Function:    memTestDevice()
 *
 * Description: Test the integrity of a physical memory device by
 *              performing an increment/decrement test over the
 *              entire region.  In the process every storage bit 
 *              in the device is tested as a zero and a one.  The
 *              base address and the size of the region are
 *              selected by the caller.
 *
 * Notes:       
 *
 * Returns:     NULL if the test succeeds.
 *
 *              A non-zero result is the first address at which an
 *              incorrect value was read back.  By examining the
 *              contents of memory, it may be possible to gather
 *              additional information about the problem.
 *
 **********************************************************************/
int memTestDevice(qhalem_handle_t em_h, int part_id,
		unsigned int baseAddress, unsigned long nBytes)	
{
	unsigned long offset;
	unsigned long nWords = nBytes / sizeof(unsigned int);
	int ret, err = 0, err2 = 0, errors = 0;
	unsigned int pattern;
	unsigned int antipattern;
	int data_cnt = 0, buffer_size = 0;
	unsigned int *write_buffer = NULL;
	unsigned int *read_buffer = NULL;

	printf("Starting memTestDevice: address 0x%08x, nBytes %ld...\n", 
			baseAddress, nBytes);
	fflush(stdout);

	/* buffer size is determined by nBytes, so lets count and then malloc */
	for (pattern = 1, offset = 0; 
			offset < nWords; pattern++, offset++)
		data_cnt++;

	buffer_size = data_cnt *(sizeof(int));
	write_buffer = (unsigned int *)malloc(buffer_size);
	if (write_buffer == NULL) {
		fprintf(stderr, 
				"ERROR: Could not allocate memory for write buffer\n");
		goto out;
	}
	memset(write_buffer, 0x0, buffer_size);

	read_buffer = (unsigned int *)malloc(buffer_size);
	if (read_buffer == NULL) {
		fprintf(stderr, 
				"ERROR: Could not allocate memory for read buffer\n");
		goto out;
	}

	/*
	 * Fill memory with a known pattern.
	 */
	printf("Starting phase 1...");fflush(stdout);
	for (pattern = 1, offset = 0; offset < nWords; pattern++, offset++) {
		write_buffer[offset] = pattern;
	}
    if ((ret = qhalem_write_bytes(em_h, part_id, baseAddress, (char *)write_buffer, buffer_size)) < 0)
	{
		fprintf(stderr, "qhal_em write failed, returned %d\n", ret);
		err = -1;
		goto out;
	}

	/*
	 * Check each location and invert it for the second pass.
	 */
	memset(read_buffer, 0x0, buffer_size);
    if ((ret = qhalem_read_bytes(em_h, part_id, baseAddress, (char *)read_buffer, buffer_size)) < 0)
	{
		fprintf(stderr, "qhal_em read failed, returned %d\n", ret);
		err = -1;
		goto out;
	}
	errors = 0;
	for (pattern = 1, offset = 0; 
			offset < nWords; pattern++, offset++) {
		if (read_buffer[offset] != pattern) {
			fprintf(stderr, "%saddress 0x%08lx, expected 0x%08x, got 0x%08x, xor 0x%08x... ERROR\n",
					((err == 0 && !verbose) ? "\n" : ""),
						baseAddress+(offset*4), 
						pattern, read_buffer[offset],
						pattern ^ read_buffer[offset]);
			err = -2;
			if (++errors >= max_error_cnt) {
				fprintf(stderr, "Max error counted exceeded, exiting...\n");
				goto out;
			}
		}
		antipattern = ~pattern;
		write_buffer[offset] = antipattern;
	}
	if (!err)
		printf("PASSED\n");
	else
		printf("Phase 1... FAILED\n");
	fflush(stdout);

	/* write inverted pattern */
    if ((ret = qhalem_write_bytes(em_h, part_id, baseAddress, (char *)write_buffer, buffer_size)) < 0)
	{
		fprintf(stderr, "qhal_em write failed, returned %d\n", ret);
		err = -1;
		goto out;
	}

	/*
	 * Check each location for the inverted pattern and zero it.
	 */
	memset(read_buffer, 0x0, buffer_size);
    if ((ret = qhalem_read_bytes(em_h, part_id, baseAddress, (char *)read_buffer, buffer_size)) < 0)
	{
		fprintf(stderr, "qhal_em read failed, returned %d\n", ret);
		err = -1;
		goto out;
	}
	printf("Starting phase 2...");fflush(stdout);
	errors = 0;
	for (pattern = 1, offset = 0; 
			offset < nWords; pattern++, offset++) {
		antipattern = ~pattern;
		if (read_buffer[offset] != antipattern) {
			fprintf(stderr, "%saddress 0x%08lx, expected 0x%08x, got 0x%08x, xor 0x%08x... ERROR\n",
					((err2 == 0 && !verbose) ? "\n" : ""),
						baseAddress+(offset*4), 
						antipattern, read_buffer[offset],
						antipattern ^ read_buffer[offset]);
			err2 = -2;
			if (++errors >= max_error_cnt) {
				fprintf(stderr, "Max error counted exceeded, exiting...\n");
				goto out;
			}
		}
	}
	if (!err2)
		printf("PASSED\n");
	else
		printf("Phase 2... FAILED\n");
	fflush(stdout);

out:
	if (read_buffer != NULL)
		free(read_buffer);
	if (write_buffer != NULL)
		free(write_buffer);
	return err;
}   /* memTestDevice() */

int main(int argc, char *argv[])
{
    qhalem_handle_t em_h;
    int ret;
    FILE *fp = NULL ;
    int part_id, offset, buffersize = DEFAULT_XFER_SIZE, byte_cnt = 0, c;
    int burst_sz    = DEFAULT_BURST_SIZE ;
    QHALEM_BURSTSIZE burst;
    int threshold   = DEFAULT_THRESHOLD;
    char filename[1024]={'\0'};
    char mode = 'n';
    char pretty = 0;
    char *buffer = NULL;
    int exit_value  = 0 ;
    int byteswap = 0;
    int codec = 0;
    QHALEM_ACCESSTYPE emtype = QHALEM_ACCESSTYPE_STREAM;

    fprintf( stderr , "%s: Built %s %s\n" , argv[0] , __DATE__ , __TIME__ ) ;
    if (argc < 3) {
        usage(argv[0]);
        return 1;
    }

    part_id = strtol(argv[1], 0, 0);
    offset = strtol(argv[2], 0, 0);
	optind = 3;
    while ( (c = getopt(argc, argv, "c:u:t:b:d:f:m:i:lph")) > 0 ) {
        switch (c) {
        case 'c':
            codec = strtol(optarg, 0, 0);
            break;
        case 'u':
            burst_sz = strtol(optarg, 0, 0);
            break;
        case 't':
            threshold = strtol(optarg, 0, 0);
            break;
        case 'b':
            buffersize = strtol(optarg, 0, 0);
            break;
        case 'd':
            byte_cnt = strtol(optarg, 0, 0);
            break;
        case 'f':
            sscanf(optarg, "%s", filename);
            break;
        case 'm':
            sscanf(optarg, "%c", &mode);
            break;
        case 'i':
            emtype = strtol(optarg, 0, 0);
            break;
        case 'l':
            byteswap=1;
			break;
        case 'p':
	    pretty = 1;
	    break;
        case 'h':
            usage(argv[0]);
            return 0;
        default:
            fprintf(stderr,"unknown option %c\n", c);
            usage(argv[0]);
            return 1;
        }
    }

    if (mode == 'r') {
        if (byte_cnt <= 0 ) {
            fprintf( stderr , "Invalid word count (%d) for read mode\n" , byte_cnt ) ;
            usage(argv[0]);
            return 1;
        }
        if (filename[0]=='\0') {
            fp = stdout;
        }
        else {
            if ( ( fp = fopen(filename, "wb")) == NULL ) {
                fprintf( stderr , "Failed to open output file \"%s\"\n" ,  filename ) ;
                return 1;
            }
        }

		buffer = (char *)malloc(buffersize);
		if (!buffer) {
			fprintf(stderr, "Failed to allocate internal buffer\n");
			return 1;
		}
    }
    else if (mode == 'w') {
        if (filename[0]=='\0') {
            if ( byte_cnt<= 0) {
                fprintf( stderr , "Invalid word count (%d) for write mode\n" , byte_cnt ) ;
                usage(argv[0]);
                return 1;
            }
            fp = stdin;
        }
        else {
            if ( ( fp = fopen(filename, "rb")) == NULL ) {
                fprintf( stderr , "Failed to open input file \"%s\"\n" ,  filename ) ;
                return 1;
            } else {
                fprintf( stderr , "Opened input file \"%s\"\n" ,  filename ) ;
            }
        }

		buffer = (char *)malloc(buffersize);
		if (!buffer) {
			fprintf(stderr, "Failed to allocate internal buffer\n");
			return 1;
		}
    }
    else if (mode == '0') {
		memtest_desc();
		return 0;
    }
    else if (mode == '1') {
    }
    else if (mode == '2') {
    }
    else if (mode == '3') {
    }
    else {
        fprintf( stderr , "Invalid mode\n" ) ;
        usage(argv[0]);
        return 1;
    }

    // burst size fix to 8, no transfer size limit
    switch (burst_sz)
    {   case 8:
            burst = QHALEM_BURSTSIZE_8WORDS;
            break;
        case 16:
            burst = QHALEM_BURSTSIZE_16WORDS;
            break;
        case 32:
            burst = QHALEM_BURSTSIZE_32WORDS;
            break;
        case 64:
            burst = QHALEM_BURSTSIZE_64WORDS;
            break;
        default:
            fprintf( stderr , "Invalid burst size (%d)\n" , burst_sz ) ;
            usage(argv[0]);
            return 1;
    }

    em_h = qhalem_open_codec(codec, emtype);
    if (em_h < 0)
    {
        fprintf(stderr, "Can't open qhalem, returned %d\n", em_h);
        return 1;
    }
    ret = qhalem_setconfig(em_h, threshold, burst, QHALEM_PRIORITY_NORMAL);
    if (ret < 0)
    {
        fprintf(stderr, "Can't set requested config: threshold=%d, burst_sz=%d, returned %d\n",threshold,burst_sz,ret);
        qhalem_close(em_h);
        return 1;
    }

	switch (mode) {
    case 'r':
	{    // Read from memory
        int     left ;
        int     size = buffersize;
        size_t  lrv ;
        int     progress    = 0 ;
	int i;

        for ( left = byte_cnt ; ( exit_value == 0 )  && ( left > 0 ) ; left -= size ) {
            size = ( buffersize > left ) ? left : buffersize ;
            if((ret=qhalem_read_bytes(em_h, part_id, offset, buffer, size))<0)
            {   fprintf(stderr, "read failed, returned %d\n",ret);
                exit_value  = 1 ;
                break;
            };
			if (byteswap)
			{
				for (i = 0; i < size; i += 4)
				{
					char tmp = buffer[i];

					buffer[i] = buffer[i+3];
					buffer[i+3] = tmp;
					tmp = buffer[i+1];
					buffer[i+1] = buffer[i+2];
					buffer[i+2] = tmp;
				}
			}
			if (pretty) {
			        unsigned char *prettyprint = (unsigned char *)buffer;
				unsigned int addr_cnt = offset;
				int bytes = size;

				while (bytes >= 16) {
					printf("0x%08x:  0x%02x%02x%02x%02x 0x%02x%02x%02x%02x 0x%02x%02x%02x%02x 0x%02x%02x%02x%02x\n",
							addr_cnt,
							prettyprint[0], prettyprint[1], prettyprint[2], prettyprint[3], 
							prettyprint[4], prettyprint[5], prettyprint[6], prettyprint[7], 
							prettyprint[8], prettyprint[9], prettyprint[10], prettyprint[11], 
							prettyprint[12], prettyprint[13], prettyprint[14], prettyprint[15]);
					bytes -= 16;
					addr_cnt += 16;
					prettyprint += 16;
				}
				for (i = 0; i < bytes; i++, addr_cnt++) {
					printf("0x%08x:  0x%02x\n", addr_cnt, prettyprint[i]);
				}
			}
            else if ( ( lrv = fwrite(buffer, 1, size, fp) ) != size ) {
                fprintf( stderr , "Failed to write to input after %d bytes\n" , byte_cnt - left - lrv ) ;
                exit_value  = 1 ;
                break;
            }
            if ( ( ++progress % 1000 ) == 0 ) {
                fprintf( stderr , "." ) ;
            }
            offset    += size ;
        }
    }
	break;

	case 'w':
	{      // Write to memory
        size_t  lrv ;
        int     progress    = 0 ;
        if ( byte_cnt ) {       // A size is specified
            int     left ;
            int     size ;
            int     sum     = 0 ;
            for ( left = byte_cnt ; ( exit_value == 0 )  && ( left > 0 ) ; left -= size ) {
                size = ( buffersize > left ) ? left : buffersize ;
                if ( ( lrv = fread(buffer, 1, size, fp) ) == size ) {
					if(byteswap)
					{	int i;
						for(i=0; i<lrv; i+=4)
						{	char tmp=buffer[i];
							buffer[i]=buffer[i+3];
							buffer[i+3]=tmp;
							tmp=buffer[i+1];
							buffer[i+1]=buffer[i+2];
							buffer[i+2]=tmp;
						}
					}
                    if((ret=qhalem_write_bytes(em_h, part_id, offset, buffer, size))<0)
                    {   fprintf(stderr, "write failed, returned %d\n",ret);
                        exit_value  = 1 ;
                        break;
                    };
                    sum += lrv ;
                } else {
                    perror( "fread()" ) ;
                    fprintf( stderr , "Failed to read from input after %d bytes\n" , byte_cnt - left - lrv ) ;
                    exit_value  = 1 ;
                }
                if ( ( ++progress % 1000 ) == 0 ) {
                    fprintf( stderr , "." ) ;
                }
                offset  += size ;
            }
            fprintf( stderr , "\nWrote %d bytes\n" , sum ) ;
        } else {
            int sum = 0 ;
            while ( ( exit_value == 0 )  && ! feof( fp ) ) {
                if ( ( lrv = fread(buffer, 1, buffersize, fp) ) > 0 ) {
					/* Transfer size must be modulo 4, if the file isn't we pad */
					if(lrv%4)
						lrv+=4-(lrv%4);
					if(byteswap)
					{	int i;
						for(i=0; i<lrv; i+=4)
						{	char tmp=buffer[i];
							buffer[i]=buffer[i+3];
							buffer[i+3]=tmp;
							tmp=buffer[i+1];
							buffer[i+1]=buffer[i+2];
							buffer[i+2]=tmp;
						}
					}
                    if((ret=qhalem_write_bytes(em_h, part_id, offset, buffer, lrv))<0)
                    {   fprintf(stderr, "write failed, returned %d\n",ret);
                        exit_value  = 1 ;
                        break;
                    };
                    sum += lrv ;
                } else if ( lrv < 0 ) {
                    perror( "fread()" ) ;
                    fprintf( stderr , "Failed to read from input after %d bytes\n" , sum ) ;
                    exit_value  = 1 ;
                }
                if ( ( ++progress % 1000 ) == 0 ) {
                    fprintf( stderr , "." ) ;
                }
                offset  += lrv ;
            }
            fprintf( stderr , "\nWrote %d bytes\n" , sum ) ;
        }
    }
	break;

	case '1':
		exit_value = memTestDataBus(em_h, part_id, offset);
		if (exit_value) {
		}
	break;

	case '2':
		exit_value = memTestAddressBus(em_h, part_id, offset, byte_cnt);
		if (exit_value) {
		}
	break;

	case '3':
		exit_value = memTestDevice(em_h, part_id, offset, byte_cnt);
		if (exit_value) {
		}
	break;
	}

    /*
    * Clean up
    */
    if ((ret = qhalem_close(em_h)) < 0)
        fprintf(stderr, "failed to close qhalem properly, returned %d\n",ret);
    if (filename[0] != '\0') {
        fclose(fp);
    }
	if (buffer != NULL)
	    free(buffer);
    return exit_value;
}

