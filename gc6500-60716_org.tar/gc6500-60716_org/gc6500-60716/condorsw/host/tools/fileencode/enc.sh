#!/bin/sh
# program for transferring source video to CondorBuB
# Make sure that mxuvc is not running already; if yes, kill it and boot image again after reset
export srcFile="/directmnt/project/video/original/420/720x480/Night/Night_720x480_30p.yuv"
export outFile="cs_720x480p_night_Bitrate_500000.h4v"
sudo echo File encode
../../apps/mxuvc/mxuvc bitrate 500000
../../apps/mxuvc/mxuvc stream --vout1 $outFile &
sleep 1
sudo ./i686-condora0-usb.build/fileencode $srcFile 720 480 0 30 200
#sudo ./i686-sub20-condora0.build/fileencode $srcFile 720 480 0 30 200
pkill mxuvc
chmod 644 $outFile
