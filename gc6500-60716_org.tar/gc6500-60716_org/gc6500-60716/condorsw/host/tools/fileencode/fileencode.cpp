/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

#include <stdio.h>
#include "hostio.h"
#include "fin_fout_video.h"

extern "C" int WriteVideoFrame(FILE *fpi, int width, int height, int bufferwidth, FILE *fpoy, FILE *fpouv, int YUVformat , int startframe);
#define BUF_SIZE    1024

int ReadWrite(char *buf, FILE *fin, FILE *fout);

// Read in all the data in fin and write them to fout.
int ReadWrite(char *buf, FILE *fin, FILE *fout)
{
    int bytes=0;
    int lenr, lenw;

    do
    {
        lenr = fread(buf, 1, BUF_SIZE, fin);
        bytes += lenr;
        if (lenr)
        {
            lenw = fwrite(buf, 1, lenr, fout);
            if (lenr != lenw)
            {
                printf("Error: read [%d] bytes, write [%d] bytes\n", lenr, lenw);
                return 0;
            }
        }
    } while (lenr==BUF_SIZE);
    return(bytes);
}

int main( int argc, char* argv[])
{
    FILE *fpoy,  *fpouv,  *fpoyuv;
    FILE *fpi;
    int loopCount=1;     //Loop count
    int loop;            //Current loop number

    int ysize = 0, uvsize=0;
    char fname_yuv[]="/tmp/fpo_yuv";

    unsigned char ch;
    int delayTime=100*1000; // 100 milli-sec

    int width=0, height=0, numFrames=0, format = 0, count =0;

    if (argc < 4)
    {
        printf("Usage: fileencode <filename with path> <width> <height> <videoFormat> <num frames> <delay_time (in millisec)> <loop count>\n");
        return 0;
    }

    if (argv[2])
        width = atoi(argv[2]);

    if (argv[3])
        height = atoi(argv[3]);
    if (argv[4])
        format = atoi(argv[4]);

    if (argv[5])
        numFrames = atoi(argv[5]);

    if (argv[6])
    {
        int factor = atoi(argv[6]);
        if (factor >= 0)
            delayTime = factor*1000;
    }

    if (argv[7])
        loopCount = atoi(argv[7]);
    
    printf("Loop Count = %d\n", loopCount);
    
    printf("Delaytime [%d] millisec\n", delayTime/1000);

    usleep(500*1000);  // pause for 0.5 sec to make sure client side is ready to receive data.
    printf("Pause for 0.5 sec!!!\n");

    FileIn fin1(1);
    fpi = fopen(argv[1], "r");
    if (fpi==NULL)
    {
        printf("Cannot open input file: %s\n", argv[1]);
        return 0;
    }

    if (numFrames == 0)  //Find number of frames depending on format
    {
        if (format == 0 || format == 5)    //YUV420 or NV12
        {
            long len;
            int frameSize;

            frameSize = width*height + width*height/2;
            fseek(fpi, 0, SEEK_END);
            len = ftell(fpi);
            fseek(fpi, 0, SEEK_SET);
            numFrames = len/frameSize;
        }
        else
        {
            //We don't support finding out number of frames automatically for other formats
            fclose(fpi);
            printf("Error: Number of frames specified is 0\n");
            return 0;
        }
    }
    fpoy = fopen("/tmp/fpo_y", "w+");
    fpouv = fopen("/tmp/fpo_uv", "w+");

    printf("Sending file[%s]\n", argv[1]);
    printf("  width[%d] height[%d] NumFrames[%d]\n", width, height, numFrames);
    switch (format)
    {
    case 0:  // YUV420 Planar (MPEG)
        printf("(File format: YUV420 Planar)\n");
        break;
    case 1:  // YUV422 Interleaved (ITU656)
        printf("(File format: YUV422 Interleaved (ITU656))\n");
        break;
    case 2:  // YUV422 Planar
        printf("(File format: YUV422 Planar)\n");
        break;
    case 3:  // YUV411 Planar
        printf("(File format: YUV411 Planar)\n");
        break;
    case 4:  // YUV444 Planar (Graphics)
        printf("(File format: YUV444 Planar (Graphics)\n");
        break;
    case 5:  // NV12
        printf("(File format: NV12)\n");
        break;
    default: // YUV420 Planar (MPEG)
        printf("(YUV420 Planar)\n");
        break;
    }

    HostIOInit(0x29fe, 0x4d53);
    printf("HostInit Done\n");

    char *buf = (char *)malloc(BUF_SIZE);
    int len, bytes;
    int rval;
    for(loop = 0; loop < loopCount; loop++)
    {
        printf("Loop %d\n", loop);
        for (count =0; count<numFrames; count++)
        {
            // Read in a raw frame (frame number = count) from file fpi
            // convert it to tile format, and output to a tiled y file (fpoy) and
            // a tiled uv file (fpout).
            WriteVideoFrame( fpi, width, height, width, fpoy, fpouv, format, count);

            // Find ysize and usize
            fseek(fpoy, 0L, SEEK_END);
            ysize = ftell(fpoy);

            fseek(fpouv, 0L, SEEK_END);
            uvsize = ftell(fpouv);

            rewind(fpoy);
            fpoyuv = fopen(fname_yuv, "w");
            if (fpoyuv==NULL)
            {
                printf("Cannot open file: [%s]\n", fname_yuv);
                return 0;
            }
            // Read in the tiled y (fpoy) and output it to fpoyuv
            bytes = ReadWrite(buf, fpoy, fpoyuv);

            // Read in the tiled uv (fpouv) and attached it to fpoyuv
            rewind(fpouv);
            bytes += ReadWrite(buf, fpouv, fpoyuv);
            fclose(fpoyuv);

            rewind(fpoy);
            rewind(fpouv);

            // Check if it is the last frame, if last frame set the count to -1
            if ( count==numFrames-1 && loop == loopCount-1)
                fin1.SetFileInfo(width, height, format, ysize, uvsize, -1); // Last Frame
            else
                fin1.SetFileInfo(width, height, format, ysize, uvsize, loop*numFrames + count);

            // Send the tiled yuv file through USB interface to Condor.
            rval = fin1.SendFileEncode(fname_yuv, ysize+uvsize);
            if (rval == -1)
            {
                printf("Function [%s]: failed\n", __FUNCTION__);
                return 0;
            }
            printf("Frame [%d] sent!\n", count);
            usleep(delayTime);
        }
        rewind(fpi);
    }

    free(buf);
    fclose(fpi);
    fclose(fpoy);
    fclose(fpouv);
    printf("FileInfo - format[%d], ysize[%d], uvsize[%d], width[%d], height[%d]\n",
           format, ysize, uvsize, width, height);
    printf("Total number of frames sent: %d\n", count);
    HostIODeinit();
    return 0;
}

/*******************************************************************************
 * vi: set shiftwidth=4 tabstop=8 softtabstop=4 expandtab nosmarttab:
 ******************************************************************************/
