Overview:
=========

The file encode tool is used to generate a H264 file from either a YUV420 or NV12
input file. The ISP is bypassed in this mode.

Usage:
======

First the system needs to be booted with a firmware and appropriate JSON file.
A sample JSON file in SVN workspace is:
    $GEOSW_ROOT/condorsw/codec/mainapp/json/fileavcenc.json
This is copied in release to this directory.

Host side tools must be built first. 
If using SVN workspace:
cd $GEOSW_ROOT
make host

If using release:
cd $GEOSW_ROOT
make -C thirdparty minimal
make -C condorsw/host/lib/mxcam
make -C condorsw/host/apps/mxcam
make -C condorsw/host/lib/mxuvc
make -C condorsw/host/apps/mxuvc
make -C condorsw/host/lib/hostio
make -C condorsw/host/lib/qhal
make -C condorsw/host/tools/fileencode

Edit the file fileavcenc.json for specifying the correct input width and height in following:

        "vcap0": {
            "type": "fevcap",
            "cropWidth": 720,
            "cropHeight": 480,

Also edit the BOARD variable. The default is:
        "BOARD": "condorbub"

For example, for 1280x720 input, specify cropWidth as 1280 and cropHeight as 720.
For board mobileyes5, change BOARD to "mobileyes5".
Any other AVC encoder parameter can be changed as desired except for the following:
        "Q_AVCENC_CMP_NTP_ENABLE": 1,
The above setting will disable any frame drop logic inside AVC encoder as this is 
not relevant when encoding from a non real time input such as a file.

The camera should be in USB boot mode. Boot with image and JSON as follows:

cd $GEOSW_ROOT/condorsw/host/tools/fileencode
sudo $GEOSW_ROOT/condorsw/host/apps/mxcam/mxcam boot <fw image> fileavcenc.json

Then modify the sample script enc.sh accordingly. For example:
Modify the AVC encoder parameters (such as bitrate and gop size) using mxuvc.
Modify fileencode parameters.
The arguments expected by fileencode tool itself are:

./i686-condora0-usb.build/fileencode
Usage: fileencode <filename with path> <width> <height> <videoFormat> <num frames> <delay_time (in millisec)> <loop count>

Where:
width:       Input width of frame
height:      Input height of frame
videoFormat: 0 for YUV420,  5 for NV12
num frames:  Number of frames to process from input file. 
             Keep 0 for processing all frames.
delay_time:  Delay (in ms)  before sending next frame from host to camera.
             Tested mostly with value 200.
loop count:  Number of times to loop the input file.
             If not specified, 1 is used.

Run the updated script as follows:

./enc.sh

The output will go in the file specified by the variable outFile in enc.sh

Multiple encodings in one datapath
==================================

A sample JSON fileavcenc_hd_rc_multi.json and corresponding shell script enc_hd_multi.sh is
also provided which uses 6 encoder instances with different bitrates all using the new rate control
BUFFER_MODEL. This will speed up the process instead of running a single encode in each file encode
session. Modify according to your needs as explained earlier.


