#
# Evaluate the video quality
#

export QUARC_ROOT=/home/udit/svnproject

###############################################################
#
# Evaluate video: 39SD_Weather_Report.yuv
#
export pgrm="/home/udit/svnproject/algorithms/video/tools/VQM/evaluatevqm.pl"

#
# Evaluate 39SD_Weather_Report.yuv
#
export srcV="/project/video/Fidelity/Condor/ref_video/720x480/39SD_Weather_Report.yuv"

# Bit rate = 500 kbps
export bitst="cs_720x480p_weather_Bitrate_500000.h4v"

# $pgrm $srcV $bitst width height 0 numFrames
$pgrm $srcV $bitst 720 480 0 450

# Bit rate = 750 kbps
export bitst="cs_720x480p_weather_Bitrate_750000.h4v"
$pgrm $srcV $bitst 720 480 0 450

# Bit rate = 1000 kbps
export bitst="cs_720x480p_weather_Bitrate_1000000.h4v"
$pgrm $srcV $bitst 720 480 0 450

# Bit rate = 1500 kbps
export bitst="cs_720x480p_weather_Bitrate_1500000.h4v"
$pgrm $srcV $bitst 720 480 0 450

# Bit rate = 2000 kbps
export bitst="cs_720x480p_weather_Bitrate_2000000.h4v"
$pgrm $srcV $bitst 720 480 0 450

###############################################################
#
# Evaluate video: Night_720x480_30p.yuv
#
export srcV="/project/video/Fidelity/Condor/ref_video/720x480/Night_720x480_30p.yuv"

# Bit rate = 500 kbps
export bitst="cs_720x480p_night_Bitrate_500000.h4v"
$pgrm $srcV $bitst 720 480 0 230

# Bit rate = 750 kbps
export bitst="cs_720x480p_night_Bitrate_750000.h4v"
$pgrm $srcV $bitst 720 480 0 230

# Bit rate = 1000 kbps
export bitst="cs_720x480p_night_Bitrate_1000000.h4v"
$pgrm $srcV $bitst 720 480 0 230

# Bit rate = 1500 kbps
export bitst="cs_720x480p_night_Bitrate_1500000.h4v"
$pgrm $srcV $bitst 720 480 0 230

# Evaluate 39SD_Weather_Report.yuv at 2000k bps
export bitst="cs_720x480p_night_Bitrate_2000000.h4v"
$pgrm $srcV $bitst 720 480 0 230


###############################################################
#
# Evaluate video: 03SD_Woman_with_Bird_Cage.yuv
#
export srcV="/project/video/Fidelity/Condor/ref_video/720x480/03SD_Woman_with_Bird_Cage.yuv"

# Bit rate = 500 kbps
export bitst="cs_720x480p_bird_Bitrate_500000.h4v"
$pgrm $srcV $bitst 720 480 0 450

# Bit rate = 750 kbps
export bitst="cs_720x480p_bird_Bitrate_750000.h4v"
$pgrm $srcV $bitst 720 480 0 450

# Bit rate = 1000 kbps
export bitst="cs_720x480p_bird_Bitrate_1000000.h4v"
$pgrm $srcV $bitst 720 480 0 450

# Bit rate = 1500 kbps
export bitst="cs_720x480p_bird_Bitrate_1500000.h4v"
$pgrm $srcV $bitst 720 480 0 450

# Evaluate 39SD_Weather_Report.yuv at 2000k bps
export bitst="cs_720x480p_bird_Bitrate_2000000.h4v"
$pgrm $srcV $bitst 720 480 0 450



