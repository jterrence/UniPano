#!/bin/sh
# program for transferring source video to Condor board
# Make sure that mxuvc is not running already; if yes, kill it and boot image again after reset
export srcFile="pedestrian_area_1080p25.yuv"
export outFile1="pedestrian1.h4v"
export outFile2="pedestrian2.h4v"
export outFile3="pedestrian3.h4v"
export outFile4="pedestrian4.h4v"
export outFile5="pedestrian5.h4v"
sudo echo File encode
../../apps/mxuvc/mxuvc stream --vout1 $outFile1 --vout2 $outFile2 --vout3 $outFile3 --vout4 $outFile4 --vout5 $outFile5 &
sleep 1
#Encoding all frames - change accordingly
sudo ./i686-condora0-usb.build/fileencode $srcFile 1920 1080 0 100 200
sleep 1
pkill mxuvc
chmod 644 $outFile1
chmod 644 $outFile2
chmod 644 $outFile3
chmod 644 $outFile4
chmod 644 $outFile5
chmod 644 $outFile6
