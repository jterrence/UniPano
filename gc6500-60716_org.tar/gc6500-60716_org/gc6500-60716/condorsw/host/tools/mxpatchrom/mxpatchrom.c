/*******************************************************************************
*
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this
* copyright notice.
*
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
*
*******************************************************************************/
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <stdint.h>
#include <assert.h>
#include <unistd.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>
#include <math.h>
#include <libgen.h>
#include <ldmap.h>
#include "cJSON/cJSON.h"

#define PADDING (32)
#define BOOTLDR_ADDR 0
// 4 bytes for TCW  and 24K for bootloader
#define MAX_BOOTLDR_SIZE ( 64 * 1024 )
// first 12 bytes are TCW, JUMP, NOP
#define BOOTLDR_HDR_START 8
// just after boot loader
#define EEPROM_CCR_START BOOTLDR_SIZE
// on second sector
#define SNOR_CCR_START	  ( 64 * 1024 )
#define MAX_JSON_SIZE     ( 192 * 1024)
#define MAX_MINIFIED_JSON_SZ     ( 32 * 1024 )
#define MAX_ALPHA_FILE_SIZE	  ( 32 * 1024 )


#define SNOR_JSON2_START    SNOR_CCR_START + MAX_MINIFIED_JSON_SZ
// Doesn't need to be bigger that the control endpoit maximum buffer size

// 4th sector
#define MAX_ISP_SIZE 	(32 * 1024)
#define SNOR_ISP_START	(3 * 64 * 1024)

// 5th sector
#define MAX_GRID_MAP_SIZE 	(64 * 1024)
#define SNOR_GRID_MAP1_START	(2 * 64 * 1024)
#define SNOR_ISP2_START     ((3 * 64 * 1024) + MAX_ISP_SIZE)
#define SNOR_GRID_MAP2_START	(4 * 64 * 1024)
#define MAX_ALPHA_MAP_SIZE 	(64 * 1024)


//MAX_JSON_SIZE covers up ISP_SIZE also, in terms of flash sectors.
#define SNOR_CCR_SZ	(SNOR_CCR_START + MAX_JSON_SIZE)
#define APP_IMG_START 	(5 * 64 * 1024) //64 * 1024*(((SNOR_CCR_SZ + MAX_GRID_MAP_SIZE)/(64*1024)) + 1)

#define SNOR_ALPHA_PHOTO	( 62* 64 * 1024 )
#define SNOR_ALPHA_VIDEO	( 63* 64 * 1024 )
#define APP_IMG_END 		( 64* 64 * 1024)

#define JSON_HDR_SIZE	8
#define ISP_HDR_SIZE	8

#define printf_bold(args...) \
	do { \
		printf("%c[1m", 27); \
		printf(args); \
		printf("%c[0m", 27); \
	} while(0)

#define PRINTV(args...) do { if(verbose) printf(args); } while (0)
#define PRINTE(args...) do { printf("ERROR: "); printf(args); } while (0)

//structures and globals
static int verbose = 0;
static const char *map_args[8];
static const char *alpha_args[6];
static int alpha = 0;


static struct option longopts[] = {
	{"bootloader", required_argument, NULL, 'b'},
	{"json", required_argument, NULL, 'j'},
	{"isp", required_argument, NULL, 'i'},
	{"map0", required_argument, NULL, 'm'},
	{"map1", required_argument, NULL, 'm'},
	{"map2", required_argument, NULL, 'm'},
	{"map3", required_argument, NULL, 'm'},
	{"obj0", required_argument, NULL, 'o'},
	{"obj1", required_argument, NULL, 'o'},
	{"obj2", required_argument, NULL, 'o'},
	{"obj3", required_argument, NULL, 'o'},
	{"a1", required_argument,   NULL, 'a'},
	{"a2", required_argument,   NULL, 'a'},
	{"a3", required_argument,   NULL, 'a'},
	{"a4", required_argument,   NULL, 'a'},
	{"fw", required_argument, NULL, 'f'},
	{"help", no_argument, NULL, 'h'},
	{"verbose", no_argument, NULL, 'v'},
	{"rom", required_argument, NULL, 'r'},
    {"json2", required_argument, NULL, 'j'},
    {"isp2", required_argument, NULL, 'i'},
	{NULL, 0, NULL, 0}
};

typedef enum{
	BOOTLOADER = 0,
	JSON,
	ISP,
	FW_IMAGE,
    JSON2,
    MAP1,
    MAP2,
    ISP2,
	UNDEFINED
}image_type;

//functions starts from here
static void usage(void)
{
	printf_bold("Usage:\n");
	printf("mxpatchrom --bootloader <bootloader.rom> --json <json-file> --isp <ispcfg-file>\n");
	printf("           --fw <firmware.img> --rom <rom-file.rom> [--verbose] [--map0/1/2 <grid.map>] [--json2 <json-file>]\n");
    printf("            [--isp2 <second-ispcfg-file>]\n");
	printf("            [--obj0/1/2 <grid-map object>] \n");
	printf("            [--a1/2/3/4 <alpha>] \n");
	printf("\nMandatory Parameters are: \n");
	printf("1)bootloader (--bootloader)\n");
	printf("2)json-file (--json)\n");
	printf("3)ispcfg-file (--isp)\n");
	printf("4)firmware (--fw)\n");

	printf("\nOptional Parameters are: \n");
	printf("1)rom-file (--rom)\n");
	printf("2)verbose (--verbose)\n");
	printf("3)gridmap-file (--map0/1/2/3)\n");
	printf("4)gridmap-object (--obj0/1/2/3)\n");
    printf("5)second-json-file (--json2)\n");
    printf("6)alpha (--a1/2/3/4)\n");
    printf("3)second-ispcfg-file (--isp2)\n");
}

static int isfileexist(const char *file)
{
	struct stat flinfo;	/* structure holding file info read by stat() */
	if ((stat(file, &flinfo)) == -1) {
		PRINTE("Failed to open %s \n", file);
		return 1;
	}
	return 0;
}

/* take out file extension(.json/.rom/.img) from file name and return */
int recover_filename(char *file, char *name) {
	int size = 0;
	char *bname = NULL, *file_name = NULL;

	//find out the actual file name
	bname = strdup(file);
	if(bname == NULL){
		PRINTE("bname is NULL\n");
		return 1;
	}
	file_name = basename(bname);

	if(file_name == NULL){
		PRINTE("filename is NULL\n");
		return 1;
	}
	if(strlen(file_name) > 255){
		PRINTE("filename length is greater then 255\n");
		return 1;
	}

	//take out the last .*
	char *e = strrchr (file_name, '.');
    	if (e == NULL)
        	e = "";
	size = strlen(file_name);
	strcpy(name, file_name);
	name[size-strlen(e)] = 0; //add null

	return 0;
}

int update_rom(const char *rom_img, char *image, image_type type)
{
	struct stat flinfo;
	char *buf = NULL;
	int count = 0;
	FILE *fin, *fout;
	int ret = 0;
	cJSON *root = NULL;
	char json_hdr[8];
	int write_location = 0;
	char *read_buf = NULL;
	unsigned int length = 0;

	if(type == BOOTLOADER)
		fout = fopen(rom_img, "w+");
	else
		fout = fopen(rom_img, "a+");
	if (fout == NULL) {
		PRINTE("Failed to open %s\n",rom_img);
		return 1;
	}

	fin = fopen(image, "r");
	if (fin == NULL) {
		PRINTE("Failed to open %s\n",image);
		fclose(fout);
		return 1;
	}

	if ((stat(image, &flinfo)) == -1) {
		PRINTE("Failed to open %s \n", image);
		fclose(fin);
		fclose(fout);
		return 1;
	}

	if(flinfo.st_size <= 0){
		PRINTE("Invalid image size %d\n", (int)(flinfo.st_size));
		fclose(fin);
		fclose(fout);
		return 1;
	}
	// +1 for '\0'
	buf = (char *)malloc(flinfo.st_size+JSON_HDR_SIZE+1);
	if(buf == NULL){
		PRINTE("malloc failed %s:%d\n",__func__,__LINE__);
		fclose(fin);
		fclose(fout);
		return 1;
	}

	//basic sanity check of the image
	switch(	type ){
		case BOOTLOADER :
			if(flinfo.st_size > MAX_BOOTLDR_SIZE){
				PRINTE("Invalid bootloader image size %d\n",(int)flinfo.st_size);
				ret = 1;
				break;
			}
		break;
		case JSON :
        case JSON2:
			//0-3 bytes will have "JSON" and then
			//4-8 bytes will have json file size
			memcpy((char *)&json_hdr[0], "JSON", 4);
			memcpy(&json_hdr[4], &flinfo.st_size, 4);
			write_location = JSON_HDR_SIZE;

			memcpy(buf, json_hdr, sizeof(json_hdr));
		break ;

		case ISP :
        case ISP2 :
			if(flinfo.st_size > MAX_ISP_SIZE){
				PRINTE("Invalid Ispcfg file size %d\n",(int)flinfo.st_size);
				ret = 1;
				break;
			}
			//0-3 bytes will have "ISP" and then
			//4-7 bytes will have ispcfg file size
			memcpy((char *)buf, "ISP", 4);
			memcpy((char *)buf + 4, &flinfo.st_size, 4);
			write_location = ISP_HDR_SIZE;
		break ;

		case FW_IMAGE :
			//max size should be less then flash size TBD

		break;
		case UNDEFINED :
		case MAP1:
		case MAP2:
			PRINTE("Undefined image type provided\n");
			ret = 1;
		break;

	}
	if(ret){
		fclose(fin);
		fclose(fout);
		free(buf);
		return ret;
	}
	read_buf = (char *)(buf+write_location);

	count = fread((char *)(read_buf), (size_t)flinfo.st_size, 1, fin);
	if(count != 1){
		PRINTE("fread failed %s:%d\n",__func__,__LINE__);
		fclose(fin);
		fclose(fout);
		free(buf);
		return 1;
	}

	length = flinfo.st_size + write_location;
	//for json need to syntax check before write
	if(type == JSON || type == JSON2){
		root = cJSON_Parse((const char *)(read_buf));
		if(root==NULL){
			ret = 1;
			PRINTE("JSON syntax error\n");
		}
		cJSON_Delete(root);

		//Minify JSON
		*((char *)read_buf + (size_t)flinfo.st_size) = 0;
		cJSON_Minify((char *)read_buf);
		length = (int)strlen((const char *)read_buf) + 1;

		//update new length
		memcpy((char *)read_buf - 4, &length, 4);

		length = length + write_location;
		if (length > (MAX_MINIFIED_JSON_SZ - 1)) {
			PRINTE("Minified json file size (%d) greater than limit %d\n",
				(int)flinfo.st_size, MAX_MINIFIED_JSON_SZ);
			fclose(fin);
	                fclose(fout);
        	        free(buf);
                	return 1;
		}

	}

	if(ret == 0){
		ret = fwrite((void *)buf, length, 1, fout);
		if(ret != 1){
			PRINTE("fwrite failed %d\n",ret);
			fclose(fin);
			fclose(fout);
			free(buf);
			return ret;
		}
		ret = 0;
	}

	fclose(fin);
	fclose(fout);
	free(buf);

	if(ret){//error
		PRINTE("ret = %d\n",ret);
		return ret;

	}
	//fill up the image
	if(type == BOOTLOADER)
		ret = truncate(rom_img, SNOR_CCR_START);
	else if (type == JSON)
		ret = truncate(rom_img, SNOR_JSON2_START);
    else if (type == JSON2)
        ret = truncate(rom_img, SNOR_GRID_MAP1_START);
	else if (type == ISP)
		ret = truncate(rom_img, SNOR_ISP2_START);
    else if (type == ISP2)
        ret = truncate(rom_img, SNOR_GRID_MAP2_START);
	else if (type == FW_IMAGE && alpha)
		ret = truncate(rom_img, SNOR_ALPHA_PHOTO);

	if(ret)
		PRINTE("truncate failed %d\n",ret);

	return ret;

}

int update_rom_with_maps(const char *rom_img, const char *args[], int map, image_type type)
{
	FILE *fout;
	unsigned int length = 0;
	unsigned char *buf = NULL;
	int ret = 0;


	if (map <= 0) { // no map args passed.
        if(type == MAP1)
            ret = truncate(rom_img, SNOR_ISP_START);
		else
            ret = truncate(rom_img, APP_IMG_START);

    	if(ret)
        	PRINTE("truncate failed %d\n",ret);

    	return ret;
	}

	fout = fopen(rom_img, "a+");
	if (fout == NULL) {
		PRINTE("Failed to open %s\n",rom_img);
		return 1;
	}

	if (MAP1 == type)
		ret = mxcam_ldmap_fill_map(args, &buf, &length);
	else
		ret = mxcam_ldmap_fill_map(args+4, &buf, &length);
	if (ret != 0) {
		PRINTE("Grid Map, unable to parse\n");
        fclose(fout);
        if(buf) free(buf);
		return 1;
	}

	if (length > MAX_GRID_MAP_SIZE) {
		PRINTE("Grid Map, calculated size too big (%d) max size %d\n",
			length, MAX_GRID_MAP_SIZE);
        fclose(fout);
        if(buf) free(buf);
		return 1;
	}

	ret = fwrite((void *)buf, length, 1, fout);
	if(ret != 1){
		PRINTE("fwrite failed %d\n",ret);
		fclose(fout);
		free(buf);
		return ret;
	}

	ret = 0;

	fclose(fout);
	free(buf);

	if(ret){//error
		PRINTE("ret = %d\n",ret);
		return ret;
	}

	if (MAP1 == type)
		ret = truncate(rom_img, SNOR_ISP_START);
	else
		ret = truncate(rom_img, APP_IMG_START);
		
	if(ret)
		PRINTE("truncate failed %d\n",ret);

	return ret;
}

int update_rom_with_compressed_alpha(const char *rom_img, const char *args[], int enable)
{
	FILE *fout[4];
	FILE *from;
	unsigned int size[4] = {0};
	unsigned char *buf[4] = {0};
	int ret = 0;
	int alpha_count=0;
	int total_size=0;
	int i;
	if (enable == 0 ) { // no alpha args passed.

		// donot truncate!
    	return ret;
	}

	from = fopen(rom_img, "a+");
	if (from == NULL) {
		PRINTE("Failed to open %s\n",rom_img);
		return 1;
	}

	total_size = 0;

	for(i=0; i<2; i++)
	{
		if(args[i])
		{
			fout[i] = fopen(args[i], "r");
			if(fout[i])
			{
				fseek(fout[i], 0, SEEK_END);
				size[i] = ftell(fout[i]);
				fseek(fout[i], 0, SEEK_SET);

				alpha_count++;

				buf[i]=(unsigned char*)malloc(size[i]);

				if(buf[i])
					fread(buf[i], 1, size[i], fout[i]);

				fclose(fout[i]);
			}
		}

		total_size += size[i]+PADDING;
	}

	if(alpha_count%2)
	{
		printf("ERR: Missing Alpha files \n");
		return 1;
	}

	printf("Total Map size %d and count %d\n", total_size, alpha_count);

	if(total_size >= (MAX_ALPHA_FILE_SIZE*2) - (alpha_count * PADDING + 16)){
        printf("ERR: Max supported Alpha Map file size %d Bytes\n", MAX_ALPHA_FILE_SIZE*2);
		return 1;
    }

    unsigned char *buffer = (unsigned char *)malloc(total_size);
    if(buffer == NULL){
			printf("ERR: out of memory\n");
			return 1;
    }

	unsigned char *mbuf = buffer;
	// 1st 5 bytes: "ALPHA" string
	strcpy((char *)mbuf, "ALPHA");
	mbuf += 5;
	// 2nd 4 bytes: Num of map count
	memcpy(mbuf, &alpha_count, sizeof(int));
	mbuf += sizeof(int);
	// 3rd 4 bytes: total size (including headers)
	memcpy(mbuf, &total_size, sizeof(total_size));
	mbuf += sizeof(total_size);
	printf("Alpha Y size is %d\n", size[0]);
	memcpy(mbuf, &size[0], sizeof(int));
	mbuf += sizeof(int);
	memcpy(mbuf, (char *)buf[0], size[0]);
	free(buf[0]);
	mbuf += size[0]+PADDING;

	memcpy(mbuf, &size[1], sizeof(int));
	mbuf += sizeof(int);
	memcpy(mbuf, (char *)buf[1], size[1]);
	free(buf[1]);

	ret = fwrite((void *)buffer, total_size, 1, from);
	if(ret != 1){
		PRINTE("fwrite failed %d\n",ret);
		fclose(from);
		return ret;
	}

	fclose(from);
	ret = truncate(rom_img, SNOR_ALPHA_VIDEO);
	ret = 0;

	free(buffer);
	alpha_count=0;
	total_size=0;

	from = fopen(rom_img, "a+");
	if (from == NULL) {
		PRINTE("Failed to open %s\n",rom_img);
		return 1;
	}

	for(i=2; i<4; i++)
	{
		if(args[i])
		{
			fout[i] = fopen(args[i], "r");
			if(fout[i])
			{
				fseek(fout[i], 0, SEEK_END);
				size[i] = ftell(fout[i]);
				fseek(fout[i], 0, SEEK_SET);

				alpha_count++;

				buf[i]=(unsigned char*)malloc(size[i]);

				if(buf[i])
					fread(buf[i], 1, size[i], fout[i]);

				fclose(fout[i]);
			}
		}

		total_size += size[i]+PADDING;
	}

	if(alpha_count%2)
	{
		printf("ERR: Missing Alpha files \n");
		return 1;
	}

	printf("Total Map size %d and count %d\n", total_size, alpha_count);

	if(total_size >= (MAX_ALPHA_FILE_SIZE*2) - (alpha_count * PADDING + 16)){
        printf("ERR: Max supported Alpha Map file size %d Bytes\n", MAX_ALPHA_FILE_SIZE*2);
		return 1;
    }

    buffer = (unsigned char *)malloc(total_size);
    if(buffer == NULL){
		printf("ERR: out of memory\n");
		return 1;
    }

	mbuf = buffer;
	// 1st 5 bytes: "MAP" string
	strcpy((char *)mbuf, "ALPHA");
	mbuf += 5;
	// 2nd 4 bytes: Num of map count
	memcpy(mbuf, &alpha_count, sizeof(int));
	mbuf += sizeof(int);
	// 3rd 4 bytes: total size (including headers)
	memcpy(mbuf, &total_size, sizeof(total_size));
	mbuf += sizeof(total_size);
	printf("Alpha Y size is %d\n", size[2]);
	memcpy(mbuf, &size[2], sizeof(int));
	mbuf += sizeof(int);
	memcpy(mbuf, (char *)buf[2], size[2]);
	free(buf[2]);
	mbuf += size[2]+32;

	memcpy(mbuf, &size[3], sizeof(int));
	mbuf += sizeof(int);
	memcpy(mbuf, (char *)buf[3], size[3]);
	free(buf[3]);

	ret = fwrite((void *)buffer, total_size, 1, from);
	if(ret != 1){
		PRINTE("fwrite failed %d\n",ret);
		fclose(from);
		return ret;
	}

	ret = 0;
	free(buffer);

	fclose(from);

	if(ret){//error
		PRINTE("ret = %d\n",ret);
		return ret;
	}

	// ret = truncate(rom_img, APP_IMG_START);
	ret = truncate(rom_img, APP_IMG_END);
	if(ret)
		PRINTE("truncate failed %d\n",ret);

	return ret;
}

int main(int argc, char *const*argv)
{
	int longidx = 0;
	int opt, r;
	char *bootloader = NULL, *json = NULL, *isp = NULL,*fw = NULL, *json2=NULL;
    char *isp2 = NULL;
	char *rom_file = NULL;
	optind = 0;
	char rom[255*3];
	int length = 0;
	int map = 0;

	while ((opt =
		getopt_long(argc, argv, "b:j:i:fw:h:v:rom:m1:m2:m3:o1:o2:o3:a1:a2:a3:a4", longopts, &longidx)) != -1) {
		switch (opt) {
		case 'b':
			bootloader = optarg;
			break;
		case 'j':
            printf("longopts[longidx].name %s\n",longopts[longidx].name);
            if(strncmp("json2",longopts[longidx].name, sizeof("json2")) == 0){
				json2 = optarg;
                printf("json2 %s\n",json2);
            }else if(strncmp("json", longopts[longidx].name, sizeof("json")) == 0)
                json = optarg;
            printf("json %s\n",json);

			break;
		case 'i':
            if(strncmp("isp2",longopts[longidx].name, sizeof("isp2")) == 0){
				isp2 = optarg;
                printf("isp2 %s\n",isp2);
            }else if(strncmp("isp", longopts[longidx].name, sizeof("isp")) == 0)
                isp = optarg;
			//isp = optarg;
			break;
		case 'f':
			fw = optarg;
			printf("fw arguments %s\n", optarg);
			break;
		case 'h':
			usage();
			exit(1);
			break;
		case 'v':
			verbose = 1;
			break;
		case 'r':
			rom_file = optarg;
			break;
		case 'm':
			if(strcmp(longopts[longidx].name, "map0") == 0) {
				map_args[0] = optarg;
				map++;
			}
			if(strcmp(longopts[longidx].name, "map1") == 0) {
				map_args[2] = optarg;
				map++;
			}
			if(strcmp(longopts[longidx].name, "map2") == 0) {
				map_args[4] = optarg;
				map++;
			}
			if(strcmp(longopts[longidx].name, "map3") == 0) {
				map_args[6] = optarg;
				map++;
			}
			break;	
		case 'o':
			if(strcmp(longopts[longidx].name, "obj0") == 0)
				map_args[1] = optarg;
			if(strcmp(longopts[longidx].name, "obj1") == 0)
				map_args[3] = optarg;
			if(strcmp(longopts[longidx].name, "obj2") == 0)
				map_args[5] = optarg;
			if(strcmp(longopts[longidx].name, "obj3") == 0)
				map_args[7] = optarg;
			break;
		case 'a':
			if(strcmp(longopts[longidx].name, "a1") == 0)
				alpha_args[0] = optarg;
			if(strcmp(longopts[longidx].name, "a2") == 0)
				alpha_args[1] = optarg;
			if(strcmp(longopts[longidx].name, "a3") == 0)
				alpha_args[2] = optarg;
			if(strcmp(longopts[longidx].name, "a4") == 0)
				alpha_args[3] = optarg;
			alpha = 1;
			break;
		default:
			usage();
			exit(0);
			break;
		}

	}

	if((bootloader == NULL)	|| (json == NULL) || (isp == NULL) || (fw == NULL))
	{
		usage();
		return 0;
	}
	if(verbose){
		printf("------------------------------------\n");
		printf_bold("Bootloader : ");printf("%s\n",bootloader);
		printf_bold("Json file  : ");printf("%s\n",json);
		printf_bold("Ispcfg file: ");printf("%s\n",isp);
		printf_bold("Firmware   : ");printf("%s\n",fw);
        if(json2){
            printf_bold("Json2 file : ");printf("%s\n",json2);
        }
        if(isp2){
            printf_bold("Ispcfg2 file : ");printf("%s\n",isp2);
        }
	}
	if(isfileexist(bootloader)){
		PRINTE("Invalid bootloader file provided\n");
		return 1;
	}
	if(isfileexist(json)){
		PRINTE("Invalid json file provided\n");
		return 1;
	}
	if(isfileexist(isp)){
		PRINTE("Invalid ispcfg file provided\n");
		return 1;
	}
	if(isfileexist(fw)){
		PRINTE("Invalid firmware file provided\n");
		return 1;
	}
    if(json2){
        if(isfileexist(json2)){
            PRINTE("Invalid json2 file provided\n");
		    return 1; 
        }
    }
    if(isp2){
        if(isfileexist(isp2)){
            PRINTE("Invalid isp2 file provided\n");
		    return 1; 
        }
    }

	if(map){
		if(isfileexist(map_args[0]) || (map_args[1] == NULL)){
			PRINTE("Invalid map0:obj0 %s:%s file provided\n", map_args[0], map_args[1]);
			return 1;
		}

		if (map_args[2] != NULL)
		if(isfileexist(map_args[2]) || (map_args[3] == NULL)){
			PRINTE("Invalid map1:obj1 %s:%s file provided\n", map_args[2], map_args[3]);
			return 1;
		}

		if (map_args[4] != NULL)
		if(isfileexist(map_args[4]) || (map_args[5]) == NULL){
			PRINTE("Invalid map2:obj2 %s:%s file provided\n", map_args[4], map_args[5]);
			return 1;
		}

		if (map_args[6] != NULL)
		if(isfileexist(map_args[6]) || (map_args[7]) == NULL){
			PRINTE("Invalid map3:obj3 %s:%s file provided\n", map_args[6], map_args[7]);
			return 1;
		}

	} 
	
	if(alpha){
		if(isfileexist(alpha_args[0])){
			PRINTE("Invalid alpha y (photo) %s file provided\n", alpha_args[0]);
			return 1;
		}

		if(isfileexist(alpha_args[1])){
			PRINTE("Invalid alpha uv (photo) %s file provided\n", alpha_args[1]);
			return 1;
		}

		if(isfileexist(alpha_args[2])){
			PRINTE("Invalid alpha y (video) %s file provided\n", alpha_args[2]);
			return 1;
		}

		if(isfileexist(alpha_args[3])){
			PRINTE("Invalid alpha uv (video) %s file provided\n", alpha_args[3]);
			return 1;
		}
	}

	if(rom_file == NULL){
		char bootloader_file[255],json_file[255],isp_file[255],fw_file[255];
		rom_file = rom;
		if(recover_filename(bootloader, bootloader_file) == 1)
			return 1;

		if(recover_filename(json, json_file) == 1)
			return 1;

		if(recover_filename(isp, isp_file) == 1)
			return 1;

		if(recover_filename(fw, fw_file) == 1)
			return 1;

		sprintf(rom_file, "%s-%s-%s.rom", bootloader_file,json_file,fw_file);
		length =  strlen(bootloader_file)+strlen(json_file)+
				strlen(fw_file)+strlen(".rom")+2*strlen("-");;

		rom_file[length] = 0;//add null
	}

	//write bootloader
	r = update_rom(rom_file, bootloader, BOOTLOADER);
	if(r){
		PRINTE("update_rom failed for bootloader\n");
		return r;
	}
	//write json
	r = update_rom(rom_file, json, JSON);
	if(r){
		PRINTE("update_rom failed for json\n");
		return r;
	}
    //if second json file is there write it
    if(json2){
        r = update_rom(rom_file, json2, JSON2);
	    if(r){
		    PRINTE("update_rom failed for json\n");
		    return r;
	    }
    }else{
        r = update_rom(rom_file, json, JSON2);
	    if(r){
		    PRINTE("update_rom failed for json\n");
		    return r;
	    }
    }
	//write maps 1
	r = update_rom_with_maps(rom_file, map_args, map, MAP1);
	if(r){
		PRINTE("update_rom failed for grid maps 1\n");
		return r;
	}

	//write ispcfg
	r = update_rom(rom_file, isp, ISP);
	if(r){
		PRINTE("update_rom failed for ispcfg\n");
		return r;
	}
    if(isp2){
        r = update_rom(rom_file, isp2, ISP2);
	    if(r){
		    PRINTE("update_rom failed for json\n");
		    return r;
	    }
    }else{
        r = update_rom(rom_file, isp, ISP2);
	    if(r){
		    PRINTE("update_rom failed for json\n");
		    return r;
	    }
    }

	map -= 2;
	//write maps 2
	r = update_rom_with_maps(rom_file, map_args, map, MAP2);
	if(r){
		PRINTE("update_rom failed for grid maps 2\n");
		return r;
	}

	//write fw image
	r = update_rom(rom_file, fw, FW_IMAGE);
	if(r){
		PRINTE("update_rom failed for fw image\n");
		return r;
	}

	//write compressed alpha bitmaps
	r = update_rom_with_compressed_alpha(rom_file, alpha_args, alpha);
	if(r){
		PRINTE("update_rom failed for alpha y\n");
		return r;
	}

	printf("------------------------------------\n");
	printf_bold("rom file : ");
	printf("%s\n",rom_file);
	return 0;
}
