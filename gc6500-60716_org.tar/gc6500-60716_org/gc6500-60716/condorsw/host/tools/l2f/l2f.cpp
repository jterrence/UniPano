/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

/*******************************************************************************
* @(#) $Header: http://svllsvn01.local.geosemi.com/repos/swrepos/trunk/condorsw/host/tools/l2f/l2f.cpp 57866 2016-04-28 11:12:50Z bsmith $
*******************************************************************************/

/*******************************************************************************
* The purpose of this chunk of code is to have the RCS/CVS/SVN keyword string
* embedded in an object file.
*
* Requirements:
* . The char array containing the keywords must be used so as to avoid compiler
*   warning.  A bogus function is created to use the said char array.
*******************************************************************************/
#define CC_IDENT_NAMESPACE RCS_IDENT_L2F_CPP
#ifdef CC_IDENT_NAMESPACE
#define KW_JOIN(x,y) x ## _ ## y
#define KW_XJOIN(x,y) KW_JOIN(x,y)
#define CC_IDENT_FUNC_NAME KW_XJOIN(CC_IDENT_NAMESPACE, FUNC)
const char* CC_IDENT_FUNC_NAME()
{
    static char rcs_id[] = "$Header: http://svllsvn01.local.geosemi.com/repos/swrepos/trunk/condorsw/host/tools/l2f/l2f.cpp 57866 2016-04-28 11:12:50Z bsmith $" ;
    return rcs_id ;
}
#undef CC_IDENT_FUNC_NAME
#undef KW_XJOIN
#undef KW_JOIN
#endif // CC_IDENT_NAMESPACE


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <qhal_em.h>
#include <qhal_qcc.h>
#ifdef CHIP_REGS_DEFINED_LOCAL
#define   QCC_BID_PMU0           0x21
#define   QCC_BID_PMC0           0x22

#define	Reg_M8_MEMS_MEM0_PMU_x_VPATSegSize_ADDRESS	0x08
#define	Reg_QA_PMCDDR_base_Mode4_ADDRESS	0x14
#define	Reg_QA_PMCDDR_base_TileSize_ADDRESS	0x15
#define	Reg_QA_PMCDDR_base_PageBankSize_ADDRESS	0x16
#else
#include "m8_reg.h"
#endif

static int frame2linearInit(int width);
static int frame2linear(int xaddr, int yaddr);
void PrintUsage (char *name);

static int frameAddrInit = 0;

// constants for seve1
static int  VPAT_SXSW =  9;         // Segment     Horizontal Size    <=  512 bytes
static int  VPAT_SYSW =  6;         // Segment     Vertical   Size    <=   64 lines

// values read from memcontroller registers - defaults are for condor
static int VPATSegYSize = 2;
static int VPATSegXSize = 2;
static int WordSize = 3;
static int TileXSize = 3; // in words
static int TileYSize = 4;
static int BankXSize = 2;
static int BankYSize = 1;
static int PageSize = 1;

static int bytes_per_word;
static int byte_offset_mask;
static int SegYSize;
static int SegXSize;

static int SegYSize_rows;
static int SegXSize_bytes;
static int seg_size;
static int tile_hsize;
static int tile_vsize;
static int tile_size;
static int num_tileh;
static int num_tilev;
static int tile_row_size;
static int tileh_word_mask;
static int tileh_sel_shift;
static int tileh_sel_mask;
static int segh_sel_shift;
static int tilev_row_mask;
static int tilev_sel_shift;
static int tilev_sel_mask;
static int segv_sel_shift;

static int tile_word_mask;
static int bank_mask;
static int bank_shift;
static int seg_shift;
static int seg_row_size;
static int frame_hsize;

int main(int argc, char** argv)
{
    int width , widthc;
    int height, heightc;
    int size, sizec;
    int part;
    int y, x, addr;
    int chromaType=420;
    char outputfile[500], inputfile[500], filename[500];
    int arg=6;

    FILE *fptr_in, *fptr_out;

    unsigned char *yBuffer = NULL, *uBuffer = NULL, *vBuffer = NULL;
    char *inputBuffer;
    qhalem_handle_t hem;

    
    if(argc >= arg && argc <= arg+1) {
        sscanf(argv[1],"%s", outputfile);
        sscanf(argv[2],"%d", &width);
        sscanf(argv[3],"%d", &height);
        sscanf(argv[4],"%d", &part);
        sscanf(argv[5],"%d", &chromaType);
    }
    else
    {
	PrintUsage (argv[0]);
	exit(1);
    }
    if (  chromaType != 420
       && chromaType != 422
       && chromaType != 0
       ) {
        fprintf( stderr , "%s: chroma type %d is not supported.\n" , argv[0] , chromaType ) ;
	PrintUsage (argv[0]);
	exit(1);
    }
    
    if(argc==(arg+1))
    {
        sscanf(argv[6],"%s", inputfile);
    }

    size    =  width * height;
    heightc =  (chromaType==420) ? height/2 : height;
    widthc  =  width/2;
    sizec   =  heightc*widthc;
    
    // xxx
    frame_hsize = width;
    
    frame2linearInit(width);



    // Read from partition 
    int buffersize = frame2linear(width-1,height-1)+1;
    buffersize = (buffersize + 3) & 0xfffffffc;
    printf("Buffersize %d\n",buffersize);

    if ( (inputBuffer   = (char *)malloc(buffersize)) == NULL ){     
        fprintf(stderr,"Could not allocate memory for inputBuffer\n");
        exit (1);
    }
    if ( (yBuffer  	= (unsigned char *)malloc(size)) == NULL ){
        fprintf(stderr,"Could not allocate memory for yBuffer\n");
        free(inputBuffer);
        exit (1);
    }
    memset(yBuffer, 0, size);

    if ( (uBuffer  	= (unsigned char *)malloc(sizec)) == NULL ){ 
        fprintf(stderr,"Could not allocate memory for uBuffer\n");
        free(inputBuffer);
        free(yBuffer);
        exit (1);
    }
    memset(uBuffer, 0, sizec);

    if ( (vBuffer  	= (unsigned char *)malloc(sizec)) == NULL ){
        fprintf(stderr,"Could not allocate memory for vBuffer\n");
        free(inputBuffer);
        free(yBuffer);
        free(uBuffer);
        exit (1);
    }
    memset(vBuffer, 0, sizec);

    if (!(fptr_out = fopen (outputfile, "w")))
    {
       fprintf (stderr, "Error opening file %s\n", outputfile);
       free(inputBuffer);
       free(yBuffer);
       free(uBuffer);
       free(vBuffer);
 
       exit (1);
    }
 
    if(argc==arg+1)
    {
	sprintf(filename,"%s.y",inputfile);
	if (!(fptr_in = fopen(filename, "r")))
	{
	   fprintf (stderr, "Error opening file %s\n", filename);
           free(inputBuffer);
           free(yBuffer);
           free(uBuffer);
           free(vBuffer);
           fclose(fptr_out);

	   exit (1);
        }
	
	fseek(fptr_in,0,SEEK_END);
	buffersize = ftell(fptr_in);
	fseek(fptr_in,0,SEEK_SET);
			       
	addr=fread(inputBuffer,1,buffersize,fptr_in);
	if(addr!=buffersize)
	{
	    fprintf(stderr,"Not enough bytes in input file Bytes to be read=%d Bytes read=%d",buffersize,addr);
            free(inputBuffer);
            free(yBuffer);
            free(uBuffer);
            free(vBuffer);
            
            fclose(fptr_out);
            fclose(fptr_in);

	    exit(1);
	}
	fclose(fptr_in);
    }
    else
    {	
	hem = qhalem_open(QHALEM_ACCESSTYPE_STREAM, QHALEM_MODE_LINEAR);
	qhalem_read_bytes(hem, part, 0, inputBuffer, buffersize);            
    }
    
    // Luma component
    for(y=0; y < height; y++)
    {
	for(x=0; x < width; x++)
	{
	    yBuffer[y*width+x] = inputBuffer[frame2linear(x,y)];
	}
    }

    //Writing luma
    addr=fwrite(yBuffer,1,size,fptr_out);
    if(addr!=size)
    {
    	fprintf(stderr,"Failed to write luma component to output file\n");
        free(inputBuffer);
        free(yBuffer);
        free(uBuffer);
        free(vBuffer);
        fclose(fptr_out);
	exit(1);
    }
   
    if(chromaType == 420 || chromaType == 422)
    {
	if(argc==arg+1)
	{
	    sprintf(filename,"%s.uv",inputfile);
	    if (!(fptr_in = fopen (filename, "r")))
	    {
	       fprintf (stderr, "Error opening file %s\n", filename);
	       exit (1);
	    }

	    fseek(fptr_in,0,SEEK_END);
	    buffersize = ftell(fptr_in);
	    fseek(fptr_in,0,SEEK_SET);

	    addr=fread(inputBuffer,1,buffersize,fptr_in);
	    if(addr!=buffersize)
	    {
		fprintf(stderr,"Not enough bytes in input file Bytes to be read=%d Bytes read=%d",buffersize,addr);
		exit(1);
	    }
	}
	else
	{
	    // Read from partition + 1
	    // Chroma component
	    qhalem_read_bytes(hem, part+1, 0, inputBuffer, buffersize);            
	    
	}
	
	for ( y = 0; y < heightc; y++)
	{
	    for ( x = 0; x < width; x+=4)
	    {
		 addr = frame2linear(x, y);
		 uBuffer[y*(widthc)+(x/2)]     = inputBuffer[addr];
		 uBuffer[y*(widthc)+(x/2) + 1] = inputBuffer[addr+2];
		 vBuffer[y*(widthc)+(x/2)]     = inputBuffer[addr+1];
		 vBuffer[y*(widthc)+(x/2) + 1] = inputBuffer[addr+3];
	    }
	}

	
	//Writing U
	addr=fwrite(uBuffer,1,sizec,fptr_out);
	if(addr!=sizec)
	{
	    fprintf(stderr,"Failed to write U component to output file\n");
            free(inputBuffer);
            free(yBuffer);
            free(uBuffer);
            free(vBuffer);
            fclose(fptr_out);
	    exit(1);
	}

	// Writing V
	addr=fwrite(vBuffer,1,sizec,fptr_out);
	if(addr!=sizec)
	{
	    fprintf(stderr,"Failed to write V component to output file\n");
	    exit(1);
	}
    }
    fclose(fptr_out);
   exit(0);
}


static int DivRoundUp(int value, int divisor)
{
    return ((value + divisor - 1)/divisor);
}

int frame2linearInit(int width = 720)
{
    
    unsigned long data;
    qhalqcc_handle_t hqcc;
    
    hqcc = qhalqcc_open();
    if(hqcc < 0) {
        fprintf(stderr,"Error in qhalqcc_open() - cannot connect to camera\n");
        fprintf(stderr,"Using default values\n");
    }
    else {
        // read csr registers    
        qhalqcc_read(hqcc, QCC_BID_PMU0 , Reg_M8_MEMS_MEM0_PMU_x_VPATSegSize_ADDRESS , &data, 2);
        VPATSegXSize = data & 0x3;    
        VPATSegYSize = (data & 0xc) >> 2;
    
        qhalqcc_read(hqcc, QCC_BID_PMC0 , Reg_QA_PMCDDR_base_Mode4_ADDRESS , &data, 1);
        WordSize = data & 0x7;
    
        qhalqcc_read(hqcc, QCC_BID_PMC0 , Reg_QA_PMCDDR_base_TileSize_ADDRESS , &data, 1);    
        TileXSize = (data & 0x70) >> 4;
        TileYSize = (data & 0x7);

        qhalqcc_read(hqcc, QCC_BID_PMC0 , Reg_QA_PMCDDR_base_PageBankSize_ADDRESS , &data, 1);    
        BankXSize = (data & 0xc) >> 2;
        BankYSize = (data & 0x3);
        PageSize = (data & 0x70) >> 4;
    
        qhalqcc_close(hqcc);
    }
    
    frame_hsize = width; // default 720
    
    // calculate various constants
    bytes_per_word   = (1 << WordSize);
    byte_offset_mask = (bytes_per_word - 1);

    SegYSize = VPAT_SYSW - 3 + VPATSegYSize;
    SegXSize = VPAT_SXSW - 3 + VPATSegXSize;

    // calculations for SwizzleFrameAddr
    SegXSize_bytes = 1 << SegXSize;
    SegYSize_rows  = 1 << SegYSize;

    tile_hsize = 1 << (TileXSize + WordSize);
    tile_vsize = 1 <<  TileYSize;
    tile_size  = tile_hsize * tile_vsize;

    num_tileh  = 1 <<  BankXSize;
    num_tilev  = 1 <<  BankYSize;

    tile_row_size = tile_size * num_tileh;

    // this is same as tile_hsize * tile_vsize * num_tileh * num_tilev;
    seg_size   = SegXSize_bytes * SegYSize_rows;

    //
    // Mask & Shift values
    //

    // For Linear Accesses
    tile_word_mask  = tile_size  - 1 ;
    bank_mask       = (1 << (BankXSize + BankYSize)) - 1;
    bank_shift      = TileYSize + TileXSize + WordSize;
    seg_shift       = bank_shift + BankYSize + BankXSize;

    // For Block Accesses
    tileh_word_mask = tile_hsize - 1 ;

    tileh_sel_shift = TileXSize + WordSize ;
    tileh_sel_mask  = num_tileh  - 1 ;

    segh_sel_shift  = BankXSize + TileXSize + WordSize ;

    tilev_row_mask  = tile_vsize - 1 ;

    tilev_sel_shift = TileYSize   ;
    tilev_sel_mask  = num_tilev  - 1 ;

    segv_sel_shift  = BankYSize + TileYSize ;

    // calculate row size
    seg_row_size = DivRoundUp(frame_hsize, SegXSize_bytes) * seg_size;

    // do only once
    frameAddrInit = 1;


printf( "VPATSegXSize %d VPATSegYSize %d\n", VPATSegXSize, VPATSegYSize );
printf( "WordSize %d\n", WordSize );
printf( "TileXSize %d TileYSize %d\n", TileXSize, TileYSize );
printf( "BankXSize %d BankYSize %d PageSize %d\n", BankXSize, BankYSize, PageSize );
printf( "SegXSize_bytes %d SegYSize_rows %d\n", SegXSize_bytes, SegYSize_rows );
printf( "tile_hsize %d tile_vsize %d\n", tile_hsize, tile_vsize );
printf( "num_tileh %d num_tilev %d\n", num_tileh, num_tilev );
printf( "tile_row_size %d seg_size %d\n", tile_row_size, seg_size );

    return 0;
}

int frame2linear(int xaddr, int yaddr)
{
    int Frame2LinearAddr;
    
    // calc addr
    Frame2LinearAddr =
        ( (   yaddr >> segv_sel_shift                     ) * seg_row_size  ) +
        ( ( ( yaddr >> tilev_sel_shift ) & tilev_sel_mask ) * tile_row_size ) +
        ( (   yaddr                      & tilev_row_mask ) * tile_hsize    ) +

        ( (   xaddr >> segh_sel_shift                     ) * seg_size      ) +
        ( ( ( xaddr >> tileh_sel_shift ) & tileh_sel_mask ) * tile_size     ) +
        (     xaddr                      & tileh_word_mask                   ) ;
    //printf("%04x %04x -> %08x\n",  xaddr, yaddr, Frame2LinearAddr);

    return (Frame2LinearAddr);
    
}

void
PrintUsage (char *name)
{
  fprintf (stderr, "\
Usage: %s <output file> <width> <height> <paritition number> <chroma type> [input file basename]\n\
\n\
Options\n\
  partition number:     Partition number of the luma component(Should be in the odd partition). Chroma is assumed to be in the odd partition\n\
\n\
  chroma type:      \n\
                        0:   No chroma processing.\n\
                        420: Process in 4:2:0 chroma format\n\
                        422: Process in 4:2:2 chroma format\n\
\n\
  input file basename:  The files basename.y and basename.uv, instead of the memory partition, will be read\n\
\n\
Examples:\n\
    %s ramp.yuv 720 486 0 420 temp  Will read the luma data from temp.y and chroma from temp.uv from the current directory\n\
    %s ramp.yuv 720 486 0 420       Will read luma and 420 chroma from memory\n\
    %s ramp.yuv 720 486 0 0         Will read luma and no  chroma from memory\n\
\n" , name , name , name , name ) ;
}

/*******************************************************************************
* vi: set shiftwidth=4 tabstop=8 softtabstop=4 expandtab nosmarttab:
*******************************************************************************/
