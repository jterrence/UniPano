/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

/*******************************************************************************
* @(#) $Header: http://svllsvn01.local.geosemi.com/repos/swrepos/trunk/condorsw/host/tools/f2l/f2l.c 57866 2016-04-28 11:12:50Z bsmith $
*******************************************************************************/

/*******************************************************************************
* The purpose of this chunk of code is to have the RCS/CVS/SVN keyword string
* embedded in an object file.
*
* Requirements:
* . The char array containing the keywords must be used so as to avoid compiler
*   warning.  A bogus function is created to use the said char array.
*******************************************************************************/
#define CC_IDENT_NAMESPACE RCS_IDENT_F2L_C
#ifdef CC_IDENT_NAMESPACE
#define KW_JOIN(x,y) x ## _ ## y
#define KW_XJOIN(x,y) KW_JOIN(x,y)
#define CC_IDENT_FUNC_NAME KW_XJOIN(CC_IDENT_NAMESPACE, FUNC)
const char* CC_IDENT_FUNC_NAME()
{
    static char rcs_id[] = "$Header: http://svllsvn01.local.geosemi.com/repos/swrepos/trunk/condorsw/host/tools/f2l/f2l.c 57866 2016-04-28 11:12:50Z bsmith $" ;
    return rcs_id ;
}
#undef CC_IDENT_FUNC_NAME
#undef KW_XJOIN
#undef KW_JOIN
#endif // CC_IDENT_NAMESPACE

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "yuvutil.h"

#define LINEAR_FRAME_SIZE (8192*8192)
#define MAX_PATH 1024

int WriteVideoFrame(FILE *fpi, int width, int height, int bufferwidth, FILE *fpoy, FILE *fpouv, int YUVformat , int startframe)
{
    int x, y;
    unsigned char * fbuffer;
    unsigned char * bufferY;
    unsigned char * bufferU;
    unsigned char * bufferV;
    unsigned char * linebuffer;
    int maxaddr=0;
    int bytesread=0;
    int chroma_width=0;
    int chroma_height=0;
    int chroma_bufferwidth;
    unsigned char *yptr, *uptr, *vptr, *lineptr;
    int size,sizec, framesize;
    long long offset;
	int planar=1;

    printf("width %d height %d bufferwidth %d yuvformat %d ", width, height, bufferwidth, YUVformat); // print the format in the case statement

    // malloc frame buffer
    fbuffer    = (unsigned char *)malloc(LINEAR_FRAME_SIZE);
    bufferY    = (unsigned char *)malloc(LINEAR_FRAME_SIZE);
    bufferU    = (unsigned char *)malloc(LINEAR_FRAME_SIZE); // up to 444 size
    bufferV    = (unsigned char *)malloc(LINEAR_FRAME_SIZE);
    linebuffer = (unsigned char *)malloc(2*width);
    
    size  = width * height;
    chroma_bufferwidth = bufferwidth;

    switch (YUVformat)
    {
    	case 0:  // YUV420 Planar (MPEG)
		printf("(YUV420 Planar)\n");
		chroma_width = width/2;
		chroma_height = height/2;
                
		break;
    	case 1:  // YUV422 Interleaved (ITU656)
		planar = 0;
		printf("(YUV422 Interleaved)\n");
		chroma_width = width/2;
		chroma_height = height;

		break;
    	case 2:  // YUV422 Planar
		printf("(YUV422 Planar)\n");
		chroma_width = width/2;
		chroma_height = height;
                
		break;
    	case 3:  // YUV411 Planar 
		printf("(YUV411 Planar)\n");
		chroma_width = width/4;
		chroma_height = height;
		
		chroma_bufferwidth /= 2; // Achtung! Gefahr, Wille Robinson!

		break;
    	case 4:  // YUV444 Planar (Graphics)
		printf("(YUV444 Planar)\n");
		chroma_width = width;
		chroma_height = height;

		chroma_bufferwidth *= 2; // Achtung! Gefahr, Wille Robinson!
                
		break;
    	default: // YUV420 Planar (MPEG)
		printf("(YUV420 Planar)\n");
		chroma_width = width/2;
		chroma_height = height/2;
		break;
    }

    if (planar)
    {

		sizec = chroma_width * chroma_height;
		framesize =  size + 2*sizec;
		offset = (long long)startframe*(long long)framesize;
		if(fseeko(fpi, offset, 0)) 
		{
		    printf("Fseek failed.\n");
		    free(fbuffer);
    		    free(bufferY);
    		    free(bufferU);
    		    free(bufferV);
		    free(linebuffer);
		    return(1);
		}
		
    		bytesread = fread(bufferY, 1, width*height, fpi);
    		if( bytesread != 0 ) bytesread = fread(bufferU, 1, chroma_width*chroma_height, fpi);
    		if( bytesread != 0 ) bytesread = fread(bufferV, 1, chroma_width*chroma_height, fpi);
    }
    else // interleaved
    {
		sizec = chroma_width * chroma_height;
		framesize =  size + 2*sizec;
		offset = (long long)startframe*(long long)framesize;
		if(fseeko(fpi, offset, 0)) 
		{
		    printf("Fseek failed.\n");
		    free(fbuffer);
    		    free(bufferY);
    		    free(bufferU);
    		    free(bufferV);
		    free(linebuffer);
		    return(1);
		}

		yptr    = bufferY;
		uptr    = bufferU;
		vptr    = bufferV;

		for (y=0; y < height; y++)
		{
    			bytesread = fread(linebuffer, 1, 2*width, fpi);
			lineptr = linebuffer;
			for (x=0; x < width/2; x++)
			{
				*uptr++ = *lineptr++;
				*yptr++ = *lineptr++;
				*vptr++ = *lineptr++;
				*yptr++ = *lineptr++;
			}
		}
    }

    // check for eof
    if (bytesread == 0)
    {
        // free the memory
        free(fbuffer);
        free(bufferY);
        free(bufferU);
        free(bufferV);
        free(linebuffer);
        return(1);
    } 
    // To deal with non multiple of 16 heights. Fake it to think its a multiple of 16. You dont care abt the extra lines that is put
    if(height%16) {
    	height = ((height+15) >> 4) << 4;
    }

    Frame2LinearInit();

    maxaddr = f2l( bufferY, width, height, fbuffer, bufferwidth);
    fprintf(stderr, "maxadddr y %d\n", maxaddr);
    fwrite(fbuffer, 1, maxaddr, fpoy);

    // To deal with non multiple of 8 heights. Fake it to think its a multiple of 8. You dont care abt the extra lines that is put
    if(chroma_height%8) {
    	chroma_height = ((chroma_height+7) >> 3) << 3;
    }

    maxaddr = f2lUV( bufferU, bufferV, chroma_width, chroma_height, fbuffer, chroma_bufferwidth );
    fprintf(stderr, "maxadddr uv %d\n", maxaddr);
    fwrite(fbuffer, 1, maxaddr, fpouv);
 
    // free the buffer
    free(fbuffer);
    free(bufferY);
    free(bufferU);
    free(bufferV);
    free(linebuffer);

    return(1);
}

int main(int argc, char** argv)
{
    int width;
    int height;
    int bufferwidth;
    int YUVformat=0;
    int startframe=0;
    char outfiley[MAX_PATH];
    char outfileuv[MAX_PATH];
    FILE *ifile, *yfile, *uvfile;

    if(argc == 5) 
    {
        strncpy(outfiley, argv[2], MAX_PATH-1);
        strncpy(outfileuv, argv[2], MAX_PATH-1);
	strcat(outfiley, ".y");
	strcat(outfileuv, ".uv");
        width = atoi(argv[3]);
        height = atoi(argv[4]);
	bufferwidth = width;
    }
    else if(argc == 6) 
    {
        strncpy(outfiley, argv[2], MAX_PATH-1);
        strncpy(outfileuv, argv[2], MAX_PATH-1);
	strcat(outfiley, ".y");
	strcat(outfileuv, ".uv");
        width = atoi(argv[3]);
        height = atoi(argv[4]);
	bufferwidth = atoi(argv[5]);
    }
    else if(argc == 7) 
    {
        strncpy(outfiley, argv[2], MAX_PATH-1);
        strncpy(outfileuv, argv[2], MAX_PATH-1);
	strcat(outfiley, ".y");
	strcat(outfileuv, ".uv");
        width = atoi(argv[3]);
        height = atoi(argv[4]);
	bufferwidth = atoi(argv[5]);
	YUVformat = atoi(argv[6]);
    }
    else if(argc == 8) 
    {
        strncpy(outfiley, argv[2], MAX_PATH-1);
        strncpy(outfileuv, argv[2], MAX_PATH-1);
	strcat(outfiley, ".y");
	strcat(outfileuv, ".uv");
        width = atoi(argv[3]);
        height = atoi(argv[4]);
	bufferwidth = atoi(argv[5]);
	YUVformat = atoi(argv[6]);
	startframe = atoi(argv[7]);
    }else
    {
        fprintf(stderr, "Usage  : f2l <infile> <outfile base> <width> <height> [<bufferwidth> [YUVformat] [start frame]]\n");
        fprintf(stderr, "\n");
        fprintf(stderr, "         YUVformat: 0=YUV420(planar), 1=YUV422(interleaved), 2=YUV422(planar) 3=YUV411(planar) 4=YUV444(planar)\n");
        fprintf(stderr, "                    If this option is used, <bufferwidth> must be present\n");
        fprintf(stderr, "                    Typically <bufferwidth> is equal to <width>\n");
        fprintf(stderr, "\n");
        fprintf(stderr, "         start frame: Number of frames you want to skip from beginning\n");
        fprintf(stderr, "                    If this option is used, <bufferwidth> and yuv format must be present\n");
        fprintf(stderr, "                    Typically <bufferwidth> is equal to <width>\n");
        fprintf(stderr, "Example: f2l Dancer420.yuv tiled.yuv 720 480\n");
        fprintf(stderr, "Example: f2l Dancer422.yuv tiled.yuv 720 480 720 1\n");
        fprintf(stderr, "Example: f2l Dancer422.yuv tiled.yuv 720 480 720 1 10 (skip first 10 frames from the input file)\n");
	exit(0);
    }

    ifile = fopen(argv[1], "r");
    if (ifile == NULL)
    {
        fprintf(stderr, "Could not open %s for reading\n", argv[1]);
    }

    yfile = fopen(outfiley, "w");
    if (yfile == NULL)
    {
        fprintf(stderr, "Could not open %s for writing\n", outfiley);
	exit(0);
    }

    uvfile = fopen(outfileuv, "w");
    if (uvfile == NULL)
    {
        fprintf(stderr, "Could not open %s for writing\n", outfileuv);
	exit(0);
    }
    
    if ( width*height > 8192*8192)
    {
    	fprintf(stderr, "Maximum picture size supported is 8192x8192\n");
	exit(0);
    }

    WriteVideoFrame(ifile, width, height, bufferwidth, yfile, uvfile, YUVformat, startframe);

    fclose(ifile);
    fclose(yfile);
    fclose(uvfile);

    exit(0);

}
