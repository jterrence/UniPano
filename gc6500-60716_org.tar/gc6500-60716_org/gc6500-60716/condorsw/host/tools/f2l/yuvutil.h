/*******************************************************************************
* 
* The content of this file or document is CONFIDENTIAL and PROPRIETARY
* to GEO Semiconductor.  It is subject to the terms of a License Agreement 
* between Licensee and GEO Semiconductor, restricting among other things,
* the use, reproduction, distribution and transfer.  Each of the embodiments,
* including this information and any derivative work shall retain this 
* copyright notice.
* 
* Copyright 2013-2016 GEO Semiconductor, Inc.
* All rights reserved.
*
* 
*******************************************************************************/

/*******************************************************************************
* @(#) $Header: http://svllsvn01.local.geosemi.com/repos/swrepos/trunk/condorsw/host/tools/f2l/yuvutil.h 57866 2016-04-28 11:12:50Z bsmith $
*******************************************************************************/

#ifndef __YUVUTIL_H
#define __YUVUTIL_H

void Frame2LinearInit();
void Linear2FrameInit();

void l2f(char* tiled, int offsetx, int offsety, int width, int height, char* output);
void l2fUV(char* tiled, int offsetx, int offsety, int width, int height, char* u, char* v);
int  f2l(unsigned char* frame, int width, int height, unsigned char* output2, int bufferwidth);
int  f2lUV(unsigned char* U, unsigned char* V, int width, int height, unsigned char* output2, int bufferwidth);

void YUVUtil_GetSegmentSize(int *width, int *height);

#endif

