#!/usr/bin/env python

import argparse
import json
import re
from libparam.param_spec import ParamSpec
from libparam.param_config import ParamConfig

FACTORY_TABLE_ID = 65500

def preprocess_hex(json):
    hexp = re.compile("\"([-]*0x[a-f0-9]+)\"", re.IGNORECASE)
    return hexp.sub(lambda m: str(int(m.group(1), 16)), json)


def load_spec(jsonfile, verbosity_level):
    try:
        f = open(jsonfile)
        json_spec = preprocess_hex(f.read())
        return ParamSpec(json.loads(json_spec), verbosity_level)
    except Exception as e:
        print "'\033[91m'ERROR: %s\033[0m" % e
        return None


def load_config(jsonfile, spec, minmax = ""):
    config = ParamConfig(spec)
    try:
        f = open(jsonfile)
        json_config = preprocess_hex(f.read())
        return config.from_json(json.loads(json_config), minmax)
    except Exception as e:
        print "'\033[91m'ERROR: %s\033[0m" % e
        return None


if __name__ == "__main__":
    import sys

    parser = argparse.ArgumentParser(description="JSON spec+config to binary.")
    parser.add_argument("spec", type=str, 
                        help="JSON spec (text) file")
    parser.add_argument("config", type=str, 
                        help="JSON config (text) file")
    parser.add_argument("endianness", type=str, 
                        help="Endianness, either \"be\" or \"le\"")
    parser.add_argument("outfile", type=str, 
                        help="Output JSON binary file")
    parser.add_argument("-d", dest="verbosity_level", required=False, type=int, 
                        default=4, 
                        help="Output verbosity level (0 = least, 4 = most)")
    parser.add_argument("-v", dest="header_version", required=False, type=int, 
                        default=2, 
                        help=argparse.SUPPRESS)
    parser.add_argument("-z", dest="debug_option", required=False, type=str, 
                        default="", 
                        help=argparse.SUPPRESS)
    args = parser.parse_args()

    spec = load_spec(args.spec, args.verbosity_level)

    if args.header_version == 2: 
        v2 = 1
    else: 
        v2 = 0
    config = load_config(args.config, spec, args.debug_option)

    if spec.scan_uninitialized() == 1: 
        sys.exit(1)
    if spec.has_uninitialized() == 1: 
        spec.vprint("WARNING: found uninitialized parameter(s) (increase verbosity level to see details)!!", 1)
        # sys.exit(1)

    skip_header = False
    if args.debug_option == "extract_fact":
        args.debug_option = "filt" + str(FACTORY_TABLE_ID)
        skip_header = True

    ids = args.debug_option[4:].split(",")
    for (param, val) in sorted(config.params().iteritems(), key=lambda k: k[0].id()):
        if args.debug_option[:4] != "filt" or str(param.id()) in ids:
            spec.vprint("{:<35} = {}".format(param.full_name(), val), 2)

    binbuf = config.to_bin(args.endianness, v2, args.debug_option, skip_header)

    outfile = open(args.outfile, 'wb')
    outfile.write(binbuf)
