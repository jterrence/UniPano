#! /bin/sh
#===============================================================================
# 
# The content of this file or document is CONFIDENTIAL and PROPRIETARY
# to GEO Semiconductor.  It is subject to the terms of a License Agreement 
# between Licensee and GEO Semiconductor, restricting among other things,
# the use, reproduction, distribution and transfer.  Each of the embodiments,
# including this information and any derivative work shall retain this 
# copyright notice.
# 
# Copyright 2013-2016 GEO Semiconductor, Inc.
# All rights reserved.
#
# 
#===============================================================================

#===============================================================================
# @(#) $Id: build_infrastructure.sh 57900 2016-04-30 06:34:51Z bsmith $
#===============================================================================

#-------------------------------------------------------------------------------
# The name of this script as invoked.
#-------------------------------------------------------------------------------
find_my_home()
{
    ( cd ${MY_HOME} ; echo ${PWD} )
}

ME=`basename $0`
MY_HOME=`dirname $0`
FULL_PATH_MY_HOME=`find_my_home`
MAKEFILE_INFRA_NAME=Makefile.infra
MAKEFILE_INFRA_FULLPATH=${MY_HOME}/${MAKEFILE_INFRA_NAME}

#-------------------------------------------------------------------------------
# Display an usage message to standard error.
#-------------------------------------------------------------------------------
usage()
{
    1>&2 cat << END_USAGE_MSG
Usage: ${ME} [-h] [-n]

    Build the infrastructure header files and libraries using
    the ${MAKEFILE_INFRA_NAME}
    in the directory ${FULL_PATH_MY_HOME}/.

Options:
    --help                  Display this help message
    --build_algo            Not to build all targets by default.  Build the 'algorithms' pieces.
    --build_thirdparty      Not to build all targets by default.  Build the 'thirdparty' pieces.
    --target_all            Makefile target is 'all' (default)
    --target_clean          Makefile target is 'clean'
    --dry_run               Dry run
END_USAGE_MSG
}

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
Option_N=
TARGET_ALL='TARGET_ALL=1'
TARGET_ALGO=
TARGET_3RD_PARTY=
TARGET_OF_MAKE=all

#-------------------------------------------------------------------------------
# Parse all command line argvs until the first one that does not begin
# with the '-' character.
#-------------------------------------------------------------------------------
while [ $# -ge 1 ] ; do
    case "${1}" in
    -h | --h | -help | --help)
        usage
        shift
        exit 0
        ;;
    -n | --dry-run)
        Option_N=-n
        shift ;;
    --build_algo)
        TARGET_ALL=
        TARGET_ALGO='TARGET_ALGO=1'
        shift ;;
    --build_thirdparty)
        TARGET_ALL=
        TARGET_3RD_PARTY='TARGET_THIRD_PARTY=1'
        shift ;;
    --target_all)
        TARGET_OF_MAKE=all
        shift ;;
    --target_clean)
        TARGET_OF_MAKE=clean
        shift ;;
    -*)
        1>&2 echo "${ME}: Unsupported option: ${1}"
        usage
        shift
        exit 1
        ;;
    *)
        break
        ;;
    esac
done

make    \
    ${Option_N}                 \
    -C ${FULL_PATH_MY_HOME}     \
    -f ${MAKEFILE_INFRA_NAME}   \
    ${TARGET_ALL}               \
    ${TARGET_ALGO}              \
    ${TARGET_3RD_PARTY}         \
    ${TARGET_OF_MAKE}

#===============================================================================
# vi: set shiftwidth=4 tabstop=8 softtabstop=4 expandtab nosmarttab:
#===============================================================================
