#!/bin/sh
if [ $# -lt 3 ] ; then
	echo 'Usage:' $0 'WIDTH HEIGHT OUTPUTNAME [ADDRESS] [BAYERPATTERN]'
	echo '[ADDRESS]: default 0x200a000'
	echo '[BAYERPATTERN]: options RGGB, BGGR, GBRG, GRBG (default RGGB)'
	exit 0
fi
if [ $# -lt 4 ] ; then
    echo 'Using default address: 0x200a000'
    addr='0x200a000'
    echo 'Using default Bayer pattern: RGGB'
    byp='RGGB'
else
	addr=$4
	byp=$5
fi
outname=$3
sW=256
sH=32
w=$1
w2=$(($1*2))
h=$2
tW=$(((w2 + sW-1)&~(sW-1)))
tH=$(((h + sH-1)&~(sH-1)))
tWH=$((tW*tH))
{
    bin/memrw_usb 0 $addr -f /tmp/$outname.y -m r -d $tWH
    bin/l2f_usb /tmp/$outname.raw $w2 $h 0 0 /tmp/$outname
    bin/bayer2rgb -i /tmp/$outname.raw -o /tmp/$outname.tiff -w $w -v $h -b 16 -f $byp -t
} &> /dev/null
echo $outname'.raw and '$outname'.tiff created at /tmp/'
#cp /tmp/$outname.raw $PWD
#cp /tmp/$outname.tiff $PWD
read -r -p "Open RGB image in GIMP? [y/N] " response
if [[ $response =~ ^([yY][eE][sS]|[yY])$ ]]
then
    gimp /tmp/$outname.tiff &
fi
