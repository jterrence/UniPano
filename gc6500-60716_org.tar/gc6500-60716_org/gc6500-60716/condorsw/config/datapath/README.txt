This directory contains the JSON files for various datapaths. They are handled
by the mainapp app_json.cpp. Mostly video datapaths are constructed; audio needs
to be controlled from system variables though there is currently one object for
controlling audio filter.
Only datapaths for muxed channels are supported which correspond to CAMERA_MODE IPCAM.
This document will cover the datapaths description which are under the "codec" 
object. Some  "system" variables are also covered.

Note: Release does not include all of the datapaths mentioned here.

The datapaths currently defined are:

app_fisheye.json:
    Data path as used in the CPP mainapp app_fisheye.cpp. Uses board type and audio settings for mobileyes5.

app_fisheye_bub.json:
    Same as app_fisheye.json but board type and audio settings for condorbub.

app_fisheye_bub_pme.json:
    Same as app_fisheye_bub.json but getting PME data also in raw channel.

bwtest_534MHzDDR.json:
    Data path as used in the CPP mainapp app_bwtest.cpp for 5MP 534MHz DDR system.

bwtest_400MHzDDR.json:
    Data path as used in the CPP mainapp app_bwtest.cpp for 2MP 400MHz DDR system running in 128MB system.

jpegenc.json:
    Data path VCAP->JPEGENC

dewarp_jpegenc.json:
    Data path VCAP->DEWARP->JPEGENC

avcenc.json:
    Data path VCAP->NVPP->AVCENC
    Uses 1920x1280 as default.

dewarp_avcenc.json:
    Data path VCAP->DEWARP->NVPP->AVCENC
    Uses dewarp output as 1920x1080 and NVPP output as 1280x720
    Can change resolution to 1920x1080 from mxuvc

dewarpcompositor2x2_jpeg.json:
    2x2 composition using 4 dewarp instances and connected to JPEG

dewarpcompositor2x2_avc.json:
    2x2 composition using 4 dewarp instances and connected to AVC

dewarpcompositor2x2_avc_jpeg.json:
    2x2 composition using 4 dewarp instances and connected to AVC and JPEG channels

dewarpcompositor2x2_avc_zcl.json:
    CH1: 2x2 composition using 4 dewarp instances and connected to AVC
    CH2: Non composited dewarp instance using ZCL and connected to AVC

dewarpcompositor2x1_1x2_avc.json:
    CH1: 2x1 composition using 2 dewarp instances and connected to AVC
    CH2: 1x2 composition using 2 separate dewarp instances and connected to AVC

2xavcencs1080p30-dewarp.json:
    Uses 2 AVC 1080p encoders with 30fps.
    Uses AVC performance level 4 which is defined as CONDOR_PERF_1080P60 in avcenclib.h

fileavcenc.json:
    Encode a YUV file transferred via USB into AVC. It uses the file encoder based vcap object "fevcap".
    After booting with this JSON file, run the file host/tools/fileencode/enc.sh after updating the script appropriately.

app_skype.json:
   Uses skype data path with some configurable objects in "codec".

Using the data paths:
=====================

app_fisheye.json sets BOARD to mobileyes5 as default.
For other data path configurations, condorbub is the default board.
When using on a board different than the one specified in the .json file, do one of the following:

    Option 1:
    ---------

    Edit the JSON file and change the BOARD variable under system accordingly.
    For example, to use on Condor BUB board, it should be:

    "BOARD": "condorbub"

    If auto detect of board is supported, just remove the above line.

    Download the updated JSON file into the camera using the command:

    mxcam writecfg file.json

    or if using downloadable JSON: 

    mxcam boot fimrware.img file.json

    Use appropriate image depending on if cache boot is used or not.

    Option 2:
    ---------

    Download the JSON into the camera using the command:

    mxcam writecfg file.json

    Where file.json is the name of the JSON file.

    Then update the board type using "mxcam setkey" command. For example, to set the board
    to condor BUB, do following:

    mxcam setkey BOARD condorbub

Reset the camera and download the image again using "mxcam boot".

The new data path would be used. Use fishy or any other mxuvc client.

Objects Description:
====================

system:
------

There will be only one instance of system and it has to be defined always with a few mandatory keys.
All System object key value pairs should be given under double quote (string format) for eg: "MIC_GAIN": "45". 
No other format is supported for key value pairs in sytem object.
This is a limitation of our implementation of JSON.

       "system": {
        "BOOTMODE": "usb",
        "BOARD": "mobileyes5",
        "CAMERA_MODE": "IPCAM",
        "MAINAPP": "app_json",
        "DAC": "ti3100",
        "ADC": "max9860",
        "AUDIO_MCLK": "audgenclk",
        "AIN0": "slave",
        "AIN1": "slave",
        "I2S_CLKSRC": "adc",
        "MIC_MAX_GAIN": "45",
        "MIC_GAIN": "45",
        "SPK_GAIN": "40",
        "NO_OF_MIC": "1",
        "MONO_MIC": "left",
        "SPKR_STATE": "off",
    }, 

Name            Description                 values                  Defaults (if any )
----            -----------                 ------                  -----------------

MANDATORY KEYS
--------------
BOOTMODE        Method of booting           usb/snor                usb             
                the firmware

BOARD           Which board configuration   condorbub/mobileyes5    [ many configurations based on customer will get added to the values list ] 
                to load                     

CAMERA_MODE     Which application mode      IPCAM/SKYPE_ISOC        IPCAM
                should the camera be        /SKYPE_BULK
                configured

MAINAPP         Main application file       app_json/app_fisheye    app_json
                which camera should use     app_skype   

OPTIONAL KEYS
-------------

MAXRES_INPUT    Sensor resolution           Sensor Specific:        Sensor specific
                                            720p60 
                                            720p
                                            1280x1024p30mipi
                                            1280x1024p60mipi
                                            1280x1024p30
                                            1280x1024p60
                                            1080p60
                                            1080p30
                                            1080p24  
                                            1296x1296
                                            1344x1080
                                            1344x1344
                                            2304x1296
                                            1920x1536
                                            2304x1536
                                            5Mp30
                                            5Mp24
                                            4Mp30
                                            4Mp15
                                            4Mp10
                                            4Mp8

DDR_SIZE_MB     DDR size in Mega Bytes      256                     256
                                            128

AUDIO_ENABLE    Enable/Disable audio        1                       1
                encode                      0

MEMBW_QUERY     Enable/Disable memory       1                       0
                bandwidth results           0
                for querydump

DAC             The DAC being used          ti3100                  [ different DAC gets used by customers based on their need so more options will get added ]
                on board for speaker        

ADC             The ADC being used          onchip/digitalmic       onchip [ more ADC options gets added based on customers board design ]
                on board for mic            max9860

AUDIO_MCLK      Source of the MCLK          audgenclk/mssi          audgenclk
                for I2S audio capture   

AIN0            Mode of I2S clock           master/slave            master
                operation for audio 
                capture on audio input
                port 0

AIN1            Mode of I2S clock           master/slave            master
                operation for audio 
                capture on audio input
                port 1

I2S_CLKSRC      Souce of I2S clock          adc/dac/condor          adc
                LRCK & BCK for audio 
                capture on audio input
                port 0/1.

MIC_MAX_GAIN    Max gain that a adc/mic                             [ max gain values is totally dependant on the adc supported gain and use case ] 
                should support in a     
                particular configuration    

MIC_GAIN        Default mic gain with                               [ options and default value again depends on ADC capability and use case ] 
                which the board should
                boot up 

SPK_GAIN        Default spk gain with                               [ options and default value again depends on DAC capability and use case ] 
                which the board should
                boot up

NO_OF_MIC       The configuration of        1/2                     2 [ 2 indicates stereo mode and 1 indicates mono mode ]
                audio interface to work
                as mono or stereo

MONO_MIC        When NO_OF_MIC=1, option    left/right              left
                to set mic to left/right
                channel depending on mic
                connection on the board

SPKR_STATE      Default state of speaker    on/off                  off [ "on" turns on the speaker and "off" turns off the speaker ]
                with which the board
                should boot up

SPK_SAMPLERATE  Default sample rate at      8000/16000/24000        16000 [ options may vary based on capability of DAC ]
                which speaker should 
                operate

MIC_SAMPLERATE  Default sample rate at      8000/16000/24000        16000 [ options may vary based on capability of ADC and use case ]
                which mic should 
                operate

I2S_MCLK        Value being set for MCLK                            [ options depend on the ADC capability and this value is valid only if
                pin of I2S for audio                                  if customer is using external ADC, not internal to our chip ]
                capture on port 0/1

SENSOR_HOFFSET  Horizontal offset to add    -8/.../-1/0/1/.../7     0 [ No need to specify if no adjustment for horizontal window offset is required ]
                to default horizontal                 
                window offset depending
                on RGGB Start setting,
                Applied to sensor OV4689,
                Practiable combinations:
                | RGGB Start | HOFFSET |
                |          3 |       0 |
                |          2 |    -1/1 |
                |          1 |       0 |
                |          0 |    -1/1 |

SENSOR_VOFFSET  Vertical offset to add      -4/.../-1/0/1/.../3     0 [ No need to specify if no adjustment for vertical window offset is required ]
                to default vertical                 
                window offset depending
                on RGGB Start setting,
                Applied to sensor OV4689,
                Practiable combinations:
                | RGGB Start | VOFFSET |
                |          3 |       0 |
                |          2 |       0 |
                |          1 |    -1/1 |
                |          0 |    -1/1 |

SENSOR          Forced sensor selection     Sensor specific         Detects the sensor  
                instead of auto detect.     like AR0330/OV4689/...
                Needed where sensor cannot  Cannot list all values 
                be auto detected though     as some are customer
                can also be used on         specific
                other sensors.

RAWCAP_PAUSED   If "1", then capture will   "0"/"1"                 "0"
                not start till bit 31 of
                Spare0 register is set
                using:
                sudo mxcam qccrw 0x13 0x4 4 0x80000000
                This allows configuration
                before capture.
                            
GRID_MAP_IN_SNOR Auto loading feature       0                       0 [ mxcam writemap should be used for flashing grid map(s) to SNOR Flash ]    
                for grid map(s) at bootup.  1
                 
BOARD_REMOVE_MIC_DC_NOISE                   1                       1
                Enable/Disable DC noise     0
                filter. The filter removes
                the dc signal and low freq
                board noise from MIC audio
                when enabled.

PRODUCT         USB descriptor product    Alphanumeric string       "GC6500"
                string. This key is only  max size is 100 bytes.
                applicable for iOS ACC 
                Mode.

MANUFACTURER    USB descriptor manufacturer Alphanumeric string     "GEOSEMI"
                string. This key is only    max size is 100 bytes.
                applicable for iOS ACC 
                Mode.

EAPNAME         USB descriptor EA interface Alphanumeric string     "com.geosemi.360camera.protocol"
                string. This key is only    max size is 100 bytes.
                applicable for iOS ACC 
                Mode.

SERIALNO        USB descriptor serial no   Alphanumeric string      "1234"
                string. This key is only   max size is 100 bytes.
                applicable for iOS ACC 
                Mode.

** NOTE:
-------
All the I2S, Mic, Spkr paramters are very much interdependent on each other. Hence play with these parameters only if you know what you are doing.
These parameters also depend on the kind of board design and components the customer has chosen. 

isp:
----

There will be only one instance and it will always be defined like:

        "isp0": {
            "type": "isp"
        }

vcap:
-----

There will be only one instance and it will be defined like:

        "vcap0": {
            "type": "vcap",
            "cropWidth": 0,
            "cropHeight": 0,
            "numOutputFrames": 6,
            "stcPerOutputFrame": 3000,
            "params" : {
                "Q_VCAP_CMP_VARIABLE_FRAME_RATE": 1,
                "Q_VCAP_CMP_AV_SYNCH_ENABLE": 0,
                "Q_VCAP_CMP_STC_PER_FRAME": 3000
            }
        }

        To enable just horizontal scaling:
            - Add the parameter "outWidth" in above.
            - Add the following in "params" section:
              "Q_VCAP_CMP_HW_BYPASS_HSCALE"    : 0

        To use both horizontal and vertical scaling, specify the following additional parameters:
            "scaling": 1,
            "outWidth": 1920,
            "outHeight": 1080,
            "maxWidth": 1920,
            "maxHeight": 1080,


"vcap0" is the instance name. Though any instance name can be used, this is the recommended name.
Here is the description of the variables outside the "params" sub-object:

Name                Description                     Values                      Default (if any)
----                -----------                     ------                      ----------------

type                Object Type                     "vcap" or "fevcap"          No default - must be specified

cropWidth           See description of              0: Sensor resolution        Sensor resolution (width) 
                    Q_VCAP_CMP_CROP_WIDTH              (width)
                                                    Any even value upto         
                                                    sensor resolution (width)

cropHeight          See description of              0: Sensor resolution        Sensor resolution (height)
                    Q_VCAP_CMP_CROP_HEIGHT             (width)
                                                    Any even value upto 
                                                    sensor resolution (height)

scaling             Enable/Disable scaling          0: Disable scaling          0
                                                    1: Enabling scaling 
                                                       using VSCALE0

outWidth            See description of              Any even value              cropWidth     
                    Q_VCAP_CMP_PICT_WIDTH           

outHeight           See description of              Any even value              cropHeight     
                    Q_VCAP_CMP_PICT_HEIGHT                                
                    Needed only when scaling=1

maxWidth            Value used in object creation   Any value up to maximum     outWidth 
                    (width)

maxHeight           Value used in object creation   Any value up to maximum     outHeight 
                    (height)

numOutputFrames     Number of output frames         Normally from 4-6           6
                    in queue

stcPerOutputFrame   See description of              Value in ticks per frame    No default - must be specified
                    Q_VCAP_CMP_STC_PER_OUTPUT_FRAME

gyroenable          Enable/Disable gyro sampling    0: Disable gyro sampling    0
                                                    1: Enable gyro sampling

gyrotype            Select Gyro HW  type            -1: invalid                 -1
                                                    0:  gt
                                                    1:  bs                                                    

The variables inside "params" refer to LiveVideoCaptureAPI_SetParam() variables of the same name.
See codec/middleware/qvcap.h for details of the variables. Any variable from there can be added here 
without any change in app_json.cpp 

ov:
-----

There can be maximum of 8 instances and it will be defined like:

        "ov0": {
            "type": "ov",
            "bypass": 0,
            "privacy": 0
        }

"ov0" is the instance name for enabling overlay using GPU. Though any instance name can be used, this is the recommended name. Here is the description of the variables outside the "params" sub-object:
The default ov mode is logo.

ov for me360:
-------------

        "ov0": {
            "type": "ov", 
            "parent": "dewarp1", 
            "alloc422": 0, 
            "cam360Enable": 1, 
            "cam360BlendType": 5,
            "cam360E1BeyondWidth": 32,
            "bypass": 0
        }, 


Name                Description                     Values                      Default (if any)
----                -----------                     ------                      ----------------

type                Object Type                     "ov"                        No default - must be specified

bypass              enable/disable bypass mode       0-1                        0 - optional, default disabled

privacy             enable/disable privacy mask      0-1                        0 - optional, default disabled (deprecated, use "mode" param)

text                enable/disable text using gpu    0-1                        0 - optional, default disabled (deprecated, use "mode" param)

mode                select between gpu modes        "LOGO"
                                                    "PRIVACY"
                                                    "BURNIN"

(ME360: Following parameters are required only for me360 mode and are not needed otherwise)

cam360Enable        enable/disable me360 mode        0-1                        0 - mandatory,
default disabled

cam360BlendType     blending mode                    0-5                        0 - mandatory
default disabled

cam360E1BeyondWidth width of dewarp artefact on left 0-32                       0 - optional
default disabled    side             

dewarp:
-------

There can be maximum of 12 instances and defined like:

Not used in compositor:

        "dewarp0": {
            "type": "dewarp",
            "parent": "vcap0",
            "outWidth": 1280,
            "outHeight": 720,
            "maxWidth": 1280,
            "maxHeight": 720,
            "maxNumMaps": 4,
            "numOutputFrames": 4,
            "minMapN": 78,
            "dewarpedFrameWidth": 1920,
            "dewarpedFrameHeight": 1024,
            "ePTZMode": "WM_ZCLSTRETCH",
            "ePTZParams": {
                "HPan": 0,
                "VPan": 0,
                "Zoom": 1
            },
            "params" : {
                "Q_DEWARP_CMP_SET_LENS_MILI_RADIUS": 833000,
                "Q_DEWARP_CMP_SET_LENS_MILI_FOV": 180000,
                "Q_DEWARP_CMP_SET_LENS_MILI_HSHIFT": 0,
                "Q_DEWARP_CMP_SET_LENS_MILI_VSHIFT": 0
            }
        }

Used in compositor:

        "dewarp0": {
            "type": "dewarp",
            "parent": "vcap0",
            "outWidth": 1280,
            "outHeight": 720,
            "maxNumMaps": 4,
            "numOutputFrames": 4,
            "minMapN": 128,
            "offsetX": 0,
            "offsetY": 0,
            "displayWidth": 1280,
            "displayHeight": 720,
            "displayStripe": 1280,
            "dewarpedFrameWidth": 1920,
            "dewarpedFrameHeight": 1024,
            "ePTZMode": "WM_1PANELEPTZ",
            "ePTZParams": {
                "HPan": 0,
                "VPan": 0,
                "Zoom": 40,
                "Tilt": 0
            },
            "videoCompositor": 1,
            "params" : {
                "Q_DEWARP_CMP_SET_LENS_MILI_RADIUS": 833000,
                "Q_DEWARP_CMP_SET_LENS_MILI_FOV": 180000,
                "Q_DEWARP_CMP_SET_LENS_MILI_HSHIFT": 0,
                "Q_DEWARP_CMP_SET_LENS_MILI_VSHIFT": 0
            }
        }

used in me360:
---------------

        "dewarp0": {
            "type": "dewarp", 
            "parent": "vcap0", 
            "dewarpedFrameWidth": 3072, 
            "dewarpedFrameHeight": 1664, 
            "outWidth": 2048, 
            "outHeight": 1080, 
            "displayWidth": 4096, 
            "displayHeight": 1080, 
            "offsetX": 0, 
            "offsetY": 0, 
            "maxNumMaps": 2, 
            "numOutputFrames": 4, 
            "minMapN": 32, 
            "ePTZMode": "OFF", 
            "videoCompositor": 1, 
            "cam360Alpha": 1, 
            "alloc422": 0, 
            "cam360AlphaWidth": 1920, 
            "cam360E0EdgeWidth": 16,
            "cam360E0EdgeOffset": 0,
            "cam360E1EdgeWidth": 16,
            "cam360E1EdgeOffset": 0, 
            "params": {
                "Q_DEWARP_CMP_MAPN": 32, 
                "Q_DEWARP_CMP_SET_ALPHA_LENS_MILI_RADIUS": 0, 
                "Q_DEWARP_CMP_SET_FRAMES_PER_MAP": 4, 
                "Q_DEWARP_CMP_INPUT_FRAME_IS420": 1, 
                "Q_DEWARP_CMP_OUTPUT_FRAME_IS420": 1, 
                "Q_DEWARP_CMP_DEWARPED_FRAME_IS420": 1, 
                "Q_DEWARP_CMP_SET_LENS_MILI_FOV": 203000, 
                "Q_DEWARP_CMP_SET_LENS_MILI_HSHIFT": -676159, 
                "Q_DEWARP_CMP_SET_LENS_MILI_VSHIFT": 7139, 
                "Q_DEWARP_CMP_SET_LENS_MILI_RADIUS": 708360
            }
        }, 
        "dewarp1": {
            "type": "dewarp", 
            "parent": "vcap0", 
            "dewarpedFrameWidth": 3072, 
            "dewarpedFrameHeight": 1664, 
            "outWidth": 2048, 
            "outHeight": 1080, 
            "displayWidth": 4096, 
            "displayHeight": 1080, 
            "offsetX": 2048, 
            "offsetY": 0, 
            "maxNumMaps": 2, 
            "numOutputFrames": 4, 
            "minMapN": 32, 
            "ePTZMode": "OFF", 
            "videoCompositor": 1, 
            "alloc422": 0, 
            "params": {
                "Q_DEWARP_CMP_MAPN": 32, 
                "Q_DEWARP_CMP_SET_ALPHA_LENS_MILI_RADIUS": 0, 
                "Q_DEWARP_CMP_SET_FRAMES_PER_MAP": 4, 
                "Q_DEWARP_CMP_INPUT_FRAME_IS420": 1, 
                "Q_DEWARP_CMP_OUTPUT_FRAME_IS420": 1, 
                "Q_DEWARP_CMP_DEWARPED_FRAME_IS420": 1, 
                "Q_DEWARP_CMP_SET_LENS_NUM2_MILI_FOV": 203000, 
                "Q_DEWARP_CMP_SET_LENS_NUM2_MILI_HSHIFT": 673180, 
                "Q_DEWARP_CMP_SET_LENS_NUM2_MILI_VSHIFT": 6159, 
                "Q_DEWARP_CMP_SET_LENS_NUM2_MILI_RADIUS": 699000
            }
        }, 

"dewarp0" is the instance name. Other instances can be named "dewarp1", "dewarp2", etc. 
Though any instance name can be used, this is the recommended scheme.
Here is the description of the variables outside the "params" sub-object:

Name                Description                     Values                      Default (if any)
----                -----------                     ------                      ----------------

type                Object Type                     "dewarp"                    No default - must be specified

parent              Parent object instance          Instance of type            No default - must be specified
                                                    "dewarp", "vcap" or "nvpp";
                                                    normally "vcap"

outWidth            See description of              Any legal value             No default - must be specified     
                    Q_DEWARP_CMP_OUTPUT_FRAME_WIDTH

outHeight           See description of              Any legal value             No default - must be specified     
                    Q_DEWARP_CMP_OUTPUT_FRAME_HEIGHT

maxNumMaps          Maximum number of maps          Any legal value             No default - must be specified

numOutputFrames     Number of output frames         Normally from 4-6           6
                    in queue

minMapN             Minimum MapN value which is     Any legal value             No default - must be specified
                    also the initial value

offsetX             See description of              Any legal value             0
                    Q_DEWARP_CMP_COMP_FRAME_OFFSET_X

offsetY             See description of              Any legal value             0
                    Q_DEWARP_CMP_COMP_FRAME_OFFSET_Y

displayWidth        See description of              Any legal value             outWidth
                    Q_DEWARP_CMP_COMP_FRAME_WIDTH

displayHeight       See description of              Any legal value             outHeight
                    Q_DEWARP_CMP_COMP_FRAME_HEIGHT

displayStride       See description of              Any legal value             displayWidth
                    Q_DEWARP_CMP_COMP_FRAME_STRIPE

dewarpedFrameWidth  See description of              0: Use default              (outWidth * 2) &~63
                    Q_DEWARP_CMP_DEWARPED_FRAME_WIDTH

dewarpedFrameHeight See description of              0: Use default              (outHeight * 2)&~63
                    Q_DEWARP_CMP_DEWARPED_FRAME_HEIGHT

maxWidth            Value used in object creation   Any value up to maximum     displayWidth 
                    (maximum output width) 

maxHeight           Value used in object creation   Any value up to maximum     displayHeight
                    (maximum output height) 

numOutputFrames     Number of output frames         Normally from 4-6           No default - must be specified
                    in queue

ePTZMode            EPTZ mode string                "WM_ZCL"                    No default - must be specified
                                                    "WM_ZCLCYLINDER"
                                                    "WM_ZCLSTRETCH"
                                                    "WM_1PANELEPTZ"
                                                    "SWEEP_WM_1PANELEPTZ"

ePTZParams          EPTZ parameters object                                      As defined in app_json.cpp
                    Valid keys are:
                    WM_ZCL: 
                      Phi0, Rx, Ry, Rz, Gshift
                    WM_ZCLCYLINDER:
                      CylinderHeightMicro
                    WM_ZCLSTRETCH:
                      HPan, VPan, Zoom
                    WM_1PANELEPTZ:
                      HPan, VPan, Zoom, Tilt
                    SWEEP_WM_1PANELEPTZ:
                      HPanStart, VPanStart,
                      ZoomStart, TiltStart,
                      HPanInc, TiltInc,
                      VPanInc, ZoomInc,
                      Period 
                    CM_CIRCPANOVIEWPAN:
                      Radius0Mili, Radius1Mili,
                      Phi1, PhiShift1
                    CAM360: and
                    DEMO_CAM360:
                      LeftHPan
                      LeftVPan
                      LeftTilt
                      LeftZoom
                      LeftDivisor
                      RightHPan
                      RightVPan
                      RightTilt
                      RightZoom
                      RightDivisor
                                                    
videoCompositor     Compositor instance             0: No compositor                0
                                                    1-4: Compositor Instance

gyro                enable/disbale gyro             0: disabled                     0
                    stabilization                   1: enabled, needs gyro 
                                                    object to be connected as parent                                        

The variables inside "params" refer to DewarpAPI_SetParam() variables of the same name.
See codec/middleware/qdewarp.h for details of the variables. Any variable from there can be added here 
without any change in app_json.cpp.
Notes:
  -  If lens variable Q_DEWARP_CMP_SET_LENS_MILI_RADIUSis not defined, then following values are used:
     5MP sensor: 1488000
     Other: 833000

  - If you want to use decimation, add the following in "params":

    "Q_DEWARP_CMP_FRAME_RATE_DECIMATION": 30

  - When using compositor, specify the last dewarp instance of the compositor instance as parent
    for the downstream component.

Dewarp ME360 Params:
---------------------

Following params are requird by dewarp to only for me360 use case.

    "cam360Alpha": 1, 
    "cam360AlphaWidth": 1920, 
    "cam360E0EdgeWidth": 16,
    "cam360E0EdgeOffset": 0,
    "cam360E1EdgeWidth": 16,
    "cam360E1EdgeOffset": 0, 

cam360Alpha        enable/disable linear alpha plane         0-1                  0 - mandatory

cam360AlphaWidth   width of linear alpha plane               1920                 1920 mandatory

cam360E0EdgeWidth   width of E0 blending plane               0-32                 16 - mandatory

cam360E0EdgeOffset   shift offset of E0 blending plane       -16-16               0 - mandatory

cam360E1EdgeWidth   width of E1 blending plane                0-32                16 -mandatory

cam360E1EdgeOffset  shift offset of E1 blending plane         -16-16              0 - mandatory


nvpp:
-----

There can be maximum of 6 instances and defined like following when not used in getting PME data::

        "nvpp0": {
            "type": "nvpp",
            "parent": "dewarp0",
            "port": 1,
            "maxWidth": 1280,
            "maxHeight": 720,
            "cropWidth": 0,
            "cropHeight": 0,
            "cropOffsetX": 0,
            "cropOffsetY": 0,
            "outWidth": 1280,
            "outHeight": 720,
            "numOutputFrames": 4,
            "stcPerOutputFrame": 3000,
            "params" : {
                "Q_NVPP_CMP_VARIABLE_FRAME_RATE": 1,
                "Q_NVPP_CMP_INITIAL_DELAY": 1
             }
        }

Defined like following when used with raw channel to get PME data:

        "nvpp3": {
            "type": "nvpp",
            "parent": "dewarp0",
            "port": 1,
            "maxWidth": 1280,
            "maxHeight": 720,
            "cropWidth": 0,
            "cropHeight": 0,
            "cropOffsetX": 0,
            "cropOffsetY": 0,
            "outWidth": 320,
            "outHeight": 240,
            "stcPerOutputFrame": 3000,
            "avcPreProc": 1,
            "pmeObjectTracking": 1,
            "pmeResultsMV": 0,
            "pmeResultsMCost": 0,
            "pmeNumRefs": 1,
            "params" : {
                "Q_NVPP_CMP_VARIABLE_FRAME_RATE": 1,
                "Q_NVPP_CMP_INITIAL_DELAY": 1
            },
            "preProcParams": {
                "Q_NVPP_CMP_PREPROC_MOTION_VECTOR_ENABLE": 1
             }
        }

"nvpp0" is the instance name. Other instances can be named "nvpp1", "nvpp2" and "nvpp3". 
Though any instance name can be used, this is the recommended scheme.
Here is the description of the variables outside the "params" sub-object:

Name                Description                     Values                       Default (if any)
----                -----------                     ------                       ----------------

type                Object Type                     "nvpp"                       No default - must be specified

parent              Parent object instance          Instance of type             No default - must be specified
                                                    "dewarp", "vcap" or "nvpp"

port                Scalar path used                1 or 2                       1
                    1=FBR0->CROP1->VPP0->FBW1
                    2=FBR1->CROP2->VSCALE0->FBW2
                    2 cannot be used when scaling=1
                    in vcap object

maxWidth            Value used in object creation   Any value up to maximum      No default - must be specified
                    (width) 

maxHeight           Value used in object creation   Any value up to maximum      No default - must be specified
                    (height) 

cropWidth           See description of              0: Parent output resolution  Parent output resolution (width)       
                    Q_NVPP_CMP_CROP_WIDTH              (width)
                                                    Any even value upto         
                                                    parent output resolution 
                                                    (width)

cropHeight          See description of              0: Parent output resolution  Parent output resolution (height)
                    Q_NVPP_CMP_CROP_HEIGHT             (width)
                                                    Any even value upto 
                                                    parent output resolution 
                                                    (height)

cropOffsetX         See description of              0 or any even value within   0
                    Q_NVPP_CMP_CROP_OFFSET_X        crop width

cropOffsetY         See description of              0 or any even value within   0
                    Q_NVPP_CMP_CROP_OFFSET_Y        crop height

outWidth            See description of              Any even value               No default - must be specified     
                    Q_NVPP_CMP_PICT_WIDTH           for initial output

outHeight           See description of              Any even value               No default - must be specified     
                    Q_NVPP_CMP_PICT_HEIGHT          for initial output           No default - must be specified

numOutputFrames     Number of output frames         Normally from 4-6            6
                    in queue

stcPerOutputFrame   See description of              Any legal value in           0
                    Q_NVPP_CMP_STC_PER_OUTPUT_FRAME ticks per frame
                    Can also use old name 
                    inputPTSInterval

avcPreProc          Used to control whether         0 or 1                       0
                    AVC preproc is used or not      Must be 1 to send PME data
                                                    to host

pmeObjectTracking   Used to control whether         0 or 1                       0
                    PME data needs to be            Must be 1 for nvpp instance
                    pre-allocated in nvpp creation  connected to raw channel 
                    Also must set                   for sending PME data to host 

pmeResultsMV        Save MV to DRAM for:            0, 1, 2 or 3                 0 
                    0=8x8, 1=8x8,8x4, 2=4x4,
                    3=8x8,8x4,4x8,4x4

pmeResultsMCost     Save MCost to DRAM:             0 or 1                       0
                    0=don’t save, 
                    1=save same as “ResultsMV”

pmeNumRef           Number of references            Must be 1                    1
                                                    Other values not supported

pmeOpStartX         Operation Start X-coordinate,   0 to (pixelwidth/8 - 1)      0
                    divided by 8

pmeOpStopX          Operation Stop X-coordinate,    0 to (pixelwidth/8 - 1)      0
                    divided by 8                                                 (Use total width)

pmeOpStartY         Operation Start Y-coordinate,   0 to (pixelheight/8 - 1)     0
                    divided by 8

pmeOpStopY          Operation Stop Y-coordinate,    0 to (pixelheight/8 - 1)     0
                    divided by 8                                                 (Use total height)


The variables inside "params" refer to NonRealTimeVideoCaptureAPI_SetParam() variables of the same name.
See codec/middleware/qnvpp.h for details of the variables. Any variable from there can be added here 
without any change in app_json.cpp 

The variables inside "preProcParams" refer to NonRealTimeVideoPreProcAPI_SetPreProcParam() variables of the same name.
See codec/middleware/qnvpp.h for details of the variables. Any variable from there can be added here 
without any change in app_json.cpp. To enable generation of PME motion vectors, must set 
Q_NVPP_CMP_PREPROC_MOTION_VECTOR_ENABLE to 1. 

avcenc:
-------

There can be maximum of 8 instances and defined like:

        "avcenc0": {
            "type": "avcenc",
            "parent": "nvpp0",
            "channel": "CH1",
            "streamId": 0,
            "maxWidth": 1920,
            "maxHeight": 1080,
            "outputBufferSize": 6000000,
            "profile": "main",
            "level": 31,
            "gopSize": 30,
            "bitRate": 2000000,
            "maxFrameSize": 147456,
            "perfLevel": 4,
            "rateControl": "CBR",
            "videoEncParams": {
                "Q_AVCENC_CMP_VPP_REGION_STAT_ENABLE": 0
             },
            "videoEncRateControlParams": {
            }
        }

"avcenc0" is the instance name. Other instances can be named "avcenc1", "avcenc2", etc. 
Though any instance name can be used, this is the recommended scheme.

Here is the description of the variables outside the "videoEncParams" and "videoEncRateControlParams" sub-objects:

Name                Description                     Values                      Default (if any)
----                -----------                     ------                      ----------------

type                Object Type                     "avcenc"                    No default - must be specified

parent              Parent object instance          Instance of type "nvpp",    No default - must be specified
                                                    "vcap", "fevcap" or "dewarp"

channel             Follows mxuvc convention        "CH1", "CH2", etc.          No default - must be specified
                    starting from "CH1". 
                    The actual firmware value will 
                    start from 0.

streamId            ID used for the stream in the   0, 1, etc. Should not       No default - must be specified
                    muxer                           duplicate.

maxWidth            Value used in object creation   Any value up to maximum     No default - must be specified
                    (width) 

maxHeight           Value used in object creation   Any value up to maximum     No default - must be specified
                    (height) 

outputBufferSize    Output buffer size              See typical values in       No default - must be specified
                    for the maximum resolution      codec/mainapp/sysconfig.h:
                    specified                       QVGA: 500000
                                                    VGA:  1500000
                                                    720p: 2500000
                                                    1080p:6000000

profile             AVC profile                     "baseline"                  No default
                                                    "main"                      
                                                    "high"

level               AVC level                       Any legal value             Whatever is set in defaultVidCfg.dwAvcLevel
                                                                                Currently 41

gopSize             GOP size in frames              Any legal value             Whatever is set in defaultVidCfg.dwKeyFrameRate
                                                                                Currently 30

bitRate             Encoding bitrate in bits/sec    Any legal value             Whatever is set in defaultVidCfg.dwBitrate
                                                                                Currently 1000000

maxFrameSize        Maximum AVC frame size          Any legal value             Whatever is set in defaultVidCfg.dwAvcMaxFrameSize
                    in Bytes                                                    Currently 144 * 1024

perfLevel           Performance Level               0, 1, 2, 3 or 4             3
                                                    0 = LOW_PERF
                                                    1 = MED_PERF
                                                    2 = HIGH_PERF
                                                    3 = RAPTOR_PERF
                                                    4 = CONDOR_PERF_1080P60

rateControl         Rate control mode               "BUFFER_MODEL"              This rate control mode can be used only if there
                                                                                is an NVPP connected in the data path of the encoder. NVPP 
                                                                                passthrough mode should be disabled and the NVPP should not 
                                                                                bypass the stats. Both the specified params should be disabled (0)
                                                                                Q_NVPP_CMP_HW_BYPASS_ALL_STAT, Q_NVPP_CMP_VID_PASSTHROUGH.
                                                                                For proper functionality of the BUFFER_MODEL the RCBufferSizeBits
                                                                                maxFrameSize has to be tuned properly.

RCBufferSizeBits    Rate control buffer OR          Any value within the limit  Default value is 3 times the set bitrate. This parameter is valid
                    Coded Picture Buffer            of selected encoder level   only for "BUFFER_MODEL" Rate control mode.
                    size in bits                    See H264 Table A-1
                                                    for LeveL Limits


The variables inside "videoEncParams" refer to AVCEncoderAPI_SetVideoEncParam() variables of the same name.
The variables inside "videoEncRateControlParams" refer to AVCEncoderAPI_SetVideoEncRateControlParam() variables of the same name.
See codec/middleware/qavcenc.h for details of the variables. Any variable from there can be added here 
without any change in app_json.cpp 

SETTINGS & TUNING PARAMETERS FOR BUFFER MODEL RATE CONTROL
----------------------------------------------- 
Buffer model rate control can be chosen only when there is atleast one NVPP present in the data path. 
The following two parameters must be disabled on NVPP for BUFFER MODEL rate control to work. 
Q_NVPP_CMP_HW_BYPASS_ALL_STAT=0,  
Q_NVPP_CMP_VID_PASSTHROUGH=0 

JSON PARAMETERS ( mandatory )
---------------

1. rateControl: It must be set to “BUFFER_MODEL”. 

2. RCBufferSizeBits: It is the Rate control buffer size or Coded Picture Buffer [CPB] size in bits.  

3. maxFrameSize:  This is the maximum size of elementary AVC frame that encoder will aim to generate. This parameter is set in Bytes.

4. gopSize :  The parameter defines the interval between two Iframes.  

5. bitrate:  This parameter has two different meanings.  
-> If the set value is higher than 51, then it is the number of bits/sec that encoder will aim to generate. 
-> If the set value is equal to 51 or lower then it is considered as fixed QP value that encoder should use to encode all the frames. 

Q PARAMETERS ( optional )
------------

1. Q_AVCENC_RC_CMP_BASE_QP                                  :   This parameter sets the initial QP (compression level) for the first Iframe.  In order to ensure that the first frame is consistent with the bitrate of the rest of the stream, low bitrate applications may set BASE_QP higher and high bitrate applications may set it lower. Firmware will ignore this parameter if set to 0. This parameter value should be greater than 0 to be considered as a valid setting.
Default value:  0 ( Base QP will not be considered if set to 0 )
Max value: 51
Min Value: 1

2. Q_AVCENC_RC_CMP_BUFFER_MODEL_RC_1ST_IFRAME_BITS          :   Sets the max amount of bits to be allocated for 1st Iframe. This parameter will be ignored if the BASE_QP parameter is set to a value greater than 0. Firmware will ignore this parameter if set to 0.
Defalut value: 0
Max value: RCBufferSizeBits
Min value: 1

3. Q_AVCENC_RC_CMP_BUFFER_MODEL_NO_FRAMEDROP_ON_OVERFLOW    :   If enabled, Rate control will not drop any frame when Buffer[CPB] overflows. If disabled, Rate control will drop all frames when Buffer[CPB] overflows. 
Default value: 1 ( enabled )

4. Q_AVCENC_RC_CMP_BUFFER_MODEL_TRGT_FULLNESS               :   The percentage medium-term target buffer fullness which the Rate Control tries to achieve over a period of GOP frames by monitoring the Buffer fullness trend.
Default value: 30 -> 30% of RCBufferSizeBits
Max Value: 100 -> 100% of RCBufferSizeBits
Min Value: 0

5. Q_AVCENC_RC_CMP_BUFFER_MODEL_FRSZ_TRKWINDOW              :   This corresponds to the window size for monitoring the short term trend of Pframe size. 
NOTE:
1. When TSVC mode is enabled, this parameter should be same as the Long Term Reference Interval of the TSVC level set by User.
For Eg: TSVC level 4 the Long Term Reference Interval is 8, hence this should be set to 8.
2. When GOPSize <= 3 then the Min value is considered.
Default value: 3
Min Value: 3
Max Value: GOPSize

6. Q_AVCENC_RC_CMP_BUFFER_MODEL_RC_PI_QP_DELTA_MAX          :   Maximum reduction of Iframe QP from previous Pframe QP.
Default value: 3
Min value: 2
Max value: 50

7. Q_AVCENC_RC_CMP_BUFFER_MODEL_RC_PP_QP_DELTA_MAX          :   Maximum QP reduction between any 2 consecutive Pframes.
Default value: 3
Min value: 2
Max value: 50


jpegenc:
--------

There can be maximum of 8 instances and defined like:

        "jpegenc0": {
            "type": "jpegenc",
            "parent": "vcap0",
            "channel": "CH3",
            "streamId": 2,
            "maxWidth": 2592,
            "maxHeight": 1944,
            "outputBufferSize": 12000000,
            "qualityFactor": 7000,
            "maxVideoFrameSize": 589824,
            "params" : {
                "Q_JPEGENC_CMP_CAPTURE_INTERVAL": 90000
            }
        }

"jpegenc0" is the instance name. Other instances can be named "jpegenc1", "jpegenc2", etc.
Though any instance name can be used, this is the recommended scheme.

Here is the description of the variables outside the "params" sub-object:

Name                Description                     Values                      Default (if any)
----                -----------                     ------                      ----------------

type                Object Type                     "jpegenc"                   No default - must be specified

parent              Parent object instance          Instance of type "vcap"     No default - must be specified
                                                    "dewarp" or "nvpp"

channel             Follows mxuvc convention        "CH1", "CH2", etc.          No default - must be specified
                    starting from "CH1". 
                    The actual firmware value will 
                    start from 0.

streamId            ID used for the stream in the   0, 1, etc. Should not       No default - must be specified
                    muxer                           duplicate.

maxWidth            Value used in object creation   Any value up to maximum     No default - must be specified
                    (width) 

maxHeight           Value used in object creation   Any value up to maximum     No default - must be specified
                    (height) 

outputBufferSize    Output buffer size              See typical values in       No default - must be specified
                    for the maximum resolution      codec/mainapp/sysconfig.h:
                    specified                       QVGA: 7500000
                                                    VGA:  1500000
                                                    720p: 3000000
                                                    1080p:6000000
                                                    5M:   12000000

qualityFactor       Quality factor                  Any legal value             Whatever is set in defaultVidCfg.dwCompQuality
                    (multiplied by 100)                                         Currently 5000

maxVideoFrameSize   Maximum frame size              Any legal value             Whatever is set in defaultVidCfg.dwMaxVideoFrameSize
                    in Bytes                                                    Currently 144 * 1024

The variables in capital refer to JPEGEncoderAPI_SetParam() variables of the same name:
For example: 
    If you want to use same frame rate as input, use:
    "Q_JPEGENC_CMP_CAPTURE_INTERVAL": -1

metadatas:
----------

There will be only one instance and it will be defined like:

    "metadatas0": {
            "type": "metadatas",
            "channel": "CH4",
            "streamId": 3,
            "gyroEnable": 1,
            "pollInterval": 20,
            "gyroFifoMode": 1,
            "outputBufferSize": 30000
	},

"metadatas0" is the instance name. Though any instance name can be used, this is the recommended name.

Name                Description                     Values                      Default (if any)
----                -----------                     ------                      ----------------

type                Object Type                     "metadatas                  No default - must be specified

channel             Follows mxuvc convention        "CH1", "CH2", etc.          No default - must be specified
                    starting from "CH1".            Must be last channel
                    The actual firmware value will 
                    start from 0.

streamId            ID used for the stream in the   0, 1, etc. Should not       No default - must be specified
                    muxer                           duplicate.

gyroEnable          Eanble Gyro data as metadatas   0 = Disable, 1= Enable.     Default value 0
                    stream contents.

pollInterval        Sample capture interval time    10-1000                     Default value 20
                    in milli seconds.

gyroFifoMode        Enable disable Fifo Mode        0 = Disable, 1= Enable.     Default value 0
                    operation in Gyro Chip. In
                    non-fifo mode single sample 
                    is read from actual regs 
                    instread of fifo.

outputBufferSize    Output buffer size              300000                      No default - must be specified
                    for the maximum number of      
                    gyro samples.
                    
raw:
----

There will be only one instance and it will be defined like:

        "raw0": {
            "type": "raw",
            "parent": "nvpp3",
            "channel": "CH5",
            "streamId": 4,
            "captureFormat": "GREY",
            "outAnalytics": 1,
            "outRawFrame": 1
        }

"raw0" is the instance name. Though any instance name can be used, this is the recommended name.

Here is the description of the variables:

Name                Description                     Values                      Default (if any)
----                -----------                     ------                      ----------------

type                Object Type                     "raw"                       No default - must be specified

parent              Parent object instance          Instances of type           No default - must be specified
                                                    "nvpp", "vcap" or "dewarp"

channel             Follows mxuvc convention        "CH1", "CH2", etc.          No default - must be specified
                    starting from "CH1".            Must be last channel
                    The actual firmware value will 
                    start from 0.

streamId            ID used for the stream in the   0, 1, etc. Should not       No default - must be specified
                    muxer                           duplicate.


captureFormat       Capture format                  "GREY" or "NV12"            No default - must be specified

outAnalytics        Enable/Disable analytics.       Bit 0: VPP MB stats         0
                    When enabling PME,              Bit 1: PME vectors
                    also set the relevant           Bit 2: Smart motion
                    nvpp variables                         on xtensa and 
                                                           rectangles sent
                                                           out over USB
                                                    Bit 3: Histogram
                                                    Bit 4: VPP Global Stats
                                                    All combinations possible
                                                    Individual values:
                                                      Bit 0         (1)
                                                      Bit 1         (2)
                                                      Bit 2         (4)
                                                      Bit 3         (8)
                                                      Bit 4         (16)
                                                    Use bitwise OR for
                                                    combinations

outRawFrame       Enable/Disable Raw frame.         0/1                         1
                  When we just want to send
                  stats but no raw frames then
                  this field should be zero.


audfil:
------

There will be only one instance of audio filter object and it will be defined like

	"audfil0": {
            "type": "audfil",
            "asp": {
                 "type": "aspfil",
                 "asp": 1,
                 "agc": 1,
                 "fsa": 0,
                 "fir": 0,
                 "ns_level": 20,
                 "nr": 1
             },
            "params" : {
                "Q_AUDFLTR_CMP_ASP_AGC_PEAK_THRESHOLD"       : -2,
                "Q_AUDFLTR_CMP_ASP_AGC_AVERAGE_TARGET"       : -9,
                "Q_AUDFLTR_CMP_ASP_AGC_FASTUPDATE_RATE"      : 50,
                "Q_AUDFLTR_CMP_ASP_AGC_FASTUPDATE_THRESHOLD" : 4,
                "Q_AUDFLTR_CMP_ASP_AGC_FASTUPDATE_GAIN_STEP" : 1,
                "Q_AUDFLTR_CMP_ASP_AGC_SLOWUPDATE_RATE"      : 200,
                "Q_AUDFLTR_CMP_ASP_AGC_SLOWUPDATE_THRESHOLD" : 2,
                "Q_AUDFLTR_CMP_ASP_AGC_SLOWUPDATE_GAIN_STEP" : 1,
                "Q_AUDFLTR_CMP_ASP_AGC_CLIP_GAIN_STEP"       : 4,
                "Q_AUDFLTR_CMP_ASP_AGC_STABILISE_FRAMES"     : 100,
            }
        },

audfil0  is the instance name. Though any instance name can be used, this is the recommended name.

Here is the description of the variables:

Name                Description                     Values                      Default (if any)
----                -----------                     ------                      ----------------

type                Object Type                     "audfil"                    No default - must be specified

subfilter           Kind of the filter              "asp"                       No default - must be specified
                    under the object 
                    of audio filter

type                subfilter object type           "aspfil"                    No default - must be specified


subfilter params    enable/disable each              
  ----              component of asp subfilter

                   
asp                 enable/disable asp filter       1                           1 default [ 0 for disable]
agc                 enable/disable agc filter       1                           0 default [ 1 for enable ] 
fsa                 enable/disable fsa filter       0                           0 default [ 1 for enable ] 
fir                 enable/disable fir filter       0                           0 default [ 1 for enable ]
nr                  enable/disable nr  filter       0                           0 default [ 1 for enable ] 
ns_level            set the noise suppression
                    level                           20                          20 default
aec                 enable/disable aec filter       0                           0 default [ 1 for enable ]
detector            enable/disable activity         1                           0 default [ 1 for enable ]
                    detector in asp                 


The variables inside "params" refer to AudioFilterAPI_SetParam() variables of the same name.
See codec/middleware/qaudfltr.h for details of the variables. Any variable from there can be added here 
without any change in app_json.cpp 


audenc:
------

       "audenc0": {
            "type": "audenc",
            "audenc_type": "aac",
            "bitrate": 32000,
            "params": {
                 "Q_AUDENC_CMP_QAC_AUDIO_OBJECT_TYPE" : 2
            }
      }

audenc0 is the instance name. Though any instance name can be used this is the recommended name.

Here is the description of the variables:

Name                Description                     Values                      Default (if any)
----                -----------                     ------                      ----------------

type                Object Type                     "audenc"                    No default - must be specified

audenc_type         Type of the audio encoder       "aac", "opus"               Default is AAC encoder [ "opus" is for OPUS encoder ]
                    to be used 

bitrate             Encoding bitrate in bits/sec    32000 to 128000             Default 32000
     
The variables inside "params" refer to AudioEncoderAPI_SetParam() variables of the same name.
See codec/middleware/qaudenc.h for details of the variables. Any variable from there can be added here 
without any change in app_json.cpp 

Q PARAMETERS ( optional )
------------

Q_AUDENC_CMP_FRAME_INTERVAL:
   This parameter sets the opus enoder frame interval in milliseconds. For example, if Q_AUDENC_CMP_FRAME_INTERVAL = 60, then opus encoder encodes the audio frame of size 60ms worth samples. The supported intervals are 20, 40 or 60. Default interval is 20ms. This qparam is not applicable for AAC enocder.

 
 
 
