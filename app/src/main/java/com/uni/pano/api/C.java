package com.uni.pano.api;

/**
 * Created by baixiaokang on 16/4/23.
 */
public class C {
    //==================API============//
    public static final String clientOS = "android";
    public static final String clientVersion = "1.0.0.100";
    public static final String clientVersionCode = "100";
    public static final String BASE_URL = "http://api.uniunivr.com/";
    //public static final String BASE_URL = "http://192.168.3.102:8080/api/";

    public static final String SHARE_URL = "http://app.uniunivr.com/?shareId=%d&timestamp=%d&content=%s";
}
