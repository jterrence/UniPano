package com.uni.pano.api;



import com.uni.pano.entity.Result;
import com.uni.pano.entity.ShareInfo;
import com.uni.pano.entity.Token;

import java.io.File;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;


/**
 * Created by Administrator on 2016/3/23.
 */
public interface ApiService {


    //@POST("share/token?shareType={shareType}&suffix={suffix}")
    //Call<ResponseBody> getToken(@Path("shareType") String shareType, @Path("suffix") String suffix);
    //Call<ResponseBody> getToken(@Part("shareType") RequestBody shareType, @Part("suffix") RequestBody suffix);
    @FormUrlEncoded
    @POST("share/token")
    Call<Result<Token>> getToken(@Field("shareType") String shareType, @Field("suffix") String suffix);
//    @FormUrlEncoded
//    @POST("share/addShare")
//    Call<ResponseBody> addShare(@Field("shareId") int shareId, @Field("shareType") String shareType, @Field("fileName") String fileName, @Field("content") String content);

    @Multipart
    @POST("share/addShare")
    Call<Result<ShareInfo>> addShare(@Part MultipartBody.Part file,  @Part("content") RequestBody content);

    //
}
