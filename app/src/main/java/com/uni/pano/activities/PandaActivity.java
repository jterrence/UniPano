package com.uni.pano.activities;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lwm.shapeimageview.CircularImageView;
import com.uni.pano.R;
import com.uni.pano.base.BaseActivity;
import com.uni.pano.fragment.DeleteDialogFragment;
import com.uni.pano.fragment.GalleryFragment;
import com.uni.pano.fragment.HomeFragment;
import com.uni.pano.fragment.OtherFilesDialogFragment;
import com.uni.pano.fragment.SetFragment;
import com.uni.pano.logutils.LogcatHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.OnClick;

public class PandaActivity extends BaseActivity {

    @BindView(R.id.iv_mul_choose)
    ImageView iv_mul_choose;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.iv_open_folder)
    ImageView iv_open_folder;
    @BindView(R.id.iv_camera)
    CircularImageView iv_camera;
    @BindView(R.id.tv_home)
    TextView tv_home;
    @BindView(R.id.tv_gallery)
    TextView tv_gallery;
    @BindView(R.id.tv_set)
    TextView tv_set;
    @BindView(R.id.ll_gallery_top)
    LinearLayout ll_gallery_top;
    @BindView(R.id.tv_cancel_select)
    TextView tv_cancel_select;
    @BindView(R.id.tv_select_all)
    TextView tv_select_all;
    @BindView(R.id.ll_gallery_bottom)
    LinearLayout ll_gallery_bottom;
    @BindView(R.id.tv_share)
    TextView tv_share;
    @BindView(R.id.tv_delete)
    TextView tv_delete;

    HomeFragment fm_home;
    GalleryFragment fm_gallery;
    SetFragment fm_set;

    public enum WhichView {
        HOME,GALLERY,SET
    };
    private WhichView mWhichFragment            =   WhichView.GALLERY;
//    private HomeFragment mHomeFragment          =   new HomeFragment();
//    private GalleryFragment mGalleryFragment    =   new GalleryFragment();
//    private SetFragment mSetFragment            =   new SetFragment();

    public static void startActivity(Activity activity){
        Intent intent = new Intent(activity, PandaActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.av_panda);
        LogcatHelper.getInstance(this).start();
        fm_home = (HomeFragment) getFragmentManager().findFragmentById(R.id.fm_home);
        fm_gallery = (GalleryFragment) getFragmentManager().findFragmentById(R.id.fm_gallery);
        fm_set = (SetFragment) getFragmentManager().findFragmentById(R.id.fm_set);
        updateView(mWhichFragment);
        tv_cancel_select.setText(String.format(getString(R.string.select_number), 0));
        fm_gallery.set_tv_cancel_select(tv_cancel_select);
    }

    private void updateView(WhichView which){
        iv_mul_choose.setVisibility(View.INVISIBLE);
        iv_open_folder.setVisibility(View.INVISIBLE);
        iv_camera.setVisibility(View.INVISIBLE);
        mWhichFragment  =   which;
        switch (which){
            case HOME: {
                FragmentTransaction transaction =   this.getFragmentManager().beginTransaction();
                transaction.show(fm_home).hide(fm_gallery).hide(fm_set).commit();
                tv_title.setText(getString(R.string.daily_number));
                iv_camera.setVisibility(View.VISIBLE);
                Drawable drawableHome   =   getDrawable(R.drawable.navi_home_press);
                drawableHome.setBounds(0, 0, drawableHome.getMinimumWidth(), drawableHome.getMinimumHeight());
                tv_home.setCompoundDrawables(null, drawableHome, null, null);
                //tv_home.setAlpha(1.0f);
                Drawable drawableGallery   =   getDrawable(R.drawable.navi_gallery);
                drawableGallery.setBounds(0, 0, drawableGallery.getMinimumWidth(), drawableGallery.getMinimumHeight());
                tv_gallery.setCompoundDrawables(null, drawableGallery, null, null);
                //tv_gallery.setAlpha(0.7f);
                Drawable drawableSet   =   getDrawable(R.drawable.navi_set);
                drawableSet.setBounds(0, 0, drawableSet.getMinimumWidth(), drawableSet.getMinimumHeight());
                tv_set.setCompoundDrawables(null, drawableSet, null, null);
                //tv_set.setAlpha(0.7f);
                tv_home.setEnabled(false);
                tv_gallery.setEnabled(true);
                tv_set.setEnabled(true);
            }
                break;
            case GALLERY: {
                FragmentTransaction transaction =   this.getFragmentManager().beginTransaction();
                transaction.hide(fm_home).show(fm_gallery).hide(fm_set).commit();
                tv_title.setText(getString(R.string.album));
                iv_mul_choose.setVisibility(View.VISIBLE);
                iv_open_folder.setVisibility(View.VISIBLE);
                iv_camera.setVisibility(View.VISIBLE);
                Drawable drawableHome   =   getDrawable(R.drawable.navi_home);
                drawableHome.setBounds(0, 0, drawableHome.getMinimumWidth(), drawableHome.getMinimumHeight());
                tv_home.setCompoundDrawables(null, drawableHome, null, null);
                //tv_home.setAlpha(0.7f);
                Drawable drawableGallery   =   getDrawable(R.drawable.navi_gallery_press);
                drawableGallery.setBounds(0, 0, drawableGallery.getMinimumWidth(), drawableGallery.getMinimumHeight());
                tv_gallery.setCompoundDrawables(null, drawableGallery, null, null);
                //tv_gallery.setAlpha(1.0f);
                Drawable drawableSet   =   getDrawable(R.drawable.navi_set);
                drawableSet.setBounds(0, 0, drawableSet.getMinimumWidth(), drawableSet.getMinimumHeight());
                tv_set.setCompoundDrawables(null, drawableSet, null, null);
                //tv_set.setAlpha(0.7f);
                tv_home.setEnabled(true);
                tv_gallery.setEnabled(false);
                tv_set.setEnabled(true);
            }
                break;
            case SET: {
                FragmentTransaction transaction =   this.getFragmentManager().beginTransaction();
                transaction.hide(fm_home).hide(fm_gallery).show(fm_set).commit();
                tv_title.setText(getString(R.string.settings));
                Drawable drawableHome   =   getDrawable(R.drawable.navi_home);
                drawableHome.setBounds(0, 0, drawableHome.getMinimumWidth(), drawableHome.getMinimumHeight());
                tv_home.setCompoundDrawables(null, drawableHome, null, null);
               // tv_home.setAlpha(0.7f);
                Drawable drawableGallery   =   getDrawable(R.drawable.navi_gallery);
                drawableGallery.setBounds(0, 0, drawableGallery.getMinimumWidth(), drawableGallery.getMinimumHeight());
                tv_gallery.setCompoundDrawables(null, drawableGallery, null, null);
                //tv_gallery.setAlpha(0.7f);
                Drawable drawableSet   =   getDrawable(R.drawable.navi_set_press);
                drawableSet.setBounds(0, 0, drawableSet.getMinimumWidth(), drawableSet.getMinimumHeight());
                tv_set.setCompoundDrawables(null, drawableSet, null, null);
                //tv_set.setAlpha(1.0f);
                tv_home.setEnabled(true);
                tv_gallery.setEnabled(true);
                tv_set.setEnabled(false);
            }
                break;
        }
    }

    @OnClick({R.id.tv_home, R.id.tv_gallery, R.id.tv_set, R.id.iv_camera, R.id.iv_mul_choose, R.id.tv_cancel_select, R.id.iv_open_folder, R.id.tv_select_all, R.id.tv_share, R.id.tv_delete})
    public void onAlbumAndFilterClick(View view) {
        switch (view.getId()){
            case R.id.tv_home: {
                updateView(WhichView.HOME);
            }
                break;
            case R.id.tv_gallery:
                updateView(WhichView.GALLERY);
                break;
            case R.id.tv_set:
                updateView(WhichView.SET);
                break;
            case R.id.iv_camera:
                PanoramaCameraActivity.startActivity(this);
                break;
            case R.id.iv_mul_choose:
                ll_gallery_bottom.setVisibility(View.VISIBLE);
                ll_gallery_top.setVisibility(View.VISIBLE);
                break;
            case R.id.tv_cancel_select:
                fm_gallery.cancelSelect();
                cancelSelect();
                break;
            case R.id.iv_open_folder:{
                OtherFilesDialogFragment.show(getFragmentManager());
            }
                break;
            case R.id.tv_select_all:
                String text =  tv_select_all.getText().toString();
                if (text.endsWith(getString(R.string.select_all))){
                    fm_gallery.selectAll();
                    tv_select_all.setText(getString(R.string.cancel_selection));
                }else{
                    fm_gallery.cancelSelect();
                    tv_select_all.setText(getString(R.string.select_all));
                }
                break;
            case R.id.tv_share:
                break;
            case R.id.tv_delete:
                int select_number = fm_gallery.getSelectedNumber();
                if (select_number>0) {
                    DeleteDialogFragment.show(select_number, new DeleteDialogFragment.DeleteListener() {
                        @Override
                        public void onYes() {
                            cancelSelect();
                            fm_gallery.deleteSelected();
                        }
                    }, getFragmentManager());
                }
                break;
            default:
                break;
        }
    }
    public void cancelSelect(){
        ll_gallery_bottom.setVisibility(View.INVISIBLE);
        ll_gallery_top.setVisibility(View.INVISIBLE);
        tv_select_all.setText(getString(R.string.select_all));
    }
}