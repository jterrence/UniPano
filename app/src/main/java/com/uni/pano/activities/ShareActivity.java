package com.uni.pano.activities;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.qiniu.android.common.Zone;
import com.qiniu.android.storage.Configuration;
import com.qiniu.android.storage.UpCancellationSignal;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UpProgressHandler;
import com.qiniu.android.storage.UploadManager;
import com.qiniu.android.storage.UploadOptions;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.media.UMWeb;
import com.uni.common.util.BitmapUtil;
import com.uni.common.util.NetUtil;
import com.uni.pano.MainApplication;
import com.uni.pano.R;
import com.uni.pano.api.ApiClient;
import com.uni.pano.api.ApiService;
import com.uni.pano.api.C;
import com.uni.pano.api.FileRequestBody;
import com.uni.pano.api.RetrofitCallback;
import com.uni.pano.base.BaseActivity;
import com.uni.pano.bean.MediaInfo;
import com.uni.pano.entity.Result;
import com.uni.pano.entity.ShareInfo;
import com.uni.pano.entity.Token;
import com.uni.pano.logutils.LogcatHelper;
import com.uni.pano.widget.CToast;
import com.qiniu.android.http.ResponseInfo;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.ByteBuffer;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.uni.pano.api.C.SHARE_URL;
public class ShareActivity extends BaseActivity {
    public static final String TAG = ShareActivity.class.getSimpleName();
    @BindView(R.id.share_tips_text)
    TextView share_tips_text;
    @BindView(R.id.share_text_edit)
    EditText share_text_edit;
    @BindView(R.id.share_character_counter_text)
    TextView share_character_counter_text;
    @BindView(R.id.upload_progress)
    ProgressBar upload_progress;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.iv_back)
    ImageView iv_back;

    private MediaInfo mMediaInfo;
    private SHARE_MEDIA mShareMedia;
    private Result<Token> mToken;
    private ApiService mApiService;
    private UploadManager mUploadManager;
    private boolean mCancelUpload   =   false;
    Result<ShareInfo> shareInfoResult = null;
    /* 分享回调 */
    private UMShareListener umShareListener = new UMShareListener() {
        @Override
        public void onStart(SHARE_MEDIA share_media) {

        }

        @Override
        public void onResult(SHARE_MEDIA share_media) {
            CToast.showToast(R.string.share_tips_success_share);
            finish();
        }
        @Override
        public void onError(SHARE_MEDIA share_media, Throwable throwable) {
            CToast.showToast(R.string.share_tips_error_share);
            finish();
        }
        @Override
        public void onCancel(SHARE_MEDIA share_media) {
            CToast.showToast(R.string.cancel);
            finish();
        }
    };

    public static void startActivity(Activity activity, SHARE_MEDIA share_media, MediaInfo mediaInfo){
        Intent intent = new Intent(activity, ShareActivity.class);
        intent.putExtra(SHARE_MEDIA.class.getSimpleName(), share_media);
        intent.putExtra(MediaInfo.TAG, mediaInfo);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.av_share);
        initShare();
        //LogcatHelper.getInstance(this).ic_splash_plus_dot();
        mApiService = ApiClient.retrofit().create(ApiService.class);
        Intent intent = getIntent();
        // 取出Intent传递过来的
        mShareMedia = (SHARE_MEDIA) intent.getSerializableExtra(SHARE_MEDIA.class.getSimpleName());
        mMediaInfo = (MediaInfo)intent.getSerializableExtra(MediaInfo.class.getSimpleName());
        tv_title.setText(R.string.share);
        upload_progress.setEnabled(false);
        // 监听输入字数
        share_text_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                share_character_counter_text.setText(s.length() + "/30");
            }
        });

        RetrofitCallback<Result<ShareInfo>> callback = new RetrofitCallback<Result<ShareInfo>>() {
            @Override
            public void onSuccess(Call<Result<ShareInfo>> call, Response<Result<ShareInfo>> response) {
                shareInfoResult = response.body();
                upload_progress.setEnabled(true);
            }
            @Override
            public void onFailure(Call<Result<ShareInfo>> call, Throwable t) {
                upload_progress.setEnabled(false);
//进度更新结束
            }
            @Override
            public void onLoading(long total, long progress) {
                upload_progress.setProgress((int)(progress*100/total));
                super.onLoading(total, progress);

//此处进行进度更新
            }
        };
        File file = new File(mMediaInfo.filePath);
        RequestBody body1 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
//通过该行代码将RequestBody转换成特定的FileRequestBody
        FileRequestBody body = new FileRequestBody(body1, callback);
        MultipartBody.Part part = MultipartBody.Part.createFormData("file", file.getName(), body);
        Call<Result<ShareInfo>> call = mApiService.addShare(part, RequestBody.create(MediaType.parse("multipart/form-data"),"test2"));
        call.enqueue(callback);
    }
    private void initShare() {
        // 分享平台密钥
        final String WX_APPID = "wx1992016308339b32";
        final String WX_APPSECRET = "a91a858c645ce1c209e9797d5810b773";

        final String QQ_APPID = "1105924103";
        final String QQ_APPSECRET = "tKBdYy1Vvp9zPBib";

        PlatformConfig.setWeixin(WX_APPID, WX_APPSECRET);
        PlatformConfig.setQQZone(QQ_APPID, QQ_APPSECRET);

        UMShareAPI.get(this);
    }
    @Override
    protected void onResume() {
        super.onResume();
    }
    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        mCancelUpload   =   true;
        super.onDestroy();
    }


    @OnClick({R.id.upload_progress, R.id.iv_back})
    public void doClick(View v) {
        switch (v.getId()){
            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.upload_progress:
                if(upload_progress.getProgress() == 100 && shareInfoResult!=null) {
                    ShareAction shareAction = new ShareAction(this);
                    String shareText    =    share_text_edit.getText().toString();
                    if (shareText.isEmpty()){
                        shareText = " ";
                    }
                    shareAction.withText(shareText);
                    try {
                        shareText = URLEncoder.encode(shareText, "utf-8");
                    }catch (Exception e){

                    }

                    // 基于URL的分享
                    UMImage image = new UMImage(getApplicationContext(), shareInfoResult.data.thumbUrl);
                    //image.setTitle(getString(R.string.share_title_default));
                    UMWeb umWeb = new UMWeb(shareInfoResult.data.shareUrl);
                    umWeb.setTitle(getString(R.string.share_title_default));
                    umWeb.setThumb(image);
                    umWeb.setDescription(shareText);
                    shareAction.withMedia(umWeb);
                    shareAction.setPlatform(mShareMedia).setCallback(umShareListener).share();
                }else{
                    CToast.showToast("请等待上传完成!");
                }
                break;
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }
}