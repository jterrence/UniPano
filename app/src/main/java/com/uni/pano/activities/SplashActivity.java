/*
 * Copyright (C) 2016 Nico(duyouhua1214@163.com), Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.uni.pano.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.uni.common.util.LanguageUtil;
import com.uni.common.util.WindowUtil;
import com.uni.pano.R;
import com.uni.pano.base.BaseActivity;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class SplashActivity extends BaseActivity {

    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.av_splash);
        gotoMainDelayed();
    }
    private void gotoMainDelayed() {
        mHandler.postDelayed(new Runnable() {

            public void run() {
                PandaActivity.startActivity(SplashActivity.this);
                 finish();
            }
        }, 500);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}