package com.uni.pano.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.uni.common.util.LanguageUtil;
import com.uni.common.util.PreferenceModel;
import com.uni.pano.R;
import com.uni.pano.activities.PandaActivity;
import com.uni.pano.base.BaseFragment;

import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by DELL on 2017/2/16.
 */

public class SetFragment extends BaseFragment {
    public final static String TAG = SetFragment.class.getSimpleName();
    @BindView(R.id.tv_language)
    TextView tv_language;
    @BindView(R.id.cb_logo)
    CheckBox cb_logo;

    @Override
    protected void initUI(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.fm_set);
        String language = getResources().getConfiguration().locale.getLanguage();
        String saveLanguage = PreferenceModel.getString(LanguageUtil.class.getSimpleName(), language);
        tv_language.setText(LanguageUtil.getDisplayLanguage(new Locale(saveLanguage)));
//        boolean showLogo = PreferenceModel.getBoolean("SHOW_LOGO_IN_TAKE_PHOTO", false);
//        cb_logo.setChecked(showLogo);
    }

    @OnClick({R.id.rl_language, R.id.rl_logo, R.id.cb_logo})
    public void onAlbumAndFilterClick(View view) {
        switch (view.getId()) {
            case R.id.rl_language: {
                LanguageDialogFragment.show(getFragmentManager());
            }
            break;
            case R.id.rl_logo:{
                LogoDialogFragment.show(getFragmentManager());
            }
            break;
            case R.id.cb_logo:{
                //PreferenceModel.putBoolean("SHOW_LOGO_IN_TAKE_PHOTO", cb_logo.isChecked());
            }
            break;
        }
    }

}
