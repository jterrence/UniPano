package com.uni.pano.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import com.uni.pano.R;
import com.uni.pano.base.BaseFragment;

/**
 * Created by DELL on 2017/2/24.
 */

public class HomeFragment extends BaseFragment {
    public final static String TAG = HomeFragment.class.getSimpleName();
    @Override
    protected void initUI(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.fm_home);
    }
    @Override
    public void onResume() {
        super.onResume();
    }
    @Override
    public void onPause() {
        super.onPause();
    }
}