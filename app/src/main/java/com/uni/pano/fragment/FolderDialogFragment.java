package com.uni.pano.fragment;

import android.app.FragmentManager;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.umeng.socialize.utils.Log;
import com.uni.common.config.PathConfig;
import com.uni.common.util.DateTimeUtil;
import com.uni.common.util.FileSizeUtil;
import com.uni.pano.MainApplication;
import com.uni.pano.R;
import com.uni.pano.activities.PanoramaPlayActivity;
import com.uni.pano.base.BaseDialogFragment;
import com.uni.pano.bean.FolderInfo;
import com.uni.pano.bean.MediaInfo;
import com.uni.pano.event.CreateFileEvent;
import com.uni.pano.event.FolderMediaInfoListUpdateEvent;
import com.uni.pano.utils.CommonUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import butterknife.BindView;
import butterknife.OnClick;

public class FolderDialogFragment extends BaseDialogFragment {
    public final static String TAG = FolderDialogFragment.class.getSimpleName();
    @BindView(R.id.srl_media)
    SwipeRefreshLayout srl_media;
    @BindView(R.id.rv_media)
    RecyclerView rv_media;
    @BindView(R.id.tv_back)
    TextView tv_back;

    FolderAdapter folderAdapter;
    boolean mUseGlide   =   true;
    LinearLayoutManager linearLayoutManager;

    List<MediaInfo> mediaInfoList = new ArrayList<>();
    FolderInfo folderInfo;
    public static void show(FolderInfo folderInfo, FragmentManager fragmentManager){
        FolderDialogFragment folderDialogFragment = new FolderDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(FolderInfo.TAG, folderInfo);
        folderDialogFragment.setArguments(bundle);
        folderDialogFragment.show(fragmentManager, FolderDialogFragment.TAG);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    protected void initView() {
        folderInfo = (FolderInfo) this.getArguments().getSerializable(FolderInfo.TAG);
        tv_back.setText(folderInfo.name);
        linearLayoutManager    =   new LinearLayoutManager(this.getActivity(), LinearLayoutManager.VERTICAL, false);
        rv_media.setLayoutManager(linearLayoutManager);
        rv_media.setHasFixedSize(true);
        rv_media.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                super.onDraw(c, parent, state);
            }
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                outRect.set(40, 0, 40, 20);
            }
        });
        rv_media.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                mUseGlide   =   true;
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        // 这句话是为了，第一次进入页面的时候显示加载进度条
        srl_media.setProgressViewOffset(false, 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources().getDisplayMetrics()));
        srl_media.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                srl_media.setRefreshing(false);
                //requestMediaInfoList(FolderDialogFragment.this.getActivity());
            }
        });
        folderAdapter = new FolderAdapter();
        rv_media.setAdapter(folderAdapter);
        requestMediaInfoList();
    }
    @OnClick({R.id.tv_back})
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_back:
                dismiss();
                break;
            default:
                break;
        }
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreateView(inflater, container, savedInstanceState);
        viewInject(inflater, container, R.layout.dfm_folder);
        return mView;
    }

    private void requestMediaInfoList(){
        srl_media.setRefreshing(true);
        MediaScannerConnection.scanFile(MainApplication.getInstance(), new String[] { folderInfo.filePath },
                null, new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(final String path, Uri uri) {
                        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
                        singleThreadExecutor.execute(new Runnable() {
                            @Override
                            public void run() {
                                List<MediaInfo> mediaInfoList = new ArrayList<MediaInfo>();
                                if (path!=null && !path.equals("")) {
                                    CommonUtil.listMediaInfo(MainApplication.getInstance(), mediaInfoList, path, 320);
                                }else{
                                    CommonUtil.listAllPanoramaMediaInfo(MainApplication.getInstance(), mediaInfoList, 320);
                                }
                                if (mediaInfoList.size()>2){
                                    //将map.entrySet()转换成list
                                    Collections.sort(mediaInfoList, new MediaInfo.SortComparator());
                                }
                                EventBus.getDefault().post(new FolderMediaInfoListUpdateEvent(mediaInfoList));
                            }
                        });
                        singleThreadExecutor.shutdown();
                    }
                });
    }

    class FolderAdapter extends RecyclerView.Adapter<FolderAdapter.GalleryViewHolder> {
        @Override
        public GalleryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            GalleryViewHolder holder = new GalleryViewHolder(LayoutInflater.from(FolderDialogFragment.this.getActivity()).inflate(R.layout.item_media, parent, false));
            return holder;
        }
        @Override
        public void onBindViewHolder(final GalleryViewHolder holder, final int position) {
            MediaInfo mediaInfo =   mediaInfoList.get(position);
            holder.view.setTag(mediaInfo);
            holder.tv_name.setText(mediaInfo.name);
            holder.tv_date.setText(DateTimeUtil.getModifiedDate(mediaInfo.lastModified));
            holder.tv_size.setText(FileSizeUtil.convertFileSize(mediaInfo.length, 2));
            switch (mediaInfo.type) {
                case MP4:
                    holder.tv_time.setVisibility(View.VISIBLE);
                    try {
                        holder.tv_time.setText(DateTimeUtil.getFormatTime(mediaInfo.duration/1000));
                        Glide.with(FolderDialogFragment.this.getActivity()).load(mediaInfo.filePath).crossFade(0).placeholder(R.drawable.ic_photo_default).into(holder.iv_media);
                    }catch (Exception e) {
                        Log.e("game", e.getMessage());
                        Glide.with(FolderDialogFragment.this.getActivity()).load(R.drawable.ic_photo_default).crossFade(0).placeholder(R.drawable.ic_photo_default).into(holder.iv_media);
                    }
                    break;
                default:
                    holder.tv_time.setVisibility(View.GONE);
                    Glide.with(FolderDialogFragment.this.getActivity()).load(mediaInfo.filePath).crossFade(0).placeholder(R.drawable.ic_photo_default).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.iv_media);
                    break;
            }
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MediaInfo mediaInfo1 = (MediaInfo)v.getTag();
                    PanoramaPlayActivity.startActivity(FolderDialogFragment.this.getActivity(), mediaInfo1);
                }
            });
        }
        @Override
        public int getItemCount() {
            return mediaInfoList.size();
        }
        class GalleryViewHolder extends RecyclerView.ViewHolder {
            View view;
            ImageView iv_media;
            TextView tv_name;
            TextView tv_size;
            TextView tv_time;
            TextView tv_date;
            public GalleryViewHolder(View view) {
                super(view);
                iv_media = (ImageView) view.findViewById(R.id.iv_media);
                tv_name = (TextView) view.findViewById(R.id.tv_name);
                tv_size = (TextView) view.findViewById(R.id.tv_size);
                tv_time = (TextView) view.findViewById(R.id.tv_time);
                tv_date = (TextView) view.findViewById(R.id.tv_date);
                this.view = view;
            }
        }
    }
   @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCreateFile(CreateFileEvent createFileEvent) {
        String pathName = createFileEvent.fileName;
        File file = new File(pathName);
        if (file.getParent().equals(folderInfo.filePath)) {
            MediaInfo mediaInfo = new MediaInfo(file);
            if (mediaInfo.valid) {
                mediaInfoList.add(0, mediaInfo);
                folderAdapter.notifyItemInserted(0);
            }
        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFolderMediaInfoListUpdateEvent(FolderMediaInfoListUpdateEvent folderMediaInfoListUpdateEvent) {
        this.mediaInfoList = folderMediaInfoListUpdateEvent.mediaInfoList;
        folderAdapter.notifyDataSetChanged();
        srl_media.setRefreshing(false);
    }
}
