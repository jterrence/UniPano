package com.uni.pano.fragment;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.uni.common.config.PathConfig;
import com.uni.common.util.LanguageUtil;
import com.uni.common.util.PreferenceModel;
import com.uni.pano.MainApplication;
import com.uni.pano.R;
import com.uni.pano.activities.SplashActivity;
import com.uni.pano.base.BaseDialogFragment;
import com.uni.pano.bean.FolderInfo;
import com.uni.pano.bean.MediaInfo;
import com.uni.pano.event.OtherFilesUpdateEvent;
import com.uni.pano.utils.CommonUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import butterknife.BindView;
import butterknife.OnClick;

public class OtherFilesDialogFragment extends BaseDialogFragment {
    public final static String TAG = OtherFilesDialogFragment.class.getSimpleName();
    @BindView(R.id.srl_folder)
    SwipeRefreshLayout srl_folder;
    @BindView(R.id.rv_folder)
    RecyclerView rv_folder;

    OtherFilesAdapter otherFilesAdapter;
    boolean mUseGlide   =   true;
    LinearLayoutManager linearLayoutManager;
    
    private Map<String,FolderInfo> folderInfoMap = new HashMap<String, FolderInfo>();

    public static void show(FragmentManager fragmentManager){
        OtherFilesDialogFragment otherFilesDialogFragment = new OtherFilesDialogFragment();
        otherFilesDialogFragment.show(fragmentManager, OtherFilesDialogFragment.TAG);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    protected void initView() {
        linearLayoutManager    =   new LinearLayoutManager(this.getActivity(), LinearLayoutManager.VERTICAL, false);
        rv_folder.setLayoutManager(linearLayoutManager);
        rv_folder.setHasFixedSize(true);
        rv_folder.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                super.onDraw(c, parent, state);
            }
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                outRect.set(40, 0, 40, 20);
            }
        });
        rv_folder.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                mUseGlide   =   true;
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        // 这句话是为了，第一次进入页面的时候显示加载进度条
        srl_folder.setProgressViewOffset(false, 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources().getDisplayMetrics()));
        srl_folder.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                srl_folder.setRefreshing(false);
            }
        });
        otherFilesAdapter = new OtherFilesAdapter();
        rv_folder.setAdapter(otherFilesAdapter);
        requestMediaInfoList();
    }
    @OnClick({R.id.tv_back,R.id.rl_all})
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_back:
                dismiss();
                break;
            case R.id.rl_all:
                FolderInfo folderInfo = new FolderInfo(getString(R.string.all_panorama_files), "");
                FolderDialogFragment.show(folderInfo, this.getFragmentManager());
                break;
            default:
                break;
        }
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreateView(inflater, container, savedInstanceState);
        viewInject(inflater, container, R.layout.dfm_other);
        return mView;
    }

    private void requestMediaInfoList(){
        srl_folder.setRefreshing(true);
        MediaScannerConnection.scanFile(MainApplication.getInstance(), new String[] { PathConfig.getMediaFolder() },
                null, new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
                        singleThreadExecutor.execute(new Runnable() {
                            @Override
                            public void run() {
                                Map<String, FolderInfo> folderInfoMap1 = new HashMap<String, FolderInfo>();
                                CommonUtil.listFolderInfo(MainApplication.getInstance(), folderInfoMap1, 320);
                                EventBus.getDefault().post(new OtherFilesUpdateEvent(folderInfoMap1));
                            }
                        });
                        singleThreadExecutor.shutdown();
                    }
                });
    }

    class OtherFilesAdapter extends RecyclerView.Adapter<OtherFilesAdapter.GalleryViewHolder> {
        @Override
        public GalleryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            GalleryViewHolder holder = new GalleryViewHolder(LayoutInflater.from(OtherFilesDialogFragment.this.getActivity()).inflate(R.layout.item_folder, parent, false));
            return holder;
        }
        @Override
        public void onBindViewHolder(final GalleryViewHolder holder, final int position) {
            String[] keys = folderInfoMap.keySet().toArray(new String[0]);
            FolderInfo folderInfo =   folderInfoMap.get(keys[position]);
            holder.tv_folder.setText(folderInfo.name);
            holder.tv_files.setText(String.format(getString(R.string.files), folderInfo.size));
            holder.view.setTag(folderInfo);
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FolderInfo folderInfo1 = (FolderInfo)v.getTag();
                    FolderDialogFragment.show(folderInfo1, OtherFilesDialogFragment.this.getFragmentManager());
                }
            });
        }
        @Override
        public int getItemCount() {
            return folderInfoMap.size();
        }
        class GalleryViewHolder extends RecyclerView.ViewHolder {
            View view;
            TextView tv_folder;
            TextView tv_files;
            public GalleryViewHolder(View view) {
                super(view);
                tv_folder = (TextView) view.findViewById(R.id.tv_folder);
                tv_files = (TextView) view.findViewById(R.id.tv_files);
                this.view = view;
            }
        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOtherFilesUpdateEvent(OtherFilesUpdateEvent otherFilesUpdateEvent) {
        this.folderInfoMap = otherFilesUpdateEvent.folderInfoMap;
        otherFilesAdapter.notifyDataSetChanged();
        srl_folder.setRefreshing(false);
    }
}
