package com.uni.vr.encoder;

import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.util.Log;
import android.view.Surface;

import com.uni.pano.bean.LogoInfo;
import com.uni.pano.config.EnumElement;

import java.nio.ByteBuffer;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;

/**
 * Created by DELL on 2017/4/14.
 */

public class VideoMediaEncoder extends MediaEncoder {
    private static final int FRAME_RATE = 30;               // 30fps
    public static final int IFRAME_INTERVAL = 1;           // 5 seconds between I-frames
    public static final int FILTER_FRAME = FRAME_RATE*IFRAME_INTERVAL;
    private volatile long encodeNumber = 0;
    private int videoWidth;
    private int videoHeight;
    private int bitRate;
    private EGLDisplay mEglDisplay;
    private EGLContext mEglContext;
    private EGLConfig mEglConfig;
    private EGLSurface mWindowSurface;
    private EGLSurface mEncoderSurface;
    private EGL10 mEgl;
    public VideoMediaEncoder(MediaMuxerWrapper mediaMuxerWrapper, MediaEncoderListener mediaEncoderListener, EGLConfig eglConfig, int videoWidth, int videoHeight, int BIT_RATE){
        super(mediaMuxerWrapper, mediaEncoderListener);
        mEglConfig = eglConfig;
        this.videoWidth = videoWidth;
        this.videoHeight = videoHeight;
        this.bitRate = BIT_RATE;
        mEgl= (EGL10) EGLContext.getEGL();
        mEglContext = mEgl.eglGetCurrentContext();
        mEglDisplay = mEgl.eglGetCurrentDisplay();
        mWindowSurface = mEgl.eglGetCurrentSurface(EGL10.EGL_DRAW);
    }
    public void startRecording(){
        super.startRecording();
        try {
            MediaFormat format = MediaFormat.createVideoFormat(MediaFormat.MIMETYPE_VIDEO_AVC, videoWidth, videoHeight);
            format.setInteger(MediaFormat.KEY_COLOR_FORMAT, MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface);
            format.setInteger(MediaFormat.KEY_BIT_RATE, bitRate);
            format.setInteger(MediaFormat.KEY_FRAME_RATE, FRAME_RATE);
            format.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, IFRAME_INTERVAL);
            bufferInfo = new MediaCodec.BufferInfo();
            mediaCodec = MediaCodec.createEncoderByType(MediaFormat.MIMETYPE_VIDEO_AVC);
            mediaCodec.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
            Surface sf = mediaCodec.createInputSurface();
            mEncoderSurface = createWindowSurface(sf);
            mediaCodec.start();
            encodeNumber = 0;
            mediaEncoderListener.onStartRecording(this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void stopRecording(){
        super.stopRecording();
        Message message = new Message();
        message.msg = EnumElement.ENCODE_MESSAGE.STOP;
        addMsg(message, true);
    }
    @Override
    public void release() {
        super.release();
        if(mEncoderSurface != null) {
            mEgl.eglDestroySurface(mEglDisplay, mEncoderSurface);
            mEncoderSurface =   null;
        }
    }
    public void drainEncoder(boolean endOfStream) {
        if (mediaCodec == null || mediaMuxerWrapperWeakReference == null ) {
            return;
        }
        final MediaMuxerWrapper mediaMuxerWrapper = mediaMuxerWrapperWeakReference.get();
        if (mediaMuxerWrapper == null) {
            return;
        }
        if (endOfStream) {
            mediaCodec.signalEndOfInputStream();
        }
        while (true) {
            int encoderStatus = mediaCodec.dequeueOutputBuffer(bufferInfo, 0);
            if (encoderStatus == MediaCodec.INFO_TRY_AGAIN_LATER) {
                if(!endOfStream) {
                    break;
                }
            } else if (encoderStatus == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                MediaFormat newFormat = mediaCodec.getOutputFormat();
                mTrackIndex = mediaMuxerWrapper.addTrack(newFormat);
                mMuxerStarted = true;
            } else {
                if ((bufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
                    bufferInfo.size = 0;
                }
                if (bufferInfo.size != 0 && mMuxerStarted) {
                    ByteBuffer encoderOutputBuffer = mediaCodec.getOutputBuffer(encoderStatus);
                    encoderOutputBuffer.position(bufferInfo.offset);
                    encoderOutputBuffer.limit(bufferInfo.offset + bufferInfo.size);
                    bufferInfo.presentationTimeUs = getPTSUs();
                    encodeNumber++;
                    mediaMuxerWrapper.writeSampleData(mTrackIndex, encoderOutputBuffer, bufferInfo, encodeNumber);
                    prevOutputPTSUs = bufferInfo.presentationTimeUs;
                }
                mediaCodec.releaseOutputBuffer(encoderStatus, false);
                if ((bufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                    break;
                }
            }
        }
    }
    public EGLSurface createWindowSurface(Surface surface) {
        int [] attrs = {EGL10.EGL_NONE};
        return mEgl.eglCreateWindowSurface(mEglDisplay, mEglConfig, surface, attrs);
    }

    public void makeWindowSurfaceCurrent() {
        mEgl.eglMakeCurrent(mEglDisplay, mWindowSurface, mWindowSurface, mEglContext);
    }

    public  void makeEncoderSurfaceCurrent() {
        mEgl.eglMakeCurrent(mEglDisplay,mEncoderSurface, mEncoderSurface, mEglContext);
    }

    public void swapEncoderSurfaceBuffer() {
        if (mEgl.eglSwapBuffers(mEglDisplay, mEncoderSurface)) {
        }
    }

}
