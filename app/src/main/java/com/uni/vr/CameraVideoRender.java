package com.uni.vr;

import android.graphics.SurfaceTexture;
import android.opengl.EGL14;
import android.opengl.EGLDisplay;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.view.Surface;

import com.umeng.socialize.utils.Log;
import com.uni.common.config.PathConfig;
import com.uni.common.util.FileUtil;
import com.uni.pano.config.EnumElement;
import com.uni.pano.event.RecordTimeEvent;
import com.uni.pano.event.RecordVideoEvent;
import com.uni.pano.event.TakePhotoEvent;
import com.uni.vr.encoder.AudioMediaEncoder;
import com.uni.vr.encoder.MediaEncoder;
import com.uni.vr.encoder.MediaMuxerWrapper;
import com.uni.vr.encoder.VideoMediaEncoder;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by DELL on 2017/3/13.
 */

public class CameraVideoRender implements GLSurfaceView.Renderer, SurfaceTexture.OnFrameAvailableListener{
    private final static String TAG 				= CameraVideoRender.class.getSimpleName();

    private final static long NANOSECONDSPERSECOND = 1000000000L;
    private final static long NANOSECONDSPERMICROSECOND = 1000000;

    private static long sAnimationInterval = (long) (1.0 / 30 * NANOSECONDSPERSECOND);

    // ===========================================================
    // Fields
    // ===========================================================
    private long mLastTickInNanoSeconds;

    private final static int PHOTO_WIDTH        = 2560;
    private final static int PHOTO_HEIGHT       = 1280;
    private final static int H264_WIDTH         = 1920;
    private final static int H264_HEIGHT        = 960;
    private final static int BIT_RATE           = 4000000;
    private SurfaceTexture mSurface               =   null;
    private boolean updateSurface = false;
    private int width;
    private int height;
    private boolean bDestroy = false;
    private MediaMuxerWrapper mediaMuxerWrapper;
    private volatile boolean isVideoRecording = false;
    private EGLConfig eglConfig;
    @Override
    public void onSurfaceCreated(GL10 glUnused, EGLConfig config) {

        EGLDisplay mEGLDisplay = EGL14.eglGetDisplay(EGL14.EGL_DEFAULT_DISPLAY);
        int[] version = new int[2];
        EGL14.eglInitialize(mEGLDisplay, version, 0, version, 1);
        android.opengl.EGLConfig eglConfig = null;
        int[] configsCount = new int[1];
        android.opengl.EGLConfig[] configs = new android.opengl.EGLConfig[1];
        int[] configSpec = new int[]{
                EGL14.EGL_RENDERABLE_TYPE, EGL14.EGL_OPENGL_ES2_BIT,
                EGL14.EGL_RED_SIZE, 8,
                EGL14.EGL_GREEN_SIZE, 8,
                EGL14.EGL_BLUE_SIZE, 8,
                EGL14.EGL_ALPHA_SIZE, 8,
                EGL14.EGL_DEPTH_SIZE, 0,
                EGL14.EGL_STENCIL_SIZE, 0,
                EGL14.EGL_NONE
        };
        if (!EGL14.eglChooseConfig(mEGLDisplay, configSpec, 0, configs, 0, configs.length, configsCount, 0)) {
            throw new IllegalArgumentException("Failed to choose config: " + GLUtils.getEGLErrorString(EGL14.eglGetError()));
        } else if (configsCount[0] > 0) {
            eglConfig = configs[0];
        }
        nativeInitGL(H264_WIDTH, H264_HEIGHT, PHOTO_WIDTH, PHOTO_HEIGHT, this);
        mSurface = new SurfaceTexture(nativeGetTextureId());
        mSurface.setOnFrameAvailableListener(this);

        synchronized(this) {
            updateSurface = false;
        }
        Surface s = new Surface(mSurface);
        nativeSetSurface(s);
        s.release();
        this.eglConfig = config;
    }
    @Override
    public void onSurfaceChanged(GL10 glUnused, int width, int height) {
        if(bDestroy){
            return;
        }
        this.width = width;
        this.height = height;
        nativeOnSurfaceChanged(width, height);
    }
    @Override
    public void onDrawFrame(GL10 glUnused) {
        if(bDestroy){
            return;
        }
        if (sAnimationInterval <= 1.0 / 30 * NANOSECONDSPERSECOND) {
            draw();
        } else {
            final long now = System.nanoTime();
            final long interval = now - this.mLastTickInNanoSeconds;

            if (interval < sAnimationInterval) {
                try {
                    Thread.sleep((sAnimationInterval - interval) / NANOSECONDSPERMICROSECOND);
                } catch (final Exception e) {
                }
            }
            this.mLastTickInNanoSeconds = System.nanoTime();
            draw();
        }
    }
    private void draw(){
        synchronized(this) {
            if (updateSurface) {
                mSurface.updateTexImage();
                updateSurface = false;
            }
        }
        if(isVideoRecording && mediaMuxerWrapper!=null) {
            mediaMuxerWrapper.frameAvailableSoon();
            mediaMuxerWrapper.makeEncoderSurfaceCurrent();
            nativeDrawPanoramaFrame();
            mediaMuxerWrapper.swapEncoderSurfaceBuffer();
            mediaMuxerWrapper.makeWindowSurfaceCurrent();
        }
        nativeDrawFrame();
    }

    public void onDestroy(){
        bDestroy = true;
        if (mediaMuxerWrapper!=null) {
            mediaMuxerWrapper.stopRecording();
            mediaMuxerWrapper = null;
        }
    }
    @Override
    synchronized public void onFrameAvailable(SurfaceTexture surface) {
        updateSurface = true;
    }
    public SurfaceTexture getSurfaceTexture() {
        return mSurface;
    }
    String outFilePath;
    public void startRecord(){
        if(mediaMuxerWrapper == null) {
            outFilePath = PathConfig.getMediaFolder() + File.separator + new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new Date()) + ".mp4";
            mediaMuxerWrapper = new MediaMuxerWrapper(outFilePath, mediaMuxerWrapperListener);
            mediaMuxerWrapper.addEncoder(new VideoMediaEncoder(mediaMuxerWrapper, mediaEncoderListener, eglConfig, H264_WIDTH, H264_HEIGHT, BIT_RATE), new AudioMediaEncoder(mediaMuxerWrapper, mediaEncoderListener));
            mediaMuxerWrapper.startRecording();
        }
    }

    public void stopRecord(){
        if (mediaMuxerWrapper != null && mediaMuxerWrapper.isStarted()) {
            isVideoRecording =   false;
            mediaMuxerWrapper.stopRecording();
        }
    }

    private final MediaMuxerWrapper.MediaMuxerWrapperListener mediaMuxerWrapperListener = new MediaMuxerWrapper.MediaMuxerWrapperListener() {
        @Override
        public void onStarted() {
            EventBus.getDefault().post(new RecordTimeEvent(EnumElement.RECORD_STATE.START));
        }
        @Override
        public void onStopped() {
            EventBus.getDefault().post(new RecordTimeEvent(EnumElement.RECORD_STATE.STOP, outFilePath));
            mediaMuxerWrapper = null;
        }

        @Override
        public void onUpdateTime(long seconds) {
            EventBus.getDefault().post(new RecordTimeEvent(EnumElement.RECORD_STATE.UPDATE_TIME, seconds));
        }
    };

    private final MediaEncoder.MediaEncoderListener mediaEncoderListener = new MediaEncoder.MediaEncoderListener() {
        @Override
        public void onStartRecording(final MediaEncoder encoder) {
            if (encoder instanceof VideoMediaEncoder) {
                isVideoRecording =   true;
            }
        }

        @Override
        public void onStopRecording(final MediaEncoder encoder) {
            if (encoder instanceof VideoMediaEncoder) {
                isVideoRecording = false;
            }
        }
    };

    public void callbackTakePhoto(int result, String filePath){
        EventBus.getDefault().post(new TakePhotoEvent(EnumElement.TAKE_PHOTO_RESULT.values()[result], filePath));
    }
    public static native int  nativeGetTextureId();
    public static native void nativeInitGL(int renderWidth, int renderHeight, int takePhotoWidth, int takePhotoHeight, CameraVideoRender cameraVideoRender);
    public static native void nativeOnSurfaceChanged(int pNewSurfaceWidth, int pNewSurfaceHeight);
    public static native void nativeSetSurface(Surface surface);
    public static native void nativeDrawPanoramaFrame();
    public static native void nativeDrawFrame();
    public static native void nativeOnResume();
    public static native void nativeOnPause();
}
