package com.uni.vr;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.SurfaceTexture;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.Surface;

import com.uni.common.config.PathConfig;
import com.uni.common.util.FileUtil;
import com.uni.common.util.PreferenceModel;
import com.uni.pano.MainApplication;
import com.uni.pano.config.EnumElement;
import com.uni.pano.event.CameraVideoGLViewCreateEvent;
import com.uni.pano.event.ConnectEvent;
import com.uni.pano.event.CreateFileEvent;
import com.uni.pano.event.DisConnectEvent;
import com.uni.pano.event.RecordVideoEvent;
import com.uni.pano.event.TakePhotoEvent;
import com.uni.pano.widget.CToast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class CameraVideoView extends GLSurfaceView {
    public final static String TAG  =   CameraVideoView.class.getSimpleName();
    private CameraVideoRender mCameraVideoRender;
    public CameraVideoView(Context context) {
        this(context, null);
    }
    public CameraVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        nativeOnCreate(PathConfig.getMediaFileSaveDir(), 90, 1.0f, MainApplication.getInstance(), context.getAssets());
        String logoFilePath = PreferenceModel.getString("logoFileName", "pano_logo_alpha.png");
        if(logoFilePath.lastIndexOf("/") == -1){
            nativeLoadLogoImage(context.getAssets(), logoFilePath);
        }else {
            if(FileUtil.isFileExist(logoFilePath)) {
                nativeLoadLogoImage(null, logoFilePath);
            }else{
                PreferenceModel.putString("logoFileName", "pano_logo_alpha.png");
                nativeLoadLogoImage(context.getAssets(), "pano_logo_alpha.png");
            }
        }
        int zoom = PreferenceModel.getInt("ZOOM", 15);
        nativeUpdateLogoAngle(zoom);

        //boolean showLogo = PreferenceModel.getBoolean("SHOW_LOGO_IN_TAKE_PHOTO", false);
        //nativeUpdateLogoInTakePhoto(showLogo?1:0);

        init();
    }
    protected void init() {
        setEGLContextClientVersion(2);
        mCameraVideoRender = new CameraVideoRender();
        setRenderer(mCameraVideoRender);
        initGesture();
    }
    @Override
    public void onResume() {
        super.onResume();
        this.setRenderMode(RENDERMODE_CONTINUOUSLY);
        mCameraVideoRender.nativeOnResume();
    }
    @Override
    public void onPause() {
        mCameraVideoRender.nativeOnPause();
        this.setRenderMode(RENDERMODE_WHEN_DIRTY);
        super.onPause();
    }
    public void onDestroy(){
        mCameraVideoRender.onDestroy();
        nativeOnDestroy();
    }
    public void runOnGLThread(final Runnable pRunnable) {
        this.queueEvent(pRunnable);
    }

    private EnumElement.VIEW_MODE view_mode = EnumElement.VIEW_MODE.FISH;
    public void doUpdateViewMode(EnumElement.VIEW_MODE viewMode){
        view_mode = viewMode;
        runOnGLThread(new Runnable() {
            @Override
            public void run() {
                nativeUpdateViewMode(view_mode.ordinal());
            }
        });

    }

    public void doUpdateLutFilter(final AssetManager assetManager, final String filePath, final EnumElement.FILTER_TYPE filterType){
        runOnGLThread(new Runnable() {
            @Override
            public void run() {
                nativeUpdateLutFilter(assetManager, filePath, filterType.ordinal());
            }
        });
    }

    public void doConnect(final int vendorId, final int productId, final int fileDescriptor, final String usbfs){
        final SurfaceTexture st = mCameraVideoRender.getSurfaceTexture();
        runOnGLThread(new Runnable() {
            @Override
            public void run() {
                if(st!=null) {
                    Surface s = new Surface(st);
                    int connectResult = nativeConnect(vendorId, productId, fileDescriptor, usbfs, s);
                    EventBus.getDefault().post(new ConnectEvent(EnumElement.CONNECT_RESULT.values()[connectResult]));
                    s.release();
                }else{
                    int connectResult = nativeConnect(vendorId, productId, fileDescriptor, usbfs, null);
                    EventBus.getDefault().post(new ConnectEvent(EnumElement.CONNECT_RESULT.values()[connectResult]));

                }
            }
        });
    }
    public void doDisConnect(){
        runOnGLThread(new Runnable() {
            @Override
            public void run() {
                int result = nativeDisConnect();
                EventBus.getDefault().post(new DisConnectEvent(EnumElement.DISCONNECT_RESULT.values()[result]));
            }
        });
    }
    public void doTakePhoto(final int ticks){
        runOnGLThread(new Runnable() {
            @Override
            public void run() {
                nativeTakePhoto(ticks);
            }
        });
    }
    public void doUpdateRotate(final float xRotateAngle){
        runOnGLThread(new Runnable() {
            @Override
            public void run() {
                nativeUpdateRotate(xRotateAngle, 0.f);
            }
        });
    }

    public void doStartRecord(){
        runOnGLThread(new Runnable() {
            @Override
            public void run() {
                mCameraVideoRender.startRecord();
            }
        });
    }

    public void doStopRecord(){
        runOnGLThread(new Runnable() {
            @Override
            public void run() {
                mCameraVideoRender.stopRecord();
            }
        });
    }

    public void doLoadLogoImage(final AssetManager assetManager, final String logoFilePath){
        runOnGLThread(new Runnable() {
            @Override
            public void run() {
                if(logoFilePath.lastIndexOf("/") == -1){
                    nativeLoadLogoImage(assetManager, logoFilePath);
                }else {
                    nativeLoadLogoImage(null, logoFilePath);
                }
            }
        });
    }
    public void doUpdateLogoAngle(final float angle){
        runOnGLThread(new Runnable() {
            @Override
            public void run() {
                nativeUpdateLogoAngle(angle);
            }
        });
    }

    /** Native methods, implemented in jni folder */
    private static native void nativeOnCreate(String mediaDir, int sphereVertexNum, float sphereRadius, Context context, AssetManager assetMgr);
    private static native void nativeOnDestroy();
    private static native void nativeLoadLogoImage(AssetManager assetManager, String filePath);
    //0:相机连接不成,1:相机连接成功
    private static native int  nativeConnect(final int vendorId, final int productId, final int fileDescriptor, String usbfs, Surface surface);
    private static native int  nativeDisConnect();
    //0:录像开始,1:没有连接相机,2:空间不够,3:正在录制
    private static native void nativeStartRecord();
    private static native void nativeStopRecord();
    //0:开始拍照,1:没有连接相机,2:正在拍照中,3:空间不够...  tickCount为连拍次数
    private static native void nativeTakePhoto(int tickCount);
    private static native void nativeUpdateScale(float scale);
    private static native void nativeUpdateRotate(float xMove, float yMove);
    private static native void nativeUpdateRotateFling(float velocityX, float velocityY);
    private static native void nativeUpdateLutFilter(AssetManager assetManager, String filePath, int filterType);
    private static native void nativeUpdateViewMode(int viewMode);
    private static native void nativeUpdateLogoAngle(float angle);
    //private static native void nativeUpdateLogoInTakePhoto(int bLogo);
    private OnClickListener mOnClickListener;
    @Override
    public void setOnClickListener(OnClickListener l) {
        mOnClickListener = l;
    }
    public void initGesture(){
        setLongClickable(true);
        setOnTouchListener(new TouchTracker(getContext(), new TouchTracker.OnGestureListener() {
            @Override
            public boolean onClick() {
                eCallOnClick();
                return true;
            }

            @Override
            public boolean onScale(float angle) {
                nativeUpdateScale(angle);
                return true;
            }

            @Override
            public boolean onDrag(float yaw, float pitch) {
                nativeUpdateRotate(yaw, pitch);
                return true;
            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                nativeUpdateRotateFling(-velocityX/100.f, -velocityY/100.f);
                return true;
            }

            private void eCallOnClick() {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(CameraVideoView.this);
                }
            }
        }));
    }
}